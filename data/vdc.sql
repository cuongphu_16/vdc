/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : vdc

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2016-01-07 15:06:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pk_about
-- ----------------------------
DROP TABLE IF EXISTS `pk_about`;
CREATE TABLE `pk_about` (
  `about_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `image` text,
  `cate_id` tinyint(4) DEFAULT NULL,
  `language` int(4) NOT NULL DEFAULT '1',
  `description` text,
  `content` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `focus` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(4) DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `views` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`about_id`),
  FULLTEXT KEY `title` (`title`,`content`,`description`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_about
-- ----------------------------
INSERT INTO `pk_about` VALUES ('1', '21212', '21212', 'logojpg.jpg', '1', '1', 'vfdsfs', 'fsdfds', '2016-01-05 10:25:38', null, '1', '0', null, '', '0');
INSERT INTO `pk_about` VALUES ('2', '13 rewr24r ', 'r345r3434', null, '1', '1', null, null, null, null, '1', '0', null, '', '0');

-- ----------------------------
-- Table structure for pk_advs
-- ----------------------------
DROP TABLE IF EXISTS `pk_advs`;
CREATE TABLE `pk_advs` (
  `advs_id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `width` int(5) NOT NULL DEFAULT '0',
  `height` int(5) NOT NULL DEFAULT '0',
  `position` tinyint(1) DEFAULT NULL,
  `language` tinyint(4) DEFAULT '1',
  `sort_order` smallint(6) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `open_new` tinyint(1) DEFAULT '1',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`advs_id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of pk_advs
-- ----------------------------
INSERT INTO `pk_advs` VALUES ('1', '', 'mr.watcharaphon-phonpao-5-1-copy.jpg', '1', '1349', '680', '1', '1', '0', '2016-01-05 15:29:34', null, '2016-01-05 15:29:41', null, '1111111111', '1', 'nội dung 111111111111111111111111111111');
INSERT INTO `pk_advs` VALUES ('2', '', 'william-lee-6-1-copy.jpg', '1', '1349', '680', '1', '1', '0', '2016-01-05 15:29:54', null, null, null, '222222222222222222', '1', 'Nội dung 222222222222222222222222');

-- ----------------------------
-- Table structure for pk_category
-- ----------------------------
DROP TABLE IF EXISTS `pk_category`;
CREATE TABLE `pk_category` (
  `cate_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `type_view` tinyint(1) DEFAULT '0',
  `p_id` int(11) DEFAULT '0',
  `description` varchar(500) NOT NULL,
  `action_run` varchar(255) DEFAULT NULL,
  `content` varchar(500) DEFAULT NULL,
  `language` tinyint(1) NOT NULL,
  `sort_order` int(4) NOT NULL DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `image` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `focus` tinyint(1) NOT NULL DEFAULT '0',
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cate_id`),
  FULLTEXT KEY `title` (`title`,`content`,`description`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_category
-- ----------------------------
INSERT INTO `pk_category` VALUES ('1', 'Giới thiệu chung', 'gioi-thieu-chung', '0', '0', null, '', null, '', '0', '0', '1', null, '2016-01-05 10:32:09', '0', null);
INSERT INTO `pk_category` VALUES ('2', 'Tầm nhìn, sứ mệnh', 'tam-nhin-su-menh', '0', '0', null, '', null, '', '0', '1', '1', null, '2016-01-05 10:32:22', '0', null);
INSERT INTO `pk_category` VALUES ('3', 'Sơ đồ tổ chức', 'so-do-to-chuc', '0', '0', null, '', null, '', '0', '2', '1', null, '2016-01-05 10:32:34', '0', null);
INSERT INTO `pk_category` VALUES ('4', 'Valuation & Advisory', 'valuation-advisory', '1', '0', null, '', null, '', '0', '3', '1', null, '2016-01-05 10:46:49', '0', null);
INSERT INTO `pk_category` VALUES ('5', 'Development Services', 'development-services', '1', '0', null, '', null, '', '0', '1', '1', null, '2016-01-05 10:46:55', '0', null);
INSERT INTO `pk_category` VALUES ('6', 'Project Sales & Marketing', 'project-sales-marketing', '1', '0', null, '', null, '', '0', '2', '1', null, '2016-01-05 10:47:01', '0', null);
INSERT INTO `pk_category` VALUES ('7', 'Investment & Investment Services', 'investment-investment-services', '1', '0', null, '', null, '', '0', '3', '1', null, '2016-01-05 10:47:09', '0', null);
INSERT INTO `pk_category` VALUES ('8', 'Tham gia đầu tư', 'tham-gia-dau-tu', '2', '0', null, '', null, '', '0', '0', '1', null, '2016-01-05 13:51:48', '0', null);
INSERT INTO `pk_category` VALUES ('9', 'Tư vấn phát triển dự án', 'tu-van-phat-trien-du-an', '2', '0', null, '', null, '', '0', '1', '1', null, '2016-01-05 13:51:57', '0', null);

-- ----------------------------
-- Table structure for pk_contact
-- ----------------------------
DROP TABLE IF EXISTS `pk_contact`;
CREATE TABLE `pk_contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `status` int(1) DEFAULT '0' COMMENT '0: chua doc, 1: da doc',
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_contact
-- ----------------------------

-- ----------------------------
-- Table structure for pk_gallery
-- ----------------------------
DROP TABLE IF EXISTS `pk_gallery`;
CREATE TABLE `pk_gallery` (
  `gallery_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `image` text,
  `thumb` text,
  `type` tinyint(4) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT '0',
  `link_yt` text,
  `created_at` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`gallery_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_gallery
-- ----------------------------
INSERT INTO `pk_gallery` VALUES ('1', 'd fhds fk fds fsd', null, '|:|anhmoi4.jpg|:|anhmoi3.jpg|:|anhmoi1.jpg|:|anhmoi2.jpg', 'anhmoi4.jpg', '0', '0', null, null, '1');
INSERT INTO `pk_gallery` VALUES ('2', 'e423423c23', null, '|:|anhmoi1.jpg|:|anhmoi2.jpg|:|anhmoi3.jpg|:|anhmoi4.jpg', 'anhmoi1.jpg', '0', '0', null, null, '1');
INSERT INTO `pk_gallery` VALUES ('3', 'Tiếng hát của nữ doanh nhân bất động sản', 'tieng-hat-cua-nu-doanh-nhan-bat-dong-san', '', null, '1', '1', 'https://www.youtube.com/embed/aGdci9kM0Us', null, '1');

-- ----------------------------
-- Table structure for pk_language
-- ----------------------------
DROP TABLE IF EXISTS `pk_language`;
CREATE TABLE `pk_language` (
  `lang_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` smallint(6) DEFAULT '0',
  `default` tinyint(1) DEFAULT '0',
  `is_web` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`lang_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of pk_language
-- ----------------------------
INSERT INTO `pk_language` VALUES ('1', 'Tiếng Việt', 'vi', '0', '1', '1', '1');
INSERT INTO `pk_language` VALUES ('2', 'Tiếng Anh', 'en', '0', '0', '1', '1');
INSERT INTO `pk_language` VALUES ('3', 'Tiếng Pháp', 'fr', '0', '0', '0', '1');
INSERT INTO `pk_language` VALUES ('4', 'Tiếng Nhật', 'jp', '0', '0', '0', '1');
INSERT INTO `pk_language` VALUES ('5', 'Tiếng Hàn', 'kr', '0', '0', '0', '1');

-- ----------------------------
-- Table structure for pk_lang_vn
-- ----------------------------
DROP TABLE IF EXISTS `pk_lang_vn`;
CREATE TABLE `pk_lang_vn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` text,
  `value` text,
  `type` tinyint(1) DEFAULT '0',
  `type2` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=829 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_lang_vn
-- ----------------------------
INSERT INTO `pk_lang_vn` VALUES ('1', 'Congratulations! You have registered successfully', 'Xin chúc mừng, bạn đã đăng ký thành công!', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('2', 'Dear {user}', 'Xin chào, {user}!', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('3', 'Thank you for registering at website Theodoi247. Your personal account details are confirmed to be as followed:', 'Cảm ơn bạn đã đăng ký tài khoản tại website Quản lý in. Thông tin tài khoản cá nhân của bạn như sau:', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('4', 'Thank you for registering at website Theodoi247.', 'Cảm ơn bạn đã đăng ký tài khoản tại website Quản lý in', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('5', 'Your acount have created at {website} website. Your personal account details are confirmed to be as followed:', 'Tài khoản của bạn đã được tạo tại website {website}. Thông tin tài khoản cá nhân của bạn như sau:', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('6', 'Your email ID: {email}', 'Tài khoản email: {email}', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('7', 'Your acount ID: {user}', 'Tên đăng nhập: {user}', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('8', 'Your acount ID:', 'Tên tài khoản:', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('9', 'Your password: {pass}', 'Mật khẩu: {pass}', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('10', 'Click link below to verify your email address:\r\n{url}', 'Bấm link dưới đây để xác thực email và hoàn tất đăng ký của bạn:\r\n{url}', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('11', 'Click link below to login:\r\n{url}', 'Bấm link dưới đây để đăng nhập vào website:\r\n{url}', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('12', 'Click link below to verify your email address:\r\n', 'Bấm link dưới đây để xác thực email và hoàn tất đăng ký của bạn:\r\n', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('13', 'Active account', 'Kích hoạt tài khoản', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('14', 'Congratulations! Your account is actived', 'Xin chúc mừng! Tài khoản của bạn đã được kích hoạt thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('15', 'Error! Your active code invalid', 'Có lỗi xảy ra! Mã kích hoạt của bạn không chính xác', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('16', 'System will automatically switch to the home page after 5 seconds. If you are not automatically transferred, please press home button', 'Hệ thống sẽ tự động chuyển về trang chủ sau 5s. Nếu không tự động chuyển, hãy ấn nút', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('17', 'System will automatically switch to the home page after 5 seconds.', 'Hệ thống sẽ tự động chuyển về trang chủ sau 5s.', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('18', 'Verification: {salt}', 'Mã xác nhận: {salt}', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('19', 'Enter Confirm Code', 'Nhập mã xác nhận', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('20', 'Register Successful', 'Đăng ký thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('21', 'Best Regards', 'Trân trọng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('22', 'This email is not exist!', 'Email không tồn tại', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('23', 'Your password was successfully updated.', 'Mật khẩu của bạn đã được cập nhật thành công.', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('24', 'If you are not automatically transferred, please press home button.', 'Nếu không tự động chuyển, hãy ấn nút trang chủ.', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('25', 'Get new password', 'Lấy lại mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('26', 'Please, check your mail inbox to activate your account!', 'Bạn vui lòng kiểm tra hòm thư email để kích hoạt tài khoản!', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('27', 'Thank you for registering at website Theodoi247', 'Cảm ơn bạn đã đăng ký tài khoản tại website Theodoi247', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('28', 'Get password Successful!', 'Lấy mật khẩu thành công!', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('29', 'Recovery', 'Khôi phục', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('30', 'You asked to reset your {website} password from {email}. To do so, please click this link:', 'Chúng tôi vừa nhận được yêu cầu thay đổi mật khẩu trên website {website} từ email {email}. Nếu đúng là bạn đã yêu cầu như vậy, vui lòng bấm vào liên kết dưới đây:', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('31', 'This will let you change your password to something new. If you didn\'t ask for this, don\'t worry, we\'ll keep your password safe.', 'Bấm vào liên kết trên, bạn có thể tạo mật khẩu mới cho tài khoản của bạn. Nếu bạn không yêu cầu đổi mật khẩu, hãy bỏ qua thư này, mật khẩu cũ của bạn sẽ vẫn được giữ nguyên.', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('32', 'Services', 'Dịch vụ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('33', 'Services package', 'Gói dich vụ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('34', 'Services Type', 'Gói dịch vụ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('35', 'Basic service', 'Gói tiêu chuẩn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('36', 'Full service', 'Trọn gói', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('37', 'Camera Type', 'Loại camera', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('38', 'Camera in DVR', 'Camera trên đầu ghi DVR', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('39', 'Camera Select', 'Chọn kênh', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('40', 'Channel', 'Kênh', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('41', 'channel', 'kênh', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('42', 'Verify code', 'Mã bảo vệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('43', 'Notice', 'Thông báo', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('44', 'The system will automatically switch pages in', 'Hệ thống sẽ tự động chuyển trang trong', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('45', 'more seconds', 'giây nữa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('46', 'Your account information has been changed successfully', 'Thông tin tài khoản của bạn đã được đổi thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('47', 'Information Your password has been changed successfully', 'Thông tin mật khẩu của bạn đã được đổi thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('48', 'Maybe the message was sent to the spam folder in your inbox', 'Có thể thư được gửi vào mục spam trong hòm thư của bạn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('49', 'Note', 'Lưu ý', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('50', 'Please check your inbox for the link to create your new password', 'Vui lòng kiểm tra hòm thư của bạn để lấy đường dẫn tạo mật khẩu mới', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('51', 'Password information has been sent to your inbox', 'Thông tin mật khẩu đã được gửi tới hòm thư của bạn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('52', 'Can be located in Section activation email spam in your inbox', 'Có thể thư kích hoạt nằm ở mục spam trong hòm thư của bạn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('53', 'Please check your inbox just sign up to get the code to activate your account', 'Vui lòng kiểm tra hòm thư bạn vừa đăng ký để lấy mã kích hoạt tài khoản của bạn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('54', 'Congratulations, you have successfully register', 'Chúc mừng bạn đã đăng ký thành viên thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('55', 'Signin', 'Đăng nhập', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('56', 'Forgot your password', 'Bạn quên mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('57', 'Or', 'Hoặc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('58', 'Signup', 'Đăng ký', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('59', 'Click for new code', 'Click để lấy mã mới', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('60', 'I agree with the', 'Tôi đồng ý với các', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('61', 'terms and conditions', 'điều khoản và điều kiện', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('62', 'of website use', 'sử dụng của website', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('63', 'Information Members', 'Thông tin thành viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('64', 'Changing the password', 'Thay đổi mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('65', 'Logout', 'Thoát', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('66', 'Enter your new password', 'Nhập mật khẩu mới', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('67', 'Restore', 'Khôi phục', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('68', 'Get the password', 'Lấy lại mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('69', 'Send', 'Gửi đi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('70', 'Update', 'Cập nhật', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('71', 'Activate the account', 'Kích hoạt tài khoản', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('72', 'Your account has been activated successfully', 'Tài khoản của bạn đã được kích hoạt thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('73', 'Activation code is incorrect or your account has been activated', 'Mã kích hoạt không chính xác hoặc Tài khoản của bạn đã được kích hoạt', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('74', 'Thank you for using our services.', 'Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi.', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('75', 'Homepage', 'Trang chủ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('76', 'About us', 'Giới thiệu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('77', 'Contact us', 'Liên hệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('78', 'Member', 'Thành viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('79', 'Signin/Signup', 'Đăng nhập/Đăng ký', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('80', 'Privacy Policy', 'Chính sách bảo mật', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('81', 'Extra services', 'Tùy chọn mở rộng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('82', 'Total Amount', 'Thành tiền', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('83', 'Amount', 'Tổng tiền', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('84', 'Contact Form', 'Liên hệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('85', 'Clear', 'Làm lại', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('86', 'Contact Information', 'Thông tin liên hệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('87', 'Fullname', 'Họ tên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('88', 'Message', 'Thông điệp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('89', 'Your name can not be blank', 'Họ tên không được để trống', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('90', 'This is not a valid name', 'Nội dung không hợp lệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('91', 'This field is required', 'Nội dung không được để trống', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('92', 'This is not a valid email address', 'Email của bạn không hợp lệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('93', 'The message is too short', 'Nội dung quá ngắn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('94', 'Contact form submitted', 'Liên hệ của bạn đã được gửi đi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('95', 'We will be in touch soon', 'Chúng tôi sẽ liên hệ trong thời gian sớm nhất', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('96', 'Latest news', 'Tin tức mới nhất', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('97', 'more info', 'Xem thêm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('99', 'Other news', 'Tin tức khác', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('100', 'User information', 'Thông tin thành viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('101', 'Service package', 'Gói dịch vụ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('104', 'Register', 'Đăng ký', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('105', 'Manage Orders', 'Quản lý đơn hàng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('106', 'SortOrder', 'STT', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('107', 'OrderID', 'Mã đơn hàng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('108', 'OrderStatus', 'TT thanh toán', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('109', 'CreateDate', 'Ngày tạo', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('110', 'Edit Orders', 'Chỉnh sửa đơn hàng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('112', 'List video', 'Quản lý video', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('113', 'Click view camera', 'Click vào để xem camera', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('114', 'Show password', 'Hiện mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('115', 'Click edit order', 'Click vào để sửa đơn hàng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('116', 'View', 'Xem', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('117', 'Title', 'Tiêu đề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('118', 'Link video', 'Xem video', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('119', 'Click view video', 'Click để xem video', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('120', 'Back to Home', 'Trở về trang chủ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('121', 'You do not have any orders', 'Bạn không có đơn hàng nào', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('122', 'You do not have any video', 'Bạn không có video nào', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('123', 'Content is being updated ...', 'Nội dung đang được cập nhật...', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('126', 'Other', 'Khác', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('127', 'Manipulate', 'Thao tác', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('129', 'Personal', 'Cá nhân', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('130', 'Enterprise', 'Doanh nghiệp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('131', 'Video search by date', 'Tìm video theo ngày', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('132', 'Information Members Menu', 'Thông tin Thành viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('133', 'Manage Orders Menu', 'Quản lý Đơn hàng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('134', 'List video Menu', 'Quản lý Video', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('135', 'Changing the password Menu', 'Đổi mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('137', 'Inland Payment Gateway', 'Cổng thanh toán nội địa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('138', 'International Payment Gateway', 'Cổng thanh toán quốc tế', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('139', 'Transaction OrderInfo', 'Tên hóa đơn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('140', 'Purchase Amount', 'Số tiền được thanh toán', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('141', 'IP address', 'Địa chỉ IP', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('142', 'Pay Now!', 'Tiếp tục', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('143', 'Merchant ID', 'Được cấp bởi OnePAY', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('144', 'Merchant Transaction Reference', 'ID của giao dịch gửi từ website merchant', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('145', 'VPC Transaction Response Code', 'Mã trạng thái giao dịch trả về', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('146', 'Transaction Response Code Description', 'Trạng thái giao dịch', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('147', 'Transaction Number', 'ID giao dịch trên cổng thanh toán', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('148', 'Manage Orders Storage', 'Đơn hàng mua dung lượng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('149', 'Payment', 'HT. Thanh toán', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('150', 'Storage Order', 'Đơn hàng mua dung lượng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('151', 'Hi', 'Chào', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('152', 'You have successfully registered service pack monitoring', 'Bạn đã đăng ký thành công gói dịch vụ giám sát', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('153', 'Please checkout according to the information below.', 'Vui lòng thanh toán đơn hàng theo thông tin dưới đây.', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('154', 'Order Information', 'Thông tin đơn hàng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('155', 'Package', 'Gói', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('157', 'Expand services', 'Dịch vụ mở rộng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('158', 'Payment information', 'Thông tin thanh toán', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('159', 'Payment at the office', 'Thanh toán tại văn phòng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('160', 'Transfer payment', 'Thanh toán chuyển khoản', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('164', 'Forms of payment', 'Hình thức thanh toán', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('165', 'Total price', 'Tổng giá', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('166', 'Order Status', 'Trạng thái đơn hàng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('167', 'Paid', 'Đã thanh toán', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('168', 'Unpaid', 'Chưa thanh toán', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('169', 'Your storage', 'Lưu trữ của bạn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('170', 'Package name', 'Tên gói', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('171', 'Price', 'Giá', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('172', 'Quantity', 'Số lượng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('173', 'Your order has been sent successfully', 'Đơn hàng của bạn đã được gửi đi thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('174', 'Your order has not been sent successfully', 'Đơn hàng của bạn đã được gửi đi không thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('176', 'User', 'Thành viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('177', 'Sort Order', 'Thứ tự', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('178', 'Created At', 'Ngày tạo', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('179', 'Content', 'Nội dung', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('180', 'Description', 'Mô tả', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('181', 'Type Post', 'Loại bài viết', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('182', 'Image', 'Hình ảnh', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('183', 'Status', 'Trạng thái', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('184', 'Total Experience', 'Số năm kinh nghiệm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('185', 'Language Requirements', 'Ngôn ngữ yêu cầu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('186', 'Position', 'Vị trí', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('187', 'Job Level', 'Cấp bậc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('188', 'Work Place', 'Địa điểm làm việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('189', 'Job Category', 'Ngành nghề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('190', 'Expected Salary', 'Mức lương dự kiến', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('191', 'Education', 'Học vấn và bằng cấp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('192', 'Total', 'Tổng số', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('193', 'Time Display', 'Thời gian hiển thị', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('194', 'Number View Candidate', 'Số lần xem ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('195', 'Function', 'Chức năng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('196', 'Degree Level', 'Bằng cấp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('197', 'Major', 'Chuyên ngành', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('198', 'From', 'Từ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('199', 'To', 'Đến', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('200', 'Resume', 'Hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('201', 'Program', 'Chương trình', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('202', 'Reviewer', 'Người nhận xét', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('203', 'Company', 'Công ty', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('204', 'Service', 'Dịch vụ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('205', 'Scale', 'Quy mô công ty', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('206', 'Company Name', 'Tên công ty', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('207', 'Company Address', 'Địa chỉ công ty', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('208', 'Company Profile', 'Sơ lược về công ty', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('209', 'Company Logo', 'Logo công ty', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('210', 'Level', 'Cấp bậc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('211', 'Apply Deadline', 'Hạn nộp hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('212', 'Name To Contact', 'Tên để liên hệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('213', 'Email To Contact', 'Email để liên hệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('214', 'Job Request', 'Yêu cầu công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('215', 'List Resumes Apply', 'Danh sách hồ sơ ứng tuyển', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('216', 'Focus', 'Nổi bật', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('217', 'Day Focus', 'Số ngày nổi bật', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('218', 'Views', 'Lượt xem', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('219', 'Choose Scale', 'Chọn quy mô', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('220', 'Choose Job Category', 'Vui lòng chọn ngành nghề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('221', 'Choose Service Package', 'Chọn gói dịch vụ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('222', 'Address', 'Địa chỉ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('223', 'Passwrod', 'Mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('224', 'Password', 'Mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('225', 'Password Cf', 'Nhập lại mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('226', 'Mobile', 'Điện thoại', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('227', 'Phone', 'Điện thoại', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('228', 'User Type', 'Loại thành viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('229', 'Avatar', 'Ảnh đại diện', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('230', 'Countries', 'Quốc gia', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('231', 'Firstname', 'Tên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('232', 'Lastname', 'Họ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('233', 'Country', 'Quốc gia', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('234', 'Region', 'Tỉnh thành', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('235', 'District', 'Quận huyện', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('236', 'Male', 'Nam', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('237', 'Female', 'Nữ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('238', 'Gender', 'Giới tính', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('239', 'Birthday', 'Ngày sinh', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('240', 'Company Name2', 'Tên giao dịch', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('241', 'Candidate', 'Ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('242', 'Employer', 'Nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('243', 'About User', 'Giới thiệu bản thân', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('244', 'Type Account', 'Loại tài khoản', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('245', 'Role', 'Quyền', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('246', 'Nick', 'Tên thường gọi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('247', 'Skill', 'Kỹ năng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('248', 'Language Field', 'Ngôn ngữ sử dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('249', 'Star Count', 'Điểm đánh giá', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('250', 'Choose only 3 place', 'Chọn tối đa 3 địa điểm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('251', 'Choose work place', 'Vui lòng chọn địa điểm làm việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('252', 'Place', 'Địa điểm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('253', 'Code', 'Mã', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('254', 'Choose only 3 language', 'Chọn tối đa 3 ngôn ngữ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('255', 'Choose language', 'Chọn ngôn ngữ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('256', 'Language', 'Ngôn ngữ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('257', 'Choose only 3 trades', 'Chọn tối đa 3 ngành nghề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('258', 'Choose trades', 'Vui lòng chọn ngành nghề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('259', 'Trades', 'Ngành nghề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('260', 'Selected', 'Đã chọn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('261', 'Career Objective', 'Mục tiêu nghề nghiệp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('262', 'Career Highlights', 'Sự nghiệp nổi bật', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('263', 'Work Experience', 'Kinh nghiệm công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('264', 'Employer View', 'Các nhà tuyển dụng đã xem hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('265', 'Number Download Resumes', 'Số lần tải về hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('266', 'Reference', 'Tham khảo', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('267', 'Monday', 'Thứ 2', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('268', 'Tuesday', 'Thứ 3', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('269', 'Wednesday', 'Thứ 4', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('270', 'Thurday', 'Thứ 5', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('271', 'Friday', 'Thứ 6', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('272', 'Saturday', 'Thứ 7', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('273', 'Sunday', 'Chủ nhật', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('274', 'Edit Resumes', 'Chỉnh sửa hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('275', 'Parent', 'Danh mục cha', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('276', 'Search jobs by category', 'Tìm việc theo ngành nghề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('277', 'Job', 'Việc làm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('278', 'Statistical', 'Thống kê', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('279', 'School', 'Trường', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('280', 'Qualifications', 'Trình độ chuyên môn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('281', 'From (month/year)', 'Từ (tháng/năm)', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('282', 'To (month/year)', 'Đến (tháng/năm)', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('283', 'Choose school', 'Chọn trường', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('284', 'Choose degree level', 'Vui lòng chọn bằng cấp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('285', 'Please enter all fields have a star', 'Vui lòng nhập đầy đủ các trường có dấu sao', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('286', 'At the school', 'Tại trường', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('287', 'Time', 'Thời gian', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('288', 'Choose Region', 'Chọn Tỉnh/Thành', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('289', 'Choose District', 'Chọn Quận/Huyện', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('290', 'Are you sure you want to delete', 'Bạn có chắc chắn muốn xóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('291', 'Add', 'Thêm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('292', 'Type of work', 'Loại hình công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('293', 'Earnings / month', 'Thu nhập / tháng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('294', 'Born Address', 'Địa chỉ nơi sinh', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('295', 'Information Personal', 'Thông tin cá nhân', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('296', 'Where temporary registration', 'Nơi đăng ký tạm trú', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('297', 'Job Description wishes', 'Mô tả công việc mong muốn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('298', 'New Resumes', 'Tạo hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('299', 'Step 1', 'Bước 1', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('300', 'Step', 'Bước', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('301', 'General Information', 'Thông tin chung', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('302', 'Detail Infomation', 'Thông tin chi tiết', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('303', 'Review & Finish', 'Xem lại & Hoàn tất', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('304', 'Rest', 'Còn lại', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('305', 'characters', 'ký tự', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('306', 'Continue', 'Tiếp tục', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('307', 'Cancel', 'Hủy bỏ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('308', 'Level Lastes', 'Cấp bậc hiện tại', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('309', 'Company Lastes', 'Công ty gần đây nhất', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('310', 'Work Lastes', 'Công việc gần đây nhất', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('311', 'Desired position', 'Vị trí mong muốn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('312', 'Desired level', 'Cấp bậc mong muốn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('313', 'Desired work', 'Công việc mong muốn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('314', 'or login by your social account', 'hoặc đăng nhập bằng tài khoản', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('315', 'Negotiate', 'Thương lượng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('316', 'Allow search', 'Cho phép tìm kiếm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('317', 'The process of working', 'Quá trình làm việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('318', 'I graduated / no experience jobs', 'Tôi mới tốt nghiệp / chưa có kinh nghiệm việc làm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('319', 'Languages qualifications (up to 3 languages)', 'Trình độ ngoại ngữ (tối đa 3 ngoại ngữ)', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('320', 'Search result', 'Kết quả tìm kiếm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('321', '<strong style=\"color: #c00\">{number}</strong> {item} found', 'Tìm thấy <strong style=\"color: #c00\">{number}</strong> {item} phù hợp.', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('322', 'Search keyword', 'Từ khóa tìm kiếm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('323', 'Skill Highlights', 'Kỹ năng nổi bật', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('324', 'Experience Highlights', 'Kinh nghiệm nổi bật', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('325', 'More Information', 'Thông tin thêm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('326', 'Back', 'Quay lại', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('327', 'No resume found, please refine your search criteria.', 'Không tìm thấy hồ sơ phù hợp, vui lòng xem lại điều kiện tìm kiếm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('328', 'Display', 'Hiển thị', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('329', 'result', 'kết quả', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('330', 'year', 'năm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('331', 'Languages qualifications', 'Trình độ ngoại ngữ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('332', 'Desired salary', 'Mức lương mong muốn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('333', 'Job Information', 'Thông tin đăng tuyển', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('334', 'Create job', 'Đăng việc làm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('335', 'Job title', 'Chức danh', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('336', 'Edit detail information', 'Chỉnh sửa thông tin chi tiết', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('337', 'Job type', 'Loại hình công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('338', 'Name of job', 'Tên công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('339', 'Salary', 'Mức lương', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('340', 'Agreement', 'Thỏa thuận', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('341', 'Enter salary', 'Nhập mức lương', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('342', 'You have <span id=\"job-desc-count-down\">{number}</span> characters remaining', 'Còn lại <span id=\"job-desc-count-down\">{number}</span> ký tự', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('343', 'Job description', 'Mô tả công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('344', 'Job requirements', 'Yêu cầu khác', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('345', 'Change Avatar', 'Đổi hình', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('346', 'Please enter {attribute} information', 'Vui lòng nhập thông tin {attribute}', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('347', 'Image not allowed empty', 'Ảnh không được để trống', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('348', 'Upload Image', 'Tải ảnh lên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('349', 'Change', 'Thay đổi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('350', 'Enter the province/city of workplace', 'Nhập tỉnh/thành phố nơi làm việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('351', 'Enter job category name', 'Nhập vào tên ngành nghề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('352', 'Hightlight', 'Nổi bật', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('353', 'Next', 'Tiếp tục', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('354', 'Previous', 'Quay lại', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('355', 'Area', 'Khu vực', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('356', 'Posting Date', 'Ngày đăng tuyển', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('357', 'Job start date', 'Ngày bắt đầu công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('358', 'Apply Dealine', 'Hạn nộp hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('359', 'Salary /month (VND)', 'Mức lương / tháng (VND)', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('360', 'Salary / Month', 'Thu nhập / Tháng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('361', 'Applies', 'Lượt nộp hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('362', 'Apply Now', 'Nộp hồ sơ ngay', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('363', 'Thank you for registering at website J4S', 'Cảm ơn bạn đã đăng ký tại website J4S', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('364', 'System will automatically switch to the home page after', 'Hệ thống sẽ tự động chuyển về trang chủ sau', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('365', 'seconds', 'giây', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('366', 'Remember me', 'Ghi nhớ đăng nhập', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('367', 'Please activate your account before logging', 'Vui lòng kích hoạt tài khoản của bạn trước khi đăng nhập', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('368', 'if you do not have an account, please', 'Nếu bạn chưa có tài khoản, hãy', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('369', 'Register now', 'Đăng ký ngay', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('370', 'I forgot my password', 'Tôi quên mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('371', 'Or login using', 'Hoặc đăng nhập bằng cách sử dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('372', 'Create your candidate account', 'Tạo tài khoản ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('373', 'Create your employer account', 'Tạo tài khoản nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('374', 'participated in J4S', 'tham gia J4S', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('375', 'Welcome', 'Chào mừng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('376', 'An email with account activation URL sent to the email address', 'Một email kích hoạt tài khoản đã được gửi đến email', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('377', 'Note: Please check your spam box if you do not see the email text in the inbox', 'Lưu ý: Vui lòng kiểm tra hộp thư rác của bạn nếu bạn không thấy email kích hoạt trong hộp thư đến', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('378', 'You can log into the system using an account now signed up.', 'Bạn có thể đăng nhập hệ thống bằng tài khoản vừa đăng ký', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('379', 'You have successfully registered by the system but can not send mail for your activation, you can not log into the system now. <br> Please contact Web Administrator for assistance', 'Bạn đã đăng ký thành công nhưng chúng tôi không thể gửi email kích hoạt tài khoản, bạn chưa thể đăng nhập hệ thống ngay lúc này. <br> Hãy liên hệ với quản trị hệ thống để được trợ giúp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('380', 'Congratulations on your successful registration. <br> You can log into the system using an account now signed up.', 'Chúc mừng bạn đã đăng kí thành công. <br> Bạn có thể đăng nhập hệ thống bằng tài khoản vừa đăng kí', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('381', 'Please', 'Hãy', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('382', 'now', 'ngay', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('383', 'You are already a member', 'Bạn đã là thành viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('384', 'login', 'đăng nhập', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('385', 'Recover password', 'Khôi phục mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('386', 'Register successfully', 'Đăng kí thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('387', 'Thank you for registering at website {sitename}. Your personal account details are confirmed to be as followed:', 'Cám ơn bạn đã đăng kí tại website {sitename}. Thông tin tài khoản của bạn như sau:', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('388', 'Student Register', 'Ứng viên đăng ký', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('389', 'Contact Info', 'Thông tin liên lạc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('390', 'Terms and regulations', 'Điều khoản và quy định', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('391', 'Send to Friends', 'Gửi cho bạn bè', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('392', 'Share Job', 'Chia sẻ công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('393', 'You get to share work from a person at the address {email} with content', 'Bạn nhận được chia sẻ công việc từ một người bạn tại địa chỉ {email} với nội dung', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('394', 'You get to share work from {person} at the address {email} with content', 'Bạn nhận được chia sẻ công việc từ { person}  tại địa chỉ {email} với nội dung', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('395', 'Click on the following link to view job details', 'Click vào link sau để xem chi tiết công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('396', 'Send email to friend successfully', 'Gửi mail cho bạn bè thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('397', 'Updated At', 'Ngày cập nhật', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('398', 'Search', 'Tìm kiếm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('399', 'Allow', 'Cho phép', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('400', 'Not Allow', 'Không cho phép', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('401', 'My Profile', 'Hồ sơ của tôi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('402', 'Error occurs', 'Đã có lỗi xảy ra', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('403', 'Create Resumes', 'Tạo hồ sơ mới', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('404', 'Hide', 'Ẩn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('405', 'You do not have permission to delete', 'Bạn không có quyền xóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('406', 'Has some errors, please try later', 'Có lỗi xảy ra, vui lòng thử lại sau', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('407', 'The job is deleted', 'Công việc đã được xóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('408', 'Undo', 'Hoàn tác', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('409', 'Deleted', 'Đã xóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('410', '{number} job(s) is deleted', '{number} công việc đã được xóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('411', 'Attractive jobs', 'Công việc hấp dẫn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('412', 'Quick Search', 'Tìm kiếm việc nhanh', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('413', 'The average income per hour', 'Thu nhập bình quân mỗi giờ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('414', 'Job Description', 'Mô tả công việc', '1', '0');
INSERT INTO `pk_lang_vn` VALUES ('415', 'Top of candidate', 'Sinh viên hàng đầu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('416', 'Review point', 'Điểm đánh giá', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('417', 'Try now', 'Trải nghiệm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('418', 'Buy now', 'Mua ngay', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('419', '{number} {item} found', 'Tìm thấy {number} {item}', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('420', 'Employer function', 'Chức năng Nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('421', 'page', 'trang', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('422', 'Detail', 'Xem chi tiết', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('423', 'Search by category', 'Tìm theo ngành nghề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('424', 'Mail Friend', 'Gửi đến Email', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('425', 'Resumes detail', 'Chi tiết hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('426', 'Resume detail', 'Chi tiết hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('427', 'System error', 'Lỗi hệ thống', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('428', 'Enter your email', 'Nhập email của bạn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('429', 'Send job information to me', 'Gửi việc cho tôi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('430', 'Create New', 'Tạo mới', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('431', 'The resumes is deleted', 'Hồ sơ đã được xóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('432', 'The alert job is deleted', 'Thông báo công việc đã được xóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('433', 'My Job', 'Việc làm của tôi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('434', 'Experiences', 'Số năm kinh nghiệm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('435', 'Foreign language', 'Ngoại ngữ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('436', 'Highest Level', 'Cấp bậc cao nhất', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('437', 'Total Years of Experience', 'Số năm kinh nghiệm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('438', 'Expected Position', 'Vị trí mong muốn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('439', 'Expected Job Level', 'Cấp bậc mong muốn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('440', 'Expected Work Place', 'Nơi làm việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('441', 'Expected Job Category', 'Ngành nghề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('442', 'Career Objectives', 'Mục tiêu nghề nghiệp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('443', 'Key Skills', 'Kỹ năng nổi bật', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('444', 'Employment History', 'Kinh nghiệm làm việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('445', 'Education History', 'Quá trình học tập', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('446', 'Start date', 'Ngày bắt đầu làm việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('447', 'Just graduated', 'Mới tốt nghiệp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('448', 'Date Save', 'Ngày lưu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('449', 'You have not saved any jobs', 'Bạn chưa lưu công việc nào', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('450', 'Thursday', 'Thứ 5', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('451', 'Viewing Newest', 'Ngày xem mới nhất', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('452', 'Qualification', 'Bằng cấp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('453', 'Subject', 'Chuyên ngành', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('454', 'Month', 'Tháng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('455', 'Create a job', 'Đăng việc làm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('456', 'My jobs', 'Công việc của tôi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('457', 'Edit Profile', 'Chỉnh sửa hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('458', 'No {item} found, please refine your search criteria.', 'Không tìm thấy dữ liệu {item}, vui lòng kiểm tra điều kiện tìm kiếm.', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('459', 'Select all', 'Chọn tất cả', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('460', 'Select none', 'Bỏ chọn tất cả', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('461', 'Delete', 'Xóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('462', 'Posted jobs', 'Công việc đã đăng tuyển', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('463', 'Suitable profile', 'Hồ sơ phù hợp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('464', 'Job level', 'Cấp bậc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('465', 'Posted date', 'Ngày đăng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('466', 'General information', 'Thông tin chung', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('467', 'Year', 'Năm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('468', 'Job information', 'Thông tin việc làm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('469', 'Contact information', 'Thông tin liên hệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('470', 'Employer Views', 'Nhà tuyển dụng đã xem hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('471', 'Employer Feedback', 'Phản hồi của nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('472', 'Alert Job', 'Thông báo việc làm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('473', 'FEEDBACK OF EMPLOYER', 'Phản hồi của nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('474', 'The select field is deleted', 'Các trường được chọn đã bị xóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('475', 'Send job infomation to me', 'Gửi việc cho tôi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('476', 'Not required', 'Không yêu cầu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('477', 'Choose Level', 'Chọn cấp bậc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('478', 'Choose type of work', 'Vui lòng chọn loại hình công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('479', 'My candidates', 'Quản lý ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('480', 'Province', 'Tỉnh/ Thành phố', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('481', 'Complete', 'Hoàn thành', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('482', 'Business Description', 'Mô tả về công ty', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('483', 'Conpany logo', 'Logo công ty', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('484', 'Name of contact', 'Tên liên hệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('485', 'Business Size', 'Quy mô công ty', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('486', 'Other Posts', 'Bài viết liên quan', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('487', 'Resume name', 'Tên hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('488', 'Apply date', 'Ngày ứng tuyển', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('489', 'views', 'lượt xem', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('490', 'My candidate', 'Ứng viên của tôi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('491', 'You do not have any resumes to apply', 'Bạn chưa có hồ sơ nào để ứng tuyển', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('492', 'Create resumes now', 'Tạo hồ sơ ngay', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('493', 'Apply', 'Nộp hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('494', 'Select the resumes to apply', 'Chọn hồ sơ để ứng tuyển', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('495', 'Set title to bold with red color 15 days', 'Tô đậm đỏ 15 ngày', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('496', 'Display on home page 15 days', 'Hiển thị trang chủ 15 ngày', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('497', 'Prioritized in search results', 'Ưu tiên trong kết quả tìm kiếm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('498', 'View contact detail of candidate', 'Xem thông tin ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('499', 'Apply Success', 'Nộp hồ sơ thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('500', 'Subscribe to newsletters success', 'Đăng ký nhận bản tin thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('501', 'You have 1500 characters remaining', 'Còn lại 1500 ký tự', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('502', 'Career Advice', 'Tư vấn nghề nghiệp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('503', 'Price for 2 post', 'Giá cho 2 tin đăng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('504', 'Price for 3 post', 'Giá cho 3 tin đăng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('505', 'Price for 4 post', 'Giá cho 4 tin đăng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('506', 'Price for 5 post', 'Giá cho 5 tin đăng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('507', 'Send Contact', 'Gửi liên hệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('508', 'Increase search result percent', 'Tỷ lệ hiệu quả tìm kiếm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('509', 'Send contact success', 'gửi liên hệ thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('510', 'Post of job', 'Đăng tuyển', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('511', 'New Contact from j4s', 'Liên hệ mới từ j4s', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('512', 'You have received a mail contact from the system j4s.vn', 'Bạn nhận được một mail liên hệ từ hệ thống website j4s.vn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('513', 'Confirm the information sent to J4S', 'Xác nhận thông tin liên hệ gửi đến J4S', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('514', 'Customer sent Email to J4S', 'Khách hàng gửi liên hệ tới J4S', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('515', 'You just sent a mail contact to the system j4s.vn website', 'Bạn vừa gửi một mail liên hệ tới hệ thống website j4s.vn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('516', 'You just sent a mail contact to the system j4s.vn website with content', 'Bạn vừa gửi một mail liên hệ tới hệ thống website j4s.vn với nội dung', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('517', 'Enter your fullname', 'Quý khách vui lòng nhập họ tên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('518', 'Enter your content', 'Quý khách vui lòng nhập nội dung', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('519', 'Latest Posts', 'Bài viết mới nhất', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('520', 'Most Read Articles', 'Bài đọc nhiều nhất', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('521', 'Suggestions for you', 'Gợi ý cho bạn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('522', 'Successful Lessons', 'Bài học thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('523', 'Career advancement', 'Thăng tiến sự nghiệp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('524', 'Search Job', 'Tìm việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('525', 'Post Focus', 'Tin nổi bật', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('526', 'Type of service', 'Loại gói dịch vụ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('527', 'Job applyed', 'Công việc đã nộp hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('528', 'You do not apply for any position', 'Bạn chưa nộp hồ sơ cho vị trí nào', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('529', 'Deselect all', 'Bỏ chọn tất cả', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('530', 'delete', 'Xóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('531', 'Feedback', 'Phản hồi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('532', 'Date Received', 'Ngày nhận', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('533', 'You do not have any feedback from employers', 'Không có phản hồi của nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('534', 'Update Resumes', 'Cập nhật hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('535', 'position', 'vị trí', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('536', 'Please login to use this function', 'Vui lòng đăng nhập để thực sử dụng chức năng này', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('537', 'Happy birthday!', 'Chúc mừng sinh nhật!', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('538', 'Apply Job', 'Ứng tuyển công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('539', 'You just sent applying for jobs', 'Bạn vừa gửi ứng tuyển vào công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('540', 'Candidate Apply Job', 'Ứng viên ứng tuyển', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('541', 'just send the file to apply to work', 'vừa gửi hồ sơ để ứng tuyển vào công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('542', 'just send the resumes to apply to work', 'vừa gửi hồ sơ để ứng tuyển vào công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('543', 'Candidate feedback', 'Ứng viên gửi phản hồi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('544', 'you just received feedback from {from} with content', 'bạn vừa nhận được phản hồi từ {from} với nội dung', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('545', 'We send you the latest updates on the website j4s.vn', 'Chúng tôi gửi bạn những cập nhật mới nhất tại website j4s.vn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('546', 'Latest Work', 'Công việc mới nhất', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('547', 'Latest Career Advice', 'Tư vấn nghề nghiệp mới nhất', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('548', 'We send you notification of jobs matching your request. Click on a job title to view the job details', 'Chúng tôi gửi cho bạn thông báo những việc làm phù hợp với yêu cầu của bạn. Click vào tiêu đề mỗi công việc để xem chi tiết công việc.', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('549', 'Thank you for using the services of job alert system j4s', 'Cám ơn bạn đã sử dụng dịch vụ thông báo việc làm của  hệ thống j4s', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('550', 'Phone To Contact', 'Điện thoại để liên hệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('551', 'Suitable Job', 'Việc làm phù hợp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('552', 'Expected Salary From', 'Mức lương từ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('553', 'Expected Salary To', 'Đến mức lương', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('554', 'No job found, please refine your search criteria', 'Không tìm thấy công việc nào. Vui lòng xem lại điều kiện tìm kiếm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('555', 'Service package register successfully', 'Đăng ký gói dịch vụ thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('556', 'You have been register service package successfully. Please check your email and complete payment proccess to active your service', 'Bạn đã đăng ky gói dịch vụ thành công. Vui lòng kiểm tra email và hoàn tất quá trình thanh toán để kích hoạt dịch vụ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('557', 'System will be redirect in {count}s', 'Hệ thống sẽ tự động chuyển trang trong {count} giây', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('558', 'Have an error in payment process. Please retry or choose other payment method', 'Có lỗi xảy ra trong quá trình thanh toán. Vui lòng thử lại hoặc chọn hình thức thanh toán khác', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('559', 'Create account', 'Tạo tài khoản', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('560', 'Click {here} for the full update personal information before posting your resume', 'Click vào {here} để cập nhật đầy đủ thông tin cá nhân trước khi đăng tải hồ sơ của bạn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('561', 'here', 'đây', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('562', 'You do not have any resumes', 'Bạn chưa có hồ sơ nào', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('563', 'Used for job post', 'Đã sử dụng cho tin đăng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('564', 'We have sent an email to your inbox. Please check your inbox or spam mailbox to make changes to email accounts', 'Chúng tôi đã gửi một email vào hòm thư của bạn. Vui lòng kiểm tra hộp thư đến hoặc hộp thư spam để tiến hành thay đổi email tài khoản này', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('565', 'Edit Email', 'Thay đổi Email', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('566', 'We just received a proposal to change your email account on the website j4s.vn. If you are a proposal, please click on the link below to change the email. If not you, ignore this mail and check your account on the system j4s.vn', 'Chúng tôi vừa nhận được đề nghị thay đổi email tài khoản của bạn trên hệ thống website j4s.vn. Nếu bạn là người đề nghị, vui lòng click vào link dưới đây để thay đổi email. Nếu không phải bạn, hãy bỏ qua mail này và kiểm tra lại tài khoản của bạn trên hệ thống j4s.vn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('567', 'Choose', 'Chọn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('568', 'Email Confirm', 'Nhập lại email mới', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('569', 'Jobseekers', 'Người tìm việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('570', 'Email to login to register on employers. Please login page for employers', 'Email đăng nhập được đăng ký trên trang nhà tuyển dụng. Vui lòng đăng nhập trên trang dành cho nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('571', 'Email to login to register on the candidate. Please login page for candidates', 'Email đăng nhập được đăng ký trên trang ứng viên. Vui lòng đăng nhập trên trang dành cho ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('572', 'Please enter a positive integer', 'Vui lòng nhập số nguyên dương', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('573', 'Have an error, please try again later', 'Email đã đăng ký! Vui lòng nhập email chưa đăng ký!', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('574', 'Deleted successfully', 'Xóa thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('575', 'You are not getting any resume alert', 'Bạn chưa tạo thông báo hồ sơ nào', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('576', 'Alert resume', 'Thông báo hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('577', 'Resume alert', 'Thông báo hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('578', 'No record found', 'Không tìm thấy dữ liệu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('579', 'Service package used report', 'Báo cáo dịch vụ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('580', 'No.', 'STT', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('581', 'Email to login to register on employers. Would you like to login and redirect of employer page', 'Email đăng nhập được đăng ký trên trang nhà tuyển dụng. Bạn có muốn đăng nhập và chuyển về trang của nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('582', 'Email to login to register on candidate. Would you like to login and redirect of employer page', 'Email đăng nhập được đăng ký trên trang nhà tuyển dụng. Bạn có muốn đăng nhập và chuyển về trang của nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('583', 'Email to login to register on candidate. Would you like to login and redirect of candidate page', 'Email đăng nhập được đăng ký trên trang ứng viên. Bạn có muốn đăng nhập và chuyển về trang của ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('584', 'New Email', 'Email mới', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('585', 'You are logged in as a {person}. You must sign out to perform this function. Do you want to sign out?', 'Bạn đang đăng nhập với tư cách là {person}. Bạn phải thoát khỏi tài khoản hiện tại trước khi thực hiện chức năng này. Bạn có chắc chắn thoát không?', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('586', 'End of homepage display', 'Ngày hết hạn hiển thị trang chủ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('587', 'Service Package report', 'Báo cáo dịch vụ ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('588', 'Agree', 'Đồng ý', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('589', 'Check your input', 'Vui lòng kiểm tra lại dữ liệu đã nhập', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('590', 'Add Education', 'Thêm trình độ chuyên môn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('591', 'Enter the district/province/city of workplace', 'Nhập vào quận huyện/tỉnh/thành phố', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('592', 'More', 'Chi tiết', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('593', 'Highlight Job', 'Công việc nổi bật', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('594', 'Have an error in payment proccess. Please retry or choose other payment method', 'Có lỗi xảy ra, vui lòng thử lại hoặc chọn hình thức thanh toán khác', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('595', 'Resume report', 'Thông báo hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('596', 'Create new', 'Thêm mới', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('597', 'When you click {strong}Create account{/strong} mean that you agree with {a}terms and conditions{/a} of {sitename}.', 'Khi bạn click {strong}Tạo tài khoản{/strong} đồng nghĩa với việc bạn đã đọc và chập nhận {a}điểu khoản, điều kiện{/a} của{sitename}.', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('598', 'All Trades', 'Tất cả ngành nghề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('599', 'All Level', 'Tất cả cấp bậc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('600', 'All Work Place', 'Tất cả địa điểm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('601', 'everyday', 'Hàng ngày', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('602', 'Every week', 'Hàng tuần', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('603', 'Need an account? Please take 30 seconds to', 'Bạn chưa có tài khoản? Hay dành 30s để', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('604', 'Forgot your password?', 'Bạn quên mật khẩu?', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('605', 'I would like to receive weekly newsletter from {sitename}', 'Tôi muốn nhận bản tin hàng tuần từ {sitename}', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('606', 'Register with your social account', 'Đặng ký với tài khoản mạng xã hội', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('607', 'You are a member?', 'Bạn đã là thành viên?', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('608', 'Please <a href=\"\" class=\"underline\">login</a> here!', 'Hãy <a href=\"\" class=\"underline\">đăng nhập</a> ngay!', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('609', '{a} Recover your password {/a} now!', '{a} Khôi phục mật khẩu {/a} ngay!', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('610', 'You can log into the system with your account', 'Bạn có thể đăng nhập vào hệ thống với tài khoản vừa đăng ký', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('611', 'Congratulations on your successful registration.', 'Chúc mừng bạn đã đăng ký thành công.', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('612', 'All', 'Tất cả', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('613', 'You have {span}{number}{/span} characters remaining', 'Còn lại {span}{number}{/span} ký tự', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('614', 'You have max {span}{number}{/span} characters', 'Tối đa {span}{number}{/span} ký tự', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('615', 'Please select job(s) you want to delete', 'Vui lòng chọn công việc muốn xóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('616', 'Salary is incorrect', 'Mức lương không phù hợp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('617', 'Guide', 'Hướng dẫn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('618', 'Student', 'Sinh viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('619', 'Find work', 'Tìm việc làm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('620', 'Contact with us', 'Liên hệ với chúng tôi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('621', 'Please enter a valid format (year-month-day)', 'Vui lòng nhập đúng định dạng (Năm-tháng-ngày)', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('622', 'Please enter the verify code', 'Vui lòng nhập mã bảo vệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('623', 'Verification incorrect', 'Mã xác nhận không đúng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('624', 'To the email address:{email} of each', 'Gửi mail tới địa chỉ:{email}', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('625', 'Forgot Pass', 'Quên mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('626', 'Get Pass', 'Khôi phục mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('627', 'Retype password', 'Nhập lại mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('628', 'Search resume', 'Tìm kiếm ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('629', 'This email is already registered. Please enter a different email', 'Email này đã được đăng ký. Vui lòng nhập Email khác', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('630', 'Please {a}login{/a} here!', 'Hãy {a}đăng nhập{/a} ngay!', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('631', 'Please select number of star', 'vui lòng chọn số sao', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('632', 'Please enter your review content', 'Vui lòng nhập nội dung đánh giá', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('633', 'Would not recommend this user', 'Tôi không thực sự hài lòng với hồ sơ ứng viên này', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('634', 'Just okay. Could have been better', 'Hồ sơ tốt nhưng cần cải thiện hơn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('635', 'Pretty good overall', 'Hồ sơ tương đối tốt', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('636', 'Great job. Recommended', 'Hồ sơ rất tốt', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('637', 'Excellent job! Highest recommendation', 'Rất tuyệt! Hồ sơ rất hoàn hảo', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('638', 'Your review', 'Điểm đánh giá', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('639', 'Write review', 'Gửi đánh giá', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('640', 'Choose only 1 employer', 'Chỉ chọn 1 nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('641', 'Choose employer', 'Chọn nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('642', 'Enter your first name', 'Vui lòng nhập tên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('643', 'Enter your last name', 'Vui lòng nhập họ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('644', 'Enter your password', 'Vui lòng nhập mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('645', 'Enter your password confirm', 'Vui lòng nhập lại mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('646', 'Enter your position', 'Vui lòng nhập vị trí', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('647', 'Enter your job level', 'Vui lòng nhập cấp bậc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('648', 'Enter your work place', 'Vui lòng nhập địa điểm làm việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('649', 'Enter your job category', 'Vui lòng nhập ngành nghề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('650', 'Enter your type of work', 'Vui lòng chọn loại hình công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('651', 'Enter your title', 'Vui lòng nhập tiêu đề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('652', 'Enter your expected salary', 'Vui lòng nhập mức lương dự kiến', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('653', 'Add language proficiency', 'Thêm trình độ ngoại ngữ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('654', 'From date', 'Từ ngày', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('655', 'To date', 'Đến ngày', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('656', 'Recruiment manager', 'Quản lý tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('657', 'Quản lý ứng viên', 'Candidate manager', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('658', 'Account manager', 'Quản lý tài khoản', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('659', 'Candidate manager', 'Quản lý ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('660', 'Last review', 'Đánh giá trước', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('661', 'Remove resume', 'Loại hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('662', 'Change password', 'Đổi mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('663', 'Business information', 'Thông tin doanh nghiệp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('664', 'Contact address information', 'Địa chỉ liên hệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('665', 'Old pass word is not correct', 'Mật khẩu cũ không chính xác', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('666', 'Your old password is not correct', 'Mật khẩu cũ không chính xác', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('667', 'Your information updated successfully', 'Cập nhật thông tin thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('668', 'Please check your input content', 'Vui lòng kiểm tra thông tin nhập vào', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('669', 'Delete logo', 'Xóa logo', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('670', 'Save', 'Lưu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('671', 'Search Work', 'Tìm kiếm việc làm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('672', 'Save work success', 'Lưu công việc thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('673', 'When you click Create Account below means that you agree to the above items', 'Khi bấm vào nút Tạo tài khoản bên dưới đồng nghĩa với việc bạn đồng ý các các mục ở trên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('674', 'want to receive information automatically from j4s.vn', 'Tôi muốn nhận tin tự động từ j4s.vn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('675', 'I want to receive information automatically from j4s.vn', 'Tôi muốn nhận tin tự động từ j4s.vn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('676', 'Resumes Management', 'Quản lý hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('677', 'Note: This link is valid only within 24h', 'Chú ý: Đường dẫn này chỉ có hiệu lực trong vòng 24h', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('678', 'you', 'bạn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('679', 'Email or Password incorrect', 'Vui lòng nhập đúng mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('680', 'Permanent residence', 'Hộ khẩu thường trú', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('681', 'Như trên', 'Như trên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('682', 'As above', 'Như trên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('683', 'Display by date', 'Hiển thị theo ngày', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('684', 'Display by view', 'Hiển thị theo lượt xem', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('685', 'Renew Job', 'Làm mới công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('686', 'Income / hour', 'Mức thu nhập / giờ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('687', 'Display type', 'Kiểu hiển thị', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('688', 'Enter keyword', 'Vui lòng nhập từ khóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('689', 'Please select the records to be deleted', 'Vui lòng chọn bản ghi cần xóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('690', 'No time limit', 'Không giới hạn thời gian', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('691', 'times', 'lượt', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('692', 'Enter your school', 'Vui lòng nhập trường của bạn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('693', 'Enter your major', 'Vui lòng nhập chuyên ngành của bạn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('694', 'You will be deducted one view from your services package to view detail information of this resume. Do you want to continue?', 'Bạn sẽ bị trừ một lượt xem vào gói dịch vụ đã đăng ký. Bạn có muốn tiếp tục?', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('695', 'You are not registered service packs can view candidate information. You do not want to sign up?', 'Bạn chưa mua gói dịch vụ nào có thể xem thông tin ứng viên. Bạn có muốn mua ngay?', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('696', 'View detail', 'Xem chi tiết', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('697', 'Expired', 'Hết hạn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('698', 'Business Information', 'Thông tin doanh nghiệp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('699', 'Sent date', 'Ngày gửi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('700', 'Feedback from candidate', 'Phản hồi từ ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('701', 'Please select feedback(s) you want to delete', 'Vui lòng chọn phản hồi mà bạn muốn xóa', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('702', 'Feedback Of Employer', 'Phản hồi của nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('703', 'You have to be an employer to access this page', 'Bạn phải là nhà tuyển dụng để truy cập trang này', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('704', 'You have to be an employer to access that page', 'Để truy cập chức năng đó bạn phải đăng nhập là nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('705', 'You are not logged in with employer account. Do you want to sign-out this account and login with another employer account?', 'Tài khoản của bạn không phải là nhà tuyển dụng. Bạn có muốn thoát và đăng nhập lại với tài khoản nhà tuyển dụng không?', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('706', 'Work', 'Công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('707', 'You are not getting any job alert', 'Bạn chưa có thông báo việc làm nào được thiết lập', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('708', 'Get notified jobs matching your search criteria', 'Gửi thông báo việc làm theo thiết lập của bạn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('709', 'You are logged in as a {person}. Do you want to log out and log back in to account the candidate?', 'Bạn đang đăng nhập với tư cách là {person}. Bạn có muốn đăng xuất và đăng nhập lại với tài khoản ứng viên không?', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('710', 'Use service packages to save up to 25%', 'Chọn gói đăng tuyển và tiết kiệm đến 25%', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('711', 'Please select 2,3,4 job postings for package to receive more incentives from j4s.vn', 'Hãy chọn 2,3,4 đăng tuyển để nhận được nhiều ưu đãi từ j4s.vn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('712', 'Select service package', 'Chọn gói dịch vụ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('713', 'Extra service', 'Dịch vụ công thêm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('714', 'Confirm order and Payment', 'Xác nhận đơn hàng và thanh toán', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('715', 'Number of post', 'Số lượng đăng tuyển', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('716', 'Online Payment by Credit Card', 'Thanh toán trực tuyến bằng thẻ tín dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('717', 'Online payment by ATM', 'Thanh toán trực tuyến bằng thẻ ATM', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('718', 'Transfer payments', 'Chuyển khoản qua ngân hàng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('719', 'Payment in cash', 'Thanh toán tiền mặt', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('720', 'Old password', 'Mật khẩu cũ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('721', 'New password', 'Mật khẩu mới', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('722', 'Retype new password', 'Nhập lại mật khẩu mới', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('723', 'Please enter verify code', 'Vui lòng nhập mã bảo vệ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('724', 'Verify code is incorrect', 'Mã bảo vệ không đúng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('725', 'Number of employer  viewed resume', 'Nhà tuyển dụng đã xem hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('726', 'Today', 'Hôm nay', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('727', 'Find us on facebook', 'Tìm chúng tôi trên facebook', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('728', 'Activity alert', 'Thông báo hoạt động', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('729', 'You will be deducted one renew time from your services package to renew your job. Do you want to continue?', 'Bạn sẽ bị trừ một lần làm mới công việc vào gói dịch vụ. Bạn đồng ý?', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('730', 'You are not registered service packs can renew job. You do not want to sign up?', 'Bạn chưa có gọi dịch vụ nào cho phép làm mới tin. Bạn có muốn đăng ký?', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('731', 'You or another person has just logged in J4S with your account and renew the job \"{link}{job_name}{/link}\"', 'Bạn hoặc ai đó đã đăng nhập vào J4S và làm mới công viêc \"{link}{job_name}{/link}\"', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('732', 'Thank you for using our services', 'Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('733', 'You have just received an review from a employer \"{employer_name}\" with title is \"{review_title}\". Please {link}login{/link} to your account to view it.', 'Bạn vừa nhận được đánh giá từ nhà tuyển dụng \"{employer_name}\" với tiêu đề \"{review_title}\". Hãy {link}đăng nhập{/link} vào tài khoản của bạn để xem nội dung chi tiết', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('734', 'Please login to use this function. Do you want to login now?', 'Vui lòng đăng nhập để thực hiện chức năng này. Bạn có muốn đăng nhập ngay', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('735', 'We send to you notification of resumes matching your request. Click on a resume title to view detail', 'Chúng tôi gửi tới bạn thông báo hồ sơ theo yêu cầu. Bạn có thể click vào từng hồ sơ để xem chi tiết.', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('736', 'Alert Resume', 'Thông báo hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('737', 'Photo / Scan Identity', 'Ảnh / Scan CMTND', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('738', 'Password strength', 'Độ mạnh mật khẩu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('739', 'Weak', 'Yếu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('740', 'Too short', 'Quá ngắn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('741', 'Good', 'Tốt', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('742', 'Strong', 'Mạnh', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('743', 'Latest', 'Mới nhất', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('744', 'Sort by', 'Sắp xếp theo', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('745', 'Most Viewed', 'Xem nhiều nhất', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('746', 'Name of contact and general information', 'Họ tên người liên hệ và thông tin chung', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('747', 'Business Field', 'Ngành nghề kinh doanh', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('748', 'We send to you notification of activities on your J4S account', 'Chúng tôi xin gửi tới bạn thông báo về các hoạt động tài khoản của bạn trên J4S', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('749', 'You or another person has just updated your account informations. If its not you please check', 'Bạn hay ai đó vừa cập nhật lại thông tin tài khoản của bạn. Nếu không phải là bạn vui lòng hãy kiểm tra việc lại.', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('750', 'Job management', 'Quản lý công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('751', 'Recruited date', 'Ngày tuyển', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('752', 'Applied resumes', 'Hồ sơ đã ứng tuyển', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('753', 'Save and Add Education', 'Lưu và thêm trình độ chuyên môn', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('754', 'Preview', 'Xem trước', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('755', 'For example: Name of job - work place - other information', 'Ví dụ: Tên công việc - địa điểm - thông tin khác', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('756', 'Salary per month', 'Lương theo tháng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('757', 'Salary per hour', 'Lương theo giờ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('758', 'Select language', 'Chọn ngôn ngữ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('759', 'Salary (VND)', 'Mức lương (VND)', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('760', 'Company information', 'Thông tin công ty', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('761', 'Job title / Position', 'Chức danh / vị trí công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('762', 'No links found', 'Không tìm thấy đường dẫn này', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('763', 'You can visit the homepage or use the search box on the right', 'Bạn có thể truy cập vào trang chủ hoặc sử dụng ô bên phải để tìm kiếm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('764', 'Salary / month', 'Lương / tháng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('765', 'Salary / hour', 'Lương / giờ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('766', 'Date Applyed', 'Ngày nộp', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('767', 'Has been received', 'Đã được nhận', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('768', 'Viewed', 'Đã xem', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('769', 'Invite confirmed', 'Mời xác nhận', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('770', 'Member {user} has appled to {job}. Detail resumes as follow', 'Thành viên {user} đã nộp hồ sơ xin việc vào {job}. Chi tiết hồ sơ như sau', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('771', 'Send mail invite success', 'Gửi mail mời xác nhận thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('772', 'Accept resume', 'Nhận ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('773', 'Search candidate', 'Tìm kiếm ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('774', 'Buy package service', 'Mua gói dịch vụ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('775', 'Create job free', 'Đăng việc làm miễn phí', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('776', 'Company Infomation', 'Thông tin công ty', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('777', 'Upload file size should not exceed 1MB', 'Kích thước file ảnh tải lên không được quá 1MB', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('778', 'Save resume successfully', 'Lưu hồ sơ thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('779', 'Employer infomation', 'Thông tin nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('780', 'The number of job has recruited over j4s', 'Số lượng công việc đã tuyển được người qua j4s', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('781', 'Resumes I saved', 'Hồ sơ đã lưu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('782', 'The job was to recruit people through j4s', 'Các công việc đã tuyển được người qua j4s', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('783', 'My resume bookmark', 'Hồ sơ đã lưu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('784', 'Candidate detail', 'Thông tin ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('785', 'Show all', 'Hiển thị tất cả', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('786', 'Temporary residence address', 'Nơi đăng ký tạm trú', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('787', 'Number of resume were recruited on j4s', 'Số hồ sơ được tuyển trên J4S', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('788', 'Number of job were done over J4S', 'Công việc đã nhận qua J4S', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('789', 'Average rating', 'Điểm đánh giá trung bình', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('790', 'Do not have any job was received over J4S', 'Chưa nhận công việc nào qua J4S', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('791', 'point', 'điểm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('792', 'Score', 'Điểm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('793', 'Jobs were received over J4S', 'Công việc đã nhận qua J4S', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('794', 'Reviews list', 'Danh sách đánh giá', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('795', 'job', 'công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('796', 'Have no review', 'Chưa có đánh giá nào', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('797', 'Top employers', 'Nhà tuyển dụng hàng đầu', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('798', 'Employer detail', 'Chi tiết nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('799', 'You have to be an candidate to access that page', 'Để truy cập chức năng đó bạn phải đăng nhập là ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('800', 'Job recruited', 'Công việc đã tuyển dụng được', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('801', 'Priority Search', 'Ưu tiên tìm kiếm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('802', '{number} resume(s) is accept', '{number} hồ sơ đã được nhận', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('803', 'Successful edit information', 'Chỉnh sửa thông tin thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('804', 'Do not recruited', 'Chưa tuyển được ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('805', 'Experience', 'Kinh nghiệm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('806', 'Create job now', 'Đăng ngay', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('807', 'Update your account information successfully', 'Cập nhật thông tin tài khoản thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('808', 'Lastest review to employer', 'Đánh giá mới nhất cho nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('809', 'Review point of employer', 'Điểm đánh giá nhà tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('810', 'Have no job', 'Chưa nhận công việc nào', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('811', 'Bookmark resume', 'Lưu hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('812', 'Maximum is {number}', 'Tối đa {number}', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('813', 'Does not have job', 'Không có việc nào', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('814', 'Browsing resumes', 'Đang duyệt hồ sơ', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('815', 'Recruitment status', 'Tình trạng tuyển dụng', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('816', 'Recruiting', 'Đang đăng tuyển', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('817', 'Assigning job', 'Đang giao việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('818', 'Search by job category', 'Tìm kiếm theo ngành nghề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('819', 'Do no received', 'Đang duyệt', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('820', 'Review this candidate', 'Đánh giá ứng viên', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('821', 'I am looking for', 'Tìm việc làm', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('822', 'Your resumes updated successfully', 'Cập nhật thông tin hồ sơ thành công', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('823', 'You do not have permisssion to view content', 'Bạn không có quyền để xem nội dung này', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('824', 'Enter email to get job', 'Nhập email, nhận công việc', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('825', 'register', 'Đăng ký', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('826', 'Search for jobs by category', 'Tìm kiếm công việc theo ngành nghề', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('827', 'Only files with the following formats are allowed to use: jpg, gif, png, jpeg', 'Chỉ có các file với các định dạng sau được phép sử dụng: jpg, gif, png, jpeg', '0', '0');
INSERT INTO `pk_lang_vn` VALUES ('828', 'Only files with the following formats are allowed to use: jpg, png, jpeg', 'Chỉ có các file với các định dạng sau được phép sử dụng: jpg, png, jpeg', '0', '0');

-- ----------------------------
-- Table structure for pk_log
-- ----------------------------
DROP TABLE IF EXISTS `pk_log`;
CREATE TABLE `pk_log` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `log_time` datetime DEFAULT NULL,
  `action_type` tinyint(2) NOT NULL,
  `action_info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action_controller` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action_model` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` smallint(5) unsigned DEFAULT NULL,
  `remote_addr` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`log_id`),
  KEY `FK_adminhtml_admin` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2474 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of pk_log
-- ----------------------------
INSERT INTO `pk_log` VALUES ('1', '2014-07-16 14:33:50', '3', 'Cập nhật ID#5', 'admincp/news/update', 'News', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('2', '2014-07-17 09:06:31', '3', 'Cập nhật ID#3', 'admincp/recruit/update', 'Recruit', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('3', '2014-07-17 09:08:51', '3', 'Cập nhật ID#2', 'admincp/recruit/update', 'Recruit', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('4', '2014-07-17 09:20:59', '3', 'Cập nhật ID#74', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('5', '2014-07-17 09:21:05', '3', 'Cập nhật ID#73', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('6', '2014-07-17 09:21:20', '3', 'Cập nhật ID#21', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('7', '2014-07-17 09:25:03', '3', 'Cập nhật ID#2', 'admincp/recruit/update', 'Recruit', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('8', '2014-07-17 09:40:45', '2', 'Thêm mới ID#138', 'admincp/node/create', 'AdmincpNode', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('9', '2014-07-17 09:41:09', '3', 'Cập nhật ID#5', 'admincp/news/update', 'News', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('11', '2014-07-17 09:45:01', '3', 'Cập nhật ID#5', 'admincp/news/update', 'News', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('12', '2014-07-17 09:45:35', '4', 'Xóa dữ liệu ID#4', 'admincp/newsletter/delete', 'Newsletter', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('13', '2014-07-17 09:45:37', '4', 'Xóa dữ liệu ID#3', 'admincp/newsletter/delete', 'Newsletter', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('14', '2014-07-17 09:47:03', '3', 'Cập nhật ID#5', 'admincp/news/update', 'News', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('15', '2014-07-17 09:49:21', '3', 'Cập nhật ID#6', 'admincp/news/update', 'News', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('16', '2014-07-17 09:52:53', '3', 'Cập nhật ID#2', 'admincp/news/update', 'News', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('17', '2014-07-17 11:00:21', '4', 'Xóa dữ liệu ID#2950209', 'admincp/user/delete', 'User', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('18', '2014-07-17 11:25:51', '2', 'Thêm mới ID#7', 'admincp/news/create', 'News', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('19', '2014-07-17 12:02:15', '4', 'Xóa dữ liệu ID#5', 'admincp/faqs/deleteSelected', 'Faqs', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('20', '2014-07-17 12:02:15', '4', 'Xóa dữ liệu ID#6', 'admincp/faqs/deleteSelected', 'Faqs', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('21', '2014-07-17 12:02:15', '4', 'Xóa dữ liệu ID#7', 'admincp/faqs/deleteSelected', 'Faqs', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('22', '2014-07-17 12:08:00', '3', 'Cập nhật ID#15', 'admincp/faqs/update', 'Faqs', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('23', '2014-07-17 12:08:16', '3', 'Cập nhật ID#15', 'admincp/faqs/update', 'Faqs', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('24', '2014-07-17 12:09:57', '3', 'Cập nhật ID#15', 'admincp/faqs/update', 'Faqs', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('25', '2014-07-17 15:21:36', '2', 'Thêm mới ID#26', 'admincp/ads/create', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('26', '2014-07-17 15:22:03', '2', 'Thêm mới ID#27', 'admincp/ads/create', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('27', '2014-07-17 15:22:22', '2', 'Thêm mới ID#28', 'admincp/ads/create', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('28', '2014-07-17 15:23:23', '3', 'Cập nhật ID#26', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('29', '2014-07-17 15:23:52', '3', 'Cập nhật ID#26', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('30', '2014-07-17 15:25:45', '3', 'Cập nhật ID#19', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('31', '2014-07-17 15:25:55', '3', 'Cập nhật ID#20', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('32', '2014-07-17 15:33:47', '3', 'Cập nhật ID#26', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('33', '2014-07-17 15:34:35', '3', 'Cập nhật ID#27', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('34', '2014-07-17 15:34:49', '3', 'Cập nhật ID#28', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('35', '2014-07-17 15:38:49', '2', 'Thêm mới ID#29', 'admincp/ads/create', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('36', '2014-07-17 15:40:26', '3', 'Cập nhật ID#27', 'admincp/ads/update', 'Ads', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('37', '2014-07-17 15:40:35', '2', 'Thêm mới ID#30', 'admincp/ads/create', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('38', '2014-07-17 15:41:40', '3', 'Cập nhật ID#27', 'admincp/ads/update', 'Ads', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('39', '2014-07-18 08:27:01', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('40', '2014-07-18 08:27:43', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('41', '2014-07-18 08:28:12', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('42', '2014-07-18 08:34:50', '2', 'Thêm mới ID#151', 'admincp/settings/create', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('43', '2014-07-18 08:35:23', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('44', '2014-07-18 08:46:17', '3', 'Cập nhật ID#148', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('45', '2014-07-18 08:46:44', '3', 'Cập nhật ID#148', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('46', '2014-07-18 08:47:20', '2', 'Thêm mới ID#152', 'admincp/settings/create', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('47', '2014-07-18 09:39:38', '4', 'Xóa dữ liệu ID#1854936', 'admincp/user/delete', 'User', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('48', '2014-07-21 14:41:23', '3', 'Cập nhật ID#29', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('49', '2014-07-21 14:44:50', '3', 'Cập nhật ID#30', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('50', '2014-07-21 15:08:38', '3', 'Cập nhật ID#142', 'admincp/settings/update', 'Settings', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('51', '2014-07-21 15:08:54', '3', 'Cập nhật ID#142', 'admincp/settings/update', 'Settings', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('52', '2014-07-21 15:18:55', '3', 'Cập nhật ID#1', 'admincp/customers/update', 'Customers', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('53', '2014-07-21 15:19:51', '3', 'Cập nhật ID#1', 'admincp/customers/update', 'Customers', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('54', '2014-07-21 15:28:37', '3', 'Cập nhật ID#1', 'admincp/customers/update', 'Customers', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('55', '2014-07-21 15:28:44', '3', 'Cập nhật ID#1', 'admincp/customers/update', 'Customers', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('56', '2014-07-21 15:29:02', '3', 'Cập nhật ID#1', 'admincp/customers/update', 'Customers', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('57', '2014-07-21 15:29:43', '3', 'Cập nhật ID#2', 'admincp/customers/update', 'Customers', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('58', '2014-07-21 15:46:57', '4', 'Xóa dữ liệu ID#44', 'admincp/node/delete', 'AdmincpNode', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('59', '2014-07-21 15:49:47', '3', 'Cập nhật ID#4', 'admincp/contact/update', 'Contact', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('60', '2014-07-21 15:50:15', '2', 'Thêm mới ID#7', 'admincp/role/create', 'AdmincpRole', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('61', '2014-07-21 15:50:22', '3', 'Cập nhật ID#7', 'admincp/role/update', 'AdmincpRole', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('62', '2014-07-21 15:50:30', '3', 'Cập nhật ID#7', 'admincp/role/update', 'AdmincpRole', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('63', '2014-07-21 15:57:15', '2', 'Thêm mới ID#557', 'admincp/package/create', 'Package', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('64', '2014-07-21 15:59:04', '3', 'Cập nhật ID#557', 'admincp/package/update', 'Package', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('65', '2014-07-21 16:00:50', '3', 'Cập nhật ID#557', 'admincp/package/update', 'Package', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('66', '2014-07-21 16:00:57', '3', 'Cập nhật ID#557', 'admincp/package/update', 'Package', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('67', '2014-07-21 16:05:22', '3', 'Cập nhật ID#557', 'admincp/package/update', 'Package', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('68', '2014-07-21 16:18:07', '3', 'Cập nhật ID#12', 'admincp/agents/update', 'Agents', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('69', '2014-07-21 16:18:13', '3', 'Cập nhật ID#12', 'admincp/agents/update', 'Agents', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('70', '2014-07-21 16:38:45', '3', 'Cập nhật ID#1', 'admincp/package/update', 'Package', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('71', '2014-07-21 16:42:36', '3', 'Cập nhật ID#557', 'admincp/package/update', 'Package', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('72', '2014-07-21 16:54:25', '3', 'Cập nhật ID#557', 'admincp/package/update', 'Package', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('73', '2014-07-21 16:54:31', '3', 'Cập nhật ID#557', 'admincp/package/update', 'Package', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('74', '2014-07-21 17:45:01', '2', 'Thêm mới ID#31', 'admincp/ads/create', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('75', '2014-07-21 17:47:55', '3', 'Cập nhật ID#31', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('76', '2014-07-21 17:48:21', '3', 'Cập nhật ID#31', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('77', '2014-07-21 17:49:04', '3', 'Cập nhật ID#31', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('78', '2014-07-22 08:28:49', '2', 'Thêm mới ID#32', 'admincp/ads/create', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('79', '2014-07-22 08:30:07', '3', 'Cập nhật ID#32', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('80', '2014-07-22 08:45:41', '2', 'Thêm mới ID#33', 'admincp/ads/create', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('81', '2014-07-22 08:46:02', '2', 'Thêm mới ID#34', 'admincp/ads/create', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('82', '2014-07-22 08:48:45', '3', 'Cập nhật ID#33', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('83', '2014-07-22 08:49:04', '3', 'Cập nhật ID#34', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('84', '2014-07-22 10:05:44', '3', 'Cập nhật ID#5', 'admincp/contact/update', 'Contact', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('85', '2014-07-22 10:09:34', '3', 'Cập nhật ID#5', 'admincp/contact/update', 'Contact', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('86', '2014-07-22 10:09:42', '3', 'Cập nhật ID#5', 'admincp/contact/update', 'Contact', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('87', '2014-07-22 10:09:42', '3', 'Cập nhật ID#5', 'admincp/contact/update', 'Contact', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('88', '2014-07-22 10:25:24', '3', 'Cập nhật ID#5', 'admincp/contact/update', 'Contact', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('89', '2014-07-22 10:25:35', '3', 'Cập nhật ID#5', 'admincp/contact/update', 'Contact', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('90', '2014-07-22 10:25:39', '3', 'Cập nhật ID#5', 'admincp/contact/update', 'Contact', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('91', '2014-07-22 10:25:39', '3', 'Cập nhật ID#5', 'admincp/contact/update', 'Contact', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('92', '2014-07-22 10:25:41', '3', 'Cập nhật ID#5', 'admincp/contact/update', 'Contact', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('93', '2014-07-22 10:45:53', '2', 'Thêm mới ID#139', 'admincp/node/create', 'AdmincpNode', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('94', '2014-07-22 10:46:12', '4', 'Xóa dữ liệu ID#2', 'admincp/recruitcv/delete', 'Recruitcv', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('95', '2014-07-22 10:46:14', '4', 'Xóa dữ liệu ID#1', 'admincp/recruitcv/delete', 'Recruitcv', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('96', '2014-07-22 10:57:13', '3', 'Cập nhật ID#147', 'admincp/settings/update', 'Settings', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('97', '2014-07-22 11:09:16', '2', 'Thêm mới ID#1872453', 'admincp/user/create', 'User', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('98', '2014-07-22 11:12:42', '2', 'Thêm mới ID#8', 'admincp/news/create', 'News', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('99', '2014-07-22 11:12:45', '4', 'Xóa dữ liệu ID#8', 'admincp/news/deleteSelected', 'News', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('100', '2014-07-22 11:13:34', '3', 'Cập nhật ID#7', 'admincp/news/update', 'News', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('101', '2014-07-22 11:14:29', '3', 'Cập nhật ID#6', 'admincp/news/update', 'News', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('102', '2014-07-22 11:29:28', '2', 'Thêm mới ID#14', 'admincp/posts/create', 'Posts', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('103', '2014-07-22 11:46:20', '2', 'Thêm mới ID#15', 'admincp/posts/create', 'Posts', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('104', '2014-07-22 11:47:41', '2', 'Thêm mới ID#16', 'admincp/posts/create', 'Posts', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('105', '2014-07-22 11:47:44', '4', 'Xóa dữ liệu ID#16', 'admincp/posts/delete', 'Posts', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('106', '2014-07-22 11:51:00', '3', 'Cập nhật ID#73', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('107', '2014-07-22 11:51:33', '3', 'Cập nhật ID#74', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('108', '2014-07-22 11:51:41', '3', 'Cập nhật ID#73', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('109', '2014-07-22 11:51:54', '3', 'Cập nhật ID#69', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('110', '2014-07-22 11:52:08', '3', 'Cập nhật ID#68', 'admincp/settings/update', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('111', '2014-07-22 11:53:22', '2', 'Thêm mới ID#153', 'admincp/settings/create', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('112', '2014-07-22 11:54:16', '2', 'Thêm mới ID#154', 'admincp/settings/create', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('113', '2014-07-22 11:55:07', '2', 'Thêm mới ID#155', 'admincp/settings/create', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('114', '2014-07-22 11:55:29', '2', 'Thêm mới ID#156', 'admincp/settings/create', 'Settings', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('115', '2014-07-22 15:21:14', '3', 'Cập nhật ID#149', 'admincp/settings/update', 'Settings', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('116', '2014-07-22 17:07:50', '3', 'Cập nhật ID#20', 'admincp/settings/update', 'Settings', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('117', '2014-07-23 11:30:56', '2', 'Thêm mới ID#140', 'admincp/node/create', 'AdmincpNode', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('118', '2014-07-23 14:00:08', '3', 'Cập nhật ID#27', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('119', '2014-07-23 14:00:31', '3', 'Cập nhật ID#27', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('120', '2014-07-23 14:05:44', '3', 'Cập nhật ID#27', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('121', '2014-07-23 14:07:59', '3', 'Cập nhật ID#28', 'admincp/ads/update', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('122', '2014-07-23 14:18:37', '3', 'Cập nhật ID#27', 'admincp/ads/update', 'Ads', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('123', '2014-07-23 14:18:53', '3', 'Cập nhật ID#28', 'admincp/ads/update', 'Ads', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('124', '2014-07-23 14:20:40', '3', 'Cập nhật ID#31', 'admincp/ads/update', 'Ads', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('125', '2014-07-23 15:09:30', '2', 'Thêm mới ID#17', 'admincp/posts/create', 'Posts', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('126', '2014-07-23 15:09:35', '4', 'Xóa dữ liệu ID#17', 'admincp/posts/deleteSelected', 'Posts', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('127', '2014-07-23 15:10:10', '4', 'Xóa dữ liệu ID#3303014', 'admincp/user/deleteSelected', 'User', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('128', '2014-07-23 15:21:07', '4', 'Xóa dữ liệu ID#7', 'admincp/recruitcv/deleteSelected', 'Recruitcv', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('129', '2014-07-23 15:21:07', '4', 'Xóa dữ liệu ID#6', 'admincp/recruitcv/deleteSelected', 'Recruitcv', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('130', '2014-07-23 15:50:10', '3', 'Cập nhật ID#6', 'admincp/contact/update', 'Contact', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('131', '2014-07-23 16:01:09', '3', 'Cập nhật ID#6', 'admincp/contact/update', 'Contact', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('132', '2014-07-23 16:01:16', '3', 'Cập nhật ID#5', 'admincp/contact/update', 'Contact', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('133', '2014-07-23 16:08:45', '2', 'Thêm mới ID#35', 'admincp/ads/create', 'Ads', '1', '-1062731026', '1');
INSERT INTO `pk_log` VALUES ('134', '2014-07-23 16:50:41', '4', 'Xóa dữ liệu ID#21', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('135', '2014-07-23 16:50:41', '4', 'Xóa dữ liệu ID#22', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('136', '2014-07-23 16:50:41', '4', 'Xóa dữ liệu ID#23', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('137', '2014-07-23 16:50:41', '4', 'Xóa dữ liệu ID#24', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('138', '2014-07-23 16:50:41', '4', 'Xóa dữ liệu ID#25', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('139', '2014-07-23 16:50:41', '4', 'Xóa dữ liệu ID#27', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('140', '2014-07-23 16:50:41', '4', 'Xóa dữ liệu ID#28', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('141', '2014-07-23 16:50:41', '4', 'Xóa dữ liệu ID#29', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('142', '2014-07-23 16:50:42', '4', 'Xóa dữ liệu ID#31', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('143', '2014-07-23 16:50:42', '4', 'Xóa dữ liệu ID#30', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('144', '2014-07-23 16:50:46', '4', 'Xóa dữ liệu ID#32', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('145', '2014-07-23 16:50:46', '4', 'Xóa dữ liệu ID#33', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('146', '2014-07-23 16:50:46', '4', 'Xóa dữ liệu ID#34', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('147', '2014-07-23 16:50:46', '4', 'Xóa dữ liệu ID#35', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('148', '2014-07-23 16:50:46', '4', 'Xóa dữ liệu ID#19', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('149', '2014-07-23 16:50:46', '4', 'Xóa dữ liệu ID#20', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('150', '2014-07-23 16:50:46', '4', 'Xóa dữ liệu ID#26', 'admincp/ads/deleteSelected', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('151', '2014-07-23 16:51:45', '2', 'Thêm mới ID#36', 'admincp/ads/create', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('152', '2014-07-23 16:53:00', '2', 'Thêm mới ID#37', 'admincp/ads/create', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('153', '2014-07-23 16:53:14', '2', 'Thêm mới ID#38', 'admincp/ads/create', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('154', '2014-07-23 16:53:25', '2', 'Thêm mới ID#39', 'admincp/ads/create', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('155', '2014-07-23 16:53:40', '2', 'Thêm mới ID#40', 'admincp/ads/create', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('156', '2014-07-23 16:54:05', '2', 'Thêm mới ID#41', 'admincp/ads/create', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('157', '2014-07-23 17:04:59', '3', 'Cập nhật ID#36', 'admincp/ads/update', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('158', '2014-07-23 17:05:16', '3', 'Cập nhật ID#37', 'admincp/ads/update', 'Ads', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('159', '2014-07-23 21:34:36', '2', 'Thêm mới ID#140', 'admincp/node/create', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('160', '2014-07-23 21:44:15', '2', 'Thêm mới ID#1', 'admincp/testimonial/create', 'Testimonial', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('161', '2014-07-24 08:35:30', '3', 'Cập nhật ID#1', 'admincp/testimonial/update', 'Testimonial', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('162', '2014-07-24 08:36:05', '2', 'Thêm mới ID#2', 'admincp/testimonial/create', 'Testimonial', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('163', '2014-07-24 08:37:37', '2', 'Thêm mới ID#3', 'admincp/testimonial/create', 'Testimonial', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('164', '2014-07-24 08:38:12', '3', 'Cập nhật ID#2', 'admincp/testimonial/update', 'Testimonial', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('165', '2014-07-24 08:42:08', '3', 'Cập nhật ID#21', 'admincp/settings/update', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('166', '2014-07-24 08:42:20', '3', 'Cập nhật ID#20', 'admincp/settings/update', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('167', '2014-07-24 08:42:47', '3', 'Cập nhật ID#73', 'admincp/settings/update', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('168', '2014-07-24 08:43:01', '3', 'Cập nhật ID#74', 'admincp/settings/update', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('169', '2014-07-24 08:43:18', '4', 'Xóa dữ liệu ID#146', 'admincp/settings/deleteSelected', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('170', '2014-07-24 08:43:18', '4', 'Xóa dữ liệu ID#145', 'admincp/settings/deleteSelected', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('171', '2014-07-24 08:43:19', '4', 'Xóa dữ liệu ID#144', 'admincp/settings/deleteSelected', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('172', '2014-07-24 08:43:37', '3', 'Cập nhật ID#153', 'admincp/settings/update', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('173', '2014-07-24 08:43:42', '3', 'Cập nhật ID#154', 'admincp/settings/update', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('174', '2014-07-24 08:43:48', '3', 'Cập nhật ID#155', 'admincp/settings/update', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('175', '2014-07-24 08:44:13', '4', 'Xóa dữ liệu ID#150', 'admincp/settings/deleteSelected', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('176', '2014-07-24 08:44:14', '4', 'Xóa dữ liệu ID#149', 'admincp/settings/deleteSelected', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('177', '2014-07-24 08:44:31', '2', 'Thêm mới ID#157', 'admincp/settings/create', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('178', '2014-07-24 08:44:38', '3', 'Cập nhật ID#157', 'admincp/settings/update', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('179', '2014-07-24 08:55:26', '3', 'Cập nhật ID#1', 'admincp/testimonial/update', 'Testimonial', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('180', '2014-07-24 10:14:17', '3', 'Cập nhật ID#1', 'admincp/testimonial/update', 'Testimonial', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('181', '2014-07-24 10:15:32', '3', 'Cập nhật ID#1', 'admincp/testimonial/update', 'Testimonial', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('182', '2014-07-24 10:30:07', '3', 'Cập nhật ID#1', 'admincp/testimonial/update', 'Testimonial', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('183', '2014-07-24 10:32:19', '3', 'Cập nhật ID#1', 'admincp/testimonial/update', 'Testimonial', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('184', '2014-07-24 11:55:37', '3', 'Cập nhật ID#2', 'admincp/testimonial/update', 'Testimonial', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('185', '2014-07-24 11:55:49', '3', 'Cập nhật ID#3', 'admincp/testimonial/update', 'Testimonial', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('186', '2014-07-24 11:56:30', '3', 'Cập nhật ID#1', 'admincp/testimonial/update', 'Testimonial', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('187', '2014-07-24 14:41:21', '2', 'Thêm mới ID#141', 'admincp/node/create', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('188', '2014-07-24 15:57:46', '2', 'Thêm mới ID#1', 'admincp/services/create', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('189', '2014-07-24 15:58:12', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('190', '2014-07-24 15:58:37', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('191', '2014-07-24 15:59:08', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('192', '2014-07-24 16:00:10', '2', 'Thêm mới ID#1', 'admincp/services/create', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('193', '2014-07-24 16:23:57', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('194', '2014-07-24 16:28:21', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('195', '2014-07-24 16:32:33', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('196', '2014-07-24 16:46:29', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('197', '2014-07-24 16:46:54', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('198', '2014-07-24 16:58:39', '2', 'Thêm mới ID#2', 'admincp/services/create', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('199', '2014-07-24 16:59:32', '3', 'Cập nhật ID#2', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('200', '2014-07-24 17:04:16', '2', 'Thêm mới ID#3', 'admincp/services/create', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('201', '2014-07-24 20:10:20', '2', 'Thêm mới ID#4', 'admincp/services/create', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('202', '2014-07-24 20:11:27', '3', 'Cập nhật ID#4', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('203', '2014-07-24 22:12:32', '2', 'Thêm mới ID#158', 'admincp/settings/create', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('204', '2014-07-24 22:27:10', '2', 'Thêm mới ID#159', 'admincp/settings/create', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('205', '2014-07-24 22:27:31', '3', 'Cập nhật ID#159', 'admincp/settings/update', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('206', '2014-07-25 13:15:34', '4', 'Xóa dữ liệu ID#123', 'admincp/node/delete', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('207', '2014-07-25 13:15:54', '4', 'Xóa dữ liệu ID#138', 'admincp/node/delete', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('208', '2014-07-25 13:17:32', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('209', '2014-07-25 13:18:20', '3', 'Cập nhật ID#3', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('210', '2014-07-25 13:18:47', '3', 'Cập nhật ID#2', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('211', '2014-07-25 13:19:24', '3', 'Cập nhật ID#4', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('212', '2014-07-25 13:27:13', '2', 'Thêm mới ID#142', 'admincp/node/create', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('213', '2014-07-25 13:30:05', '3', 'Cập nhật ID#1', 'admincp/language/update', 'Language', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('214', '2014-07-25 13:34:30', '3', 'Cập nhật ID#2', 'admincp/language/update', 'Language', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('215', '2014-07-25 13:34:43', '3', 'Cập nhật ID#1', 'admincp/language/update', 'Language', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('216', '2014-07-25 13:43:47', '3', 'Cập nhật ID#2', 'admincp/language/update', 'Language', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('217', '2014-07-25 13:44:02', '3', 'Cập nhật ID#1', 'admincp/language/update', 'Language', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('218', '2014-07-26 09:46:36', '2', 'Thêm mới ID#5', 'admincp/services/create', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('219', '2014-07-26 09:47:32', '2', 'Thêm mới ID#6', 'admincp/services/create', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('220', '2014-07-26 09:48:03', '2', 'Thêm mới ID#7', 'admincp/services/create', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('221', '2014-07-26 09:49:08', '2', 'Thêm mới ID#8', 'admincp/services/create', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('222', '2014-07-26 09:56:55', '2', 'Thêm mới ID#9', 'admincp/services/create', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('223', '2014-07-26 10:32:13', '2', 'Thêm mới ID#10', 'admincp/services/create', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('224', '2014-07-26 10:33:27', '2', 'Thêm mới ID#11', 'admincp/services/create', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('225', '2014-07-26 10:35:48', '2', 'Thêm mới ID#160', 'admincp/settings/create', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('226', '2014-07-26 11:05:34', '3', 'Cập nhật ID#11', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('227', '2014-07-26 19:26:03', '2', 'Thêm mới ID#143', 'admincp/node/create', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('228', '2014-07-26 19:36:59', '2', 'Thêm mới ID#1', 'admincp/about/create', 'About', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('229', '2014-07-26 21:04:57', '3', 'Cập nhật ID#1', 'admincp/about/update', 'About', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('230', '2014-07-26 21:08:44', '2', 'Thêm mới ID#2', 'admincp/about/create', 'About', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('231', '2014-07-26 21:09:23', '2', 'Thêm mới ID#3', 'admincp/about/create', 'About', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('232', '2014-07-26 21:10:08', '2', 'Thêm mới ID#4', 'admincp/about/create', 'About', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('233', '2014-07-26 22:01:22', '4', 'Xóa dữ liệu ID#129', 'admincp/node/delete', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('234', '2014-07-26 22:01:35', '4', 'Xóa dữ liệu ID#136', 'admincp/node/delete', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('235', '2014-07-26 22:01:39', '4', 'Xóa dữ liệu ID#131', 'admincp/node/delete', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('236', '2014-07-26 22:01:42', '4', 'Xóa dữ liệu ID#130', 'admincp/node/delete', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('237', '2014-07-26 22:01:45', '4', 'Xóa dữ liệu ID#128', 'admincp/node/delete', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('238', '2014-07-26 22:01:48', '4', 'Xóa dữ liệu ID#127', 'admincp/node/delete', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('239', '2014-07-26 22:01:50', '4', 'Xóa dữ liệu ID#126', 'admincp/node/delete', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('240', '2014-07-26 22:02:06', '3', 'Cập nhật ID#141', 'admincp/node/update', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('241', '2014-07-26 22:15:36', '2', 'Thêm mới ID#144', 'admincp/node/create', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('242', '2014-07-27 07:18:18', '2', 'Thêm mới ID#161', 'admincp/settings/create', 'Settings', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('243', '2014-07-27 07:26:22', '2', 'Thêm mới ID#1', 'admincp/staff/create', 'Staff', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('244', '2014-07-27 07:29:56', '3', 'Cập nhật ID#1', 'admincp/staff/update', 'Staff', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('245', '2014-07-27 09:48:23', '3', 'Cập nhật ID#5', 'admincp/services/update', 'Services', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('246', '2014-07-27 15:55:00', '2', 'Thêm mới ID#145', 'admincp/node/create', 'AdmincpNode', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('247', '2014-07-27 16:04:28', '2', 'Thêm mới ID#1', 'admincp/category/create', 'Category', '1', '0', '1');
INSERT INTO `pk_log` VALUES ('248', '2014-07-28 09:22:23', '4', 'Xóa dữ liệu ID#23', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('249', '2014-07-28 09:22:23', '4', 'Xóa dữ liệu ID#22', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('250', '2014-07-28 09:22:23', '4', 'Xóa dữ liệu ID#21', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('251', '2014-07-28 09:22:23', '4', 'Xóa dữ liệu ID#20', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('252', '2014-07-28 09:22:23', '4', 'Xóa dữ liệu ID#19', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('253', '2014-07-28 09:22:23', '4', 'Xóa dữ liệu ID#18', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('254', '2014-07-28 09:22:23', '4', 'Xóa dữ liệu ID#17', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('255', '2014-07-28 09:22:23', '4', 'Xóa dữ liệu ID#16', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('256', '2014-07-28 09:22:23', '4', 'Xóa dữ liệu ID#15', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('257', '2014-07-28 09:22:23', '4', 'Xóa dữ liệu ID#14', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('258', '2014-07-28 09:22:26', '4', 'Xóa dữ liệu ID#13', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('259', '2014-07-28 09:22:26', '4', 'Xóa dữ liệu ID#12', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('260', '2014-07-28 09:22:26', '4', 'Xóa dữ liệu ID#11', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('261', '2014-07-28 09:22:26', '4', 'Xóa dữ liệu ID#10', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('262', '2014-07-28 09:22:26', '4', 'Xóa dữ liệu ID#9', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('263', '2014-07-28 09:22:26', '4', 'Xóa dữ liệu ID#8', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('264', '2014-07-28 09:22:26', '4', 'Xóa dữ liệu ID#7', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('265', '2014-07-28 09:22:26', '4', 'Xóa dữ liệu ID#6', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('266', '2014-07-28 09:22:26', '4', 'Xóa dữ liệu ID#5', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('267', '2014-07-28 09:22:26', '4', 'Xóa dữ liệu ID#4', 'admincp/contact/deleteSelected', 'Contact', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('268', '2014-07-28 11:44:28', '2', 'Thêm mới ID#162', 'admincp/settings/create', 'Settings', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('269', '2014-07-28 11:53:02', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('270', '2014-07-28 11:53:49', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('271', '2014-07-28 11:54:41', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('272', '2014-07-28 14:00:47', '3', 'Cập nhật ID#11', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('273', '2014-07-28 14:05:30', '3', 'Cập nhật ID#10', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('274', '2014-07-28 14:06:27', '3', 'Cập nhật ID#9', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('275', '2014-07-28 14:07:53', '3', 'Cập nhật ID#8', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('276', '2014-07-28 14:08:18', '3', 'Cập nhật ID#7', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('277', '2014-07-29 08:29:24', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('278', '2014-07-29 08:29:39', '3', 'Cập nhật ID#1', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('279', '2014-07-29 08:30:08', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('280', '2014-07-29 08:30:29', '3', 'Cập nhật ID#5', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('281', '2014-07-29 08:31:17', '3', 'Cập nhật ID#5', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('282', '2014-07-29 08:31:42', '3', 'Cập nhật ID#1', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('283', '2014-07-29 08:31:48', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('284', '2014-07-29 08:31:53', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('285', '2014-07-29 08:31:59', '3', 'Cập nhật ID#5', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('286', '2014-07-29 08:53:08', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('287', '2014-07-29 08:53:19', '3', 'Cập nhật ID#11', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('288', '2014-07-29 08:53:41', '3', 'Cập nhật ID#12', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('289', '2014-07-29 08:54:07', '3', 'Cập nhật ID#13', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('290', '2014-07-29 08:54:42', '3', 'Cập nhật ID#11', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('291', '2014-07-29 08:54:58', '3', 'Cập nhật ID#11', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('292', '2014-07-29 08:57:33', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('293', '2014-07-29 08:58:19', '3', 'Cập nhật ID#5', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('294', '2014-07-29 09:08:16', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('295', '2014-07-29 10:31:05', '2', 'Thêm mới ID#12', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('296', '2014-07-29 10:35:05', '3', 'Cập nhật ID#4', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('297', '2014-07-29 10:35:59', '3', 'Cập nhật ID#3', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('298', '2014-07-29 10:37:02', '3', 'Cập nhật ID#4', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('299', '2014-07-29 10:37:25', '3', 'Cập nhật ID#3', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('300', '2014-07-29 10:38:56', '3', 'Cập nhật ID#2', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('301', '2014-07-29 10:40:45', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('302', '2014-07-29 10:42:46', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('303', '2014-07-29 10:44:39', '3', 'Cập nhật ID#12', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('304', '2014-07-29 10:45:12', '3', 'Cập nhật ID#4', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('305', '2014-07-29 10:45:22', '3', 'Cập nhật ID#12', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('306', '2014-07-29 10:45:44', '3', 'Cập nhật ID#4', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('307', '2014-07-29 10:47:58', '2', 'Thêm mới ID#13', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('308', '2014-07-29 10:50:42', '3', 'Cập nhật ID#13', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('309', '2014-07-29 10:51:53', '3', 'Cập nhật ID#4', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('310', '2014-07-29 10:52:28', '3', 'Cập nhật ID#4', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('311', '2014-07-29 11:04:40', '3', 'Cập nhật ID#12', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('312', '2014-07-29 11:07:26', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('313', '2014-07-29 11:12:49', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '-1062731044', '1');
INSERT INTO `pk_log` VALUES ('314', '2014-07-29 11:13:14', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '-1062731044', '1');
INSERT INTO `pk_log` VALUES ('315', '2014-07-29 11:31:40', '3', 'Cập nhật ID#13', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('316', '2014-07-29 11:31:50', '3', 'Cập nhật ID#13', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('317', '2014-07-29 11:32:30', '3', 'Cập nhật ID#13', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('318', '2014-07-29 11:37:51', '3', 'Cập nhật ID#13', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('319', '2014-07-29 11:46:12', '3', 'Cập nhật ID#13', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('320', '2014-07-29 11:48:13', '3', 'Cập nhật ID#12', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('321', '2014-07-29 11:48:36', '3', 'Cập nhật ID#12', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('322', '2014-07-29 11:51:11', '3', 'Cập nhật ID#4', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('323', '2014-07-29 11:55:43', '3', 'Cập nhật ID#4', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('324', '2014-07-29 11:56:38', '3', 'Cập nhật ID#4', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('325', '2014-07-29 11:59:54', '3', 'Cập nhật ID#3', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('326', '2014-07-29 12:04:14', '3', 'Cập nhật ID#2', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('327', '2014-07-29 12:04:45', '3', 'Cập nhật ID#2', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('328', '2014-07-29 12:08:19', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('329', '2014-07-29 12:09:09', '3', 'Cập nhật ID#1', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('330', '2014-07-29 13:04:23', '2', 'Thêm mới ID#4', 'admincp/testimonial/create', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('331', '2014-07-29 13:06:03', '3', 'Cập nhật ID#4', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('332', '2014-07-29 14:10:09', '3', 'Cập nhật ID#4', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('333', '2014-07-29 14:13:25', '2', 'Thêm mới ID#5', 'admincp/testimonial/create', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('334', '2014-07-29 14:13:39', '3', 'Cập nhật ID#3', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('335', '2014-07-29 14:13:49', '3', 'Cập nhật ID#5', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('336', '2014-07-29 14:16:50', '3', 'Cập nhật ID#5', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('337', '2014-07-29 14:17:13', '3', 'Cập nhật ID#5', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('338', '2014-07-29 14:17:24', '3', 'Cập nhật ID#5', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('339', '2014-07-29 14:18:17', '3', 'Cập nhật ID#1', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('340', '2014-07-29 14:18:26', '3', 'Cập nhật ID#2', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('341', '2014-07-29 14:49:06', '3', 'Cập nhật ID#10', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('342', '2014-07-29 14:50:16', '3', 'Cập nhật ID#159', 'admincp/settings/update', 'Settings', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('343', '2014-07-29 14:50:42', '3', 'Cập nhật ID#159', 'admincp/settings/update', 'Settings', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('344', '2014-07-29 14:54:36', '3', 'Cập nhật ID#159', 'admincp/settings/update', 'Settings', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('345', '2014-07-29 14:56:05', '3', 'Cập nhật ID#159', 'admincp/settings/update', 'Settings', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('346', '2014-07-29 14:56:44', '3', 'Cập nhật ID#151', 'admincp/settings/update', 'Settings', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('347', '2014-07-29 15:21:57', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('348', '2014-07-29 15:22:08', '3', 'Cập nhật ID#7', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('349', '2014-07-29 15:29:46', '3', 'Cập nhật ID#7', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('350', '2014-07-29 15:29:57', '3', 'Cập nhật ID#8', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('351', '2014-07-29 15:32:16', '2', 'Thêm mới ID#14', 'admincp/category/create', 'Category', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('352', '2014-07-29 15:32:32', '2', 'Thêm mới ID#15', 'admincp/category/create', 'Category', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('353', '2014-07-29 15:33:08', '2', 'Thêm mới ID#16', 'admincp/category/create', 'Category', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('354', '2014-07-29 15:33:23', '2', 'Thêm mới ID#17', 'admincp/category/create', 'Category', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('355', '2014-07-29 15:33:38', '2', 'Thêm mới ID#18', 'admincp/category/create', 'Category', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('356', '2014-07-29 15:36:08', '3', 'Cập nhật ID#14', 'admincp/category/update', 'Category', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('357', '2014-07-29 15:37:49', '3', 'Cập nhật ID#11', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('358', '2014-07-29 15:38:07', '3', 'Cập nhật ID#10', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('359', '2014-07-29 15:38:54', '3', 'Cập nhật ID#8', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('360', '2014-07-29 15:39:16', '3', 'Cập nhật ID#9', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('361', '2014-07-29 15:39:24', '3', 'Cập nhật ID#8', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('362', '2014-07-29 15:40:55', '3', 'Cập nhật ID#7', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('363', '2014-07-29 15:41:15', '3', 'Cập nhật ID#7', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('364', '2014-07-29 15:41:36', '3', 'Cập nhật ID#6', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('365', '2014-07-29 15:41:57', '3', 'Cập nhật ID#5', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('366', '2014-07-29 15:42:18', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('367', '2014-07-29 15:42:24', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('368', '2014-07-29 15:44:46', '2', 'Thêm mới ID#14', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('369', '2014-07-29 15:46:23', '2', 'Thêm mới ID#15', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('370', '2014-07-29 15:47:17', '2', 'Thêm mới ID#16', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('371', '2014-07-29 15:54:32', '3', 'Cập nhật ID#11', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('372', '2014-07-29 15:58:27', '3', 'Cập nhật ID#11', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('373', '2014-07-29 15:59:37', '3', 'Cập nhật ID#11', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('374', '2014-07-29 16:01:43', '3', 'Cập nhật ID#11', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('375', '2014-07-29 16:04:20', '3', 'Cập nhật ID#10', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('376', '2014-07-29 16:04:46', '3', 'Cập nhật ID#10', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('377', '2014-07-29 16:05:05', '3', 'Cập nhật ID#10', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('378', '2014-07-29 16:08:39', '3', 'Cập nhật ID#5', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('379', '2014-07-29 16:21:00', '3', 'Cập nhật ID#6', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('380', '2014-07-29 16:21:59', '3', 'Cập nhật ID#6', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('381', '2014-07-29 16:22:49', '3', 'Cập nhật ID#6', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('382', '2014-07-29 16:23:12', '3', 'Cập nhật ID#6', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('383', '2014-07-29 16:25:24', '3', 'Cập nhật ID#16', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('384', '2014-07-29 16:27:50', '3', 'Cập nhật ID#10', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('385', '2014-07-29 16:27:58', '3', 'Cập nhật ID#11', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('386', '2014-07-29 16:35:36', '3', 'Cập nhật ID#162', 'admincp/settings/update', 'Settings', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('387', '2014-07-29 16:37:07', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('388', '2014-07-29 16:38:33', '2', 'Thêm mới ID#2', 'admincp/staff/create', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('389', '2014-07-29 16:39:21', '2', 'Thêm mới ID#3', 'admincp/staff/create', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('390', '2014-07-29 16:40:18', '2', 'Thêm mới ID#4', 'admincp/staff/create', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('391', '2014-07-29 16:42:01', '2', 'Thêm mới ID#6', 'admincp/testimonial/create', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('392', '2014-07-29 16:42:12', '2', 'Thêm mới ID#163', 'admincp/settings/create', 'Settings', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('393', '2014-07-29 16:42:28', '3', 'Cập nhật ID#163', 'admincp/settings/update', 'Settings', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('394', '2014-07-29 16:42:47', '3', 'Cập nhật ID#163', 'admincp/settings/update', 'Settings', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('395', '2014-07-29 16:42:58', '3', 'Cập nhật ID#151', 'admincp/settings/update', 'Settings', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('396', '2014-07-29 16:42:59', '3', 'Cập nhật ID#2', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('397', '2014-07-29 16:43:51', '2', 'Thêm mới ID#7', 'admincp/testimonial/create', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('398', '2014-07-29 16:44:15', '3', 'Cập nhật ID#5', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('399', '2014-07-29 16:46:48', '2', 'Thêm mới ID#42', 'admincp/ads/create', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('400', '2014-07-29 16:47:07', '3', 'Cập nhật ID#42', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('401', '2014-07-29 16:49:01', '3', 'Cập nhật ID#42', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('402', '2014-07-29 16:49:43', '2', 'Thêm mới ID#43', 'admincp/ads/create', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('403', '2014-07-29 16:51:14', '3', 'Cập nhật ID#42', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('404', '2014-07-29 16:52:05', '3', 'Cập nhật ID#43', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('405', '2014-07-29 16:52:48', '2', 'Thêm mới ID#44', 'admincp/ads/create', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('406', '2014-07-29 16:53:18', '2', 'Thêm mới ID#45', 'admincp/ads/create', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('407', '2014-07-29 16:53:37', '2', 'Thêm mới ID#46', 'admincp/ads/create', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('408', '2014-07-29 16:53:53', '2', 'Thêm mới ID#47', 'admincp/ads/create', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('409', '2014-07-29 16:56:12', '3', 'Cập nhật ID#14', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('410', '2014-07-29 16:56:31', '3', 'Cập nhật ID#14', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('411', '2014-07-29 16:57:34', '3', 'Cập nhật ID#15', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('412', '2014-07-29 16:58:17', '3', 'Cập nhật ID#16', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('413', '2014-07-29 16:58:46', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('414', '2014-07-29 16:59:15', '3', 'Cập nhật ID#18', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('415', '2014-07-29 17:00:10', '2', 'Thêm mới ID#19', 'admincp/category/create', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('416', '2014-07-29 17:00:51', '2', 'Thêm mới ID#20', 'admincp/category/create', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('417', '2014-07-29 17:01:08', '3', 'Cập nhật ID#20', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('418', '2014-07-29 17:01:28', '3', 'Cập nhật ID#19', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('419', '2014-07-29 17:01:44', '2', 'Thêm mới ID#21', 'admincp/category/create', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('420', '2014-07-29 17:02:12', '2', 'Thêm mới ID#22', 'admincp/category/create', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('421', '2014-07-29 17:02:38', '3', 'Cập nhật ID#21', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('422', '2014-07-29 17:03:13', '3', 'Cập nhật ID#22', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('423', '2014-07-29 17:03:28', '2', 'Thêm mới ID#23', 'admincp/category/create', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('424', '2014-07-29 17:10:35', '2', 'Thêm mới ID#17', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('425', '2014-07-29 17:11:05', '3', 'Cập nhật ID#17', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('426', '2014-07-29 17:13:29', '2', 'Thêm mới ID#18', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('427', '2014-07-29 17:18:57', '2', 'Thêm mới ID#8', 'admincp/testimonial/create', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('428', '2014-07-29 17:22:25', '2', 'Thêm mới ID#19', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('429', '2014-07-29 17:25:23', '2', 'Thêm mới ID#20', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('430', '2014-07-29 17:27:12', '2', 'Thêm mới ID#21', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('431', '2014-07-29 17:29:44', '2', 'Thêm mới ID#22', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('432', '2014-07-29 17:30:02', '3', 'Cập nhật ID#22', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('433', '2014-07-29 17:31:16', '2', 'Thêm mới ID#9', 'admincp/testimonial/create', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('434', '2014-07-29 17:31:33', '3', 'Cập nhật ID#9', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('435', '2014-07-29 17:31:57', '3', 'Cập nhật ID#9', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('436', '2014-07-29 17:33:00', '3', 'Cập nhật ID#9', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('437', '2014-07-29 17:36:33', '2', 'Thêm mới ID#23', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('438', '2014-07-29 17:37:03', '3', 'Cập nhật ID#23', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('439', '2014-07-29 17:37:53', '3', 'Cập nhật ID#23', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('440', '2014-07-29 17:38:01', '3', 'Cập nhật ID#23', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('441', '2014-07-29 17:40:48', '2', 'Thêm mới ID#24', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('442', '2014-07-29 17:41:24', '3', 'Cập nhật ID#24', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('443', '2014-07-29 17:43:29', '2', 'Thêm mới ID#25', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('444', '2014-07-29 17:44:00', '3', 'Cập nhật ID#25', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('445', '2014-07-29 17:44:11', '3', 'Cập nhật ID#25', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('446', '2014-07-29 17:45:16', '2', 'Thêm mới ID#26', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('447', '2014-07-29 17:48:40', '3', 'Cập nhật ID#22', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('448', '2014-07-29 17:49:06', '3', 'Cập nhật ID#22', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('449', '2014-07-29 17:49:48', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('450', '2014-07-29 17:51:39', '2', 'Thêm mới ID#27', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('451', '2014-07-29 17:52:32', '3', 'Cập nhật ID#27', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('452', '2014-07-29 17:53:04', '3', 'Cập nhật ID#27', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('453', '2014-07-29 17:54:01', '2', 'Thêm mới ID#28', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('454', '2014-07-29 17:54:43', '3', 'Cập nhật ID#28', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('455', '2014-07-29 17:56:27', '2', 'Thêm mới ID#29', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('456', '2014-07-29 17:58:27', '3', 'Cập nhật ID#162', 'admincp/settings/update', 'Settings', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('457', '2014-07-29 17:59:07', '3', 'Cập nhật ID#162', 'admincp/settings/update', 'Settings', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('458', '2014-07-29 18:00:59', '2', 'Thêm mới ID#5', 'admincp/about/create', 'About', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('459', '2014-07-29 18:01:26', '3', 'Cập nhật ID#5', 'admincp/about/update', 'About', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('460', '2014-07-29 18:02:18', '2', 'Thêm mới ID#6', 'admincp/about/create', 'About', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('461', '2014-07-29 18:02:43', '3', 'Cập nhật ID#6', 'admincp/about/update', 'About', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('462', '2014-07-29 18:03:21', '2', 'Thêm mới ID#7', 'admincp/about/create', 'About', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('463', '2014-07-29 18:03:30', '3', 'Cập nhật ID#7', 'admincp/about/update', 'About', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('464', '2014-07-29 18:04:25', '2', 'Thêm mới ID#8', 'admincp/about/create', 'About', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('465', '2014-07-29 18:05:20', '2', 'Thêm mới ID#5', 'admincp/staff/create', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('466', '2014-07-29 18:05:41', '3', 'Cập nhật ID#5', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('467', '2014-07-29 18:05:54', '3', 'Cập nhật ID#5', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('468', '2014-07-29 18:06:40', '2', 'Thêm mới ID#6', 'admincp/staff/create', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('469', '2014-07-29 18:07:22', '2', 'Thêm mới ID#7', 'admincp/staff/create', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('470', '2014-07-30 08:14:10', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('471', '2014-07-30 08:14:18', '3', 'Cập nhật ID#7', 'admincp/category/update', 'Category', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('472', '2014-07-30 08:15:35', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('473', '2014-07-30 08:16:07', '3', 'Cập nhật ID#7', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('474', '2014-07-30 08:16:44', '3', 'Cập nhật ID#8', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('475', '2014-07-30 08:19:18', '3', 'Cập nhật ID#5', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('476', '2014-07-30 08:19:51', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('477', '2014-07-30 08:19:58', '3', 'Cập nhật ID#7', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('478', '2014-07-30 08:22:02', '3', 'Cập nhật ID#8', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('479', '2014-07-30 08:25:21', '3', 'Cập nhật ID#8', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('480', '2014-07-30 08:26:55', '3', 'Cập nhật ID#8', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('481', '2014-07-30 08:33:22', '3', 'Cập nhật ID#15', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('482', '2014-07-30 08:33:36', '3', 'Cập nhật ID#14', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('483', '2014-07-30 08:33:52', '3', 'Cập nhật ID#16', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('484', '2014-07-30 08:34:01', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('485', '2014-07-30 08:34:25', '3', 'Cập nhật ID#18', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('486', '2014-07-30 08:39:55', '2', 'Thêm mới ID#24', 'admincp/category/create', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('487', '2014-07-30 08:41:01', '2', 'Thêm mới ID#25', 'admincp/category/create', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('488', '2014-07-30 08:41:35', '3', 'Cập nhật ID#24', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('489', '2014-07-30 08:41:40', '3', 'Cập nhật ID#25', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('490', '2014-07-30 08:43:41', '2', 'Thêm mới ID#26', 'admincp/category/create', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('491', '2014-07-30 08:44:07', '2', 'Thêm mới ID#27', 'admincp/category/create', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('492', '2014-07-30 08:46:25', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('493', '2014-07-30 08:48:06', '3', 'Cập nhật ID#162', 'admincp/settings/update', 'Settings', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('494', '2014-07-30 08:48:33', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('495', '2014-07-30 08:50:56', '3', 'Cập nhật ID#6', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('496', '2014-07-30 08:51:05', '3', 'Cập nhật ID#5', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('497', '2014-07-30 08:51:13', '3', 'Cập nhật ID#7', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('498', '2014-07-30 08:55:48', '2', 'Thêm mới ID#10', 'admincp/testimonial/create', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('499', '2014-07-30 08:56:24', '2', 'Thêm mới ID#11', 'admincp/testimonial/create', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('500', '2014-07-30 08:57:24', '2', 'Thêm mới ID#12', 'admincp/testimonial/create', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('501', '2014-07-30 08:57:56', '3', 'Cập nhật ID#12', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('502', '2014-07-30 08:58:07', '3', 'Cập nhật ID#10', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('503', '2014-07-30 08:59:18', '3', 'Cập nhật ID#11', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('504', '2014-07-30 08:59:47', '3', 'Cập nhật ID#10', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('505', '2014-07-30 09:00:10', '2', 'Thêm mới ID#30', 'admincp/services/create', 'Services', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('506', '2014-07-30 09:00:27', '4', 'Xóa dữ liệu ID#30', 'admincp/services/delete', 'Services', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('507', '2014-07-30 09:02:10', '2', 'Thêm mới ID#13', 'admincp/testimonial/create', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('508', '2014-07-30 09:16:56', '3', 'Cập nhật ID#24', 'admincp/contact/update', 'Contact', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('509', '2014-07-30 09:17:00', '3', 'Cập nhật ID#24', 'admincp/contact/update', 'Contact', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('510', '2014-07-30 09:17:00', '3', 'Cập nhật ID#24', 'admincp/contact/update', 'Contact', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('511', '2014-07-30 09:17:04', '3', 'Cập nhật ID#24', 'admincp/contact/update', 'Contact', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('512', '2014-07-30 09:17:23', '3', 'Cập nhật ID#24', 'admincp/contact/update', 'Contact', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('513', '2014-07-30 09:17:23', '3', 'Cập nhật ID#24', 'admincp/contact/update', 'Contact', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('514', '2014-07-30 09:43:57', '2', 'Thêm mới ID#31', 'admincp/services/create', 'Services', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('515', '2014-07-30 09:44:21', '4', 'Xóa dữ liệu ID#31', 'admincp/services/deleteSelected', 'Services', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('516', '2014-07-30 10:55:38', '3', 'Cập nhật ID#27', 'admincp/contact/update', 'Contact', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('517', '2014-07-30 11:42:12', '2', 'Thêm mới ID#8', 'admincp/staff/create', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('518', '2014-07-30 11:43:13', '2', 'Thêm mới ID#9', 'admincp/staff/create', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('519', '2014-07-30 11:44:08', '2', 'Thêm mới ID#10', 'admincp/staff/create', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('520', '2014-07-30 11:49:25', '3', 'Cập nhật ID#26', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('521', '2014-07-30 11:49:31', '3', 'Cập nhật ID#25', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('522', '2014-07-30 11:49:40', '3', 'Cập nhật ID#24', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('523', '2014-07-30 11:49:46', '3', 'Cập nhật ID#27', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('524', '2014-07-30 11:53:06', '3', 'Cập nhật ID#7', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('525', '2014-07-30 11:53:34', '2', 'Thêm mới ID#28', 'admincp/category/create', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('526', '2014-07-30 11:54:03', '3', 'Cập nhật ID#8', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('527', '2014-07-30 11:55:19', '3', 'Cập nhật ID#25', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('528', '2014-07-30 11:55:25', '3', 'Cập nhật ID#27', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('529', '2014-07-30 11:58:17', '3', 'Cập nhật ID#10', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('530', '2014-07-30 11:59:12', '2', 'Thêm mới ID#9', 'admincp/about/create', 'About', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('531', '2014-07-30 12:01:15', '2', 'Thêm mới ID#10', 'admincp/about/create', 'About', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('532', '2014-07-30 12:05:32', '3', 'Cập nhật ID#9', 'admincp/about/update', 'About', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('533', '2014-07-30 16:06:14', '3', 'Cập nhật ID#12', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('534', '2014-07-30 16:06:23', '3', 'Cập nhật ID#13', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('535', '2014-07-30 16:06:32', '3', 'Cập nhật ID#4', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('536', '2014-07-30 16:14:09', '3', 'Cập nhật ID#6', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('537', '2014-07-30 16:14:23', '3', 'Cập nhật ID#5', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('538', '2014-07-30 16:14:32', '3', 'Cập nhật ID#15', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('539', '2014-07-30 16:14:39', '3', 'Cập nhật ID#16', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('540', '2014-07-30 16:14:51', '3', 'Cập nhật ID#7', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('541', '2014-07-30 16:15:04', '3', 'Cập nhật ID#14', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('542', '2014-07-30 16:15:14', '3', 'Cập nhật ID#9', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('543', '2014-07-30 16:15:21', '3', 'Cập nhật ID#8', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('544', '2014-07-30 16:18:48', '3', 'Cập nhật ID#11', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('545', '2014-07-30 16:18:56', '3', 'Cập nhật ID#10', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('546', '2014-07-30 16:20:16', '3', 'Cập nhật ID#2', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('547', '2014-07-30 16:22:52', '3', 'Cập nhật ID#1', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('548', '2014-07-30 16:23:06', '3', 'Cập nhật ID#3', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('549', '2014-07-30 16:23:16', '3', 'Cập nhật ID#4', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('550', '2014-07-30 16:23:55', '3', 'Cập nhật ID#4', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('551', '2014-07-30 16:24:15', '3', 'Cập nhật ID#4', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('552', '2014-07-30 16:26:14', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('553', '2014-07-30 16:27:05', '3', 'Cập nhật ID#1', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('554', '2014-07-30 16:27:23', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('555', '2014-07-30 16:27:33', '3', 'Cập nhật ID#4', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('556', '2014-07-30 16:27:42', '3', 'Cập nhật ID#5', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('557', '2014-07-30 16:28:11', '3', 'Cập nhật ID#14', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('558', '2014-07-30 16:28:24', '3', 'Cập nhật ID#15', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('559', '2014-07-30 16:28:36', '3', 'Cập nhật ID#16', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('560', '2014-07-30 16:28:44', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('561', '2014-07-30 16:28:56', '3', 'Cập nhật ID#18', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('562', '2014-07-30 16:42:50', '3', 'Cập nhật ID#9', 'admincp/about/update', 'About', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('563', '2014-07-30 16:55:25', '2', 'Thêm mới ID#48', 'admincp/ads/create', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('564', '2014-07-30 16:56:29', '2', 'Thêm mới ID#49', 'admincp/ads/create', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('565', '2014-07-30 16:57:03', '2', 'Thêm mới ID#50', 'admincp/ads/create', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('566', '2014-07-31 09:17:01', '3', 'Cập nhật ID#50', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('567', '2014-07-31 09:17:09', '3', 'Cập nhật ID#50', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('568', '2014-07-31 10:28:13', '3', 'Cập nhật ID#9', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('569', '2014-07-31 10:28:22', '3', 'Cập nhật ID#9', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('570', '2014-07-31 10:41:11', '2', 'Thêm mới ID#164', 'admincp/settings/create', 'Settings', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('571', '2014-07-31 10:50:06', '3', 'Cập nhật ID#164', 'admincp/settings/update', 'Settings', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('572', '2014-07-31 13:40:02', '3', 'Cập nhật ID#16', 'admincp/category/update', 'Category', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('573', '2014-07-31 13:40:52', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('574', '2014-07-31 14:10:33', '3', 'Cập nhật ID#27', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('575', '2014-07-31 14:18:37', '3', 'Cập nhật ID#27', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('576', '2014-07-31 14:19:02', '3', 'Cập nhật ID#24', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('577', '2014-07-31 14:31:36', '2', 'Thêm mới ID#14', 'admincp/testimonial/create', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('578', '2014-07-31 14:32:16', '3', 'Cập nhật ID#14', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('579', '2014-07-31 14:32:55', '3', 'Cập nhật ID#7', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('580', '2014-07-31 14:33:09', '3', 'Cập nhật ID#5', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('581', '2014-07-31 14:33:35', '3', 'Cập nhật ID#6', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('582', '2014-07-31 14:34:01', '3', 'Cập nhật ID#2', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('583', '2014-07-31 14:48:23', '3', 'Cập nhật ID#25', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('584', '2014-07-31 14:49:00', '3', 'Cập nhật ID#28', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('585', '2014-07-31 14:50:06', '3', 'Cập nhật ID#26', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('586', '2014-07-31 15:20:05', '3', 'Cập nhật ID#24', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('587', '2014-07-31 15:20:21', '3', 'Cập nhật ID#25', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('588', '2014-07-31 15:20:30', '3', 'Cập nhật ID#26', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('589', '2014-07-31 15:20:37', '3', 'Cập nhật ID#29', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('590', '2014-07-31 15:20:40', '3', 'Cập nhật ID#29', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('591', '2014-07-31 15:20:51', '3', 'Cập nhật ID#28', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('592', '2014-07-31 15:21:00', '3', 'Cập nhật ID#27', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('593', '2014-07-31 15:24:21', '3', 'Cập nhật ID#9', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('594', '2014-07-31 15:24:29', '3', 'Cập nhật ID#8', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('595', '2014-07-31 15:24:36', '3', 'Cập nhật ID#10', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('596', '2014-07-31 15:24:46', '3', 'Cập nhật ID#5', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('597', '2014-07-31 15:24:54', '3', 'Cập nhật ID#6', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('598', '2014-07-31 15:25:02', '3', 'Cập nhật ID#7', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('599', '2014-07-31 15:26:08', '3', 'Cập nhật ID#6', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('600', '2014-07-31 15:27:14', '2', 'Thêm mới ID#11', 'admincp/staff/create', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('601', '2014-07-31 15:35:16', '3', 'Cập nhật ID#9', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('602', '2014-07-31 15:58:50', '2', 'Thêm mới ID#12', 'admincp/staff/create', 'Staff', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('603', '2014-07-31 16:00:08', '3', 'Cập nhật ID#12', 'admincp/staff/update', 'Staff', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('604', '2014-07-31 16:00:21', '4', 'Xóa dữ liệu ID#12', 'admincp/staff/delete', 'Staff', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('605', '2014-07-31 16:02:54', '3', 'Cập nhật ID#7', 'admincp/staff/update', 'Staff', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('606', '2014-07-31 16:03:27', '3', 'Cập nhật ID#4', 'admincp/staff/update', 'Staff', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('607', '2014-07-31 16:03:57', '3', 'Cập nhật ID#3', 'admincp/staff/update', 'Staff', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('608', '2014-07-31 16:08:00', '2', 'Thêm mới ID#13', 'admincp/staff/create', 'Staff', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('609', '2014-07-31 16:08:25', '4', 'Xóa dữ liệu ID#2', 'admincp/staff/delete', 'Staff', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('610', '2014-07-31 16:08:36', '3', 'Cập nhật ID#13', 'admincp/staff/update', 'Staff', '1', '-1062731060', '1');
INSERT INTO `pk_log` VALUES ('611', '2014-07-31 16:12:16', '3', 'Cập nhật ID#25', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('612', '2014-07-31 16:58:44', '3', 'Cập nhật ID#29', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('613', '2014-07-31 16:59:02', '3', 'Cập nhật ID#27', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('614', '2014-07-31 17:04:49', '3', 'Cập nhật ID#5', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('615', '2014-07-31 17:05:36', '3', 'Cập nhật ID#6', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('616', '2014-07-31 17:05:51', '3', 'Cập nhật ID#14', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('617', '2014-07-31 17:06:01', '3', 'Cập nhật ID#16', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('618', '2014-07-31 17:09:12', '2', 'Thêm mới ID#14', 'admincp/staff/create', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('619', '2014-07-31 17:10:08', '3', 'Cập nhật ID#13', 'admincp/staff/update', 'Staff', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('620', '2014-07-31 17:25:11', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('621', '2014-07-31 17:27:05', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('622', '2014-08-01 12:17:19', '3', 'Cập nhật ID#24', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('623', '2014-08-01 12:17:32', '3', 'Cập nhật ID#23', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('624', '2014-08-04 16:42:51', '2', 'Thêm mới ID#29', 'admincp/category/create', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('625', '2014-08-04 16:43:16', '3', 'Cập nhật ID#8', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('626', '2014-08-04 16:43:29', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('627', '2014-08-04 16:43:32', '3', 'Cập nhật ID#7', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('628', '2014-08-04 16:43:36', '3', 'Cập nhật ID#28', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('629', '2014-08-04 16:43:41', '3', 'Cập nhật ID#29', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('630', '2014-08-04 16:44:17', '3', 'Cập nhật ID#27', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('631', '2014-08-04 16:44:21', '3', 'Cập nhật ID#26', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('632', '2014-08-04 16:44:25', '3', 'Cập nhật ID#25', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('633', '2014-08-04 16:44:29', '3', 'Cập nhật ID#24', 'admincp/category/update', 'Category', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('634', '2014-08-04 16:45:43', '2', 'Thêm mới ID#30', 'admincp/services/create', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('635', '2014-08-04 16:48:10', '3', 'Cập nhật ID#7', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('636', '2014-08-04 16:51:08', '3', 'Cập nhật ID#7', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('637', '2014-08-05 09:45:06', '3', 'Cập nhật ID#24', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('638', '2014-08-05 09:45:46', '3', 'Cập nhật ID#23', 'admincp/services/update', 'Services', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('639', '2014-08-05 17:15:09', '3', 'Cập nhật ID#48', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('640', '2014-08-05 17:15:17', '3', 'Cập nhật ID#49', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('641', '2014-08-05 17:15:27', '3', 'Cập nhật ID#47', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('642', '2014-08-05 17:15:36', '3', 'Cập nhật ID#46', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('643', '2014-08-05 17:15:46', '3', 'Cập nhật ID#45', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('644', '2014-08-05 17:16:01', '3', 'Cập nhật ID#44', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('645', '2014-08-05 17:16:18', '3', 'Cập nhật ID#43', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('646', '2014-08-05 17:30:50', '3', 'Cập nhật ID#37', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('647', '2014-08-05 17:31:14', '3', 'Cập nhật ID#38', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('648', '2014-08-05 17:31:28', '3', 'Cập nhật ID#39', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('649', '2014-08-05 17:32:03', '3', 'Cập nhật ID#46', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('650', '2014-08-05 17:38:00', '3', 'Cập nhật ID#40', 'admincp/ads/update', 'Ads', '1', '-1062731042', '1');
INSERT INTO `pk_log` VALUES ('651', '2014-08-12 16:13:24', '3', 'Cập nhật ID#155', 'admincp/settings/update', 'Settings', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('652', '2014-08-12 16:26:04', '3', 'Cập nhật ID#5', 'admincp/category/update', 'Category', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('653', '2014-08-12 16:26:52', '3', 'Cập nhật ID#4', 'admincp/category/update', 'Category', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('654', '2014-08-12 16:27:21', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('655', '2014-08-12 16:27:40', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('656', '2014-08-12 16:27:52', '3', 'Cập nhật ID#1', 'admincp/category/update', 'Category', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('657', '2014-08-12 17:06:17', '2', 'Thêm mới ID#15', 'admincp/testimonial/create', 'Testimonial', '1', '-1062731032', '1');
INSERT INTO `pk_log` VALUES ('658', '2014-09-07 01:24:09', '2', 'Thêm mới ID#146', 'admincp/node/create', 'AdmincpNode', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('659', '2014-09-09 06:01:57', '3', 'Cập nhật ID#27', 'admincp/category/update', 'Category', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('660', '2014-09-09 06:02:31', '3', 'Cập nhật ID#26', 'admincp/category/update', 'Category', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('661', '2014-09-09 06:02:38', '3', 'Cập nhật ID#25', 'admincp/category/update', 'Category', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('662', '2014-09-09 06:02:48', '3', 'Cập nhật ID#24', 'admincp/category/update', 'Category', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('663', '2014-09-09 06:03:00', '3', 'Cập nhật ID#25', 'admincp/category/update', 'Category', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('664', '2014-09-09 06:03:12', '3', 'Cập nhật ID#26', 'admincp/category/update', 'Category', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('665', '2014-09-09 06:05:17', '2', 'Thêm mới ID#11', 'admincp/about/create', 'About', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('666', '2014-09-09 06:05:28', '3', 'Cập nhật ID#11', 'admincp/about/update', 'About', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('667', '2014-09-09 06:06:07', '2', 'Thêm mới ID#12', 'admincp/about/create', 'About', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('668', '2014-09-09 06:07:16', '2', 'Thêm mới ID#13', 'admincp/about/create', 'About', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('669', '2014-09-09 06:13:02', '3', 'Cập nhật ID#12', 'admincp/about/update', 'About', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('670', '2014-09-09 06:13:09', '3', 'Cập nhật ID#11', 'admincp/about/update', 'About', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('671', '2014-09-09 06:15:56', '2', 'Thêm mới ID#14', 'admincp/about/create', 'About', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('672', '2014-09-09 06:16:15', '3', 'Cập nhật ID#14', 'admincp/about/update', 'About', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('673', '2014-09-22 10:32:58', '2', 'Thêm mới ID#147', 'admincp/node/create', 'AdmincpNode', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('674', '2014-09-22 10:33:16', '2', 'Thêm mới ID#148', 'admincp/node/create', 'AdmincpNode', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('675', '2014-09-22 10:33:34', '3', 'Cập nhật ID#137', 'admincp/node/update', 'AdmincpNode', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('676', '2014-09-22 10:33:47', '3', 'Cập nhật ID#4', 'admincp/node/update', 'AdmincpNode', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('677', '2014-09-22 10:33:57', '3', 'Cập nhật ID#148', 'admincp/node/update', 'AdmincpNode', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('678', '2014-09-22 10:34:11', '2', 'Thêm mới ID#149', 'admincp/node/create', 'AdmincpNode', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('679', '2014-09-22 14:22:22', '4', 'Xóa dữ liệu ID#30', 'admincp/services/delete', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('680', '2014-09-22 14:22:30', '4', 'Xóa dữ liệu ID#29', 'admincp/category/delete', 'Category', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('681', '2014-09-22 14:24:15', '3', 'Cập nhật ID#16', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('682', '2014-09-22 14:24:31', '3', 'Cập nhật ID#15', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('683', '2014-09-22 14:24:48', '3', 'Cập nhật ID#14', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('684', '2014-09-22 14:25:17', '3', 'Cập nhật ID#11', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('685', '2014-09-22 14:25:29', '3', 'Cập nhật ID#10', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('686', '2014-09-22 14:25:56', '3', 'Cập nhật ID#9', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('687', '2014-09-22 14:26:08', '3', 'Cập nhật ID#8', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('688', '2014-09-22 14:26:24', '3', 'Cập nhật ID#6', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('689', '2014-09-22 14:26:36', '3', 'Cập nhật ID#5', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('690', '2014-09-22 14:26:50', '3', 'Cập nhật ID#8', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('691', '2014-09-22 14:40:29', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('692', '2014-09-22 14:41:12', '3', 'Cập nhật ID#16', 'admincp/category/update', 'Category', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('693', '2014-09-22 14:41:30', '3', 'Cập nhật ID#21', 'admincp/category/update', 'Category', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('694', '2014-09-22 14:42:07', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('695', '2014-09-23 08:52:06', '3', 'Cập nhật ID#29', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('696', '2014-09-23 08:52:19', '3', 'Cập nhật ID#28', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('697', '2014-09-23 08:52:37', '3', 'Cập nhật ID#27', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('698', '2014-09-23 08:52:48', '3', 'Cập nhật ID#26', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('699', '2014-09-23 08:52:59', '3', 'Cập nhật ID#25', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('700', '2014-09-23 08:53:17', '3', 'Cập nhật ID#22', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('701', '2014-09-23 08:53:24', '3', 'Cập nhật ID#22', 'admincp/services/update', 'Services', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('702', '2014-09-23 09:09:33', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('703', '2014-09-23 09:10:27', '3', 'Cập nhật ID#8', 'admincp/category/update', 'Category', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('704', '2014-09-23 09:10:55', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('705', '2014-09-23 09:11:21', '3', 'Cập nhật ID#8', 'admincp/category/update', 'Category', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('706', '2014-09-23 09:13:28', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('707', '2014-09-23 09:13:54', '3', 'Cập nhật ID#8', 'admincp/category/update', 'Category', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('708', '2014-09-23 11:09:59', '2', 'Thêm mới ID#4', 'admincp/discounts/create', 'Discounts', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('709', '2014-09-23 13:59:08', '3', 'Cập nhật ID#16', 'admincp/category/update', 'Category', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('710', '2014-09-23 13:59:43', '3', 'Cập nhật ID#16', 'admincp/category/update', 'Category', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('711', '2014-09-23 14:03:23', '3', 'Cập nhật ID#13', 'admincp/testimonial/update', 'Testimonial', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('712', '2014-09-23 14:08:27', '2', 'Thêm mới ID#15', 'admincp/about/create', 'About', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('713', '2014-09-23 14:08:47', '3', 'Cập nhật ID#15', 'admincp/about/update', 'About', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('714', '2014-09-23 14:09:12', '3', 'Cập nhật ID#15', 'admincp/about/update', 'About', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('715', '2014-09-23 14:12:22', '2', 'Thêm mới ID#16', 'admincp/about/create', 'About', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('716', '2014-09-23 14:12:34', '3', 'Cập nhật ID#15', 'admincp/about/update', 'About', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('717', '2014-09-23 14:13:41', '2', 'Thêm mới ID#17', 'admincp/about/create', 'About', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('718', '2014-09-23 14:13:48', '3', 'Cập nhật ID#17', 'admincp/about/update', 'About', '1', '-1062731041', '1');
INSERT INTO `pk_log` VALUES ('719', '2014-09-23 15:56:10', '2', 'Thêm mới ID#165', 'admincp/settings/create', 'Settings', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('720', '2014-09-23 15:56:17', '2', 'Thêm mới ID#166', 'admincp/settings/create', 'Settings', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('721', '2014-09-23 15:56:25', '3', 'Cập nhật ID#165', 'admincp/settings/update', 'Settings', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('722', '2014-09-23 15:56:31', '3', 'Cập nhật ID#166', 'admincp/settings/update', 'Settings', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('723', '2014-09-23 15:56:45', '2', 'Thêm mới ID#167', 'admincp/settings/create', 'Settings', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('724', '2014-09-23 15:56:55', '2', 'Thêm mới ID#168', 'admincp/settings/create', 'Settings', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('725', '2014-09-23 15:57:17', '2', 'Thêm mới ID#169', 'admincp/settings/create', 'Settings', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('726', '2014-09-23 15:57:30', '2', 'Thêm mới ID#170', 'admincp/settings/create', 'Settings', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('727', '2014-09-26 09:48:03', '2', 'Thêm mới ID#171', 'admincp/settings/create', 'Settings', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('728', '2014-09-26 09:48:20', '2', 'Thêm mới ID#172', 'admincp/settings/create', 'Settings', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('729', '2014-09-26 11:00:49', '2', 'Thêm mới ID#150', 'admincp/node/create', 'AdmincpNode', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('730', '2014-09-29 09:01:28', '2', 'Thêm mới ID#5', 'admincp/discounts/create', 'Discounts', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('731', '2014-09-29 10:19:13', '3', 'Cập nhật ID#57', 'admincp/node/update', 'AdmincpNode', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('732', '2014-09-29 10:19:36', '2', 'Thêm mới ID#151', 'admincp/node/create', 'AdmincpNode', '1', '-1062731248', '1');
INSERT INTO `pk_log` VALUES ('733', '2014-09-29 10:41:34', '2', 'Thêm mới ID#6', 'admincp/discounts/create', 'Discounts', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('734', '2014-09-29 10:41:52', '3', 'Cập nhật ID#6', 'admincp/discounts/update', 'Discounts', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('735', '2014-09-29 13:53:41', '3', 'Cập nhật ID#26', 'admincp/category/update', 'Category', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('736', '2014-09-29 14:01:38', '3', 'Cập nhật ID#1', 'admincp/payment/update', 'Payment', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('737', '2014-09-29 14:06:45', '3', 'Cập nhật ID#2', 'admincp/payment/update', 'Payment', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('738', '2014-09-29 15:19:52', '3', 'Cập nhật ID#10', 'admincp/services/update', 'Services', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('739', '2014-09-29 15:20:02', '3', 'Cập nhật ID#9', 'admincp/services/update', 'Services', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('740', '2014-09-29 15:20:36', '3', 'Cập nhật ID#14', 'admincp/services/update', 'Services', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('741', '2014-09-29 15:20:43', '3', 'Cập nhật ID#11', 'admincp/services/update', 'Services', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('742', '2014-09-29 15:20:55', '3', 'Cập nhật ID#8', 'admincp/services/update', 'Services', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('743', '2014-09-29 15:21:03', '3', 'Cập nhật ID#7', 'admincp/services/update', 'Services', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('744', '2014-09-29 15:21:10', '3', 'Cập nhật ID#5', 'admincp/services/update', 'Services', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('745', '2014-09-30 09:43:33', '3', 'Cập nhật ID#4', 'admincp/discounts/update', 'Discounts', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('746', '2014-09-30 17:17:51', '2', 'Thêm mới ID#7', 'admincp/discounts/create', 'Discounts', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('747', '2014-10-01 11:01:27', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('748', '2014-10-01 12:09:02', '2', 'Thêm mới ID#18', 'admincp/about/create', 'About', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('749', '2014-10-01 12:10:01', '3', 'Cập nhật ID#18', 'admincp/about/update', 'About', '1', '-1062731021', '1');
INSERT INTO `pk_log` VALUES ('750', '2014-10-06 15:45:45', '3', 'Cập nhật ID#4', 'admincp/about/update', 'About', '1', '-1062731029', '1');
INSERT INTO `pk_log` VALUES ('751', '2014-10-06 15:46:24', '3', 'Cập nhật ID#4', 'admincp/about/update', 'About', '1', '-1062731029', '1');
INSERT INTO `pk_log` VALUES ('752', '2014-10-06 15:48:46', '3', 'Cập nhật ID#1', 'admincp/about/update', 'About', '1', '-1062731029', '1');
INSERT INTO `pk_log` VALUES ('753', '2014-11-04 08:58:35', '2', 'Thêm mới ID#173', 'admincp/settings/create', 'Settings', '1', '-1062731142', '1');
INSERT INTO `pk_log` VALUES ('754', '2014-11-04 09:30:54', '2', 'Thêm mới ID#29', 'admincp/category/create', 'Category', '1', '-1062731142', '1');
INSERT INTO `pk_log` VALUES ('755', '2014-11-04 09:31:11', '2', 'Thêm mới ID#30', 'admincp/category/create', 'Category', '1', '-1062731142', '1');
INSERT INTO `pk_log` VALUES ('756', '2014-11-04 09:31:49', '3', 'Cập nhật ID#29', 'admincp/category/update', 'Category', '1', '-1062731142', '1');
INSERT INTO `pk_log` VALUES ('757', '2014-11-04 09:31:53', '3', 'Cập nhật ID#30', 'admincp/category/update', 'Category', '1', '-1062731142', '1');
INSERT INTO `pk_log` VALUES ('758', '2014-11-07 09:01:43', '2', 'Thêm mới ID#152', 'admincp/node/create', 'AdmincpNode', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('759', '2014-11-07 09:05:21', '2', 'Thêm mới ID#153', 'admincp/node/create', 'AdmincpNode', '1', '-1062731142', '1');
INSERT INTO `pk_log` VALUES ('760', '2014-11-07 09:11:20', '2', 'Thêm mới ID#31', 'admincp/category/create', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('761', '2014-11-07 09:12:30', '2', 'Thêm mới ID#32', 'admincp/category/create', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('762', '2014-11-07 09:14:59', '2', 'Thêm mới ID#33', 'admincp/category/create', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('763', '2014-11-07 09:15:34', '2', 'Thêm mới ID#34', 'admincp/category/create', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('764', '2014-11-07 09:16:25', '3', 'Cập nhật ID#31', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('765', '2014-11-07 09:16:35', '3', 'Cập nhật ID#32', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('766', '2014-11-07 09:16:41', '3', 'Cập nhật ID#33', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('767', '2014-11-07 09:16:49', '3', 'Cập nhật ID#34', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('768', '2014-11-07 09:18:54', '3', 'Cập nhật ID#8', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('769', '2014-11-07 09:19:07', '3', 'Cập nhật ID#4', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('770', '2014-11-07 09:19:28', '3', 'Cập nhật ID#1', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('771', '2014-11-07 09:21:46', '2', 'Thêm mới ID#9', 'admincp/products/create', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('772', '2014-11-07 09:22:39', '3', 'Cập nhật ID#9', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('773', '2014-11-07 09:24:29', '2', 'Thêm mới ID#10', 'admincp/products/create', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('774', '2014-11-07 09:27:51', '2', 'Thêm mới ID#11', 'admincp/products/create', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('775', '2014-11-07 09:29:04', '3', 'Cập nhật ID#11', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('776', '2014-11-07 09:40:49', '2', 'Thêm mới ID#12', 'admincp/products/create', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('777', '2014-11-07 09:42:10', '2', 'Thêm mới ID#13', 'admincp/products/create', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('778', '2014-11-07 09:55:21', '3', 'Cập nhật ID#7', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('779', '2014-11-07 10:21:23', '3', 'Cập nhật ID#3', 'admincp/payment/update', 'Payment', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('780', '2014-11-07 10:42:40', '3', 'Cập nhật ID#5', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('781', '2014-11-07 10:54:50', '3', 'Cập nhật ID#11', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('782', '2014-11-07 10:55:12', '3', 'Cập nhật ID#11', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('783', '2014-11-07 11:40:45', '3', 'Cập nhật ID#16', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('784', '2014-11-07 11:41:10', '3', 'Cập nhật ID#15', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('785', '2014-11-07 11:41:50', '3', 'Cập nhật ID#15', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('786', '2014-11-07 11:41:59', '3', 'Cập nhật ID#14', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('787', '2014-11-07 11:43:43', '3', 'Cập nhật ID#2', 'admincp/payment/update', 'Payment', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('788', '2014-11-07 11:44:20', '3', 'Cập nhật ID#1', 'admincp/payment/update', 'Payment', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('789', '2014-11-07 11:44:56', '3', 'Cập nhật ID#1', 'admincp/payment/update', 'Payment', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('790', '2014-11-07 11:45:05', '3', 'Cập nhật ID#3', 'admincp/payment/update', 'Payment', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('791', '2014-11-07 11:47:04', '2', 'Thêm mới ID#14', 'admincp/products/create', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('792', '2014-11-07 11:56:49', '2', 'Thêm mới ID#15', 'admincp/products/create', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('793', '2014-11-07 12:03:35', '2', 'Thêm mới ID#16', 'admincp/products/create', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('794', '2014-11-07 14:40:15', '2', 'Thêm mới ID#35', 'admincp/category/create', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('795', '2014-11-07 14:40:39', '2', 'Thêm mới ID#36', 'admincp/category/create', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('796', '2014-11-07 14:41:12', '3', 'Cập nhật ID#35', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('797', '2014-11-07 14:42:11', '3', 'Cập nhật ID#9', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('798', '2014-11-07 14:42:27', '3', 'Cập nhật ID#10', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('799', '2014-11-07 14:42:35', '3', 'Cập nhật ID#11', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('800', '2014-11-07 14:42:50', '3', 'Cập nhật ID#35', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('801', '2014-11-07 14:42:59', '3', 'Cập nhật ID#13', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('802', '2014-11-07 14:43:06', '3', 'Cập nhật ID#12', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('803', '2014-11-07 14:43:52', '3', 'Cập nhật ID#11', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('804', '2014-11-07 14:55:18', '3', 'Cập nhật ID#29', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('805', '2014-11-07 14:55:33', '3', 'Cập nhật ID#29', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('806', '2014-11-07 14:56:30', '3', 'Cập nhật ID#29', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('807', '2014-11-07 14:56:51', '3', 'Cập nhật ID#35', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('808', '2014-11-07 14:57:06', '3', 'Cập nhật ID#36', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('809', '2014-11-07 14:57:31', '3', 'Cập nhật ID#30', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('810', '2014-11-07 15:59:20', '3', 'Cập nhật ID#12', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('811', '2014-11-07 16:01:22', '3', 'Cập nhật ID#12', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('812', '2014-11-07 16:02:44', '3', 'Cập nhật ID#12', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('813', '2014-11-07 16:15:27', '3', 'Cập nhật ID#10', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('814', '2014-11-07 16:15:45', '3', 'Cập nhật ID#10', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('815', '2014-11-11 14:52:16', '2', 'Thêm mới ID#17', 'admincp/products/create', 'Products', '1', '-1062731142', '1');
INSERT INTO `pk_log` VALUES ('816', '2014-11-11 14:52:20', '4', 'Xóa dữ liệu ID#17', 'admincp/products/delete', 'Products', '1', '-1062731142', '1');
INSERT INTO `pk_log` VALUES ('817', '2014-11-11 15:17:53', '2', 'Thêm mới ID#17', 'admincp/products/create', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('818', '2014-11-11 15:20:47', '3', 'Cập nhật ID#3', 'admincp/payment/update', 'Payment', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('819', '2014-11-11 15:21:10', '3', 'Cập nhật ID#3', 'admincp/payment/update', 'Payment', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('820', '2014-11-11 15:57:05', '3', 'Cập nhật ID#15', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('821', '2014-11-11 16:01:42', '3', 'Cập nhật ID#14', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('822', '2014-11-11 16:03:00', '2', 'Thêm mới ID#18', 'admincp/products/create', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('823', '2014-11-11 16:03:39', '3', 'Cập nhật ID#18', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('824', '2014-11-11 16:16:31', '3', 'Cập nhật ID#18', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('825', '2014-11-11 16:17:24', '3', 'Cập nhật ID#18', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('826', '2014-11-11 16:20:40', '3', 'Cập nhật ID#7', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('827', '2014-11-11 16:21:01', '3', 'Cập nhật ID#7', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('828', '2014-11-11 16:32:06', '3', 'Cập nhật ID#6', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('829', '2014-11-11 17:06:12', '2', 'Thêm mới ID#19', 'admincp/products/create', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('830', '2014-11-12 11:10:50', '3', 'Cập nhật ID#21', 'admincp/productorders/update', 'Productorder', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('831', '2014-11-12 11:12:33', '3', 'Cập nhật ID#18', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('832', '2014-11-12 11:12:50', '3', 'Cập nhật ID#12', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('833', '2014-11-12 11:13:17', '3', 'Cập nhật ID#21', 'admincp/productorders/update', 'Productorder', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('834', '2014-11-12 11:17:45', '2', 'Thêm mới ID#20', 'admincp/products/create', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('835', '2014-11-12 11:18:56', '3', 'Cập nhật ID#20', 'admincp/products/update', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('836', '2014-11-12 11:21:49', '2', 'Thêm mới ID#21', 'admincp/products/create', 'Products', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('837', '2014-11-12 11:24:57', '3', 'Cập nhật ID#19', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('838', '2014-11-12 11:25:10', '3', 'Cập nhật ID#20', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('839', '2014-11-12 11:25:21', '3', 'Cập nhật ID#21', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('840', '2014-11-12 11:25:30', '3', 'Cập nhật ID#36', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('841', '2014-11-12 11:25:42', '3', 'Cập nhật ID#22', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('842', '2014-11-12 11:25:52', '3', 'Cập nhật ID#23', 'admincp/category/update', 'Category', '1', '-1062731048', '1');
INSERT INTO `pk_log` VALUES ('843', '2014-11-12 12:01:20', '2', 'Thêm mới ID#174', 'admincp/settings/create', 'Settings', '1', '-1062731142', '1');
INSERT INTO `pk_log` VALUES ('844', '2014-11-12 12:02:08', '3', 'Cập nhật ID#174', 'admincp/settings/update', 'Settings', '1', '-1062731142', '1');
INSERT INTO `pk_log` VALUES ('845', '2014-11-21 11:15:14', '2', 'Thêm mới ID#154', 'admincp/node/create', 'AdmincpNode', '1', '-1062731263', '1');
INSERT INTO `pk_log` VALUES ('846', '2014-11-21 11:15:53', '2', 'Thêm mới ID#155', 'admincp/node/create', 'AdmincpNode', '1', '-1062731263', '1');
INSERT INTO `pk_log` VALUES ('847', '2014-11-21 11:16:15', '3', 'Cập nhật ID#155', 'admincp/node/update', 'AdmincpNode', '1', '-1062731263', '1');
INSERT INTO `pk_log` VALUES ('848', '2014-11-21 11:16:42', '2', 'Thêm mới ID#156', 'admincp/node/create', 'AdmincpNode', '1', '-1062731263', '1');
INSERT INTO `pk_log` VALUES ('849', '2014-11-21 11:19:39', '2', 'Thêm mới ID#157', 'admincp/node/create', 'AdmincpNode', '1', '-1062731263', '1');
INSERT INTO `pk_log` VALUES ('850', '2014-11-21 11:20:09', '2', 'Thêm mới ID#158', 'admincp/node/create', 'AdmincpNode', '1', '-1062731263', '1');
INSERT INTO `pk_log` VALUES ('851', '2014-12-05 13:37:59', '4', 'Xóa dữ liệu ID#119', 'admincp/node/delete', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('852', '2014-12-05 13:42:59', '4', 'Xóa dữ liệu ID#151', 'admincp/node/delete', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('853', '2014-12-05 13:46:41', '4', 'Xóa dữ liệu ID#99', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('854', '2014-12-05 13:46:41', '4', 'Xóa dữ liệu ID#121', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('855', '2014-12-05 13:46:41', '4', 'Xóa dữ liệu ID#124', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('856', '2014-12-05 13:46:41', '4', 'Xóa dữ liệu ID#140', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('857', '2014-12-05 13:46:41', '4', 'Xóa dữ liệu ID#141', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('858', '2014-12-05 13:46:41', '4', 'Xóa dữ liệu ID#143', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('859', '2014-12-05 13:46:41', '4', 'Xóa dữ liệu ID#144', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('860', '2014-12-05 13:46:41', '4', 'Xóa dữ liệu ID#145', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('861', '2014-12-05 13:46:41', '4', 'Xóa dữ liệu ID#149', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('862', '2014-12-05 13:46:41', '4', 'Xóa dữ liệu ID#150', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('863', '2014-12-05 13:46:42', '4', 'Xóa dữ liệu ID#152', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('864', '2014-12-05 13:46:42', '4', 'Xóa dữ liệu ID#153', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('865', '2014-12-05 13:46:42', '4', 'Xóa dữ liệu ID#154', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('866', '2014-12-05 13:47:05', '4', 'Xóa dữ liệu ID#148', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('867', '2014-12-05 13:47:30', '4', 'Xóa dữ liệu ID#147', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('868', '2014-12-05 13:48:06', '2', 'Thêm mới ID#159', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('869', '2014-12-05 13:48:53', '3', 'Cập nhật ID#125', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('870', '2014-12-05 13:49:11', '4', 'Xóa dữ liệu ID#125', 'admincp/node/delete', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('871', '2014-12-05 13:52:25', '2', 'Thêm mới ID#160', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('872', '2014-12-05 14:35:01', '2', 'Thêm mới ID#1', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('873', '2014-12-05 14:35:31', '2', 'Thêm mới ID#2', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('874', '2014-12-05 14:37:08', '4', 'Xóa dữ liệu ID#2', 'admincp/post/delete', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('875', '2014-12-05 14:37:10', '4', 'Xóa dữ liệu ID#1', 'admincp/post/delete', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('876', '2014-12-05 14:38:21', '2', 'Thêm mới ID#3', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('877', '2014-12-05 15:19:22', '2', 'Thêm mới ID#2', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('878', '2014-12-05 15:20:21', '2', 'Thêm mới ID#1', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('879', '2014-12-05 15:24:21', '2', 'Thêm mới ID#1', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('880', '2014-12-05 15:26:25', '2', 'Thêm mới ID#1', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('881', '2014-12-05 15:26:50', '2', 'Thêm mới ID#2', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('882', '2014-12-05 15:29:36', '2', 'Thêm mới ID#161', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('883', '2014-12-05 15:39:28', '2', 'Thêm mới ID#1', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('884', '2014-12-05 15:42:22', '3', 'Cập nhật ID#26', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('885', '2014-12-08 14:34:39', '3', 'Cập nhật ID#122', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('886', '2014-12-08 14:52:35', '2', 'Thêm mới ID#1', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('887', '2014-12-08 14:52:38', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('888', '2014-12-08 14:52:43', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('889', '2014-12-08 14:52:51', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('890', '2014-12-08 14:52:53', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('891', '2014-12-08 14:52:54', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('892', '2014-12-08 14:52:54', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('893', '2014-12-08 14:52:56', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('894', '2014-12-08 14:53:00', '4', 'Xóa dữ liệu ID#1', 'admincp/post/delete', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('895', '2014-12-08 15:02:13', '2', 'Thêm mới ID#162', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('896', '2014-12-09 08:15:16', '4', 'Xóa dữ liệu ID#142', 'admincp/node/delete', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('897', '2014-12-09 09:18:53', '2', 'Thêm mới ID#132', 'admincp/ward/create', 'Ward', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('898', '2014-12-09 09:28:57', '2', 'Thêm mới ID#163', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('899', '2014-12-09 09:29:12', '2', 'Thêm mới ID#164', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('900', '2014-12-09 09:29:48', '2', 'Thêm mới ID#165', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('901', '2014-12-09 09:30:01', '2', 'Thêm mới ID#166', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('902', '2014-12-09 09:30:25', '2', 'Thêm mới ID#167', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('903', '2014-12-09 09:48:12', '2', 'Thêm mới ID#168', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('904', '2014-12-09 09:48:31', '2', 'Thêm mới ID#169', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('905', '2014-12-09 09:48:49', '3', 'Cập nhật ID#168', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('906', '2014-12-09 13:54:56', '2', 'Thêm mới ID#1', 'admincp/servicePackage/create', 'ServicePackage', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('907', '2014-12-09 13:55:38', '3', 'Cập nhật ID#1', 'admincp/servicePackage/update', 'ServicePackage', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('908', '2014-12-09 13:56:37', '3', 'Cập nhật ID#1', 'admincp/servicePackage/update', 'ServicePackage', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('909', '2014-12-09 13:57:17', '3', 'Cập nhật ID#1', 'admincp/servicePackage/update', 'ServicePackage', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('910', '2014-12-09 15:11:46', '3', 'Cập nhật ID#1', 'admincp/servicePackage/update', 'ServicePackage', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('911', '2014-12-09 15:16:31', '2', 'Thêm mới ID#170', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('912', '2014-12-09 15:26:40', '3', 'Cập nhật ID#161', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('913', '2014-12-09 15:26:57', '4', 'Xóa dữ liệu ID#146', 'admincp/node/delete', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('914', '2014-12-09 15:27:24', '2', 'Thêm mới ID#171', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('915', '2014-12-22 08:50:01', '2', 'Thêm mới ID#1', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('916', '2014-12-22 08:50:08', '2', 'Thêm mới ID#2', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('917', '2014-12-22 08:50:32', '2', 'Thêm mới ID#3', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('918', '2014-12-22 08:50:37', '2', 'Thêm mới ID#4', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('919', '2014-12-22 08:50:45', '2', 'Thêm mới ID#5', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('920', '2014-12-22 08:51:51', '2', 'Thêm mới ID#1', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('921', '2014-12-22 08:51:54', '2', 'Thêm mới ID#2', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('922', '2014-12-22 08:52:09', '2', 'Thêm mới ID#3', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('923', '2014-12-22 08:52:21', '2', 'Thêm mới ID#4', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('924', '2014-12-22 08:52:29', '2', 'Thêm mới ID#5', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('925', '2014-12-22 08:52:33', '2', 'Thêm mới ID#6', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('926', '2014-12-22 09:16:49', '2', 'Thêm mới ID#172', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('927', '2014-12-22 09:17:01', '3', 'Cập nhật ID#172', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('928', '2014-12-22 09:58:11', '3', 'Cập nhật ID#1', 'admincp/servicePackage/update', 'ServicePackage', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('929', '2014-12-22 10:00:09', '2', 'Thêm mới ID#173', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('930', '2014-12-22 13:27:27', '2', 'Thêm mới ID#1', 'admincp/scale/create', 'Scale', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('931', '2014-12-22 13:27:54', '2', 'Thêm mới ID#2', 'admincp/scale/create', 'Scale', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('932', '2014-12-22 13:28:00', '2', 'Thêm mới ID#3', 'admincp/scale/create', 'Scale', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('933', '2014-12-23 10:16:51', '2', 'Thêm mới ID#1', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('934', '2014-12-23 10:21:25', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('935', '2014-12-23 10:28:00', '2', 'Thêm mới ID#2', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('936', '2014-12-23 10:28:05', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('937', '2014-12-23 10:30:36', '2', 'Thêm mới ID#1', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('938', '2014-12-23 10:30:39', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('939', '2014-12-23 10:36:47', '2', 'Thêm mới ID#2', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('940', '2014-12-23 10:36:50', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('941', '2014-12-23 10:36:57', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('942', '2014-12-23 10:36:57', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('943', '2014-12-23 10:36:58', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('944', '2014-12-23 10:37:06', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('945', '2014-12-23 10:37:06', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('946', '2014-12-23 10:37:08', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('947', '2014-12-23 10:37:15', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('948', '2014-12-23 10:37:15', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('949', '2014-12-23 10:37:18', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('950', '2014-12-23 10:49:07', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('951', '2014-12-23 10:49:12', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('952', '2014-12-23 10:49:12', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('953', '2014-12-23 10:49:14', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('954', '2014-12-23 10:49:21', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('955', '2014-12-23 10:49:21', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('956', '2014-12-23 10:49:23', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('957', '2014-12-23 10:49:33', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('958', '2014-12-23 10:49:39', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('959', '2014-12-23 10:49:39', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('960', '2014-12-23 10:49:43', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('961', '2014-12-23 10:49:48', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('962', '2014-12-23 10:49:48', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('963', '2014-12-23 10:50:12', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('964', '2014-12-23 10:50:16', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('965', '2014-12-23 10:50:16', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('966', '2014-12-23 10:50:18', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('967', '2014-12-23 10:50:22', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('968', '2014-12-23 10:50:22', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('969', '2014-12-23 10:50:25', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('970', '2014-12-23 13:32:50', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('971', '2014-12-23 13:32:55', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('972', '2014-12-23 13:32:55', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('973', '2014-12-23 13:32:57', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('974', '2014-12-23 13:33:06', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('975', '2014-12-23 13:33:06', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('976', '2014-12-23 13:33:08', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('977', '2014-12-23 13:38:01', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('978', '2014-12-23 13:38:05', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('979', '2014-12-23 13:38:06', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('980', '2014-12-23 13:38:08', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('981', '2014-12-23 13:38:13', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('982', '2014-12-23 13:38:13', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('983', '2014-12-23 13:39:58', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('984', '2014-12-23 13:40:02', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('985', '2014-12-23 13:40:02', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('986', '2014-12-23 13:40:04', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('987', '2014-12-23 13:40:09', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('988', '2014-12-23 13:40:10', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('989', '2014-12-23 13:40:12', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('990', '2014-12-23 13:40:22', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('991', '2014-12-23 13:40:22', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('992', '2014-12-23 13:40:23', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('993', '2014-12-23 13:43:49', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('994', '2014-12-23 13:43:53', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('995', '2014-12-23 13:43:53', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('996', '2014-12-23 13:43:54', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('997', '2014-12-23 13:44:08', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('998', '2014-12-23 13:44:08', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('999', '2014-12-23 13:44:10', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1000', '2014-12-23 13:44:15', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1001', '2014-12-23 13:44:15', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1002', '2014-12-23 13:44:18', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1003', '2014-12-23 13:44:25', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1004', '2014-12-23 13:44:25', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1005', '2014-12-23 13:44:27', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1006', '2014-12-23 13:46:59', '2', 'Thêm mới ID#3', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1007', '2014-12-23 13:47:04', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1008', '2014-12-23 13:47:36', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1009', '2014-12-23 13:47:47', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1010', '2014-12-23 13:47:47', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1011', '2014-12-23 13:47:50', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1012', '2014-12-23 14:26:47', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1013', '2014-12-23 14:26:53', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1014', '2014-12-23 14:26:53', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1015', '2014-12-23 14:26:56', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1016', '2014-12-23 14:27:01', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1017', '2014-12-23 14:27:01', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1018', '2014-12-23 14:27:04', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1019', '2014-12-23 14:27:10', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1020', '2014-12-23 14:27:10', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1021', '2014-12-23 14:27:12', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1022', '2014-12-23 14:27:15', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1023', '2014-12-23 14:27:15', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1024', '2014-12-23 14:27:18', '3', 'Cập nhật ID#3', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1025', '2014-12-23 14:27:53', '2', 'Thêm mới ID#4', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1026', '2014-12-23 14:27:56', '3', 'Cập nhật ID#4', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1027', '2014-12-23 14:53:57', '3', 'Cập nhật ID#4', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1028', '2014-12-23 14:54:13', '2', 'Thêm mới ID#5', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1029', '2014-12-23 14:54:15', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1030', '2014-12-23 14:54:18', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1031', '2014-12-23 14:56:44', '2', 'Thêm mới ID#6', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1032', '2014-12-23 14:56:46', '3', 'Cập nhật ID#6', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1033', '2014-12-23 14:56:49', '3', 'Cập nhật ID#6', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1034', '2014-12-23 14:59:22', '3', 'Cập nhật ID#6', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1035', '2014-12-23 14:59:36', '2', 'Thêm mới ID#7', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1036', '2014-12-23 14:59:38', '3', 'Cập nhật ID#7', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1037', '2014-12-23 14:59:58', '2', 'Thêm mới ID#8', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1038', '2014-12-23 15:00:35', '2', 'Thêm mới ID#9', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1039', '2014-12-23 15:00:37', '3', 'Cập nhật ID#9', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1040', '2014-12-23 15:00:44', '3', 'Cập nhật ID#9', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1041', '2014-12-23 15:00:44', '3', 'Cập nhật ID#9', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1042', '2014-12-23 15:00:47', '3', 'Cập nhật ID#9', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1043', '2014-12-23 15:02:25', '3', 'Cập nhật ID#9', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1044', '2014-12-23 15:02:25', '3', 'Cập nhật ID#9', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1045', '2014-12-23 15:02:41', '2', 'Thêm mới ID#10', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1046', '2014-12-23 15:02:45', '3', 'Cập nhật ID#10', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1047', '2014-12-23 15:13:25', '3', 'Cập nhật ID#10', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1048', '2014-12-23 15:13:25', '3', 'Cập nhật ID#10', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1049', '2014-12-23 15:13:27', '3', 'Cập nhật ID#10', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1050', '2014-12-23 15:13:48', '3', 'Cập nhật ID#10', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1051', '2014-12-24 09:28:26', '2', 'Thêm mới ID#174', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1052', '2014-12-24 14:18:05', '2', 'Thêm mới ID#3729669', 'admincp/usersystem/create', 'User', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1053', '2014-12-24 14:36:58', '2', 'Thêm mới ID#3589841', 'admincp/user/create', 'User', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1054', '2014-12-25 10:59:57', '2', 'Thêm mới ID#1', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1055', '2014-12-25 11:44:42', '2', 'Thêm mới ID#2', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1056', '2014-12-25 11:45:21', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1057', '2014-12-25 11:46:22', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1058', '2014-12-25 11:58:26', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1059', '2014-12-25 11:59:20', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1060', '2014-12-25 11:59:30', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1061', '2014-12-25 12:00:21', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1062', '2014-12-25 12:02:47', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1063', '2014-12-25 14:21:03', '2', 'Thêm mới ID#175', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1064', '2014-12-25 14:21:17', '3', 'Cập nhật ID#175', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1065', '2014-12-25 14:21:31', '3', 'Cập nhật ID#175', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1066', '2014-12-25 14:22:50', '2', 'Thêm mới ID#1', 'admincp/language/create', 'Language', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1067', '2014-12-25 14:43:27', '2', 'Thêm mới ID#2', 'admincp/language/create', 'Language', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1068', '2014-12-25 14:44:49', '2', 'Thêm mới ID#3', 'admincp/language/create', 'Language', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1069', '2014-12-25 14:44:56', '2', 'Thêm mới ID#4', 'admincp/language/create', 'Language', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1070', '2014-12-25 14:45:25', '2', 'Thêm mới ID#5', 'admincp/language/create', 'Language', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1071', '2014-12-25 14:50:09', '2', 'Thêm mới ID#1', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1072', '2014-12-25 15:12:08', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1073', '2014-12-25 15:14:18', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1074', '2014-12-25 15:15:26', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1075', '2014-12-25 15:15:46', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1076', '2014-12-25 15:15:47', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1077', '2014-12-25 15:15:49', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1078', '2014-12-25 15:18:56', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1079', '2014-12-25 15:18:56', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1080', '2014-12-25 15:18:58', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1081', '2014-12-25 15:20:10', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1082', '2014-12-25 15:20:10', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1083', '2014-12-25 15:20:14', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1084', '2014-12-25 15:22:33', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1085', '2014-12-25 15:25:51', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1086', '2014-12-25 15:26:12', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1087', '2014-12-25 15:26:17', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1088', '2014-12-25 15:26:18', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1089', '2014-12-25 15:31:22', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1090', '2014-12-25 15:31:38', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1091', '2014-12-25 15:31:48', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1092', '2014-12-25 15:32:00', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1093', '2014-12-25 15:32:23', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1094', '2014-12-25 15:32:47', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1095', '2014-12-25 15:37:22', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1096', '2014-12-25 15:40:47', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1097', '2014-12-25 15:41:43', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1098', '2014-12-25 15:42:03', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1099', '2014-12-25 15:42:19', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1100', '2014-12-25 15:42:32', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1101', '2014-12-25 15:42:32', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1102', '2014-12-25 16:07:48', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1103', '2014-12-25 16:09:01', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1104', '2014-12-25 16:09:03', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1105', '2014-12-25 16:17:04', '2', 'Thêm mới ID#1', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1106', '2014-12-25 16:17:28', '2', 'Thêm mới ID#2', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1107', '2014-12-25 16:17:36', '2', 'Thêm mới ID#3', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1108', '2014-12-25 16:17:43', '2', 'Thêm mới ID#4', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1109', '2014-12-25 16:17:59', '2', 'Thêm mới ID#5', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1110', '2014-12-25 16:18:15', '2', 'Thêm mới ID#6', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1111', '2014-12-25 16:18:22', '3', 'Cập nhật ID#6', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1112', '2014-12-25 16:20:12', '2', 'Thêm mới ID#7', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1113', '2014-12-25 16:20:36', '2', 'Thêm mới ID#8', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1114', '2014-12-25 16:20:51', '2', 'Thêm mới ID#9', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1115', '2014-12-25 16:21:14', '2', 'Thêm mới ID#10', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1116', '2014-12-25 16:21:29', '2', 'Thêm mới ID#11', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1117', '2014-12-25 16:21:42', '3', 'Cập nhật ID#11', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1118', '2014-12-25 16:22:13', '2', 'Thêm mới ID#12', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1119', '2014-12-25 16:22:26', '2', 'Thêm mới ID#13', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1120', '2014-12-25 16:22:55', '2', 'Thêm mới ID#14', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1121', '2014-12-25 16:25:06', '2', 'Thêm mới ID#15', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1122', '2014-12-25 16:25:37', '2', 'Thêm mới ID#16', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1123', '2014-12-25 16:25:54', '2', 'Thêm mới ID#17', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1124', '2014-12-25 16:26:17', '2', 'Thêm mới ID#18', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1125', '2014-12-25 16:26:50', '2', 'Thêm mới ID#19', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1126', '2014-12-25 16:27:35', '2', 'Thêm mới ID#20', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1127', '2014-12-25 16:27:50', '3', 'Cập nhật ID#20', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1128', '2014-12-25 16:28:12', '2', 'Thêm mới ID#21', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1129', '2014-12-25 16:28:33', '2', 'Thêm mới ID#22', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1130', '2014-12-25 16:29:39', '2', 'Thêm mới ID#23', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1131', '2014-12-25 16:29:55', '2', 'Thêm mới ID#24', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1132', '2014-12-25 16:30:12', '2', 'Thêm mới ID#25', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1133', '2014-12-25 16:30:41', '2', 'Thêm mới ID#26', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1134', '2014-12-25 16:30:57', '2', 'Thêm mới ID#27', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1135', '2014-12-25 16:31:42', '2', 'Thêm mới ID#28', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1136', '2014-12-25 16:32:02', '2', 'Thêm mới ID#29', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1137', '2014-12-25 16:32:25', '2', 'Thêm mới ID#30', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1138', '2014-12-25 16:32:44', '2', 'Thêm mới ID#31', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1139', '2014-12-25 16:33:18', '3', 'Cập nhật ID#19', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1140', '2014-12-25 16:34:04', '2', 'Thêm mới ID#32', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1141', '2014-12-25 16:44:43', '2', 'Thêm mới ID#33', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1142', '2014-12-26 10:25:35', '2', 'Thêm mới ID#2', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1143', '2014-12-26 10:25:44', '3', 'Cập nhật ID#2', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1144', '2014-12-26 10:32:10', '2', 'Thêm mới ID#3', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1145', '2014-12-26 10:33:39', '2', 'Thêm mới ID#4', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1146', '2014-12-26 10:34:17', '2', 'Thêm mới ID#5', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1147', '2014-12-26 10:35:23', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1148', '2014-12-26 10:36:07', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1149', '2014-12-26 10:36:20', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1150', '2014-12-26 10:41:44', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1151', '2014-12-26 10:43:11', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1152', '2014-12-26 10:43:25', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1153', '2014-12-26 10:44:28', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1154', '2014-12-26 10:45:09', '2', 'Thêm mới ID#6', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1155', '2014-12-26 11:52:06', '2', 'Thêm mới ID#7', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1156', '2014-12-26 11:55:56', '2', 'Thêm mới ID#8', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1157', '2014-12-26 11:58:09', '2', 'Thêm mới ID#9', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1158', '2014-12-26 11:58:50', '3', 'Cập nhật ID#9', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1159', '2014-12-26 12:00:37', '2', 'Thêm mới ID#10', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1160', '2014-12-26 12:00:56', '3', 'Cập nhật ID#10', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1161', '2014-12-26 12:02:09', '2', 'Thêm mới ID#11', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1162', '2014-12-26 13:45:24', '2', 'Thêm mới ID#12', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1163', '2014-12-26 13:46:17', '2', 'Thêm mới ID#13', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1164', '2014-12-26 13:52:52', '2', 'Thêm mới ID#14', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1165', '2014-12-26 13:54:56', '2', 'Thêm mới ID#15', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1166', '2014-12-26 13:55:33', '2', 'Thêm mới ID#16', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1167', '2014-12-26 13:56:46', '2', 'Thêm mới ID#17', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1168', '2014-12-26 13:56:48', '2', 'Thêm mới ID#18', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1169', '2014-12-26 13:58:44', '2', 'Thêm mới ID#19', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1170', '2014-12-26 13:59:26', '2', 'Thêm mới ID#20', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1171', '2014-12-26 14:02:06', '2', 'Thêm mới ID#21', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1172', '2014-12-26 14:05:48', '2', 'Thêm mới ID#22', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1173', '2014-12-26 14:11:43', '2', 'Thêm mới ID#23', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1174', '2014-12-26 14:14:32', '2', 'Thêm mới ID#24', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1175', '2014-12-26 14:38:44', '2', 'Thêm mới ID#25', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1176', '2014-12-26 14:42:55', '2', 'Thêm mới ID#26', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1177', '2014-12-26 14:58:01', '2', 'Thêm mới ID#27', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1178', '2014-12-26 15:02:52', '2', 'Thêm mới ID#28', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1179', '2014-12-26 15:03:26', '2', 'Thêm mới ID#29', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1180', '2014-12-26 15:09:18', '2', 'Thêm mới ID#30', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1181', '2014-12-26 15:09:45', '2', 'Thêm mới ID#31', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1182', '2014-12-26 15:10:22', '2', 'Thêm mới ID#32', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1183', '2014-12-26 15:11:01', '2', 'Thêm mới ID#33', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1184', '2014-12-26 15:11:58', '2', 'Thêm mới ID#34', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1185', '2014-12-26 15:12:33', '4', 'Xóa dữ liệu ID#26', 'admincp/job/deleteSelected', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1186', '2014-12-26 15:13:07', '4', 'Xóa dữ liệu ID#26', 'admincp/job/delete', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1187', '2014-12-26 15:21:48', '2', 'Thêm mới ID#35', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1188', '2014-12-26 15:23:17', '2', 'Thêm mới ID#36', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1189', '2014-12-26 15:37:08', '2', 'Thêm mới ID#37', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1190', '2014-12-26 15:59:24', '2', 'Thêm mới ID#38', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1191', '2014-12-26 16:16:11', '2', 'Thêm mới ID#39', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1192', '2014-12-26 16:21:50', '2', 'Thêm mới ID#40', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1193', '2014-12-26 16:22:43', '2', 'Thêm mới ID#41', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1194', '2014-12-26 16:23:24', '2', 'Thêm mới ID#42', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1195', '2014-12-26 16:39:26', '2', 'Thêm mới ID#43', 'admincp/job/create', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1196', '2014-12-30 14:43:53', '2', 'Thêm mới ID#1', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1197', '2014-12-30 14:44:20', '2', 'Thêm mới ID#2', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1198', '2014-12-30 14:45:21', '2', 'Thêm mới ID#3', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1199', '2014-12-31 14:34:04', '2', 'Thêm mới ID#174', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1200', '2015-01-05 08:41:47', '2', 'Thêm mới ID#7', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1201', '2015-01-05 08:48:38', '3', 'Cập nhật ID#1', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1202', '2015-01-05 08:48:42', '3', 'Cập nhật ID#7', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1203', '2015-01-05 08:48:53', '3', 'Cập nhật ID#7', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1204', '2015-01-05 08:55:19', '3', 'Cập nhật ID#7', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1205', '2015-01-05 13:38:07', '3', 'Cập nhật ID#7', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1206', '2015-01-05 15:40:04', '3', 'Cập nhật ID#7', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1207', '2015-01-05 15:40:42', '3', 'Cập nhật ID#7', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1208', '2015-01-05 16:22:03', '2', 'Thêm mới ID#1', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1209', '2015-01-05 16:31:38', '2', 'Thêm mới ID#2', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1210', '2015-01-05 16:41:21', '2', 'Thêm mới ID#3', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1211', '2015-01-05 16:41:32', '2', 'Thêm mới ID#4', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1212', '2015-01-05 16:49:23', '2', 'Thêm mới ID#5', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1213', '2015-01-05 17:16:01', '2', 'Thêm mới ID#6', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1214', '2015-01-06 08:34:23', '2', 'Thêm mới ID#4', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1215', '2015-01-06 08:35:12', '2', 'Thêm mới ID#5', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1216', '2015-01-06 08:44:33', '2', 'Thêm mới ID#7', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1217', '2015-01-06 08:44:51', '2', 'Thêm mới ID#8', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1218', '2015-01-06 09:15:50', '2', 'Thêm mới ID#9', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1219', '2015-01-06 09:15:59', '2', 'Thêm mới ID#10', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1220', '2015-01-08 08:38:19', '2', 'Thêm mới ID#175', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1221', '2015-01-08 08:41:56', '2', 'Thêm mới ID#11', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1222', '2015-01-08 08:47:21', '2', 'Thêm mới ID#12', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1223', '2015-01-08 08:47:31', '2', 'Thêm mới ID#13', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1224', '2015-01-08 08:52:16', '3', 'Cập nhật ID#13', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1225', '2015-01-10 15:49:25', '2', 'Thêm mới ID#176', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1226', '2015-01-10 15:52:54', '3', 'Cập nhật ID#176', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1227', '2015-01-12 10:15:20', '2', 'Thêm mới ID#51', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1228', '2015-01-12 10:43:46', '2', 'Thêm mới ID#1', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1229', '2015-01-12 10:44:08', '2', 'Thêm mới ID#2', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1230', '2015-01-12 10:44:26', '2', 'Thêm mới ID#3', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1231', '2015-01-14 09:24:44', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1232', '2015-01-14 09:31:17', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1233', '2015-01-14 09:31:26', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1234', '2015-01-14 09:31:26', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1235', '2015-01-14 09:31:35', '3', 'Cập nhật ID#3', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1236', '2015-01-14 09:31:42', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1237', '2015-01-14 09:31:51', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1238', '2015-01-14 09:31:51', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1239', '2015-01-14 09:31:55', '3', 'Cập nhật ID#3', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1240', '2015-01-14 10:32:36', '3', 'Cập nhật ID#2', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1241', '2015-01-14 10:32:44', '3', 'Cập nhật ID#2', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1242', '2015-01-14 10:32:44', '3', 'Cập nhật ID#2', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1243', '2015-01-19 13:58:54', '2', 'Thêm mới ID#8', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1244', '2015-01-19 14:00:07', '3', 'Cập nhật ID#8', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1245', '2015-01-19 14:00:10', '3', 'Cập nhật ID#8', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1246', '2015-01-19 14:00:10', '3', 'Cập nhật ID#8', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1247', '2015-01-19 15:27:55', '2', 'Thêm mới ID#177', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1248', '2015-01-19 15:28:08', '2', 'Thêm mới ID#178', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1249', '2015-01-20 11:25:47', '3', 'Cập nhật ID#3', 'admincp/scale/update', 'Scale', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1250', '2015-01-23 11:35:59', '3', 'Cập nhật ID#5', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1251', '2015-01-23 11:38:09', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1252', '2015-01-23 11:38:23', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1253', '2015-01-23 14:28:47', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1254', '2015-01-23 15:18:33', '3', 'Cập nhật ID#20', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1255', '2015-01-23 15:44:53', '2', 'Thêm mới ID#179', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1256', '2015-01-26 08:05:08', '2', 'Thêm mới ID#34', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1257', '2015-01-26 09:50:09', '3', 'Cập nhật ID#2', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1258', '2015-01-26 10:16:25', '3', 'Cập nhật ID#159', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1259', '2015-01-26 10:20:18', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1260', '2015-01-28 10:18:20', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1261', '2015-01-28 10:39:26', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1262', '2015-01-28 10:40:17', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1263', '2015-01-28 10:40:26', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1264', '2015-01-28 10:40:28', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1265', '2015-01-28 10:40:34', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1266', '2015-01-28 10:40:44', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1267', '2015-01-28 10:40:44', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1268', '2015-01-28 10:43:45', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1269', '2015-01-28 10:43:47', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1270', '2015-01-28 10:43:47', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1271', '2015-01-28 10:43:49', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1272', '2015-01-28 10:44:00', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1273', '2015-01-28 10:44:00', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1274', '2015-01-28 10:44:02', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1275', '2015-01-28 10:44:54', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1276', '2015-01-28 10:44:58', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1277', '2015-01-28 12:00:47', '3', 'Cập nhật ID#5', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1278', '2015-01-28 15:49:33', '3', 'Cập nhật ID#15', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1279', '2015-01-28 16:04:16', '3', 'Cập nhật ID#15', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1280', '2015-01-28 16:04:31', '3', 'Cập nhật ID#15', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1281', '2015-01-30 09:36:52', '2', 'Thêm mới ID#2', 'admincp/servicePackage/create', 'ServicePackage', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1282', '2015-01-30 09:37:14', '3', 'Cập nhật ID#2', 'admincp/servicePackage/update', 'ServicePackage', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1283', '2015-01-30 10:51:03', '3', 'Cập nhật ID#2', 'admincp/servicePackage/update', 'ServicePackage', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1284', '2015-02-02 10:18:23', '3', 'Cập nhật ID#4', 'admincp/servicePackage/update', 'ServicePackage', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1285', '2015-02-02 10:18:30', '3', 'Cập nhật ID#4', 'admincp/servicePackage/update', 'ServicePackage', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1286', '2015-02-02 10:20:11', '3', 'Cập nhật ID#4', 'admincp/servicePackage/update', 'ServicePackage', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1287', '2015-02-02 10:20:11', '3', 'Cập nhật ID#4', 'admincp/servicePackage/update', 'ServicePackage', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1288', '2015-02-04 16:42:43', '3', 'Cập nhật ID#16', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1289', '2015-02-04 16:43:03', '3', 'Cập nhật ID#16', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1290', '2015-02-04 16:43:15', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1291', '2015-02-04 16:44:36', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1292', '2015-02-04 16:44:47', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1293', '2015-02-04 16:44:57', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1294', '2015-02-04 16:45:09', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1295', '2015-02-04 16:45:29', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1296', '2015-02-04 16:45:31', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1297', '2015-02-04 16:45:36', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1298', '2015-02-04 16:46:13', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1299', '2015-02-04 16:46:13', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1300', '2015-02-04 16:46:16', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1301', '2015-02-04 16:46:18', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1302', '2015-02-04 16:46:21', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1303', '2015-02-04 16:46:21', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1304', '2015-02-04 16:46:25', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1305', '2015-02-04 16:46:27', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1306', '2015-02-04 16:47:09', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1307', '2015-02-04 16:47:12', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1308', '2015-02-04 16:47:12', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1309', '2015-02-04 16:47:14', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1310', '2015-02-04 16:47:15', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1311', '2015-02-04 16:47:19', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1312', '2015-02-04 16:47:24', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1313', '2015-02-04 16:47:24', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1314', '2015-02-04 16:47:26', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1315', '2015-02-04 16:50:13', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1316', '2015-02-04 16:50:39', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1317', '2015-02-04 16:50:47', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1318', '2015-02-04 16:50:47', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1319', '2015-02-04 16:50:49', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1320', '2015-02-04 16:53:12', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1321', '2015-02-04 16:56:54', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1322', '2015-02-04 16:57:12', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1323', '2015-02-04 16:57:20', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1324', '2015-02-04 16:57:21', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1325', '2015-02-04 16:57:38', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1326', '2015-02-04 16:57:47', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1327', '2015-02-04 16:58:35', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1328', '2015-02-04 17:02:10', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1329', '2015-02-04 17:02:44', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1330', '2015-02-04 17:03:24', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1331', '2015-02-04 17:05:14', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1332', '2015-02-04 17:05:50', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1333', '2015-02-04 17:07:35', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1334', '2015-02-05 08:14:27', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1335', '2015-02-05 08:32:33', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1336', '2015-02-05 08:32:34', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1337', '2015-02-05 08:32:55', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1338', '2015-02-05 08:33:00', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1339', '2015-02-05 08:33:04', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1340', '2015-02-05 08:34:49', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1341', '2015-02-05 08:36:45', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1342', '2015-02-05 08:40:47', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1343', '2015-02-05 08:40:48', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1344', '2015-02-05 08:41:13', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1345', '2015-02-05 08:42:22', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1346', '2015-02-05 08:42:56', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1347', '2015-02-05 08:45:30', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1348', '2015-02-05 08:45:50', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1349', '2015-02-05 08:46:25', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1350', '2015-02-05 08:47:10', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1351', '2015-02-05 08:47:35', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1352', '2015-02-05 08:48:49', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1353', '2015-02-05 08:48:59', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1354', '2015-02-05 08:49:13', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1355', '2015-02-05 08:49:34', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1356', '2015-02-05 08:49:43', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1357', '2015-02-05 08:49:47', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1358', '2015-02-05 08:56:10', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1359', '2015-02-05 08:56:12', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1360', '2015-02-05 08:56:19', '3', 'Cập nhật ID#15', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1361', '2015-02-05 08:56:40', '3', 'Cập nhật ID#6', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1362', '2015-02-05 08:56:54', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1363', '2015-02-05 08:57:24', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1364', '2015-02-05 08:57:25', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1365', '2015-02-05 08:57:52', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1366', '2015-02-05 08:58:13', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1367', '2015-02-05 08:59:24', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1368', '2015-02-05 09:01:01', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1369', '2015-02-05 09:01:01', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1370', '2015-02-05 09:01:04', '3', 'Cập nhật ID#15', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1371', '2015-02-05 09:01:06', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1372', '2015-02-05 09:02:12', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1373', '2015-02-05 09:02:21', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1374', '2015-02-05 09:02:21', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1375', '2015-02-05 09:02:28', '3', 'Cập nhật ID#8', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1376', '2015-02-05 09:02:31', '3', 'Cập nhật ID#6', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1377', '2015-02-05 09:02:33', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1378', '2015-02-05 09:02:38', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1379', '2015-02-05 09:02:38', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1380', '2015-02-05 09:02:40', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1381', '2015-02-05 09:02:42', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1382', '2015-02-05 09:02:42', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1383', '2015-02-05 09:16:14', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1384', '2015-02-05 09:17:12', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1385', '2015-02-05 09:20:00', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1386', '2015-02-05 09:21:15', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1387', '2015-02-05 09:24:52', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1388', '2015-02-05 09:24:52', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1389', '2015-02-05 09:55:52', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1390', '2015-02-05 09:56:01', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1391', '2015-02-05 09:56:07', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1392', '2015-02-05 09:56:15', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1393', '2015-02-05 09:56:56', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1394', '2015-02-05 10:04:58', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1395', '2015-02-05 10:05:14', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1396', '2015-02-05 10:05:20', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1397', '2015-02-05 10:05:31', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1398', '2015-02-05 10:05:46', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1399', '2015-02-05 10:06:01', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1400', '2015-02-05 10:06:14', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1401', '2015-02-05 10:07:00', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1402', '2015-02-05 10:07:44', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1403', '2015-02-05 10:08:27', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1404', '2015-02-05 10:08:54', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1405', '2015-02-05 10:09:15', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1406', '2015-02-05 10:10:12', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1407', '2015-02-05 10:15:33', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1408', '2015-02-05 10:15:50', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1409', '2015-02-05 10:20:15', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1410', '2015-02-05 10:20:33', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1411', '2015-02-05 10:20:33', '3', 'Cập nhật ID#3', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1412', '2015-02-05 10:20:36', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1413', '2015-02-05 10:21:55', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1414', '2015-02-05 10:22:55', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1415', '2015-02-05 10:22:55', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1416', '2015-02-05 10:22:57', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1417', '2015-02-05 10:23:05', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1418', '2015-02-05 10:23:13', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1419', '2015-02-05 10:23:13', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1420', '2015-02-05 10:23:15', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1421', '2015-02-05 10:23:40', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1422', '2015-02-05 10:28:08', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1423', '2015-02-05 10:28:44', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1424', '2015-02-05 10:30:17', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1425', '2015-02-05 10:37:17', '2', 'Thêm mới ID#18', 'admincp/resumes/create', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1426', '2015-02-05 10:37:19', '3', 'Cập nhật ID#18', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1427', '2015-02-05 10:37:33', '3', 'Cập nhật ID#18', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1428', '2015-02-05 10:37:33', '3', 'Cập nhật ID#18', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1429', '2015-02-05 10:37:35', '3', 'Cập nhật ID#18', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1430', '2015-02-05 10:49:50', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1431', '2015-02-05 10:50:05', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1432', '2015-02-05 10:53:49', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1433', '2015-02-05 11:07:44', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1434', '2015-02-05 11:08:25', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1435', '2015-02-05 11:08:53', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1436', '2015-02-05 11:09:14', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1437', '2015-02-05 11:09:22', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1438', '2015-02-05 11:09:54', '3', 'Cập nhật ID#9', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1439', '2015-02-05 11:10:35', '3', 'Cập nhật ID#9', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1440', '2015-02-05 11:10:54', '3', 'Cập nhật ID#9', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1441', '2015-02-05 11:11:12', '3', 'Cập nhật ID#9', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1442', '2015-02-05 11:11:27', '3', 'Cập nhật ID#9', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1443', '2015-02-05 11:16:55', '3', 'Cập nhật ID#18', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1444', '2015-02-05 11:17:05', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1445', '2015-02-05 11:17:41', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1446', '2015-02-05 11:18:00', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1447', '2015-02-05 11:18:21', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1448', '2015-02-05 11:18:34', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1449', '2015-02-05 11:18:35', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1450', '2015-02-05 11:18:35', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1451', '2015-02-05 11:18:42', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1452', '2015-02-05 11:18:55', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1453', '2015-02-05 11:19:01', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1454', '2015-02-05 11:19:40', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1455', '2015-02-05 11:19:49', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1456', '2015-02-05 11:19:56', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1457', '2015-02-05 11:20:27', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1458', '2015-02-05 11:20:46', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1459', '2015-02-05 11:22:26', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1460', '2015-02-05 11:24:43', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1461', '2015-02-05 11:24:53', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1462', '2015-02-05 11:26:22', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1463', '2015-02-05 11:26:22', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1464', '2015-02-05 11:26:36', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1465', '2015-02-05 11:26:52', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1466', '2015-02-05 11:26:58', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1467', '2015-02-05 11:27:02', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1468', '2015-02-05 11:28:11', '3', 'Cập nhật ID#3', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1469', '2015-02-05 11:29:17', '3', 'Cập nhật ID#3', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1470', '2015-02-05 11:29:18', '3', 'Cập nhật ID#3', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1471', '2015-02-05 11:29:23', '3', 'Cập nhật ID#3', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1472', '2015-02-05 11:29:23', '3', 'Cập nhật ID#3', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1473', '2015-02-05 11:29:26', '3', 'Cập nhật ID#3', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1474', '2015-02-05 11:30:10', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1475', '2015-02-05 11:30:14', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1476', '2015-02-05 11:30:24', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1477', '2015-02-05 11:36:48', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1478', '2015-02-05 11:37:08', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1479', '2015-02-05 11:37:27', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1480', '2015-02-05 11:37:28', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1481', '2015-02-05 11:37:41', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1482', '2015-02-05 11:37:53', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1483', '2015-02-05 11:37:53', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1484', '2015-02-05 11:38:11', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1485', '2015-02-05 11:38:11', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1486', '2015-02-05 11:40:52', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1487', '2015-02-05 11:41:50', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1488', '2015-02-05 11:41:59', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1489', '2015-02-05 11:42:13', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1490', '2015-02-05 11:42:29', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1491', '2015-02-05 11:42:44', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1492', '2015-02-05 11:43:06', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1493', '2015-02-05 11:43:06', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1494', '2015-02-05 11:43:27', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1495', '2015-02-05 11:44:20', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1496', '2015-02-05 11:44:21', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1497', '2015-02-05 11:47:11', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1498', '2015-02-05 11:47:27', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1499', '2015-02-05 11:47:36', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1500', '2015-02-05 11:47:50', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1501', '2015-02-05 11:48:05', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1502', '2015-02-05 11:49:05', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1503', '2015-02-05 11:49:16', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1504', '2015-02-05 11:51:35', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1505', '2015-02-05 11:52:25', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1506', '2015-02-05 11:52:43', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1507', '2015-02-05 11:56:45', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1508', '2015-02-05 11:57:18', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1509', '2015-02-05 11:57:24', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1510', '2015-02-05 11:57:59', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1511', '2015-02-05 11:58:04', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1512', '2015-02-05 13:16:58', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1513', '2015-02-05 13:17:01', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1514', '2015-02-05 13:28:07', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1515', '2015-02-05 13:28:27', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1516', '2015-02-05 13:29:16', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1517', '2015-02-05 13:29:27', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1518', '2015-02-05 13:29:42', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1519', '2015-02-05 13:29:53', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1520', '2015-02-05 13:30:22', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1521', '2015-02-05 13:30:55', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1522', '2015-02-05 13:31:21', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1523', '2015-02-05 13:31:30', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1524', '2015-02-05 13:31:31', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1525', '2015-02-05 13:35:25', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1526', '2015-02-05 13:35:33', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1527', '2015-02-05 13:35:34', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1528', '2015-02-05 13:35:35', '3', 'Cập nhật ID#18', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1529', '2015-02-05 13:35:40', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1530', '2015-02-05 13:49:22', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1531', '2015-02-05 13:49:34', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1532', '2015-02-05 13:52:12', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1533', '2015-02-05 13:52:23', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1534', '2015-02-05 13:53:05', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1535', '2015-02-05 13:53:12', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1536', '2015-02-05 13:53:45', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1537', '2015-02-05 13:53:54', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1538', '2015-02-05 13:53:54', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1539', '2015-02-05 13:54:19', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1540', '2015-02-05 13:54:46', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1541', '2015-02-05 13:55:07', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1542', '2015-02-05 13:57:02', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1543', '2015-02-05 13:58:10', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1544', '2015-02-05 13:58:49', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1545', '2015-02-05 13:59:27', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1546', '2015-02-05 14:00:02', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1547', '2015-02-05 14:00:14', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1548', '2015-02-05 14:03:49', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1549', '2015-02-05 14:04:31', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1550', '2015-02-05 14:05:52', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1551', '2015-02-05 14:07:49', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1552', '2015-02-05 14:08:08', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1553', '2015-02-05 14:09:43', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1554', '2015-02-05 14:09:51', '2', 'Thêm mới ID#138', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1555', '2015-02-05 14:10:26', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1556', '2015-02-05 14:11:07', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1557', '2015-02-05 14:11:19', '2', 'Thêm mới ID#139', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1558', '2015-02-05 14:14:07', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1559', '2015-02-05 14:14:41', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1560', '2015-02-05 14:14:58', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1561', '2015-02-05 14:15:05', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1562', '2015-02-05 14:15:23', '2', 'Thêm mới ID#140', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1563', '2015-02-05 14:15:58', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1564', '2015-02-05 14:16:07', '2', 'Thêm mới ID#141', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1565', '2015-02-05 14:19:01', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1566', '2015-02-05 14:20:50', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1567', '2015-02-05 14:20:57', '2', 'Thêm mới ID#142', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1568', '2015-02-05 14:21:07', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1569', '2015-02-05 14:21:12', '2', 'Thêm mới ID#143', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1570', '2015-02-05 14:22:14', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1571', '2015-02-05 14:22:20', '2', 'Thêm mới ID#144', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1572', '2015-02-05 14:22:30', '2', 'Thêm mới ID#145', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1573', '2015-02-05 14:24:06', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1574', '2015-02-05 14:24:14', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1575', '2015-02-05 14:24:23', '2', 'Thêm mới ID#146', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1576', '2015-02-05 14:25:12', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1577', '2015-02-05 14:25:22', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1578', '2015-02-05 14:25:35', '2', 'Thêm mới ID#147', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1579', '2015-02-05 14:25:58', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1580', '2015-02-05 14:26:04', '2', 'Thêm mới ID#148', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1581', '2015-02-05 14:26:07', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1582', '2015-02-05 14:32:05', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1583', '2015-02-05 14:32:17', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1584', '2015-02-05 14:32:24', '2', 'Thêm mới ID#149', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1585', '2015-02-05 14:32:46', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1586', '2015-02-05 14:32:47', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1587', '2015-02-05 14:32:57', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1588', '2015-02-05 14:33:05', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1589', '2015-02-05 14:33:27', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1590', '2015-02-05 14:34:23', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1591', '2015-02-05 14:34:37', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1592', '2015-02-05 15:51:28', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1593', '2015-02-05 15:51:36', '2', 'Thêm mới ID#150', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1594', '2015-02-05 15:51:40', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1595', '2015-02-05 15:54:00', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1596', '2015-02-05 15:54:08', '2', 'Thêm mới ID#151', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1597', '2015-02-05 16:00:04', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1598', '2015-02-05 16:03:08', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1599', '2015-02-05 16:04:42', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1600', '2015-02-05 16:05:41', '3', 'Cập nhật ID#12', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1601', '2015-02-05 16:05:48', '3', 'Cập nhật ID#14', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1602', '2015-02-05 16:06:34', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1603', '2015-02-05 16:07:23', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1604', '2015-02-05 16:07:45', '2', 'Thêm mới ID#152', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1605', '2015-02-05 16:07:49', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1606', '2015-02-05 16:10:59', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1607', '2015-02-05 16:11:49', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1608', '2015-02-05 16:12:13', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1609', '2015-02-05 16:12:46', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1610', '2015-02-05 16:13:41', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1611', '2015-02-05 16:13:52', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1612', '2015-02-05 16:14:05', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1613', '2015-02-05 16:15:02', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1614', '2015-02-05 16:15:49', '2', 'Thêm mới ID#153', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1615', '2015-02-05 16:16:03', '2', 'Thêm mới ID#154', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1616', '2015-02-05 16:17:26', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1617', '2015-02-05 16:24:42', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1618', '2015-02-05 16:25:09', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1619', '2015-02-05 16:25:16', '2', 'Thêm mới ID#155', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1620', '2015-02-05 16:25:56', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1621', '2015-02-05 16:27:14', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1622', '2015-02-05 16:29:33', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1623', '2015-02-05 16:30:24', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1624', '2015-02-05 16:30:41', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1625', '2015-02-05 16:31:06', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1626', '2015-02-05 16:31:23', '2', 'Thêm mới ID#156', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1627', '2015-02-05 16:31:26', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1628', '2015-02-05 16:31:26', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1629', '2015-02-05 16:31:29', '3', 'Cập nhật ID#18', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1630', '2015-02-05 16:31:35', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1631', '2015-02-05 16:32:40', '3', 'Cập nhật ID#17', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1632', '2015-02-05 16:33:19', '3', 'Cập nhật ID#15', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1633', '2015-02-05 16:33:29', '3', 'Cập nhật ID#15', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1634', '2015-02-05 16:33:29', '3', 'Cập nhật ID#15', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1635', '2015-02-05 16:33:33', '3', 'Cập nhật ID#15', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1636', '2015-02-05 16:35:05', '2', 'Thêm mới ID#157', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1637', '2015-02-05 16:35:24', '2', 'Thêm mới ID#158', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1638', '2015-02-05 16:35:27', '2', 'Thêm mới ID#19', 'admincp/resumes/create', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1639', '2015-02-05 16:35:29', '3', 'Cập nhật ID#19', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1640', '2015-02-05 16:36:12', '3', 'Cập nhật ID#19', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1641', '2015-02-05 16:36:12', '3', 'Cập nhật ID#19', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1642', '2015-02-05 16:37:39', '2', 'Thêm mới ID#159', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1643', '2015-02-05 16:37:52', '2', 'Thêm mới ID#160', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1644', '2015-02-05 16:38:20', '2', 'Thêm mới ID#20', 'admincp/resumes/create', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1645', '2015-02-05 16:38:24', '3', 'Cập nhật ID#20', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1646', '2015-02-05 16:39:07', '3', 'Cập nhật ID#20', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1647', '2015-02-05 16:39:10', '3', 'Cập nhật ID#20', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1648', '2015-02-05 16:39:23', '3', 'Cập nhật ID#20', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1649', '2015-02-05 16:41:29', '2', 'Thêm mới ID#161', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1650', '2015-02-05 16:42:12', '2', 'Thêm mới ID#162', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1651', '2015-02-05 16:42:17', '2', 'Thêm mới ID#163', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1652', '2015-02-05 16:47:37', '2', 'Thêm mới ID#164', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1653', '2015-02-05 16:47:59', '2', 'Thêm mới ID#165', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1654', '2015-02-05 16:48:19', '2', 'Thêm mới ID#166', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1655', '2015-02-05 16:50:17', '2', 'Thêm mới ID#167', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1656', '2015-02-05 16:50:38', '2', 'Thêm mới ID#168', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1657', '2015-02-05 16:52:04', '2', 'Thêm mới ID#169', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1658', '2015-02-05 16:53:10', '2', 'Thêm mới ID#170', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1659', '2015-02-05 16:53:28', '2', 'Thêm mới ID#171', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1660', '2015-02-05 16:57:32', '2', 'Thêm mới ID#172', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1661', '2015-02-05 16:57:50', '2', 'Thêm mới ID#173', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1662', '2015-02-05 17:01:09', '2', 'Thêm mới ID#174', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1663', '2015-02-05 17:02:09', '2', 'Thêm mới ID#175', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1664', '2015-02-05 17:02:42', '2', 'Thêm mới ID#176', 'admincp/resumes/createEducation', 'Education', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1665', '2015-02-05 17:11:52', '3', 'Cập nhật ID#20', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1666', '2015-02-05 17:11:54', '3', 'Cập nhật ID#11', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1667', '2015-02-06 07:59:21', '3', 'Cập nhật ID#3', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1668', '2015-02-07 08:16:58', '3', 'Cập nhật ID#3', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1669', '2015-02-07 09:03:53', '2', 'Thêm mới ID#180', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1670', '2015-02-14 11:42:32', '2', 'Thêm mới ID#181', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1671', '2015-02-26 11:40:04', '2', 'Thêm mới ID#14', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1672', '2015-03-02 15:24:11', '3', 'Cập nhật ID#9', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1673', '2015-03-02 15:24:58', '3', 'Cập nhật ID#9', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1674', '2015-03-02 15:35:33', '3', 'Cập nhật ID#10', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1675', '2015-03-02 15:49:19', '3', 'Cập nhật ID#10', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1676', '2015-03-02 15:49:53', '3', 'Cập nhật ID#10', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1677', '2015-03-02 15:51:52', '3', 'Cập nhật ID#10', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1678', '2015-03-02 15:52:19', '3', 'Cập nhật ID#10', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1679', '2015-03-02 15:52:23', '3', 'Cập nhật ID#10', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1680', '2015-03-02 15:53:32', '3', 'Cập nhật ID#10', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1681', '2015-03-02 15:53:45', '3', 'Cập nhật ID#10', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1682', '2015-03-02 15:54:11', '3', 'Cập nhật ID#10', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1683', '2015-03-02 15:55:34', '3', 'Cập nhật ID#10', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1684', '2015-03-03 16:00:21', '2', 'Thêm mới ID#182', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1685', '2015-03-03 16:07:20', '3', 'Cập nhật ID#182', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1686', '2015-03-04 11:32:54', '3', 'Cập nhật ID#90', 'admincp/feedback/update', 'Feedback', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1687', '2015-03-04 11:33:36', '3', 'Cập nhật ID#90', 'admincp/feedback/update', 'Feedback', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1688', '2015-03-04 14:48:00', '3', 'Cập nhật ID#25', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1689', '2015-03-04 14:48:02', '3', 'Cập nhật ID#25', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1690', '2015-03-04 14:48:02', '3', 'Cập nhật ID#25', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1691', '2015-03-04 14:49:34', '3', 'Cập nhật ID#15', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1692', '2015-03-04 14:49:38', '3', 'Cập nhật ID#16', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1693', '2015-03-04 14:51:47', '3', 'Cập nhật ID#16', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1694', '2015-03-04 14:51:47', '3', 'Cập nhật ID#16', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1695', '2015-03-04 14:54:32', '3', 'Cập nhật ID#25', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1696', '2015-03-04 14:55:04', '3', 'Cập nhật ID#25', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1697', '2015-03-04 14:55:19', '3', 'Cập nhật ID#25', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1698', '2015-03-04 14:55:23', '3', 'Cập nhật ID#25', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1699', '2015-03-10 10:55:33', '2', 'Thêm mới ID#184', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1700', '2015-03-10 10:57:37', '4', 'Xóa dữ liệu ID#184', 'admincp/settings/delete', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1701', '2015-03-12 10:24:38', '3', 'Cập nhật ID#1', 'admincp/transactionHistory/update', 'TransactionHistory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1702', '2015-03-12 15:13:30', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1703', '2015-03-12 15:13:36', '3', 'Cập nhật ID#29', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1704', '2015-03-12 15:14:08', '3', 'Cập nhật ID#29', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1705', '2015-03-12 15:14:08', '3', 'Cập nhật ID#29', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1706', '2015-03-12 15:14:12', '3', 'Cập nhật ID#29', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1707', '2015-03-12 15:14:18', '3', 'Cập nhật ID#29', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1708', '2015-03-12 15:14:18', '3', 'Cập nhật ID#29', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1709', '2015-03-12 15:14:20', '3', 'Cập nhật ID#29', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1710', '2015-03-12 15:14:26', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1711', '2015-03-12 15:14:32', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1712', '2015-03-12 15:14:32', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1713', '2015-03-12 15:14:34', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1714', '2015-03-12 15:15:52', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1715', '2015-03-12 15:16:07', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1716', '2015-03-12 15:16:15', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1717', '2015-03-12 15:16:15', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1718', '2015-03-12 15:16:18', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1719', '2015-03-12 15:16:27', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1720', '2015-03-12 15:16:27', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1721', '2015-03-12 15:16:29', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1722', '2015-03-12 15:19:04', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1723', '2015-03-12 15:19:11', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1724', '2015-03-12 15:19:11', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1725', '2015-03-12 15:19:13', '3', 'Cập nhật ID#31', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1726', '2015-03-12 15:19:30', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1727', '2015-03-12 15:19:39', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1728', '2015-03-12 15:19:39', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1729', '2015-03-12 15:19:42', '3', 'Cập nhật ID#30', 'admincp/resumes/update', 'Resumes', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1730', '2015-03-14 08:49:51', '2', 'Thêm mới ID#52', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1731', '2015-03-18 11:49:59', '2', 'Thêm mới ID#184', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1732', '2015-03-18 15:45:57', '2', 'Thêm mới ID#185', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1733', '2015-03-20 15:51:25', '3', 'Cập nhật ID#181', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1734', '2015-03-20 16:31:11', '2', 'Thêm mới ID#53', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1735', '2015-03-20 16:31:30', '2', 'Thêm mới ID#54', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1736', '2015-03-21 08:43:44', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1737', '2015-03-21 08:43:47', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1738', '2015-03-21 08:44:07', '3', 'Cập nhật ID#1', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1739', '2015-03-21 12:15:44', '2', 'Thêm mới ID#186', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1740', '2015-03-21 12:22:50', '3', 'Cập nhật ID#186', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1741', '2015-03-23 09:16:13', '2', 'Thêm mới ID#55', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1742', '2015-03-23 11:07:37', '3', 'Cập nhật ID#44', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1743', '2015-03-23 11:07:45', '3', 'Cập nhật ID#44', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1744', '2015-03-23 11:07:45', '3', 'Cập nhật ID#44', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1745', '2015-03-24 10:27:17', '2', 'Thêm mới ID#56', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1746', '2015-03-24 11:01:12', '3', 'Cập nhật ID#158', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1747', '2015-03-24 11:01:29', '2', 'Thêm mới ID#187', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1748', '2015-03-24 15:06:59', '3', 'Cập nhật ID#159', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1749', '2015-03-24 15:29:43', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1750', '2015-03-24 15:30:11', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1751', '2015-03-24 15:30:13', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1752', '2015-03-24 15:30:31', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1753', '2015-03-24 15:30:48', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1754', '2015-03-24 15:38:15', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1755', '2015-03-24 15:38:18', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1756', '2015-03-24 15:38:18', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1757', '2015-03-24 15:38:24', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1758', '2015-03-24 15:39:40', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1759', '2015-03-24 15:39:51', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1760', '2015-03-25 14:15:22', '2', 'Thêm mới ID#15', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1761', '2015-03-25 14:15:46', '2', 'Thêm mới ID#16', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1762', '2015-03-25 14:15:51', '3', 'Cập nhật ID#15', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1763', '2015-03-25 15:14:01', '3', 'Cập nhật ID#182', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1764', '2015-03-25 15:28:41', '3', 'Cập nhật ID#182', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1765', '2015-03-26 14:06:37', '2', 'Thêm mới ID#188', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1766', '2015-03-26 14:07:25', '3', 'Cập nhật ID#186', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1767', '2015-03-26 14:21:26', '2', 'Thêm mới ID#189', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1768', '2015-03-28 09:02:55', '2', 'Thêm mới ID#9', 'admincp/jobCategory/create', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1769', '2015-03-28 09:04:09', '3', 'Cập nhật ID#2', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1770', '2015-03-28 09:04:25', '3', 'Cập nhật ID#44', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1771', '2015-03-28 09:04:56', '3', 'Cập nhật ID#44', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1772', '2015-03-28 09:04:56', '3', 'Cập nhật ID#44', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1773', '2015-03-28 09:04:58', '3', 'Cập nhật ID#44', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1774', '2015-03-28 09:05:06', '3', 'Cập nhật ID#2', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1775', '2015-03-28 09:05:07', '3', 'Cập nhật ID#2', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1776', '2015-03-28 09:05:14', '3', 'Cập nhật ID#2', 'admincp/jobCategory/update', 'JobCategory', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1777', '2015-03-28 09:33:55', '2', 'Thêm mới ID#2957549', 'admincp/user/create', 'User', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1778', '2015-03-28 09:35:15', '2', 'Thêm mới ID#1306092', 'admincp/user/create', 'User', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1779', '2015-03-28 09:35:31', '2', 'Thêm mới ID#6567749', 'admincp/user/create', 'User', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1780', '2015-03-30 09:32:29', '2', 'Thêm mới ID#17', 'admincp/carrerCategory/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1781', '2015-03-30 09:33:30', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1782', '2015-03-30 09:33:32', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1783', '2015-03-30 09:33:32', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1784', '2015-03-30 09:33:37', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1785', '2015-03-30 09:33:47', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1786', '2015-03-30 09:33:47', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1787', '2015-03-30 09:33:49', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1788', '2015-03-30 09:34:08', '2', 'Thêm mới ID#18', 'admincp/carrerCategory/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1789', '2015-03-30 09:34:48', '3', 'Cập nhật ID#18', 'admincp/carrerCategory/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1790', '2015-03-30 09:34:52', '3', 'Cập nhật ID#18', 'admincp/carrerCategory/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1791', '2015-03-30 09:34:52', '3', 'Cập nhật ID#18', 'admincp/carrerCategory/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1792', '2015-03-30 10:17:23', '2', 'Thêm mới ID#8', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1793', '2015-03-30 13:40:25', '2', 'Thêm mới ID#190', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1794', '2015-03-30 13:45:59', '3', 'Cập nhật ID#190', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1795', '2015-03-30 13:46:24', '3', 'Cập nhật ID#190', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1796', '2015-03-30 16:38:53', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1797', '2015-03-30 16:39:21', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1798', '2015-03-30 16:39:43', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1799', '2015-03-31 12:04:57', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1800', '2015-04-02 08:31:49', '3', 'Cập nhật ID#181', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1801', '2015-04-02 08:32:07', '3', 'Cập nhật ID#159', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1802', '2015-04-03 11:08:08', '2', 'Thêm mới ID#191', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1803', '2015-04-03 11:09:00', '3', 'Cập nhật ID#191', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1804', '2015-04-04 10:04:34', '3', 'Cập nhật ID#181', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1805', '2015-04-04 15:32:55', '3', 'Cập nhật ID#181', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1806', '2015-04-04 15:41:57', '3', 'Cập nhật ID#181', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1807', '2015-04-06 08:32:18', '3', 'Cập nhật ID#12', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1808', '2015-04-06 09:12:17', '3', 'Cập nhật ID#12', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1809', '2015-04-07 14:12:16', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1810', '2015-04-07 14:12:21', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1811', '2015-04-07 14:12:26', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1812', '2015-04-07 14:12:38', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1813', '2015-04-07 14:12:39', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1814', '2015-04-07 14:12:40', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1815', '2015-04-07 14:12:49', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1816', '2015-04-07 14:13:12', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1817', '2015-04-07 14:13:42', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1818', '2015-04-07 14:14:20', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1819', '2015-04-07 14:14:23', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1820', '2015-04-07 14:14:43', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1821', '2015-04-07 14:14:45', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1822', '2015-04-07 14:14:48', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1823', '2015-04-07 14:14:48', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1824', '2015-04-07 14:14:50', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1825', '2015-04-07 14:14:57', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1826', '2015-04-07 14:20:42', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1827', '2015-04-07 14:20:59', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1828', '2015-04-07 14:20:59', '3', 'Cập nhật ID#45', 'admincp/job/update', 'Job', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1829', '2015-04-07 14:28:34', '3', 'Cập nhật ID#96', 'admincp/feedback/update', 'Feedback', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1830', '2015-04-07 15:09:35', '3', 'Cập nhật ID#54', 'admincp/advs/update', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1831', '2015-04-07 15:21:01', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1832', '2015-04-07 15:22:16', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1833', '2015-04-07 15:22:17', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1834', '2015-04-07 15:25:52', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1835', '2015-04-07 15:26:37', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1836', '2015-04-07 15:28:31', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1837', '2015-04-07 15:34:27', '3', 'Cập nhật ID#12', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1838', '2015-04-07 15:41:23', '3', 'Cập nhật ID#12', 'admincp/alertJob/update', 'AlertJob', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1839', '2015-04-07 15:50:48', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1840', '2015-04-07 15:51:45', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1841', '2015-04-07 15:51:47', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1842', '2015-04-07 15:51:47', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1843', '2015-04-07 16:41:34', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1844', '2015-04-07 16:41:40', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1845', '2015-04-07 16:41:40', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1846', '2015-04-07 16:41:55', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1847', '2015-04-07 16:41:58', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1848', '2015-04-07 16:42:19', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1849', '2015-04-07 16:42:19', '3', 'Cập nhật ID#8', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1850', '2015-04-08 11:32:00', '2', 'Thêm mới ID#192', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1851', '2015-04-09 09:08:19', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1852', '2015-04-09 09:08:41', '3', 'Cập nhật ID#9', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1853', '2015-04-09 09:08:58', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1854', '2015-04-09 09:11:41', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1855', '2015-04-09 09:11:41', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1856', '2015-05-27 18:44:06', '4', 'Xóa dữ liệu ID#9', 'admincp/node/delete', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1857', '2015-05-27 18:44:54', '4', 'Xóa dữ liệu ID#10', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1858', '2015-05-27 18:44:54', '4', 'Xóa dữ liệu ID#11', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1859', '2015-05-27 18:44:54', '4', 'Xóa dữ liệu ID#12', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1860', '2015-05-27 18:44:54', '4', 'Xóa dữ liệu ID#13', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1861', '2015-05-27 18:44:54', '4', 'Xóa dữ liệu ID#14', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1862', '2015-05-27 18:44:54', '4', 'Xóa dữ liệu ID#15', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1863', '2015-05-27 18:44:55', '4', 'Xóa dữ liệu ID#20', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1864', '2015-05-27 18:44:55', '4', 'Xóa dữ liệu ID#26', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1865', '2015-05-27 18:44:55', '4', 'Xóa dữ liệu ID#27', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1866', '2015-05-27 18:44:55', '4', 'Xóa dữ liệu ID#28', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1867', '2015-05-27 18:44:55', '4', 'Xóa dữ liệu ID#29', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1868', '2015-05-27 18:44:55', '4', 'Xóa dữ liệu ID#31', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1869', '2015-05-27 18:44:55', '4', 'Xóa dữ liệu ID#32', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1870', '2015-05-27 18:44:55', '4', 'Xóa dữ liệu ID#33', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1871', '2015-05-27 18:45:48', '3', 'Cập nhật ID#8', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1872', '2015-05-27 21:46:04', '2', 'Thêm mới ID#1', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1873', '2015-05-27 21:49:48', '2', 'Thêm mới ID#2', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1874', '2015-05-27 22:27:20', '2', 'Thêm mới ID#1', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1875', '2015-05-27 22:28:17', '2', 'Thêm mới ID#2', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1876', '2015-05-27 22:28:30', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1877', '2015-05-27 22:28:51', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1878', '2015-05-27 22:29:21', '2', 'Thêm mới ID#3', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1879', '2015-05-27 22:33:49', '2', 'Thêm mới ID#4', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1880', '2015-05-27 22:33:54', '3', 'Cập nhật ID#4', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1881', '2015-05-27 22:33:57', '3', 'Cập nhật ID#4', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1882', '2015-05-27 22:33:58', '3', 'Cập nhật ID#4', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1883', '2015-05-27 22:34:22', '2', 'Thêm mới ID#5', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1884', '2015-05-27 22:38:16', '2', 'Thêm mới ID#6', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1885', '2015-05-27 22:39:02', '2', 'Thêm mới ID#7', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1886', '2015-05-27 22:39:24', '2', 'Thêm mới ID#8', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1887', '2015-05-27 22:39:51', '2', 'Thêm mới ID#9', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1888', '2015-05-27 22:40:20', '2', 'Thêm mới ID#10', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1889', '2015-05-27 22:40:38', '3', 'Cập nhật ID#10', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1890', '2015-05-27 22:40:43', '3', 'Cập nhật ID#10', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1891', '2015-05-27 22:40:43', '3', 'Cập nhật ID#10', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1892', '2015-05-27 22:40:58', '2', 'Thêm mới ID#11', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1893', '2015-05-27 22:41:55', '2', 'Thêm mới ID#12', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1894', '2015-05-27 22:42:34', '2', 'Thêm mới ID#13', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1895', '2015-05-27 22:42:56', '2', 'Thêm mới ID#14', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1896', '2015-05-28 20:38:31', '2', 'Thêm mới ID#15', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1897', '2015-05-28 20:46:54', '2', 'Thêm mới ID#16', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1898', '2015-05-28 20:47:25', '2', 'Thêm mới ID#17', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1899', '2015-05-28 20:47:43', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1900', '2015-05-28 20:52:54', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1901', '2015-05-28 20:56:50', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1902', '2015-05-28 20:57:28', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1903', '2015-05-28 20:57:48', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1904', '2015-05-28 21:05:57', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1905', '2015-05-28 21:06:17', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1906', '2015-05-28 21:09:51', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1907', '2015-05-28 21:11:22', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1908', '2015-05-28 21:14:12', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1909', '2015-05-28 21:14:26', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1910', '2015-05-28 21:16:37', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1911', '2015-05-28 21:17:01', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1912', '2015-05-28 21:25:30', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1913', '2015-05-28 21:26:40', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1914', '2015-05-28 21:26:57', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1915', '2015-05-28 21:31:26', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1916', '2015-05-28 21:32:55', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1917', '2015-05-28 21:34:13', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1918', '2015-05-28 21:34:54', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1919', '2015-05-28 21:35:17', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1920', '2015-05-28 21:35:42', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1921', '2015-05-28 22:39:43', '3', 'Cập nhật ID#23', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1922', '2015-05-28 22:40:02', '3', 'Cập nhật ID#24', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1923', '2015-05-28 23:48:11', '2', 'Thêm mới ID#18', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1924', '2015-05-28 23:49:03', '2', 'Thêm mới ID#19', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1925', '2015-05-29 21:33:47', '2', 'Thêm mới ID#35', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1926', '2015-05-29 21:34:34', '3', 'Cập nhật ID#35', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1927', '2015-05-29 21:41:30', '2', 'Thêm mới ID#1', 'admincp/testimonial/create', 'Testimonial', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1928', '2015-05-29 21:42:25', '3', 'Cập nhật ID#1', 'admincp/testimonial/update', 'Testimonial', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1929', '2015-05-29 21:43:13', '3', 'Cập nhật ID#1', 'admincp/testimonial/update', 'Testimonial', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1930', '2015-05-29 21:43:26', '3', 'Cập nhật ID#1', 'admincp/testimonial/update', 'Testimonial', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1931', '2015-05-29 21:56:48', '2', 'Thêm mới ID#2', 'admincp/testimonial/create', 'Testimonial', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1932', '2015-05-30 06:52:30', '2', 'Thêm mới ID#1', 'admincp/member/create', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1933', '2015-05-30 06:52:35', '3', 'Cập nhật ID#1', 'admincp/member/update', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1934', '2015-05-30 06:53:32', '2', 'Thêm mới ID#2', 'admincp/member/create', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1935', '2015-05-30 06:54:03', '2', 'Thêm mới ID#3', 'admincp/member/create', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1936', '2015-05-30 06:54:40', '2', 'Thêm mới ID#4', 'admincp/member/create', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1937', '2015-05-30 07:07:02', '3', 'Cập nhật ID#4', 'admincp/member/update', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1938', '2015-05-30 07:07:06', '3', 'Cập nhật ID#4', 'admincp/member/update', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1939', '2015-05-30 07:07:06', '3', 'Cập nhật ID#4', 'admincp/member/update', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1940', '2015-05-30 08:00:47', '3', 'Cập nhật ID#4', 'admincp/member/update', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1941', '2015-05-30 08:00:52', '3', 'Cập nhật ID#1', 'admincp/member/update', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1942', '2015-05-30 08:01:01', '3', 'Cập nhật ID#1', 'admincp/member/update', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1943', '2015-05-30 08:01:01', '3', 'Cập nhật ID#1', 'admincp/member/update', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1944', '2015-05-30 08:01:36', '3', 'Cập nhật ID#1', 'admincp/member/update', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1945', '2015-05-30 08:28:35', '4', 'Xóa dữ liệu ID#221', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1946', '2015-05-30 08:28:35', '4', 'Xóa dữ liệu ID#220', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1947', '2015-05-30 08:28:35', '4', 'Xóa dữ liệu ID#217', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1948', '2015-05-30 08:28:35', '4', 'Xóa dữ liệu ID#216', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1949', '2015-05-30 08:28:35', '4', 'Xóa dữ liệu ID#215', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1950', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#214', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1951', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#213', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1952', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#212', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1953', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#211', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1954', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#210', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1955', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#206', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1956', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#205', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1957', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#204', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1958', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#203', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1959', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#202', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1960', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#201', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1961', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#200', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1962', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#199', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1963', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#198', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1964', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#197', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1965', '2015-05-30 08:29:26', '4', 'Xóa dữ liệu ID#196', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1966', '2015-05-30 08:29:27', '4', 'Xóa dữ liệu ID#195', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1967', '2015-05-30 08:29:27', '4', 'Xóa dữ liệu ID#194', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1968', '2015-05-30 08:29:27', '4', 'Xóa dữ liệu ID#193', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1969', '2015-05-30 08:29:27', '4', 'Xóa dữ liệu ID#191', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1970', '2015-05-30 08:29:27', '4', 'Xóa dữ liệu ID#190', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1971', '2015-05-30 08:29:27', '4', 'Xóa dữ liệu ID#189', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1972', '2015-05-30 08:29:27', '4', 'Xóa dữ liệu ID#188', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1973', '2015-05-30 08:29:27', '4', 'Xóa dữ liệu ID#187', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1974', '2015-05-30 08:29:27', '4', 'Xóa dữ liệu ID#186', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1975', '2015-05-30 08:30:14', '4', 'Xóa dữ liệu ID#209', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1976', '2015-05-30 08:30:14', '4', 'Xóa dữ liệu ID#185', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1977', '2015-05-30 08:30:14', '4', 'Xóa dữ liệu ID#184', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1978', '2015-05-30 08:30:14', '4', 'Xóa dữ liệu ID#183', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1979', '2015-05-30 08:30:14', '4', 'Xóa dữ liệu ID#182', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1980', '2015-05-30 08:30:14', '4', 'Xóa dữ liệu ID#181', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1981', '2015-05-30 08:30:14', '4', 'Xóa dữ liệu ID#180', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1982', '2015-05-30 08:30:14', '4', 'Xóa dữ liệu ID#179', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1983', '2015-05-30 08:30:14', '4', 'Xóa dữ liệu ID#178', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1984', '2015-05-30 08:30:14', '4', 'Xóa dữ liệu ID#177', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1985', '2015-05-30 08:30:14', '4', 'Xóa dữ liệu ID#176', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1986', '2015-05-30 08:30:14', '4', 'Xóa dữ liệu ID#175', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1987', '2015-05-30 08:30:14', '4', 'Xóa dữ liệu ID#174', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1988', '2015-05-30 08:31:45', '2', 'Thêm mới ID#223', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1989', '2015-05-30 08:32:23', '2', 'Thêm mới ID#224', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1990', '2015-05-30 08:32:51', '3', 'Cập nhật ID#223', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1991', '2015-05-30 08:33:48', '3', 'Cập nhật ID#223', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1992', '2015-05-30 08:33:52', '3', 'Cập nhật ID#224', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1993', '2015-05-30 08:37:21', '3', 'Cập nhật ID#7', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1994', '2015-05-30 08:47:15', '3', 'Cập nhật ID#7', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1995', '2015-05-30 08:57:48', '3', 'Cập nhật ID#155', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1996', '2015-05-30 08:58:25', '3', 'Cập nhật ID#154', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1997', '2015-05-30 08:58:54', '3', 'Cập nhật ID#155', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1998', '2015-05-30 09:00:26', '2', 'Thêm mới ID#225', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('1999', '2015-05-30 09:00:54', '2', 'Thêm mới ID#226', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2000', '2015-05-30 09:03:13', '2', 'Thêm mới ID#227', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2001', '2015-05-30 09:03:32', '2', 'Thêm mới ID#228', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2002', '2015-05-30 09:40:00', '3', 'Cập nhật ID#11', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2003', '2015-05-30 09:40:18', '3', 'Cập nhật ID#11', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2004', '2015-05-30 09:40:18', '3', 'Cập nhật ID#11', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2005', '2015-05-30 09:40:21', '3', 'Cập nhật ID#11', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2006', '2015-05-30 09:40:23', '3', 'Cập nhật ID#11', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2007', '2015-05-30 09:40:23', '3', 'Cập nhật ID#11', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2008', '2015-05-30 09:40:25', '3', 'Cập nhật ID#11', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2009', '2015-05-30 09:40:36', '3', 'Cập nhật ID#7', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2010', '2015-05-30 09:40:36', '3', 'Cập nhật ID#8', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2011', '2015-05-30 09:40:37', '3', 'Cập nhật ID#9', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2012', '2015-05-30 09:40:37', '3', 'Cập nhật ID#10', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2013', '2015-05-30 09:40:38', '3', 'Cập nhật ID#11', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2014', '2015-05-30 09:41:20', '3', 'Cập nhật ID#9', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2015', '2015-05-30 09:41:27', '3', 'Cập nhật ID#9', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2016', '2015-05-30 09:41:27', '3', 'Cập nhật ID#9', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2017', '2015-05-30 09:41:29', '3', 'Cập nhật ID#10', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2018', '2015-05-30 09:41:29', '3', 'Cập nhật ID#10', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2019', '2015-05-30 14:49:19', '3', 'Cập nhật ID#8', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2020', '2015-05-30 14:49:36', '3', 'Cập nhật ID#8', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2021', '2015-05-30 14:50:48', '3', 'Cập nhật ID#8', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2022', '2015-05-30 14:50:48', '3', 'Cập nhật ID#8', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2023', '2015-05-30 15:14:28', '2', 'Thêm mới ID#20', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2024', '2015-05-30 15:14:45', '2', 'Thêm mới ID#21', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2025', '2015-05-30 15:19:00', '2', 'Thêm mới ID#1', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2026', '2015-05-30 15:31:39', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2027', '2015-05-30 15:32:16', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2028', '2015-05-30 15:32:17', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2029', '2015-05-30 21:56:56', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2030', '2015-05-30 21:57:22', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2031', '2015-05-30 21:57:22', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2032', '2015-05-30 22:15:13', '2', 'Thêm mới ID#1', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2033', '2015-05-30 22:22:25', '2', 'Thêm mới ID#2', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2034', '2015-05-31 07:33:58', '2', 'Thêm mới ID#36', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2035', '2015-05-31 07:45:57', '2', 'Thêm mới ID#22', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2036', '2015-05-31 07:46:21', '2', 'Thêm mới ID#23', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2037', '2015-05-31 07:47:14', '2', 'Thêm mới ID#24', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2038', '2015-05-31 07:50:45', '2', 'Thêm mới ID#1', 'admincp/gallery/create', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2039', '2015-05-31 07:53:06', '2', 'Thêm mới ID#2', 'admincp/gallery/create', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2040', '2015-05-31 07:54:42', '2', 'Thêm mới ID#3', 'admincp/gallery/create', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2041', '2015-05-31 07:55:08', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2042', '2015-05-31 07:55:22', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2043', '2015-05-31 07:55:40', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2044', '2015-05-31 07:55:47', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2045', '2015-05-31 07:55:47', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2046', '2015-05-31 07:55:53', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2047', '2015-05-31 07:55:57', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2048', '2015-05-31 07:56:01', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2049', '2015-05-31 07:56:01', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2050', '2015-05-31 08:00:01', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2051', '2015-05-31 08:00:03', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2052', '2015-05-31 08:00:03', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2053', '2015-05-31 08:00:52', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2054', '2015-05-31 08:00:54', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2055', '2015-05-31 08:00:54', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2056', '2015-05-31 08:01:44', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2057', '2015-05-31 08:01:48', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2058', '2015-05-31 08:01:48', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2059', '2015-05-31 08:01:51', '3', 'Cập nhật ID#2', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2060', '2015-05-31 08:01:59', '3', 'Cập nhật ID#2', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2061', '2015-05-31 08:01:59', '3', 'Cập nhật ID#2', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2062', '2015-05-31 08:05:24', '3', 'Cập nhật ID#1', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2063', '2015-05-31 08:05:37', '3', 'Cập nhật ID#2', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2064', '2015-05-31 08:06:03', '3', 'Cập nhật ID#3', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2065', '2015-05-31 08:08:40', '2', 'Thêm mới ID#229', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2066', '2015-05-31 08:09:15', '2', 'Thêm mới ID#230', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2067', '2015-05-31 09:42:24', '3', 'Cập nhật ID#5', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2068', '2015-05-31 09:42:29', '3', 'Cập nhật ID#5', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2069', '2015-05-31 09:42:29', '3', 'Cập nhật ID#5', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2070', '2015-05-31 12:06:21', '3', 'Cập nhật ID#5', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2071', '2015-05-31 12:06:54', '3', 'Cập nhật ID#23', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2072', '2015-05-31 12:07:06', '3', 'Cập nhật ID#24', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2073', '2015-05-31 15:09:00', '2', 'Thêm mới ID#231', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2074', '2015-05-31 15:09:33', '4', 'Xóa dữ liệu ID#147', 'admincp/settings/delete', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2075', '2015-05-31 15:09:45', '4', 'Xóa dữ liệu ID#158', 'admincp/settings/delete', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2076', '2015-05-31 15:09:49', '4', 'Xóa dữ liệu ID#207', 'admincp/settings/delete', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2077', '2015-05-31 15:10:14', '4', 'Xóa dữ liệu ID#148', 'admincp/settings/delete', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2078', '2015-05-31 15:11:06', '2', 'Thêm mới ID#232', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2079', '2015-05-31 15:12:14', '2', 'Thêm mới ID#233', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2080', '2015-05-31 15:13:03', '2', 'Thêm mới ID#234', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2081', '2015-05-31 15:17:09', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2082', '2015-05-31 15:18:37', '3', 'Cập nhật ID#162', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2083', '2015-05-31 15:19:40', '3', 'Cập nhật ID#22', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2084', '2015-05-31 15:20:29', '3', 'Cập nhật ID#21', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2085', '2015-05-31 15:20:54', '3', 'Cập nhật ID#25', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2086', '2015-05-31 15:22:53', '2', 'Thêm mới ID#235', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2087', '2015-05-31 15:23:41', '2', 'Thêm mới ID#236', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2088', '2015-05-31 15:24:19', '2', 'Thêm mới ID#237', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2089', '2015-05-31 15:25:28', '2', 'Thêm mới ID#238', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2090', '2015-05-31 15:25:58', '3', 'Cập nhật ID#238', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2091', '2015-06-01 16:42:50', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2092', '2015-06-01 22:22:03', '3', 'Cập nhật ID#1', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2093', '2015-06-01 22:35:56', '2', 'Thêm mới ID#239', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2094', '2015-06-01 22:36:24', '3', 'Cập nhật ID#239', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2095', '2015-06-01 22:37:22', '2', 'Thêm mới ID#240', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2096', '2015-06-01 22:37:55', '3', 'Cập nhật ID#240', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2097', '2015-06-01 22:39:18', '2', 'Thêm mới ID#241', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2098', '2015-06-01 22:39:44', '2', 'Thêm mới ID#242', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2099', '2015-06-01 22:43:36', '2', 'Thêm mới ID#243', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2100', '2015-06-01 22:43:59', '2', 'Thêm mới ID#244', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2101', '2015-06-01 22:57:04', '2', 'Thêm mới ID#245', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2102', '2015-06-01 23:01:48', '3', 'Cập nhật ID#245', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2103', '2015-06-01 23:03:17', '2', 'Thêm mới ID#25', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2104', '2015-06-01 23:03:47', '2', 'Thêm mới ID#26', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2105', '2015-06-01 23:04:24', '2', 'Thêm mới ID#27', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2106', '2015-06-01 23:04:40', '2', 'Thêm mới ID#28', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2107', '2015-06-01 23:04:53', '2', 'Thêm mới ID#29', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2108', '2015-06-01 23:04:58', '3', 'Cập nhật ID#29', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2109', '2015-06-01 23:05:01', '3', 'Cập nhật ID#29', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2110', '2015-06-01 23:05:01', '3', 'Cập nhật ID#29', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2111', '2015-06-01 23:05:13', '2', 'Thêm mới ID#30', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2112', '2015-06-01 23:05:43', '2', 'Thêm mới ID#31', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2113', '2015-06-01 23:07:13', '2', 'Thêm mới ID#32', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2114', '2015-06-01 23:08:05', '2', 'Thêm mới ID#33', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2115', '2015-06-01 23:13:37', '3', 'Cập nhật ID#31', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2116', '2015-06-01 23:13:45', '3', 'Cập nhật ID#31', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2117', '2015-06-01 23:13:45', '3', 'Cập nhật ID#31', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2118', '2015-06-01 23:14:13', '3', 'Cập nhật ID#31', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2119', '2015-06-01 23:14:23', '3', 'Cập nhật ID#31', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2120', '2015-06-01 23:14:23', '3', 'Cập nhật ID#31', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2121', '2015-06-02 13:36:40', '2', 'Thêm mới ID#2', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2122', '2015-06-02 13:36:43', '3', 'Cập nhật ID#2', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2123', '2015-06-02 13:36:46', '3', 'Cập nhật ID#2', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2124', '2015-06-02 13:36:46', '3', 'Cập nhật ID#2', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2125', '2015-06-02 13:39:49', '2', 'Thêm mới ID#3', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2126', '2015-06-02 13:41:54', '2', 'Thêm mới ID#4', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2127', '2015-06-02 13:41:58', '3', 'Cập nhật ID#2', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2128', '2015-06-02 13:42:22', '3', 'Cập nhật ID#2', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2129', '2015-06-02 13:42:23', '3', 'Cập nhật ID#2', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2130', '2015-06-02 13:42:50', '3', 'Cập nhật ID#2', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2131', '2015-06-02 13:42:55', '3', 'Cập nhật ID#18', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2132', '2015-06-02 13:42:59', '3', 'Cập nhật ID#17', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2133', '2015-06-02 14:10:59', '2', 'Thêm mới ID#246', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2134', '2015-06-02 14:11:25', '2', 'Thêm mới ID#247', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2135', '2015-06-02 14:18:50', '2', 'Thêm mới ID#248', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2136', '2015-06-02 14:19:04', '2', 'Thêm mới ID#249', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2137', '2015-06-02 15:27:26', '3', 'Cập nhật ID#1', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2138', '2015-06-02 15:27:53', '2', 'Thêm mới ID#37', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2139', '2015-06-03 10:39:55', '3', 'Cập nhật ID#1', 'admincp/booking/update', 'Booking', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2140', '2015-06-03 14:10:09', '3', 'Cập nhật ID#173', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2141', '2015-06-03 16:43:49', '3', 'Cập nhật ID#4', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2142', '2015-06-03 16:43:50', '3', 'Cập nhật ID#2', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2143', '2015-06-03 16:43:53', '3', 'Cập nhật ID#4', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2144', '2015-06-03 16:43:53', '3', 'Cập nhật ID#4', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2145', '2015-06-03 16:43:57', '3', 'Cập nhật ID#2', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2146', '2015-06-03 16:43:57', '3', 'Cập nhật ID#2', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2147', '2015-06-03 21:51:16', '2', 'Thêm mới ID#250', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2148', '2015-06-03 22:05:41', '2', 'Thêm mới ID#38', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2149', '2015-06-03 22:12:54', '3', 'Cập nhật ID#3', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2150', '2015-06-03 22:14:42', '2', 'Thêm mới ID#5', 'admincp/postIndex/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2151', '2015-06-03 22:18:38', '3', 'Cập nhật ID#5', 'admincp/postIndex/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2152', '2015-06-03 22:18:48', '3', 'Cập nhật ID#5', 'admincp/postIndex/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2153', '2015-06-03 22:18:48', '3', 'Cập nhật ID#5', 'admincp/postIndex/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2154', '2015-06-03 22:25:28', '3', 'Cập nhật ID#5', 'admincp/postIndex/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2155', '2015-06-03 22:25:42', '3', 'Cập nhật ID#5', 'admincp/postIndex/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2156', '2015-06-03 22:25:42', '3', 'Cập nhật ID#5', 'admincp/postIndex/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2157', '2015-06-03 22:27:44', '2', 'Thêm mới ID#6', 'admincp/postIndex/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2158', '2015-06-03 22:35:11', '3', 'Cập nhật ID#6', 'admincp/postIndex/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2159', '2015-06-03 22:35:35', '3', 'Cập nhật ID#6', 'admincp/postIndex/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2160', '2015-06-03 22:35:35', '3', 'Cập nhật ID#6', 'admincp/postIndex/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2161', '2015-06-03 22:42:23', '2', 'Thêm mới ID#251', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2162', '2015-06-03 22:43:03', '3', 'Cập nhật ID#251', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2163', '2015-06-16 09:07:03', '3', 'Cập nhật ID#22', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2164', '2015-06-21 21:20:51', '3', 'Cập nhật ID#1', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2165', '2015-06-21 21:20:57', '3', 'Cập nhật ID#1', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2166', '2015-06-21 21:20:57', '3', 'Cập nhật ID#1', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2167', '2015-06-23 21:43:15', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2168', '2015-06-23 21:43:16', '3', 'Cập nhật ID#26', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2169', '2015-06-23 21:43:27', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2170', '2015-06-23 21:43:27', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2171', '2015-06-23 21:43:30', '3', 'Cập nhật ID#26', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2172', '2015-06-23 21:43:30', '3', 'Cập nhật ID#26', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2173', '2015-06-23 22:04:38', '3', 'Cập nhật ID#4', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2174', '2015-06-23 22:04:44', '3', 'Cập nhật ID#4', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2175', '2015-06-23 22:04:44', '3', 'Cập nhật ID#4', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2176', '2015-06-23 22:08:42', '2', 'Thêm mới ID#7', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2177', '2015-06-23 22:09:03', '2', 'Thêm mới ID#8', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2178', '2015-06-24 21:37:37', '3', 'Cập nhật ID#12', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2179', '2015-06-24 21:38:08', '3', 'Cập nhật ID#12', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2180', '2015-06-24 21:38:57', '3', 'Cập nhật ID#12', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2181', '2015-06-24 21:38:57', '3', 'Cập nhật ID#12', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2182', '2015-06-25 20:51:28', '3', 'Cập nhật ID#1', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2183', '2015-06-25 20:55:02', '3', 'Cập nhật ID#1', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2184', '2015-06-25 20:55:24', '3', 'Cập nhật ID#1', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2185', '2015-06-25 20:55:24', '3', 'Cập nhật ID#1', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2186', '2015-06-25 20:55:31', '3', 'Cập nhật ID#1', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2187', '2015-06-25 21:00:42', '3', 'Cập nhật ID#15', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2188', '2015-06-25 21:00:50', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2189', '2015-06-25 21:00:59', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2190', '2015-06-25 21:00:59', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2191', '2015-06-25 21:02:54', '3', 'Cập nhật ID#15', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2192', '2015-06-25 21:02:55', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2193', '2015-06-25 21:02:59', '3', 'Cập nhật ID#15', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2194', '2015-06-25 21:02:59', '3', 'Cập nhật ID#15', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2195', '2015-06-25 21:03:07', '3', 'Cập nhật ID#15', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2196', '2015-06-25 21:03:15', '3', 'Cập nhật ID#15', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2197', '2015-06-25 21:03:15', '3', 'Cập nhật ID#15', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2198', '2015-06-25 21:52:23', '3', 'Cập nhật ID#16', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2199', '2015-07-13 20:41:33', '2', 'Thêm mới ID#1', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2200', '2015-07-13 20:50:23', '2', 'Thêm mới ID#2', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2201', '2015-07-13 20:50:50', '2', 'Thêm mới ID#3', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2202', '2015-07-13 20:51:17', '2', 'Thêm mới ID#4', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2203', '2015-07-13 20:51:29', '2', 'Thêm mới ID#5', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2204', '2015-07-13 20:51:37', '2', 'Thêm mới ID#6', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2205', '2015-07-13 20:52:06', '2', 'Thêm mới ID#7', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2206', '2015-07-13 20:56:39', '2', 'Thêm mới ID#8', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2207', '2015-07-13 21:01:02', '4', 'Xóa dữ liệu ID#38', 'admincp/node/delete', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2208', '2015-07-13 21:02:30', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2209', '2015-07-13 21:02:53', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2210', '2015-07-13 21:03:06', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2211', '2015-07-13 21:03:06', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2212', '2015-07-13 21:04:20', '2', 'Thêm mới ID#9', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2213', '2015-07-13 21:06:14', '3', 'Cập nhật ID#8', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2214', '2015-07-13 21:30:40', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2215', '2015-07-13 21:31:10', '2', 'Thêm mới ID#10', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2216', '2015-07-13 21:32:28', '3', 'Cập nhật ID#9', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2217', '2015-07-13 21:32:44', '3', 'Cập nhật ID#9', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2218', '2015-07-13 21:32:44', '3', 'Cập nhật ID#9', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2219', '2015-07-14 21:05:56', '2', 'Thêm mới ID#39', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2220', '2015-07-14 21:06:44', '3', 'Cập nhật ID#23', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2221', '2015-07-14 21:16:09', '4', 'Xóa dữ liệu ID#24', 'admincp/node/delete', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2222', '2015-07-14 21:16:15', '4', 'Xóa dữ liệu ID#5', 'admincp/node/delete', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2223', '2015-07-14 21:17:55', '2', 'Thêm mới ID#1', 'admincp/member/create', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2224', '2015-07-14 21:19:02', '4', 'Xóa dữ liệu ID#1', 'admincp/member/delete', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2225', '2015-07-14 21:28:36', '2', 'Thêm mới ID#40', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2226', '2015-07-14 21:28:47', '2', 'Thêm mới ID#41', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2227', '2015-07-14 21:29:08', '2', 'Thêm mới ID#42', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2228', '2015-07-14 21:30:10', '2', 'Thêm mới ID#1', 'admincp/sponsor/create', 'Sponsor', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2229', '2015-07-15 20:55:21', '2', 'Thêm mới ID#43', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2230', '2015-07-15 21:03:05', '2', 'Thêm mới ID#44', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2231', '2015-07-15 21:28:01', '4', 'Xóa dữ liệu ID#44', 'admincp/node/delete', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2232', '2015-07-15 21:28:14', '3', 'Cập nhật ID#36', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2233', '2015-07-15 21:37:19', '2', 'Thêm mới ID#1', 'admincp/gallery/create', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2234', '2015-07-15 22:07:02', '2', 'Thêm mới ID#45', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2235', '2015-07-15 22:10:17', '2', 'Thêm mới ID#2', 'admincp/video/create', 'Video', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2236', '2015-07-15 22:12:01', '2', 'Thêm mới ID#3', 'admincp/video/create', 'Video', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2237', '2015-07-15 22:15:56', '2', 'Thêm mới ID#1', 'admincp/member/create', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2238', '2015-07-15 22:16:00', '3', 'Cập nhật ID#1', 'admincp/member/update', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2239', '2015-07-15 22:17:16', '3', 'Cập nhật ID#3', 'admincp/video/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2240', '2015-07-15 22:18:37', '2', 'Thêm mới ID#4', 'admincp/video/create', 'Video', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2241', '2015-07-15 22:19:05', '3', 'Cập nhật ID#4', 'admincp/video/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2242', '2015-07-15 22:21:55', '3', 'Cập nhật ID#2', 'admincp/video/update', 'Video', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2243', '2015-07-16 20:17:37', '3', 'Cập nhật ID#1', 'admincp/member/update', 'Member', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2244', '2015-07-16 20:48:55', '4', 'Xóa dữ liệu ID#2', 'admincp/advs/deleteSelected', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2245', '2015-07-16 20:48:55', '4', 'Xóa dữ liệu ID#1', 'admincp/advs/deleteSelected', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2246', '2015-07-16 21:05:38', '2', 'Thêm mới ID#3', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2247', '2015-07-16 21:06:06', '2', 'Thêm mới ID#4', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2248', '2015-07-16 21:11:37', '2', 'Thêm mới ID#1', 'admincp/gallery/create', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2249', '2015-07-16 21:12:08', '2', 'Thêm mới ID#2', 'admincp/gallery/create', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2250', '2015-07-16 21:28:46', '3', 'Cập nhật ID#2', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2251', '2015-07-16 21:29:12', '3', 'Cập nhật ID#1', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2252', '2015-07-16 21:30:32', '3', 'Cập nhật ID#2', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2253', '2015-07-16 21:30:36', '3', 'Cập nhật ID#1', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2254', '2015-07-16 21:30:59', '3', 'Cập nhật ID#2', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2255', '2015-07-16 21:31:11', '3', 'Cập nhật ID#2', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2256', '2015-07-16 21:31:30', '3', 'Cập nhật ID#2', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2257', '2015-07-16 21:31:42', '3', 'Cập nhật ID#2', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2258', '2015-07-16 21:31:58', '3', 'Cập nhật ID#2', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2259', '2015-07-16 21:32:04', '3', 'Cập nhật ID#1', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2260', '2015-07-16 21:33:13', '3', 'Cập nhật ID#2', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2261', '2015-07-16 21:34:40', '3', 'Cập nhật ID#2', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2262', '2015-07-16 21:34:57', '3', 'Cập nhật ID#1', 'admincp/gallery/update', 'Gallery', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2263', '2015-07-16 21:43:10', '2', 'Thêm mới ID#3', 'admincp/video/create', 'Video', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2264', '2015-07-16 22:09:59', '2', 'Thêm mới ID#1', 'admincp/sponsor/create', 'Sponsor', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2265', '2015-07-16 22:10:18', '4', 'Xóa dữ liệu ID#1', 'admincp/sponsor/delete', 'Sponsor', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2266', '2015-07-16 22:10:43', '2', 'Thêm mới ID#2', 'admincp/sponsor/create', 'Sponsor', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2267', '2015-07-16 22:18:01', '2', 'Thêm mới ID#3', 'admincp/sponsor/create', 'Sponsor', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2268', '2015-07-16 22:33:15', '2', 'Thêm mới ID#11', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2269', '2015-07-16 22:33:26', '2', 'Thêm mới ID#12', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2270', '2015-07-16 22:35:49', '2', 'Thêm mới ID#13', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2271', '2015-07-16 22:36:11', '2', 'Thêm mới ID#14', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2272', '2015-07-16 22:37:40', '2', 'Thêm mới ID#15', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2273', '2015-07-16 22:41:21', '2', 'Thêm mới ID#16', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2274', '2015-07-16 23:11:08', '2', 'Thêm mới ID#17', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2275', '2015-07-16 23:15:05', '2', 'Thêm mới ID#18', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2276', '2015-07-16 23:15:20', '2', 'Thêm mới ID#19', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2277', '2015-07-16 23:16:52', '3', 'Cập nhật ID#19', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2278', '2015-07-16 23:17:16', '3', 'Cập nhật ID#19', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2279', '2015-07-16 23:17:16', '3', 'Cập nhật ID#19', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2280', '2015-07-16 23:17:55', '2', 'Thêm mới ID#20', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2281', '2015-07-16 23:37:53', '4', 'Xóa dữ liệu ID#7', 'admincp/category/delete', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2282', '2015-07-16 23:38:39', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2283', '2015-07-16 23:38:41', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2284', '2015-07-16 23:38:53', '3', 'Cập nhật ID#4', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2285', '2015-07-16 23:38:54', '3', 'Cập nhật ID#5', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2286', '2015-07-16 23:38:56', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2287', '2015-07-16 23:39:00', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2288', '2015-07-16 23:39:00', '3', 'Cập nhật ID#17', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2289', '2015-07-16 23:39:03', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2290', '2015-07-16 23:39:03', '3', 'Cập nhật ID#3', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2291', '2015-07-16 23:39:05', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2292', '2015-07-16 23:39:08', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2293', '2015-07-16 23:39:08', '3', 'Cập nhật ID#2', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2294', '2015-07-16 23:39:11', '3', 'Cập nhật ID#4', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2295', '2015-07-16 23:39:11', '3', 'Cập nhật ID#4', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2296', '2015-07-16 23:39:14', '3', 'Cập nhật ID#5', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2297', '2015-07-16 23:39:14', '3', 'Cập nhật ID#5', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2298', '2015-07-16 23:39:17', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2299', '2015-07-16 23:39:17', '3', 'Cập nhật ID#6', 'admincp/category/update', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2300', '2015-07-17 00:18:52', '3', 'Cập nhật ID#3', 'admincp/video/update', 'Video', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2301', '2015-12-08 09:36:34', '4', 'Xóa dữ liệu ID#23', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2302', '2015-12-08 09:36:34', '4', 'Xóa dữ liệu ID#34', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2303', '2015-12-08 09:36:34', '4', 'Xóa dữ liệu ID#35', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2304', '2015-12-08 09:36:34', '4', 'Xóa dữ liệu ID#36', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2305', '2015-12-08 09:36:34', '4', 'Xóa dữ liệu ID#37', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2306', '2015-12-08 09:36:34', '4', 'Xóa dữ liệu ID#39', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2307', '2015-12-08 09:36:34', '4', 'Xóa dữ liệu ID#40', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2308', '2015-12-08 09:36:50', '4', 'Xóa dữ liệu ID#43', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2309', '2015-12-08 09:36:50', '4', 'Xóa dữ liệu ID#45', 'admincp/node/deleteSelected', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2310', '2015-12-08 09:37:10', '4', 'Xóa dữ liệu ID#42', 'admincp/node/delete', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2311', '2015-12-08 09:37:13', '4', 'Xóa dữ liệu ID#41', 'admincp/node/delete', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2312', '2015-12-08 09:39:12', '3', 'Cập nhật ID#8', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2313', '2015-12-08 09:48:40', '2', 'Thêm mới ID#1', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2314', '2015-12-08 09:49:21', '2', 'Thêm mới ID#2', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2315', '2015-12-08 09:49:28', '2', 'Thêm mới ID#3', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2316', '2015-12-08 09:49:53', '2', 'Thêm mới ID#4', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2317', '2015-12-08 09:50:06', '2', 'Thêm mới ID#5', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2318', '2015-12-08 09:50:16', '2', 'Thêm mới ID#6', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2319', '2015-12-08 09:58:42', '2', 'Thêm mới ID#46', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2320', '2015-12-08 09:59:00', '2', 'Thêm mới ID#47', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2321', '2015-12-08 10:28:05', '2', 'Thêm mới ID#1', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2322', '2015-12-08 10:29:03', '3', 'Cập nhật ID#1', 'admincp/manga/update', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2323', '2015-12-08 10:50:00', '3', 'Cập nhật ID#1', 'admincp/manga/update', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2324', '2015-12-08 10:51:37', '3', 'Cập nhật ID#1', 'admincp/manga/update', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2325', '2015-12-08 11:26:59', '2', 'Thêm mới ID#48', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2326', '2015-12-08 11:32:34', '2', 'Thêm mới ID#', 'admincp/group/create', 'Group', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2327', '2015-12-08 11:33:05', '2', 'Thêm mới ID#1', 'admincp/group/create', 'Group', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2328', '2015-12-08 11:33:07', '3', 'Cập nhật ID#1', 'admincp/group/update', 'Group', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2329', '2015-12-08 11:33:16', '2', 'Thêm mới ID#2', 'admincp/group/create', 'Group', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2330', '2015-12-08 11:33:26', '2', 'Thêm mới ID#3', 'admincp/group/create', 'Group', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2331', '2015-12-08 11:33:40', '2', 'Thêm mới ID#4', 'admincp/group/create', 'Group', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2332', '2015-12-08 11:34:57', '3', 'Cập nhật ID#2', 'admincp/group/update', 'Group', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2333', '2015-12-08 11:37:34', '3', 'Cập nhật ID#1', 'admincp/manga/update', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2334', '2015-12-08 11:38:02', '3', 'Cập nhật ID#1', 'admincp/manga/update', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2335', '2015-12-08 11:39:14', '2', 'Thêm mới ID#2', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2336', '2015-12-08 11:41:31', '2', 'Thêm mới ID#3', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2337', '2015-12-08 11:42:09', '2', 'Thêm mới ID#4', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2338', '2015-12-08 11:42:41', '2', 'Thêm mới ID#5', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2339', '2015-12-08 11:42:46', '3', 'Cập nhật ID#5', 'admincp/manga/update', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2340', '2015-12-08 11:42:56', '3', 'Cập nhật ID#3', 'admincp/manga/update', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2341', '2015-12-08 11:43:05', '3', 'Cập nhật ID#4', 'admincp/manga/update', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2342', '2015-12-08 11:44:41', '2', 'Thêm mới ID#6', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2343', '2015-12-08 11:46:18', '2', 'Thêm mới ID#7', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2344', '2015-12-08 11:59:27', '2', 'Thêm mới ID#49', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2345', '2015-12-08 12:00:21', '3', 'Cập nhật ID#49', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2346', '2015-12-08 13:55:26', '2', 'Thêm mới ID#1', 'admincp/chapter/create', 'Chapter', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2347', '2015-12-08 14:00:03', '2', 'Thêm mới ID#2', 'admincp/chapter/create', 'Chapter', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2348', '2015-12-08 14:00:22', '2', 'Thêm mới ID#3', 'admincp/chapter/create', 'Chapter', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2349', '2015-12-08 14:01:53', '3', 'Cập nhật ID#2', 'admincp/chapter/update', 'Chapter', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2350', '2015-12-08 14:02:01', '3', 'Cập nhật ID#3', 'admincp/chapter/update', 'Chapter', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2351', '2015-12-08 14:02:13', '3', 'Cập nhật ID#1', 'admincp/chapter/update', 'Chapter', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2352', '2015-12-08 15:00:37', '4', 'Xóa dữ liệu ID#251', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2353', '2015-12-08 15:00:37', '4', 'Xóa dữ liệu ID#250', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2354', '2015-12-08 15:00:37', '4', 'Xóa dữ liệu ID#249', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2355', '2015-12-08 15:00:37', '4', 'Xóa dữ liệu ID#248', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2356', '2015-12-08 15:00:38', '4', 'Xóa dữ liệu ID#246', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2357', '2015-12-08 15:00:38', '4', 'Xóa dữ liệu ID#245', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2358', '2015-12-08 15:00:38', '4', 'Xóa dữ liệu ID#244', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2359', '2015-12-08 15:00:38', '4', 'Xóa dữ liệu ID#243', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2360', '2015-12-08 15:00:38', '4', 'Xóa dữ liệu ID#242', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2361', '2015-12-08 15:00:38', '4', 'Xóa dữ liệu ID#241', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2362', '2015-12-08 15:00:38', '4', 'Xóa dữ liệu ID#240', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2363', '2015-12-08 15:00:38', '4', 'Xóa dữ liệu ID#239', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2364', '2015-12-08 15:00:38', '4', 'Xóa dữ liệu ID#238', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2365', '2015-12-08 15:00:38', '4', 'Xóa dữ liệu ID#237', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2366', '2015-12-08 15:00:38', '4', 'Xóa dữ liệu ID#236', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2367', '2015-12-08 15:00:38', '4', 'Xóa dữ liệu ID#235', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2368', '2015-12-08 15:00:53', '4', 'Xóa dữ liệu ID#247', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2369', '2015-12-08 15:00:53', '4', 'Xóa dữ liệu ID#230', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2370', '2015-12-08 15:00:54', '4', 'Xóa dữ liệu ID#229', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2371', '2015-12-08 15:00:54', '4', 'Xóa dữ liệu ID#228', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2372', '2015-12-08 15:00:54', '4', 'Xóa dữ liệu ID#227', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2373', '2015-12-08 15:00:54', '4', 'Xóa dữ liệu ID#226', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2374', '2015-12-08 15:00:54', '4', 'Xóa dữ liệu ID#225', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2375', '2015-12-08 15:00:54', '4', 'Xóa dữ liệu ID#224', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2376', '2015-12-08 15:00:54', '4', 'Xóa dữ liệu ID#223', 'admincp/settings/deleteSelected', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2377', '2015-12-08 15:01:04', '3', 'Cập nhật ID#164', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2378', '2015-12-08 15:01:19', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2379', '2015-12-08 15:02:02', '3', 'Cập nhật ID#25', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2380', '2015-12-08 15:03:41', '3', 'Cập nhật ID#233', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2381', '2015-12-08 15:03:45', '3', 'Cập nhật ID#231', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2382', '2015-12-08 15:03:50', '4', 'Xóa dữ liệu ID#162', 'admincp/settings/delete', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2383', '2015-12-08 15:03:54', '4', 'Xóa dữ liệu ID#159', 'admincp/settings/delete', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2384', '2015-12-08 15:04:07', '3', 'Cập nhật ID#155', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2385', '2015-12-08 15:04:11', '4', 'Xóa dữ liệu ID#154', 'admincp/settings/delete', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2386', '2015-12-08 15:04:21', '3', 'Cập nhật ID#153', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2387', '2015-12-08 15:04:34', '3', 'Cập nhật ID#74', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2388', '2015-12-08 15:04:37', '3', 'Cập nhật ID#73', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2389', '2015-12-08 15:04:40', '3', 'Cập nhật ID#69', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2390', '2015-12-08 15:04:43', '3', 'Cập nhật ID#68', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2391', '2015-12-08 15:04:53', '3', 'Cập nhật ID#21', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2392', '2015-12-08 15:05:14', '4', 'Xóa dữ liệu ID#222', 'admincp/settings/delete', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2393', '2015-12-08 16:09:59', '3', 'Cập nhật ID#7', 'admincp/manga/update', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2394', '2015-12-08 16:10:04', '3', 'Cập nhật ID#5', 'admincp/manga/update', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2395', '2015-12-08 20:34:57', '2', 'Thêm mới ID#8', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2396', '2015-12-08 20:36:32', '2', 'Thêm mới ID#9', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2397', '2015-12-08 20:38:02', '2', 'Thêm mới ID#10', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2398', '2015-12-08 20:38:49', '2', 'Thêm mới ID#11', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2399', '2015-12-08 20:39:35', '2', 'Thêm mới ID#12', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2400', '2015-12-08 20:40:18', '2', 'Thêm mới ID#13', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2401', '2015-12-08 20:41:00', '2', 'Thêm mới ID#14', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2402', '2015-12-08 20:42:16', '2', 'Thêm mới ID#15', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2403', '2015-12-08 20:42:26', '2', 'Thêm mới ID#16', 'admincp/manga/create', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2404', '2015-12-08 21:47:36', '3', 'Cập nhật ID#12', 'admincp/manga/update', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2405', '2015-12-08 21:50:07', '3', 'Cập nhật ID#12', 'admincp/manga/update', 'Manga', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2406', '2015-12-26 00:10:31', '2', 'Thêm mới ID#9', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2407', '2016-01-05 10:25:38', '2', 'Thêm mới ID#1', 'admincp/about/create', 'About', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2408', '2016-01-05 10:32:09', '2', 'Thêm mới ID#1', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2409', '2016-01-05 10:32:22', '2', 'Thêm mới ID#2', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2410', '2016-01-05 10:32:34', '2', 'Thêm mới ID#3', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2411', '2016-01-05 10:33:01', '3', 'Cập nhật ID#1', 'admincp/about/update', 'About', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2412', '2016-01-05 10:46:49', '2', 'Thêm mới ID#4', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2413', '2016-01-05 10:46:55', '2', 'Thêm mới ID#5', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2414', '2016-01-05 10:47:01', '2', 'Thêm mới ID#6', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2415', '2016-01-05 10:47:09', '2', 'Thêm mới ID#7', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2416', '2016-01-05 10:58:08', '2', 'Thêm mới ID#1', 'admincp/services/create', 'Services', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2417', '2016-01-05 11:05:46', '2', 'Thêm mới ID#50', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2418', '2016-01-05 11:06:01', '2', 'Thêm mới ID#51', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2419', '2016-01-05 11:06:38', '2', 'Thêm mới ID#52', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2420', '2016-01-05 11:06:49', '2', 'Thêm mới ID#53', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2421', '2016-01-05 11:07:06', '2', 'Thêm mới ID#54', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2422', '2016-01-05 11:07:39', '2', 'Thêm mới ID#55', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2423', '2016-01-05 11:25:11', '2', 'Thêm mới ID#10', 'admincp/post/create', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2424', '2016-01-05 11:25:40', '3', 'Cập nhật ID#17', 'admincp/node/update', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2425', '2016-01-05 11:25:51', '2', 'Thêm mới ID#56', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2426', '2016-01-05 11:26:10', '4', 'Xóa dữ liệu ID#56', 'admincp/node/delete', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2427', '2016-01-05 11:29:54', '3', 'Cập nhật ID#4', 'admincp/advs/update', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2428', '2016-01-05 11:30:22', '3', 'Cập nhật ID#4', 'admincp/advs/update', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2429', '2016-01-05 11:33:01', '3', 'Cập nhật ID#4', 'admincp/advs/update', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2430', '2016-01-05 11:35:23', '3', 'Cập nhật ID#4', 'admincp/advs/update', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2431', '2016-01-05 11:49:24', '2', 'Thêm mới ID#57', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2432', '2016-01-05 11:51:18', '2', 'Thêm mới ID#1', 'admincp/partner/create', 'Partner', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2433', '2016-01-05 11:51:25', '2', 'Thêm mới ID#2', 'admincp/partner/create', 'Partner', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2434', '2016-01-05 12:05:37', '2', 'Thêm mới ID#58', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2435', '2016-01-05 12:05:49', '2', 'Thêm mới ID#59', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2436', '2016-01-05 12:06:09', '2', 'Thêm mới ID#60', 'admincp/node/create', 'AdmincpNode', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2437', '2016-01-05 13:51:48', '2', 'Thêm mới ID#8', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2438', '2016-01-05 13:51:57', '2', 'Thêm mới ID#9', 'admincp/category/create', 'Category', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2439', '2016-01-05 13:53:34', '2', 'Thêm mới ID#1', 'admincp/project/create', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2440', '2016-01-05 13:53:49', '3', 'Cập nhật ID#1', 'admincp/project/update', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2441', '2016-01-05 13:54:05', '3', 'Cập nhật ID#1', 'admincp/project/update', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2442', '2016-01-05 13:54:14', '3', 'Cập nhật ID#1', 'admincp/project/update', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2443', '2016-01-05 13:54:26', '3', 'Cập nhật ID#1', 'admincp/project/update', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2444', '2016-01-05 13:54:36', '3', 'Cập nhật ID#1', 'admincp/project/update', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2445', '2016-01-05 14:15:27', '3', 'Cập nhật ID#1', 'admincp/project/update', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2446', '2016-01-05 14:15:37', '3', 'Cập nhật ID#1', 'admincp/project/update', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2447', '2016-01-05 14:15:45', '3', 'Cập nhật ID#1', 'admincp/project/update', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2448', '2016-01-05 15:20:06', '3', 'Cập nhật ID#93', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2449', '2016-01-05 15:20:38', '3', 'Cập nhật ID#21', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2450', '2016-01-05 15:21:10', '3', 'Cập nhật ID#74', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2451', '2016-01-05 15:21:13', '3', 'Cập nhật ID#73', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2452', '2016-01-05 15:21:17', '3', 'Cập nhật ID#69', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2453', '2016-01-05 15:21:19', '3', 'Cập nhật ID#68', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2454', '2016-01-05 15:21:34', '3', 'Cập nhật ID#152', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2455', '2016-01-05 15:21:47', '3', 'Cập nhật ID#173', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2456', '2016-01-05 15:22:03', '3', 'Cập nhật ID#153', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2457', '2016-01-05 15:22:06', '3', 'Cập nhật ID#155', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2458', '2016-01-05 15:22:30', '3', 'Cập nhật ID#25', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2459', '2016-01-05 15:23:31', '2', 'Thêm mới ID#252', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2460', '2016-01-05 15:29:34', '2', 'Thêm mới ID#1', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2461', '2016-01-05 15:29:41', '3', 'Cập nhật ID#1', 'admincp/advs/update', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2462', '2016-01-05 15:29:54', '2', 'Thêm mới ID#2', 'admincp/advs/create', 'Advs', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2463', '2016-01-05 16:23:24', '2', 'Thêm mới ID#1', 'admincp/project/create', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2464', '2016-01-05 16:23:33', '2', 'Thêm mới ID#2', 'admincp/project/create', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2465', '2016-01-05 16:24:14', '2', 'Thêm mới ID#3', 'admincp/project/create', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2466', '2016-01-05 16:24:30', '2', 'Thêm mới ID#4', 'admincp/project/create', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2467', '2016-01-05 16:24:53', '2', 'Thêm mới ID#5', 'admincp/project/create', 'Project', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2468', '2016-01-05 16:32:29', '3', 'Cập nhật ID#10', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2469', '2016-01-05 16:32:41', '3', 'Cập nhật ID#10', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2470', '2016-01-05 16:32:45', '3', 'Cập nhật ID#9', 'admincp/post/update', 'Post', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2471', '2016-01-05 16:54:35', '2', 'Thêm mới ID#253', 'admincp/settings/create', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2472', '2016-01-05 16:55:06', '3', 'Cập nhật ID#102', 'admincp/settings/update', 'Settings', '65535', '0', '1');
INSERT INTO `pk_log` VALUES ('2473', '2016-01-05 16:57:02', '2', 'Thêm mới ID#254', 'admincp/settings/create', 'Settings', '65535', '0', '1');

-- ----------------------------
-- Table structure for pk_member
-- ----------------------------
DROP TABLE IF EXISTS `pk_member`;
CREATE TABLE `pk_member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `about` text,
  `image` text,
  `code` varchar(20) DEFAULT NULL,
  `business` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `field` text,
  `link` varchar(255) DEFAULT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_member
-- ----------------------------
INSERT INTO `pk_member` VALUES ('1', 'đâsdasdas', 'dasdasdas', '', 'Another-You---Armin-Van-Buuren-Mr-Prob-FLAC-Losslessflac.flac', '', '', '', '', '', null, null, '1');

-- ----------------------------
-- Table structure for pk_newsletter
-- ----------------------------
DROP TABLE IF EXISTS `pk_newsletter`;
CREATE TABLE `pk_newsletter` (
  `newsletter_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`newsletter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_newsletter
-- ----------------------------
INSERT INTO `pk_newsletter` VALUES ('1', null, 'cuongphu.nd16@gmail.com', '2015-01-26 08:48:46');
INSERT INTO `pk_newsletter` VALUES ('2', null, 'cuong.vu@htecom.vn', '2015-01-26 08:48:46');
INSERT INTO `pk_newsletter` VALUES ('3', null, 'cuongphu.nd@gmail.com', '2015-01-26 08:48:46');
INSERT INTO `pk_newsletter` VALUES ('4', null, 'cuongphu.nd111116@gmail.com', '2015-02-04 16:10:18');
INSERT INTO `pk_newsletter` VALUES ('5', '43242342', '3432@gma.ocm', '2015-06-02 14:51:02');
INSERT INTO `pk_newsletter` VALUES ('6', 'fdsfds', 'dsadsds@gmail.com', '2015-06-02 14:51:20');
INSERT INTO `pk_newsletter` VALUES ('7', 'dfdf', 'sdfsdfsd@gf.com', '2015-06-02 14:51:56');
INSERT INTO `pk_newsletter` VALUES ('8', 'gdfgfd', '32244t@gmail.com', '2015-06-02 14:58:57');
INSERT INTO `pk_newsletter` VALUES ('9', 'dfsdfds', 'saffdfd@gmakls.com', '2015-06-02 15:12:41');

-- ----------------------------
-- Table structure for pk_node
-- ----------------------------
DROP TABLE IF EXISTS `pk_node`;
CREATE TABLE `pk_node` (
  `node_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` smallint(6) DEFAULT '0',
  `node_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controller` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `node_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `node_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` smallint(6) DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`node_id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of pk_node
-- ----------------------------
INSERT INTO `pk_node` VALUES ('1', '0', '', '', 'Quản lý', '', '0', '1');
INSERT INTO `pk_node` VALUES ('2', '0', '', '', 'Tin tức', '', '1', '1');
INSERT INTO `pk_node` VALUES ('3', '0', '', '', 'Tiện ích', '', '3', '1');
INSERT INTO `pk_node` VALUES ('4', '0', '', '', 'Thành viên quản trị', '', '5', '1');
INSERT INTO `pk_node` VALUES ('6', '1', 'Settings', 'SettingsController', 'Cấu hình Website', 'admincp/Settings/admin', '0', '1');
INSERT INTO `pk_node` VALUES ('7', '1', '', '', 'Cấu hình tiếng Việt', 'admincp/language/vn', '0', '1');
INSERT INTO `pk_node` VALUES ('8', '1', 'Category', 'CategoryController', 'Danh mục', 'admincp/Category/admin', '0', '1');
INSERT INTO `pk_node` VALUES ('16', '1', 'Node', 'NodeController', 'Cấu hình menu', 'admincp/Node/admin', '0', '1');
INSERT INTO `pk_node` VALUES ('17', '2', 'Post', 'PostController', 'Danh sách bài tin tức', 'admincp/Post/admin', '0', '1');
INSERT INTO `pk_node` VALUES ('18', '2', '', '', 'Thêm mới Tin tức', 'admincp/Post/create', '0', '1');
INSERT INTO `pk_node` VALUES ('19', '3', 'Advs', 'AdvsController', 'Slide Banner / Quảng cáo', 'admincp/Advs/admin', '0', '1');
INSERT INTO `pk_node` VALUES ('21', '4', 'Role', 'RoleController', 'Phân nhóm quản trị', 'admincp/Role/admin', '0', '1');
INSERT INTO `pk_node` VALUES ('22', '4', 'Usersystem', 'UsersystemController', 'Danh sách quản trị viên', 'admincp/Usersystem/admin', '0', '1');
INSERT INTO `pk_node` VALUES ('25', '0', '', '', 'Tuyển dụng', '', '5', '1');
INSERT INTO `pk_node` VALUES ('30', '3', 'Contact', 'ContactController', 'Danh sách liên hệ', 'admincp/Contact/admin', '0', '1');
INSERT INTO `pk_node` VALUES ('50', '0', '', '', 'Giới thiệu', '', '0', '1');
INSERT INTO `pk_node` VALUES ('51', '50', 'About', 'AboutController', 'Danh sách bài giới thiệu', 'admincp/About/admin', '0', '1');
INSERT INTO `pk_node` VALUES ('52', '50', '', '', 'Thêm mới bài giới thiệu', 'admincp/about/create', '0', '1');
INSERT INTO `pk_node` VALUES ('53', '0', '', '', 'Dịch vụ', '', '0', '1');
INSERT INTO `pk_node` VALUES ('54', '53', 'Services', 'ServicesController', 'Danh sách bài dịch vụ', 'admincp/Services/admin', '0', '1');
INSERT INTO `pk_node` VALUES ('55', '53', '', '', 'Thêm mới bài dịch vụ', 'admincp/services/create', '0', '1');
INSERT INTO `pk_node` VALUES ('57', '3', 'Partner', 'PartnerController', 'Danh sách đối tác', 'admincp/Partner/admin', '0', '1');
INSERT INTO `pk_node` VALUES ('58', '0', '', '', 'Dự án', '', '0', '1');
INSERT INTO `pk_node` VALUES ('59', '58', 'Project', 'ProjectController', 'Danh sách dự án', 'admincp/Project/admin', '0', '1');
INSERT INTO `pk_node` VALUES ('60', '58', '', '', 'Thêm mới dự án', 'admincp/project/create', '0', '1');

-- ----------------------------
-- Table structure for pk_partner
-- ----------------------------
DROP TABLE IF EXISTS `pk_partner`;
CREATE TABLE `pk_partner` (
  `partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `image` text,
  `created_at` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `sort_order` int(4) DEFAULT '0',
  PRIMARY KEY (`partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_partner
-- ----------------------------

-- ----------------------------
-- Table structure for pk_post
-- ----------------------------
DROP TABLE IF EXISTS `pk_post`;
CREATE TABLE `pk_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `image` text,
  `type` tinyint(2) DEFAULT NULL COMMENT 'Loại bài viết như giới thiêu, điều khoản sử dụng, tư vấn nghề nghiệp, góc báo chí,.....\r\n',
  `cate_id` tinyint(4) DEFAULT NULL,
  `language` int(4) NOT NULL DEFAULT '1',
  `description` text,
  `content` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `focus` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(4) DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `views` int(4) NOT NULL DEFAULT '0',
  `type_index` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`post_id`),
  FULLTEXT KEY `title` (`title`,`content`,`description`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_post
-- ----------------------------
INSERT INTO `pk_post` VALUES ('1', 'Test thử', 'test-thu', 'recent-post-img1jpg.jpg', null, '20', '1', '', 'test dữ liệu chức năng', '2015-05-30 15:18:59', 'admin', '1', '1', null, '', '0', '0');
INSERT INTO `pk_post` VALUES ('2', 'Tin tức khám sức khỏe', 'tin-tuc-kham-suc-khoe', 'latest-news3jpg.jpg', null, '20', '1', 'Khám sức khỏe thường xuyên định kỳ Khám sức khỏe thường xuyên định kỳ Khám sức khỏe thường xuyên định kỳ Khám sức khỏe thường xuyên định kỳ Khám sức khỏe thường xuyên định kỳ', 'Khám sức khỏe thường xuyên định kỳ', '2015-06-02 13:36:40', null, '1', '1', null, '', '0', '0');
INSERT INTO `pk_post` VALUES ('3', 'Bài tin tức nội bộ hai', 'bai-tin-tuc-noi-bo-hai', '', null, '20', '1', '', 'Chào mừng tân trưởng khoa ngoại Đoàn', '2015-06-02 13:39:49', null, '1', '0', null, '', '0', '0');
INSERT INTO `pk_post` VALUES ('4', 'Kiểm tra sức khỏe', 'kiem-tra-suc-khoe', 'latest-news1jpg.jpg', null, '20', '1', '', 'bác sĩ Thu Hiền chuyên kiểm tra sức khỏe tổng thế', '2015-06-02 13:41:54', null, '1', '0', null, '', '0', '0');
INSERT INTO `pk_post` VALUES ('5', 'dfsdfds', 'dfsdfds', 'app-available-imgjpg.jpg', null, null, '1', '<p>\r\n	If you need a doctor for to the consectetuer consectetur adipiscing Graphic Design is elit.</p>\r\n<ul class=\"list-unstyled\">\r\n	<li class=\"clearfix\">\r\n		<span>Thứ 2 - Thứ 6 </span>\r\n		<div class=\"value\">\r\n			8.00 - 16.00</div>\r\n	</li>\r\n	<li class=\"clearfix\">\r\n		<span>Thứ 7 </span>\r\n		<div class=\"value\">\r\n			9.30 - 15.30</div>\r\n	</li>\r\n	<li class=\"clearfix\">\r\n		<span>Chủ nhật </span>\r\n		<div class=\"value\">\r\n			9.30 - 17.00</div>\r\n	</li>\r\n</ul>\r\n', '<div>\r\n	<span style=\"font-family: Consolas, \'Lucida Console\', monospace; white-space: pre-wrap;\">If you need a doctor for to the consectetuer consectetur adipiscing Graphic Design is elit.</span></div>\r\n<ul class=\"list-unstyled\">\r\n	<li class=\"clearfix\">\r\n		<span>Thứ 2 - Thứ 6 </span>\r\n		<div class=\"value\">\r\n			8.00 - 16.00</div>\r\n	</li>\r\n	<li class=\"clearfix\">\r\n		<span>Thứ 7 </span>\r\n		<div class=\"value\">\r\n			9.30 - 15.30</div>\r\n	</li>\r\n	<li class=\"clearfix\">\r\n		<span>Chủ nhật </span>\r\n		<div class=\"value\">\r\n			9.30 - 17.00</div>\r\n	</li>\r\n</ul>\r\n', '2015-06-03 22:14:42', null, '1', '0', null, '', '0', '1');
INSERT INTO `pk_post` VALUES ('6', ' fsd fds fsd fsd f', 'fsd-fds-fsd-fsd-f', '', null, null, '1', 'sd fsd fsd fsd fsd f&nbsp;fds fsd fsd f<br />\r\nds f<br />\r\nsdf<br />\r\n&nbsp;sd<br />\r\n&nbsp;fsd<br />\r\n&nbsp;f<br />\r\nsd fsd fsd fsd fsd fsd fsd fsd fd&nbsp;fds fsd fsd f<br />\r\nds f<br />\r\nsdf<br />\r\n&nbsp;sd<br />\r\n&nbsp;fsd<br />\r\n&nbsp;f<br />\r\nsd fsd fsd fsd fsd fsd fsd fsd fd&nbsp;fds fsd fsd f<br />\r\nds f<br />\r\nsdf<br />\r\n&nbsp;sd<br />\r\n&nbsp;fsd<br />\r\n&nbsp;f<br />\r\nsd fsd fsd fsd fsd fsd fsd fsd fd&nbsp;fds fsd fsd f<br />\r\nds f<br />\r\nsdf<br />\r\n&nbsp;sd<br />\r\n&nbsp;fsd<br />\r\n&nbsp;f<br />\r\nsd fsd fsd fsd fsd fsd fsd fsd fd&nbsp;fds fsd fsd f<br />\r\nds f<br />\r\nsdf<br />\r\n&nbsp;sd<br />\r\n&nbsp;fsd<br />\r\n&nbsp;f<br />\r\nsd fsd fsd fsd fsd fsd fsd fsd fd', '&nbsp;fds fsd fsd f<br />\r\nds f<br />\r\nsdf<br />\r\n&nbsp;sd<br />\r\n&nbsp;fsd<br />\r\n&nbsp;f<br />\r\nsd fsd fsd fsd fsd fsd fsd fsd fd', '2015-06-03 22:27:44', null, '1', '0', null, '', '0', '1');
INSERT INTO `pk_post` VALUES ('7', 'sdfds fds ', 'sdfds-fds', '', null, '2', '1', '<sup>fsd f fsd</sup>', '&nbsp;fsd fsd fsd', '2015-06-23 22:08:42', null, '1', '0', null, '', '0', '0');
INSERT INTO `pk_post` VALUES ('8', ' ds vs dvds vs v', 'ds-vs-dvds-vs-v', '', null, '2', '1', '&nbsp;vsd vsd', '&nbsp;svd vsd vds', '2015-06-23 22:09:03', null, '1', '0', null, '', '0', '0');
INSERT INTO `pk_post` VALUES ('9', '31231', '31231', 'hiroyuki-kamogawa-3-1-copyjpg.jpg', null, null, '1', '2312321', 'fsdfds', '2015-12-26 00:10:31', null, '1', '1', null, '', '0', '0');
INSERT INTO `pk_post` VALUES ('10', 'fdsfds', 'fdsfds', 'logojpg.jpg', null, null, '1', 'fsdfsdfsdf', 'sdfsdfsdfsd', '2016-01-05 11:25:11', null, '1', '1', null, '', '0', '1');

-- ----------------------------
-- Table structure for pk_project
-- ----------------------------
DROP TABLE IF EXISTS `pk_project`;
CREATE TABLE `pk_project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `image` text,
  `thumb` text,
  `cate_id` int(11) NOT NULL,
  `desc` text,
  `content` text,
  `focus` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `sort_order` int(4) DEFAULT '0',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_project
-- ----------------------------
INSERT INTO `pk_project` VALUES ('1', 'fsdfds', 'fsdfds', 'yoshino-takashi-4-4-copy.jpg|:|yuta-komori-4-1-copy.jpg|:|yuta-komori-4-2-copy.jpg|:|yuta-komori-4-3.jpg', 'yuta-komori-4-2-copy.jpg', '9', 'fdfds', 'fffffffffffffffffds &nbsp;dddddddddddddđ', '0', '1', '2016-01-05 16:23:23', '0');
INSERT INTO `pk_project` VALUES ('2', '3123213c12', '3123213c12', 'ivan-lee-6-2-copy.jpg', 'ivan-lee-6-2-copy.jpg', '8', 'fsd fds', '&nbsp;fs fsd fds', '0', '1', '2016-01-05 16:23:32', '0');
INSERT INTO `pk_project` VALUES ('3', '1321321', '1321321', 'paulo-7-1-copy.jpg|:|paulo-7-2-copy.jpg', 'paulo-7-1-copy.jpg', '8', 'dsf sdf sd fsd fsd fds', '&nbsp;fds f ds', '0', '1', '2016-01-05 16:24:14', '0');
INSERT INTO `pk_project` VALUES ('4', ' fds fsd fds', 'fds-fsd-fds', 'ryan-walkow-3-3-copy.jpg|:|william-lee-6-1-copy.jpg|:|yuta-komori-4-3.jpg', 'william-lee-6-1-copy.jpg', '8', '', '&nbsp;fds fdsfds fsd&nbsp;', '0', '1', '2016-01-05 16:24:29', '0');
INSERT INTO `pk_project` VALUES ('5', '21x21d1', '21x21d1', 'hiroyuki-kamogawa-3-1-copy.jpg|:|ivan-lee-6-1-copy.jpg', 'hiroyuki-kamogawa-3-1-copy.jpg', '9', '', 'ffffffffffffffff', '0', '1', '2016-01-05 16:24:53', '0');

-- ----------------------------
-- Table structure for pk_role
-- ----------------------------
DROP TABLE IF EXISTS `pk_role`;
CREATE TABLE `pk_role` (
  `role_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_label` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acl_desc` text COLLATE utf8_unicode_ci,
  `p_id` smallint(5) unsigned DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `admin_use` int(1) DEFAULT '0',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of pk_role
-- ----------------------------
INSERT INTO `pk_role` VALUES ('1', 'admin', 'Administrator', 'ALL_PRIVILEGES', '3', '0000-00-00 00:00:00', '2012-11-20 16:35:38', '1', '1');
INSERT INTO `pk_role` VALUES ('3', 'quan-tri-thanh-vien', 'Quản trị thành viên ', 'null', null, '0000-00-00 00:00:00', '2013-08-22 16:09:25', '1', '0');
INSERT INTO `pk_role` VALUES ('5', 'admin_article', 'Quản trị Bài viết', 'a:4:{s:15:\"AdminController\";a:1:{i:0;s:7:\"publish\";}s:18:\"AdsblockController\";a:8:{i:0;s:13:\"testheaderxml\";i:1;s:5:\"admin\";i:2;s:6:\"create\";i:3;s:6:\"update\";i:4;s:6:\"delete\";i:5;s:14:\"deleteselected\";i:6;s:7:\"publish\";i:7;s:9:\"unpublish\";}s:16:\"AdwardController\";a:8:{i:0;s:5:\"admin\";i:1;s:6:\"create\";i:2;s:6:\"update\";i:3;s:6:\"delete\";i:4;s:7:\"publish\";i:5;s:9:\"unpublish\";i:6;s:14:\"deleteselected\";i:7;s:10:\"updateinfo\";}s:18:\"CategoryController\";a:8:{i:0;s:5:\"admin\";i:1;s:6:\"create\";i:2;s:6:\"update\";i:3;s:6:\"delete\";i:4;s:7:\"publish\";i:5;s:9:\"unpublish\";i:6;s:8:\"loadtype\";i:7;s:14:\"deleteselected\";}}', '1', null, '2013-07-30 09:15:32', '1', '1');
INSERT INTO `pk_role` VALUES ('6', 'cong-tac-vien', 'Cộng tác viên', 'null', null, '2013-06-25 01:48:09', null, '1', '0');
INSERT INTO `pk_role` VALUES ('7', 'test', 'Nhóm test', 'a:2:{s:17:\"TimeuseController\";a:1:{i:0;s:6:\"update\";}s:14:\"NodeController\";a:7:{i:0;s:5:\"admin\";i:1;s:6:\"create\";i:2;s:6:\"update\";i:3;s:6:\"delete\";i:4;s:14:\"deleteselected\";i:5;s:7:\"publish\";i:6;s:9:\"unpublish\";}}', '5', '2014-07-21 03:50:15', '2014-07-21 15:50:30', '1', '1');

-- ----------------------------
-- Table structure for pk_services
-- ----------------------------
DROP TABLE IF EXISTS `pk_services`;
CREATE TABLE `pk_services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `image` text,
  `cate_id` tinyint(4) DEFAULT NULL,
  `language` int(4) NOT NULL DEFAULT '1',
  `description` text,
  `content` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `focus` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(4) DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `views` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`service_id`),
  FULLTEXT KEY `title` (`title`,`content`,`description`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_services
-- ----------------------------
INSERT INTO `pk_services` VALUES ('1', '2321', '343243', null, '5', '1', null, 'g reger gre gẻ gre gre', null, null, '1', '0', null, '', '0');
INSERT INTO `pk_services` VALUES ('2', '423cr23rv23r23', 'dsg-s', null, '5', '1', null, ' vdf gfd gẻ g34', null, null, '1', '0', null, '', '0');

-- ----------------------------
-- Table structure for pk_settings
-- ----------------------------
DROP TABLE IF EXISTS `pk_settings`;
CREATE TABLE `pk_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(64) NOT NULL DEFAULT 'system',
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `isnumber` tinyint(4) DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`),
  KEY `category_key` (`category`,`key`)
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_settings
-- ----------------------------
INSERT INTO `pk_settings` VALUES ('3', 'system', 'mailchimp_api', 's:36:\"94b6f8e608a7e1866b5afae709e35d99-us6\";', '0', null);
INSERT INTO `pk_settings` VALUES ('4', 'system', 'mailchimp_listId', 's:10:\"83c9b339f6\";', '0', null);
INSERT INTO `pk_settings` VALUES ('5', 'system', 'mailchimp_apiUrl', 's:29:\"http://api.mailchimp.com/1.3/\";', '0', null);
INSERT INTO `pk_settings` VALUES ('20', 'system', 'copyright', 's:35:\"© 2015 J4S.vn. All rights reserved\";', '0', '');
INSERT INTO `pk_settings` VALUES ('21', 'system', 'sendmail_name', 's:7:\"Fairway\";', '0', '');
INSERT INTO `pk_settings` VALUES ('22', 'system', 'sendmail_email', 's:23:\"cuongphu.nd16@gmail.com\";', '0', '');
INSERT INTO `pk_settings` VALUES ('23', 'system', 'sendmail_manual', 'i:0;', '1', '');
INSERT INTO `pk_settings` VALUES ('24', 'system', 'sendmail_server', 's:14:\"smtp.gmail.com\";', '0', null);
INSERT INTO `pk_settings` VALUES ('25', 'system', 'sendmail_account', 's:15:\"info@fairway.vn\";', '0', '');
INSERT INTO `pk_settings` VALUES ('26', 'system', 'sendmail_password', 's:9:\"truongj4s\";', '0', '');
INSERT INTO `pk_settings` VALUES ('27', 'system', 'sendmail_port', 'i:587;', '1', '');
INSERT INTO `pk_settings` VALUES ('28', 'system', 'sendmail_ssl', 'i:1;', '1', null);
INSERT INTO `pk_settings` VALUES ('29', 'system', 'mailchimp_api1', 's:36:\"94b6f8e608a7e1866b5afae709e35d99-us6\";', '0', null);
INSERT INTO `pk_settings` VALUES ('68', 'system', 'meta_description', 's:7:\"Fairway\";', '0', '');
INSERT INTO `pk_settings` VALUES ('69', 'system', 'description', 's:7:\"Fairway\";', '0', '');
INSERT INTO `pk_settings` VALUES ('73', 'system', 'meta_title', 's:7:\"Fairway\";', '0', '');
INSERT INTO `pk_settings` VALUES ('74', 'system', 'meta_keyword', 's:7:\"Fairway\";', '0', '');
INSERT INTO `pk_settings` VALUES ('93', 'system', 'hotline', 's:11:\"04 36330559\";', '0', 'Số điện thoại hỗ trợ Hotline');
INSERT INTO `pk_settings` VALUES ('102', 'system', 'info_contact_address', 's:64:\"Đ/c: 9 floor, HD building, 57 Tran Quoc Toan, Hoan Kiem, Ha Noi\";', '0', 'Địa chỉ Liên hệ');
INSERT INTO `pk_settings` VALUES ('137', 'system', 'support', 's:424:\"a:4:{i:0;a:3:{s:4:\"type\";s:1:\"1\";s:5:\"title\";s:36:\"Hỗ trợ bán hàng trực tuyến\";s:4:\"nick\";s:10:\"vyquanghoa\";}i:1;a:3:{s:4:\"type\";s:1:\"1\";s:5:\"title\";s:23:\"Hỗ trợ kỹ thuật\";s:4:\"nick\";s:18:\"twinkling_star_169\";}i:2;a:3:{s:4:\"type\";s:1:\"2\";s:5:\"title\";s:20:\"Tư vấn bán hàng\";s:4:\"nick\";s:10:\"mycaalecsu\";}i:3;a:3:{s:4:\"type\";s:1:\"2\";s:5:\"title\";s:17:\"Tư vấn website\";s:4:\"nick\";s:12:\"oanhptk.pham\";}}\";', '0', null);
INSERT INTO `pk_settings` VALUES ('138', 'system', 'payment', 's:1044:\"a:4:{i:0;a:2:{s:5:\"title\";s:21:\"Đến thu tận nhà\";s:4:\"desc\";s:327:\"Đến tận nhà thu\r\nNội dung giới thiệu phương thức thanh toán bằng thẻ ghi nợ nội địa hoặc visa\r\nNội dung giới thiệu phương thức thanh toán bằng thẻ ghi nợ nội địa hoặc visa. Nội dung giới thiệu phương thức thanh toán bằng thẻ ghi nợ nội địa hoặc visa\";}i:1;a:2:{s:5:\"title\";s:26:\"Thanh toán tại công ty\";s:4:\"desc\";s:47:\"Đến trực tiếp công ty để thanh toán\";}i:2;a:2:{s:5:\"title\";s:30:\"Chuyển khoản trực tiếp\";s:4:\"desc\";s:44:\"Chuyển khoản trực tiếp bằng TK ATM\";}i:3;a:2:{s:5:\"title\";s:39:\"Thẻ tín dụng hoặc thẻ ghi nợ\";s:4:\"desc\";s:306:\"Nội dung giới thiệu phương thức thanh toán bằng thẻ ghi nợ nội địa hoặc visa. Nội dung giới thiệu phương thức thanh toán bằng thẻ ghi nợ nội địa hoặc visa. Nội dung giới thiệu phương thức thanh toán bằng thẻ ghi nợ nội địa hoặc visa. \";}}\";', '0', null);
INSERT INTO `pk_settings` VALUES ('139', 'system', 'position', 's:270:\"a:5:{i:0;a:1:{s:5:\"title\";s:22:\"Giám đốc Marketing\";}i:1;a:1:{s:5:\"title\";s:19:\"Trưởng phòng IT\";}i:2;a:1:{s:5:\"title\";s:24:\"Nhân viên lập trình\";}i:3;a:1:{s:5:\"title\";s:22:\"Nhân viên kinh doanh\";}i:4;a:1:{s:5:\"title\";s:27:\"Nhân viện vận chuyển\";}}\";', '0', null);
INSERT INTO `pk_settings` VALUES ('152', 'system', 'link_rss', 's:17:\"http://fairway.vn\";', '0', 'Link RSS');
INSERT INTO `pk_settings` VALUES ('153', 'system', 'share_content', 's:7:\"Fairway\";', '0', 'Nội dung chia sẻ mạng xã hôi');
INSERT INTO `pk_settings` VALUES ('155', 'system', 'website_name', 's:7:\"Fairway\";', '0', 'Tên website tiếng Việt');
INSERT INTO `pk_settings` VALUES ('156', 'system', 'shareImage', 's:0:\"\";', '0', 'Ảnh chia sẻ mạng xã hội');
INSERT INTO `pk_settings` VALUES ('164', 'system', 'sendmail_sign', 's:8:\"<br />\r\n\";', '0', 'Nội dung chữ ký gửi mail');
INSERT INTO `pk_settings` VALUES ('173', 'system', 'sendmail_admin', 's:15:\"info@fairway.vn\";', '0', 'email quản trị nhận thông tin');
INSERT INTO `pk_settings` VALUES ('208', 'system', 'site_logo', 's:0:\"\";', '0', 'Logo Công ty');
INSERT INTO `pk_settings` VALUES ('218', 'system', 'facebook_fanpage', 's:309:\"<div class=\"fb-page\" data-href=\"https://www.facebook.com/facebook\" data-hide-cover=\"false\" data-show-facepile=\"true\" data-show-posts=\"true\"><div class=\"fb-xfbml-parse-ignore\"><blockquote cite=\"https://www.facebook.com/facebook\"><a href=\"https://www.facebook.com/facebook\">Facebook</a></blockquote></div></div>\";', '0', 'Link fanpage facebook');
INSERT INTO `pk_settings` VALUES ('231', 'system', 'link_youtube', 's:0:\"\";', '0', '');
INSERT INTO `pk_settings` VALUES ('232', 'system', 'link_googleplus', 's:0:\"\";', '0', '');
INSERT INTO `pk_settings` VALUES ('233', 'system', 'link_facebook', 's:0:\"\";', '0', '');
INSERT INTO `pk_settings` VALUES ('234', 'system', 'link_twitter', 's:0:\"\";', '0', '');
INSERT INTO `pk_settings` VALUES ('252', 'system', 'working_time', 's:33:\"Thứ 2 - Thứ 7 : 08:00 - 18:00\";', '0', 'Thời gian làm việc');
INSERT INTO `pk_settings` VALUES ('253', 'system', 'company_name', 's:20:\"FAIRWAY PROPERTY JSC\";', '0', 'Tên công ty');
INSERT INTO `pk_settings` VALUES ('254', 'system', 'hotline_footer', 's:37:\"Tel: 04 8888 9999 - Fax: 04 8888 9998\";', '0', 'Điện thoại ở chân trang');

-- ----------------------------
-- Table structure for pk_sponsor
-- ----------------------------
DROP TABLE IF EXISTS `pk_sponsor`;
CREATE TABLE `pk_sponsor` (
  `sponsor_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `image` text,
  `link` varchar(255) DEFAULT NULL,
  `sort_order` int(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `type` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`sponsor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_sponsor
-- ----------------------------
INSERT INTO `pk_sponsor` VALUES ('2', 'ELLE', 'elle', '6jpg.jpg', '', null, '2015-07-16 22:10:43', '1', '0');
INSERT INTO `pk_sponsor` VALUES ('3', 'PNE', 'pne', '4jpg.jpg', '', null, '2015-07-16 22:18:01', '1', null);

-- ----------------------------
-- Table structure for pk_testimonial
-- ----------------------------
DROP TABLE IF EXISTS `pk_testimonial`;
CREATE TABLE `pk_testimonial` (
  `testimonial_id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `positions` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `image` text,
  `created_at` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `language` tinyint(1) DEFAULT '1',
  `sort_order` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`testimonial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_testimonial
-- ----------------------------
INSERT INTO `pk_testimonial` VALUES ('1', 'Hoàng Văn Bình', 'Nhân viên lập trình', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna&nbsp;<br />\r\naliquam erat volutpat. Nam vitae felis pretium, euismod ipsum nec, placerat turpis. Aenean eu gravida arcu, et consectetur&nbsp;<br />\r\norci Quisque posuere dolor in malesuada fermentum.', 'doctor-imgpng.png', '2015-05-29 21:41:30', '1', '1', null);
INSERT INTO `pk_testimonial` VALUES ('2', 'Chung Văn Nam', 'Marketing FPT Shop', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Nam vitae felis pretium, euismod ipsum nec, placerat turpis. Aenean eu gravida arcu, et consectetur orci Quisque posuere dolor in malesuada fermentum.', 'appointment-imgjpg.jpg', '2015-05-29 21:56:48', '1', '1', null);

-- ----------------------------
-- Table structure for pk_user
-- ----------------------------
DROP TABLE IF EXISTS `pk_user`;
CREATE TABLE `pk_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT '0',
  `user_type` tinyint(1) DEFAULT NULL COMMENT '0- ung vien;1- NTD',
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(50) NOT NULL,
  `amount` float NOT NULL DEFAULT '0' COMMENT 'So tien trong tk cua ntd',
  `active` varchar(200) DEFAULT NULL,
  `code` varchar(32) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `birthday` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `email2` varchar(100) DEFAULT NULL,
  `confirm_email` varchar(100) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `images_cmtnd` varchar(255) DEFAULT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `region_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `ward_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `born_country_id` int(11) NOT NULL DEFAULT '0',
  `born_region_id` int(11) NOT NULL DEFAULT '0',
  `born_district_id` int(11) NOT NULL DEFAULT '0',
  `born_ward_id` int(11) NOT NULL DEFAULT '0',
  `born_address` varchar(255) DEFAULT NULL,
  `work_place` varchar(255) DEFAULT NULL,
  `work_place2` varchar(255) DEFAULT NULL,
  `type_of_work` int(4) DEFAULT NULL,
  `expected_salary` varchar(20) DEFAULT NULL,
  `job_category` varchar(255) NOT NULL,
  `about` varchar(255) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `phone` varchar(255) DEFAULT '',
  `mobile` varchar(15) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `gender` tinyint(1) DEFAULT NULL,
  `nick` varchar(100) DEFAULT NULL,
  `skill` text,
  `reference_list` text COMMENT 'chuoi danh sach tham khao',
  `receive_newsletter` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Nhan ban tin dinh ky',
  `receive_notification` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ung vien - nhan thong bao viec lam; NTD - nhan thong bao ho so',
  `company_name` varchar(255) DEFAULT '' COMMENT 'Ten cong ty (chi NTD co)',
  `company_name2` varchar(255) DEFAULT NULL COMMENT 'Ten giao dich',
  `business_field_id` int(11) NOT NULL DEFAULT '0',
  `business_size_id` int(11) NOT NULL DEFAULT '0',
  `business_description` text,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(20) DEFAULT NULL,
  `contact_address` varchar(255) DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `fb_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `hightlight` tinyint(1) DEFAULT NULL,
  `star_count` float NOT NULL DEFAULT '0',
  `education` text COMMENT 'chuoi serialize mang hoc van',
  `work_experience` text COMMENT 'chuoi serialize mang kinh nghiem lam viec',
  `language_field` varchar(50) DEFAULT NULL,
  `criteria_job_notice` text NOT NULL COMMENT 'Tieu chi thong bao viec lam cua ung vien',
  `review_value` decimal(2,1) NOT NULL DEFAULT '0.0',
  `expired_link` datetime NOT NULL,
  `employer_viewed_count` int(10) NOT NULL DEFAULT '0',
  `focus_logo` tinyint(1) DEFAULT '0',
  `type_account` tinyint(1) DEFAULT NULL,
  `sort_order` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9858665 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pk_user
-- ----------------------------
INSERT INTO `pk_user` VALUES ('7572847', '1', null, null, 'c1e3ef2915825556418f6d047df80cca', '1fce59502fcd45872ce8c02c28021b97', '0', null, '', 'Super', 'Admin', '', 'admin@j4s.vn', '', null, '10253909764959490240455688128836233198042njpg.jpg', null, '0', null, '262', null, '', '0', '0', '262', '0', '', null, null, null, null, '', '', '1', '0000-00-00 00:00:00', '', '', null, null, '2015-05-27 16:46:29', null, '0', null, null, '1', null, null, null, null, '1', '0', '', '', '0', '0', null, null, null, null, null, null, null, null, '0', null, null, null, '', '0.0', '0000-00-00 00:00:00', '0', '0', '0', null);
INSERT INTO `pk_user` VALUES ('3589841', '0', '0', null, '5ea67773f941a7500e850f14ca97ee87', 'c141effdd36ee576f133147b25ddbfe9', '0', null, '', 'Cường', 'Vũ', null, 'cuong.vu@htecom.vn', 'cuongphu.nd16@gmail.com', null, '10253909764959490240455688128836233198042njpg.jpg', null, '260', '4564', '1', null, 'vghch', '260', '4628', '289', '0', 'Trần Hưng Đạo', ',4564,4564,', ',1,2,', '4', '', '18,35,8', '', null, '2015-04-21 08:41:53', '0436249535', '0948029067', '2014-12-24 14:36:58', null, '2015-04-25 14:16:43', 'New pass form', '0', null, null, '1', '0', null, null, null, '1', '0', '', '', '0', '0', null, null, null, null, null, null, '104652272210050282786', null, '0', null, null, '2', '', '0.6', '2015-04-24 10:47:57', '3', '0', '0', null);
INSERT INTO `pk_user` VALUES ('1549694', '0', '1', null, '1af841f4d09cf75011bd8fa341a1a685', '9f61371b4eb9140a5a6fbb0f979b5435', '0', null, '', 'Oanh', 'Phạm', '16/09/1984', 'oanh.pham@htecom.vn', 'kio984@gmail.com', null, 'SelectionRecruitment-128png.png', null, '260', '4564', '1', null, 'Số 189, nguyễn đức cảnh', '260', '4564', '1', '0', 'Số 189, nguyễn đức cảnh', null, null, null, null, '', '', null, '2015-01-28 15:04:56', '0469898899', '0989803236', '2014-12-30 15:47:31', null, '2015-03-06 17:08:13', null, '0', null, null, '1', '1', '', '', null, '1', '0', 'ht', '', '0', '0', 'Mô tả', '', '', null, '', null, null, null, '0', null, null, '', '', '0.0', '0000-00-00 00:00:00', '0', '0', '0', null);
INSERT INTO `pk_user` VALUES ('9858664', '0', '1', null, '59ffeaeeac68d3da10a1c0008951b281', 'a80aca32650e66832d181e346bbdb217', '0', null, '', 'admin', 'admin', null, 'admin', null, null, null, null, '0', null, null, null, null, '0', '0', '0', '0', null, null, null, null, null, '', '', '1', null, '', '', null, null, null, null, '0', null, null, '1', null, null, null, null, '1', '0', '', null, '0', '0', null, null, null, null, null, null, null, null, '0', null, null, null, '', '0.0', '0000-00-00 00:00:00', '0', '0', null, null);
