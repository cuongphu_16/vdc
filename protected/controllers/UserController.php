<?php

class UserController extends Controller {

    public function actions() {
	return array(
	    'captcha' => array(
		'class' => 'CCaptchaAction',
		'backColor' => 0x666666,
		'foreColor' => 0xFFFFFF,
		'maxLength' => 5,
		'minLength' => 6,
		'padding' => 1,
		'testLimit' => 0,
	    ),
	    'page' => array(
		'class' => 'CViewAction',
	    ),
	);
    }

    public function getProtocol() {
	if (Yii::app()->request->isSecureConnection)
	    return 'https';
	return 'http';
    }

    public function actionLogin() {
	if (!Yii::app()->user->isGuest)
	    $this->redirect(Yii::app()->createUrl('main/index'));
	$this->pageTitle = Yii::t('main', 'Sign in your account');
	$this->pageDesc = Controller::settingGet("meta_description");
	$this->pageKey = Controller::settingGet("meta_keyword");
	$this->shareTitle = $this->settingGet('meta_title');
	$this->shareContent = Controller::settingGet("share_content") != '' ? Controller::settingGet("share_content") : Controller::settingGet("meta_description");
	$this->shareLink = $this->getProtocol() . "://" . Yii::app()->request->serverName . Yii::app()->request->requestUri;

	$serviceName = Yii::app()->request->getQuery('service');
	if (isset($serviceName)) {
	    /** @var $eauth EAuthServiceBase */
	    $eauth = Yii::app()->eauth->getIdentity($serviceName);
	    $eauth->redirectUrl = Yii::app()->user->returnUrl;
	    $eauth->cancelUrl = Yii::app()->createUrl('main/index');
	    try {
		if ($eauth->authenticate()) {
//var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes());
		    $identity = new EAuthUserIdentity($eauth);
// successful authentication
		    if ($identity->authenticate()) {
//                        Yii::app()->user->login($identity);
//var_dump($identity->id, $identity->name, Yii::app()->user->id);exit;
// special redirect with closing popup window
			$user = User::model()->findByAttributes(array('email' => $eauth->email));
			$password = '111111';
			if (!empty($user)) {
			    $login = new SiginForm('face');
			    $login->attributes = array(
				'username' => $user->email,
				'password' => $password,
			    );
			    $error = CActiveForm::validate($login);
			    if ($error == '[]' && $login->login()) {
				if ($user->fb_id != $identity->id && $serviceName == 'facebook') {
				    $user->fb_id = $identity->id;
				} elseif ($user->google_id != $identity->id && $serviceName == 'google_oauth') {
				    $user->google_id = $identity->id;
				}
				if ($user->save()) {
				    if ($user->user_type == User::TYPE_USER_EMPLOYER) {
					if (isset(Yii::app()->session['redirect_url']) && !empty(Yii::app()->session['redirect_url'])) {
					    $url = Yii::app()->session['redirect_url'];
					    unset(Yii::app()->session['redirect_url']);
					    $eauth->redirect($url);
					} else
					    $eauth->redirect($this->createAbsoluteUrl("employer/index/lang/" . $this->langCode));
				    } else if ($user->user_type == User::TYPE_USER_CANDIDATE) {
					$eauth->redirect($this->createAbsoluteUrl($this->langCode . "/"));
				    } else
					$eauth->redirect(Yii::app()->user->returnUrl);
				} else {
				    print_r($user->getErrors());
				    die;
				}
			    }
			} else {
			    $user = new User;
//print_r($eauth);die();
//                            $password = vH_Utils::generateRandomString(10);
			    $username_ = @explode('@', $eauth->email);
			    $user->username = $eauth->email;

			    if ($serviceName == 'facebook') {
				$user->fb_id = $identity->id;
			    } elseif ($serviceName == 'google_oauth') {
				$user->google_id = $identity->id;
			    }
			    $user->password = $password;
			    $user->firstname = $eauth->name;

			    $error = CActiveForm::validate($user);
			    if ($error == '[]' && $user->save()) {
				$login = new SiginForm('face');
				$login->attributes = array(
				    'username' => $user->email,
				    'password' => $password,
				);
				$error2 = CActiveForm::validate($login);
				if ($login->login()) {
				    $mailer = SiteHelper::loadMailer();
				    $mailer->clearAddresses();
				    $mail_body = $this->renderPartial("/mail/mail_register", array('model' => $user, 'text_pass' => $password), true);
				    $mailer->AddAddress($user->email);
				    $mailer->Subject = Yii::t('main', 'Register Successful');
				    $mailer->Body = $mail_body;
				    $mailer->send();
				    if ($user->user_type == User::TYPE_USER_EMPLOYER) {
					$eauth->redirect($this->createAbsoluteUrl("employer/index/lang/" . $this->langCode));
				    } else if ($user->user_type == User::TYPE_USER_CANDIDATE) {
					$eauth->redirect($this->createAbsoluteUrl($this->langCode . "/"));
				    } else
					$eauth->redirect(Yii::app()->user->returnUrl);
				} else {
				    print_r($error2);
				    die();
				}
			    }
			}
		    } else {
// close popup window and redirect to cancelUrl                       
			$eauth->cancel();
		    }
		}
// Something went wrong, redirect to login page
		$this->redirect(Yii::app()->createUrl('main/index'));
	    } catch (EAuthException $e) {
// save authentication error to session
		Yii::app()->user->setFlash('error', 'EAuthException: ' . $e->getMessage());
		echo $e->getMessage();
		die;
// close popup window and redirect to cancelUrl
		$eauth->redirect($eauth->getCancelUrl());
	    }
	}
	$model = new SiginForm;

	if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
	    echo CActiveForm::validate($model);
	    Yii::app()->end();
	}

	if (isset($_POST['SiginForm'])) {
	    $model->attributes = $_POST['SiginForm'];
	    if ($model->validate() && $model->login()) {
		User::model()->updateByPk(Yii::app()->user->id, array(
		    'last_login' => date('Y-m-d H:i:s', time())
		));
		$user = User::model()->findByAttributes(array('email' => $model->username));
		if (!empty($_GET['return'])) {
		    $this->redirect($_GET['return']);
		} else
		    $this->redirect(Yii::app()->homeUrl);
	    }
	}

	$this->render("login", array(
	    'model' => $model,
	));
    }

    public function actionRegister() {
	if (!Yii::app()->user->isGuest)
	    $this->redirect(Yii::app()->homeUrl);
	$this->pageTitle = Yii::t('main', 'Create your candidate account');
	$this->pageDesc = Controller::settingGet("meta_description");
	$this->pageKey = Controller::settingGet("meta_keyword");
	$this->shareTitle = $this->settingGet('meta_title');
	$this->shareContent = Controller::settingGet("share_content") != '' ? Controller::settingGet("share_content") : Controller::settingGet("meta_description");
	$this->shareLink = $this->getProtocol() . "://" . Yii::app()->request->serverName . Yii::app()->request->requestUri;
	$model = new User('frontcreate');
	if (isset($_POST['ajax']) && $_POST['ajax'] === 'new_register') {
	    echo CActiveForm::validate($model);
	    Yii::app()->end();
	}
	if (isset($_POST['User'])) {
	    $model->attributes = $_POST['User'];
	    $username = $_POST['User']['username'];
	    if ($model->validate()) {
//		$model->status = 0;
		$text_pass = $_POST['User']['password'];
		if ($model->save()) {
		    $user = User::model()->findByAttributes(array('username' => $username));
		    $password = '111111';
		    if (!empty($user)) {
			$login = new SiginForm('face');
			$login->attributes = array(
			    'username' => $user->username,
			    'password' => $password,
			);
			$error = CActiveForm::validate($login);
			if ($error == '[]')
			    $login->login();
		    }

		    MailHelper::sendMailRegister($model, $text_pass);
		    Yii::app()->session['register_success'] = 'true';
		    $this->redirect($this->createUrl('main/index'));
		}
	    }
	}
	$this->render("register", array(
	    'model' => $model
	));
    }

    public function actionLogout() {
	Yii::app()->user->logout();
	$this->redirect(Yii::app()->request->urlReferrer);
	Yii::app()->end();
    }

    public function actionInformation() {
	if (Yii::app()->user->isGuest)
	    $this->redirect(Yii::app()->homeUrl);
	$this->pageTitle = Yii::t('main', 'Information Member');
	$this->pageDesc = Controller::settingGet("meta_description");
	$this->pageKey = Controller::settingGet("meta_keyword");
	$this->shareTitle = Yii::t('main', 'Information Member');
	$this->shareContent = Controller::settingGet("share_content") != '' ? Controller::settingGet("share_content") : Controller::settingGet("meta_description");
	$this->shareLink = $this->getProtocol() . "://" . Yii::app()->request->serverName . Yii::app()->request->requestUri;

	$model2 = new ChangePass2;
	if (isset($_POST['ajax']) && $_POST['ajax'] === 'edit_pass') {
	    echo CActiveForm::validate($model2);
	    Yii::app()->end();
	}
	if (isset($_POST['ChangePass2'])) {
	    $model2->attributes = $_POST['ChangePass2'];
	    if ($model2->validate()) {
		$salt = Yii::app()->params['salt'];
		$password = vH_Utils::userPass($model2->password_new, $salt);

		$user = User::model()->findByPk(Yii::app()->user->id);
		$user->password = $password;
		$user->salt = $salt;
		if ($user->update()) {
		    Yii::app()->session['status_change_pass'] = 'Thay đổi mật khẩu thành công';
		    $this->redirect(Yii::app()->createUrl('user/information'));
		} else {
		    Yii::app()->session['status_change_pass'] = 'Thay đổi mật khẩu không thành công';
		    $this->refresh();
		}
	    }
	}


	$model = User::model()->findByPk(Yii::app()->user->id);
	$model->setScenario('edit');
	if (isset($_POST['ajax']) && $_POST['ajax'] === 'member_form') {
	    echo CActiveForm::validate($model);
	    Yii::app()->end();
	}
	if (isset($_POST['User'])) {
	    $model->attributes = $_POST['User'];
	    if ($model->validate()) {
		$model->save();
		Yii::app()->session['status_update_info'] = 'Cập nhật thông tin tài khoản thành công';
		$this->refresh();
	    }
	}


	//theo doi truyen
	$cri_log_ajax = new CDbCriteria(array('order' => 'created_at DESC'));
	$total_log = LogChapter::model()->countByAttributes(array('user_id' => Yii::app()->user->id), $cri_log_ajax);
	$page = isset($_GET['page']) ? $_GET['page'] : 1;
	if (!isset($total_log))
	    $total_log = 0;
	$limit = 5;
	if ($total_log % $limit == 0) {
	    $total_page = $total_log / $limit;
	} else {
	    $total_page = intval($total_log / $limit) + 1;
	}
	$this->render('information', array('page' => $page, 'total_pages' => $total_page, 'model2' => $model2, 'model' => $model));
    }

    public function actionAjaxLog() {
	if (isset($_POST['page']))
	    $page = intval($_POST['page']);
	else
	    $page = 1;

	$total_log = LogChapter::model()->countByAttributes(array('user_id' => Yii::app()->user->id));
	if (!isset($total_log))
	    $total_log = 0;
	$limit = 5;
	if ($total_log % $limit == 0) {
	    $total_page = $total_log / $limit;
	} else {
	    $total_page = intval($total_log / $limit) + 1;
	}
	$begin = ($page - 1) * $limit;
	$end = $page * $limit;
	$endpage = '';
	$beginpage = '';
	if ($page == $total_page) {
	    $endpage = false;
	} else {
	    $endpage = true;
	}
	if ($page == 1) {
	    $beginpage = false;
	} else {
	    $beginpage = true;
	}
	$criteria = new CDbCriteria();
	$criteria->order = "created_at DESC";
	$criteria->limit = $limit;
	$criteria->offset = $begin;
	$log = LogChapter::model()
		->findAllByAttributes(array('user_id' => Yii::app()->user->id), $criteria);

	$this->layout = "//layouts/null";
	$this->renderPartial('ajax_log', array('log' => $log, 'pages' => $page, 'total' => $total_log, 'limit' => $limit, 'total_page' => $total_page), false, true);
    }

    public function actionRemoveChapter() {
	if (!empty($_POST['id'])) {
	    $log = LogChapter::model()->findByPk($_POST['id']);
	    if ($log->delete()) {
		echo 'remove success';
		Yii::app()->end();
	    }
	}
    }

}

?>