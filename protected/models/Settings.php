<?php

class Settings extends CommonAR {

    public $title1, $title2, $title3, $title4, $title5, $title6, $title7, $title8, $title9;
    public $content1, $content2, $content3, $content4, $content5, $content6, $content7, $content8, $content9, $content10, $content11, $content12, $content13, $content14, $content15, $content16, $content17;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'Settings';
    }

    public function tableName() {
        return '{{settings}}';
    }

     public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(),
                'defaultStickOnClear' => false
            ),
        );
    }

    public function rules() {
        return array(
            array('key', 'unique'),
            array('key', 'required'),
            array('isnumber', 'numerical', 'integerOnly' => true),
            array(
                'key', 'match', 'not' => true, 'pattern' => '/[^a-zA-Z0-9_-]/',
                'message' => 'Chỉ được sử dụng các ký tự a-z, A-Z, 0-9 và dấu gạch chân ( _ ), gạch ngang ( - )', 'on' => 'update, create'
            ),
            array('key, description', 'length', 'max' => 255),
            array('value, isnumber', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, key, value, isnumber, description', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'key' => 'Tên',
            'value' => 'Nội dung',
            'isnumber' => 'Nội dung là số',
            'description' => 'Mô tả'
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->value = serialize($this->value);
            return true;
        }
        return false;
    }

    protected function afterSave() {
        if ($this->isnumber)
            Controller::settingSet($this->key, (int) unserialize($this->value));
        else
            Controller::settingSet($this->key, unserialize($this->value));
        return parent::afterSave();
    }

    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            Controller::settingDelete();
            return true;
        } else
            return false;
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('`key`', $this->key, true);
        $criteria->addCondition("`key` NOT IN ('support','payment','position', 'percentage_enjoy')");
        //$criteria->compare('value',$this->value,true);
        if ($this->value)
            $criteria->addCondition("MATCH (value) AGAINST ('{$this->value}' IN BOOLEAN MODE)");

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'id DESC'),
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

}
