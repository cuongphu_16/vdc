<?php
class Mailtemp extends CommonAR
{	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getResourceName()
	{
		return 'Mailtemp';
	}
	
	public function tableName()
	{
		return '{{mailtemp}}';
	}
	
	public function rules()
	{
		return array(
			array('key', 'unique'),
			array('key','required'),
			array('isnumber', 'numerical', 'integerOnly'=>true),
			array(
			  'key', 'match', 'not' => true, 'pattern' => '/[^a-zA-Z0-9_-]/',
			  'message' => 'Chỉ được sử dụng các ký tự a-z, A-Z, 0-9 và dấu gạch chân ( _ ), gạch ngang ( - )', 'on' => 'update, create'
			),
			
			array('key', 'length', 'max'=>255),
			array('value, isnumber', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, key, value, isnumber', 'safe', 'on'=>'search'),
		);
	}
	
	
	public function relations()
	{
		return array(
			
		);
	}	
	
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'key' => 'Tên',
			'value' => 'Nội dung',
			'isnumber' => 'Nội dung là số',
		);
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave()){			
			$this->value = serialize($this->value);
			return true;
		}
		return false;
	}
	
	protected function afterSave()
	{
//		if($this->isnumber)
//			Controller::settingSet($this->key,(int)unserialize($this->value));
//		else
//			Controller::settingSet($this->key,unserialize($this->value));
		return parent::afterSave();
	}
	
	protected function beforeDelete()
	{
		if(parent::beforeDelete())
		{
			Controller::settingDelete();
			return true;
		}else
			return false;
	}
	
	
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('`key`',$this->key,true);
		
		//$criteria->compare('value',$this->value,true);
		if($this->value)
			$criteria->addCondition("MATCH (value) AGAINST ('{$this->value}' IN BOOLEAN MODE)");

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}