<?php

/**
 * This is the model class for table "{{ward}}".
 *
 * The followings are the available columns in table '{{ward}}':
 * @property integer $ward_id
 * @property integer $dictrict_id
 * @property integer $region_id
 * @property string $name
 * @property integer $created_at
 * @property string $created_by
 * @property integer $updated_at
 * @property string $updated_by
 * @property integer $deleted
 * @property integer $deleted_at
 * @property string $deleted_by
 * @property integer $status
 */
class Ward extends CommonAR {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'Ward';
    }

    public function tableName() {
        return '{{ward}}';
    }
    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(),
                'defaultStickOnClear' => false
            ),
        );
    }
    public function rules() {
        return array(
            array('title,district_id,region_id', 'required'),
            array('district_id+title', 'ext.uniqueMultiColumnValidator', 'caseSensitive' => true),
            array('ward_id, district_id,region_id, status, country_id', 'numerical', 'integerOnly' => true),
            array('title,', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('ward_id, district_id, country_id, title, region_id, status', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        $a = array(
            'ward_id' => 'Ward',
            'district_id' => 'District',
            'title' => 'Ward',
            'region_id' => 'Region',            
            'country_id'=>'Country',
            'status' => 'Status',
        );
        foreach($a as $key=>$val){
            $a[$key] = Yii::t('admincp',$val);
        }
        return $a;
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('ward_id', $this->ward_id);
        $criteria->compare('country_id', $this->country_id);
        $criteria->compare('region_id', $this->region_id);
        $criteria->compare('district_id', $this->district_id);
        $criteria->compare('title', $this->title, true);        
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'ward_id DESC'),
             'pagination' => array(
                'pageSize' => 100,
            ),
        ));
    }

    public function toOptionHash($filter = array(), $key = 'ward_id', $value = 'title') {
        $return = array();
        $criteria = new CDbCriteria;
        $criteria->order = "title ASC";
        $result = $this->findAllByAttributes((array) $filter, $criteria);
        for ($i = 0; $i < count($result); $i++) {
            $return[$result[$i][$key]] = html_entity_decode($result[$i][$value], ENT_QUOTES);
        }
        return $return;
    }

}
