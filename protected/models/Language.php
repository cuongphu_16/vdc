<?php

/**
 * This is the model class for table "{{language}}".
 *
 * The followings are the available columns in table '{{language}}':
 * @property string $lang_id
 * @property string $title
 * @property string $code
 * @property integer $sort_order
 * @property integer $status
 */
class Language extends CommonAR {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'Language';
    }

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(),
                'defaultStickOnClear' => false
            ),
        );
    }

    public function tableName() {
        return '{{language}}';
    }

    public function rules() {
        return array(
            array('title, code', 'required'),
            array('sort_order, status,is_web', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 50),
            array('code', 'length', 'max' => 10),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('lang_id, title, code, sort_order, is_web, status', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        $a = array(
            'lang_id' => 'ID',
            'title' => 'Title',
            'code' => 'Code',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'is_web'=>'Ngôn ngữ website',
        );
        foreach ($a as $key => $val) {
            $a[$key] = Yii::t('main', $val);
        }
        return $a;
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('lang_id', trim($this->lang_id), true);
        $criteria->compare('title', trim($this->title), true);
        $criteria->compare('code', trim($this->code), true);
        $criteria->compare('sort_order', $this->sort_order);
        $criteria->compare('status', $this->status);
         $criteria->compare('is_web', $this->is_web);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'title ASC'),
        ));
    }
    public function toOptionHash($filter = array(), $key = 'lang_id', $value = 'title',$is_web=false) {
        $return = array();
        $criteria = new CDbCriteria;
        $criteria->order = "title ASC";
        if ($is_web==true)
        {
            $criteria->condition = "is_web=1";
        }
        $result = $this->findAllByAttributes((array) $filter, $criteria);
        for ($i = 0; $i < count($result); $i++) {
            $return[$result[$i][$key]] = html_entity_decode($result[$i][$value], ENT_QUOTES);
        }
        return $return;
    }

}
