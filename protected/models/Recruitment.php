<?php

/**
 * This is the model class for table "{{recruitment}}".
 *
 * The followings are the available columns in table '{{recruitment}}':
 * @property integer $recruitment_id
 * @property string $title
 * @property string $alias
 * @property string $image
 * @property integer $type
 * @property string $description
 * @property string $content
 * @property string $created_at
 * @property string $created_by
 * @property integer $status
 */
class Recruitment extends CommonAR {

    public $upload_dir;

    public function init() {
        $this->upload_dir = Yii::app()->params['uploadPath']['recruitment'];
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'Recruitment';
    }

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(),
                'defaultStickOnClear' => false
            ),
        );
    }

    public function tableName() {
        return '{{recruitment}}';
    }

    public function rules() {
        return array(
            array('title', 'unique'),
            array('title,language,description', 'required'),
            array('type, sort_order, status,focus,quantity', 'numerical', 'integerOnly' => true),
            array('title, alias', 'length', 'max' => 255),
            array('created_by', 'length', 'max' => 50),
            array('image, type_index, description, content, created_at,date_first,date_last, gender, education, experience, dateline, salary, request, benefit, cv', 'safe'),
            array('image', 'file', 'allowEmpty' => true, 'types' => 'jpg, png, jpeg', 'maxSize' => 1024 * 1024 * 5),
            // The following rule is used by search().
// Please remove those attributes that should not be searched.
            array('recruitment_id, type_index, sort_order, quantity, gender, education, experience, dateline, salary, request, benefit, cv, language, title, focus, alias, image, type, description, content, created_at, created_by, status', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        $a = array(
            'recruitment_id' => 'Recruitment',
            'title' => 'Title',
            'alias' => 'Alias',
            'image' => 'Image',
            'type' => 'Type Recruitment',
            'description' => 'Description',
            'content' => 'Content',
            'language' => 'Language',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'status' => 'Status',
            'sort_order' => 'Sort Order',
            'focus' => 'Focus',
            'type_index' => 'Bài viết hot',
            'quantity' => 'Quantity',
            'gender' => 'Gender',
            'education' => 'Education',
            'experience' => 'Experience',
            'dateline' => 'Dateline',
            'salary' => 'Salary',
            'request' => 'Request',
            'benefit' => 'Benefit',
            'cv' => 'Resume',
        );
        foreach ($a as $key => $val) {
            $a[$key] = Yii::t('main', $val);
        }
        return $a;
    }

    public function search($cri = '') {

        $criteria = new CDbCriteria;

        $criteria->compare('recruitment_id', $this->recruitment_id);
        $criteria->compare('title', trim($this->title), true);
        $criteria->compare('alias', trim($this->alias), true);
        $criteria->compare('image', trim($this->image), true);
        $criteria->compare('type', $this->type);
        $criteria->compare('description', trim($this->description), true);
        $criteria->compare('content', trim($this->content), true);
        $criteria->compare('created_at', trim($this->created_at), true);
        $criteria->compare('created_by', trim($this->created_by), true);
        $criteria->compare('status', $this->status);
        $criteria->compare('language', $this->language);
        $criteria->compare('focus', $this->focus);
        if ($cri != "")
            $criteria->addCondition($cri);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'created_at DESC'),
        ));
    }

    public function getImage($width, $height = 0) {
        $width = (int) $width;
        $height = (int) $height;
        if ($width <= 0)
            return false;

        $recruitment_id = $this->recruitment_id;
        $image = $this->image;

        if (empty($this->image)) {
            $recruitment_id = 'default';
            $image = 'logo.png';
        }
        $img_data = Controller::getImageUpload((object) array(
                            'folder' => 'recruitment',
                            'id' => $recruitment_id,
                            'width' => $width,
                            'height' => $height,
                            'filename' => $image
        ));

        if ($img_data->src == '') {
            $img_data = Controller::getImageUpload((object) array(
                                'folder' => 'recruitment',
                                'id' => 'default',
                                'width' => $width,
                                'height' => $height,
                                'filename' => 'logo.png'
            ));
        }
        return $img_data;
    }

    public function getUrl() {
        return Yii::app()->createUrl('main/detailRecruitment/lang/' . Yii::app()->language, array(
                    'url_recruitment' => $this->alias,
        ));
    }

    protected function beforeValidate() {
        if (parent::beforeValidate()) {
// something happens here
            if (!empty($_FILES['Recruitment']['name']['image'])) {
                if (!$this->isNewRecord) {
                    $folder = $this->recruitment_id . '/';
                    $this->deleteImage();
                } else {
                    Yii::app()->session['recruitment_upload_folder'] = $folder = md5($_SERVER['REMOTE_ADDR']) . '_' . time() . '/';
                }
//upload image
                $uploadedFile = vH_Utils::alias($_FILES['Recruitment']['name']['image']) . vH_Utils::fileEx(CUploadedFile::getInstance($this, 'image'), true);
                if (!empty($uploadedFile)) {
                    Yii::app()->session['recruitment_upload_image'] = $uploadedFile = vH_Upload::uploadFile('Recruitment', 'image', $this->upload_dir . $folder, $uploadedFile);
                    $this->image = $uploadedFile;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            if (!empty($_FILES['Recruitment']['name']['image'])) {
                if (!$this->isNewRecord) {
                    $folder = $this->recruitment_id . '/';
                    $this->deleteImage();
                    $uploadedFile = vH_Utils::alias($_FILES['Recruitment']['name']['image']) . vH_Utils::fileEx(CUploadedFile::getInstance($this, 'image'), true);
                    if (!empty($uploadedFile)) {
                        $uploadedFile = vH_Upload::uploadFile('Recruitment', 'image', $this->upload_dir . $folder, $uploadedFile);
                        $this->image = $uploadedFile;
                    }
                } else {
                    $folder = Yii::app()->session['recruitment_upload_folder'];
                }
            }

            if ($this->isNewRecord) {
                $this->created_at = date('Y-m-d H:i:s', time());
            }
            return true;
        } else
            return false;
    }

    protected function afterSave() {
        parent::afterSave();

        if ($this->isNewRecord) {
            if (!empty(Yii::app()->session['recruitment_upload_folder'])):
                $recruitment_upload_folder = Yii::app()->session->get('recruitment_upload_folder');
                rename($this->upload_dir . $recruitment_upload_folder, $this->upload_dir . $this->recruitment_id);
                unset(Yii::app()->session['recruitment_upload_folder']);
            endif;
        }
    }

    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            $this->deleteImage(true);
            return true;
        } else
            return false;
    }

    private function deleteImage($d = false) {
        vH_Utils::rmdirf_($this->upload_dir . $this->recruitment_id);
        if ($d)
            vH_Utils::rmdir_($this->upload_dir . $this->recruitment_id);
    }

}
