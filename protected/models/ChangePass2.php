<?php

class ChangePass2 extends CFormModel {

    public
	    $password_old,
	    $password_new,
	    $password_cf,
	    $verifyCode;

    public function rules() {
	return array(
	    array('password_old,password_new,password_cf', 'required'),
	    array('password_old', 'validatePasswordOld'),
	    array('password_new, password_cf', 'length', 'min' => 6),
	    array('password_cf', 'compare', 'compareAttribute' => 'password_new'),
	);
    }

    public function attributeLabels() {
	$arr = array(
	    'password_old' => 'Old password',
	    'password_new' => 'New password',
	    'password_cf' => 'Retype new password',
	    'verifyCode' => 'Verify code'
	);
	foreach ($arr as $key => $val) {
	    $a[$key] = Yii::t('main', $val);
	}
	return $a;
    }

    public function validatePasswordOld() {
	$user = User::model()->findByPk(Yii::app()->user->id);
	if ($user->password == vH_Utils::userPass($this->password_old, $user->salt))
	    return true;
	$this->addError('password_old', Yii::t('main', 'Old pass word is not correct'));
	return false;
    }

}
