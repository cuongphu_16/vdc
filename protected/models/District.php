<?php

/**
 * This is the model class for table "{{district}}".
 *
 * The followings are the available columns in table '{{district}}':
 * @property integer $district_id
 * @property string $district
 * @property integer $status
 * @property integer $region_id
 * @property integer $sort_order
 */
class District extends CommonAR {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'District';
    }

    public function tableName() {
        return '{{district}}';
    }
    
    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(),
                'defaultStickOnClear' => false
            ),
        );
    }

    public function rules() {
        return array(
            array('title, region_id, country_id', 'required'),
//                        array('district+region_id','unique'),
            array('district_id, status, region_id, sort_order', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('district_id, title, status, region_id, country_id, sort_order', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'wards' => array(self::HAS_MANY, 'Ward', 'district_id', 'order' => 'ward asc'),
            'region' => array(self::BELONGS_TO, 'Regions', 'region_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'district_id' => 'ID',
            'title' => 'Quận / Huyện',
            'status' => 'Trạng thái',
            'region_id' => 'Tỉnh / Thành phố',
            'country_id'=>'Quốc gia',
            'sort_order' => 'Thứ tự',
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('district_id', $this->district_id);
        $criteria->compare('country_id', $this->country_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('region_id', $this->region_id);
        $criteria->compare('sort_order', $this->sort_order);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'district_id DESC'),
             'pagination' => array(
                'pageSize' => 100,
            ),
        ));
    }

    public function toOptionHash($filter = array(), $key = 'district_id', $value = 'title') {
        $return = array();
        $criteria = new CDbCriteria;
        $criteria->order = "title ASC";
        $result = $this->findAllByAttributes((array) $filter, $criteria);
        for ($i = 0; $i < count($result); $i++) {
            $return[$result[$i][$key]] = html_entity_decode($result[$i][$value], ENT_QUOTES);
        }
        return $return;
    }

}
