<?php

class ChangePass extends CFormModel {

    public
            $password_old,
            $password_new,
            $password_cf,
            $verifyCode;

    public function rules() {
        return array(
            array('password_old, password_new, password_cf', 'required'),
            array('verifyCode', 'required', 'message' => Yii::t('main', 'Please enter verify code')),
            array('password_old', 'validatePasswordOld'),
            array('password_old, password_new, password_cf', 'length', 'min' => 6),
            array('password_cf', 'compare', 'compareAttribute' => 'password_new'),
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements(), 'message' => Yii::t('main', 'Verify code is not correct')),
        );
    }

    public function attributeLabels() {
        $arr = array(
            'password_old' => 'Old password',
            'password_new' => 'New password',
            'password_cf' => 'Retype new password',
            'verifyCode' => 'Verify code'
        );
        foreach ($arr as $key => $val) {
            $a[$key] = Yii::t('main', $val);
        }
        return $a;
    }

    public function validatePasswordOld() {
        $user = User::model()->findByPk(Yii::app()->user->id);
        if ($user->password == vH_Utils::userPass($this->password_old, $user->salt))
            return true;
        $this->addError('password_old', Yii::t('main', 'Old password not correct'));
        return false;
    }

}
