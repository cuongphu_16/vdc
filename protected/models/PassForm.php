<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class PassForm extends CFormModel {

    public $email;
    public $verifyCode;
    public $mem;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // username and password are required
            array('email', 'required'),
            array('email', 'email'),
             array('verifyCode', 'required', 'message' => Yii::t('main','Please enter the verify code')),
//            array('email', 'check_email_exist'),
            array('email','exist', 'attributeName' => 'email', 'className' => 'User','message'=>'Email của bạn không tồn tại trên hệ thống'),
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements(), 'message' => 'Mã xác nhận không đúng'),
        );
    }

    public function check_email_exist() {
        $this->mem = User::model()->findByAttributes(array('email' => $this->email));
        if ($this->mem != NULL)
            return TRUE;
        else {
            $this->addError('email', 'Email này không tồn tại trên hệ thống');
            return FALSE;
        }
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        $array = array(
            'email' => 'Email',
            'verifyCode' => 'Mã bảo vệ',
        );

        foreach ($array as $key => $val) {
            $array[$key] = Yii::t('main', $val);
        }

        return $array;
    }

}
