<?php

/**
 * This is the model class for table "{{photos}}".
 *
 * The followings are the available columns in table '{{photos}}':
 * @property integer $photo_id
 * @property string $picture
 * @property integer $sort_order
 * @property integer $post_id
 */
class Photos extends CommonAR {

    public $upload_dir;

    public function init() {
        $this->upload_dir = Yii::app()->params['uploadPath']['photos'];
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'Photos';
    }

    public function tableName() {
        return '{{photos}}';
    }

    public function rules() {
        return array(
            array('sort_order, post_id, showdefault', 'numerical', 'integerOnly' => true),
            array('picture', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('photo_id, picture, sort_order, post_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        return array(
            'photo_id' => 'ID',
            'picture' => 'Ảnh',
            'sort_order' => 'Thứ tự',
            'post_id' => 'Bài đăng',
            'showdefault' => 'Ảnh đại diện'
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            if (!empty($_FILES['Photos']['name']['picture'])) {
                if (!$this->isNewRecord) {
                    $folder = $this->photo_id . '/';
                    $this->deleteImage();
                } else {
                    Yii::app()->session['user_upload_folder'] = $folder = md5($_SERVER['REMOTE_ADDR']) . '_' . time() . '/';
                }
                //upload thumb
                $uploadedFile = vH_Utils::alias(CUploadedFile::getInstance($this, 'picture')).'_'.time() . vH_Utils::fileEx(CUploadedFile::getInstance($this, 'picture'), true);
                if (!empty($uploadedFile)) {
                    $uploadedFile = vH_Upload::uploadFile('Photos', 'picture', $this->upload_dir . $folder, $uploadedFile);
                    $this->picture = $uploadedFile;
                }
            }

            return true;
        } else
            return false;
    }

    protected function afterSave() {
        parent::afterSave();

        if (!empty($_FILES['Photos']['name']['picture']) and $this->isNewRecord) {
            $user_upload_folder = Yii::app()->session->get('user_upload_folder');
            rename($this->upload_dir . $user_upload_folder, $this->upload_dir . $this->photo_id);
            Yii::app()->session->remove('user_upload_folder');
        }
    }

    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            $this->deleteImage(true);
            return true;
        } else
            return false;
    }

    private function deleteImage($d = false) {
        vH_Utils::rmdirf_($this->upload_dir . $this->photo_id);
        if ($d)
            vH_Utils::rmdir_($this->upload_dir . $this->photo_id);
    }

    public function getImage($width, $height = 0) {
        $width = (int) $width;
        $height = (int) $height;
        if ($width <= 0)
            return false;

        $news = $this->photo_id;
        $thumb = $this->picture;
        if (empty($this->picture)) {
            $news = 'default';
            $thumb = 'logo.png';
        }

        $img_data = Controller::getImageUpload((object) array(
                            'folder' => 'photos',
                            'id' => $news,
                            'width' => $width,
                            'height' => $height,
                            'filename' => $thumb
        ));

        if ($img_data->src == '') {
            $img_data = Controller::getImageUpload((object) array(
                                'folder' => 'photos',
                                'id' => 'default',
                                'width' => $width,
                                'height' => $height,
                                'filename' => 'no-image.png'
            ));
        }

        return $img_data;
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('photo_id', $this->photo_id);
        $criteria->compare('picture', trim($this->picture), true);
        $criteria->compare('sort_order', $this->sort_order);
        $criteria->compare('post_id', $this->post_id);
        $criteria->compare('showdefault', $this->showdefault);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'sort_order ASC'),
        ));
    }

}
