<?php

/**
 * This is the model class for table "{{slide}}".
 *
 * The followings are the available columns in table '{{slide}}':
 * @property integer $advs_id
 * @property string $url
 * @property string $image
 * @property integer $status
 * @property integer $width
 * @property integer $height
 * @property integer $sort
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $title
 * @property integer $open_new
 */
class Advs extends CommonAR {

    public $upload_dir;

    const POSITION_BANNER = 1,
            POSITION_QC_300x235 = 2,
            POSITON_QC_DOC = 3,
            POSITION_LOGO = 4,
            POSITION_BANNER_BG = 5,
            POSITION_LOGO_DVTC = 6,
            POSITION_LOGO_CQBT1 = 7,
            POSITION_LOGO_CQBT2 = 8,
            POSITION_LOGO_BOTTOM = 9;

    public function init() {
        $this->upload_dir = Yii::app()->params['uploadPath']['slide'];
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'Advs';
    }

    public function tableName() {
        return '{{advs}}';
    }

    public function rules() {
        return array(
            array('position,title', 'required'),
            array('status, width, height, sort_order, open_new, position, language', 'numerical', 'integerOnly' => true),
            array('url, image, description, title', 'length', 'max' => 255),
            array('created_by, updated_by', 'length', 'max' => 50),
            array('created_at, updated_at', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('advs_id, url, image, status,description, width, height, language, sort, created_at, created_by, updated_at, updated_by, title, open_new, position', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        return array(
            'advs_id' => 'ID',
            'url' => 'Link',
            'image' => 'Hình ảnh',
            'status' => 'Trạng thái',
            'width' => 'Chiều rộng',
            'height' => 'Chiều cao',
            'sort_order' => 'Thứ tự',
            'created_at' => 'Ngày tạo',
            'language' => 'Ngôn ngữ',
            'created_by' => 'Người tạo',
            'updated_at' => 'Ngày sửa',
            'updated_by' => 'Người sửa',
            'title' => 'Tiêu đề',
            'description'=>'Mô tả',
            'open_new' => 'Click link',
            'position' => 'Vị trí quảng cáo'
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('advs_id', $this->advs_id);
        $criteria->compare('url', trim($this->url), true);
        $criteria->compare('image', trim($this->image), true);
        $criteria->compare('status', $this->status);
        $criteria->compare('width', $this->width);
        $criteria->compare('height', $this->height);
        $criteria->compare('language', $this->language);
        $criteria->compare('sort_order', $this->sort_order);
        $criteria->compare('created_at', trim($this->created_at), true);
        $criteria->compare('created_by', trim($this->created_by), true);
        $criteria->compare('updated_at', trim($this->updated_at), true);
        $criteria->compare('updated_by', trim($this->updated_by), true);
        $criteria->compare('title', trim($this->title), true);
        $criteria->compare('open_new', $this->open_new);
        $criteria->compare('position', $this->position);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'advs_id DESC'),
        ));
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            if (!empty($_FILES['Advs']['name']['image'])) {
                if (!$this->isNewRecord) {
                    $folder = $this->advs_id . '/';
                    $this->deleteImage();
                } else {
                    Yii::app()->session['user_upload_folder'] = $folder = md5($_SERVER['REMOTE_ADDR']) . '_' . time() . '/';
                }
                //upload thumb
                $uploadedFile = pathinfo($_FILES['Advs']['name']['image'], PATHINFO_FILENAME) . vH_Utils::fileEx(CUploadedFile::getInstance($this, 'image'), true);
                if (!empty($uploadedFile)) {
                    $uploadedFile = vH_Upload::uploadFile('Advs', 'image', $this->upload_dir . $folder, $uploadedFile);
                    $this->image = $uploadedFile;
                }
            } else {
                if (empty($_POST['Advs']['picture_hidden']) and empty($this->image)) {
                    $this->addError('image', 'Hình ảnh không được phép rỗng');
                    return false;
                }
            }
            if ($this->isNewRecord) {
                $this->created_at = date('Y-m-d H:i:s', time());
            } else {
                $this->updated_at = date('Y-m-d H:i:s', time());
            }

            return true;
        } else
            return false;
    }

    protected function afterSave() {
        parent::afterSave();

        if (!empty($_FILES['Advs']['name']['image']) and $this->isNewRecord) {
            $user_upload_folder = Yii::app()->session->get('user_upload_folder');
            rename($this->upload_dir . $user_upload_folder, $this->upload_dir . $this->advs_id);
            Yii::app()->session->remove('user_upload_folder');
        }
    }

    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            $this->deleteImage(true);
            return true;
        } else
            return false;
    }

    private function deleteImage($d = false) {
        vH_Utils::rmdirf_($this->upload_dir . $this->advs_id);
        if ($d)
            vH_Utils::rmdir_($this->upload_dir . $this->advs_id);
    }

    public function getImage($width, $height = 0, $default = true) {
        $width = (int) $width;
        $height = (int) $height;
        if ($width <= 0)
            return false;

        $news = $this->advs_id;
        $thumb = $this->image;
        if (empty($this->image)) {
            $news = 'default';
            $thumb = 'logo.png';
        }

        $img_data = Controller::getImageUpload((object) array(
                            'folder' => 'slide',
                            'id' => $news,
                            'width' => $width,
                            'height' => $height,
                            'filename' => $thumb
        ));

        if ($img_data->src == '') {
            if ($default == true)
                $img_data = Controller::getImageUpload((object) array(
                                    'folder' => 'slide',
                                    'id' => 'default',
                                    'width' => $width,
                                    'height' => $height,
                                    'filename' => 'no-image.png'
                ));
            else
                return false;
        }

        return $img_data;
    }

    public function getIsImage() {
        if (!$this)
            return false;
        return vH_Utils::isFile($this->file, 'image');
    }

    public function getShowFile($thumb = false, $width = 0, $height = 0) {
        $file_exists = Yii::app()->params['uploadPath']['slide'] . $this->advs_id . '/' . $this->image;
        $file_view = Yii::app()->request->baseUrl . Yii::app()->params['uploadPath']['view']['slide'] . $this->advs_id . '/' . $this->image;
        //var_dump(file_exists($file_));
        if (empty($this->image) || !file_exists($file_exists))
            return false;
        list($w, $h) = getimagesize($file_exists);
        if ($width > 0) {
            $w_ = $width;
        } else {
            $w_ = $w;
        }
        if ($height > 0) {
            $h_ = $height;
        } else {
            $h_ = $h;
        }
//        if ($thumb) {
//            $scaleImage = vH_Utils::scaleImage($w, $h, 200, 130, true);
//            $w = $scaleImage['width'];
//            $h = $scaleImage['height'];
//        }
//        if ($width) {
//            $scaleImage = vH_Utils::scaleImage($w, $h, $width, 1000, true);
//            $w = $scaleImage['width'];
////            $h = $scaleImage['height'];
//            $h = $height;
//        }

        if (vH_Utils::isFile($this->image, 'image')) {
//                    return '<img src="'.$file_view.'" width="'.$w.'" height="'.$h.'" />';
            return '<img name="Image' . $this->advs_id . '" id="Image' . $this->advs_id . '" src="' . $this->getImage($w_, $h_)->src . '" />';
        } elseif (vH_Utils::isFile($this->image, 'flash')) {
            echo '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' . $w_ . '" height="' . $h_ . '">
				<param name="movie" value="' . $file_view . '" />
				<!--[if !IE]>-->
				<object type="application/x-shockwave-flash" data="' . $file_view . '" width="' . $w_ . '" height="' . $h_ . '">
				<!--<![endif]-->
				  <p>Alternative content</p>
				<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
			  </object>';
        }
        //registerJsRoot
    }

}
