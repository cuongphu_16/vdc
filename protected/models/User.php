<?php

/**
 * This is the model class for table "{{user}}".
 */
class User extends CommonAR {

    public $upload_dir,$password_cf, $email_cf, $password_new = "";

    public function init() {
	$this->upload_dir = Yii::app()->params['uploadPath']['user'];
    }

    public static function model($className = __CLASS__) {
	return parent::model($className);
    }

    public function getResourceName() {
	return 'User';
    }

    public function behaviors() {
	return array(
	    'ERememberFiltersBehavior' => array(
		'class' => 'application.components.ERememberFiltersBehavior',
		'defaults' => array(),
		'defaultStickOnClear' => false
	    ),
	);
    }

    public function tableName() {
	return '{{user}}';
    }

    public function rules() {
	return array(
	    array('email', 'required', 'message' => Yii::t('main', 'Enter your email')),
	    array('email', 'email'),
	    array('username', 'required', 'message' => Yii::t('main', 'Enter your username')),
	    array('password', 'required', 'on' => 'create', 'message' => Yii::t('main', 'Enter your password')),
	    array('email,username', 'unique'),
	    array('firstname', 'required', 'on' => 'edit'),
	    array('password, password_cf', 'match', 'pattern' => '/^([0-9a-zA-Z!@#$%^&*()])+$/'),
	    array('role_id, deleted, status, gender,sort_order', 'numerical', 'integerOnly' => true),
	    array('type', 'numerical'),
	    array('password, lastname,  avatar, address, phone, username', 'length', 'max' => 255),
	    array('password', 'length', 'min' => 6),
	    array('salt', 'length', 'max' => 50),
	    array('active', 'length', 'max' => 200),
	    array('code', 'length', 'max' => 32),
	    array('firstname, email, email2, confirm_email, created_by, updated_by, deleted_by', 'length', 'max' => 100),
	    array('mobile', 'length', 'max' => 15),
	    array('last_login, created_at, link_facebook, updated_at, deleted_at', 'safe'),
	    array('password', 'required', 'on' => 'get_pass_form, frontcreate, changepass, backcreate', 'message' => Yii::t('main', 'Enter your password')),
	    array('password_cf', 'required', 'on' => 'get_pass_form, frontcreate, changepass, backcreate', 'message' => Yii::t('main', 'Enter your password confirm')),
	    array('password_cf', 'compare', 'compareAttribute' => 'password', 'on' => 'get_pass_form, frontcreate, changepass, backcreate, backsytemcreate'),
	    //array('password_cf', 'compare', 'compareAttribute' => 'password', 'on' => 'editprofile'),
	    array('email_cf', 'compare', 'compareAttribute' => 'email', 'on' => 'edit_email'),
	    array('email,email_cf', 'required', 'message' => Yii::t('main', 'Enter your email'), 'on' => 'edit_email'),
	    array('mobile, phone', 'match', 'pattern' => '/^([0-9\-. +])+$/', 'allowEmpty' => true),
	    // The following rule is used by search().
// Please remove those attributes that should not be searched.
	    array('user_id, user_type, password,link_facebook, type, salt, active, code, firstname, lastname, email, confirm_email, role_id, last_login, phone, mobile, created_at, created_by, updated_at, updated_by, status, gender, password_new', 'safe', 'on' => 'search'),
	);
    }

    public function relations() {
	return array(
	);
    }

    public function attributeLabels() {
	$a = array(
	    'user_id' => 'ID',
	    'user_type' => 'User Type',
	    'password' => 'Password',
	    'password_new' => 'Password',
	    'password_cf' => 'Retype password',
	    'salt' => 'Salt',
	    'active' => 'Active',
	    'code' => 'Code',
	    'firstname' => 'Firstname',
	    'lastname' => 'Lastname',
	    'email' => 'Email',
	    'address' => 'Address',
	    'role_id' => 'Role',
	    'last_login' => 'Last Login',
	    'phone' => 'Other phone',
	    'mobile' => 'Mobile',
	    'created_at' => 'Created At',
	    'created_by' => 'Created By',
	    'updated_at' => 'Updated At',
	    'updated_by' => 'Updated By',
	    'status' => 'Status',
	    'link_facebook' => 'Link Facebook'
	);
	foreach ($a as $key => $val) {
	    $a[$key] = Yii::t('main', $val);
	}
	return $a;
    }

    public function search($clause = '') {

	$criteria = new CDbCriteria;

	$criteria->compare('user_id', $this->user_id);
	$criteria->compare('type', $this->type);
	$criteria->compare('password', trim($this->password), true);
	$criteria->compare('password_new', trim($this->password), true);
	$criteria->compare('salt', trim($this->salt), true);
	$criteria->compare('active', trim($this->active), true);
	$criteria->compare('code', trim($this->code), true);
	$criteria->compare('firstname', trim($this->firstname), true);
	$criteria->compare('lastname', trim($this->lastname), true);
	$criteria->compare('email', trim($this->email), true);
	$criteria->compare('address', trim($this->address), true);
	$criteria->compare('role_id', $this->role_id);
	$criteria->compare('last_login', trim($this->last_login), true);
	$criteria->compare('phone', trim($this->phone), true);
	$criteria->compare('mobile', trim($this->mobile), true);
	$criteria->compare('created_at', trim($this->created_at), true);
	$criteria->compare('created_by', trim($this->created_by), true);
	$criteria->compare('updated_at', trim($this->updated_at), true);
	$criteria->compare('updated_by', trim($this->updated_by), true);
	$criteria->compare('status', $this->status);

	if ($clause != '')
	    $criteria->addCondition($clause);
	return new CActiveDataProvider($this, array(
	    'criteria' => $criteria,
	    'sort' => array('defaultOrder' => 'created_at DESC'),
	    'pagination' => array(
		'pageSize' => 50
	    ),
	));
    }

    public function validatePassword($password) {
	return vH_Utils::userPass($password, $this->salt) === $this->password;
    }

    public function createPassword() {
	$salt = Yii::app()->params['salt'];
	$pass = rand();
	$pass_encode = vH_Utils::userPass($pass, $salt);
    }

    public function loadByAccount($account = NULL) {
	if (!isset($account)) {
	    throw new CHttpException(404, 'Can not find administrator by account.');
	} else {
	    return $this->findByAttributes(array('email' => $account));
	}
    }

    public function loadRoleIdByAccount($account = NULL) {
	if ($this->loadByAccount($account)) {
	    return $this->loadByAccount($account)->getAttribute('role_id');
	} else {
	    return false;
	}
    }

    public static function toOptionHash($filter = array(), $key = 'user_id', $value = 'firstname') {
	$return = array();
	$result = User::model()->findAllByAttributes((array) $filter);
	$return[''] = '-- ' . Yii::t('main', 'User') . ' --';
	for ($i = 0; $i < count($result); $i++) {
	    $return[$result[$i][$key]] = $result[$i][$value];
	}
	return $return;
    }

    public function getActiveUrl() {
	return Yii::app()->createAbsoluteUrl('user/active', array(
		    'user' => $this->email,
		    'code' => $this->salt,
	));
    }

    public function getPassUrl() {
	return Yii::app()->createAbsoluteUrl('user/getPass/lang/' . Yii::app()->language, array(
		    'get_pass' => $this->email,
		    'code' => $this->salt,
	));
    }

    public function getImage($width, $height = 0) {
	$width = (int) $width;
	$height = (int) $height;
	if ($width <= 0)
	    return false;

	$id = $this->user_id;
	$image = $this->avatar;

	if (empty($this->avatar)) {
	    $id = 'default';
	    $image = 'default-avatar.png';
	}
	$img_data = Controller::getImageUpload((object) array(
			    'folder' => 'user',
			    'id' => $id,
			    'width' => $width,
			    'height' => $height,
			    'filename' => $image
	));

	if ($img_data->src == '') {
	    $img_data = Controller::getImageUpload((object) array(
				'folder' => 'user',
				'id' => 'default',
				'width' => $width,
				'height' => $height,
				'filename' => 'default-avatar.png'
	    ));
	}
	return $img_data;
    }
    
    
    
    protected function beforeValidate() {
        if (parent::beforeValidate()) {
// something happens here
            if (!empty($_FILES['User']['name']['avatar'])) {
                if (!$this->isNewRecord) {
                    $folder = $this->user_id . '/';
                    $this->deleteImage();
                } else {
                    Yii::app()->session['user_upload_folder'] = $folder = md5($_SERVER['REMOTE_ADDR']) . '_' . time() . '/';
                }
//upload image
                $uploadedFile = vH_Utils::alias($_FILES['User']['name']['avatar']) . vH_Utils::fileEx(CUploadedFile::getInstance($this, 'avatar'), true);
                if (!empty($uploadedFile)) {
                    Yii::app()->session['user_upload_image'] = $uploadedFile = vH_Upload::uploadFile('User', 'avatar', $this->upload_dir . $folder, $uploadedFile);
                    $this->avatar = $uploadedFile;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            if (!empty($_FILES['User']['name']['avatar'])) {
                if (!$this->isNewRecord) {
                    $folder = $this->user_id . '/';
                    $this->deleteImage();
                    $uploadedFile = vH_Utils::alias($_FILES['User']['name']['avatar']) . vH_Utils::fileEx(CUploadedFile::getInstance($this, 'avatar'), true);
                    if (!empty($uploadedFile)) {
                        $uploadedFile = vH_Upload::uploadFile('User', 'avatar', $this->upload_dir . $folder, $uploadedFile);
                        $this->avatar = $uploadedFile;
                    }
                } else {
                    $folder = Yii::app()->session['user_upload_folder'];
                }
            }

            if ($this->isNewRecord) {
		$salt = Yii::app()->params['salt'];
		$this->created_at = date('Y-m-d H:i:s', time());
		$this->password = vH_Utils::userPass($this->password, $salt);
		$this->salt = $salt;
		$this->user_id = SiteHelper::getDataTableID($this->tableName(), 'user_id');
	    } else {
		$this->updated_at = date('Y-m-d H:i:s', time());
	    }
            return true;
        } else
            return false;
    }

    protected function afterSave() {
        parent::afterSave();

        if ($this->isNewRecord) {
            if (!empty(Yii::app()->session['user_upload_folder'])):
                $user_upload_folder = Yii::app()->session->get('user_upload_folder');
                rename($this->upload_dir . $user_upload_folder, $this->upload_dir . $this->user_id);
                unset(Yii::app()->session['user_upload_folder']);
            endif;
        }
    }

    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            $this->deleteImage(true);
            return true;
        } else
            return false;
    }

    private function deleteImage($d = false) {
        vH_Utils::rmdirf_($this->upload_dir . $this->user_id);
        if ($d)
            vH_Utils::rmdir_($this->upload_dir . $this->user_id);
    }
  
}
