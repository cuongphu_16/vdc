<?php

/**
 * This is the model class for table "{{slide}}".
 *
 * The followings are the available columns in table '{{slide}}':
 * @property integer $slide_id
 * @property string $picture
 * @property integer $sort_order
 * @property integer $status
 */
class Slide extends CommonAR {

    public $upload_dir;

    public function init() {
        $this->upload_dir = Yii::app()->params['uploadPath']['background'];
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(),
                'defaultStickOnClear' => false
            ),
        );
    }

    public function getResourceName() {
        return 'Slide';
    }

    public function tableName() {
        return '{{slide}}';
    }

    public function rules() {
        return array(
            array('title', 'required'),
            array('slide_id, sort_order, status', 'numerical', 'integerOnly' => true),
            array('title, picture', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('slide_id, title, picture, sort_order, status', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        return array(
            'slide_id' => 'ID',
            'picture' => 'Ảnh',
            'sort_order' => 'Thứ tự',
            'status' => 'Trạng thái',
            'title' => 'Tiêu đề',
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            if (!empty($_FILES['Slide']['name']['picture'])) {
                if (!$this->isNewRecord) {
                    $folder = $this->slide_id . '/';
                    $this->deleteImage();
                } else {
                    Yii::app()->session['user_upload_folder'] = $folder = md5($_SERVER['REMOTE_ADDR']) . '_' . time() . '/';
                }
                //upload thumb
                $uploadedFile = vH_Utils::alias($this->title) . vH_Utils::fileEx(CUploadedFile::getInstance($this, 'picture'), true);
                if (!empty($uploadedFile)) {
                    $uploadedFile = vH_Upload::uploadFile('Slide', 'picture', $this->upload_dir . $folder, $uploadedFile);
                    $this->picture = $uploadedFile;
                }
            }

            return true;
        } else
            return false;
    }

    protected function afterSave() {
        parent::afterSave();

        if (!empty($_FILES['Slide']['name']['picture']) and $this->isNewRecord) {
            $user_upload_folder = Yii::app()->session->get('user_upload_folder');
            rename($this->upload_dir . $user_upload_folder, $this->upload_dir . $this->slide_id);
            Yii::app()->session->remove('user_upload_folder');
        }
    }

    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            $this->deleteImage(true);
            return true;
        } else
            return false;
    }

    private function deleteImage($d = false) {
        vH_Utils::rmdirf_($this->upload_dir . $this->slide_id);
        if ($d)
            vH_Utils::rmdir_($this->upload_dir . $this->slide_id);
    }

    public function getImage($width, $height = 0) {
        $width = (int) $width;
        $height = (int) $height;
        if ($width <= 0)
            return false;

        $news = $this->slide_id;
        $thumb = $this->picture;
        if (empty($this->picture)) {
            $news = 'default';
            $thumb = 'logo.png';
        }

        $img_data = Controller::getImageUpload((object) array(
                            'folder' => 'background',
                            'id' => $news,
                            'width' => $width,
                            'height' => $height,
                            'filename' => $thumb
        ));

        if ($img_data->src == '') {
            $img_data = Controller::getImageUpload((object) array(
                                'folder' => 'background',
                                'id' => 'default',
                                'width' => $width,
                                'height' => $height,
                                'filename' => 'no-image.png'
            ));
        }

        return $img_data;
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('slide_id', $this->slide_id);
        $criteria->compare('title', trim($this->title), true);
        $criteria->compare('picture', trim($this->picture), true);
        $criteria->compare('sort_order', $this->sort_order);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'sort_order ASC'),
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

}
