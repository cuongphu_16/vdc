<?php

/**
 * This is the model class for table "{{project}}".
 *
 * The followings are the available columns in table '{{project}}':
 * @property integer $project_id
 * @property string $title
 * @property string $alias
 * @property string $image
 * @property string $thumb
 * @property integer $cate_id
 * @property string $desc
 * @property string $content
 * @property integer $focus
 * @property integer $status
 * @property string $created_at
 * @property integer $sort_order
 */
class Project extends CommonAR {

    public $upload_dir = "";

    public function init() {
        $this->upload_dir = Yii::app()->params['uploadPath']['project'];
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(),
                'defaultStickOnClear' => false
            ),
        );
    }

    public function getResourceName() {
        return 'Project';
    }

    public function tableName() {
        return '{{project}}';
    }

    public function rules() {
        return array(
            array('title,language, content', 'required'),
            array('title', 'unique'),
            array('cate_id, focus, status, sort_order', 'numerical', 'integerOnly' => true),
            array('title, alias', 'length', 'max' => 255),
            array('image, thumb, desc, content, created_at', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('project_id, title,language, alias, image, thumb, cate_id, desc, content, focus, status, created_at, sort_order', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'category' => array(self::BELONGS_TO, 'Category', 'cate_id'),
        );
    }

    public function attributeLabels() {
        $a = array(
            'project_id' => 'Project',
            'title' => 'Title',
            'alias' => 'Alias',
            'image' => 'Image',
            'thumb' => 'Thumb',
            'cate_id' => 'Category',
            'desc' => 'Description',
            'language' => 'Language',
            'content' => 'Content',
            'focus' => 'Focus',
            'status' => 'Status',
            'created_at' => 'Created At',
            'sort_order' => 'Sort Order',
        );
        foreach ($a as $key => $val) {
            $a[$key] = Yii::t('main', $val);
        }
        return $a;
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('project_id', $this->project_id);
        $criteria->compare('title', trim($this->title), true);
        $criteria->compare('alias', trim($this->alias), true);
        $criteria->compare('image', trim($this->image), true);
        $criteria->compare('thumb', trim($this->thumb), true);
        $criteria->compare('cate_id', $this->cate_id);
        $criteria->compare('desc', trim($this->desc), true);
        $criteria->compare('content', trim($this->content), true);
        $criteria->compare('focus', $this->focus);
        $criteria->compare('status', $this->status);
        $criteria->compare('language', $this->language);
        $criteria->compare('created_at', trim($this->created_at), true);
        $criteria->compare('sort_order', $this->sort_order);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'sort_order ASC'),
        ));
    }

    protected function afterSave() {
        parent::afterSave();

        if ($this->isNewRecord) {
            if (isset(Yii::app()->session['session_upload_photo_dir']) && strpos(Yii::app()->session['session_upload_photo_dir'], 'tmp') !== false) {
                $photo_upload_folder = Yii::app()->session['session_upload_photo_dir'];
                @rename($this->upload_dir . $photo_upload_folder, $this->upload_dir . $this->project_id);
                Yii::app()->session->remove('session_upload_photo_dir');
            }
        }
    }

    public function getImage($fileName = '') {
        $img_data = Yii::app()->request->getBaseUrl(true) . Yii::app()->params['uploadPath']['view']['project'] . $this->project_id . '/' . $fileName;
        return $img_data;
    }

    public function getAllimage($width = 0, $height = 0) {
        $width = (int) $width;
        $height = (int) $height;
        if ($width <= 0)
            return false;
        if ($this->image != "") {
            $images = explode("|:|", $this->image);
            if (!empty($images) && is_array($images)) {
                $arr_image = array();
                foreach ($images as $image_) {
                    $img_data = Controller::getImageUpload((object) array(
                                        'folder' => 'project',
                                        'id' => $this->project_id,
                                        'width' => $width,
                                        'height' => $height,
                                        'filename' => $image_
                    ));
                    if (is_object($img_data))
                        $img_data->title = $image_;
                    if ($img_data != false)
                        array_push($arr_image, $img_data);
                }
                return $arr_image;
            }else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getthumbImage($width, $height = 0, $fileName = '') {

        $width = (int) $width;
        $height = (int) $height;
        if ($width <= 0)
            return false;

        $img_data = Yii::app()->request->getBaseUrl(true) . '/thumb.php?i=' . substr(Yii::app()->params['uploadPath']['view']['project'], 1, strlen(Yii::app()->params['uploadPath']['view']['project'])) . ((!empty($this->project_id)) ? $this->project_id : Yii::app()->session['session_upload_photo_dir']) . '/' . $fileName . "&w=$width&h=$height";
        return $img_data;
    }

    public function getthumbImagexh($width = 0, $height, $fileName = '') {

        $width = (int) $width;
        $height = (int) $height;
        if ($height <= 0)
            return false;

        $img_data = Yii::app()->request->getBaseUrl(true) . '/thumb.php?i=' . substr(Yii::app()->params['uploadPath']['view']['project'], 1, strlen(Yii::app()->params['uploadPath']['view']['project'])) . $this->project_id . '/' . $fileName . "&w=$width&h=$height";
        return $img_data;
    }

    public function getSlideImage() {
        $id = $this->project_id;
        $image = $this->image;
        $arr = explode('|:|', $image);
        if ($arr) {
            foreach ($arr as $key => $ar) {
                if ($ar != '') {
                    $arrImg[$key] = $ar;
                }
            }
        }
        return $arrImg;
    }

    public function getThumb($width, $height = 0) {
        $width = (int) $width;
        $height = (int) $height;
        if ($width <= 0)
            return false;

        $project_id = $this->project_id;
        $thumb = $this->thumb;

        if (empty($this->thumb)) {
            $project_id = 'default';
            $thumb = 'logo.png';
        }
        $img_data = Controller::getImageUpload((object) array(
                            'folder' => 'project',
                            'id' => $project_id,
                            'width' => $width,
                            'height' => $height,
                            'filename' => $thumb
        ));

        if ($img_data->src == '') {
            $img_data = Controller::getImageUpload((object) array(
                                'folder' => 'project',
                                'id' => 'default',
                                'width' => $width,
                                'height' => $height,
                                'filename' => 'logo.png'
            ));
        }
        return $img_data;
    }

    public function getThumbSlide($width, $height = 0, $thumb) {
        $width = (int) $width;
        $height = (int) $height;
        if ($width <= 0)
            return false;

        $project_id = $this->project_id;

        if (empty($this->thumb)) {
            $project_id = 'default';
            $thumb = 'logo.png';
        }
        $img_data = Controller::getImageUpload((object) array(
                            'folder' => 'project',
                            'id' => $project_id,
                            'width' => $width,
                            'height' => $height,
                            'filename' => $thumb
        ));

        if ($img_data->src == '') {
            $img_data = Controller::getImageUpload((object) array(
                                'folder' => 'project',
                                'id' => 'default',
                                'width' => $width,
                                'height' => $height,
                                'filename' => 'logo.png'
            ));
        }
        return $img_data;
    }

    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            $this->deleteImage(true);
            return true;
        } else
            return false;
    }

    private function deleteImage($delete_directory = true) {
        //delete_folder: xoa thu muc chinh
        vH_Utils::rmdir_($this->upload_dir . $this->project_id, $delete_directory);
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {

            if ($this->isNewRecord) {
                $this->created_at = date('Y-m-d H:i:s', time());
            }
            return true;
        } else
            return false;
    }

    public function getUrl() {
        return Yii::app()->createUrl('main/projectDetail/lang/' . Yii::app()->language, array(
                    'url_project' => $this->alias,
        ));
    }

}
