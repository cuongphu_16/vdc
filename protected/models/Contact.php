<?php

/**
 * This is the model class for table "{{contact}}".
 *
 * The followings are the available columns in table '{{contact}}':
 * @property integer $contact_id
 * @property string $fullname
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $content
 * @property string $ip
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property integer $status
 */
class Contact extends CommonAR {

    public $verifyCode;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'Contact';
    }

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(),
                'defaultStickOnClear' => false
            ),
        );
    }

    public function tableName() {
        return '{{contact}}';
    }

    public function rules() {
        return array(
            array('fullname, content, title, email', 'required'),
            array('status', 'numerical', 'integerOnly' => true),
            array('fullname, address,title', 'length', 'max' => 255),
            array('phone, email, created_by', 'length', 'max' => 100),
            array('email', 'email'),
            array('content', 'safe'),
            array('phone', 'match', 'pattern' => '/^([0-9+])+$/', 'allowEmpty' => true, 'on' => 'front'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('contact_id,title, fullname, address, phone, email, content, ip, created_at, created_by, status', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        $a = array(
            'contact_id' => 'ID',
            'fullname' => 'Fullname',
            'title'=>'Title',
            'address' => 'Address',
            'phone' => 'Mobile',
            'email' => 'Email',
            'content' => 'Content',
            'ip' => 'IP',
            'verifyCode' => 'verifyCode',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'status' => 'Status',
        );
        foreach ($a as $key => $val) {
            $a[$key] = Yii::t('main', $val);
        }
        return $a;
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('contact_id', $this->contact_id);
        $criteria->compare('title', $this->title);
        $criteria->compare('fullname', trim($this->fullname), true);
        $criteria->compare('address', trim($this->address), true);
        $criteria->compare('phone', trim($this->phone), true);
        $criteria->compare('email', trim($this->email), true);
        $criteria->compare('content', trim($this->content), true);
        $criteria->compare('ip', trim($this->ip), true);
        $criteria->compare('created_at', trim($this->created_at), true);
        $criteria->compare('created_by', trim($this->created_by), true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'contact_id DESC'),
            'pagination' => array(
                'pageSize' => 100,
            ),
        ));
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {

            if ($this->isNewRecord) {
                $this->created_at = date('Y-m-d H:i:s', time());
            }
            return true;
        } else
            return false;
    }

}
