<?php

/**
 * This is the model class for table "{{role}}".
 *
 * The followings are the available columns in table '{{role}}':
 * @property integer $role_id
 * @property string $role_name
 * @property string $role_label
 * @property string $acl_desc
 * @property integer $p_id
 * @property string $create_time
 * @property string $update_time
 * @property integer $status
 * @property integer $admin_use
 */
class Role extends CommonAR {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'Role';
    }

    public function tableName() {
        return '{{role}}';
    }

    public function rules() {
        return array(
            array('p_id, status, admin_use', 'numerical', 'integerOnly' => true),
            array('role_name, role_label', 'length', 'max' => 20),
            array('acl_desc, create_time, update_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('role_id, role_name, role_label, acl_desc, p_id, create_time, update_time, status, admin_use', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'users' => array(
                self::HAS_MANY, 'User', 'role_id',
                'condition' => 'status=1',
            ),
        );
    }

    public function attributeLabels() {
        return array(
            'role_id' => 'Role',
            'role_name' => 'Role Name',
            'role_label' => 'Role Label',
            'acl_desc' => 'Acl Desc',
            'p_id' => 'P',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
            'status' => 'Status',
            'admin_use' => 'Admin Use',
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('role_id', $this->role_id);
        $criteria->compare('role_name', $this->role_name, true);
        $criteria->compare('role_label', $this->role_label, true);
        $criteria->compare('acl_desc', $this->acl_desc, true);
        $criteria->compare('p_id', $this->p_id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('admin_use', $this->admin_use);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'sort_order ASC'),
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

}
