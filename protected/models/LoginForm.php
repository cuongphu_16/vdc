<?php

class LoginForm extends CFormModel {

    public $username;
    public $password;
    public $rememberMe;
    private $_identity;

    public function rules() {
        return array(
            // username and password are required
            array('username, password', 'required'),
            // rememberMe needs to be a boolean
            array('rememberMe', 'boolean'),
            // password needs to be authenticated
            array('password', 'authenticate'),
        );
    }

    public function attributeLabels() {
        return array(
            'username' => 'Email',
            'password' => 'Mật khẩu',
            'rememberMe' => 'Nhớ đăng nhập lần sau',
        );
    }

    public function authenticate($attribute, $params) {
        $model = User::model()->find('LOWER(email) = ? ', array(strtolower($this->username)));
        $username = ($model != NULL) ? $this->username : '';
        $this->_identity = new UserIdentity($username, $this->password, ($this->scenario === 'face'));
        if (!$this->_identity->authenticate()) {
            $this->addError('password', 'Email, mật khẩu không chính xác.');
        }elseif($model->status == 0){
            $this->addError('password', 'Tài khoản của bạn chưa được kích hoạt');
        }
    }

    public function login() {

        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->username, $this->password);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        } else
            return false;
    }

}
