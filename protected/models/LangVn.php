<?php

/**
 * This is the model class for table "{{lang_vn}}".
 *
 * The followings are the available columns in table '{{lang_vn}}':
 * @property integer $id
 * @property string $key
 * @property string $value
 */
class LangVn extends CommonAR {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'LangVn';
    }

    public function tableName() {
        return '{{lang_vn}}';
    }

    public function rules() {
        return array(
            array('key, value', 'safe'),
            array('type,type2', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, key, value,type,type2', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'key' => 'Key',
            'value' => 'Value',
            'type' => 'Type'
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('key', $this->key, true);
        $criteria->compare('value', $this->value, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('type2', $this->type2, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'id ASC'),
        ));
    }

}
