<?php

/**
 * This is the model class for table "rd_countries".
 *
 * The followings are the available columns in table 'rd_countries':
 * @property integer $country_id
 * @property string $title
 * @property string $comment
 * @property string $LatLng
 * @property integer $status
 * @property integer $sort_order
 */
class Countries extends CommonAR {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'Countries';
    }

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(),
                'defaultStickOnClear' => false
            ),
        );
    }

    public function tableName() {
        return 'pk_countries';
    }

    public function rules() {
        return array(
            array('title', 'required'),
            array('status, sort_order', 'numerical', 'integerOnly' => true),
            array('title, comment, LatLng', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('country_id, title, comment, LatLng, status, sort_order', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        $a = array(
            'country_id' => 'Country',
            'title' => 'Title',
            'comment' => 'Comment',
            'LatLng' => 'Lat Lng',
            'status' => 'Status',
            'sort_order' => 'Sort Order',
        );
        foreach ($a as $key => $val) {
            $a[$key] = Yii::t('main', $val);
        }
        return $a;
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('country_id', $this->country_id);
        $criteria->compare('title', trim($this->title), true);
        $criteria->compare('comment', trim($this->comment), true);
        $criteria->compare('LatLng', trim($this->LatLng), true);
        $criteria->compare('status', $this->status);
        $criteria->compare('sort_order', $this->sort_order);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'country_id DESC'),
            'pagination' => array(
                'pageSize' => 100,
            ),
        ));
    }

    public function toOptionHash($filter = array(), $key = 'country_id', $value = 'title') {
        $return = array();
        $criteria = new CDbCriteria;
        $criteria->order = "sort_order ASC, title ASC";
        $result = $this->findAllByAttributes((array) $filter, $criteria);
        $return[''] = '-- ' . Yii::t('main', 'Countries') . ' --';
        for ($i = 0; $i < count($result); $i++) {
            $return[$result[$i][$key]] = html_entity_decode($result[$i][$value], ENT_QUOTES);
        }
        return $return;
    }

}
