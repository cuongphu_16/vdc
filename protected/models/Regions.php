<?php

class Regions extends CommonAR {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'Regions';
    }

    public function tableName() {
        return '{{regions}}';
    }

    public function rules() {
        return array(
            array('title,alias', 'required'),
            array('sort_order, area, country_id', 'numerical', 'integerOnly' => true),
            array('title, code, ADM1Code', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('region_id, title,alias, code, ADM1Code, country_id, sort_order, status', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'district' => array(self::HAS_MANY, 'District', 'region_id', 'order' => 'title asc'),
        );
    }

    public function attributeLabels() {
        return array(
            'region_id' => 'ID',
            'country_id' => "Quốc gia",
            'title' => 'Tỉnh / Thành phố',
            'alias' => 'Tên url',
            'code' => 'Mã',
            'ADM1Code' => 'Mã Adm',
            'status' => 'Trạng thái',
            'sort_order' => 'Thứ tự',
            'area' => 'Miền',
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('region_id', $this->region_id);
        $criteria->compare('country_id', $this->country_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('alias', $this->alias, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('ADM1Code', $this->ADM1Code, true);
        $criteria->compare('sort_order', $this->sort_order);
        $criteria->compare('status', $this->status);
        $criteria->compare('area', $this->area);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'region_id DESC'),
            'pagination' => array(
                'pageSize' => 100,
            ),
        ));
    }
    
    public function toOptionHashExtra($filter = array(), $lang=1, $key = 'region_id', $value = 'title') {
        $return = array();
        $criteria = new CDbCriteria;
        $criteria->order = "sort_order ASC, title ASC";
        $result = $this->findAllByAttributes((array) $filter, $criteria);
//        $return[''] = '-- ' . Yii::t('main', 'Region') . ' --';
        for ($i = 0; $i < count($result); $i++) {
            if ($lang == 1) {
                $return[$result[$i][$key]] = html_entity_decode($result[$i][$value], ENT_QUOTES);
            } else {
                $return[$result[$i][$key]] = vH_Utils::stripUnicode($result[$i][$value], ENT_QUOTES);
            }
        }
        return $return;
    }
    
    public function toOptionHash($filter = array(), $lang=1, $key = 'region_id', $value = 'title') {
        $return = array();
        $criteria = new CDbCriteria;
        $criteria->order = "sort_order ASC, title ASC";
        $result = $this->findAllByAttributes((array) $filter, $criteria);
//        $return[''] = '-- ' . Yii::t('main', 'Region') . ' --';
        for ($i = 0; $i < count($result); $i++) {
            if ($lang == 1) {
                $return[$result[$i][$key]] = html_entity_decode($result[$i][$value], ENT_QUOTES);
            } else {
                $return[$result[$i][$key]] = vH_Utils::stripUnicode($result[$i][$value], ENT_QUOTES);
            }
        }
        return $return;
    }

}
