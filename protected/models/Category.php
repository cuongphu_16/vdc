<?php

/**
 * This is the model class for table "{{category}}".
 *
 * The followings are the available columns in table '{{category}}':
 * @property integer $cate_id
 * @property string $title
 * @property string $alias
 * @property integer $type
 * @property integer $status
 */
class Category extends CommonAR {

    const TYPE_ABOUT = 0,
            TYPE_SERVICES = 1,
            TYPE_PROJECT = 2;

    public $upload_dir = "";

    public function init() {
        $this->upload_dir = Yii::app()->params['uploadPath']['category'];
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'Category';
    }

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(),
                'defaultStickOnClear' => false
            ),
        );
    }

    public function tableName() {
        return '{{category}}';
    }

    public function rules() {
        return array(
            array('title+type', 'ext.uniqueMultiColumnValidator', 'caseSensitive' => true, 'message' => 'Không được nhập tiêu đề trùng nhau nếu cùng phân loại'),
            array('title,type,language', 'required', 'on' => 'create'),
            array('title,alias', 'required', 'on' => 'create_carrer'),
            array('type, status,sort_order, p_id', 'numerical', 'integerOnly' => true),
            array('title, alias,action_run,image', 'length', 'max' => 255),
            array('created_at,content,description', 'safe'),
            // The following rule is used by search().
// Please remove those attributes that should not be searched.
            array('cate_id, title,sort_order,type_view,action_run, alias, type, status,description,content,image,created_at', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array();
    }

    public function attributeLabels() {
        $a = array(
            'cate_id' => 'ID',
            'title' => 'Title',
            'alias' => 'Alias',
            'type' => 'Phân loại',
            'p_id' => 'Parent',
            'description' => "Description",
            'status' => 'Status',
            'sort_order' => 'Sort Order',
            'content' => 'Nội dung chi tiết',
            'image' => 'Ảnh đại diện',
            'created_at' => 'Ngày tạo',
            'type_view' => 'Loại hiển thị',
        );
        foreach ($a as $key => $val) {
            $a[$key] = Yii::t('main', $val);
        }
        return $a;
    }

    public function search($condidtion = '') {

        $criteria = new CDbCriteria;

        $criteria->compare('cate_id', $this->cate_id);
        $criteria->compare('title', trim($this->title), true);
        $criteria->compare('alias', trim($this->alias), true);
        $criteria->compare('type', $this->type);
        $criteria->compare('image', trim($this->image), true);
        $criteria->compare('description', trim($this->description), true);
        $criteria->compare('content', trim($this->content), true);
        $criteria->compare('sort_order', $this->sort_order);
        $criteria->compare('status', $this->status);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('type_view', $this->type_view, true);
        if ($condidtion != "")
            $criteria->addCondition($condidtion);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'created_at DESC'),
            'pagination' => array(
                'pageSize' => 100,
            ),
        ));
    }

    public function getCatOptionsTree($p_id = '', $margin = 0) {
        $criteria = new CDbCriteria();
        $criteria->order = 'sort_order ASC, cate_id DESC';
        $criteria->condition = "status=1";

        if (trim($p_id) != "") {
            $p_id = (int) $p_id;
            $criteria->addCondition('p_id=' . $p_id);
        }
        $result = $this->findAll($criteria);
        $options = array();
        if (count($result) > 0) {

            foreach ($result as $v) {
                $cate_id = (int) $v->getAttribute('cate_id');
                $options[$cate_id] = str_repeat('---', (int) $margin) . ' ' . $v->getAttribute('title');
                $tmp = $this->getCatOptionsTree($cate_id, $margin + 1);
                if (count($tmp) > 0) {
                    $options = $options + $tmp;
                }
            }
        }
        return $options;
    }

    public function getTypeCatOptionsTree($type = 0, $margin = 0, $language = 0) {
        $criteria = new CDbCriteria();
        $criteria->order = 'sort_order ASC';

        $criteria->addCondition("type = " . $type);
        if ((int) $language > 0)
            $criteria->addCondition("language  = " . $language);
        $result = $this->findAllByAttributes(array(
            'status' => 1,
                ), $criteria);
        $options = array();
        if (count($result) > 0) {

            foreach ($result as $v) {
                $cate_id = (int) $v->getAttribute('cate_id');
                $options[$cate_id] = str_repeat('---', (int) $margin) . ' ' . $v->getAttribute('title');
//                $tmp = $this->getTypeCatOptionsTree($type + 1, $margin + 1, $language);
//                if (count($tmp) > 0) {
//                    $options = $options + $tmp;
//                }
            }
        }

        return $options;
    }

    public function getTypeViewTree($type_view = 0, $margin = 0) {
        $criteria = new CDbCriteria();
        $criteria->order = 'sort_order ASC';

        $criteria->addCondition("type_view = $type_view");
        $result = $this->findAllByAttributes(array(
            'status' => 1
                ), $criteria);
        $options = array();
        if (count($result) > 0) {

            foreach ($result as $v) {
                $cate_id = (int) $v->getAttribute('cate_id');
                $options[$cate_id] = str_repeat('---', (int) $margin) . ' ' . $v->getAttribute('title');
                $tmp = $this->getTypeCatOptionsTree($type_view, $margin + 1);
                if (count($tmp) > 0) {
                    $options = $options + $tmp;
                }
            }
        }

        return $options;
    }

    public function getUrl() {
        if ($this->type == Category::TYPE_ABOUT) {
            return Yii::app()->createUrl('main/listAbout/lang/' . Yii::app()->language, array(
                        'url_about' => $this->alias
            ));
        } elseif ($this->type == Category::TYPE_SERVICES) {
            return Yii::app()->createUrl('main/listServices/lang/' . Yii::app()->language, array(
                        'url_services' => $this->alias
            ));
        } elseif ($this->type == Category::TYPE_PROJECT) {
            return Yii::app()->createUrl('main/listProject/lang/' . Yii::app()->language, array(
                        'url_project' => $this->alias
            ));
        }
    }

    public function toOptionHash($filter = array(), $key = 'cate_id', $value = 'title') {
        $return = array();
        $criteria = new CDbCriteria;
        $criteria->order = "sort_order ASC, title ASC";
        $result = $this->findAllByAttributes((array) $filter, $criteria);
        for ($i = 0; $i < count($result); $i++) {
            $return[$result[$i][$key]] = html_entity_decode($result[$i][$value], ENT_QUOTES);
        }
        return $return;
    }

    public function getImage($width, $height = 0) {
        $width = (int) $width;
        $height = (int) $height;
        if ($width <= 0)
            return false;

        $cate_id = $this->cate_id;
        $image = $this->image;

        if (empty($this->image)) {
            $cate_id = 'default';
            $image = 'logo.png';
        }
        $img_data = Controller::getImageUpload((object) array(
                            'folder' => 'category',
                            'id' => $cate_id,
                            'width' => $width,
                            'height' => $height,
                            'filename' => $image
        ));

        if ($img_data->src == '') {
            $img_data = Controller::getImageUpload((object) array(
                                'folder' => 'category',
                                'id' => 'default',
                                'width' => $width,
                                'height' => $height,
                                'filename' => 'logo.png'
            ));
        }
        return $img_data;
    }

    protected function beforeValidate() {
        if (parent::beforeValidate()) {
// something happens here
            if (!empty($_FILES['Category']['name']['image'])) {
                if (!$this->isNewRecord) {
                    $folder = $this->cate_id . '/';
                    $this->deleteImage();
                } else {
                    Yii::app()->session['category_upload_folder'] = $folder = md5($_SERVER['REMOTE_ADDR']) . '_' . time() . '/';
                }
//upload image
                $uploadedFile = vH_Utils::alias($_FILES['Category']['name']['image']) . vH_Utils::fileEx(CUploadedFile::getInstance($this, 'image'), true);
                if (!empty($uploadedFile)) {
                    Yii::app()->session['category_upload_image'] = $uploadedFile = vH_Upload::uploadFile('Category', 'image', $this->upload_dir . $folder, $uploadedFile);
                    $this->image = $uploadedFile;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            if (!empty($_FILES['Category']['name']['image'])) {
                if (!$this->isNewRecord) {
                    $folder = $this->cate_id . '/';
                    $this->deleteImage();
                    $uploadedFile = vH_Utils::alias($_FILES['Category']['name']['image']) . vH_Utils::fileEx(CUploadedFile::getInstance($this, 'image'), true);
                    if (!empty($uploadedFile)) {
                        $uploadedFile = vH_Upload::uploadFile('Category', 'image', $this->upload_dir . $folder, $uploadedFile);
                        $this->image = $uploadedFile;
                    }
                } else {
                    $folder = Yii::app()->session['category_upload_folder'];
                }
            }

            if ($this->isNewRecord) {
                $this->created_at = date('Y-m-d H:i:s', time());
            }


            return true;
        } else
            return false;
    }

    protected function afterSave() {
        parent::afterSave();

        if ($this->isNewRecord) {
            if (!empty(Yii::app()->session['category_upload_folder'])):
                $category_upload_folder = Yii::app()->session->get('category_upload_folder');
                rename($this->upload_dir . $category_upload_folder, $this->upload_dir . $this->cate_id);
                unset(Yii::app()->session['category_upload_folder']);
            endif;
        }
    }

    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            $this->deleteImage(true);
            return true;
        } else
            return false;
    }

    private function deleteImage($d = false) {
        if ($this->upload_dir != "") {
            vH_Utils::rmdirf_($this->upload_dir . $this->cate_id);
            if ($d)
                vH_Utils::rmdir_($this->upload_dir . $this->cate_id);
        }
    }

}
