<?php

/**
 * This is the model class for table "{{partner}}".
 *
 * The followings are the available columns in table '{{partner}}':
 * @property integer $partner_id
 * @property string $title
 * @property string $image
 * @property string $description
 * @property string $content
 * @property string $created_at
 * @property string $created_by
 * @property integer $status
 */
class Partner extends CommonAR {

    public $upload_dir;

    public function init() {
        $this->upload_dir = Yii::app()->params['uploadPath']['partner'];
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'Partner';
    }

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(),
                'defaultStickOnClear' => false
            ),
        );
    }

    public function tableName() {
        return '{{partner}}';
    }

    public function rules() {
        return array(
            array('title', 'unique'),
            array('title', 'required'),
            array('sort_order, status', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            array('image, description, content, created_at,date_first,date_last', 'safe'),
            array('image', 'file', 'allowEmpty' => true, 'types' => 'jpg, png, jpeg', 'maxSize' => 1024 * 1024 * 5),
            // The following rule is used by search().
// Please remove those attributes that should not be searched.
            array('partner_id, sort_order, title, image, created_at, status', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        $a = array(
            'partner_id' => 'Partner',
            'title' => 'Title',
            'image' => 'Image',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'status' => 'Status',
            'sort_order' => 'Sort Order',
        );
        foreach ($a as $key => $val) {
            $a[$key] = Yii::t('main', $val);
        }
        return $a;
    }

    public function search($cri = '') {

        $criteria = new CDbCriteria;

        $criteria->compare('partner_id', $this->partner_id);
        $criteria->compare('title', trim($this->title), true);
        $criteria->compare('image', trim($this->image), true);
        $criteria->compare('created_at', trim($this->created_at), true);
        $criteria->compare('status', $this->status);
        if ($cri != "")
            $criteria->addCondition($cri);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'created_at DESC'),
        ));
    }

    public function getImage($width, $height = 0) {
        $width = (int) $width;
        $height = (int) $height;
        if ($width <= 0)
            return false;

        $partner_id = $this->partner_id;
        $image = $this->image;

        if (empty($this->image)) {
            $partner_id = 'default';
            $image = 'logo.png';
        }
        $img_data = Controller::getImageUpload((object) array(
                            'folder' => 'partner',
                            'id' => $partner_id,
                            'width' => $width,
                            'height' => $height,
                            'filename' => $image
        ));

        if ($img_data->src == '') {
            $img_data = Controller::getImageUpload((object) array(
                                'folder' => 'partner',
                                'id' => 'default',
                                'width' => $width,
                                'height' => $height,
                                'filename' => 'logo.png'
            ));
        }
        return $img_data;
    }

    public function getUrl() {
        return Yii::app()->createUrl('main/partnerDetail', array(
                    'url_partner' => $this->alias,
        ));
    }

    protected function beforeValidate() {
        if (parent::beforeValidate()) {
            if (!empty($_FILES['Partner']['name']['image'])) {
                if (!$this->isNewRecord) {
                    $folder = $this->partner_id . '/';
                    $this->deleteImage();
                } else {
                    Yii::app()->session['partner_upload_folder'] = $folder = md5($_SERVER['REMOTE_ADDR']) . '_' . time() . '/';
                }
                $uploadedFile = pathinfo($_FILES['Partner']['name']['image'], PATHINFO_FILENAME) . vH_Utils::fileEx(CUploadedFile::getInstance($this, 'image'), true);
                if (!empty($uploadedFile)) {
                    Yii::app()->session['partner_upload_image'] = $uploadedFile = vH_Upload::uploadFile('Partner', 'image', $this->upload_dir . $folder, $uploadedFile);
                    $this->image = $uploadedFile;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            if (!empty($_FILES['Partner']['name']['image'])) {
                if (!$this->isNewRecord) {
                    $folder = $this->partner_id . '/';
                    $this->deleteImage();
                    $uploadedFile = vH_Utils::alias($_FILES['Partner']['name']['image']) . vH_Utils::fileEx(CUploadedFile::getInstance($this, 'image'), true);
                    if (!empty($uploadedFile)) {
                        $uploadedFile = vH_Upload::uploadFile('Partner', 'image', $this->upload_dir . $folder, $uploadedFile);
                        $this->image = $uploadedFile;
                    }
                } else {
                    $folder = Yii::app()->session['partner_upload_folder'];
                }
            }

            if ($this->isNewRecord) {
                $this->created_at = date('Y-m-d H:i:s', time());
            }
            return true;
        } else
            return false;
    }

    protected function afterSave() {
        parent::afterSave();

        if ($this->isNewRecord) {
            if (!empty(Yii::app()->session['partner_upload_folder'])):
                $partner_upload_folder = Yii::app()->session->get('partner_upload_folder');
                rename($this->upload_dir . $partner_upload_folder, $this->upload_dir . $this->partner_id);
                unset(Yii::app()->session['partner_upload_folder']);
            endif;
        }
    }

    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            $this->deleteImage(true);
            return true;
        } else
            return false;
    }

    private function deleteImage($d = false) {
        vH_Utils::rmdirf_($this->upload_dir . $this->partner_id);
        if ($d)
            vH_Utils::rmdir_($this->upload_dir . $this->partner_id);
    }

}
