<?php

/**
 * This is the model class for table "{{rate}}".
 *
 * The followings are the available columns in table '{{rate}}':
 * @property integer $rate_id
 * @property integer $option
 * @property string $comment
 * @property string $created_at
 */
class Rate extends CommonAR {

    public static function model($className = __CLASS__) {
	return parent::model($className);
    }

    public function getResourceName() {
	return 'Rate';
    }

    public function tableName() {
	return '{{rate}}';
    }

    public function behaviors() {
	return array(
	    'ERememberFiltersBehavior' => array(
		'class' => 'application.components.ERememberFiltersBehavior',
		'defaults' => array(),
		'defaultStickOnClear' => false
	    ),
	);
    }

    public function rules() {
	return array(
	    array('option', 'numerical', 'integerOnly' => true),
	    array('comment', 'length', 'max' => 255),
	    array('created_at', 'safe'),
	    // The following rule is used by search().
	    // Please remove those attributes that should not be searched.
	    array('rate_id, option, comment, created_at', 'safe', 'on' => 'search'),
	);
    }

    public function relations() {
	return array(
	);
    }

    public function attributeLabels() {
	return array(
	    'rate_id' => 'Rate',
	    'option' => 'Đánh giá',
	    'comment' => 'Bình luận',
	    'created_at' => 'Ngày tạo',
	);
    }

    public function search() {

	$criteria = new CDbCriteria;

	$criteria->compare('rate_id', $this->rate_id);
	$criteria->compare('option', $this->option);
	$criteria->compare('comment', trim($this->comment), true);
	$criteria->compare('created_at', trim($this->created_at), true);

	return new CActiveDataProvider($this, array(
	    'criteria' => $criteria,
	    'sort' => array('defaultOrder' => 'rate_id DESC'),
	));
    }

    protected function beforeSave() {
	if (parent::beforeSave()) {
	    if ($this->isNewRecord) {
		$this->created_at = date('Y-m-d H:i:s', time());
	    }
	    return true;
	} else
	    return false;
    }

}
