<?php

/**
 * This is the model class for table "{{services}}".
 *
 * The followings are the available columns in table '{{services}}':
 * @property integer $service_id
 * @property string $title
 * @property string $alias
 * @property string $image
 * @property string $description
 * @property string $content
 * @property string $created_at
 * @property string $created_by
 * @property integer $status
 */
class Services extends CommonAR {

    public $upload_dir;

    public function init() {
        $this->upload_dir = Yii::app()->params['uploadPath']['services'];
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'Services';
    }

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(),
                'defaultStickOnClear' => false
            ),
        );
    }

    public function tableName() {
        return '{{services}}';
    }

    public function rules() {
        return array(
            array('title', 'unique'),
            array('title,language, content', 'required'),
            array('sort_order, cate_id, status,focus', 'numerical', 'integerOnly' => true),
            array('title, alias', 'length', 'max' => 255),
            array('created_by', 'length', 'max' => 50),
            array('image, description, content, created_at,date_first,date_last', 'safe'),
            array('image', 'file', 'allowEmpty' => true, 'types' => 'jpg, png, jpeg', 'maxSize' => 1024 * 1024 * 5),
            // The following rule is used by search().
// Please remove those attributes that should not be searched.
            array('service_id, sort_order,cate_id, title, focus, alias, language, image, description, content, created_at, created_by, status', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'category' => array(self::BELONGS_TO, 'Category', 'cate_id'),
        );
    }

    public function attributeLabels() {
        $a = array(
            'service_id' => 'Services',
            'title' => 'Title',
            'alias' => 'Alias',
            'image' => 'Image',
            'language' => "Language",
            'description' => 'Description',
            'content' => 'Content',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'status' => 'Status',
            'sort_order' => 'Sort Order',
            'focus' => 'Focus',
            'cate_id' => 'Category',
        );
        foreach ($a as $key => $val) {
            $a[$key] = Yii::t('main', $val);
        }
        return $a;
    }

    public function search($cri = '') {

        $criteria = new CDbCriteria;

        $criteria->compare('service_id', $this->service_id);
        $criteria->compare('title', trim($this->title), true);
        $criteria->compare('alias', trim($this->alias), true);
        $criteria->compare('image', trim($this->image), true);
        $criteria->compare('language', $this->language);
        $criteria->compare('description', trim($this->description), true);
        $criteria->compare('content', trim($this->content), true);
        $criteria->compare('created_at', trim($this->created_at), true);
        $criteria->compare('created_by', trim($this->created_by), true);
        $criteria->compare('status', $this->status);
        $criteria->compare('focus', $this->focus);
        $criteria->compare('cate_id', $this->cate_id);
        if ($cri != "")
            $criteria->addCondition($cri);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'created_at DESC'),
        ));
    }

    public function getImage($width, $height = 0) {
        $width = (int) $width;
        $height = (int) $height;
        if ($width <= 0)
            return false;

        $service_id = $this->service_id;
        $image = $this->image;

        if (empty($this->image)) {
            $service_id = 'default';
            $image = 'logo.png';
        }
        $img_data = Controller::getImageUpload((object) array(
                            'folder' => 'services',
                            'id' => $service_id,
                            'width' => $width,
                            'height' => $height,
                            'filename' => $image
        ));

        if ($img_data->src == '') {
            $img_data = Controller::getImageUpload((object) array(
                                'folder' => 'services',
                                'id' => 'default',
                                'width' => $width,
                                'height' => $height,
                                'filename' => 'logo.png'
            ));
        }
        return $img_data;
    }

    public function getUrl() {
        return Yii::app()->createUrl('main/detailServices/lang/' . Yii::app()->language, array(
                    'url_cate' => $this->category->alias,
                    'url_services' => $this->alias,
        ));
    }

    protected function beforeValidate() {
        if (parent::beforeValidate()) {
            if (!empty($_FILES['Services']['name']['image'])) {
                if (!$this->isNewRecord) {
                    $folder = $this->service_id . '/';
                    $this->deleteImage();
                } else {
                    Yii::app()->session['services_upload_folder'] = $folder = md5($_SERVER['REMOTE_ADDR']) . '_' . time() . '/';
                }
                $uploadedFile = vH_Utils::alias($_FILES['Services']['name']['image']) . vH_Utils::fileEx(CUploadedFile::getInstance($this, 'image'), true);
                if (!empty($uploadedFile)) {
                    Yii::app()->session['services_upload_image'] = $uploadedFile = vH_Upload::uploadFile('Services', 'image', $this->upload_dir . $folder, $uploadedFile);
                    $this->image = $uploadedFile;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            if (!empty($_FILES['Services']['name']['image'])) {
                if (!$this->isNewRecord) {
                    $folder = $this->service_id . '/';
                    $this->deleteImage();
                    $uploadedFile = vH_Utils::alias($_FILES['Services']['name']['image']) . vH_Utils::fileEx(CUploadedFile::getInstance($this, 'image'), true);
                    if (!empty($uploadedFile)) {
                        $uploadedFile = vH_Upload::uploadFile('Services', 'image', $this->upload_dir . $folder, $uploadedFile);
                        $this->image = $uploadedFile;
                    }
                } else {
                    $folder = Yii::app()->session['services_upload_folder'];
                }
            }

            if ($this->isNewRecord) {
                $this->created_at = date('Y-m-d H:i:s', time());
            }
            return true;
        } else
            return false;
    }

    protected function afterSave() {
        parent::afterSave();

        if ($this->isNewRecord) {
            if (!empty(Yii::app()->session['services_upload_folder'])):
                $services_upload_folder = Yii::app()->session->get('services_upload_folder');
                rename($this->upload_dir . $services_upload_folder, $this->upload_dir . $this->service_id);
                unset(Yii::app()->session['services_upload_folder']);
            endif;
        }
    }

    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            $this->deleteImage(true);
            return true;
        } else
            return false;
    }

    private function deleteImage($d = false) {
        vH_Utils::rmdirf_($this->upload_dir . $this->service_id);
        if ($d)
            vH_Utils::rmdir_($this->upload_dir . $this->service_id);
    }

}
