<?php
class PasswordRecovery extends CFormModel
{
	public
		$email,
		$user,
		$code,
		$password_n,
		$password_c;

	public function rules()
	{
		return array(
			array('email', 'required', 'on'=>'get'),
			array('email','email','message' => 'Email không phải là một email hợp lệ.','on'=>'get'),
			array('email','validateEmailExists','on'=>'get'),
			
			array('password_n, password_c', 'required', 'on'=>'change'),
			array('password_n, password_c', 'length', 'min'=>6, 'on'=>'change'),
            array('password_c', 'compare', 'compareAttribute' => 'password_n', 'on' => 'change'),
		);
	}
	
	public function validateEmailExists()
	{
		if(!CModel::hasErrors()){
			$user = User::model()->findByAttributes(array(
				'email' => $this->email
			));
			if(empty($user)){
				$this->addError('email','Không tìm thấy tài khoản có email <strong>'.$this->email.'</strong>');
				return false;
			}else{
				$this->user = $user;
				$this->code = md5(Yii::app()->params['salt']);
				User::model()->updateByPk($user->user_id,array(
					'code' => $this->code
				));
				return true;
			}
		}
		return true;
	}
	
	public function attributeLabels()
	{
		return array(
			'email' => 'Địa chỉ email khôi phục',
			'password_n' => 'Mật khẩu mới',
			'password_c' => 'Nhập lại mật khẩu mới',
		);
	}
}
