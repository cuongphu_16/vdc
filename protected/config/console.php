<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'J4S',
    'language' => 'vi',
    'timezone' => 'Asia/Saigon',
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.debugtoolbar.*',
//        'ext.eoauth.*',
//        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.*',
        'ext.eauth.services.*',
    ),
    'commandMap' => array(
        'cron' => 'ext.PHPDocCrontab.PHPDocCrontab'
    ),
    // application components
    'components' => array(
        'request' => array(
            'enableCookieValidation' => true,
        ),
        'metadata' => array('class' => 'ext.Metadata'),
        'clientScript' => array(
            //'class'=>'vH_clientScript',
            'packages' => array(
                'jquery' => false
            )
        ),
        'file' => array(
            'class' => 'application.extensions.file.CFile',
        ),
        // set two default role
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
        //'defaultRoles'=>array('authenticated', 'admin'),
        ),
        'user' => array(
            // enable cookie-based authentication
            //'class'=>'FrontUser',
            'allowAutoLogin' => true,
        ),
        //'session'=>array(
        // enable cookie-based authentication
        //'class'=>'FrontSession',
        //),
        // uncomment the following to enable URLs in path-format
        'urlManager' => require(dirname(__FILE__) . '/urlManager.php'),
//        'db' => require(dirname(__FILE__) . '/db.php'),
        'db' => require(dirname(__FILE__) . '/db.php'),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'main/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    //'levels'=>'error, warning',
                    'levels' => 'trace, info, error, warning',
                ),
            ),
        ),
        'cache' => array(
            'class' => 'system.caching.CFileCache',
        ),
        'settings' => array(
            'class' => 'SiteSettings',
            'cacheComponentId' => 'cache',
            'cacheId' => 'global_website_settings',
            'cacheTime' => 84000,
            'tableName' => '{{settings}}',
            'dbComponentId' => 'db',
            'createTable' => true,
            'dbEngine' => 'InnoDB',
        ),
        'counter' => array(
            'class' => 'SiteCounter',
            'cacheComponentId' => 'cache',
            'cacheId' => 'global_website_counter',
            'cacheTime' => 84000,
            'tableName' => '{{counter}}',
            'dbComponentId' => 'db',
            'createTable' => true,
            'dbEngine' => 'InnoDB',
        ),
        'Mailer' => array(
            'class' => 'ext.PhpMailer.Mailer',
        ),
    ),
    'params' => require(dirname(__FILE__) . '/params2.php'),
);
