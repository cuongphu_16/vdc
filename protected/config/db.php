<?php
	return array(
		/*
		// uncomment the following to use a sqlite or MySQL database
		'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		*/
		
		'connectionString' => 'mysql:host=127.0.0.1;dbname=vdc',
		'emulatePrepare' => true,
		'username' => 'root',
		'password' => '',
		'charset' => 'utf8',
		'tablePrefix' => 'pk_',
		'schemaCachingDuration'=>3600,
                'schemaCacheID' => 'dbSchemaCache',
                'enableParamLogging' => true,
                'enableProfiling' => true,
	);
?>