<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Fairway',
    'language' => 'vi',
    'timezone' => 'Asia/Saigon',
    'defaultController' => 'main',
    'theme' => 'default',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.debugtoolbar.*',
//        'ext.eoauth.*',
//        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.*',
        'ext.eauth.services.*',
        'ext.vmgsms.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool
        /* */
        'gii' => array(
            //'class'=>'system.gii.GiiModule',
            'password' => '123456',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
        'admincp',
        'backup' => array(
            'path' => __DIR__ . '/../_backup/',
        ),
    ),
    // application components
    'components' => array(
        'request' => array(
            'enableCookieValidation' => true,
        ),
        'metadata' => array('class' => 'ext.Metadata'),
        'clientScript' => array(
            //'class'=>'vH_clientScript',
            'packages' => array(
                'jquery' => false
            )
        ),
        'file' => array(
            'class' => 'application.extensions.file.CFile',
        ),
        // set two default role
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
        //'defaultRoles'=>array('authenticated', 'admin'),
        ),
        'user' => array(
            // enable cookie-based authentication
            //'class'=>'FrontUser',
            'allowAutoLogin' => true,
        ),
        'MobileDetect' => array(
            'class' => 'ext.MobileDetect.MobileDetect'
        ),
        //'session'=>array(
        // enable cookie-based authentication
        //'class'=>'FrontSession',
        //),
        // uncomment the following to enable URLs in path-format
        'urlManager' => require(dirname(__FILE__) . '/urlManager.php'),
//        'db' => require(dirname(__FILE__) . '/db.php'),
        'db' => require(dirname(__FILE__) . '/db.php'),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'main/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    //'levels'=>'error, warning',
                    'levels' => 'trace, info, error, warning',
                ),
            //hien thi cau query duoi chan trang
            /* array(
              'class'=>'CWebLogRoute',
              'levels'=>'trace, info, error, warning',
              'categories'=>'system.db.*',
              ), */
            ),
        ),
        'cache' => array(
//            'class' => 'CMemCache',
//            'servers' => array(
//                array(
//                    'host' => '127.0.0.1',
//                    'port' => 11211,
//                    'weight' => 60,
//                )
//            ),
            'class' => 'system.caching.CFileCache',
        ),
        'settings' => array(
            'class' => 'SiteSettings',
            'cacheComponentId' => 'cache',
            'cacheId' => 'global_website_settings',
            'cacheTime' => 84000,
            'tableName' => '{{settings}}',
            'dbComponentId' => 'db',
            'createTable' => true,
            'dbEngine' => 'InnoDB',
        ),
        'counter' => array(
            'class' => 'SiteCounter',
            'cacheComponentId' => 'cache',
            'cacheId' => 'global_website_counter',
            'cacheTime' => 84000,
            'tableName' => '{{counter}}',
            'dbComponentId' => 'db',
            'createTable' => true,
            'dbEngine' => 'InnoDB',
        ),
        'vH_Upload' => array(
            'class' => 'ext.vH_Upload.vH_Upload'
        ),
        'phpThumb' => array(
            'class' => 'ext.EPhpThumb.EPhpThumb',
        ),
        'Mailer' => array(
            'class' => 'ext.PhpMailer.Mailer',
        ),
        'facebook' => require(dirname(__FILE__) . '/facebook.php'),
        'loid' => array(
            'class' => 'ext.lightopenid.loid',
        ),
        'eauth' => array(
            'class' => 'ext.eauth.EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache'.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'services' => array(// You can change the providers and their classes.
                'facebook' => array(
                    // register your app here: https://developers.facebook.com/apps/
                    'class' => 'FacebookOAuthService',
                    'client_id' => '508145622661882',
                    'client_secret' => '0d194f82c46296e37570ddf07ee23fd9',
                ),
                'google_oauth' => array(
                    // register your app here: https://code.google.com/apis/console/
                    'class' => 'GoogleOAuthService',
                    'client_id' => '452000195547-s1rdbm1g445ju34m2i5ji3g01msso5er.apps.googleusercontent.com',
                    'client_secret' => 'IZ9cPqRFe50gEGZV3eRde5FO',
                    'title' => 'Google (OAuth)',
                ),
            ),
        ),
    ),
    'params' => require(dirname(__FILE__) . '/params.php'),
);
