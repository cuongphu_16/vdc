<?php

/* $f = @fopen($fn='admindir.txt','r+');
  $admin_dir = @fread($f,filesize($fn));
  @fclose($f);
  if(empty($admin_dir)) */
$admin_dir = 'admincp';

return array(
    //'class'=>'application.extensions.multilanguage.MLUrlManager',
    //'languages'=>array('en','vi'),
    'urlFormat' => 'path',
    'showScriptName' => false,
    'rules' => array(
        '<module:(' . $admin_dir . ')>' => 'admincp/main/index',
        '<module:(' . $admin_dir . ')>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
        'user/captcha' => 'user/captcha/lang/vi',
        'gii' => 'gii',
        'gii/model' => 'gii/model',
        'gii/crud' => 'gii/crud',
        /*
         *  đường dẫn cho mainController
         * ***************************************************************** */
        '/loaddata' => 'loadData/index',
        "<lang>/" => array('main/index/lang/<lang>'),
        /*         * ************************ URL VI *********************************** */
        'vi/gioi-thieu/<url_about>.html' => 'main/listAbout/lang/vi',
        'vi/gioi-thieu/<url_about>.html' => 'main/detailAbout/lang/vi',
        'vi/dich-vu/<url_cate>/<url_services>.html' => 'main/detailServices/lang/vi',
        'vi/tin-tuc/<url_news>.html' => 'main/detailNews/lang/vi',
        'vi/san-giao-dich/<url_trading>.html' => 'main/detailTrading/lang/vi',
        'vi/tuyen-dung/<url_recruitment>.html' => 'main/detailRecruitment/lang/vi',
        'vi/gioi-thieu.html' => 'main/about/lang/vi',
        'vi/du-an.html' => 'main/project/lang/vi',
        'vi/lien-he.html' => 'main/contact/lang/vi',
        'vi/tuyen-dung.html' => 'main/recruitment/lang/vi',
        'vi/dich-vu.html' => 'main/services/lang/vi',
        'vi/dich-vu/<url_services>.html' => 'main/listServices/lang/vi',
        'vi/du-an/<url_project>.html' => 'main/listProject/lang/vi',
        'vi/tin-tuc.html' => 'main/news/lang/vi',
        'vi/san-giao-dich.html' => 'main/trading/lang/vi',
        'vi/chi-tiet-du-an/<url_project>.html' => 'main/projectDetail/lang/vi',
        /*         * ************************ URL VI *********************************** */


        /*         * ************************ URL EN *********************************** */
        'en/about-us/<url_about>.html' => 'main/listAbout/lang/en',
        'en/about-us/<url_cate>/<url_about>.html' => 'main/detailAbout/lang/en',
        'en/services/<url_cate>/<url_services>.html' => 'main/detailServices/lang/en',
        'en/news/<url_news>.html' => 'main/detailNews/lang/en',
        'en/trading/<url_trading>.html' => 'main/detailTrading/lang/en',
        'en/recruitment/<url_recruitment>.html' => 'main/detailRecruitment/lang/en',
        'en/about-us.html' => 'main/about/lang/en',
        'en/project.html' => 'main/project/lang/en',
        'en/contact.html' => 'main/contact/lang/en',
        'en/recruiment.html' => 'main/recruitment/lang/en',
        'en/services.html' => 'main/services/lang/en',
        'en/services/<url_services>.html' => 'main/listServices/lang/en',
        'en/project/<url_project>.html' => 'main/listProject/lang/en',
        'en/news.html' => 'main/news/lang/en',
        'en/trading.html' => 'main/trading/lang/en',
        'en/project-detail/<url_project>.html' => 'main/projectDetail/lang/en',
        /*         * ************************ URL EN *********************************** */
        "/" => 'main/index',
        'main/<action:(loadRegions|loginSocial|loadBornRegionsUser|loadBornDistrictUser|loaddata|loadRegionsOption|loadDictrictOption|voted)>' => 'main/<action>',
        '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
        '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
    ),
);
?>