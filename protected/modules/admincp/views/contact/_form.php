<div class="form">

    <?php
    $form = $this->beginWidget('vH_CActiveForm', array(
        'id' => 'contact-form',
    ));
    ?>

    <?php if ($msg = Yii::app()->admin->showPutMsg()) echo $msg . '<br>'; ?>
    <?php if ($error = $form->errorSummary($model)) echo $error . '<br>'; ?>

    <table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
        <tr>
            <th colspan="2" align="left">Thông tin</th>
        </tr>
        <tr>
            <td colspan="2"><p class="note">Điền đầy đủ các mục có dấu <span class="required">*</span>.</p></td>
        </tr>
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'fullname'); ?></strong></td>		
            <td><?=$model->fullname?></td>
        </tr>	
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'address'); ?></strong></td>		
            <td colspan="3"><?=$model->address?></td>
        </tr>
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'email'); ?></strong></td>
            <td colspan="3"><?=$model->email?></td>
        </tr>
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'phone'); ?></strong></td>
            <td colspan="3"><?=$model->phone?></td>
        </tr>
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'ip'); ?></strong></td>
            <td colspan="3"><?=$model->ip?></td>
        </tr>
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'created_at'); ?></strong></td>
            <td colspan="3"><?= AdmincpHelper::dateConvert($model->created_at)?></td>
        </tr>
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'content'); ?></strong></td>
            <td colspan="3"><?=$model->content?></td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'status'); ?></strong></td>
            <td>
                <?php echo $form->dropDownList($model, 'status', AdmincpHelper::getViewStatusOptions(), array('style' => 'width: 150px')); ?>
                <?php echo $form->error($model, 'status'); ?>
            </td>
        </tr>

    </table>
    <?php $this->endWidget(); ?>
</div><!-- form -->