<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('contact-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>
<?php if ($msg = Yii::app()->admin->showPutMsg()) echo $msg . '<br>'; ?>
<?php
$this->widget('vH_CGridView', array(
    'id' => 'contact-grid',
    'dataProvider' => $model->search(),
    'cssFile' => $this->module->getAssetsUrl() . '/css/CGridView.css',
    'itemsCssClass' => 'adminlist',
    'itemAttributes' => array('cellspacing' => '1', 'cellpadding' => '3', 'border' => '0'),
    //'afterAjaxUpdate' => "function(){jQuery('#".CHtml::activeId($model, 'create_time')."').datepicker({'dateFormat':'yy-mm-dd'})}",
    'pager' => array(
        'class' => 'vH_CLinkPager',
    ),
    'filter' => $model,
    'columns' => array(
        array(
            'header' => 'STT',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'htmlOptions' => array('style' => 'text-align:center; width:30px; font-weight:bold'),
        ),
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'ids',
            'selectableRows' => 2,
            'htmlOptions' => array('style' => 'text-align:center; width:30px'),
        ),
        array(
            'name' => 'fullname',
//            'value' => 'CHtml::link("$data->fullname",array("contact/update/","id"=>$data->contact_id))',
            'type' => 'html',
            'htmlOptions' => array('style' => 'width:80px'),
        ),
        array(
            'name' => 'email',
            'type' => 'html',
        ),
        array(
            'name' => 'title',
            'type' => 'html',
            'htmlOptions' => array('style' => 'width:250px'),
        ),
        array(
            'name' => 'content',
            'type' => 'html',
        ),
        array(
            'name' => 'created_at',
            'type' => 'raw',
            'filter' => '',
            'htmlOptions' => array('style' => 'width:150px; text-align:center'),
        ),
//        array(
//            'name' => 'status',
//            'type' => 'raw',
//            'value' => '$data->status == 1 ? "Đã xem" : "Chưa xem"',
//            'filter' => AdmincpHelper::getViewStatusOptions(),
//            'htmlOptions' => array('style' => 'width:80px; text-align:center'),
//        ),
//        array(
//            'class' => 'vH_CButtonColumn'
//        ),
    ),
));
