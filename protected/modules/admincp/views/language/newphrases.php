<?php $form=$this->beginWidget('vH_CActiveForm', array(
	'id'=>'language-form',
)); ?>
<?php
	if($err = $form->errorSummary($model))
		echo $err.'<br>';
?>
<table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
	<tr>
		<th colspan="2" align="left">Thông tin</th>
	</tr>
	<!--<tr>
		<td width="20%"><?php //echo $form->labelEx($model,'category')?></td>
		<td width="80%"><?php //echo $form->hiddenField($model,'category')?></td>
	</tr>-->
	<tr>
		<td width="20%"><?=$form->labelEx($model,'message')?></td>
		<td>
			<?php 
				echo $form->textField($model,'message',array('size'=>60));
				echo $form->error($model,'message');
			?>
		</td>
	</tr>
	<tr>
		<th colspan="2" align="left">Dịch</th>
	</tr>
	<?php
		foreach($languages as $k=>$lang):
	?>
	<tr>
		<td><strong><?=$lang->title?></strong></td>
		<td>
			<? //print_r($lang->trans->attributes);?>
			<textarea name="Message[<?=$k?>][translation]" cols="60"><?php $msg=$lang->getMsgTrans($source->id)->translation; if(!empty($msg))echo $msg; else echo $source->message;?></textarea>
			<input type="hidden" name="Message[<?=$k?>][language]" value="<?=$lang->code?>" />
		</td>
	</tr>
	<?php
		endforeach;
	?>
</table>
<?php $this->endWidget(); ?>
