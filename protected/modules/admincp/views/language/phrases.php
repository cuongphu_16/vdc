<?php
/*$this->breadcrumbs=array(
	'Languages'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Language', 'url'=>array('index')),
	array('label'=>'Create Language', 'url'=>array('create')),
);
*/
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('language-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<!--<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array('model'=>$model,)); ?>
</div>-->

<?php $this->widget('vH_CGridView', array(
	'id'=>'language-grid',
	'dataProvider'=>$model->search(),
	
	'cssFile' => $this->module->getAssetsUrl().'/css/CGridView.css',
	'itemsCssClass' => 'adminlist',
	'itemAttributes' => array('cellspacing'=> '1', 'cellpadding'=>'3', 'border'=> '0'),
	//'afterAjaxUpdate' => "function(){jQuery('#".CHtml::activeId($model, 'create_time')."').datepicker({'dateFormat':'yy-mm-dd'})}",
	'pager' => array(
		'class' => 'vH_CLinkPager',
	),
	
	'filter'=>$model,
	'columns'=> $columns,
)); ?>
