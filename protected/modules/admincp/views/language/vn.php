<?php
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($this->module->assetsUrlRoot . '/js/jCore.js');
?>

<?php
$form = $this->beginWidget('vH_CActiveForm', array(
    'id' => 'language-form',
        ));
?>
<input type="hidden" name="keywordsearch" id="keywordsearch" value="<?= (isset($keyword)) ? $keyword : "" ?>" />
<input type="hidden" name="submittype" id="submittype" value="save" />
<?php $this->endWidget(); ?>
<div style=" display:none">
    <div id="addlang-form" style=" width:470px; padding:10px;">
        <div style="margin-bottom: 10px;font-size: 16px;font-weight: bold;">Thêm cấu hình Tiếng Việt</div>
        <div class="error fl" style="width:95%; display:none"></div>
        <div class="fl">
            <label style="float:left; margin-bottom:5px;"><strong>Nội dung Tiếng Anh</strong></label>
            <textarea id="lang_key" name="lang_key" style=" width:95%; height:90px; resize:none"></textarea>
            <label style="margin-bottom: 5px;margin-top: 10px;float: left;"><strong>Nội dung Tiếng Việt</strong></label>
            <textarea id="lang_value" name="lang_value" style="width:95%; height:90px; resize:none"></textarea><br />
            <label style="margin-bottom: 5px;margin-top: 10px;float: left;"><strong>Áp dụng cho:</strong></label><span style="padding-top: 10px;float: left;padding-left: 10px;"><input type="radio" name="lang_type" id="lang_type_0" value="0" checked="checked" />PHP&nbsp;&nbsp;&nbsp;<input type="radio" name="lang_type" id="lang_type_1" value="1" />JS</span>
        </div>
        <div class="fl ToolBarBtn" style="margin-bottom:10px"><a class="save" href="javascript:;" id="addLangSave" style="margin-left:0px">Lưu lại</a></div>
    </div>
</div>
<table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
    <tr>
        <th align="left">Thông tin tiếng Anh</th>
        <th align="left">Thông tin tiếng Việt</th>
    </tr>
    <?php
    if (!empty($langvns)) {
        foreach ($langvns as $lang) {
            ?>
            <tr>
                <td>
                    <?php echo $lang->key ?>
                </td>
                <td width="50%">
                    <textarea style="width:100%; height:50px" name="Config[]" id="Config" rel="<?= $lang->id ?>" class="lang_content" data-rel="<?= $lang->type ?>" data-content=""><?= $lang->value ?></textarea>
                </td>
            </tr>
            <?php
        }
    }
    ?>
</table>
<div style="margin-top: 30px; text-align: center">
<?php
$this->widget('CLinkPager', array(
    'pages' => $pages,
))
?>
</div>
<script language="javascript" type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/language/language_js.js"></script>

<script type="text/javascript">
    $(function () {
        $('.page_breadcrumb').html('<div class="ToolBarBtn"><ul class="operations" id="yw2"><li></li><li><input name="keyword" id="keyword" type="text" maxlength="300" value="<?= (isset($keyword)) ? $keyword : "" ?>" style="float:left; height:28px; line-height:24px; width: 300px"><a class="search" href="javascript:;" id="searchLang" style="padding-left:8px">Tìm kiếm</a></li><li><a class="add" href="javascript:;" id="addLang" style="margin-left:10px">Thêm mới</a></li></ul></div>');
        $('.lang_content').bind('change', function () {
            console.log($(this).val(), $(this).attr('rel'));
            $.ajax({
                url: "<?= $this->createAbsoluteUrl("language/udpatelang") ?>",
                data: {'content': $(this).val(), 'id': $(this).attr('rel'), 'type': $(this).attr('data-rel'), 'data-key': $(this).attr('data-content')},
                type: 'post',
                dataType: "json",
                success: function (data) {
                    console.log(data);
                }
            });
        });
        $('#searchLang').bind('click', function () {
            var keyword = $('#keyword').val();

            $('#submittype').val('search');
            $('#keywordsearch').val(keyword);
            $('#language-form').submit();
        });
        $('#keyword').bind('keypress', function (e) {
            evt = e || window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13)
                $('#searchLang').click();
        });
        $('#addLang').bind('click', function () {
            $().colorbox({
                href: '#addlang-form',
                inline: true,
                onComplete: function () {
                },
                onClosed: function () {
                    $('#lang_key').val('');
                    $('#lang_value').val('');
                },
            });
        });
        $('#addLangSave').off('click').on('click', function () {
            if ($('#lang_key').val() != "" && $('#lang_value').val() != "")
            {
                $('#addLangSave').attr('disabled', true);
                $.ajax({
                    'url': '<?= $this->createAbsoluteUrl("language/addlang") ?>',
                    type: 'post',
                    dataType: "json",
                    data: {'lang_key': $('#lang_key').val(), 'lang_value': $('#lang_value').val(), 'lang_type': $('input[name=lang_type]:radio:checked').val()},
                    success: function (data) {
                        console.log(data);
                        if (data.success == 'success')
                        {
                            window.location.reload();
                        } else {
                            $().colorbox().resize();
                        }
                        $('#addLangSave').attr('disabled', false);
                    },
                });
            }
        });
    });
</script>

