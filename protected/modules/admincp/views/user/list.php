<?php
$html = '';
$crypt = new Crypt(Yii::app()->params['ENCRYPTION_KEY']);
if (!empty($model)):
    foreach ($model as $user):
        $count = count($user->orders) > 0 ? ' rowspan="' . count($user->orders) . '"' : '';
        if (count($user->orders) > 0):
            $html .= "<tr>";
            $html .= "<td{$count} valign=\"top\">{$user->user_id}</td>\r\n";
            $html .= "<td{$count} valign=\"top\">{$user->fullname}</td>\r\n";
            $html .= "<td{$count} valign=\"top\"><div style=\"margin-bottom: 7px\">{$user->email}</div>{$user->address}</td>\r\n";
            if (count($user->orders) > 0):
                foreach ($user->orders as $key => $order):
                    if ($key > 0) {
                        $html.= "<tr>";
                    }
                    $html .= "<td>{$order->id}</td>\r\n";
                    $html .= "<td>{$order->service->services_title}</td>\r\n";
                    $html .= "<td>{$order->camera_link}</td>\r\n";
                    $html .= "<td>{$order->camera_username}</td>\r\n";
                    $html .= "<td>{$crypt->decrypt($order->camera_password)}</td>\r\n";
                    $html .= "</tr>\r\n";
                endforeach;
            else:
                $html .= "<td></td>\r\n";
                $html .= "<td></td>\r\n";
                $html .= "<td></td>\r\n";
                $html .= "<td></td>\r\n";
                $html .= "<td></td>\r\n";
                $html .= "</tr>\r\n";
            endif;
        endif;
    endforeach;
endif;
?>
<table border="1" cellpadding="10" width="100%" spacepadding="0" style="border-collapse: collapse; border: #efefef 1px solid">
    <tr>
        <th>ID</th>
        <th>Thành viên</th>
        <th>Email / Địa chỉ</th>
        <th>Mã đơn hàng</th>
        <th>Gói dịch vụ</th>
        <th>Link camera</th>
        <th>Tên đăng nhập</th>
        <th>Mật khẩu</th>
    </tr>
    <?= $html ?>
</table>