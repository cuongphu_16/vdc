<?php
foreach ($list_education as $education): $i++;
    ?>
    <td>
        <h3 class="title_mobile"><?= Yii::t('main', 'Degree Level') ?></h3>
        <?= SiteHelper::getCatLabel($education->degree_level) ?>
    </td>
    <td>
        <h3 class="title_mobile"><?= Yii::t('main', 'At the school') ?></h3>
        <?= $education->program ?>
    </td>
    <td>
        <h3 class="title_mobile"><?= Yii::t('main', 'Major') ?></h3>
        <?= $education->major ?>
    </td>
    <td class="td_status" style="  padding-right: 10px !important;">
        <h3 class="title_mobile"><?= Yii::t('main', 'Time') ?></h3>
        <?php
        if (!empty($education->from) && !empty($education->to))
            echo $education->from . " - " . $education->to;
        elseif (!empty($education->from) && empty($education->to))
            echo $education->from;
        elseif (empty($education->from) && !empty($education->to))
            echo $education->to;
        else
            echo '';
        ?>
        <a href="javascript:;" class="delete_function" title="<?= Yii::t('main', 'delete') ?>" onclick="deleteEducation(<?= $education->id ?>)"></a>
        <a href="<?= Yii::app()->createAbsoluteUrl('admincp/user/editEducation', array('education_id' => $education->id)) ?>"  class="fancybox fancybox.ajax edit_function" title="<?= Yii::t('main', 'Chỉnh sửa bằng cấp') ?>"></a>   
    </td>
<?php endforeach; ?>