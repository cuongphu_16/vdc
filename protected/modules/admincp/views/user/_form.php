<?php echo $this->module->registerJs('jquery.multiselect.js'); ?>
<?php echo $this->module->registerJs('jquery.multiselect.filter.js'); ?>
<?php echo $this->module->registerCss('jquery.multiselect.css'); ?>
<?php echo $this->module->registerCss('jquery.multiselect.filter.css'); ?>
<div class="form">
    <?php
    $form = $this->beginWidget('vH_CActiveForm', array(
	'id' => 'user-form',
    ));
    echo $form->hiddenField($model, 'type', array('value' => 0));
    ?>
    <?php if ($msg = Yii::app()->admin->showPutMsg()) echo $msg . '<br>'; ?>
    <?php if ($error = $form->errorSummary($model)) echo $error . '<br>'; ?>
    <table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
        <tr>
            <th colspan="4" align="left">Thông tin tài khoản</th>
        </tr>
        <tbody id="thong_tin_tai_khoan">                                      
            <tr>
                <td valign="top" class="images_avatar"><strong><?php echo $form->labelEx($model, 'avatar'); ?></strong></td>		
                <td class="images_avatar">
		    <?php echo $form->fileField($model, 'avatar'); ?>
		    <?php echo $form->error($model, 'avatar'); ?>
		    <?php if (!empty($model->avatar)) : ?>

			<?php echo $form->hiddenField($model, 'avatar_hidden', array('value' => $model->avatar)); ?>
    		    <div style="margin-top: 10px"><?php echo CHtml::image($model->getImage(300, 0)->src); ?></div>
    		    <div style=" margin-top:5px; float:left">
    			<input type="checkbox" value="1" name="delete_avatar" id="delete_avatar" /> <label for="delete_avatar">Xoá avatar</label>
    		    </div>
		    <?php endif; ?>

                </td>                		

        <script type="text/javascript">
	    $("#ytUser_avatar").val($("#User_avatar_hidden").val());
	</script>>
        </tr>
        <tr class="name_to_contact">
            <td valign="top"><strong><?php echo $form->labelEx($model, 'contact_name'); ?></strong></td>	
            <td colspan="3">
		<?php echo $form->textField($model, 'contact_name', array('size' => 45, 'maxlength' => 100)); ?>
		<?php echo $form->error($model, 'contact_name'); ?>
            </td>

        </tr>
        <tr class="info_candidate">
            <td valign="top"><strong><?php echo $form->labelEx($model, 'firstname'); ?></strong></td>		
            <td>
		<?php echo $form->textField($model, 'firstname', array('size' => 45, 'maxlength' => 100)); ?>
		<?php echo $form->error($model, 'firstname'); ?>
            </td>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'lastname'); ?></strong></td>		
            <td>
		<?php echo $form->textField($model, 'lastname', array('size' => 45, 'maxlength' => 100)); ?>
		<?php echo $form->error($model, 'lastname'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'email'); ?></strong></td>		
            <td colspan="3">
		<?php echo $form->textField($model, 'email', array('size' => 45, 'maxlength' => 100)); ?>
		<?php echo $form->error($model, 'email'); ?>
            </td>           

        </tr>

	<?php if ($model->isNewRecord): ?>
    	<tr>
    	    <td valign="top"><strong><?php echo $form->labelEx($model, 'password'); ?></strong></td>
    	    <td>
		    <?php echo $form->passwordField($model, 'password', array('size' => 37, 'maxlength' => 64, 'autocomplete' => 'off')); ?>
		    <?php echo $form->error($model, 'password'); ?>
    	    </td>                
    	    <td valign="top"><strong><?php echo $form->labelEx($model, 'password_cf'); ?></strong></td>
    	    <td>
		    <?php echo $form->passwordField($model, 'password_cf', array('size' => 37, 'maxlength' => 64, 'autocomplete' => 'off')); ?>
		    <?php echo $form->error($model, 'password_cf'); ?>
    	    </td>
    	</tr>
	<?php else: ?>
    	<tr>
    	    <td valign="top"><strong><?php echo $form->labelEx($model, 'password'); ?></strong></td>
    	    <td>
		    <?php echo $form->passwordField($model, 'password', array('size' => 37, 'maxlength' => 64, 'class' => 'fl', 'value' => '', 'disabled' => ($_POST['User']['chg_password'] !== '1'), 'autocomplete' => 'off')); ?>

    		<div class="fl" style="padding:5px 0 0 5px"><input type="checkbox" id="chg_password" value="1" name="User[chg_password]" <?php if ($_POST['User']['chg_password']): ?> checked="checked" <?php endif; ?> class="fl" style="margin:0 3px 0 0" /><label for="chg_password"> Đổi mật khẩu</label></div>
    		<div class="clr"></div>
		    <?php echo $form->error($model, 'password'); ?>
    	    </td>
    	    <td valign="top" class="chg_password_cf" style="<?php if (!$_POST['User']['chg_password']): ?>display:none;<?php endif; ?>"><strong><?php echo $form->labelEx($model, 'password_cf'); ?></strong></td>
    	    <td class="chg_password_cf" style="<?php if (!$_POST['User']['chg_password']): ?>display:none;<?php endif; ?>">
    		<input type="password" id="User_password_cf" name="User[password_cf]" class="<?php if ($form->error($model, 'password_cf')): ?>error<?php endif; ?>" maxlength="64" size="32" autocomplete="off">
		    <?php echo $form->error($model, 'password_cf'); ?>
    	    </td>
    	</tr>
	<?php endif; ?>
        <tr class="not-for-merchant">
            <th colspan="4" align="left">Việc làm mong muốn</th>
        </tr>        
        <tr class="info_candidate">
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'expected_salary'); ?></strong></td>           
            <td class="hide_selectlm">
		<?php echo $form->textField($model, 'expected_salary', array('style' => 'width: 341px')); ?>
		<?php echo $form->error($model, 'expected_salary'); ?>
            </td>

        </tr>
        <tr class="not-for-merchant">
            <th colspan="4" align="left">Chỗ ở hiện tại</th>
        </tr>        
        <tr class="">           
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'address'); ?></strong></td>
            <td colspan="3">
		<?php echo $form->textField($model, 'address', array('size' => 45, 'maxlength' => 100)); ?>
		<?php echo $form->error($model, 'address'); ?>
            </td>
        </tr>


        <tr class="for-merchant">
            <th colspan="4" align="left">Thông tin công ty</th>
        </tr>


        <tr>
            <th colspan="4" align="left">Chi tiết tài khoản</th>
        </tr>

        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'mobile'); ?></strong></td>
            <td>
		<?php echo $form->textField($model, 'mobile', array('size' => 45, 'maxlength' => 100)); ?>
		<?php echo $form->error($model, 'mobile'); ?>
            </td>            
        </tr>   
<!--        <tr class="name_to_contact">
	   
        </tr>-->
        <tr>            
            <td valign="top" class="focus_logo"><strong><?php echo $form->labelEx($model, 'sort_order'); ?></strong></td>
            <td  class="focus_logo">
		<?php echo $form->textField($model, 'sort_order', array('size' => 45, 'maxlength' => 100)); ?>
		<?php echo $form->error($model, 'sort_order'); ?>
            </td>
            <td valign="top" class="info_candidate"><strong><?php echo $form->labelEx($model, 'language_field'); ?></strong></td>
            <td class="info_candidate">
		<?php echo $form->textField($model, 'language_field', array('size' => 45, 'maxlength' => 100, 'readonly' => 'true')); ?>
		<?php echo $form->error($model, 'language_field'); ?>
            </td>
            <td valign="top" class="focus_logo"><strong><?php echo $form->labelEx($model, 'focus_logo'); ?></strong></td>
            <td class="focus_logo">
		<?php echo $form->dropDownList($model, 'focus_logo', array('0' => 'Không nổi bật', '1' => 'Slide ngang', '2' => 'Bảng dọc'), array('style' => 'width:266px')); ?>
		<?php echo $form->error($model, 'focus_logo'); ?>
            </td>
        </tr>   
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'phone'); ?></strong></td>
            <td>
		<?php echo $form->textField($model, 'phone', array('size' => 45, 'maxlength' => 100)); ?>
		<?php echo $form->error($model, 'phone'); ?>
            </td>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'status'); ?></strong></td>
            <td>
		<?php echo $form->dropDownList($model, 'status', AdmincpHelper::getUserStatusOptions(), array('style' => 'width:266px')); ?>
		<?php echo $form->error($model, 'status'); ?>
            </td>
        </tr>
        </tbody>
    </table>
    <?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
    $('#chg_password').change(function() {
	if (this.checked == true) {
	    $('#User_password').attr('disabled', false);
	    $('#User_password').val('');
	    $('.chg_password_cf').show();
	    $('#User_password_cf').val('');
	} else {
	    $('#User_password').val('******');
	    $('#User_password').attr('disabled', true);
	    $('.chg_password_cf').hide();
	}
	$(window).trigger('resize');
    });
    $('#User_user_type').change(function() {
	if ($(this).val() == 1) {
	    $("#name_to_contact").show();
	    $('.role').show();
	    $('.norole').hide();
	    $('#name_company').show();
	    $(".focus_logo").show();
	    $('.not-for-merchant').hide();
	    $(".show_hide_type_account").show();
	    $(".info_candidate").hide();
	} else {
	    $("#name_to_contact").hide();
	    $('.role').hide();
	    $('.norole').show();
	    $('#name_company').hide();
	    $(".focus_logo").hide();
	    $('.not-for-merchant').show();
	    $(".show_hide_type_account").hide();
	    $(".info_candidate").show();
	    $('.for-merchant').hide();
	    $(".images_avatar").show();
	}
	if ($("#User_type_account").val() == 1 && $("#User_user_type").val() == 1) {
	    $('.for-merchant').show();
	    $(".images_avatar").hide();
	} else {
	    $('.for-merchant').hide();
	    $(".images_avatar").show();
	}
    });
    if ($("#User_user_type").val() == 1) {
	$("#name_to_contact").show();
	$('#name_company').show();
	$(".focus_logo").show();
	$('.not-for-merchant').hide();
	$(".show_hide_type_account").show();
	$(".info_candidate").hide();
	if ($("#User_type_account").val() == 1) {
	    $('.for-merchant').show();
	    $(".images_avatar").hide();
	} else {
	    $('.for-merchant').hide();
	    $(".images_avatar").show();
	}
    }
    else {
	$("#name_to_contact").hide();
	$('#name_company').hide();
	$(".focus_logo").hide();
	$('.not-for-merchant').show();
	$('.for-merchant').hide();
	$(".show_hide_type_account").hide();
	$(".info_candidate").show();
    }

    $('#User_type_account').change(function() {
	if ($(this).val() == 1) {
	    if ($("#User_user_type").val() == 1) {
		$('.for-merchant').show();
		$(".images_avatar").hide();
	    } else {
		$('.for-merchant').hide();
		$(".images_avatar").show();
	    }
	} else {
	    $('.for-merchant').hide();
	    $(".images_avatar").show();
	}

    });
    if ($("#User_type_account").val() == 1) {
	if ($("#User_user_type").val() == 1) {
	    $('.for-merchant').show();
	    $(".images_avatar").hide();
	} else {
	    $('.for-merchant').hide();
	    $(".images_avatar").show();
	}
    }

    function deleteImage(id) {
	$.ajax({
	    url: "<?= $this->createUrl("User/deleteImage") ?>",
	    type: "post",
	    dataType: 'html',
	    cache: false,
	    data: {id: id},
	    success: function(data) {
		location.reload();
	    }
	});
    }
    function addSelectItem2(obj, label, id, label2, id2)
    {
	var value = $('#' + obj.data('id')).val();
	if (value != "")
	{
	    arrValue = value.split(',');
	} else {
	    arrValue = new Array();
	}

	var region = $('#' + obj.data('rel')).val();
	if (region != "")
	{
	    arrRegion = region.split(',');
	} else {
	    arrRegion = new Array();
	}
	if ($.inArray(id2, arrValue) == -1)
	{
	    if (arrValue.length < 3) {
		arrValue.push(id2);
		$('#' + obj.data('id')).val(arrValue.join(','));
		$('ul', obj).append('<li data-rel="' + obj.data('rel') + '" data-id="' + obj.data('id') + '"><div>' + label + ' | ' + label2 + '<span data-id="' + id2 + '" data-rel="' + id + '">X</span></div></li>');
		if ($.inArray(id, arrRegion) == -1)
		{
		    arrRegion.push(id);
		    $('#' + obj.data('rel')).val(arrRegion.join(','));
		}

	    } else {
		$('.work_place_error').html('Bạn chỉ có thể chọn tối đa 3 địa điểm').show();
	    }

	}


    }

    $(function() {
	$('.list_active li span').off('click').on('click', function() {
	    var id = $(this).data('id');
	    var obj = $(this).parentsUntil('li').parent();
	    var intpuId = obj.data('id');
	    var files = $('#' + intpuId).val().split(',');
	    y = jQuery.grep(files, function(n, i) {
		if (n != id && n != "")
		    return n;
	    })
	    files = y;
	    $('#' + intpuId).val(files.join(','));
	    if ($('#User_work_place2').val() == "") {
		$('#User_work_place').val('');
		$('#User_work_place').trigger('change');
	    }
	    obj.remove();
	    $('.work_place_error').html('').hide();
	});
	$('#work_place_district').bind('change', function() {
	    var obj_dis = $(this);
	    if ($(this).val() != "") {
		addSelectItem2($('#work_place_select'), $("#work_place_region option:selected").text(), $('#work_place_region').val(), $("#work_place_district option:selected").text(), $('#work_place_district').val());
		$('.list_active li span').off('click').on('click', function() {
		    var id = $(this).data('id');
		    var obj = $(this).parentsUntil('li').parent();
		    var intpuId = obj.data('id');
		    var files = $('#' + intpuId).val().split(',');
		    y = jQuery.grep(files, function(n, i) {
			if (n != id && n != "")
			    return n;
		    })
		    files = y;
		    $('#' + intpuId).val(files.join(','));
		    if ($('#User_work_place2').val() == "") {
			$('#User_work_place').val('');
			$('#User_work_place').trigger('change');
			$('#work_place_district option:eq("")').prop('selected', true);
		    }
		    obj.remove();
		    $('.work_place_error').html('').hide();
		});
	    }
	});
    });
    $('#User_job_category').multiselect({
	header: '<?= Yii::t('main', 'Choose only 3 trades') ?>',
	noneSelectedText: "<?= Yii::t('main', 'Choose trades') ?>",
	selectedText: "<?= Yii::t('main', 'Selected') ?> # <?= Yii::t('main', 'Trades') ?>",
		filter: function(event, matches) {
		    if (!matches.length) {
		    }
		},
		click: function(e) {
		    if ($(this).multiselect("widget").find("input:checked").length > 3)
		    {
			return false;
		    }
		    else
		    {
			return true;
		    }
		}
	    }).multiselectfilter({'label': 'Tìm kiếm', 'placeholder': "Nhập từ khóa"});
	    $("#User_birthday").datepicker({
		dateFormat: "dd-mm-yy",
		changeMonth: true,
		changeYear: true,
		yearRange: '1960:2000'
	    });</script>
<style>
    .hide_selectlm i{
        background: none !important;
    }
    .list_active {
        float: left;
        margin-left: 0px;
        padding-left: 10px;
    }
    .list_active li{
        list-style: none;
        float: left;
        white-space: nowrap;
        margin-right: 5px;
        padding: 3px 5px 3px 13px;
        background: #CCC;
        margin-bottom: 10px;																
    }
    .list_active li span{
        margin-left: 7px;
        color: #8C8C8C;
        cursor:pointer;
    }
    .list_active li span:hover{
        color: #FFF;

    }
</style>