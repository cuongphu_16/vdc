
<div style="font-weight: bold; margin-bottom: 20px;"><a style="background: #3b5998;  color: #fff;  padding: 10px;" href="<?= $this->createUrl('user/excel'); ?>">Xuất dữ liệu thành viên</a></div>
<div class="clr"></div>


<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>

<?php
$this->widget('vH_CGridView', array(
    'id' => 'user-grid',
    'dataProvider' => $model->search('type = 0'),
    'cssFile' => $this->module->getAssetsUrl() . '/css/CGridView.css',
    'itemsCssClass' => 'adminlist',
    'itemAttributes' => array('cellspacing' => '1', 'cellpadding' => '3', 'border' => '0'),
    //'afterAjaxUpdate' => "function(){jQuery('#".CHtml::activeId($model, 'create_time')."').datepicker({'dateFormat':'yy-mm-dd'})}",
//    'afterAjaxUpdate' => 'reinstallDatePicker',
    'pager' => array(
        'class' => 'vH_CLinkPager',
    ),
    'filter' => $model,
    'columns' => array(
        array(
            'header' => 'STT',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'htmlOptions' => array('style' => 'text-align:center; width:30px; font-weight:bold'),
        ),
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'ids',
            'selectableRows' => 2,
            'htmlOptions' => array('style' => 'text-align:center; width:30px'),
        //'name'=>'admincp-node-grid-ids',
        ),
        array(
            'name' => 'avatar',
            'type' => 'html',
            'value' => '$data->user_type == 0 ? CHtml::image($data->getImage(72,72)->src,NULL,array("width"=>72,"height"=>72)): CHtml::image($data->getImage(72,72)->src,NULL) ',
            'filter' => '',
            'htmlOptions' => array('style' => 'text-align:center; width:80px;'),
        ),
        array(
            'name' => 'user_id',
            'htmlOptions' => array('style' => 'width:60px; font-weight:bold; text-align:center'),
        ),
        array(
            'name' => 'firstname',
            'type' => 'html',
            'value' => 'CHtml::link("$data->firstname",array("user/update/","id"=>$data->user_id))',
            'headerHtmlOptions' => array('align' => 'left', 'style' => 'width:80px;'),
        ),
        array(
            'name' => 'lastname',
            'type' => 'html',
            'value' => 'CHtml::link("$data->lastname",array("user/update/","id"=>$data->user_id))',
            'headerHtmlOptions' => array('align' => 'left', 'style' => 'width:80px;'),
        ),
        array(
            'name' => 'email',
            'type' => 'html',
            'headerHtmlOptions' => array('align' => 'left'),
        ),
        array(
            'name' => 'about',
            'type' => 'html',
            'value' => 'SiteHelper::cut_string($data->about, 200)',
            'headerHtmlOptions' => array('align' => 'left'),
        ),
      
        array(
            'name' => 'created_at',
            'type' => 'raw',
            'filter' => $dateisOn,
            'type' => 'html',
            'htmlOptions' => array('style' => 'width:230px;text-align:center'),
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'htmlOptions' => array('style' => 'width:80px; text-align:center'),
            'value' => 'AdmincpHelper::getStatusLabel( $data->status, $data->user_id )',
            'filter' => AdmincpHelper::getStatusOptions(),
        ),
        array(
            'class' => 'vH_CButtonColumn',
            'htmlOptions' => array('style' => 'width:80px; text-align:center'),
        ),
        
    ),
));
?>