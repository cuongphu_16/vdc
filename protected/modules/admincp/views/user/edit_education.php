<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'edit_education',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'form_2col',
    ),
        ));
?>
<div id="form-add-certificate" class="form-add-certificate">
    <div class="row">
        <div class="form-group">
            <label><?= Yii::t('main', 'School') ?> <span class="require">*</span></label>            
            <?php echo $form->textField($model, 'program', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'program'); ?>

        </div>
        <div class="form-group">
            <label><?= Yii::t('main', 'Major') ?> <span class="require">*</span></label>            
            <?php echo $form->textField($model, 'major', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'major'); ?>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label><?= Yii::t('main', 'From (month/year)') ?> <span class="require">*</span></label>            <?php echo $form->textField($model, 'from', array('class' => 'form-control Education_from_', 'readonly' => "true")); ?>
            <?php echo $form->error($model, 'from'); ?>
        </div>
        <div class="form-group">
            <label><?= Yii::t('main', 'To (month/year)') ?> <span class="require">*</span></label>
            <?php echo $form->textField($model, 'to', array('class' => 'form-control Education_to_', 'readonly' => "true")); ?>
            <?php echo $form->error($model, 'to'); ?>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label><?= Yii::t('main', 'Level') ?> <span class="require">*</span></label>
            <?php echo $form->dropDownList($model, 'degree_level', SiteHelper::getCatTypeOptions_(Category::TYPE_DEGREE_LEVEL, 1), array('class' => 'form-control vSelect', 'empty' => Yii::t('main', 'Choose degree level'))); ?>
            <?php echo $form->error($model, 'degree_level'); ?>
        </div>
        <div class="form-group">
            <div><label>&nbsp;</label></div>
            <input type="button" class="btn btn-danger" id="add-certificate" value="<?= Yii::t('main', 'Update') ?>" onclick="saveEducation()">
        </div>
        <script>
             var myDate = new Date(); // your date
            $(".Education_from_").datepicker({
                yearRange: '1940:' + (myDate.getFullYear() + 10),
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'mm/yy',
                onClose: function(dateText, inst) {
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).datepicker('setDate', new Date(year, month, 1));
                }
            });
            $(".Education_to_").datepicker({
                yearRange: '1940:' + (myDate.getFullYear() + 10),
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'mm/yy',
                onClose: function(dateText, inst) {
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).datepicker('setDate', new Date(year, month, 1));
                }
            });
            function saveEducation() {
                var data = $("#edit_education").serialize();
                $.ajax({
                    url: '<?= $this->createUrl('User/saveEditEducation', array('education_id' => $model->id)) ?>',
                    type: 'post',
                    data: data,
                    success: function(data)
                    {
                        if (data == 'success') {
                            $.fancybox.close();
                            $.ajax({
                                url: '<?= $this->createUrl('user/loadTrEducation') ?>',
                                type: 'post',
                                data: {'id': <?= $model->id ?>,'user_id':'<?= $model->user_id ?>'},
                                success: function(data)
                                {
                                    $("#education" + '<?= $model->id ?>').html(data);
                                    showNotify("<?= Yii::t('main','Update successfully') ?>");
                                }
                            });
                        }
                        else {
                            showNotify('<?= Yii::t('main', 'Please enter all fields have a star') ?>');
                        }
                    }
                });
            }
            ;
        </script>
    </div>
</div>

<?php $this->endWidget(); ?>
<style>
    .ui-datepicker-calendar {
        display: none;
    }
    .fancybox-close{top: 0 !important; right: 0 !important}
</style>