<?php if (isset($list_education) && !empty($list_education)): ?>
    <div class="form-group">
        <table id="table_education">
            <tr class="first">
                <th style="width:15%"><?= Yii::t('main', 'Degree Level') ?></th>
                <th style="width:35%"><?= Yii::t('main', 'At the school') ?></th>
                <th style="width:25%"><?= Yii::t('main', 'Major') ?></th>
                <th style="width:25%"><?= Yii::t('main', 'Time') ?></th>
            </tr>
            <?php
            $i = 0;
            $arr_education = array();
            foreach ($list_education as $education): $i++;
                array_push($arr_education, $education->id);
                ?>
                <tr class="hover <?= $i % 2 == 0 ? "chan" : 'le' ?>" id="education<?= $education->id ?>">
                    <td>
                        <h3 class="title_mobile"><?= Yii::t('main', 'Degree Level') ?></h3>
                        <?= SiteHelper::getCatLabel($education->degree_level) ?>
                    </td>
                    <td>
                        <h3 class="title_mobile"><?= Yii::t('main', 'At the school') ?></h3>
                        <?= $education->program ?>
                    </td>
                    <td>
                        <h3 class="title_mobile"><?= Yii::t('main', 'Major') ?></h3>
                        <?= $education->major ?>
                    </td>
                    <td class="td_status" style="  padding-right: 10px !important;">
                        <h3 class="title_mobile"><?= Yii::t('main', 'Time') ?></h3>
                        <?php
                        if (!empty($education->from) && !empty($education->to))
                            echo $education->from . " - " . $education->to;
                        elseif (!empty($education->from) && empty($education->to))
                            echo $education->from;
                        elseif (empty($education->from) && !empty($education->to))
                            echo $education->to;
                        else
                            echo '';
                        ?>
                        <a href="javascript:;" class="delete_function" title="<?= Yii::t('main', 'delete') ?>" onclick="deleteEducation(<?= $education->id ?>)"></a>
                        <a href="<?= Yii::app()->createUrl('User/editEducation', array('education_id' => $education->id)) ?>"  class="fancybox fancybox.ajax edit_function" title="<?= Yii::t('main', 'Edit Resumes') ?>"></a>                       
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>                            
    </div>
<?php endif; ?>