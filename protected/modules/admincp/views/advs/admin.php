<?php
// this is the date picker
$dateisOn = $this->widget('vH_CJuiDatePicker', array(
            // 'model'=>$model,
            'name' => 'Advs[updated_at]',
            'language' => 'en',
            'value' => $model->updated_at,
            // additional javascript options for the date picker plugin
            'options' => array(
                'showAnim' => 'fold',
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => 'true',
                'changeYear' => 'true',
                'constrainInput' => 'false',
                'showButtonPanel' => true,
                'beforeShow' => 'js:function(input){
                    setTimeout(function() {
                        var buttonPane = $( input )
                            .datepicker( "widget" )
                            .find( ".ui-datepicker-buttonpane" );

                        $( "<button>", {
                            text: "Clear",
                            click: function() {
                                $.datepicker._clearDate( input );
                            }
                        }).appendTo( buttonPane ).addClass("ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all");
                    }, 1 );
                }',
                'onChangeMonthYear' => 'js:function( year, month, instance ) {
        setTimeout(function() {
            var buttonPane = $( instance )
                .datepicker( "widget" )
                .find( ".ui-datepicker-buttonpane" );

            $( "<button>", {
                text: "Clear",
                click: function() {
                    $.datepicker._clearDate( instance.input );
                }
            }).appendTo( buttonPane ).addClass("ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all");
        }, 1 );
    }'
            ),
            'htmlOptions' => array(
                'autocomplete' => 'off',
                'style' => 'width: 110px',
                'readonly' => true
            ),
// DONT FORGET TO ADD TRUE this will create the datepicker return as string
                ), true);

Yii::app()->clientScript->registerScript('re-install-date-picker', "
function reinstallDatePicker(id, data) {
    $('input[name=\"Advs[updated_at]\"]').datepicker({'showAnim':'fold','dateFormat':'yy-mm-dd','changeMonth':'true','changeYear':'true','constrainInput':'false',showButtonPanel: true, 'beforeShow': function( input ) {
        setTimeout(function() {
            var buttonPane = $( input )
                .datepicker( \"widget\" )
                .find( \".ui-datepicker-buttonpane\" );

            $( \"<button>\", {
                text: \"Clear\",
                click: function() {
                    $.datepicker._clearDate( input );
                }
            }).appendTo( buttonPane ).addClass(\"ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all\");
        }, 1 );
    },'onChangeMonthYear' : function( year, month, instance ) {
        setTimeout(function() {
            var buttonPane = $( instance )
                .datepicker( \"widget\" )
                .find( \".ui-datepicker-buttonpane\" );

            $( \"<button>\", {
                text: \"Clear\",
                click: function() {
                    $.datepicker._clearDate( instance.input );
                }
            }).appendTo( buttonPane ).addClass(\"ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all\");
        }, 1 );
    }});   
}
");
?>

<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>
<?php if ($msg = Yii::app()->admin->showPutMsg()) echo $msg . '<br>'; ?>

<?php
$this->widget('vH_CGridView', array(
    'id' => 'advs-grid',
    'dataProvider' => $model->search('deleted = 0'),
    'cssFile' => $this->module->getAssetsUrl() . '/css/CGridView.css',
    'itemsCssClass' => 'adminlist',
    'itemAttributes' => array('cellspacing' => '1', 'cellpadding' => '3', 'border' => '0'),
//    'afterAjaxUpdate' => 'reinstallDatePicker',
    'pager' => array(
        'class' => 'vH_CLinkPager',
    ),
    'filter' => $model,
    'columns' => array(
        array(
            'header' => 'STT',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'htmlOptions' => array('style' => 'text-align:center; width:30px; font-weight:bold'),
        ),
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'ids',
            'selectableRows' => 2,
            'htmlOptions' => array('style' => 'text-align:center; width:30px'),
        ),
        array(
            'name' => 'title',
            'value' => 'CHtml::link("$data->title",array("advs/update/","id"=>$data->advs_id))',
            'type' => 'html',
            'headerHtmlOptions' => array('align' => 'center'),
            'htmlOptions' => array('style' => 'width:250px;'),
        ),        
        array(
            'name' => 'image',
            'type' => 'html',
            'filter'=>'',
            'value' => '(!empty($data->image))?CHtml::image($data->getImage(150,0)->src,NULL,array("width"=>150)):""',
            'htmlOptions' => array('style' => 'text-align:center; width:80px;'),
        ),       
        array(
            'name' => 'position',
            'type' => 'raw',
            'value' => 'AdmincpHelper::getPositionLabel($data->position)',
            'filter' => AdmincpHelper::getPositionOptions(),
            'htmlOptions' => array('style' => 'width:80px; text-align:center'),
        ),
         array(
            'name' => 'updated_at',
            'type' => 'raw',
             'filter' => $dateisOn,
            'htmlOptions' => array('style' => 'width:110px; text-align:center'),
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => 'AdmincpHelper::getStatusLabel( $data->status, $data->advs_id )',
            'filter' => AdmincpHelper::getStatusOptions(),
            'htmlOptions' => array('style' => 'width:80px; text-align:center'),
        ),
        array(
            'class' => 'EButtonColumnWithClearFilters',
            'onClick_BeforeClear' => 'return true;',
            'url' => 'Yii::app()->controller->createUrl(Yii::app()->controller->action->ID,array("clearFilters"=>1))',
            'template' => '{update} {delete}',
            'htmlOptions' => array('style' => 'width:40px; text-align:center'),
        ),
    ),
));
?>
