<div class="form">

    <?php
    $form = $this->beginWidget('vH_CActiveForm', array(
        'id' => 'advs-form',
    ));
    ?>

    <?php if ($msg = Yii::app()->admin->showPutMsg()) echo $msg . '<br>'; ?>
    <?php if ($error = $form->errorSummary($model)) echo $error . '<br>'; ?>

    <table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
        <tr>
            <th colspan="2" align="left">Thông tin</th>
        </tr>
        <tr>
            <td colspan="2"><p class="note">Điền đầy đủ các mục có dấu <span class="required">*</span>.</p></td>
        </tr>
        <tr>
            <td valign="top" width="20%"><strong><?php echo $form->labelEx($model, 'title'); ?> </strong></td>		
            <td>
                <?php echo $form->textField($model, 'title', array('size' => 60)); ?>
                <?php echo $form->error($model, 'title'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'url'); ?></strong></td>		
            <td colspan="3">
                <?php echo $form->textField($model, 'url', array('size' => 60, 'maxlength' => 255)); ?>
                <label>http://www.{link} hoặc www.{link}</label>
                <?php echo $form->error($model, 'url'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'image'); ?></strong></td>		
            <td>
                <?php echo $form->fileField($model, 'image'); ?>
                <?php echo $form->error($model, 'image'); ?>
                <?php if (!empty($model->image)) : ?>

                    <?php echo $form->hiddenField($model, 'image_hidden', array('value' => $model->image)); ?>
                    <div><?php echo CHtml::image($model->getImage(300, 0)->src); ?></div>
                <?php endif; ?>
            </td>
        <script type="text/javascript">
            $("#ytAdvs_image").val($("#Advs_image_hidden").val());
        </script>
        </tr>   
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'position'); ?></strong></td>
            <td>
                <?php echo $form->dropDownList($model, 'position', AdmincpHelper::getPositionOptions(), array('empty' => 'Chọn vị trí')); ?>
                <div><?php echo $form->error($model, 'position'); ?></div>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong>Kích thước</strong></td>
            <td>
                <?php echo $form->textField($model, 'width', array('style' => 'width: 80px')); ?> x <?php echo $form->textField($model, 'height', array('style' => 'width: 80px')); ?> C.Rông (px) x C.Cao (px)
                <?php echo $form->error($model, 'width'); ?>
                <?php echo $form->error($model, 'height'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'description'); ?></strong></td>
            <td>
                <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50)); ?>
                <div><?php echo $form->error($model, 'description'); ?></div>
            </td>
        </tr>
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'sort_order'); ?></strong></td>		
            <td colspan="3">
                <?php echo $form->textField($model, 'sort_order', array('size' => 60, 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'sort_order'); ?>
            </td>
        </tr>     
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'status'); ?></strong></td>
            <td>
                <?php echo $form->dropDownList($model, 'status', AdmincpHelper::getStatusOptions()); ?>
                <?php echo $form->error($model, 'status'); ?>
            </td>
        </tr>
    </table>
    <?php $this->endWidget(); ?>   
</div>
<script>
    $('#Advs_position').change(function () {
        if ($(this).val() == '<?= Advs::POSITION_BANNER ?>') {
            $('#Advs_width').val('1349');
            $('#Advs_height').val('680');
        }
    });
</script>