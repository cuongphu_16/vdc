<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('admincp-node-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php if ($msg = Yii::app()->admin->showPutMsg()) echo $msg . '<br>'; ?>

<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>
<div class="search-form" style="display:none"></div>

<?php
$grid = $this->widget('vH_CGridView', array(
    'id' => 'admincp-node-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'adminlist',
    'itemAttributes' => array('cellspacing' => '1', 'cellpadding' => '3', 'border' => '0'),
    'filter' => $model,
    'columns' => array(
        array(
            'header' => 'STT',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'htmlOptions' => array('style' => 'text-align:center; width:30px; font-weight:bold'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'ids',
            'selectableRows' => 2,
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'name' => 'node_id',
            'htmlOptions' => array('style' => 'text-align:center; width:50px; font-weight:bold'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'name' => 'p_id',
            'htmlOptions' => array('style' => 'width:150px;'),
            'headerHtmlOptions' => array('style' => 'text-align:left;'),
            'filter' => AdmincpHelper::getNodeOptions(),
            'value' => '$data->parent->node_name'
        ),
        array(
            'name' => 'node_name',
            'headerHtmlOptions' => array('style' => 'text-align:left;'),
        ),
        array(
            'name' => 'controller',
            'headerHtmlOptions' => array('style' => 'text-align:left;'),
        ),
        array(
            'name' => 'node_code',
            'headerHtmlOptions' => array('style' => 'text-align:left;'),
        ),
        array(
            'name' => 'sort',
            'class' => 'vH_SortColumn',
            'htmlOptions' => array('style' => 'text-align:center; width:50px;'),
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => 'AdmincpHelper::getStatusLabel( $data->status, $data->node_id)',
            'filter' => AdmincpHelper::getStatusOptions(),
            'htmlOptions' => array('style' => 'width:80px; text-align:center'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'class' => 'vH_CButtonColumn',
        ),
    ),
        ));
?>