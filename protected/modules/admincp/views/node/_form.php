<div class="form">

<?php 
Yii::app()->clientScript->registerCoreScript('jquery');
$form=$this->beginWidget('vH_CActiveForm', array(
	'id'=>'admincp-node-form',
	//'enableAjaxValidation'=>true,
)); /* @var $form vH_CActiveForm */?>

	<?php echo Yii::app()->admin->showPutMsg(); ?>
	<?php echo $form->errorSummary($model); ?>
	<br />

	<table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
		<tr>
			<th colspan="2" align="left">Thông tin menu admin</th>
		</tr>
		<tr>
			<td colspan="2"><p class="note">Điền đầy đủ các mục có dấu <span class="required">*</span>.</p></td>
		</tr>
		<tr>
			<td valign="top" width="200"><strong><?php echo $form->labelEx($model,'node_name'); ?></strong></td>
			<td>
				<?php echo $form->textField($model,'node_name',array('size'=>30,'maxlength'=>45)); ?>
				<?php echo $form->error($model,'node_name'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'p_id'); ?></strong></td>
			<td>
				<?php echo $form->dropDownList($model,'p_id', array('0'=>'Root')+AdmincpHelper::getNodeOptions()  ); ?>
				<?php echo $form->error($model,'p_id'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'node_code'); ?></strong></td>
			<td>
				<?php echo $form->textField($model,'node_code',array('size'=>30,'maxlength'=>100)); ?>
				<?php echo $form->error($model,'node_code'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'controller'); ?></strong></td>
			<td>
				<?php $exceptionControllers = $model->getRemainControllers($model->controller); 
				$array = AdmincpHelper::getControllerOptions(true, $exceptionControllers);
				?>
				<?php echo $form->dropDownList($model,'controller', array_merge(array(''=>'- Không chọn -'), $array ) ); ?>
				<?php echo $form->error($model,'controller'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'node_url'); ?></strong></td>
			<td>
				<?php echo $form->textField($model,'node_url',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'node_url'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'sort'); ?></strong></td>
			<td>
				<?php echo $form->textField($model,'sort',array('size'=>10,'maxlength'=>10)); ?>
				<?php echo $form->error($model,'sort'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'status'); ?></strong></td>
			<td>
				<?php echo $form->dropDownList($model,'status', AdmincpHelper::getStatusOptions()  ); ?>
				<?php echo $form->error($model,'status'); ?>
			</td>
		</tr>
		<!--<tr>
			<td valign="top"><strong></strong></td>
			<td>
				<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
			</td>
		</tr>-->
	</table>

<?php $this->endWidget(); ?>
<script type="text/javascript">
$('#AdmincpNode_controller').change(function(){
	var name= $('#AdmincpNode_controller').val();
	name= name.substring(0,name.length -10);
	$('#AdmincpNode_node_code').val(name);
	$('#AdmincpNode_node_url').val('admincp/'+ name+ '/admin');
	if(!$('#AdmincpNode_node_name').val())
		$('#AdmincpNode_node_name').val(name);
});
//camelize date exchange
function camelize(str){
	return str.replace(/_(w)/g, function(all, letter){
        return letter.toUpperCase();
    });
}
function unCamelize(str){
	var result= str.replace(/([A-Z])/g,"_$1").toLowerCase();
	return result.substring(1);
}
</script>
</div><!-- form -->