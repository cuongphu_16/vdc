<div class="form">

    <?php
    $form = $this->beginWidget('vH_CActiveForm', array(
        'id' => 'partner-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo Yii::app()->admin->showPutMsg(); ?>
    <?php echo $form->errorSummary($model); ?>
    <br />

    <table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
        <tr>
            <th colspan="2" align="left">Thông tin tài khoản</th>
        </tr>
        <tr>
            <td colspan="2"><p class="note">Điền đầy đủ các mục có dấu <span class="required">*</span>.</p></td>
        </tr>

        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'title'); ?></strong></td>
            <td>
                <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 100)); ?>
                <?php echo $form->error($model, 'title'); ?>
            </td>
        </tr>

        <tr>
            <td><strong><?php echo $form->labelEx($model, 'image'); ?></strong></td>		
            <td>
                <?php echo $form->fileField($model, 'image'); ?>
                <?php echo $form->error($model, 'image'); ?>
                <div style="clear: both;margin-top: 10px"></div>
                <?php if (!empty($model->image)) : ?>

                    <?php echo $form->hiddenField($model, 'image_hidden', array('value' => $model->image)); ?>
                    <div><?php echo CHtml::image($model->getImage(300, 0)->src); ?></div>
                    <a style="line-height: 30px;cursor: pointer" onclick="deleteImage(<?= $model->partner_id ?>)">Xóa ảnh</a>
                <?php endif; ?>
                <?php if (isset(Yii::app()->session['partner_upload_image'])): ?>
                    <?php
                    $img_data = Controller::getImageUpload((object) array(
                                        'folder' => 'partner',
                                        'id' => Yii::app()->session['partner_upload_folder'],
                                        'width' => 300,
                                        'height' => 0,
                                        'filename' => Yii::app()->session['partner_upload_image'],
                                        'not_int' => 'true'
                    ));
                    echo CHtml::image($img_data->src) . '<br>';
                    ?>
                <?php endif; ?>
            </td>
        <script type="text/javascript">
            $("#ytPartner_image").val($("#Partner_image_hidden").val());
        </script>
        </tr>

        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'sort_order'); ?></strong></td>		
            <td colspan="3">
                <?php echo $form->textField($model, 'sort_order', array('size' => 60, 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'sort_order'); ?>
            </td>
        </tr>        
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'status'); ?></strong></td>
            <td>
                <?php echo $form->dropDownList($model, 'status', AdmincpHelper::getStatusOptions(), array('style' => 'width: 150px')); ?>
                <?php echo $form->error($model, 'status'); ?>
            </td>
        </tr>       
    </table>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    function deleteImage(id) {
        $.ajax({
            url: "<?= $this->createUrl("Partner/deleteImage") ?>",
            type: "partner",
            dataType: 'html',
            cache: false,
            data: {id: id},
            success: function (data) {
                location.reload();
            }
        });
    }
</script>