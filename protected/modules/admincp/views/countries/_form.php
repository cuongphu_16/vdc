<div class="form">

    <?php
    $form = $this->beginWidget('vH_CActiveForm', array(
        'id' => 'post-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo Yii::app()->admin->showPutMsg(); ?>
    <?php echo $form->errorSummary($model); ?>
    <br />

    <table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
        <tr>
            <th colspan="2" align="left">Thông tin tài khoản</th>
        </tr>
        <tr>
            <td colspan="2"><p class="note">Điền đầy đủ các mục có dấu <span class="required">*</span>.</p></td>
        </tr>
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'title'); ?></strong></td>
            <td>
                <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 100)); ?>
                <?php echo $form->error($model, 'title'); ?>
            </td>
        </tr>     
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'comment'); ?></strong></td>		
            <td>
                <?php echo $form->textArea($model, 'comment', array('rows' => 6, 'cols' => 50, 'class' => 'editors_desc')); ?>
                <?php echo $form->error($model, 'comment'); ?>
                <?php
                $editor = vH_Utils::showEditor();
                echo $editor->replaceAll('editors_desc');
                ?>
            </td>
        </tr>
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'LatLng'); ?></strong></td>
            <td>
                <?php echo $form->textField($model, 'LatLng', array('size' => 60, 'maxlength' => 100)); ?>
                <?php echo $form->error($model, 'LatLng'); ?>
            </td>
        </tr>     
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'sort_order'); ?></strong></td>		
            <td colspan="3">
                <?php echo $form->textField($model, 'sort_order', array('size' => 60, 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'sort_order'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'status'); ?></strong></td>
            <td>
                <?php echo $form->dropDownList($model, 'status', AdmincpHelper::getStatusOptions(), array('style' => 'width: 150px')); ?>
                <?php echo $form->error($model, 'status'); ?>
            </td>
        </tr>
    </table>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    function deleteImage(id) {
        $.ajax({
            url: "<?= $this->createUrl("Post/deleteImage") ?>",
            type: "post",
            dataType: 'html',
            cache: false,
            data: {id: id},
            success: function(data) {
                location.reload();
            }
        });
    }
</script>