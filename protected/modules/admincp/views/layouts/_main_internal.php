<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="vi" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admincp.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<title><?php echo CHtml::encode($this->rendererTile() ); ?></title>
</head>

<body>
<div id="header">
    <div id="logo"><a href="<?php echo Yii::app()->createAbsoluteUrl('admincp/main/index'); ?>"><?php echo CHtml::encode(Yii::app()->name); ?></a></div>
</div><!-- header -->
<div class="admin_bg" > 
	<div class="container" id="page">
		<div id="admihtml_mainmenu">
			<?php 
				$menuArray= Yii::app()->admin->getState('menuArray')? Yii::app()->admin->getState('menuArray'): AdmincpHelper::getMenuArray() ?>
			<?php $this->widget('application.extensions.menu.SMenu', $menuArray ); ?>
		</div><!-- mainmenu -->

		<?php $this->widget('zii.widgets.CBreadcrumbs', array( 'links'=>$this->getBreadcrumbs(), )); ?><!-- breadcrumbs -->

		<?php echo $content; ?>

	</div><!-- page -->
</div>
<div id="footer">
	Copyright &copy; <?php echo date('Y'); ?> by seiferli@gmail.com All Rights Reserved.<br/>
</div><!-- footer -->
</body>
</html>