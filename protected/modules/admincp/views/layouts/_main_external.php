<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php //echo CHtml::encode($this->rendererTile() ); ?></title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex, nofollow" />
	<meta name="googlebot" content="noindex, nofollow" />
	
	<?php echo $this->module->registerJs('jquery.min.js');?>
	<?php echo $this->module->registerJs('jquery.min.js');?>
	<?php echo $this->module->registerCss('screen.css');?>
	<?php echo $this->module->registerJs('screen.js');?>
	
	<script language="javascript" type="text/javascript">
		$(window).bind('load',function(){
			$('input[name*=username]').trigger('focus');
		});		
	</script>
</head>
<body style="padding:0 !important">
	<div class="hdrBar"></div>
	<div class="hdr">
		<div class="hdr_top">
			<a href="" title="Trang chủ" class="fl"><img src="<?php echo $this->module->registerImage('administrator-system.png');?>" width="223" height="12" /></a>
		</div>
		
	</div>
	<?php echo $content; ?>
	
	<div class="footer"></div>
</body>
</html>
