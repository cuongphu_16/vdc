<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi">

    <head>
        <meta name="language" content="vi" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="googlebot" content="noindex, nofollow" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <?php echo $this->module->registerJs('jquery.min.js'); ?>
        <title><?php echo CHtml::encode(strip_tags($this->rendererTile())); ?></title>
        <?php
        echo $this->module->registerCss('colorbox/colorbox.css');
        echo $this->module->registerCss('bootstrap-timepicker.css');
        echo $this->module->registerJs('colorbox/jquery.colorbox_cus.js');
        echo $this->module->registerJs('jquery.scrollTo-min.js');
        echo $this->module->registerJs('fancybox/jquery.fancybox.js?v=2.1.5');
        echo $this->module->registerCss('fancybox/jquery.fancybox.css?v=2.1.5');
        echo $this->module->registerJs('vhplugin/vhplugin.js');
        echo $this->module->registerJs('swfobject.js');
        echo $this->module->registerJs('utils.js');
        echo $this->module->registerJs('screen.js');
        echo $this->module->registerJs('jCore.js');
        echo $this->module->registerJs('jquery.cookie.js');
        echo $this->module->registerJs('jquery-ui-1.9.2/jquery-ui.min.js');
        $href_ = $this->module->getAssetsUrl() . '/js/jquery-ui-1.9.2/themes/base/min/jquery-ui.min.css';
        echo '<link rel="stylesheet" type="text/css" href="' . $href_ . '" media="all" />';
        echo $this->module->registerJs('bootstrap-timepicker.js');
        echo $this->module->registerCss('screen.css');
        $cs = Yii::app()->getClientScript();
        $script = <<<VH
                jcore.setSelectStyle();
                $(document).ajaxComplete(function(){
            jcore.setSelectStyle();
        });
VH;
        $cs->registerScript('selectElm', "\n\n/* layouts/index */\n" . vH_Utils::trimTotal($script));
        ?>
        <script language="javascript" type="text/javascript">
            var action = '<?= Yii::app()->controller->action->id ?>';
            var hotkey;
            $(function () {
                hotkey = "Phím nóng: \n";
                hotkey += "Delete: Xóa bản ghi\n";
                hotkey += "Ctrl+S: Lưu dữ liệu\n";
                hotkey += "S: Hiển thị\n";
                hotkey += "H: Không hiển thị\n";
                hotkey += "N: Thêm mới\n";
                hotkey += "K: Xem danh sách Hotkey\n";
                hotkey += "Ctrl+Shift+L: Thoát\n";
                hotkey += "Ctrl+A: Chọn tất cả hoặc bỏ chọn tất cả";

                $(document).on('keydown', function (e) {

                    var nodeName = e.target.nodeName;
                    //console.log(e.keyCode);

                    switch (e.keyCode) {
                        case 46:
                        {
                            //delete record
                            if (!e.ctrlKey && !e.shiftKey) {
                                if (e.target.nodeName == 'BODY' || (nodeName == "INPUT" && $(e.target).attr('type') == "checkbox")) {
                                    $('.ToolBarBtn .delete').trigger('click');
                                    return false;
                                }
                            }
                            break;
                        }

                        case 83:
                        {
                            //save
                            if (e.ctrlKey) {
                                if ($('.ToolBarBtn .save').size() > 0) {
                                    $('.ToolBarBtn .save').trigger('click');
                                    return false;
                                }
                            } else {
                                if ($('.ToolBarBtn .publish').size() > 0) {
                                    if (nodeName == "BODY" || (nodeName == "INPUT" && $(e.target).attr('type') == "checkbox")) {
                                        $('.ToolBarBtn .publish').trigger('click');
                                        return false;
                                    }
                                }
                            }
                            break;
                        }

                        case 27:
                        {
                            $('.ToolBarBtn .cancel').trigger('click');
                            return false;
                            break;
                        }

                        case 65:
                        {
                            //check all checkbox in gridview
                            if (e.ctrlKey && action == 'admin') {
                                if ($('#ids_all').attr('checked') == "checked") {
                                    $('.adminlist input[name^="ids"]').attr('checked', false);
                                } else {
                                    $('.adminlist input[name^="ids"]').attr('checked', true);
                                }
                                return false;
                            }
                            break;
                        }

                        case 72:
                        {
                            if ($('.ToolBarBtn .unpublish').size() > 0) {
                                if (nodeName == "BODY" || (nodeName == "INPUT" && $(e.target).attr('type') == "checkbox")) {
                                    $('.ToolBarBtn .unpublish').trigger('click');
                                    return false;
                                }
                            }
                            break;
                        }

                        case 75:
                        {
                            if (nodeName != "INPUT" && nodeName != "TEXTAREA") {
                                alert(hotkey);
                                return false;
                            }

                            break;
                        }

                        case 76:
                        {
                            if (e.ctrlKey && e.shiftKey) {
                                if (confirm('Bạn có chắc chắn muốn thoát?')) {
                                    window.location.href = $('.hdr_top .logoutLnk').attr('href');
                                    return false;
                                }
                            }
                            break;
                        }

                        case 78:
                        {
                            //add new record
                            if ($('.ToolBarBtn .add').size() > 0) {
                                if (nodeName == "BODY" || (nodeName == "INPUT" && $(e.target).attr('type') == "checkbox")) {
                                    window.location.href = $('.ToolBarBtn .add').attr('href');
                                    return false;
                                }
                            }
                            break;
                        }
                    }
                });
            });
        </script>
    </head>
    <body>
        <div id="loading">
            <img src="<?php echo $this->module->registerImage('loadbar.gif'); ?>" />
            <p>Đang tải dữ liệu. Vui lòng chờ</p>
        </div>
        <div style="height:140px" class="boxhdrScroll">
            <div class="hdrScroll">
                <div class="hdrBar"></div>	

                <div class="hdr">
                    <div class="hdr_top">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/admincp" title="Trang chủ" class="fl"><img src="<?php echo $this->module->registerImage('administrator-system.png'); ?>" width="223" height="12" /></a>
                        
                        <div class="logoutLnk fr">
                            Chào <strong style="color:#f00; "><?php echo ucfirst(Yii::app()->admin->name); ?></strong> - <a href="<?php echo Yii::app()->request->baseUrl; ?>" target="_blank"><strong>Xem website</strong></a>
                            | <a href="<?php echo Yii::app()->request->baseUrl; ?>/admincp/main/logout" onclick="if (!confirm('Bạn có chắc chắn muốn thoát?'))
                                        return false;">Thoát</a>
                        </div>
                    </div>
                    <div class="navbar <?php if ($this->navbarDisabled) echo 'disabled'; ?>">
                        <?php //$menuArray = Yii::app()->admin->getState('menuArray')? Yii::app()->admin->getState('menuArray'): AdmincpHelper::getMenuArray();  ?>
                        <?php $menuArray = AdmincpHelper::getMenuArray(); ?>
                        <?php $this->widget('application.extensions.vH_Menu.NavMenu', $menuArray); ?>
                        <div class="clr"></div>
                    </div>
                    <div class="toolbar">
                        <div class="page_breadcrumb fl">
                            <?php
                            $this->widget('zii.widgets.CBreadcrumbs', array(
                                'links' => $this->breadcrumbs,
                                'encodeLabel' => false,
                                'tagName' => 'ul',
                                'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
                                'inactiveLinkTemplate' => '<li><span>{label}</span></li>',
                                'separator' => '<li class="separator">/</li>',
                                'homeLink' => CHtml::tag('li', array(), CHtml::link('Trang chủ', Yii::app()->request->baseUrl . '/admincp', array('style' => 'font-weight: bold; font-size: 16px'))),
                                'htmlOptions' => array(
                                    'class' => 'breadcrumb',
                                ),
                            ));
                            ?>
                        </div>

                        <div class="fr ToolBarBtn">
                            <?php
                            $this->widget('vH_CMenu', array(
                                'items' => $this->menu,
                                'htmlOptions' => array('class' => 'operations'),
                            ));
                            ?>
                        </div>

                        <div class="clr"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bdw">
            <?php echo $content; ?>
        </div>

        <div class="footer" style="color:#999; font-weight:normal">
            <div class="bdw">
                <div class="fl">&copy; 2015 by <a href="http://tincorner.vn" target="blank">Tincorner team</a></div>
                <div class="fr">
                    <?php echo Yii::t('admincp', 'Page load time'); ?>: 
                    <?php
                    echo round(Yii::getLogger()->getExecutionTime(), 2) . 's';
                    ?>
                </div>
            </div>
        </div>

        <div id="loading">
            <img src="<?php echo $this->module->registerImage('loading1.gif'); ?>" width="16" height="11" class="fl" />Đang tải...
            <div class="clr"></div>
        </div>
        <script language="javascript">
            function showNotify(content, callback) {
                if (content != '')
                {
                    clearTimeout(notify_time);
                    $('#notify_div').html(content);
                    $('#notify_div').fadeIn();
                    var notify_time = setTimeout(function () {
                        $('#notify_div').fadeOut();
                        if (callback)
                        {
                            callback.call();
                        }
                    }, 8000);
                } else {
                    $('#notify_div').hide();
                }

            }
        </script>
    </body>
</html>