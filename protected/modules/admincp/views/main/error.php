<div class="logout_page" align="center" style="padding:50px 0;">
	<h1 style="font-weight:normal; padding-bottom:10px">Đã xảy ra lỗi <?php echo $code; ?></h1>
		
	<div class="error">
		<h3 style="font-weight:normal;"><?php echo CHtml::encode($message); ?></h3>
		<h3 style="font-weight:normal;"><a href="<?php echo Yii::app()->createAbsoluteUrl('admincp/main/index'); ?>">[Quay lại]</a></h3>
	</div>
</div>
<div class="clr"></div>