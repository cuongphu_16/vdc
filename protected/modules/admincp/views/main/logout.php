<div class="logout_page" align="center" style="padding:50px 0;">

	<h1 style="font-weight:normal; padding-bottom:10px">Đăng xuất thành công</h1>
	<?php $returnUrl= Yii::app()->createAbsoluteUrl('admincp/main/login')?>
	<h3 style="font-weight:normal; color:#444">
		Hệ thống sẽ tự động rời khỏi trang này sau <strong style="color:#c00" id="timer">5</strong> giây, hoặc <a href='<?php echo $returnUrl ?>'>bấm vào đây</a> để rời ngay.
	</h3>
	<script type="text/javascript">
	//<![CDATA[ 
	var t=5;
	var oTimer= setInterval(function(){
	    if(t>0) {
	    document.getElementById('timer').innerHTML = t;
		    t-=1; 
	    } else {
		    window.clearInterval(oTimer);
		    location.href = '<?php echo $returnUrl ?>';
	    } }, 1000);
	//]]>
	</script>
</div>