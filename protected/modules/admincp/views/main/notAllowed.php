<div style="text-align:center;">
	<h1 style="font-weight:normal; padding-bottom:10px"><?php echo $message; ?></h1>
	<h3 style="font-weight:normal;">Bạn sẽ được chuyển về trang trước sau <strong style="color:#c00" id="timer">5</strong> giây hoặc <a href="<?php echo $returnUrl?>">bấm vào đây</a> để chuyển ngay về trang trước.</h3>
</div>
<script type="text/javascript">
//<![CDATA[ 
var t=5;
var oTimer= setInterval(function(){
	if(t>0) {
	document.getElementById('timer').innerHTML = t;
		t-=1; 
	} else {
		window.clearInterval(oTimer);
		location.href = '<?php echo $returnUrl?>';
	} }, 1000);
//]]>
</script>