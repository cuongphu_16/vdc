<?php
$form = $this->beginWidget('vH_CActiveForm', array(
    'id' => 'login-form',
        //'enableAjaxValidation'=>true,
        ));
?>
<div class="loginFrm" style="padding:100px 0">
    <div class="loginBox">
        <div><img src="<?php echo $this->module->registerImage('login_system.png'); ?>" width="168" height="19" /><br /><br /></div>			
        <!--<div style="{if !$msg}display:none{/if}" id="msg" class="msg">{$msg}</div>-->

        <?php if ($error = $form->errorSummary($model)) echo $error . '<br>'; ?>

        <table border="0" cellspacing="0" cellpadding="5" class="fl">
            <tr>
                <td valign="top"><?php echo $form->labelEx($model, 'username'); ?></td>
                <td>
                    <?php echo $form->textField($model, 'username', array('size' => '32', 'autocomplete' => 'off', 'tabindex' => 1, 'class' => 'iptext')); ?>
                </td>
            </tr>
            <tr>
                <td valign="top"><?php echo $form->labelEx($model, 'password'); ?></td>
                <td>
                    <?php echo $form->passwordField($model, 'password', array('size' => '32', 'autocomplete' => 'off', 'tabindex' => 2, 'class' => 'iptext')); ?>
                </td>
            </tr>
            <tr>
                <td valign="top"></td>
                <td>
                    <?php echo $form->checkBox($model, 'rememberMe'); ?>
                    <?php echo $form->labelEx($model, 'rememberMe'); ?>
                </td>
            </tr>
            <tr>
                <td valign="top"><?php echo $form->labelEx($model, 'verifyCode'); ?></td>
                <td>
                    <?php echo $form->textField($model, 'verifyCode', array('size' => '32', 'autocomplete' => 'off', 'tabindex' => 2, 'class' => 'iptext')); ?>
                    <div>
                        <?php
                        $this->widget('CCaptcha', array(
                            'showRefreshButton' => false,
                            'clickableImage' => true,
                            'imageOptions' => array(
                                'id' => 'captcha_img',
                            ),
                        ));
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <?php echo CHtml::button('Dang nhap', array('type' => 'image', 'src' => $this->module->registerImage('login_btn.png'), 'style' => 'width:162px; height:43px')); ?>
                </td>
            </tr>
        </table>
        <img src="<?php echo $this->module->registerImage('icon_lock_key.jpg'); ?>" alt="" width="137" height="137" class="fr" />
        <div class="clr"></div>
    </div>
</div>
<?php $this->endWidget(); ?>
<?php echo $this->module->registerJs('login.js'); ?>