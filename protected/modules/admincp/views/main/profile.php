<div class="form">
<?php $form=$this->beginWidget('vH_CActiveForm', array(
	'id'=>'admincp-admin-form',
	//'enableAjaxValidation'=>false,
)); /* @var $form vH_CActiveForm */?>

<?php echo Yii::app()->admin->showPutMsg(); ?>
<?php echo $form->errorSummary($model);
?>
<br />
	
	<table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
		<tr>
			<th></th>
			<th>&nbsp;</th>
		</tr>
		<tr>
			<td colspan="2"><p class="note">Điền đầy đủ các mục có dấu <span class="required">*</span>.</p></td>
		</tr>
		<tr>
			<td valign="top" width="20%"><strong><?php echo $form->labelEx($model,'username'); ?></strong></td>
			<td>
				<?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>64)); ?>
				<?php echo $form->error($model,'username'); ?>
			</td>
		</tr>
		<!--<tr>
			<td valign="top"><strong><?php //echo $form->labelEx($model,'nickname'); ?></strong></td>
			<td>
				<?php //echo $form->textField($model,'nickname',array('size'=>20,'maxlength'=>50)); ?>
				<?php //echo $form->error($model,'nickname'); ?>
			</td>
		</tr>-->
		<tr style="<?php if($_POST['chg_password'] != 1) echo 'display:none' ?>">
			<td valign="top"><strong><?php echo $form->labelEx($model,'password_cr'); ?></strong></td>
			<td>
				<input type="password" id="AdmincpAdmin_password_cr" class="<?php if($form->error($model,'password_cr')) echo 'error';?>" name="User[password_cr]" maxlength="64" size="32" autocomplete="off" value="" />
				<?php echo $form->error($model,'password_cr'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'password'); ?></strong></td>
			<td>
				<?php echo $form->passwordField($model,'password',array('size'=>32, 'maxlength'=>64, 'class'=>'fl','value'=>'','disabled'=>($_POST['chg_password'] != 1), 'autocomplete'=>'off')); ?>
				<input type="checkbox" id="chg_password" name="chg_password" <?php if($_POST['chg_password'] == 1): ?>checked="checked"<?php endif; ?> value="1" class="fl" /> <label style="line-height:16px" for="chg_password">Đổi mật khẩu</label>
				<div class="clr"></div>
				<?php echo $form->error($model,'password'); ?>
			</td>
		</tr>
		<tr style="<?php if($_POST['chg_password'] != 1) echo 'display:none' ?>">
			<td valign="top"><strong><?php echo $form->labelEx($model,'password_cf'); ?></strong></td>
			<td>
				<input class="<?php if($form->error($model,'password_cf')) echo 'error';?>" type="password" id="AdmincpAdmin_password_cf" name="User[password_cf]" maxlength="64" size="32" autocomplete="off">
				<?php echo $form->error($model,'password_cf'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'email'); ?></strong></td>
			<td>
				<?php echo $form->textField($model,'email',array('size'=>32,'maxlength'=>32)); ?>
				<?php echo $form->error($model,'email'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'firstname'); ?></strong></td>
			<td>
				<?php echo $form->textField($model,'firstname',array()); ?>
				<?php echo $form->error($model,'firstname'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'lastname'); ?></strong></td>
			<td>
				<?php echo $form->textField($model,'lastname',array()); ?>
				<?php echo $form->error($model,'lastname'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'countryId'); ?></strong></td>
			<td>
				<?php echo $form->dropDownList($model,'countryId', AdmincpHelper::getCountriesOptions(),array('onchange'=>CHtml::ajax(array('type'=>'GET', 'url'=>array('main/loadRegions/'),'update'=>'#AdmincpAdmin_regionId') ))); ?>
				<?php echo $form->error($model,'countryId'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'regionId'); ?></strong></td>
			<td>
				<?php echo $form->dropDownList($model,'regionId', AdmincpHelper::getRegionsOptions($model->countryId)); ?>
				<?php echo $form->error($model,'regionId'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'about'); ?></strong></td>
			<td>
				<?php echo $form->textArea($model,'about',array('rows'=>4,'cols'=>50,'value'=>$model->about)); ?>
				<?php echo $form->error($model,'about'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'status'); ?></strong></td>
			<td>
				<?php echo $form->dropDownList($model,'status', AdmincpHelper::getStatusOptions() ); ?>
				<?php echo $form->error($model,'status'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'avatar'); ?></strong></td>
			<td>				
				<?php echo $form->fileField($model,'avatar'); ?>
				<?php
					echo $form->error($model,'avatar'); 
					
					if(!empty($model->avatar)):
				?>				
				<input type="hidden" name="User[avatar_hidden]" value="<?php echo $model->avatar;?>" />
				<div><img src="<?php echo $this->module->registerUpload('user/'.$model->avatar.'?t='.time())?>" /></div>
				<?php endif;?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'fb_publish'); ?></strong></td>
			<td>
				<?php echo $form->checkBox($model,'fb_publish',array('value'=>1)); ?>
				<?php echo $form->error($model,'fb_publish'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'newsletter'); ?></strong></td>
			<td>
				<?php echo $form->checkBox($model,'newsletter',array('value'=>1)); ?>
				<?php echo $form->error($model,'newsletter'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'remark'); ?></strong></td>
			<td>
				<?php echo $form->textField($model,'remark',array('size'=>50)); ?>
				<?php echo $form->error($model,'remark'); ?>
			</td>
		</tr>
		
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'create_time'); ?></strong></td>
			<td>
				<?php echo $form->textField($model,'create_time', array('disabled'=>true,)); ?>
				<?php echo $form->error($model,'create_time'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'update_time'); ?></strong></td>
			<td>
				<?php echo $form->textField($model,'update_time', array('disabled'=>true,)); ?>
				<?php echo $form->error($model,'update_time'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'last_login'); ?></strong></td>
			<td>
				<?php echo $form->textField($model,'last_login', array('disabled'=>true,)); ?>
				<?php echo $form->error($model,'last_login'); ?>
			</td>
		</tr>
		<!--<tr>
			<td valign="top"></td>
			<td><?php //echo CHtml::submitButton('Cập nhật'); ?></td>
		</tr>-->
	</table>

<?php $this->endWidget(); ?>
<script type="text/javascript">
$('#chg_password').change(function()
{
	if(this.checked==true){
		$('#AdmincpAdmin_password_cr').parents('tr').show();
		$('#AdmincpAdmin_password_cf').parents('tr').show();
		$('#AdmincpAdmin_password').attr('disabled',false);
		$('#AdmincpAdmin_password').val('');
	} else {
		$('#AdmincpAdmin_password_cr').parents('tr').hide();
		$('#AdmincpAdmin_password_cf').parents('tr').hide();
		$('#AdmincpAdmin_password').val('******');
		$('#AdmincpAdmin_password').attr('disabled',true);
	}
	$(window).trigger('resize');
});
/*$('input[type=password]').blur(function(){
	if( $('#AdmincpAdmin_password').val() && $('#AdmincpAdmin_password_cf').val()
	  && $('#AdmincpAdmin_password').val()!=$('#AdmincpAdmin_password_cf').val() 
	){
		$('#AdmincpAdmin_password').addClass('error');
		$('#AdmincpAdmin_password_cf').addClass('error');
		$('input[type=submit]').each(function(){this.disabled= true;});
		alert('aaaa');
	} else {
		$('#AdmincpAdmin_password').removeClass('error');
		$('#AdmincpAdmin_password_cf').removeClass('error');
		$('input[type=submit]').each(function(){this.disabled= false;});
	}
});*/
</script>
</div><!-- form -->