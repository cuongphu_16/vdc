<?php
if (!empty($order)):
    echo $this->module->registerJs('jquery.min.js');
    $crypt = new Crypt(Yii::app()->params['ENCRYPTION_KEY']);
    ?>
    <form action="<?= $order->camera_link ?>" id="pass-login-form" name="pass-login-form" method="post" style="display: none">
        <input type="hidden" name="command" value="login">
        <div style="margin: 7px 0 0 0px;">
            <input id="username" name="username" value="<?= $order->camera_username ?>">
        </div>
        <div style="margin: 5px 0 0 0px;">
            <input id="password" name="password" type="password" value="<?= $crypt->decrypt($order->camera_password) ?>">
        </div>
        <div style="margin: 0px 0px 0 174px;">
            <button id="loginBT" type="submit">Login</button>
        </div>
        <script>
            $('#pass-login-form').submit();
        </script>
    </form>
    <?php


endif;