<div class="form">

    <?php
    $form = $this->beginWidget('vH_CActiveForm', array(
        'id' => 'ward-form',
    ));
    ?>

    <?php if ($msg = Yii::app()->admin->showPutMsg()) echo $msg . '<br>'; ?>
    <?php if ($error = $form->errorSummary($model)) echo $error . '<br>'; ?>

    <table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
        <tr>
            <th colspan="2" align="left">Thông tin</th>
        </tr>
        <tr>
            <td colspan="2"><p class="note">Điền đầy đủ các mục có dấu <span class="required">*</span>.</p></td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'country_id'); ?></strong></td>
            <td>
                <?php echo $form->dropDownList($model, 'country_id', SiteHelper::getCountriesOption(), array('onchange' => CHtml::ajax(array('type' => 'GET', 'url' => array('main/loadRegionsWard/'), 'update' => '#Ward_region_id')), 'style' => 'width:342px;', 'empty' => 'Chọn Quốc gia')); ?>
                <?php echo $form->error($model, 'region_id'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'region_id'); ?></strong></td>
            <td>
                <?php echo $form->dropDownList($model, 'region_id', SiteHelper::getRegionAndCountries(), array('onchange' => CHtml::ajax(array('type' => 'GET', 'url' => array('main/loadDistrict/'), 'update' => '#Ward_district_id')), 'style' => 'width:342px;', 'empty' => 'Chọn Tỉnh/Thành')); ?>
                <?php echo $form->error($model, 'region_id'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'district_id'); ?></strong></td>		
            <td>
                <?php echo $form->dropdownList($model, 'district_id', SiteHelper::getDistrictOptions_($model->region_id), array('style' => 'width:342px;')); ?>
                <?php echo $form->error($model, 'district_id'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'title'); ?></strong></td>		
            <td>
                <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'title'); ?>
            </td>
        </tr>  
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'status'); ?></strong></td>
            <td>
                <?php echo $form->dropDownList($model, 'status', AdmincpHelper::getStatusOptions()); ?>
                <?php echo $form->error($model, 'status'); ?>
            </td>
        </tr>

    </table>
    <!--<div class="row buttons">
    <?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save');  ?>
    </div>-->


    <?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">

    function loadRegions(countryId) {

        var c = 0;
        $(json_regions).each(function(i) {
            console.log(i);
        });
    }
    function loadDistrict(region_id) {
        var c = 0;
        $(json_regions).each(function(i) {
            console.log(i);
        });
    }
</script>