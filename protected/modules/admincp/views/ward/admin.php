<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ward-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button'));  ?>
<!--<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array('model'=>$model,));  ?>
</div>-->

<?php
$this->widget('vH_CGridView', array(
    'id' => 'ward-grid',
    'dataProvider' => $model->search(),
    'cssFile' => $this->module->getAssetsUrl() . '/css/CGridView.css',
    'itemsCssClass' => 'adminlist',
    'itemAttributes' => array('cellspacing' => '1', 'cellpadding' => '3', 'border' => '0'),
    //'afterAjaxUpdate' => "function(){jQuery('#".CHtml::activeId($model, 'create_time')."').datepicker({'dateFormat':'yy-mm-dd'})}",
    'pager' => array(
        'class' => 'vH_CLinkPager',
    ),
    'filter' => $model,
    'columns' => array(
        array(
            'header' => 'STT',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'htmlOptions' => array('style' => 'text-align:center; width:30px; font-weight:bold'),
        ),
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'ids',
            'selectableRows' => 2,
            'htmlOptions' => array('style' => 'text-align:center; width:30px'),
        ),
        array(
            'name' => 'district_id',
            'type' => 'raw',
            'value' => 'AdmincpHelper::getDistrictLabel($data->district_id)',
            'filter' => AdmincpHelper::getDistrictAndRegions(),
            'htmlOptions' => array('style' => 'width:200px; text-align:left'),
            'headerHtmlOptions' => array('align' => 'left'),
        ),
        array(
            'name' => 'title',
            'type' => 'html',
            'value' => 'CHtml::link("$data->title",array("ward/update/","id"=>$data->ward_id))',
            'headerHtmlOptions' => array('align' => 'left'),
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => 'AdmincpHelper::getStatusLabel( $data->status, $data->ward_id )',
            'filter' => AdmincpHelper::getStatusOptions(),
            'htmlOptions' => array('style' => 'width:80px; text-align:center'),
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update} {delete}',
            'htmlOptions' => array('style' => 'width:40px; text-align:center'),
        ),
    ),
));
?>
