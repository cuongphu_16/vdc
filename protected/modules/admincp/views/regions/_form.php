<div class="form">

    <?php
    $form = $this->beginWidget('vH_CActiveForm', array(
        'id' => 'regions-form',
    ));
    ?>

    <?php echo Yii::app()->admin->showPutMsg(); ?>
    <?php echo $form->errorSummary($model); ?>
    <br />

    <table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
        <tr>
            <th colspan="2" align="left">Thông tin Tỉnh / Thành phố</th>
        </tr>
        <tr>
            <td colspan="2"><p class="note">Điền đầy đủ các mục có dấu <span class="required">*</span>.</p></td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'country_id'); ?></strong></td>
            <td>
                <?php echo $form->dropdownList($model, 'country_id', SiteHelper::getCountriesOption() ,array('style'=>'width:341px')); ?>
                <?php echo $form->error($model, 'country_id'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'title'); ?></strong></td>
            <td>
                <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'title'); ?>
            </td>
        </tr>
        <script type="text/javascript" language="javascript">
            $(function() {
                $('#Regions_title').die('focusout keyup').live('focusout keyup', function() {
                    /*console.log(this.value);*/
                    $('#Regions_alias').val(Utils.alias(this.value, true));
                });
            });
        </script>	
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'alias'); ?></strong></td>		
            <td colspan="3">
                <?php echo $form->textField($model, 'alias', array('size' => 60, 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'alias'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'code'); ?></strong></td>
            <td>
                <?php echo $form->textField($model, 'code', array('size' => 60, 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'code'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'ADM1Code'); ?></strong></td>
            <td>	
                <?php echo $form->textField($model, 'ADM1Code', array('size' => 60, 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'ADM1Code'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'area'); ?></strong></td>
            <td>	
                <?php echo $form->dropDownList($model, 'area', SiteHelper::getAreaOptions(),array('style'=>'width:342px')); ?>
                <?php echo $form->error($model, 'area'); ?>
            </td>
        </tr>        
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'sort_order'); ?></strong></td>		
            <td colspan="3">
                <?php echo $form->textField($model, 'sort_order', array('size' => 60, 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'sort_order'); ?>
            </td>
        </tr>
    </table>

    <?php $this->endWidget(); ?>

</div>