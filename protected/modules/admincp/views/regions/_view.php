<?php
/* @var $this RegionsController */
/* @var $data Regions */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('regionId')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->regionId), array('view', 'id'=>$data->regionId)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('countryId')); ?>:</b>
	<?php echo CHtml::encode($data->countryId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('region')); ?>:</b>
	<?php echo CHtml::encode($data->region); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code')); ?>:</b>
	<?php echo CHtml::encode($data->code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ADM1Code')); ?>:</b>
	<?php echo CHtml::encode($data->ADM1Code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_edit')); ?>:</b>
	<?php echo CHtml::encode($data->user_edit); ?>
	<br />


</div>