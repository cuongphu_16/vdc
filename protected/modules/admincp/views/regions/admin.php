<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('regions-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php if ($msg = Yii::app()->admin->showPutMsg()) echo $msg . '<br>'; ?>

<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>


<?php
$this->widget('vH_CGridView', array(
    'id' => 'regions-grid',
    'dataProvider' => $model->search(),
    'cssFile' => $this->module->getAssetsUrl() . '/css/CGridView.css',
    'itemsCssClass' => 'adminlist',
    'itemAttributes' => array('cellspacing' => '1', 'cellpadding' => '3', 'border' => '0'),
    //'afterAjaxUpdate' => "function(){jQuery('#".CHtml::activeId($model, 'create_time')."').datepicker({'dateFormat':'yy-mm-dd'})}",
    'pager' => array(
        'cssFile' => $this->module->getAssetsUrl() . '/css/pages.css',
        'header' => false,
        'pageSize' => 5,
    ),
    'filter' => $model,
    'columns' => array(
        array(
            'header' => 'STT',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'htmlOptions' => array('style' => 'text-align:center; width:30px; font-weight:bold'),
        ),
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'ids',
            'selectableRows' => 2,
            'htmlOptions' => array('style' => 'text-align:center; width:30px'),
        //'name'=>'admincp-node-grid-ids',
        ),
        array(
            'name' => 'country_id',
            'value'=>'SiteHelper::getCountriesLabel($data->country_id)',
            'filter'=>  SiteHelper::getCountriesOption(),
            'htmlOptions' => array('style' => 'width:150px;'),
        ),
        array(
            'name' => 'region_id',
            'htmlOptions' => array('style' => 'width:50px; font-weight:bold; text-align:center'),
        ),
        array(
            'name' => 'title',
            'type' => 'html',
            'value' => 'CHtml::link("$data->title",array("regions/update/","id"=>$data->region_id))',
            'headerHtmlOptions' => array('align' => 'left'),
        ),
        array(
            'name' => 'code',
            'type' => 'html',
            'value' => 'CHtml::link("$data->code",array("regions/update/","id"=>$data->region_id))',
            'htmlOptions' => array('style' => 'text-align:center; width:80px;'),
        ),
        array(
            'name' => 'ADM1Code',
            'type' => 'html',
            'value' => 'CHtml::link("$data->ADM1Code",array("regions/update/","id"=>$data->region_id))',
            'htmlOptions' => array('style' => 'text-align:center; width:80px;'),
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'htmlOptions' => array('style' => 'width:80px; text-align:center'),
            'value' => 'AdmincpHelper::getStatusLabel( $data->status, $data->region_id )',
            'filter' => AdmincpHelper::getStatusOptions(),
        ),
        array(
            'name' => 'sort_order',
            'htmlOptions' => array('style' => 'text-align:center; width:50px;'),
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update} {delete}',
            'htmlOptions' => array('style' => 'width:40px; text-align:center'),
        ),
    ),
));
?>
