<div class="form">

    <?php
    $form = $this->beginWidget('vH_CActiveForm', array(
        'id' => 'changepass-form',
        //'enableAjaxValidation'=>true,
            ));
    ?>

    <?php if($error = $form->errorSummary($model)) echo $error.'<br>'; ?>
    <table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
        <tr>
            <th colspan="2" align="left">Thông tin tài khoản</th>
        </tr>
        <tbody id="thong_tin_tai_khoan">
            <tr>
                <td valign="top"><strong><?php echo $form->labelEx($model, 'password_old'); ?></strong></td>		
                <td>
                    <?php echo $form->passwordField($model, 'password_old', array('size' => 32, 'maxlength' => 100, 'autocomplete' => 'off', 'value' => '')); ?>
                    <?php echo $form->error($model, 'password_old'); ?>
                </td>
            </tr>
            <tr>
                <td valign="top"><strong><?php echo $form->labelEx($model, 'password'); ?></strong></td>		
                <td>
                    <?php echo $form->passwordField($model, 'password', array('size' => 32, 'maxlength' => 100, 'autocomplete' => 'off', 'value' => '')); ?>
                    <?php echo $form->error($model, 'password'); ?>
                </td>
            </tr>
            <tr>
                <td valign="top"><strong><?php echo $form->labelEx($model, 'password_cf'); ?></strong></td>		
                <td>
                    <?php echo $form->passwordField($model, 'password_cf', array('size' => 32, 'maxlength' => 100, 'autocomplete' => 'off', 'value' => '')); ?>
                    <?php echo $form->error($model, 'password_cf'); ?>
                </td>
            </tr>
        </tbody>
    </table>
<?php $this->endWidget(); ?>

</div>