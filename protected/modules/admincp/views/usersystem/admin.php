<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>

<?php
$this->widget('vH_CGridView', array(
    'id' => 'user-grid',
    'dataProvider' => $model->search('type = 1'),
    'cssFile' => $this->module->getAssetsUrl() . '/css/CGridView.css',
    'itemsCssClass' => 'adminlist',
    'itemAttributes' => array('cellspacing' => '1', 'cellpadding' => '3', 'border' => '0'),
    //'afterAjaxUpdate' => "function(){jQuery('#".CHtml::activeId($model, 'create_time')."').datepicker({'dateFormat':'yy-mm-dd'})}",
    'pager' => array(
        'class' => 'vH_CLinkPager',
    ),
    'filter' => $model,
    'columns' => array(
        array(
            'header' => 'STT',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'htmlOptions' => array('style' => 'text-align:center; width:30px; font-weight:bold'),
        ),
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'ids',
            'selectableRows' => 2,
            'htmlOptions' => array('style' => 'text-align:center; width:30px'),
        //'name'=>'admincp-node-grid-ids',
        ),
        array(
            'name' => 'user_id',
            'htmlOptions' => array('style' => 'width:60px; font-weight:bold; text-align:center'),
        ),
        array(
            'name' => 'firstname',
            'type' => 'html',
            'value' => 'CHtml::link("$data->firstname $data->lastname",array("usersystem/update/","id"=>$data->user_id))',
            'headerHtmlOptions' => array('align' => 'left'),
        ),
        array(
            'name' => 'email',
            'type' => 'html',
            'value' => 'CHtml::link("$data->email",array("usersystem/update/","id"=>$data->user_id))',
            'headerHtmlOptions' => array('align' => 'left'),
        ),        
        array(
            'name' => 'created_at',
            'type' => 'html',
            'headerHtmlOptions' => array('align' => 'left'),
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'htmlOptions' => array('style' => 'width:80px; text-align:center'),
            'value' => 'AdmincpHelper::getStatusLabel( $data->status, $data->user_id )',
            'filter' => AdmincpHelper::getStatusOptions(),
        ),
        array(
            'class' => 'vH_CButtonColumn'
        ),
    ),
));
?>
