<div class="form">
    <?php
    $form = $this->beginWidget('vH_CActiveForm', array(
        'id' => 'user-form',
    ));
    echo $form->hiddenField($model, 'type', array('value' => 1));
    ?>
    <?php if ($msg = Yii::app()->admin->showPutMsg()) echo $msg . '<br>'; ?>
    <?php if ($error = $form->errorSummary($model)) echo $error . '<br>'; ?>
    <table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
        <tr>
            <th colspan="2" align="left">Thông tin tài khoản</th>
        </tr>
        <tbody id="thong_tin_tai_khoan">
           
            <tr>
                <td valign="top"><strong><?php echo $form->labelEx($model, 'email'); ?></strong></td>		
                <td>
                    <?php echo $form->textField($model, 'email', array('size' => 32, 'maxlength' => 100)); ?>
                    <?php echo $form->error($model, 'email'); ?>
                </td>
            </tr>           
            <tr>
                <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'firstname'); ?></strong></td>
                <td>
                    <?php echo $form->textField($model, 'firstname', array('size' => 32, 'maxlength' => 100)); ?>
                    <?php echo $form->error($model, 'firstname'); ?>
                </td>
            </tr>
            <tr>
                <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'lastname'); ?></strong></td>
                <td>
                    <?php echo $form->textField($model, 'lastname', array('size' => 32, 'maxlength' => 100)); ?>
                    <?php echo $form->error($model, 'lastname'); ?>
                </td>
            </tr>
            <tr class="role">
                <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'role_id'); ?></strong></td>
                <td>
                    <?php echo $form->dropDownList($model, 'role_id', AdmincpHelper::getRoleOptions(), array('empty' => 'Chọn quyền', 'style' => 'width: 228px')); ?>
                    <?php echo $form->error($model, 'role_id'); ?>
                </td>
            </tr>
            <?php if ($model->isNewRecord): ?>
                <tr>
                    <td valign="top"><strong><?php echo $form->labelEx($model, 'password'); ?></strong></td>
                    <td>
                        <?php echo $form->passwordField($model, 'password', array('size' => 32, 'maxlength' => 64, 'autocomplete' => 'off')); ?>
                        <?php echo $form->error($model, 'password'); ?>
                    </td>
                </tr>
                <tr>
                    <td valign="top"><strong><?php echo $form->labelEx($model, 'password_cf'); ?></strong></td>
                    <td>
                        <?php echo $form->passwordField($model, 'password_cf', array('size' => 32, 'maxlength' => 64, 'autocomplete' => 'off')); ?>
                        <?php echo $form->error($model, 'password_cf'); ?>
                    </td>
                </tr>
            <?php else: ?>
                <tr>
                    <td valign="top"><strong><?php echo $form->labelEx($model, 'password'); ?></strong></td>
                    <td>
                        <?php echo $form->passwordField($model, 'password', array('size' => 32, 'maxlength' => 64, 'class' => 'fl', 'value' => '', 'disabled' => ($_POST['User']['chg_password'] !== '1'), 'autocomplete' => 'off')); ?>

                        <div class="fl" style="padding:5px 0 0 5px"><input type="checkbox" id="chg_password" value="1" name="User[chg_password]" <?php if ($_POST['User']['chg_password']): ?> checked="checked" <?php endif; ?> class="fl" style="margin:0 3px 0 0" /><label for="chg_password"> Đổi mật khẩu</label></div>
                        <div class="clr"></div>
                        <?php echo $form->error($model, 'password'); ?>
                    </td>
                </tr>
                <tr id="chg_password_cf" style="<?php if (!$_POST['User']['chg_password']): ?>display:none;<?php endif; ?>">
                    <td valign="top"><strong><?php echo $form->labelEx($model, 'password_cf'); ?></strong></td>
                    <td>
                        <input type="password" id="User_password_cf" name="User[password_cf]" class="<?php if ($form->error($model, 'password_cf')): ?>error<?php endif; ?>" maxlength="64" size="32" autocomplete="off">
                        <?php echo $form->error($model, 'password_cf'); ?>
                    </td>
                </tr>
            <?php endif; ?>
            <tr>
                <td valign="top"><strong><?php echo $form->labelEx($model, 'status'); ?></strong></td>
                <td>
                    <?php echo $form->dropDownList($model, 'status', AdmincpHelper::getUserStatusOptions()); ?>
                    <?php echo $form->error($model, 'status'); ?>
                </td>
            </tr>
        </tbody>
    </table>
    <?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
    $('#chg_password').change(function() {
        if (this.checked == true) {
            $('#User_password').attr('disabled', false);
            $('#User_password').val('');
            $('#chg_password_cf').show();
            $('#User_password_cf').val('');
        } else {
            $('#User_password').val('******');
            $('#User_password').attr('disabled', true);
            $('#chg_password_cf').hide();
        }
        $(window).trigger('resize');
    });
</script>