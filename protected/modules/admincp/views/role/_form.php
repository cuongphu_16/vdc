<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="50%" valign="top">
			<?php 
				Yii::app()->clientScript->registerCoreScript('jquery');
				$form = $this->beginWidget('vH_CActiveForm', array(
					'id'=>'admincp-role-form',
					//'method'=>'get',
					//'enableAjaxValidation'=>false,
				)); /* @var $form vH_CActiveForm */
			?>
			<div style="width:99%">
			<?php //echo Yii::app()->admin->showPutMsg(); ?>
			<?php if($msg = $form->errorSummary($model))echo $msg.'<br>'; ?>
			
			<table border="0" cellspacing="1" cellpadding="5" class="adminlist" width="100%">
				<tr>
					<th colspan="2" align="left">Thông tin nhóm người dùng</th>
				</tr>
				<tr>
					<td colspan="2"><p class="note">Điền đầy đủ các mục có dấu <span class="required">*</span>.</p></td>
				</tr>
				<tr>
					<td valign="top" width="200"><strong><?php echo $form->labelEx($model,'role_label'); ?></strong></td>
					<td>
						<?php echo $form->textField($model,'role_label',array('size'=>20,'maxlength'=>20)); ?>
						<?php echo $form->error($model,'role_label'); ?>
					</td>
				</tr>
				<tr>
					<td valign="top"><strong><?php echo $form->labelEx($model,'role_name'); ?></strong></td>
					<td>
						<?php echo $form->textField($model,'role_name',array('size'=>20,'maxlength'=>20)); ?>
						<?php echo $form->error($model,'role_name'); ?>
					</td>
				</tr>
				<tr>
					<td valign="top"><strong><?php echo $form->labelEx($model,'p_id'); ?></strong></td>
					<td>
						<?php echo $form->dropDownList($model,'p_id', AdmincpHelper::getRoleOptions($model->role_id), array('empty'=>'Không chọn')  ); ?>
						<?php echo $form->error($model,'p_id'); ?>
					</td>
				</tr>
				<tr>
					<td valign="top"><strong><?php echo $form->labelEx($model,'create_time'); ?></strong></td>
					<td>
						<?php echo $form->textField($model,'create_time', array('disabled'=>true,)); ?>
						<?php echo $form->error($model,'create_time'); ?>
					</td>
				</tr>
				<tr>
					<td valign="top"><strong><?php echo $form->labelEx($model,'update_time'); ?></strong></td>
					<td>
						<?php echo $form->textField($model,'update_time', array('disabled'=>true,)); ?>
						<?php echo $form->error($model,'update_time'); ?>
					</td>
				</tr>
				<tr>
					<td valign="top"><strong><?php echo $form->labelEx($model,'status'); ?></strong></td>
					<td>
						<?php echo $form->dropDownList($model,'status', AdmincpHelper::getStatusOptions()  ); ?>
						<?php echo $form->error($model,'status'); ?>
					</td>
				</tr>
				<tr>
					<td valign="top"><strong><?php echo $form->labelEx($model,'acl_desc'); ?></strong></td>
					<td>
						<select id="acl_type">
							<option value="full" <?php if($model->acl_desc == 'ALL_PRIVILEGES'):?> selected="selected"<?php endif;?>>Đầy đủ</option>
							<option value="custom" <?php if($model->acl_desc != 'null' && $model->acl_desc != 'ALL_PRIVILEGES' ):?> selected="selected"<?php endif;?>>Tùy chỉnh</option>
							<option <?php if($model->acl_desc == 'null'):?> selected="selected"<?php endif;?> value="null">Không có quyền</option>
						</select>
						<input type="hidden" value="" id="AdmincpRole_acl_desc" name="AdmincpRole[acl_desc]">
					</td>
				</tr>
				<tr>
					<td valign="top"><strong><?php echo $form->labelEx($model,'admin_use'); ?></strong></td>
					<td>
						<?php echo $form->checkBox($model,'admin_use'); ?>
						<?php echo $form->error($model,'admin_use'); ?>
					</td>
				</tr>
				<!--<tr>
					<td valign="top"><strong></strong></td>
					<td>
						<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
					</td>
				</tr>-->
			</table>
			</div>
			<?php $this->endWidget(); ?>
		</td>
		
		<td width="50%" valign="top">
			<table border="0" cellspacing="1" cellpadding="5" class="adminlist" style="width:99%">
				<tr>
					<th align="left">Quyền hạn</th>
				</tr>
				<tr>
					<td>	
						<div id="acl_custom_div" class="acl_custom_role">
						<form id="custom_form" action="" method="post" name="custom_form">
						<?php /*$this->beginWidget('ext.ECheckBoxTree.ECheckBoxTree', array(
							'id'=>'admincp-role-tree',
							'htmlOptions'=>array(
								'collapseImage'=> Yii::app()->getBaseUrl(). '/images/admincp/treeRightArrow.gif',
								'expandImage'=> Yii::app()->getBaseUrl(). '/images/admincp/treeRightArrow.gif',
								'onCheck'=> '{node:"expand"}',
								'onUncheck'=> '{node:"collapse"}',
							),
						));*/ ?>
						
						<ul class="ul">
							<?php echo AdmincpHelper::getRoleTreeHtml( $model->acl_desc ); ?>
						</ul>
						
						<?php //$this->endWidget() ?>
						</form>
						</div>						
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<style media="all" type="text/css">
	.acl_custom_role .parent{
		float:left;
		width:48%;
		padding:5px;
	}
	.acl_custom_role .parent ul{
		padding:5px 0 0 20px;
	}
	.acl_custom_role .parent ul li{
		padding:3px 0;
	}
	.acl_custom_role label{
		line-height:16px;
	}
	.acl_custom_role label span{
		color:#999;
	}
</style>

<script type="text/javascript">
function setFullAccessCheckbox(){
	$("#acl_custom_div input[type='checkbox']").each(function(){
		$(this).attr({'checked':true, 'disabled':true});
	});
	//$('#acl_custom_div').hide();
	$('#AdmincpRole_acl_desc').val('<?php echo Yii::app()->params['fullAccess']?>');
}

$('#acl_type').change( function (){
	if($('#acl_type').val() =='full'){
		setFullAccessCheckbox();
	}else{
	$('#AdmincpRole_acl_desc').val($('#acl_type').val());
	}
	
	if($('#acl_type').val() =='custom') {
		$('#acl_custom_div').show();
		$('#acl_custom_div input[type="checkbox"]').attr({'checked':false, 'disabled':false});
	}
});

//implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function (obj) {
    var t = typeof (obj);
    if (t != "object" || obj === null) {
        // simple data type
        if (t == "string") obj = '"'+obj+'"';
        return String(obj);
    }
    else {
        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);
        for (n in obj) {
            v = obj[n]; t = typeof(v);
            if (t == "string") v = '"'+v+'"';
            else if (t == "object" && v !== null) v = JSON.stringify(v);
            json.push((arr ? "" : '"' + n + '":') + String(v));
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};

$.fn.serializeObject = function() {
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return JSON.stringify(o);
};

/*$("input[type='checkbox']").click(function(){
	$('#AdmincpRole_acl_desc').val($('#custom_form').serializeObject());
});*/

$("ul.sub input[type='checkbox']").change(function(){
	var ul = $(this).parents('ul.sub');
	var li_parent = $(this).parents('li.parent');
	if(this.checked){
		var count_checkbox = $(':checkbox',ul).size();
		var count_checked = $(':checked',ul).size();
		//console.log(count_checkbox+'___'+count_checked);
		if(count_checkbox == count_checked)
			li_parent.find('.check_pr').attr('checked',true);
		else
			li_parent.find('.check_pr').attr('checked',false);
	}else{
		li_parent.find('.check_pr').attr('checked',false);
	}
	$('#AdmincpRole_acl_desc').val($('#custom_form').serializeObject());
});

$('li.parent input.check_pr').change(function(){
	if(this.checked){
		$('ul :checkbox',$(this).parents('.parent')).attr('checked',true);
	}else{
		$('ul :checkbox',$(this).parents('.parent')).attr('checked',false);
	}
	$('#AdmincpRole_acl_desc').val($('#custom_form').serializeObject());
});

var acl_desc = '<?php echo $model->acl_desc ?>';
if(acl_desc == '<?php echo Yii::app()->params['fullAccess']?>'){
	setFullAccessCheckbox();
}/*else{
	$('#acl_type').val('custom');
}*/

$('li.parent').each(function(){
	var ul_sub = $('ul.sub',this);
	var count_checkbox = $(':checkbox',ul_sub).size();
	var count_checked = $(':checked',ul_sub).size();
	if(count_checkbox == count_checked){
		$('.check_pr',this).attr('checked',true);
	}else{
		$('.check_pr',this).attr('checked',false);
	}
});
</script>