<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('admincp-role-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php if($msg = Yii::app()->admin->showPutMsg())echo $msg.'<br>'; ?>

<!--<h1><?php echo $this->getActionLabel() ?></h1>-->

<div class="fl" style="padding-bottom:5px">Có thể sử dụng các ký tự này ở trước từ khóa tìm kiếm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>

<!--<?php echo CHtml::link('Tìm kiếm nâng cao','#',array('class'=>'search-button')); ?>-->
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('vH_CGridView', array(
	'id'=>'admincp-role-grid',
	'dataProvider'=>$model->search(),
	'cssFile' => $this->module->getAssetsUrl().'/css/CGridView.css',
	'itemsCssClass' => 'adminlist',
	'itemAttributes' => array('cellspacing'=> '1', 'cellpadding'=>'3', 'border'=> '0'),
	'filter'=>$model,
	'pager' => array(
		'cssFile' => $this->module->getAssetsUrl().'/css/pages.css',
		'header' => false,
		'pageSize' => 5,
	),
	'columns'=>array(
		array(
			'header'=>'STT',
			'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			'htmlOptions'=>array('style'=>'text-align:center; width:30px; font-weight:bold'),
		),
		array(
			'class'=>'CCheckBoxColumn',
		    'id'=>'ids', 
            'selectableRows'=>2, 
			//'name'=>'admincp-node-grid-ids',
			'htmlOptions'=>array('style'=>'width:30px; text-align:center'),
		),
		//'role_id',
		array(
			'name'=>'role_id',
			'htmlOptions'=>array('style'=>'width:40px; text-align:center; font-weight:bold'),
		),
		array(
			'name' => 'role_label',
			'type' => 'html',
			'value' => 'CHtml::link("$data->role_label",array("role/update/","id"=>$data->role_id))',
            'headerHtmlOptions'=>array('align'=>'left'),
		),
		array(
			'name' => 'role_name',
			'type' => 'html',
			'value' => 'CHtml::link("$data->role_name",array("role/update/","id"=>$data->role_id))',
            'headerHtmlOptions'=>array('align'=>'left'),
		),
		array(
			'name' => 'admin_use',
	        'value' => 'AdmincpHelper::getYNLabel($data->admin_use)',
			'filter'=> AdmincpHelper::getYNOptions(),
	    	'htmlOptions'=>array('style'=>'width:80px; text-align:center'),
		),
		array(
	        'name' => 'status',
	        'type' => 'raw',
	        'value' => 'AdmincpHelper::getStatusLabel( $data->status, $data->role_id )',
			'filter'=> AdmincpHelper::getStatusOptions(),
	    	'htmlOptions'=>array('style'=>'width:80px; text-align:center'),
	    ),
	    /*
		'acl_desc',
		'p_id',
		*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
			'htmlOptions'=>array('style'=>'width:40px; text-align:center'),
		),
	),
)); ?>
