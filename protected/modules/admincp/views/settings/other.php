<div class="form">
    <form action="" method="POST" id="upload_video_form">
        <div style="max-height: 200px; overflow: auto"><?= $html ?></div>
        <table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
            <tr>
                <td width="200"><strong>Folder Trên Máy tính</strong></td>	
                <td><input type="text" name="folder_from" size="50" /> (VD: D:/xampp/htdocs/theodoi247/video/)</td>
            </tr>
            <tr>
                <td width="200"><strong>Folder Trên Server</strong></td>	
                <td><input type="text" name="folder_to" size="50" /> (VD: videoupload)</td>
            </tr>
            <tr>
                <td width="200"><strong>ID thành viên</strong></td>	
                <td><input type="text" name="user_id" size="50" /></td>
            </tr>
            <tr>
                <td width="200"><strong>Tìm kiếm thành viên</strong></td>	
                <td>
                    <div style="position: relative;" class="fl">
                        <input type="text" id="searchid" placeholder="Nhập tên khách hàng" size="50" class="search" autocomplete="off" />
                        <div id="result_search"></div>
                    </div>
                </td>
            </tr>
        </table>
    </form>
    <style>
        #result_search
        {
            position:absolute;
            width:350px;
            display:none;
            margin-top:0px;
            border-top:0px;
            overflow:hidden;
            background-color: white;
            background: #eaeaea;
            z-index: 9999;
            zoom: 1;
        }
        #result_search .show{
            padding: 5px;
            color: #000;
        }
        #result_search .show .name{
            line-height: 20px;
            font-weight: bold;
            font-size: 14px;
            margin-bottom: 5px;
        }
        #result_search .show:hover
        {
            background:#444;
            color:#FFF;
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('a.save').bind('click', function () {
                $('#upload_video_form').submit();
            });
            $(".search").keyup(function ()
            {
                var searchid = $(this).val();
                var dataString = 'search=' + searchid;
                if (searchid != '')
                {
                    $.ajax({
                        type: "POST",
                        url: "<?= $this->createUrl('settings/searchMember') ?>",
                        data: dataString,
                        cache: false,
                        success: function (html)
                        {
                            $("#result_search").html(html).show();
                        }
                    });
                } else {
                    if (!$('.new_info').is(':visible')) {
                        $('.new_info').slideDown();
                    }
                }
                return false;
            });

            $('#result_search .show').live('click', function () {
                var val = $(this).children('.name').text();
                $('#searchid').val(val);
                var userid = $(this).children('.userid').text();
                $('input[name=user_id]').val(userid);
                if (val != '') {
                    $('.new_info').slideUp();
                } else {
                    $('.new_info').slideDown();
                }
            });
            jQuery(document).live("click", function (e) {
                var $clicked = $(e.target);
                if (!$clicked.hasClass("search")) {
                    jQuery("#result_search").fadeOut();
                }
            });
            $('#searchid').click(function () {
                jQuery("#result_search").fadeIn();
            });
        });
    </script>
</div><!-- form -->