<?php
if (!empty($users)):
    foreach ($users as $user):
        $fullname = $user->fullname;
        $b_fullname = '<span style="color: rgb(0, 148, 255)">' . $keywords . '</span>';
        $final_fullname = str_ireplace($keywords, $b_fullname, $fullname);
        ?>
        <div class="show">
            <div class="name"><?= $final_fullname ?></div>
            <div class="address"><?= $user->address ?></div>
            <div class="userid" style="display: none"><?=$user->user_id?></div>
        </div>
        <?php
    endforeach;
endif;
?>