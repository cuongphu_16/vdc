<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('settings-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php if ($msg = Yii::app()->admin->showPutMsg()) echo $msg . '<br>'; ?>

<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>


<?php
$this->widget('vH_CGridView', array(
    'id' => 'settings-grid',
    'dataProvider' => $model->search(),
    'cssFile' => $this->module->getAssetsUrl() . '/css/CGridView.css',
    'itemsCssClass' => 'adminlist',
    'itemAttributes' => array('cellspacing' => '1', 'cellpadding' => '3', 'border' => '0'),
    'filter' => $model,
    'pager' => array(
        'class' => 'vH_CLinkPager',
    ),
    'columns' => array(
        array(
            'header' => 'STT',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'htmlOptions' => array('style' => 'text-align:center; width:30px; font-weight:bold'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),
        //'id',
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'ids',
            'selectableRows' => 2,
            'htmlOptions' => array('style' => 'text-align:center'),
        ),
        'key',
        'description' => array(
            'type' => 'html',
            'value' => '$data->description',
            'filter' => '',
            'header' => 'Mô tả',
            'htmlOptions' => array('style' => 'width:300px; font-style: italic')
        ),
        'value' => array(
            'type' => 'html',
            'value' => '$data->key == "color_menu" ? "" : strip_tags(unserialize($data->value),"<br><p><b><strong><a>")',
            'header' => 'Nội dung'
        ),
        array(
            'class' => 'vH_CButtonColumn'
        ),
    ),
));
?>
