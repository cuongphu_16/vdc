<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('admincp-log-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h1><?php echo $this->getActionLabel() ?></h1>-->

<div class="fl" style="padding-bottom:5px">Có thể sử dụng các ký tự này ở trước từ khóa tìm kiếm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>

<!--<?php //echo CHtml::link('Tìm kiếm nâng cao','#',array('class'=>'search-button'));  ?>
<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array('model'=>$model,)); ?>
</div>-->



<?php
$this->widget('vH_CGridView', array(
    'id' => 'admincp-log-grid',
    'dataProvider' => $model->search(),
    'cssFile' => $this->module->getAssetsUrl() . '/css/CGridView.css',
    'itemsCssClass' => 'adminlist',
    'itemAttributes' => array('cellspacing' => '1', 'cellpadding' => '3', 'border' => '0'),
    'afterAjaxUpdate' => "function(){jQuery('#" . CHtml::activeId($model, 'log_time') . "').datepicker({'dateFormat':'yy-mm-dd'})}",
    //'ajaxUpdate' => false,
    'pager' => array(
        'cssFile' => $this->module->getAssetsUrl() . '/css/pages.css',
        'header' => false,
        'pageSize' => 5,
    ),
    'filter' => $model,
    'columns' => array(
        array(
            'header' => 'STT',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'htmlOptions' => array('style' => 'text-align:center; width:30px; font-weight:bold'),
        ),
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'ids',
            'selectableRows' => 2,
            'htmlOptions' => array('style' => 'width:20px; text-align:center'),
        ),
        //'log_id',
        array(
            'name' => 'log_id',
            'htmlOptions' => array('style' => 'width:40px; text-align:center; font-weight:bold'),
        ),
        //'action_type',
        array(
            'name' => 'action_type',
            'type' => 'raw',
            'htmlOptions' => array('style' => 'width:80px; text-align:center'),
            'value' => 'AdmincpHelper::getLogTypeLabel( $data->action_type )',
            'filter' => AdmincpHelper::getLogTypeOptions(),
        ),
        array(
            'name' => 'action_controller',
        ),
        'action_model',
        array(
            'name' => 'log_time',
            'htmlOptions' => array('style' => 'width:120px; text-align:center'),
        ),
        //'user_id',
        array(
            'name' => 'user_id',
            'value' => '$data->admin->username',
        ),
        /*
          'action_params',
          'action_info',
         */
        /* array(
          'name' => 'status',
          'type' => 'raw',
          'htmlOptions'=>array('style'=>'width:80px'),
          'value' => 'AdmincpHelper::getStatusLabel( $data->status )',
          'filter'=> AdmincpHelper::getStatusOptions(),
          ), */
        array(
            'name' => 'remote_addr',
            'type' => 'raw',
            'htmlOptions' => array('style' => 'width:70px; text-align:center'),
            'value' => 'long2ip( $data->remote_addr )',
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{view} {delete}',
            'htmlOptions' => array('style' => 'width:40px; text-align:center'),
        ),
    ),
));
?>

<?php
/* $a = $model->search()->pagination;
  $this->widget('CLinkPager', array(
  'pages' => $a,
  )) */
?>