<div class="view">

	<?php echo Yii::app()->admin->showPutMsg(); ?>

	<b><?php echo CHtml::encode($data->getAttributeLabel('log_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->log_id), array('view', 'id'=>$data->log_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('log_time')); ?>:</b>
	<?php echo CHtml::encode($data->log_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('action_type')); ?>:</b>
	<?php echo CHtml::encode($data->action_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('action_info')); ?>:</b>
	<?php echo CHtml::encode($data->action_info); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('action_controller')); ?>:</b>
	<?php echo CHtml::encode($data->action_controller); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('action_model')); ?>:</b>
	<?php echo CHtml::encode($data->action_model); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remote_addr')); ?>:</b>
	<?php echo CHtml::encode($data->remote_addr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<?php 
	
	/*
	
	*/ ?>

</div>