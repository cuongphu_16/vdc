<!--<h1><?php //echo $this->getActionLabel(). $model->log_id ?></h1>-->

<?php echo Yii::app()->admin->showPutMsg(); ?>
<?php $form=$this->beginWidget('vH_CActiveForm', array(
	'id'=>'log-form',
	'enableAjaxValidation'=>false,
));

?>
<?php $this->widget('vH_CDetailView', array(
	'data'=>$model,
	'htmlOptions' => array(
		'class' => 'adminlist',
		'cellpadding' => 5,
		'cellspacing' => 1,
		'border' => 0,
	),
	'thead' => '<tr><th colspan=2 align=left>Thông tin chi tiết</th></tr>',
	'attributes'=>array(
		'log_id',
		'log_time',
		array(
	        'name' => 'action_type',
	        'type' => 'raw',
	        'value' => AdmincpHelper::getLogTypeLabel( $model->action_type ),
	    ),
	    'action_info',
		'action_controller',
		'action_model',
		'admin.username',
		array(
	        'name' => 'remote_addr',
	        'type' => 'raw',
	        'value' => long2ip( $model->remote_addr ),
	    ),
		/*array(
	        'name' => 'status',
	        'type' => 'raw',
	        'value' => AdmincpHelper::getStatusLabel( $model->status, null, false),
	    ),*/
	),
)); ?>
<?php $this->endWidget(); ?>