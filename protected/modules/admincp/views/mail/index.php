<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style media="all" type="text/css">
img{
	border:0;
}
a:link,
a:visited{
	text-decoration:none;
	color:#af2624
}
</style>
</head>

<body>
<div style="color:#6a6a6a; font-family:Arial, Helvetica, sans-serif; font-size:13px; padding:20px 0 0">
	<div style="width:600px;">
		<?php
			$this->renderPartial('/mail/_header');
		?>
		<div style="padding:15px 0 30px 0">
			<?=$content;?>
		</div>
		<?php
			$this->renderPartial('/mail/_footer');
		?>
	</div>
</div>
</body>
</html>
