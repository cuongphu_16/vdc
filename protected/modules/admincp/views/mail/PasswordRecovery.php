<?php $this->beginContent('/mail/index'); ?>
<div style="font-size:22px; color:#333">Lấy lại mật khẩu</div>
<div style="margin:0 0 20px 0"><img src="cid:arr_tb" width="31" height="16" /></div>

<div>
	Hệ thống đã nhận được một yêu cầu khôi phục mật khẩu tài khoản của bạn trên Theodoi247.
	<br>
	Nếu đúng bạn là người đã gửi yêu cầu này thì bấm <a href="<?=$recovery_url?>">vào đây</a> để khôi phục mật khẩu hoặc copy URL sau và dán vào thanh địa chỉ của trình duyệt:<br>
	<?=$recovery_url?>
</div>

<?php $this->endContent('/mail/index'); ?>