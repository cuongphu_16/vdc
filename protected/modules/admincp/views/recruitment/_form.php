<div class="form">

    <?php
    $form = $this->beginWidget('vH_CActiveForm', array(
        'id' => 'recruitment-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo Yii::app()->admin->showPutMsg(); ?>
    <?php echo $form->errorSummary($model); ?>
    <br />

    <table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
        <tr>
            <th colspan="2" align="left">Thông tin tài khoản</th>
        </tr>
        <tr>
            <td colspan="2"><p class="note">Điền đầy đủ các mục có dấu <span class="required">*</span>.</p></td>
        </tr>  

        <tr>
            <td width="200"><strong><?php echo $form->labelEx($model, 'language'); ?></strong></td>		
            <td colspan="3">
                <?php echo $form->dropDownList($model, 'language', SiteHelper::getLanguage(), array('empty' => '-- Ngôn Ngữ--', 'style' => 'width: 150px')); ?>
                <?php echo $form->error($model, 'language'); ?>
            </td>
        </tr>

        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'title'); ?></strong></td>
            <td>
                <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 100)); ?>
                <?php echo $form->error($model, 'title'); ?>
            </td>
        </tr>
        <script type="text/javascript" language="javascript">
            $(function() {
                $('#Recruitment_title').die('focusout keyup').live('focusout keyup', function() {
                    /*console.log(this.value);*/
                    $('#Recruitment_alias').val(Utils.alias(this.value, true));
                });
            });
        </script>	

        <?php echo $form->hiddenField($model, 'alias', array('size' => 60, 'maxlength' => 255)); ?>

        <tr>
            <td><strong><?php echo $form->labelEx($model, 'image'); ?></strong></td>		
            <td>
                <?php echo $form->fileField($model, 'image'); ?>
                <?php echo $form->error($model, 'image'); ?>
                <div style="clear: both;margin-top: 10px"></div>
                <?php if (!empty($model->image)) : ?>

                    <?php echo $form->hiddenField($model, 'image_hidden', array('value' => $model->image)); ?>
                    <div><?php echo CHtml::image($model->getImage(300, 0)->src); ?></div>
                    <a style="line-height: 30px;cursor: pointer" onclick="deleteImage(<?= $model->recruitment_id ?>)">Xóa ảnh</a>
                <?php endif; ?>
                <?php if (isset(Yii::app()->session['recruitment_upload_image'])): ?>
                    <?php
                    $img_data = Controller::getImageUpload((object) array(
                                        'folder' => 'recruitment',
                                        'id' => Yii::app()->session['recruitment_upload_folder'],
                                        'width' => 300,
                                        'height' => 0,
                                        'filename' => Yii::app()->session['recruitment_upload_image'],
                                        'not_int' => 'true'
                    ));
                    echo CHtml::image($img_data->src) . '<br>';
                    ?>
                <?php endif; ?>
            </td>
        <script type="text/javascript">
            $("#ytPost_image").val($("#Post_image_hidden").val());
        </script>
        </tr>

        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'quantity'); ?></strong></td>
            <td>
                <?php echo $form->textField($model, 'quantity', array('size' => 60, 'maxlength' => 100)); ?>
                <?php echo $form->error($model, 'quantity'); ?>
            </td>
        </tr>

        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'gender'); ?></strong></td>
            <td>
                <?php echo $form->textField($model, 'gender', array('size' => 60, 'maxlength' => 100)); ?>
                <?php echo $form->error($model, 'gender'); ?>
            </td>
        </tr>

        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'education'); ?></strong></td>
            <td>
                <?php echo $form->textField($model, 'education', array('size' => 60, 'maxlength' => 100)); ?>
                <?php echo $form->error($model, 'education'); ?>
            </td>
        </tr>

        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'experience'); ?></strong></td>
            <td>
                <?php echo $form->textField($model, 'experience', array('size' => 60, 'maxlength' => 100)); ?>
                <?php echo $form->error($model, 'experience'); ?>
            </td>
        </tr>

        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'dateline'); ?></strong></td>
            <td>
                <?php echo $form->textField($model, 'dateline', array('size' => 60, 'maxlength' => 100)); ?>
                <?php echo $form->error($model, 'dateline'); ?>
            </td>
        </tr>

        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'salary'); ?></strong></td>
            <td>
                <?php echo $form->textField($model, 'salary', array('size' => 60, 'maxlength' => 100)); ?>
                <?php echo $form->error($model, 'salary'); ?>
            </td>
        </tr>        

        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'description'); ?></strong></td>		
            <td>
                <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'editors_desc')); ?>
                <?php echo $form->error($model, 'description'); ?>
                <?php
                $editor = vH_Utils::showEditor();
                echo $editor->replaceAll('editors_desc');
                ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'request'); ?></strong></td>		
            <td>
                <?php echo $form->textArea($model, 'request', array('rows' => 6, 'cols' => 50, 'class' => 'editors')); ?>
                <?php echo $form->error($model, 'request'); ?>
                <?php
                $editor = vH_Utils::showEditor();
                echo $editor->replaceAll('editors');
                ?>
            </td>
        </tr>    
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'benefit'); ?></strong></td>		
            <td>
                <?php echo $form->textArea($model, 'benefit', array('rows' => 6, 'cols' => 50, 'class' => 'editors_benefit')); ?>
                <?php echo $form->error($model, 'benefit'); ?>
                <?php
                $editor = vH_Utils::showEditor();
                echo $editor->replaceAll('editors_benefit');
                ?>
            </td>
        </tr>    
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'cv'); ?></strong></td>		
            <td>
                <?php echo $form->textArea($model, 'cv', array('rows' => 6, 'cols' => 50, 'class' => 'editors_cv')); ?>
                <?php echo $form->error($model, 'cv'); ?>
                <?php
                $editor = vH_Utils::showEditor();
                echo $editor->replaceAll('editors_cv');
                ?>
            </td>
        </tr>    

        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'status'); ?></strong></td>
            <td>
                <?php echo $form->dropDownList($model, 'status', AdmincpHelper::getStatusOptions(), array('style' => 'width: 150px')); ?>
                <?php echo $form->error($model, 'status'); ?>
            </td>
        </tr>       
    </table>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    function deleteImage(id) {
        $.ajax({
            url: "<?= $this->createUrl("Post/deleteImage") ?>",
            type: "recruitment",
            dataType: 'html',
            cache: false,
            data: {id: id},
            success: function(data) {
                location.reload();
            }
        });
    }
    $("#Post_type").change(function() {
        if ($("#Post_type").val() == 4) {
            $("#hide_type_career").show();
        } else {
            $("#hide_type_career").hide();
        }
    });
    $("#Post_type").change(function() {
        if ($("#Post_type").val() == 9) {
            $(".hide_career_category").show();
        } else {
            $(".hide_career_category").hide();
        }
    });
    if ($("#Post_type").val() == 4) {
        $("#hide_type_career").show();
    } else {
        $("#hide_type_career").hide();
    }
    if ($("#Post_type").val() == 9) {
        $(".hide_career_category").show();
    } else {
        $(".hide_career_category").hide();
    }
</script>