<div class="form">

<?php $form=$this->beginWidget('vH_CActiveForm', array(
	'id'=>'mailtemplate-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo Yii::app()->admin->showPutMsg(); ?>
	<?php echo $form->errorSummary($model); ?>
	<br />

	<table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
		<tr>
			<th colspan="2" align="left">Thông tin tài khoản</th>
		</tr>
		<tr>
			<td colspan="2"><p class="note">Điền đầy đủ các mục có dấu <span class="required">*</span>.</p></td>
		</tr>
		<tr>
			<td valign="top" width="200"><strong><?php echo $form->labelEx($model,'key'); ?></strong></td>
			<td>
				<?php echo $form->textField($model,'key',array('size'=>60,'maxlength'=>100)); ?>
				<?php echo $form->error($model,'key'); ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'value'); ?></strong></td>
			<td>
				<?php echo $form->textArea($model,'value',array('rows'=>6, 'cols'=>50,'class'=>'ckeditor')); ?>
				<?php echo $form->error($model,'value'); ?>
				
				<?php
					$edt = vH_Utils::showEditor();
					echo $edt->replaceAll('ckeditor');
				?>
			</td>
		</tr>
		<tr>
			<td valign="top"><strong><?php echo $form->labelEx($model,'isnumber'); ?></strong></td>
			<td>
				<?php echo $form->checkBox($model,'isnumber'); ?>
				<?php echo $form->error($model,'isnumber'); ?>
			</td>
		</tr>
	</table>

<?php $this->endWidget(); ?>

</div><!-- form -->