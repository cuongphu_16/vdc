<?php 
//echo $this->module->registerJsRoot('jquery.jeditable.mini.js');

/*Yii::app()->clientScript->registerScript('value','
	$("span[class*=\'editable\']").live("click", function (){
		$(this).editable("'.$this->createUrl('settings/edittable').'", {
			submitdata : function (value,settings){
				return {"id":$(this).attr("class").replace("editable-","")};
			},
			indicator : "Saving...",
			tooltip   : "Click to edit...",
			type : "textarea",
			//data   : "{\'1\':\'Active\',\'0\':\'Inactive\'}",
			submit   : "Lưu",
			name : "value"
		 });
	});	 

',CClientScript::POS_READY);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('settings-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php if($msg = Yii::app()->admin->showPutMsg()) echo $msg.'<br>'; ?>

<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>


<?php $this->widget('vH_CGridView', array(
	'id'=>'settings-grid',
	'dataProvider'=>$model->search(),
	'cssFile' => $this->module->getAssetsUrl().'/css/CGridView.css',
	'itemsCssClass' => 'adminlist',
	'itemAttributes' => array('cellspacing'=> '1', 'cellpadding'=>'3', 'border'=> '0'),
	'filter'=>$model,
	'pager' => array(
		'cssFile' => $this->module->getAssetsUrl().'/css/pages.css',
		'header' => false,
		'pageSize' => 5,
	),
	'columns'=>array(
		array(
			'header'=>'STT',
			'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			'htmlOptions'=>array('style'=>'text-align:center; width:30px; font-weight:bold'),
		),
		//'id',
		array(
			'class'=>'CCheckBoxColumn',
		    'id'=>'ids', 
            'selectableRows'=>2, 
			'htmlOptions'=>array('style'=>'text-align:center; width:50px;'),
			//'name'=>'admincp-node-grid-ids',
		),
		array(
			'name' => 'key',
			'htmlOptions'=>array('style'=>'width:300px;'),
		),
		
		'value' => array(
			'type'=>'html',
			'value' => 'strip_tags(unserialize($data->value),"<br><p><b><strong><a>")',
			'header' => 'Nội dung'
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
			'htmlOptions'=>array('style'=>'width:40px; text-align:center'),
		),
	),
));

?>
