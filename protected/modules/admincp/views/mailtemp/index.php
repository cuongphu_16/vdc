<?php
/* @var $this MailtempController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mailtemp',
);

$this->menu=array(
	array('label'=>'Create Mailtemp', 'url'=>array('create')),
	array('label'=>'Manage Mailtemp', 'url'=>array('admin')),
);
?>

<h1>Mailtemp</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
