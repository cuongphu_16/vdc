<?php
/* @var $this MailtempController */
/* @var $model Mailtemp */

$this->breadcrumbs=array(
	'Mailtemp'=>array('index'),
	$model->setting_id,
);

$this->menu=array(
	array('label'=>'List Mailtemp', 'url'=>array('index')),
	array('label'=>'Create Mailtemp', 'url'=>array('create')),
	array('label'=>'Update Mailtemp', 'url'=>array('update', 'id'=>$model->setting_id)),
	array('label'=>'Delete Mailtemp', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->setting_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Mailtemp', 'url'=>array('admin')),
);
?>

<h1>View Mailtemp #<?php echo $model->setting_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'setting_id',
		'varname',
		'value',
		'sort_order',
		'update_time',
		'update_by',
	),
)); ?>
