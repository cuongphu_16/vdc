<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('category-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>
<?php if ($msg = Yii::app()->admin->showPutMsg()) echo $msg . '<br>'; ?>
<?php
$this->widget('vH_CGridView', array(
    'id' => 'category-grid',
    'dataProvider' => $model->search(),
    'cssFile' => $this->module->getAssetsUrl() . '/css/CGridView.css',
    'itemsCssClass' => 'adminlist',
    'itemAttributes' => array('cellspacing' => '1', 'cellpadding' => '3', 'border' => '0'),
    //'afterAjaxUpdate' => "function(){jQuery('#".CHtml::activeId($model, 'create_time')."').datepicker({'dateFormat':'yy-mm-dd'})}",
    'pager' => array(
	'cssFile' => $this->module->getAssetsUrl() . '/css/pages.css',
	'header' => false,
    //'pageSize' => 5,
    ),
    'filter' => $model,
    'columns' => array(
	array(
	    'header' => 'STT',
	    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
	    'htmlOptions' => array('style' => 'text-align:center; width:30px; font-weight:bold'),
	),
	array(
	    'class' => 'CCheckBoxColumn',
	    'id' => 'ids',
	    'selectableRows' => 2,
	    'htmlOptions' => array('style' => 'text-align:center; width:30px'),
	),
	array(
	    'name' => 'cate_id',
	    'htmlOptions' => array('style' => 'width:60px; font-weight:bold; text-align:center'),
	),
	array(
	    'name' => 'title',
	    'type' => 'html',
	    'value' => 'CHtml::link("$data->title",array("category/update/","id"=>$data->cate_id))',
	    'headerHtmlOptions' => array('align' => 'left'),
	),
	array(
	    'name' => 'type',
	    'value' => 'SiteHelper::getTypeCateLabel($data->type)',
	    'filter' => SiteHelper::getTypeCateOptions(),
	    'htmlOptions' => array('style' => 'width:150px;text-align:center'),
	),
	array(
	    'name' => 'sort_order',
	    'htmlOptions' => array('style' => 'width:30px; text-align:center'),
	),
	array(
	    'name' => 'status',
	    'type' => 'raw',
	    'value' => 'AdmincpHelper::getStatusLabel( $data->status, $data->cate_id )',
	    'filter' => AdmincpHelper::getStatusOptions(),
	    'htmlOptions' => array('style' => 'width:80px; text-align:center'),
	),
	array(
	    'class' => 'EButtonColumnWithClearFilters',
	    'onClick_BeforeClear' => 'return true;',
	    'url' => 'Yii::app()->controller->createUrl(Yii::app()->controller->action->ID,array("clearFilters"=>1))',
	    'template' => '{update} {delete}',
	    'htmlOptions' => array('style' => 'width:40px; text-align:center'),
	),
    ),
));
?>