<div class="form">

    <?php
    $form = $this->beginWidget('vH_CActiveForm', array(
        'id' => 'services-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo Yii::app()->admin->showPutMsg(); ?>
    <?php echo $form->errorSummary($model); ?>
    <br />

    <table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
        <tr>
            <th colspan="2" align="left">Thông tin tài khoản</th>
        </tr>
        <tr>
            <td colspan="2"><p class="note">Điền đầy đủ các mục có dấu <span class="required">*</span>.</p></td>
        </tr>

        <tr>
            <td width="200"><strong><?php echo $form->labelEx($model, 'language'); ?></strong></td>		
            <td colspan="3">
                <?php echo $form->dropDownList($model, 'language', SiteHelper::getLanguage(), array('empty' => '-- Ngôn Ngữ--', 'style' => 'width: 150px', 'onchange' => CHtml::ajax(array('type' => 'GET', 'url' => array('main/loadCateServices/'), 'update' => '#Services_cate_id')))); ?>
                <?php echo $form->error($model, 'language'); ?>
            </td>
        </tr>


        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'title'); ?></strong></td>
            <td>
                <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 100)); ?>
                <?php echo $form->error($model, 'title'); ?>
            </td>
        </tr>
<!--        <tr>
            <td width="200"><strong><?php echo $form->labelEx($model, 'cate_id'); ?></strong></td>		
            <td colspan="3">
                <?php echo $form->dropDownList($model, 'cate_id', SiteHelper::getTypeCatOptions(Category::TYPE_SERVICES, 0, $model->language), array('empty' => '-- Danh mục dịch vụ --', 'style' => 'width: 400px')); ?>
                <?php echo $form->error($model, 'cate_id'); ?>
            </td>
        </tr>-->
        <script type="text/javascript" language="javascript">
            $(function () {
                $('#Services_title').die('focusout keyup').live('focusout keyup', function () {
                    /*console.log(this.value);*/
                    $('#Services_alias').val(Utils.alias(this.value, true));
                });
            });
        </script>	

        <?php echo $form->hiddenField($model, 'alias', array('size' => 60, 'maxlength' => 255)); ?>

        <tr>
            <td><strong><?php echo $form->labelEx($model, 'image'); ?></strong></td>		
            <td>
                <?php echo $form->fileField($model, 'image'); ?>
                <?php echo $form->error($model, 'image'); ?>
                <div style="clear: both;margin-top: 10px"></div>
                <?php if (!empty($model->image)) : ?>

                    <?php echo $form->hiddenField($model, 'image_hidden', array('value' => $model->image)); ?>
                    <div><?php echo CHtml::image($model->getImage(300, 0)->src); ?></div>
                    <a style="line-height: 30px;cursor: pointer" onclick="deleteImage(<?= $model->service_id ?>)">Xóa ảnh</a>
                <?php endif; ?>
                <?php if (isset(Yii::app()->session['services_upload_image'])): ?>
                    <?php
                    $img_data = Controller::getImageUpload((object) array(
                                        'folder' => 'services',
                                        'id' => Yii::app()->session['services_upload_folder'],
                                        'width' => 300,
                                        'height' => 0,
                                        'filename' => Yii::app()->session['services_upload_image'],
                                        'not_int' => 'true'
                    ));
                    echo CHtml::image($img_data->src) . '<br>';
                    ?>
                <?php endif; ?>
            </td>
        <script type="text/javascript">
            $("#ytServices_image").val($("#Services_image_hidden").val());
        </script>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'description'); ?></strong></td>		
            <td>
                <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'editors_desc')); ?>
                <?php echo $form->error($model, 'description'); ?>
                <?php
                $editor = vH_Utils::showEditor();
                echo $editor->replaceAll('editors_desc');
                ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'content'); ?></strong></td>		
            <td>
                <?php echo $form->textArea($model, 'content', array('rows' => 6, 'cols' => 50, 'class' => 'editors')); ?>
                <?php echo $form->error($model, 'content'); ?>
                <?php
                $editor = vH_Utils::showEditor();
                echo $editor->replaceAll('editors');
                ?>
            </td>
        </tr>
        <tr>
            <td valign="top" width="200"><strong><?php echo $form->labelEx($model, 'sort_order'); ?></strong></td>		
            <td colspan="3">
                <?php echo $form->textField($model, 'sort_order', array('size' => 60, 'maxlength' => 255)); ?>
                <?php echo $form->error($model, 'sort_order'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'focus'); ?></strong></td>
            <td>
                <?php echo $form->dropDownList($model, 'focus', array('0' => 'Bài thường', '1' => "Bài nổi bật"), array('style' => 'width: 150px')); ?>
                <?php echo $form->error($model, 'focus'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"><strong><?php echo $form->labelEx($model, 'status'); ?></strong></td>
            <td>
                <?php echo $form->dropDownList($model, 'status', AdmincpHelper::getStatusOptions(), array('style' => 'width: 150px')); ?>
                <?php echo $form->error($model, 'status'); ?>
            </td>
        </tr>       
    </table>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    function deleteImage(id) {
        $.ajax({
            url: "<?= $this->createUrl("Services/deleteImage") ?>",
            type: "services",
            dataType: 'html',
            cache: false,
            data: {id: id},
            success: function (data) {
                location.reload();
            }
        });
    }
</script>