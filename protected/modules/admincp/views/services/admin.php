<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>
<?php if ($msg = Yii::app()->admin->showPutMsg()) echo $msg . '<br>'; ?>
<?php
$this->widget('vH_CGridView', array(
    'id' => 'services-grid',
    'dataProvider' => $model->search(),
    'cssFile' => $this->module->getAssetsUrl() . '/css/CGridView.css',
    'itemsCssClass' => 'adminlist',
    'itemAttributes' => array('cellspacing' => '1', 'cellpadding' => '3', 'border' => '0'),
    'pager' => array(
        'class' => 'vH_CLinkPager',
    ),
    'filter' => $model,
    'columns' => array(
        array(
            'header' => 'STT',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'htmlOptions' => array('style' => 'text-align:center; width:30px; font-weight:bold'),
        ),
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'ids',
            'selectableRows' => 2,
            'htmlOptions' => array('style' => 'text-align:center; width:30px'),
        ),
        array(
            'name' => 'title',
            'value' => 'CHtml::link("$data->title",array("services/update/","id"=>$data->service_id))',
            'type' => 'html',
            'htmlOptions' => array('style' => 'width:150px;'),
        ),
        array(
            'name' => 'image',
            'type' => 'html',
            'value' => '(!empty($data->image))?CHtml::image($data->getImage(72,72)->src,NULL,array("width"=>72,"height"=>72)):""',
            'filter' => '',
            'htmlOptions' => array('style' => 'text-align:center; width:80px;'),
        ),
        array(
            'name' => 'content',
            'value' => 'SiteHelper::cut_string($data->content,150)',
            'type' => 'html',
            'htmlOptions' => array('style' => 'width:400px;'),
        ),        
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => 'AdmincpHelper::getStatusLabel( $data->status, $data->service_id )',
            'filter' => AdmincpHelper::getStatusOptions(),
            'htmlOptions' => array('style' => 'width:80px; text-align:center'),
        ),
        array(
            'class' => 'vH_CButtonColumn'
        ),
    ),
));
