<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>
<?php if ($msg = Yii::app()->admin->showPutMsg()) echo $msg . '<br>'; ?>
<?php
$this->widget('vH_CGridView', array(
    'id' => 'post-grid',
    'dataProvider' => $model->search(),
    'cssFile' => $this->module->getAssetsUrl() . '/css/CGridView.css',
    'itemsCssClass' => 'adminlist',
    'itemAttributes' => array('cellspacing' => '1', 'cellpadding' => '3', 'border' => '0'),
    'pager' => array(
	'class' => 'vH_CLinkPager',
    ),
    'filter' => $model,
    'columns' => array(
	array(
	    'header' => 'STT',
	    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
	    'htmlOptions' => array('style' => 'text-align:center; width:30px; font-weight:bold'),
	),
	array(
	    'class' => 'CCheckBoxColumn',
	    'id' => 'ids',
	    'selectableRows' => 2,
	    'htmlOptions' => array('style' => 'text-align:center; width:30px'),
	),
	array(
	    'name' => 'option',
	    'type' => 'html',
	    'value'=>'SiteHelper::getRateLabel($data->option)'
	),
	array(
	    'name' => 'comment',
	    'type' => 'html',
	),
	array(
	    'name' => 'created_at',
	    'type' => 'html',
	),
	array(
	    'class' => 'vH_CButtonColumn'
	),
    ),
));
