<?php

class vH_PkRandom extends CActiveRecordBehavior {

    public function beforeSave($event) {
        $model = $event->sender;
        if ($model->isNewRecord) {
            if (empty($model->primaryKey))
                $model->{$model->tableSchema->primaryKey} = SiteHelper::getDataTableID($model);
        }
        return $model;
    }

}

?>
