<?php

class AdmincpARB extends CActiveRecordBehavior
{
	
	//only catch original data on update event.
	/*
	public function beforeSave($event)
	{
		$model= $event->sender;
		if ( !$model->getIsNewRecord() ) {
			Yii::app()->admin->setFlash($model->tableName(). '_Orig_Attribute', $model->attributes );
		}
	}
	*/
	
	public function afterSave($event)
	{
		$model= $event->sender;
		$logModel = new AdmincpLog();
		if ( !$model->getIsNewRecord() ) {
			if ( Yii::app()->admin->hasFlash($model->tableName(). '_Orig_Attribute') ) {
				$origData= Yii::app()->admin->getFlash($model->tableName(). '_Orig_Attribute');
				$newData= $model->attributes;
				$actionInfo = $insert = $modify = '';
				foreach ($newData as $k=> $v) {
					if ( !isset($origData[$k]) ) {
	
						$insert .= "{$model->getAttributeLabel($k)},";
					} elseif ($newData[$k]!= $origData[$k]) {
						
						$modify .= "{$model->getAttributeLabel($k)},";
					}
				}
				//print_r($origData);print_r($newData);die;
				$actionInfo .= $insert? 'bổ sung:"'. $insert: '"';
				$actionInfo .= $modify? 'sửa đổi:"'. $modify: '"';
				
			} else {
				$actionInfo = 'Cập nhật ID#'. $model->getPrimaryKey();
				
			}
			$logModel->actionRecord(AdmincpLog::ACTION_TYPE_UPDATE, array(
				'action_info'=> $actionInfo,
				'action_model'=> $model->getResourceName(),
			) );
			
		} else {
			$actionInfo = 'Thêm mới ID#'. $model->getPrimaryKey();
			$logModel->actionRecord(AdmincpLog::ACTION_TYPE_CREATE, array(
				'action_info'=> $actionInfo,
				'action_model'=> $model->getResourceName(),
			) );
		}
	}	
	
	public function afterDelete($event)
	{
		$model = $event->sender;
		$logModel = new AdmincpLog();
		$actionInfo = 'Xóa dữ liệu ID#'. $model->getPrimaryKey();
		$logModel->actionRecord(AdmincpLog::ACTION_TYPE_DELETE, array(
			'action_info'=> $actionInfo,
			'action_model'=> $model->getResourceName(),
		) );
	}
	
}
