<?php

class AdmincpLog extends AdmincpCAR {

    const ACTION_TYPE_LOGIN = 1;
    const ACTION_TYPE_CREATE = 2;
    const ACTION_TYPE_UPDATE = 3;
    const ACTION_TYPE_DELETE = 4;

    public function getResourceName() {
        return 'AdmincpLog';
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{log}}';
    }

    public function rules() {
        return array(
            array('action_type, action_info, action_controller, action_model', 'required'),
            array('action_type, user_id, status', 'numerical', 'integerOnly' => true),
            array('action_info', 'length', 'max' => 255),
            array('action_controller, action_model', 'length', 'max' => 50),
            array('remote_addr', 'length', 'max' => 20),
            array('log_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('log_id, log_time, action_type, action_info, action_controller, action_model, user_id, remote_addr, status', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'admin' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'log_id' => 'ID',
            'log_time' => 'Thời gian',
            'action_type' => 'Hành động',
            'action_info' => 'Thông tin',
            'action_controller' => 'Controller',
            'action_model' => 'Model',
            'user_id' => 'User ID',
            'remote_addr' => 'IP',
            'status' => 'Trạng thái',
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('log_id', $this->log_id);
        $criteria->compare('log_time', $this->log_time);
        $criteria->compare('action_type', $this->action_type);
        $criteria->compare('action_info', $this->action_info, true);
        $criteria->compare('action_controller', $this->action_controller, true);
        $criteria->compare('action_model', $this->action_model, true);
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('remote_addr', $this->remote_addr, true);
        $criteria->compare('status', $this->status);


        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function getActionType() {
        $array = array(
            //self::ACTION_TYPE_LOGIN  => 'Xem',
            self::ACTION_TYPE_CREATE => 'Thêm mới',
            self::ACTION_TYPE_UPDATE => 'Cập nhật',
            self::ACTION_TYPE_DELETE => 'Xóa',
        );
        return $array;
    }

    public function actionRecord($type = self::ACTION_TYPE_LOGIN, $option = array()) {
        $adminId = Yii::app()->admin->getState('user_id');
        if (User::model()->findByPk($adminId)) {
            $option['action_type'] = $type;
            $option['action_info'] = $option['action_info'];
            $option['action_model'] = $option['action_model'];
            $option['action_controller'] = Yii::app()->getController()->getRoute();
            $option['user_id'] = $adminId;
            $option['remote_addr'] = ip2long(Yii::app()->request->getUserHostAddress());
            $option['log_time'] = date('Y-m-d H:i:s');
            $this->attributes = $option;
            $this->save();
        }
    }

}
