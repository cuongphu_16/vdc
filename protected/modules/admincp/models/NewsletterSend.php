<?php
class NewsletterSend extends CFormModel
{
	public $title = 'Raodat - ';
	public $content;
	public $content_plain;
	public $listId;
	
	private $_identity;

	public function rules()
	{
		return array(
			array('title, content, content_plain, listId', 'required'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'title'=>'Tiêu đề email',
			'content'=>'Nội dung mail có định dạng',
			'content_plain'=>'Nội dung mail không định dạng',
			'listId'=>'Danh sách nhận tin',
		);
	}
}
