<?php

class AdmincpChangepassForm extends CFormModel {

    public $password_cf;
    public $password;
    public $password_old;

    public function rules() {
        return array(
            array('password_cf, password, password_old', 'required'),
        );
    }

    public function attributeLabels() {
        return array(
            'password_old' => 'Mật khẩu cũ',
            'password_cf' => 'Nhập lại mật khẩu',
            'password' => 'Mật khẩu',
        );
    }

}
