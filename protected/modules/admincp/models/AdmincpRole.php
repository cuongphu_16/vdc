<?php

class AdmincpRole extends AdmincpCAR {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'AdmincpRole';
    }

    public function tableName() {
        return '{{role}}';
    }

    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('role_name, role_label', 'required'),
            array('p_id, status, admin_use', 'numerical', 'integerOnly' => true),
            array('role_name, role_label', 'length', 'max' => 20),
            array('role_name', 'unique'),
            array('acl_desc, create_time, update_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('role_id, role_name, role_label, acl_desc, p_id, create_time, update_time, status, admin_use', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'admincpAdmins' => array(self::HAS_MANY, 'User', 'role_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'role_id' => 'ID',
            'role_name' => 'Tên',
            'role_label' => 'Nhãn',
            'acl_desc' => 'Quyền hạn',
            'p_id' => 'Nhóm gốc',
            'create_time' => 'Thời gian tạo',
            'update_time' => 'Thời gian cập nhật',
            'status' => 'Trạng thái',
            'admin_use' => 'Sử dụng admincp'
        );
    }

    public function search() {

        $criteria = new CDbCriteria;
        $criteria->compare('role_id', $this->role_id);
        $criteria->compare('role_name', $this->role_name, true);
        $criteria->compare('role_label', $this->role_label, true);
        $criteria->compare('acl_desc', $this->acl_desc, true);
        $criteria->compare('p_id', $this->p_id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('admin_use', $this->admin_use);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'role_id DESC'),
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function toOptionHash($filter = array(), $key = 'role_id', $value = 'role_label') {
        $return = array();
        $result = $this->findAllByAttributes((array) $filter);
        for ($i = 0; $i < count($result); $i++) {
            $return[$result[$i][$key]] = $result[$i][$value];
        }
        return $return;
    }

    public function getAclArrays() {
        $array = array();
        foreach ($this->findAll() as $v) {
            $array[$v->getAttribute('role_id')] = unserialize($v->getAttribute('acl_desc'));
        }
        return $array;
    }

    public function getOneAclArray($roleId = null, $filter = array()) {
        if ($roleId == null) {
            throw new CHttpException(404, 'RoleId can not be empty!');
        } else {
            $filter = array_merge($filter, array('role_id' => $roleId));
            $result = $this->findByAttributes($filter);
            if ($result->getAttribute('acl_desc') === Yii::app()->params['fullAccess']) {
                return Yii::app()->params['fullAccess'];
            } else {
                return unserialize($result->getAttribute('acl_desc'));
            }
        }
    }

    public function getRoleTreeArray($level = 0) {
        $tree = array();
        $controllers = AdmincpHelper::getControllerOptions(true);
        foreach ($controllers as $k => $v) {
            try {
                $ctl_path = dirname(dirname(__FILE__)) . '/controllers/';
                //echo $ctl_path;die;
                require_once($ctl_path . $v . '.php');
                $ctlObj = new $v(substr($v, 0, -10));
                $ctlObj->init();
                $tree[$v]['label'] = $ctlObj->controllerLabel;
                $actions = get_class_methods($v);
                foreach ($actions as $sv) {
                    if ($sv == 'actions') {
                        continue;
                    }
                    if (preg_match('/^action.*/i', $sv)) {
                        $ac = strtolower(substr($sv, 6));
                        if (!is_array($tree[$v]['actions']))
                            $tree[$v]['actions'] = array();
                        array_push($tree[$v]['actions'], $ac);
                    }else {
                        continue;
                    }
                }
            } catch (Exception $e) {
                throw new Exception('Something really gone wrong', 0, $e);
            }
        }
        return $tree;
    }

    public function formArrayToAclDesc($formArray = array()) {
        $return = array();
        if ($formArray != NULL && !empty($formArray)) {
            foreach ($formArray as $k => $v) {
                list($controller, $action) = explode('__', $k);
                $return[$controller][] = $action;
            }
        }
        return serialize($return);
    }

}
