<?php

class AdmincpNode extends AdmincpCAR {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getResourceName() {
        return 'AdmincpNode';
    }

    public function tableName() {
        return '{{node}}';
    }

    public function rules() {
        return array(
            array('p_id, sort, status', 'numerical', 'integerOnly' => true),
            array('controller', 'length', 'max' => 100),
            array('node_code', 'length', 'max' => 100),
            array('node_name', 'length', 'max' => 45),
            array('node_url', 'length', 'max' => 255),
            array('controller, node_code, node_url', 'unique'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('node_id, p_id, node_code, node_name, node_url, sort, status', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'parent' => array(self::BELONGS_TO, 'AdmincpNode', 'p_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'node_id' => 'ID',
            'p_id' => 'Menu cha',
            //'controller' => '',
            'node_code' => 'Mã',
            'node_name' => 'Tên',
            'node_url' => 'URL',
            'sort' => 'Thứ tự',
            'status' => 'Trạng thái',
        );
    }

    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        $criteria->compare('node_id', $this->node_id);
        //$criteria->compare('p_id',$this->p_id);
        if ($this->p_id) {
            $criteria->compare('p_id', $this->p_id);
            //$criteria->addCondition = "";
            //print_r(AdmincpNode::getNodeTree(false,$this->p_id));			
        }
        $criteria->compare('controller', $this->controller);
        $criteria->compare('node_code', $this->node_code, true);
        $criteria->compare('node_name', $this->node_name, true);
        $criteria->compare('node_url', $this->node_url, true);
        $criteria->compare('sort', $this->sort);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'sort ASC'),
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function getNodeTree($level = false, $pid = 0) {
        $acl = Yii::app()->admin->getState('allowActions');

        /* if($acl){
          print_r($acl);
          die();
          } */

        //MENU paramters: url,label,icon,disabled,visible,
        //sub-paramters->url: router,link,params,htmlOptions
        if ($level >= 0 || $level === false) {
            $result = $this->findAllByAttributes(array('p_id' => $pid, 'status' => 1), array('order' => 'sort asc'));
            $data = array();
            if (count($result) > 0) {
                foreach ($result as $v) {
                    $node_url = $v->getAttribute('node_url');
                    $node_name = $v->getAttribute('node_name');
                    $controller = $v->getAttribute('controller');
                    /* if($acl != Yii::app()->params['fullAccess'] && !empty($node_url) && $node_url != '#' && !empty($controller)){
                      $node_url_ = explode('/',$node_url);
                      if($acl[$controller] == NULL || !in_array(strtolower(!empty($node_url_[2]) ? $node_url_[2] : 'admin'),(array)$acl[$controller]))
                      continue;
                      } */

                    $tmp = array(
                        'url' => array(
                            'route' => $node_url
                        ),
                        'label' => $node_name,
                    );

                    $_level = $level === false ? $level : $level - 1;


                    $_pid = $v->getAttribute('node_id');
                    $children = $this->getNodeTree($_level, $_pid);
                    if (count($children) > 0) {
                        foreach ($children as $sv) {
                            array_push($tmp, $sv);
                        }
                    }

                    if ($pid === 0 && count($children) == 0) {
                        continue;
                    } else {
                        $data[$v->getAttribute('node_id')] = $tmp;
                    }
                }
                return $data;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    public function getNodeOptionsHash($level = false, $pid = 0, $hideNotActive = false, $margin = 0) {
        if ($level >= 0 || $level === false) {
            $filer = array('p_id' => $pid);
            if ($hideNotActive === true) {
                $filer = array_merge($filer, array('status' => 1));
            }

            $result = $this->findAllByAttributes($filer);
            if (count($result) > 0) {
                $options = array();
                foreach ($result as $v) {
                    $key = (int) $v->getAttribute('node_id');
                    $options[$key] = str_repeat('--- ', (int) $margin) . $v->getAttribute('node_name');
                    $_level = $level === false ? $level : $level - 1;
                    $_pid = $v->getAttribute('node_id');
                    $tmp = $this->getNodeOptionsHash($_level, $_pid, $hideNotActive, $margin + 1);
                    if (count($tmp) > 0) {
                        $options = $options + $tmp;
                    }
                }
                return $options;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    public static function getExceptionControllers() {
        return array('DefaultController', 'MainController');
    }

    public function getRemainControllers($currentController = null) {
        $exceptControllers = array();
        $collection = $this->findAll();
        foreach ($collection as $k => $v) {
            if ($v->getAttribute('controller') == $currentController)
                continue;
            if ($v->getAttribute('controller'))
                $exceptControllers[] = $v->getAttribute('controller');
        }
        return array_merge($exceptControllers, self::getExceptionControllers());
    }

}
