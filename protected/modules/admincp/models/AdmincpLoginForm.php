<?php

class AdmincpLoginForm extends CFormModel {

    public $username;
    public $password;
    public $rememberMe;
    public $verifyCode;
    private $_identity;

    public function rules() {
        return array(
            array('username, password', 'required'),
            array('password', 'authenticate'),
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }

    public function attributeLabels() {
        return array(
            'username' => 'Tên đăng nhập',
            'password' => 'Mật khẩu',
            'rememberMe' => 'Ghi nhớ đăng nhập',
            'verifyCode' => 'Mã bảo vệ',
        );
    }

    public function authenticate($attribute, $params) {
        if (!$this->hasErrors()) {
            $this->_identity = new AdminIdentity($this->username, $this->password);
            if (!$this->_identity->authenticate()) {
                switch ($this->_identity->errorCode) {
                    case AdminIdentity::ERROR_STATE_INVALID:
                        $this->addError('username', 'Tài khoản chưa được kích hoạt.');
                        break;
                    case AdminIdentity::NOT_BE_ALLOWED_ACCESS:
                        //khong duoc phep truy cap vao admincp
                        $this->addError('username', 'Thông tin đăng nhập không chính xác<!--(1)-->.');
                        break;
                    default:
                        $this->addError('password', 'Thông tin đăng nhập không chính xác<!--(2)-->.');
                        break;
                }
            }
        }
    }

    public function login() {
        if ($this->_identity === null) {
            $this->_identity = new AdminIdentity($this->username, $this->password);
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === AdminIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * AdmincpSession::REMEMBER_DAYS : 0; // 30 days
            Yii::app()->admin->login($this->_identity, $duration);

            $model = User::model()->loadByAccount($this->username);
            /* print_r($model->attributes);
              exit(); */
            $roleId = $model->getAttribute('role_id');
            $roleModel = AdmincpRole::model();
            $roleAcl = $roleModel->getOneAclArray($roleId);


            Yii::app()->admin->setState('user_id', $model->getAttribute('user_id'));
            Yii::app()->admin->setState('username', $model->getAttribute('username'));
            Yii::app()->admin->setState('area', Yii::app()->params['area']['admincp']);
            Yii::app()->admin->setState('allowActions', $roleAcl);
            Yii::app()->admin->setState('menuArray', AdmincpHelper::getMenuArray());
            //TODO set other variables.

            $model->setAttribute('last_login', date('Y-m-d H:i:s'));
            $model->save();

            return true;
        } else {
            return false;
        }
    }

}
