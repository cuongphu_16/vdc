<?php

class NodeController extends AdmincpController {

    public $layout = 'index';
    public $defaultAction = 'admin';

    /* this function must write final controller file!! */

    protected function getActionsKey() {
        return __CLASS__;
    }

    public function init() {
        parent::init();

        $this->controllerLable = 'Menu admin';
    }

    public function actionAdmin() {
        $this->setUnionTitle('Danh sách menu admin');
        $this->breadcrumbs = array(
            'Danh sách',
        );
        $model = new AdmincpNode('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['AdmincpNode']))
            $model->attributes = $_GET['AdmincpNode'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate() {

        $this->setUnionTitle('Thêm mới menu admin');
        $this->breadcrumbs = array(
            $this->controllerLabel => array('admin'),
            'Thêm mới',
        );
        $model = new AdmincpNode;
        $this->attachLogBehavior($model);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['AdmincpNode'])) {
            $model->attributes = $_POST['AdmincpNode'];
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm mới thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {

        $this->setUnionTitle('Cập nhật menu admin');
        $this->breadcrumbs = array(
            $this->controllerLabel => array('admin'),
            'Cập nhật',
        );
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['AdmincpNode'])) {
            $model->attributes = $_POST['AdmincpNode'];
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Cập nhật thành công');
                $this->redirect(array('admin'));
            }
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = $this->loadModel($id);
            $this->attachLogBehavior($model);
            $model->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionDeleteSelected() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục!';
        } else if (!is_array($ids)) {
            echo 'Yêu cầu không hợp lệ!';
        } else {
            foreach ($ids as $id) {
                $model = $this->loadModel($id);
                $this->attachLogBehavior($model);
                $model->delete();
            }
        }
        return;
    }

    public function actionPublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Hãy chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'node_id in(' . implode(',', $ids) . ')';
            $model = new AdmincpNode;
            //$this->attachLogBehavior($model);
            $model->model()->updateAll(array('status' => '1'), $criteria);
        }
        return;
    }

    public function actionUnpublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Hãy chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'node_id in(' . implode(',', $ids) . ')';
            $model = new AdmincpNode;
            //$this->attachLogBehavior($model);
            $model->model()->updateAll(array('status' => '0'), $criteria);
        }
        return;
    }

    public function loadModel($id) {
        $model = AdmincpNode::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'admincp-node-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionSortOrder() {
        $sort_input = $_POST['sort_input'];
        if (is_array($sort_input) && !empty($sort_input)) {
            foreach ($sort_input as $id => $sort) {
                $model = AdmincpNode::model()->findByPk($id);
                if (isset($model)) {
                    $model->saveAttributes(array(
                        'sort' => $sort,
                    ));
                }
            }
        }
        return;
    }

}
