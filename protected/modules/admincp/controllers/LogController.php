<?php

class LogController extends AdmincpController {

    public $layout = 'index';
    public $defaultAction = 'admin';

    /* this function must write final controller file!! */

    protected function getActionsKey() {
        return __CLASS__;
    }

    public function init() {
        parent::init();

        $this->controllerLable = 'Lịch sử truy cập';
    }

    public function actionAdmin() {
        $this->breadcrumbs = array(
            'Danh sách Lịch sử truy cập',
        );
        AdmincpHelper::unsetMenu(array('publish', 'unpublish', 'add'));

        $this->setUnionTitle('Lịch sử truy cập');
        $model = new AdmincpLog('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['AdmincpLog']))
            $model->attributes = $_GET['AdmincpLog'];

        $this->render('admin', array('model' => $model->with('admin'),));
    }

    public function actionView($id) {
        $this->setUnionTitle('Chi tiết log');

        $model = $this->loadModel($id)->with('admin');
        //print_r($model->attributes);
        $this->render('view', array(
            'model' => $model
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /* public function actionIndex()
      {
      $this->setUnionTitle('Danh sách');
      $dataProvider=new CActiveDataProvider('AdmincpLog');
      $this->render('index',array(
      'dataProvider'=>$dataProvider,
      ));
      } */

    public function actionDeleteSelected() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'log_id in(' . implode(',', $ids) . ')';
            $model = new AdmincpLog;
            $model->model()->deleteAll($criteria);
        }
        return;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = AdmincpLog::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'admincp-log-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
