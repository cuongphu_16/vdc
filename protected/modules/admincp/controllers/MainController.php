<?php

class MainController extends AdmincpController {

    public $layout = 'index';

    /* this function must write final controller file!! */

    protected function getActionsKey() {
        return __CLASS__;
    }

    public function init() {
        parent::init();
        $this->controllerLable = 'Admin';
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xF3F0E7,
                'foreColor' => 0x2040A0,
                'maxLength' => 5,
                'minLength' => 6,
                'padding' => 1,
            //'width'=>'120px',
            //'height'=>'20px',
            //'fixedVerifyCode'=>'hello',
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function getBreadcrumbs() {
        return $this->breadcrumbs = array(
            $this->getControllerLabel() => array('index'),
            $this->getActionLabel(),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $this->navbarDisabled = false;
        $this->setUnionTitle('Admin Control Panel');
        $this->render('index', array());
    }

    public function actionProfile() {
        $this->navbarDisabled = false;
        $this->menu = array(
            array('label' => 'Lưu lại', 'url' => '#', 'linkOptions' => array('submit' => array('profile'), 'onclick' => 'document.' . Yii::app()->params['adminFormName'] . '.submit(); return false;', 'class' => 'save')),
        );
        $this->setUnionTitle('Hồ sơ');
        $adminModel = new User;
        $model = $adminModel->model()->findByPk(Yii::app()->admin->user_id);
        //print_r($model);
        if (isset($_POST['User'])) {
            $valid = true;

            if (isset($_POST['chg_password'])) {
                if (empty($_POST['User']['password_cr']) && empty($_POST['User']['password']) && empty($_POST['User']['password_cf'])) {
                    $valid = false;
                    $model->addErrors(array('password_cr' => 'Mật khẩu cũ không được phép rỗng!'));
                    $model->addErrors(array('password' => 'Mật khẩu không được phép rỗng!'));
                    $model->addErrors(array('password_cf' => 'Mật khẩu xác nhận không được phép rỗng!'));
                } elseif (vH_Utils::userPass($_POST['User']['password_cr'], $model->getAttribute('salt')) != $model->getAttribute('password')) {
                    $valid = false;
                    $model->addErrors(array('password_cr' => 'Mật khẩu cũ không chính xác.'));
                } elseif ($_POST['User']['password'] != $_POST['User']['password_cf']) {
                    $valid = false;
                    $model->addErrors(array('password_cf' => 'Mật khẩu xác nhận không khớp!'));
                } else {
                    $salt = Yii::app()->params['salt'];
                    $_POST['User']['password'] = vH_Utils::userPass($_POST['User']['password'], $salt);
                    $model->setAttribute('salt', $salt);
                }
            }
            //var_dump($model->validate());
            if ($valid) {
                $model->setAttributes($_POST['User']);

                if (!empty($_FILES['User']['name']['avatar'])) {
                    $upload_dir = Yii::app()->params['uploadPath']['user'];

                    @unlink($upload_dir . $_POST['User']['avatar_hidden']);
                    $uploadedFile = vH_Utils::alias($model->username) . vH_Utils::fileEx(CUploadedFile::getInstance($model, 'avatar'), true);
                    if (!empty($uploadedFile)) {
                        $uploadedFile = vH_Upload::uploadFile('User', 'avatar', $upload_dir, $uploadedFile);
                        $thumb = Yii::app()->phpThumb->create($upload_dir . $uploadedFile)->resize(80, 500)->save($upload_dir . $uploadedFile);
                        $model->setAttribute('avatar', $uploadedFile);
                    }
                }
                $model->setAttribute('update_time', date('Y-m-d h:i:s'));
                //$_POST['User']['password']= md5($_POST['User']['password']);
                //$model->attributes = $_POST['User'];
                if ($model->save()) {
                    Yii::app()->admin->putSuccessMsg('Cập nhật thành công!');
                    Yii::app()->admin->setState('name', $model->getAttribute('username'));
                    $this->refresh();
                }
            }
        }
        //$model->setAttribute('password', '******');
        $this->render('profile', array('model' => $model));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        $this->layout = 'nonav';
        $this->setUnionTitle('Lỗi');
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $this->setUnionTitle('Đăng nhập hệ thống');
        $this->layout = 'nonav';
        if (!Yii::app()->admin->getIsGuest()) {
            if (!isset(Yii::app()->session['admin_exist']) || Yii::app()->session['admin_exist'] != 'true') {
                $model = User::model()->loadByAccount(Yii::app()->admin->id);
                /* print_r($model->attributes);
                  exit(); */
                if ($model != NULL) {
                    $roleId = $model->getAttribute('role_id');
                    $roleModel = AdmincpRole::model();
                    $roleAcl = $roleModel->getOneAclArray($roleId);


                    Yii::app()->admin->setState('user_id', $model->getAttribute('user_id'));
                    Yii::app()->admin->setState('username', $model->getAttribute('username'));
                    Yii::app()->admin->setState('area', Yii::app()->params['area']['admincp']);
                    Yii::app()->admin->setState('allowActions', $roleAcl);
                    Yii::app()->admin->setState('menuArray', AdmincpHelper::getMenuArray());
                    //TODO set other variables.

                    $model->setAttribute('last_login', date('Y-m-d H(idea)(worry)'));
                    $model->save();
                    Yii::app()->session['admin_exist'] = 'true';
                }
            }
            $this->redirect(array('index'));
        }


        $model = new AdmincpLoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['AdmincpLoginForm'])) {
            /* @var $model CFormModel */
            $model->attributes = $_POST['AdmincpLoginForm'];
            // validate user input and redirect to the previous page if valid
            $model->rememberMe = $_POST['AdmincpLoginForm']['rememberMe'];
            if ($model->validate() && $model->login()) {
                if (isset($_GET['referrer'])) {
                    $r = Yii::app()->createAbsoluteUrl(base64_decode($_GET['referrer']));
                } else {
                    $r = Yii::app()->admin->returnUrl;
                }
                $this->redirect($r);
            }
        }
        // display the login form
        $model->password = null;
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        $this->setUnionTitle('Thoát đăng nhập hệ thống');
        $this->layout = 'nonav';
        Yii::app()->admin->logout();
        //$this->redirect(Yii::app()->homeUrl);
        $this->render('logout');
    }

    public function actionNotAllowed() {
        $message = 'Bạn không đủ quyền để làm việc này';
        if (isset($_POST['ajax'])) {
            echo CJSON::encode(array('status' => 0, 'message' => $message));
        } else {
            //$this->setUnionTitle('');
            $this->render('notAllowed', array('message' => $message, 'returnUrl' => base64_decode($_GET['returnUrl'])));
        }
    }

    public function actionLoadTypeCat() {
        $language = (int) $_REQUEST['Category']['language'];
        $catModel = Category::model();
        $html = '<option value="">Chọn danh mục</option>';
        if ($language > 0) {
            $return = $catModel->getTypeCatOptionsTree(0, 0, 0, $language);
            if (!empty($return)) {
                foreach ($return as $k => $v) {
                    $html.= '<option value="' . $k . '">' . $v . '</option>';
                }
            }
        } else {
            $return = $catModel->getTypeCatOptionsTree(0, 0, 0, 0);
            if (!empty($return)) {
                foreach ($return as $k => $v) {
                    $html.= '<option value="' . $k . '">' . $v . '</option>';
                }
            }
        }
        echo $html;
        return;
    }

    public function actionLoadCatePost() {
        $language = (int) $_REQUEST['Post']['language'];
        $catModel = Category::model();
        $html = '<option value="">Chọn danh mục</option>';
        if ($language > 0) {
            $return = $catModel->getTypeCatOptionsTree(0, 0, 0, $language);
            if (!empty($return)) {
                foreach ($return as $k => $v) {
                    $html.= '<option value="' . $k . '">' . $v . '</option>';
                }
            }
        } else {
            $return = $catModel->getTypeCatOptionsTree(0, 0, 0, 0);
            if (!empty($return)) {
                foreach ($return as $k => $v) {
                    $html.= '<option value="' . $k . '">' . $v . '</option>';
                }
            }
        }
        echo $html;
        return;
    }

    public function actionLoadCateAbout() {
        $language = (int) $_REQUEST['About']['language'];
        $catModel = Category::model();
        $html = '<option value="">-- Danh mục giới thiệu --</option>';
        if ($language > 0) {
            $return = $catModel->getTypeCatOptionsTree(Category::TYPE_ABOUT, 0, $language);
            if (!empty($return)) {
                foreach ($return as $k => $v) {
                    $html.= '<option value="' . $k . '">' . $v . '</option>';
                }
            }
        } else {
            $return = $catModel->getTypeCatOptionsTree(Category::TYPE_ABOUT, 0, 1);
            if (!empty($return)) {
                foreach ($return as $k => $v) {
                    $html.= '<option value="' . $k . '">' . $v . '</option>';
                }
            }
        }
        echo $html;
        return;
    }

    public function actionLoadCateServices() {
        $language = (int) $_REQUEST['Services']['language'];
        $catModel = Category::model();
        $html = '<option value="">-- Danh mục dịch vụ --</option>';
        if ($language > 0) {
            $return = $catModel->getTypeCatOptionsTree(Category::TYPE_SERVICES, 0, $language);
            if (!empty($return)) {
                foreach ($return as $k => $v) {
                    $html.= '<option value="' . $k . '">' . $v . '</option>';
                }
            }
        } else {
            $return = $catModel->getTypeCatOptionsTree(Category::TYPE_SERVICES, 0, 1);
            if (!empty($return)) {
                foreach ($return as $k => $v) {
                    $html.= '<option value="' . $k . '">' . $v . '</option>';
                }
            }
        }
        echo $html;
        return;
    }

    public function actionLoadCateProject() {
        $language = (int) $_REQUEST['Project']['language'];
        $catModel = Category::model();
        $html = '<option value="">-- Danh mục dự án --</option>';
        if ($language > 0) {
            $return = $catModel->getTypeCatOptionsTree(Category::TYPE_PROJECT, 0, $language);
            if (!empty($return)) {
                foreach ($return as $k => $v) {
                    $html.= '<option value="' . $k . '">' . $v . '</option>';
                }
            }
        } else {
            $return = $catModel->getTypeCatOptionsTree(Category::TYPE_PROJECT, 0, 1);
            if (!empty($return)) {
                foreach ($return as $k => $v) {
                    $html.= '<option value="' . $k . '">' . $v . '</option>';
                }
            }
        }
        echo $html;
        return;
    }

    public function actionShowUploadImg() {
        $width = 30;
        $height = 30;
        if ((int) $_REQUEST['w'] > 0)
            $width = $_REQUEST['w'];

        if ((int) $_REQUEST['h'] > 0)
            $height = $_REQUEST['h'];

        $upload_folder = 'upload/';
        if (!file_exists($upload_folder . $_REQUEST['src']))
            return false;
        header("Cache-Control: no-cache, must-revalidate");
        Yii::app()->phpThumb->create($upload_folder . $_REQUEST['src'])->resize($width, $height)->show();
    }

    protected function beforeAction($action) {
        /* @var CWebApplication */
        if (!$this->isExceptAction() && Yii::app()->admin->getIsGuest()) {
            $this->redirect(array('main/login'));
        }
        if (!$this->isExceptAction() && Yii::app()->admin->getState('area') !== Yii::app()->params['area']['admincp']) {
            $this->redirect(array('main/login'));
        } else {
            return true;
        }
    }

    public function actionLoginiframe() {
        $id = (int) $_GET['id'];
        $order = Orders::model()->findByPk($id);
        $this->renderPartial('login_iframe', array('order' => $order));
    }

    public function actionLoadiframe() {
        $id = (int) $_POST['id'];
        $order = Orders::model()->findByPk($id);
        echo '<div class="customHTML" title="Click và giữ chuột để xem thông tin">Xem thông tin đăng nhập</div><span class="view"></span><iframe src="' . Yii::app()->request->baseUrl . '/admincp/main/loginiframe?id=' . $id . ' frameborder="0" width="100%" height="100%" align="middle"></iframe>';
    }

    public function actionGetinfo() {
        $id = (int) $_POST['id'];
        $info = Orders::model()->findByPk($id);
        $crypt = new Crypt(Yii::app()->params['ENCRYPTION_KEY']);
        if (!empty($info)):
            echo "Username: {$info->camera_username} - Password: " . $crypt->decrypt($info->camera_password);
        endif;
    }

    public function actionLoadDistrict() {
        $regionId = (int) $_REQUEST['Ward']['region_id'];
        echo SiteHelper::loadDistrictOptions($regionId);
        return;
    }

    public function actionLoadRegionsWard() {
        $regionId = (int) $_REQUEST['Ward']['country_id'];
        echo SiteHelper::loadRegionsOptions($regionId);
        return;
    }

    public function actionLoadRegionsDistrict() {
        $regionId = (int) $_REQUEST['District']['country_id'];
        echo SiteHelper::loadRegionsOptions($regionId);
        return;
    }

    public function actionLoadDistrictUser() {
        $regionId = (int) $_REQUEST['User']['region_id'];
        echo SiteHelper::loadDistrictOptions($regionId);
        return;
    }

    public function actionLoadRegionsUser() {
        $regionId = (int) $_REQUEST['User']['country_id'];
        echo SiteHelper::loadRegionsOptions($regionId);
        return;
    }

    public function actionLoadBornDistrictUser() {
        $regionId = (int) $_REQUEST['User']['born_region_id'];
        echo SiteHelper::loadDistrictOptions($regionId);
        return;
    }

    public function actionLoadBornRegionsUser() {
        $regionId = (int) $_REQUEST['User']['born_country_id'];
        echo SiteHelper::loadRegionsOptions($regionId);
        return;
    }

}
