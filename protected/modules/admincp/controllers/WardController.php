<?php

class WardController extends AdmincpController {

    public $layout = 'index';
    public $defaultAction = 'admin';

    protected function getActionsKey() {
        return __CLASS__;
    }

    public function init() {
        parent::init();
        $this->controllerLable = 'Phường / Xã';
    }

    public function actionIndex() {
        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $this->setUnionTitle('Danh sách Phường / Xã');
        $this->breadcrumbs = array(
            'Danh sách Phường / Xã',
        );

        $model = new Ward('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Ward']))
            $model->attributes = $_GET['Ward'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /* public function actionView($id)
      {
      $this->render('view',array(
      'model'=>$this->loadModel($id),
      ));
      } */

    public function actionCreate() {
        $this->setUnionTitle('Thêm mới Phường / Xã');
        $model = new Ward('create');
        $this->attachLogBehavior($model);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Ward'])) {
            $model->attributes = $_POST['Ward'];

            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $this->setUnionTitle('Chỉnh sửa Phường / Xã');

        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Ward'])) {
            $model->attributes = $_POST['Ward'];
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);
        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionPublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'ward_id in(' . implode(',', $ids) . ')';
            $model = Ward::model()->updateAll(array('status' => '1'), $criteria);
            //$this->attachLogBehavior($model);
        }
        return;
    }

    public function actionUnpublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'ward_id in(' . implode(',', $ids) . ')';
            $model = Ward::model()->updateAll(array('status' => '0'), $criteria);
            //$this->attachLogBehavior($model);
        }
        return;
    }

    public function actionDeleteSelected() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục!';
        } else if (!is_array($ids)) {
            echo 'Yêu cầu không hợp lệ!';
        } else {
            foreach ($ids as $id) {
                $model = $this->loadModel($id);
                $this->attachLogBehavior($model);
                $model->delete();
            }
        }
        return;
    }

    public function loadModel($id) {
        $model = Ward::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'ward-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
