<?php

class AdvsController extends AdmincpController {

    public $layout = 'index';
    public $defaultAction = 'admin';

    protected function getActionsKey() {
        return __CLASS__;
    }

    public function init() {
        parent::init();
        $this->controllerLable = 'Quản lý Slide';
    }

    public function actionAdmin() {
        $this->setUnionTitle('Danh sách Slide');
        $this->breadcrumbs = array(
            'Danh sách Slide'
        );
        $model = new Advs('search');
        $model->unsetAttributes();  // clear any default values
        if (intval(Yii::app()->request->getParam('clearFilters')) == 1) {
            EButtonColumnWithClearFilters::clearFilters($this, $model); //where $this is the controller
        }
        if (isset($_GET['Advs']))
            $model->attributes = $_GET['Advs'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate() {

        $this->setUnionTitle('Thêm mới Slide ');
        $model = new Advs('create');
        $this->attachLogBehavior($model);

        if (isset($_POST['Advs'])) {
            $model->attributes = $_POST['Advs'];

            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $this->setUnionTitle('Chỉnh sửa Slide');

        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Advs'])) {
            $model->attributes = $_POST['Advs'];
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);
        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    //hien thi tin
    public function actionPublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'advs_id in(' . implode(',', $ids) . ')';
            $model = Advs::model()->updateAll(array('status' => '1'), $criteria);
        }
        return;
    }

    //an hien thi
    public function actionUnpublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'advs_id in(' . implode(',', $ids) . ')';
            $model = Advs::model()->updateAll(array('status' => '0'), $criteria);
        }
        return;
    }

    public function actionDeleteSelected() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục!';
        } else if (!is_array($ids)) {
            echo 'Yêu cầu không hợp lệ!';
        } else {
            foreach ($ids as $id) {
                $model = $this->loadModel($id);
                $this->attachLogBehavior($model);
                $model->delete();
            }
        }
        return;
    }

    public function loadModel($id) {
        $model = Advs::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'ads-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
