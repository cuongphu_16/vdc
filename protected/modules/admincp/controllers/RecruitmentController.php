<?php

class RecruitmentController extends AdmincpController {

    public $layout = 'index';
    public $defaultAction = 'admin';

    protected function getActionsKey() {
        return __CLASS__;
    }

    public function init() {
        parent::init();
        $this->controllerLable = 'Tuyển dụng';
    }

    public function actionAdmin() {
        $this->breadcrumbs = array(
            'Danh sách Tuyển dụng',
        );
        unset(Yii::app()->session['recruitment_upload_folder']);
        unset(Yii::app()->session['news_upload_image']);
        $this->setUnionTitle('Danh sách Tuyển dụng');
        $model = new Recruitment('search');
//        $model->unsetAttributes();  // clear any default values
        $model->setRememberScenario(array_keys($model->attributes));
        if (intval(Yii::app()->request->getParam('clearFilters')) == 1) {
            EButtonColumnWithClearFilters::clearFilters($this, $model); //where $this is the controller
        }
        if (isset($_GET['Recruitment']))
            $model->attributes = $_GET['Recruitment'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $this->setUnionTitle('Chỉnh sửa Tuyển dụng');
        $this->breadcrumbs = array(
            $this->controllerLabel => array('admin'),
            'Cập nhật',
        );
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $model->setScenario('not_index');
        if (isset($_POST['Recruitment'])) {
            $model->attributes = $_POST['Recruitment'];
            if ($_POST['Recruitment']['renew'] == 'on') {
                $model->updated_at = date('Y-m-d H:i:s', time());
            }
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionCreate() {
        $this->breadcrumbs = array(
            $this->controllerLabel => array('admin'),
            'Thêm mới',
        );
        $this->setUnionTitle('Thêm mới Tuyển dụng');
        $model = new Recruitment('not_index');
        $this->attachLogBehavior($model);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Recruitment'])) {
            $model->attributes = $_POST['Recruitment'];
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);
        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionPublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'recruitment_id in(' . implode(',', $ids) . ')';
            $model = Recruitment::model()->updateAll(array('status' => '1'), $criteria);
            //$this->attachLogBehavior($model);
        }
        return;
    }

    public function actionUnpublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'recruitment_id in(' . implode(',', $ids) . ')';
            $model = Recruitment::model()->updateAll(array('status' => '0'), $criteria);
            //$this->attachLogBehavior($model);
        }
        return;
    }

    public function actionDeleteSelected() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục!';
        } else if (!is_array($ids)) {
            echo 'Yêu cầu không hợp lệ!';
        } else {
            foreach ($ids as $id) {
                $model = $this->loadModel($id);
                $this->attachLogBehavior($model);
                $model->delete();
            }
        }
        return;
    }

    public function loadModel($id) {
        $model = Recruitment::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'recruitment-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionDeleteImage() {
        if (isset($_POST['id']) && $_POST['id'] > 0) {
            $about = Recruitment::model()->findByAttributes(array('recruitment_id' => $_POST['id']));
            $about->image = "";
            $about->save();
        }
    }

}
