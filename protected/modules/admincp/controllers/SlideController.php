<?php

class SlideController extends AdmincpController {

    public $layout = 'index';
    public $defaultAction = 'admin';

    protected function getActionsKey() {
        return __CLASS__;
    }

    public function init() {
        parent::init();
        $this->controllerLable = 'Hình nền';
    }

    public function actionAdmin() {
        $this->breadcrumbs = array(
            'Danh sách Hình nền',
        );
        $this->setUnionTitle('Hình nền');
        $model = new Slide('search');
//        $model->unsetAttributes();  // clear any default values
        //filter remember
        $model->setRememberScenario(array_keys($model->attributes));
        if (intval(Yii::app()->request->getParam('clearFilters')) == 1) {
            EButtonColumnWithClearFilters::clearFilters($this, $model); //where $this is the controller
        }
        //end filter remember

        if (isset($_GET['Slide'])) {
            $model->attributes = $_GET['Slide'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate() {
        $this->breadcrumbs = array(
            $this->controllerLabel => array('admin'),
            'Thêm mới',
        );
        $this->setUnionTitle('Thêm mới hình nền');
        $model = new Slide('create');
        $this->attachLogBehavior($model);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Slide'])) {
            $model->attributes = $_POST['Slide'];

            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $this->setUnionTitle('Cập nhật hình nền');
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);
        $this->breadcrumbs = array(
            $this->controllerLabel => array('admin'),
            'Cập nhật',
        );
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Slide'])) {
            $model->attributes = $_POST['Slide'];
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Cập nhật thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    //xoa 1 tin
    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);
        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    //hien thi tin
    public function actionPublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'slide_id in(' . implode(',', $ids) . ')';
            $model = Slide::model()->updateAll(array('status' => '1'), $criteria);
        }
        return;
    }

    //an hien thi
    public function actionUnpublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'slide_id in(' . implode(',', $ids) . ')';
            $model = Slide::model()->updateAll(array('status' => '0'), $criteria);
        }
        return;
    }

    public function actionDeleteSelected() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục!';
        } else if (!is_array($ids)) {
            echo 'Yêu cầu không hợp lệ!';
        } else {
            foreach ($ids as $id) {
                $model = $this->loadModel($id);
                $this->attachLogBehavior($model);
                $model->delete();
            }
        }
        return;
    }

    public function loadModel($id) {
        $model = Slide::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'slide-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionSortOrder() {
        $sort_input = $_POST['sort_input'];
        if (is_array($sort_input) && !empty($sort_input)) {
            foreach ($sort_input as $id => $sort) {
                $model = Slide::model()->findByPk($id);
                if (isset($model)) {
                    $model->saveAttributes(array(
                        'sort_order' => $sort,
                    ));
                }
            }
        }
        return;
    }

}
