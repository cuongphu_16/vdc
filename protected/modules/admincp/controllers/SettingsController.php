<?php

class SettingsController extends AdmincpController {

    public $layout = 'index';

    protected function getActionsKey() {
        return __CLASS__;
    }

    public function init() {
        parent::init();
        $this->controllerLable = 'Cấu hình website';
    }

    public function actionAdmin() {
        $this->setUnionTitle('Cấu hình website');
        $this->breadcrumbs = array(
            'Cấu hình website'
        );
        $model = new Settings('search');
        $model->unsetAttributes();
        if (isset($_GET['Settings']))
            $model->attributes = $_GET['Settings'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate() {
        $this->setUnionTitle('Thêm mới');
        $this->breadcrumbs = array(
            $this->controllerLabel => array('admin'),
            'Thêm mới',
        );
        $model = new Settings('create');
        $this->attachLogBehavior($model);

        if (isset($_POST['Settings'])) {
            $model->attributes = $_POST['Settings'];
            if ($model->getAttribute('key') == 'admindir') {
                $f = fopen('admindir.txt', 'w+');
                $admin_dir = fputs($f, trim(strip_tags($model->value)));
                fclose($f);
            }
            /* if($model->isnumber == 1)
              Controller::settingSet($model->key,(int)$model->value);
              else
              Controller::settingSet($model->key,$model->value); */
            $model->setAttribute('update_time', date('Y-m-d h:i:s'));
            $model->setAttribute('update_by', Yii::app()->admin->user_id);
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $this->breadcrumbs = array(
            $this->controllerLabel => array('admin'),
            'Cập nhật',
        );
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);
        $model->value = unserialize($model->value);

        if (isset($_POST['Settings'])) {
            $model->attributes = $_POST['Settings'];
            if ($model->getAttribute('key') == 'admindir') {
                $f = fopen('admindir.txt', 'w+');
                $admin_dir = fputs($f, trim(strip_tags($model->value)));
                fclose($f);
            }
            /* if($model->isnumber == 1)
              Controller::settingSet($model->key,(int)$model->value);
              else
              Controller::settingSet($model->key,$model->value); */

            $model->attributes = $_POST['Settings'];
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Cập nhật thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /* public function actionEdittable()
      {
      $id = (int)$_POST['id'];
      if($id <=0 )
      return false;

      $value = trim($_POST['value']);
      $model = $this->loadModel($id);
      $this->attachLogBehavior($model);
      $model->setAttribute('value', $value);
      $model->save();
      echo $value;
      } */

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = $this->loadModel($id);
            $this->attachLogBehavior($model);
            $model->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionDeleteSelected() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục!';
        } else if (!is_array($ids)) {
            echo 'Yêu cầu không hợp lệ!';
        } else {
            foreach ($ids as $id) {
                $model = $this->loadModel($id);
                $this->attachLogBehavior($model);
                $model->delete();
            }
        }
        return;
    }

    public function loadModel($id) {
        $model = Settings::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'settings-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function ftp_putAll($conn_id, $src_dir, $dst_dir, $user_id) {
        $d = dir($src_dir);
        $html = '';
        while ($file = $d->read()) { // do this for each file in the directory
            if ($file != "." && $file != "..") { // to prevent an infinite loop
                if (is_dir($src_dir . "/" . $file)) { // do the following if it is a directory
                    if (!@ftp_chdir($conn_id, $dst_dir . "/" . $file)) {
                        ftp_mkdir($conn_id, $dst_dir . "/" . $file); // create directories that do not yet exist
                    }
                    self::ftp_putAll($conn_id, $src_dir . "/" . $file, $dst_dir . "/" . $file, $user_id); // recursive part
                } else {
                    //$upload = ftp_put($conn_id, $dst_dir . "/" . $file, $src_dir . "/" . $file, FTP_BINARY); // put the files
                    $ch = curl_init();
                    $web = Yii::app()->params['ftp_config']['ftp_ip'];
                    $user = Yii::app()->params['ftp_config']['ftp_user'];
                    $pass = Yii::app()->params['ftp_config']['ftp_pass'];
                    $localfile = $src_dir . "/" . $file;
                    $fp = fopen($localfile, 'r');
                    curl_setopt($ch, CURLOPT_URL, 'ftp://' . $web . '/' . $dst_dir . "/" . $file);
                    curl_setopt($ch, CURLOPT_USERPWD, "$user:$pass");
                    curl_setopt($ch, CURLOPT_UPLOAD, 1);
                    curl_setopt($ch, CURLOPT_INFILE, $fp);
                    curl_setopt($ch, CURLOPT_INFILESIZE, filesize($localfile));
                    curl_exec($ch);
                    $error_no = curl_errno($ch);
                    curl_close($ch);
                    $pathinfo = pathinfo($file);
                    if ($error_no == 0) {
                        $video = new Videos;
                        $video->title = $pathinfo['filename'];
                        $video->source = $dst_dir . "/" . $file;
                        $video->user_id = $user_id;
                        $video->sizefile = @filesize($dst_dir . "/" . $file);
                        $video->created_at = date('Y-m-d H:i:s');
                        if ($video->save()) {

                            $html .= '<p>' . $file . ' Uploaded Success</p>';
                        } else {
                            print_r($video->getErrors());
                            die;
                        }
                    } else {
                        $html .= '<p>' . $file . ' Failed Success</p>';
                    }
                }
            }
        }
        return $html;
        $d->close();
    }

    protected function ftp_is_dir($conn_id, $dir) {
        // Get the current working directory 
        $origin = ftp_pwd($conn_id);

        // Attempt to change directory, suppress errors 
        if (@ftp_chdir($conn_id, $dir)) {
            // If the directory exists, set back to origin 
            ftp_chdir($conn_id, $origin);
            return true;
        }

        // Directory does not exist 
        return false;
    }

    public function actionOther() {
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);
        $this->setUnionTitle('Cấu hình khác');
        $this->menu[] = array('label' => 'Lưu lại', 'url' => '#', 'linkOptions' => array('submit' => array('update'), 'onclick' => 'document.' . Yii::app()->params['adminFormName'] . '.submit(); return false;', 'class' => 'save'));
        if (isset($_POST['folder_from'])) {
            // connect and login data
            $web = Yii::app()->params['ftp_config']['ftp_ip'];
            $user = Yii::app()->params['ftp_config']['ftp_user'];
            $pass = Yii::app()->params['ftp_config']['ftp_pass'];
            // file location
            $conn_id = ftp_connect($web);
            $login_result = ftp_login($conn_id, $user, $pass);
            $folder_to = trim($_POST['folder_to']);
            $user_id = trim($_POST['user_id']);
            if (!self::ftp_is_dir($conn_id, $folder_to)) {
                ftp_mkdir($conn_id, $folder_to);
            }
            $html = self::ftp_putAll($conn_id, trim($_POST['folder_from']), $folder_to, $user_id);
        }

        $this->render('other', array('html' => $html));
    }

    public function actionSearchMember() {
        $keywords = trim($_POST['search']);
        $criteria = new CDbCriteria();
        if (!empty($keywords))
            $criteria->addCondition("(fullname like '%$keywords%' OR address like '%$keywords%')");
        $users = User::model()->findAllByAttributes(array(), $criteria);
        $this->renderPartial('result', array('users' => $users, 'keywords' => $keywords));
    }
    public function actionDeleteAssets() {
        $dir = dirname($_SERVER['SCRIPT_FILENAME']) . DIRECTORY_SEPARATOR . 'assets/';
        AdmincpHelper::rrmdir__($dir);
        $this->redirect(array('admin'));
    }

}
