<?php

class RoleController extends AdmincpController {

    public $layout = 'index';

    /* this function must write final controller file!! */

    protected function getActionsKey() {
        return __CLASS__;
    }

    public function init() {
        parent::init();

        $this->controllerLable = 'Nhóm người dùng';
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $this->breadcrumbs = array(
            'Danh sách',
        );
        $this->setUnionTitle('Danh sách nhóm người dùng');
        $model = new AdmincpRole('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['AdmincpRole']))
            $model->attributes = $_GET['AdmincpRole'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /* public function actionView($id)
      {
      $this->redirect(array('admin'));
      $this->setUnionTitle('Chi tiết');
      $this->render('view',array(
      'model'=>$this->loadModel($id),
      ));
      } */

    public function actionCreate() {
        $this->setUnionTitle('Thêm mới nhóm người dùng');
        $model = new AdmincpRole;
        $this->attachLogBehavior($model);
        $this->breadcrumbs = array(
            $this->controllerLabel => array('admin'),
            'Thêm mới',
        );
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['AdmincpRole'])) {
            if (isset($_POST['AdmincpRole']['acl_desc']) && $_POST['AdmincpRole']['acl_desc'] == '') {
                unset($_POST['AdmincpRole']['acl_desc']);
            } elseif ($_POST['AdmincpRole']['acl_desc'] !== Yii::app()->params['fullAccess'] && $_POST['AdmincpRole']['acl_desc'] !== 'null') {
                $formArray = json_decode($_POST['AdmincpRole']['acl_desc']);
                $_POST['AdmincpRole']['acl_desc'] = $model->formArrayToAclDesc($formArray);
            }
            $model->attributes = $_POST['AdmincpRole'];
            $model->setAttribute('create_time', date('Y-m-d h:i:s'));
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm thành công nhóm người dùng <strong>' . $model->attributes['role_name'] . '</strong>');
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $this->setUnionTitle('Cập nhật');
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);
        $this->breadcrumbs = array(
            $this->controllerLabel => array('admin'),
            'Cập nhật',
        );
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['AdmincpRole'])) {
            if (isset($_POST['AdmincpRole']['acl_desc']) && $_POST['AdmincpRole']['acl_desc'] == '') {
                unset($_POST['AdmincpRole']['acl_desc']);
            } elseif ($_POST['AdmincpRole']['acl_desc'] !== Yii::app()->params['fullAccess'] && $_POST['AdmincpRole']['acl_desc'] !== 'null') {
                $formArray = json_decode($_POST['AdmincpRole']['acl_desc']);
                $_POST['AdmincpRole']['acl_desc'] = $model->formArrayToAclDesc($formArray);
            }

            if ($id) {
                $_POST['AdmincpRole']['update_time'] = date('Y-m-d H:i:s');
            } else {
                $_POST['AdmincpRole']['create_time'] = date('Y-m-d H:i:s');
            }
            $model->attributes = $_POST['AdmincpRole'];
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Cập nhật thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = $this->loadModel($id);
            $this->attachLogBehavior($model);
            $model->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionDeleteSelected() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục!';
        } else if (!is_array($ids)) {
            echo 'Yêu cầu không hợp lệ!';
        } else {
            foreach ($ids as $id) {
                $model = $this->loadModel($id);
                $this->attachLogBehavior($model);
                $model->delete();
            }
        }
        return;
    }

    public function actionPublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'role_id in(' . implode(',', $ids) . ')';
            $model = AdmincpRole::model()->updateAll(array('status' => '1'), $criteria);
            //$this->attachLogBehavior($model);
        }
        return;
    }

    public function actionUnpublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'role_id in(' . implode(',', $ids) . ')';
            $model = AdmincpRole::model()->updateAll(array('status' => '0'), $criteria);
            //$this->attachLogBehavior($model);
        }
        return;
    }

    /* public function actionIndex()
      {
      $this->redirect(array('admin'));
      $this->setUnionTitle('...');
      $dataProvider=new CActiveDataProvider('AdmincpRole');
      $this->render('index',array(
      'dataProvider'=>$dataProvider,
      ));
      } */

    public function loadModel($id) {
        $model = AdmincpRole::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'admincp-role-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
