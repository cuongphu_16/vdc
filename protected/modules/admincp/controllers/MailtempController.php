<?php

class MailtempController extends AdmincpController
{
	public $layout='index';
	
	protected function getActionsKey()
	{
		return __CLASS__;
	}
	
	public function init()
	{
		parent::init();
		$this->controllerLable = 'Mail template';
	}
	
	
	public function actionAdmin()
	{
		$this->setUnionTitle('Mail template');
		$model = new Mailtemp('search');
		$model->unsetAttributes();
		if(isset($_GET['Mailtemp']))
			$model->attributes = $_GET['Mailtemp'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	
	public function actionCreate()
	{
		$this->setUnionTitle('Thêm mới');
		
		$model = new Mailtemp('create');
		$this->attachLogBehavior($model);

		if(isset($_POST['Mailtemp']))
		{
			$model->attributes = $_POST['Mailtemp'];
			if($model->getAttribute('key') == 'admindir'){
				$f = fopen('admindir.txt','w+');
				$admin_dir = fputs($f,trim(strip_tags($model->value)));
				fclose($f);
			}
			/*if($model->isnumber == 1)
				Controller::settingSet($model->key,(int)$model->value);
			else
				Controller::settingSet($model->key,$model->value);*/
			$model->setAttribute('update_time',date('Y-m-d h:i:s'));
			$model->setAttribute('update_by', Yii::app()->admin->user_id);
			if($model->save()){
				Yii::app()->admin->putSuccessMsg('Thêm thành công!');
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$this->attachLogBehavior($model);
		$model->value = unserialize($model->value);
			
		if(isset($_POST['Mailtemp']))
		{
			$model->attributes=$_POST['Mailtemp'];
			if($model->getAttribute('key') == 'admindir'){
				$f = fopen('admindir.txt','w+');
				$admin_dir = fputs($f,trim(strip_tags($model->value)));
				fclose($f);
			}
			/*if($model->isnumber == 1)
				Controller::settingSet($model->key,(int)$model->value);
			else
				Controller::settingSet($model->key,$model->value);*/
			
			$model->attributes=$_POST['Mailtemp'];
			if($model->save()){
				Yii::app()->admin->putSuccessMsg('Cập nhật thành công!');
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	/*public function actionEdittable()
	{
		$id = (int)$_POST['id'];
		if($id <=0 )
			return false;
		
		$value = trim($_POST['value']);
		$model = $this->loadModel($id);
		$this->attachLogBehavior($model);
		$model->setAttribute('value', $value);
		$model->save();
		echo $value;
	}*/
	
	
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = $this->loadModel($id);
			$this->attachLogBehavior($model);
			$model->delete();
			
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	
	public function loadModel($id)
	{
		$model=Mailtemp::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mailtemplate-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
