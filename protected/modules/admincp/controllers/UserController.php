<?php

class UserController extends AdmincpController {

    public
            $layout = 'index',
            $defaultAction = 'admin',
            $valid = true;

    protected function getActionsKey() {
        return __CLASS__;
    }

    public function init() {
        parent::init();
        $this->controllerLable = 'Thành viên, Người dùng';
    }

    public function actionIndex() {
        $this->redirect(array('admin'));
    }

    public function actionAdmin() {
        $this->breadcrumbs = array(
            'Danh sách Thành viên, Người dùng',
        );
        $this->setUnionTitle('Danh sách người dùng');

        $model = new User('search');
        //$model->unsetAttributes();  // clear any default values 
        //filter remember
        $model->setRememberScenario(array_keys($model->attributes));
        if (intval(Yii::app()->request->getParam('clearFilters')) == 1) {
            EButtonColumnWithClearFilters::clearFilters($this, $model); //where $this is the controller
        }
        //end filter remember

        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate() {
        $this->setUnionTitle('Thêm mới người dùng');
        $this->breadcrumbs = array(
            $this->controllerLabel => array('admin'),
            'Thêm mới',
        );
        $model = new User('backcreate');
        $this->attachLogBehavior($model);

        // Uncomment the following line if AJAX validation is needed 
        // $this->performAjaxValidation($model); 

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if (!empty($model->package_storage))
                $model->package_storage = SiteHelper::calSizeUnits($model->package_storage);
            if ($_POST['User']['job_category'] != NULL && is_array($_POST['User']['job_category']))
                $model->job_category = ',' . implode(",", $_POST['User']['job_category']) . ',';
            if ($_POST['User']['work_place'] != NULL && is_array($_POST['User']['work_place']))
                $model->work_place = ',' . implode(",", $_POST['User']['work_place']) . ',';
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $this->setUnionTitle('Chỉnh sửa thông tin người dùng');
        $this->breadcrumbs = array(
            $this->controllerLabel => array('admin'),
            'Cập nhật',
        );
        $model = $this->loadModel($id);
        $model->setScenario('backupdate');
        //$usertype = $model->type;
        if (isset($_POST['User'])) {
            $model->setAttributes($_POST['User']);
            if ((int) $_POST['User']['chg_password'] == 1) {
                if (empty($_POST['User']['password'])) {
                    $this->valid = false;
                    $model->addErrors(array('password' => 'Mật khẩu không được phép rỗng!'));
                }
                if (empty($_POST['User']['password_cf'])) {
                    $this->valid = false;
                    $model->addErrors(array('password_cf' => 'Mật khẩu xác nhận không được phép rỗng!'));
                } elseif ($_POST['User']['password'] != $_POST['User']['password_cf']) {
                    $this->valid = false;
                    $model->addErrors(array('password_cf' => 'Mật khẩu xác nhận không đúng!'));
                } else {
                    $salt = Yii::app()->params['salt'];
                    $_POST['User']['password'] = vH_Utils::userPass($_POST['User']['password'], $salt);
                    $model->setAttribute('salt', $salt);
                }
            }
            if ($_POST['User']['work_place'] == NULL) {
                $model->work_place = '';
            }
            if ($_POST['User']['work_place2'] == NULL) {
                $model->work_place2 = '';
            }

            if ($_POST['User']['job_category'] == NULL) {
                $model->job_category = '';
            } else {
                $model->job_category = ',' . implode(",", $_POST['User']['job_category']) . ',';
            }

            if ($model->save()) {
                $this->attachLogBehavior($model);
                Yii::app()->admin->putSuccessMsg('Cập nhật thành công');
                $this->redirect(array('admin'));
            }
        }
        $arr_workplace = explode(",", $model->work_place2);
        $arr_workplace = array_filter($arr_workplace);
        $model->work_place2 = implode(",", $arr_workplace);
        if (!empty($arr_workplace)) {
            foreach ($arr_workplace as $key => $district) {
                if (strpos($district, '-') != false) {
                    $districtsall = new District;
                    $districtsall->district_id = $district;
                    $districtsall->title = Yii::t('main', 'All');
                    $districtsall->region_id = str_replace('-', '', $district);
                    unset($arr_workplace[$key]);
                }
            }
            $criteria = new CDbCriteria();
            $criteria->addInCondition('district_id', $arr_workplace);
            $workplaces = District::model()->findAll($criteria);
            if (isset($districtsall)) {
                if ($workplaces == NULL) {
                    $workplaces = array();
                    array_push($workplaces, $districtsall);
                } else
                    array_push($workplaces, $districtsall);
            }
        }
        $this->render('update', array(
            'model' => $model,
            'workplaces' => $workplaces,
        ));
    }

    public function actionLoadData() {
        $list_education = Education::model()->findAllByAttributes(array('user_id' => $_POST['user_id']));
        $this->renderPartial('table_education', array('list_education' => $list_education));
    }

    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);
        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser 
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionPublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'user_id in(' . implode(',', $ids) . ')';
            $model = User::model()->updateAll(array('status' => '1'), $criteria);
            //$this->attachLogBehavior($model); 
        }
        return;
    }

    public function actionUnpublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'user_id in(' . implode(',', $ids) . ')';
            $model = User::model()->updateAll(array('status' => '0'), $criteria);
            //$this->attachLogBehavior($model); 
        }
        return;
    }

    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionChangepass() {
        $this->setUnionTitle('Thay đổi mật khẩu');
        $this->menu[] = array('label' => 'Lưu lại', 'url' => '#', 'linkOptions' => array('submit' => array('update', 'id' => $id), 'onclick' => 'document.' . Yii::app()->params['adminFormName'] . '.submit(); return false;', 'class' => 'save'));
        $model = new AdmincpChangepassForm;
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'changepass-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
//        //$usertype = $model->type;
        if (isset($_POST['AdmincpChangepassForm'])) {
            $salt = '';
            $model2 = $this->loadModel(Yii::app()->admin->user_id);
            if (empty($_POST['AdmincpChangepassForm']['password_old'])) {
                $this->valid = false;
                $model->addErrors(array('password_old' => 'Mật khẩu cũ không được phép rỗng!'));
            } elseif (vH_Utils::userPass($_POST['AdmincpChangepassForm']['password_old'], $model2->salt) != $model2->password) {
                $this->valid = false;
                $model->addErrors(array('password_old' => 'Mật khẩu cũ không chính xác!'));
            } elseif (empty($_POST['AdmincpChangepassForm']['password'])) {
                $this->valid = false;
                $model->addErrors(array('password' => 'Mật khẩu không được phép rỗng!'));
            } elseif (empty($_POST['AdmincpChangepassForm']['password_cf'])) {
                $this->valid = false;
                $model->addErrors(array('password_cf' => 'Mật khẩu xác nhận không được phép rỗng!'));
            } elseif ($_POST['AdmincpChangepassForm']['password'] != $_POST['AdmincpChangepassForm']['password_cf']) {
                $this->valid = false;
                $model->addErrors(array('password_cf' => 'Mật khẩu xác nhận không đúng!'));
            } else {
                $salt = Yii::app()->params['salt'];
                $_POST['AdmincpChangepassForm']['password'] = vH_Utils::userPass($_POST['AdmincpChangepassForm']['password'], $salt);
            }

            $model2->password = $_POST['AdmincpChangepassForm']['password'];
            $model2->salt = $salt;

            if ($this->valid) {
                if ($model2->save()) {
                    $this->attachLogBehavior($model2);
                    Yii::app()->admin->putSuccessMsg('Cập nhật thành công');
                    $this->redirect(array('admin'));
                } else {
                    print_r($model2->getErrors());
                }
            }
        }
        $this->render('changepass', array('model' => $model));
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionChangeCountry() {
        $countryId = (int) $_REQUEST['countryId'];
        echo SiteHelper::loadRegionsOptions($countryId);
        return;
    }

    public function actionDeleteSelected() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục!';
        } else if (!is_array($ids)) {
            echo 'Yêu cầu không hợp lệ!';
        } else {
            foreach ($ids as $id) {
                $model = $this->loadModel($id);
                $this->attachLogBehavior($model);
                $model->delete();
            }
        }
        return;
    }
    
    public function actionDeleteImage() {
        if (isset($_POST['id']) && $_POST['id'] > 0) {
            $about = User::model()->findByAttributes(array('user_id' => $_POST['id']));
            $about->avatar = "";
            $about->save();
        }
    }

}
