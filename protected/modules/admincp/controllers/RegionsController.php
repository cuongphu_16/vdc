<?php

class RegionsController extends AdmincpController {

    public $layout = 'index';
    public $defaultAction = 'admin';

    protected function getActionsKey() {
        return __CLASS__;
    }

    public function init() {
        parent::init();
        $this->controllerLable = 'Tỉnh / Thành phố';
    }

    public function actionAdmin() {
        $this->setUnionTitle('Danh sách Tỉnh / Thành phố');
        $this->breadcrumbs = array(
            'Danh sách Tỉnh / Thành phố',
        );
        $model = new Regions('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Regions']))
            $model->attributes = $_GET['Regions'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCreate() {
        $this->setUnionTitle('Thêm mới Tỉnh / Thành phố');
        $model = new Regions('create');
        $this->attachLogBehavior($model);

        if (isset($_POST['Regions'])) {
            $model->attributes = $_POST['Regions'];
            $model->setAttribute('user_edit', Yii::app()->admin->user_id);
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm mới thành công');
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $this->setUnionTitle('Sửa chữa Tỉnh / Thành phố');
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);

        if (isset($_POST['Regions'])) {
            $model->attributes = $_POST['Regions'];
            $model->setAttribute('user_edit', Yii::app()->admin->user_id);

            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Cập nhật thành công');
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);
        $model->delete();

        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionPublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'regionId in(' . implode(',', $ids) . ')';
            $model = Regions::model()->updateAll(array('status' => '1'), $criteria);
            //$this->attachLogBehavior($model);
        }
        return;
    }

    public function actionUnpublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'regionId in(' . implode(',', $ids) . ')';
            $model = Regions::model()->updateAll(array('status' => '0'), $criteria);
            //$this->attachLogBehavior($model);
        }
        return;
    }

    public function actionDeleteSelected() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục!';
        } else if (!is_array($ids)) {
            echo 'Yêu cầu không hợp lệ!';
        } else {
            foreach ($ids as $id) {
                $model = $this->loadModel($id);
                $this->attachLogBehavior($model);
                $model->delete();
            }
        }
        return;
    }

    public function loadModel($id) {
        $model = Regions::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'regions-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
