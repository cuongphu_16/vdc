<?php

class LanguageController extends AdmincpController {

    public $layout = 'index';
    public $defaultAction = 'admin';

    protected function getActionsKey() {
        return __CLASS__;
    }

    public function init() {
        parent::init();
        $this->controllerLable = 'LanguageController';
    }

    public function actionAdmin() {
        $this->setUnionTitle('Danh sách ngôn ngữ');
         $this->breadcrumbs = array(
            'Danh sách ngôn ngữ',
        );
        if (!empty($_GET['setDefault'])) {
            $lang_id = (int) $_GET['lang_id'];
            if ($lang_id > 0) {
                Language::model()->updateAll(array('default' => 0));
                $lang = Language::model()->findByPk($lang_id);
                $lang->default = 1;
                $lang->status = 1;
                $lang->save();
            }
            $this->redirect(Yii::app()->controller->action->ID);
        }

        $model = new Language('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Language']))
            $model->attributes = $_GET['Language'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /* public function actionView($id)
      {
      $this->render('view',array(
      'model'=>$this->loadModel($id),
      ));
      } */

    public function actionCreate() {
        $this->setUnionTitle('Thêm mới ngôn ngữ');

        $model = new Language('create');
        $this->attachLogBehavior($model);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Language'])) {
            $model->attributes = $_POST['Language'];

            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $this->setUnionTitle('Chỉnh sửa ngôn ngữ');
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Language'])) {
            $model->attributes = $_POST['Language'];
            if ($model->save()) {
                Yii::app()->admin->putSuccessMsg('Thêm thành công!');
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $this->attachLogBehavior($model);
        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionPublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'lang_id in(' . implode(',', $ids) . ')';
            $model = Language::model()->updateAll(array('status' => '1'), $criteria);
            //$this->attachLogBehavior($model);
        }
        return;
    }

    public function actionUnpublish() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục.';
        } else {
            $criteria = new CDbCriteria;
            $criteria->condition = 'lang_id in(' . implode(',', $ids) . ')';
            $model = Language::model()->updateAll(array('status' => '0'), $criteria);
            //$this->attachLogBehavior($model);
        }
        return;
    }

    public function actionDeleteSelected() {
        $ids = $_POST['ids'];
        if (empty($ids)) {
            echo 'Bạn phải chọn ít nhất 1 mục!';
        } else if (!is_array($ids)) {
            echo 'Yêu cầu không hợp lệ!';
        } else {
            foreach ($ids as $id) {
                $model = $this->loadModel($id);
                $this->attachLogBehavior($model);
                $model->delete();
            }
        }
        return;
    }

    public function loadModel($id) {
        $model = Language::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'language-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    //============================

    public function actionPhrases() {
        $this->setUnionTitle('Các cụm từ');
        $this->menu[] = array('label' => 'Thêm mới', 'url' => array('newphrases'), 'linkOptions' => array('class' => 'add'));

        $model = new SourceMessage('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['SourceMessage']))
            $model->attributes = $_GET['SourceMessage'];


        $columns = array(
            array(
                'header' => 'STT',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'htmlOptions' => array('style' => 'text-align:center; width:30px; font-weight:bold'),
            ),
            array(
                'class' => 'CCheckBoxColumn',
                'id' => 'ids',
                'selectableRows' => 2,
                'htmlOptions' => array('style' => 'text-align:center'),
                //'name'=>'admincp-node-grid-ids',
                'htmlOptions' => array('style' => 'text-align:center; width:30px'),
            ),
            array(
                'name' => 'id',
                'htmlOptions' => array('style' => 'text-align:center; width:50px'),
            ),
            array(
                'name' => 'category',
                'type' => 'html',
                'value' => 'CHtml::link($data->category,array("language/phrasesUpdate","id"=>$data->id))',
                'headerHtmlOptions' => array('style' => 'text-align:left; width:100px'),
            ),
            array(
                'name' => 'message',
                'type' => 'html',
                'value' => 'CHtml::link($data->message,array("language/phrasesUpdate","id"=>$data->id))',
                'htmlOptions' => array('style' => 'text-align:left;'),
                'headerHtmlOptions' => array('style' => 'text-align:left;'),
            ),
        );

        $languages = Language::model()->findAll();
        if (!empty($languages)) {
            $n = 0;
            foreach ($languages as $lang) {
                $n++;
                $columns[] = array(
                    'header' => $lang->title,
                    'type' => 'raw',
                    'value' => '($trans = Language::model()->getMsgTrans($data->id, ' . $lang->code . ')->translation)!="" ? $trans : $data->message',
                    'headerHtmlOptions' => array('style' => 'text-align:left'),
                );
                if ($n > 7)
                    break;
            }
        }

        $columns[] = array(
            'class' => 'CButtonColumn',
            'template' => '{update} {delete}', // {delete}
            'buttons' => array(
                'update' => array(
                    'url' => 'Yii::app()->createUrl("admincp/language/phrasesUpdate", array("id"=>$data->id))',
                ),
                'delete' => array(
                    'url' => 'Yii::app()->createUrl("admincp/language/phrasesDelete", array("id"=>$data->id))',
                ),
            ),
            'htmlOptions' => array('style' => 'width:40px; text-align:center'),
        );
        $this->render('phrases', array(
            'model' => $model,
            'columns' => $columns
        ));
    }
    public function actionAddlang()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect($this->createAbsoluteUrl("/"));
        $arr_result = array();
        if (isset($_POST['lang_key']))
            $lang_key = trim($_POST['lang_key']);
        else
            $lang_key = "";
        if (isset($_POST['lang_value']))
            $lang_value = trim($_POST['lang_value']);
        else
            $lang_value = "";
        
        if (isset($_POST['lang_type']))
            $lang_type = (int)$_POST['lang_type'];
        else
            $lang_type = 0;
        if ($lang_key!="" && $lang_value!=""){
            $criteria = new CDbCriteria();
            
            $criteria->addCondition("BINARY `key`='".$lang_key."'");
            $criteria->addCondition("type=".$lang_type."");
            $langVn = LangVn::model()->find($criteria);
            if ($langVn==NULL)
            {
                $langVn = new LangVn();
                $langVn->key = $lang_key;
                $langVn->value = $lang_value;
                $langVn->type = $lang_type;
                if ($langVn->save())
                {
                    $langvns = LangVn::model()->findAllByAttributes(array('type'=>$lang_type));
                    if ($langvns!=NULL)
                    {
                        if ($lang_type==0)
                        {
                            $content = "<?php" . "\n";
                            $content .= "\$array = array(" . "\n";
                            $i = -1;

                            foreach ($langvns as $key => $lang) {
                                $content .= "'" . str_replace(array("\r\n", "\r", "\n", "\t", '\"'), array("", "", "", "", '"'), addslashes($lang->key)) . "' => '" . str_replace(array("\r\n", "\r", "\n", "\t"), array("", "", "", ""), $lang->value) . "'," . "\n";
                            }
                            $content .= ");";
                            //echo $content;
                            file_put_contents('language/_langvn.php', $content);
                        }else{
                            $contents2 .= " var lang_js = {" . "\n";
                            $i = -1;

                            foreach ($langvns as $key => $lang) {
                                $contents2 .= "'" . $lang->key . "' : '" . str_replace(array("\r\n", "\r", "\n", "\t", "'"), array("", "", "", "", "\'"), $lang->value) . "'," . "\n";
                            }
                            $contents2 .= "};";
                            //echo $content;
                            file_put_contents('language/language_js.js', $contents2);
                        }
                    }
                    $arr_result['success'] = 'success';
                }else{
                    $arr_result['success'] = 'error';
                    $arr_result['msg'] = 'Không thể thêm cấu hình, vui lòng thử lại sau';
                }
            }else{
                $arr_result['success'] = 'error';
                $arr_result['msg'] = 'Nội dung Tiếng Anh đã tồn tại';
            }
        }
        echo json_encode($arr_result);
        Yii::app()->end();
    }
    public function actionUdpatelang() {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect("/");
        else {
            if (isset($_POST['id']))
                $id = (int) $_POST['id'];
            else
                $id = 0;
            if (isset($_POST['type']))
                $type = (int) $_POST['type'];
            else
                $type = 0;
            if (isset($_POST['content']))
                $content = $_POST['content'];
            else
                $content = "";
            if (isset($_POST['data-key']))
                $datakey = $_POST['data-key'];
            else
                $datakey = "";
            if ($id > 0) {
                if (isset(Yii::app()->session['langChange']) && is_array(Yii::app()->session['langChange'])) {
                    $arrLang = Yii::app()->session['langChange'];
                } else {
                    $arrLang = array();
                }
                $arrLang[$id] = array('value'=>$content,'type'=>$type);
                Yii::app()->session['langChange'] = $arrLang;
            }else{
                if (trim($datakey!=""))
                {
                    if (isset(Yii::app()->session['langChange']) && is_array(Yii::app()->session['langChange'])) {
                        $arrLang = Yii::app()->session['langChange'];
                    } else {
                        $arrLang = array();
                    }
                    $id1 = "new_".time();
                    $arrLang[$id1] = array('key'=>$datakey,'value'=>$content,'type'=>$type);
                    Yii::app()->session['langChange'] = $arrLang;
                }
            }
            $arr_result = array();
            $arr_result['success'] = 'success';
            $arr_result['msg'] = Yii::app()->session['langChange'];
            echo json_encode($arr_result);
        }
    }

    public function actionVn() {
        
        $this->setUnionTitle('Cấu hình ngôn ngữ tiếng Việt');
        $this->menu[] = array('label' => 'Lưu lại', 'url' => '#', 'linkOptions' => array('submit' => array('update', 'id' => $id), 'onclick' => 'document.' . Yii::app()->params['adminFormName'] . '.submit(); return false;', 'class' => 'save'));
        if ((!isset($_POST['submittype']) || $_POST['submittype']!='search') && !empty(Yii::app()->session['langChange']))
        {
            foreach (Yii::app()->session['langChange'] as $key=>$lang)
            {
                
                if (strpos($key,'new')===false)
                    LangVn::model()->updateByPk($key,array('value'=>$lang['value'],'type'=>$lang['type']));
                else{
                    $langSet = new LangVn();
                    $langSet->key = $lang['key'];
                    $langSet->value = $lang['value'];
                    $langSet->type = $lang['type'];
                    if ($langSet->save())
                    {
                    }
                }
            }
        }
        
        if (isset($_GET['page'])) $page = (int) $_GET['page'];
        else if (isset(Yii::app()->session['page'])) $page = (int)Yii::app()->session['page'];
        else $page = 1;
        Yii::app()->session['page'] = $page;
        $criteria = new CDbCriteria();
        $keyword = "";
        if (isset($_POST['submittype']) && $_POST['submittype']=='search' && !empty($_POST['keywordsearch']))
        {
            $keyword = trim($_POST['keywordsearch']);
            $keyword = str_replace("'", "", $keyword);
            $criteria->condition = "`key` LIKE '%".$keyword."%' OR `value` like '%".$keyword."%'";
        }
        $count = LangVn::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 500;
        //$pages->setCurrentPage($page);
        $criteria->order = "type ASC, id DESC";
        $pages->applyLimit($criteria);
        $langvns = LangVn::model()->findAll($criteria);
        if ((!isset($_POST['submittype']) || $_POST['submittype']!='search') && !empty(Yii::app()->session['langChange']))
        {
            $langvns1 = LangVn::model()->findAll();
            //require_once( dirname(__FILE__) . '/../../../../language/_langvn.php');
            $content = "<?php" . "\n";
            $content .= "\$array = array(" . "\n";
            
            $contents2 .= " var lang_js = {" . "\n";
            $i = -1;
            
            foreach ($langvns1 as $key => $lang) {
                if ($lang->type==0)
                {
                    $i++;
                    $content .= "'" . str_replace(array("\r\n", "\r", "\n", "\t", '\"'), array("", "", "", "", '"'), addslashes($lang->key)) . "' => '" . str_replace(array("\r\n", "\r", "\n", "\t"), array("", "", "", ""), $lang->value) . "'," . "\n";
                }else{
                    $contents2 .= "'" . $lang->key . "' : '" . str_replace(array("\r\n", "\r", "\n", "\t", "'"), array("", "", "", "", "\'"), $lang->value) . "'," . "\n";
                }
            }
            $content .= ");";
            $contents2 .= "};";
            //echo $content;
            file_put_contents('language/_langvn.php', $content);
            file_put_contents('language/language_js.js', $contents2);
            unset($langvns1);
        }
        
        Yii::app()->session['langChange'] = array();
        $this->render('vn', array('langvns' => $langvns,'pages'=>$pages,'keyword'=>$keyword));
    }

    public function actionPhrasesUpdate($id) {
        $this->menu[] = array('label' => 'Quay lại Danh sách', 'url' => array('phrases'), 'linkOptions' => array('class' => 'back'));
        $this->menu[] = array('label' => 'Lưu lại', 'url' => '#', 'linkOptions' => array('submit' => array('update', 'id' => $id), 'onclick' => 'document.' . Yii::app()->params['adminFormName'] . '.submit(); return false;', 'class' => 'save'));

        $this->setUnionTitle('Chỉnh sửa / Dịch');
        $source_message = SourceMessage::model()->findByPk($id);
        if (empty($source_message))
            $this->redirect(array('admin'));

        $languages = Language::model()->findAll();

        if (!empty($_POST['Message'])) {
            foreach ($_POST['Message'] as $msg_) {
                $msg = Message::model()->findByAttributes(array(
                    'language' => $msg_['language'],
                    'id' => $source_message->id
                ));
                if (count($msg) > 0) {
                    $msg->translation = trim($msg_['translation']);
                    $msg->save();
                } else {
                    if (!empty($msg_['translation'])) {
                        $msg = new Message;
                        $msg->translation = trim($msg_['translation']);
                        $msg->id = $source_message->id;
                        $msg->language = $msg_['language'];
                        $msg->save();
                    }
                }
            }
            $this->redirect(array('phrases'));
        }


        $this->render('phrasesUpdate', array(
            'message' => $message,
            'languages' => $languages,
            'source' => $source_message,
            'arr_message_translate' => $arr_message_translate,
        ));
    }

    public function actionNewphrases() {
        $this->menu[] = array('label' => 'Quay lại Danh sách', 'url' => array('phrases'), 'linkOptions' => array('class' => 'back'));
        $this->menu[] = array('label' => 'Lưu lại', 'url' => '#', 'linkOptions' => array('submit' => array(), 'onclick' => 'document.' . Yii::app()->params['adminFormName'] . '.submit(); return false;', 'class' => 'save'));

        $this->setUnionTitle('Thêm cụm từ');

        $languages = Language::model()->findAll();
        $model = new SourceMessage;
        if (!empty($_POST)) {
            $model->attributes = $_POST['SourceMessage'];
            $model->setAttribute('category', 'site');
            if ($model->validate()) {
                if ($model->save()) {
                    $source_id = Yii::app()->db->getLastInsertID();
                    foreach ($_POST['Message'] as $msg_) {
                        if (!empty($msg_['translation'])) {
                            $msg = new Message;
                            $msg->translation = trim($msg_['translation']);
                            $msg->id = $source_id;
                            $msg->language = $msg_['language'];
                            $msg->save();
                        }
                    }
                }
                $this->redirect(array('phrases'));
            }
            //print_r($_POST);
        }
        //render
        $this->render('newphrases', array(
            'model' => $model,
            'languages' => $languages,
        ));
    }

    public function actionPhrasesDelete($id) {
        $model = SourceMessage::model()->findByPk($id);
        if ($model === NULL)
            throw new CHttpException(404, 'Trang không tồn tại');
        $this->attachLogBehavior($model);
        $model->delete();
    }

}
