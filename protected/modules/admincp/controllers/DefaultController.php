<?php

class DefaultController extends AdmincpController
{
	const AJAX_STATUS_TRUE=  1;
	const AJAX_STATUS_FALSE= 0;
	
	protected function getActionsKey()
	{
		return __CLASS__;
	}
	
	public function actions()
    {
        return array(
            'upload'=>array(
                'class'=>'ext.xupload.actions.XUploadAction',
        		'subfolderVar'=>'parent_id',
        		'path'=> Yii::app()->params['uploadPath']['adv'],
            ),
        );
    }
    
	public function actionIndex()
	{
		$this->render('index');
	}
	
	/**
	 * Get one recommender's bank account.
	 * 
	 * @return array(1=>'', 2=>'')
	 */
	public function actionAjaxGetRecommenderBanks()
	{
		$return= array( 'status'=>self::AJAX_STATUS_FALSE, 'message'=>'！', 'data'=>array() );
		//if ( true ) {
			$data= array();
			$model= new RecommenderBank;
			$bankArray= RecommenderBank::model()->getBanksOptions();
			$banks= $model->findAllByAttributes(array('recommender_id'=>$_GET['id']) );
			foreach ($banks as $v ) {
				$data[$v->bank_id]= $v->account_name. ' | '. $bankArray[$v->bank_name];
			}
			$return['status']= self::AJAX_STATUS_TRUE;
			$return['message']= "";
			$return['data']= $data;
			
		//}
		echo CJSON::encode($return);
		
	}
	
	
    public function actionConnector()
	{
        $this->layout=false;
        
        Yii::import('elfinder.vendors.*');
        require_once('elFinder.class.php');

        $opts=array(
            'root'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR,
            'URL'=>Yii::app()->baseUrl.'/uploads/',
            'rootAlias'=>'Home',
        );

        $fm=new elFinder($opts);
        $fm->run();
	}
}