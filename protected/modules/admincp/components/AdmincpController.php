<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
abstract class AdmincpController extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = 'nonav';
    //public $layout='//../modules/admincp/views/layouts/column1';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    public $controllerLable = '';
    public $actionLable = '';
    public $actionIco = 'icon48_article';
    public $actionLink = '';
    public $assets = "";
    public $toolbar = array();
    public $navbarDisabled = true;

    public function init() {
        parent::init();
        $this->assets = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admincp.assets')) . '/';
        if (!empty($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', $_GET['pageSize']);
        }
    }

    abstract protected function getActionsKey();

    public function getModuleLabel() {
        if ($this->getModule() && $this->getModule()->moduleLabel) {
            return $this->getModule()->moduleLabel;
        } else {
            return Yii::app()->name;
        }
    }

    public function getControllerLabel() {
        if ($this->controllerLable) {
            return $this->controllerLable;
        } else {
            return $this->getId();
        }
    }

    public function getActionLabel() {

        if ($this->actionLable) {
            return $this->actionLable;
        } elseif ($this->getAction() && $this->getAction()->getId()) {
            return $this->getAction()->getId();
        } else {
            return '--';
        }
    }

    public function setUnionTitle($actionName = NULL) {
        $title['module'] = $this->getModuleLabel();
        $title['controller'] = $this->getControllerLabel();
        if ($actionName !== NULL) {
            $title['action'] = $actionName;
        } elseif ($this->getAction() !== null && $this->getAction()->getId()) {
            $title['action'] = $this->getAction()->getId();
        }

        $this->controllerLable = $title['controller'];
        $this->actionLable = $title['action'];
        ksort($title);
        $this->setPageTitle(implode(' - ', (array) $title));
    }

    public function rendererTile() {
        if ($this->getPageTitle()) {
            return $this->getPageTitle();
        } else {
            return Yii::app()->name;
        }
    }

    public function setBreadcrumbs($array) {
        
    }

    public function getBreadcrumbs() {
        return $this->breadcrumbs = array(
            $this->getControllerLabel() => array('admin'),
            $this->getActionLabel(),
        );
    }

    public function isExceptAction() {
        return preg_match('/main\/login|main\/logout|main\/captcha/i', strtolower($this->getRoute()));
    }

    protected function getAllowActions() {
        /* @var $session AdmincpSession */
        $acl = Yii::app()->admin->getState('allowActions');

        if ($acl) {
            $roleAcl = $acl;
        } else {
            $roleId = User::model()->loadRoleIdByAccount(Yii::app()->admin->name);
            $roleAcl = AdmincpRole::model()->getOneAclArray($roleId);
            Yii::app()->admin->setState('allowActions', $roleAcl);
        }
        //print_r($roleAcl);die;
        if (isset($roleAcl[$this->getActionsKey()])) {
            return $roleAcl[$this->getActionsKey()];
        } else {
            return array();
        }
    }

    protected function beforeAction($action) {
        if (Yii::app()->params['adminFormName'] === NULL)
            Yii::app()->params['adminFormName'] = 'adminForm';

        $this->getToolbar();
        if (!empty($_POST['session_upload_photo'])) {
            return true;
        }
        /* @var CWebApplication */
        if ((!$this->isExceptAction() && Yii::app()->admin->getIsGuest() ) || (!$this->isExceptAction() && Yii::app()->admin->getState('area') !== Yii::app()->params['area']['admincp'] )
        ) {
            $this->redirect(array('main/login', 'referrer' => base64_encode($this->getRoute())));
            return;
        }
        if ($this->getId() === 'default' || Yii::app()->admin->getState('allowActions') === Yii::app()->params['fullAccess'] || in_array(strtolower($this->getAction()->getId()), (array) $this->getAllowActions())
        ) {
            return true;
        } else {
            /* $arr_action_allow = array('loadRegions');
              if(in_array($this->getAction()->getId(),$arr_action_allow))
              return true; */
            $this->redirect(array('main/notAllowed', 'returnUrl' => base64_encode(Yii::app()->getRequest()->getUrlReferrer())));
        }
    }

    public function getToolbar() {
        Yii::app()->clientScript->registerScript('getChecked', "function getChecked(){
			var table_grid = $('div." . Yii::app()->params['gridViewClass'] . "');
			var checked = '', checked_count=0;
			$('tbody tr',table_grid).each(function(){
				var checkbox = $(':checkbox:eq(0)',this);
				if(checkbox.attr('checked') == 'checked')
				{
					checked_count++;
					if($.trim(checked) == '')
						checked+='ids[]= '+checkbox.val();
					else
						checked+='&ids[]= '+checkbox.val();
				}
			});
			if(checked_count == 0){
				return false;
			}else{
				return checked;
			}
		};\n");

        $action = Yii::app()->controller->action->id;
        $id = Yii::app()->request->getQuery('id');
        $this->toolbar['cancel'] = array('label' => 'Hủy bỏ', 'url' => array('admin'), 'linkOptions' => array('confirm' => 'Bạn có chắc chắn muốn Hủy bỏ?', 'class' => 'cancel'));

        $this->toolbar['delete'] = array('label' => 'Xóa', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $id), 'confirm' => 'Bạn có chắc chắn muốn xóa?', 'class' => 'delete'));

        $this->toolbar['create'] = array('label' => 'Lưu lại', 'url' => '#', 'linkOptions' => array('submit' => array('create'), 'onclick' => 'document.' . Yii::app()->params['adminFormName'] . '.submit(); return false;', 'class' => 'save'));

        $this->toolbar['update'] = array('label' => 'Lưu lại', 'url' => '#', 'linkOptions' => array('submit' => array('update', 'id' => $id), 'onclick' => 'document.' . Yii::app()->params['adminFormName'] . '.submit(); return false;', 'class' => 'save'));

        $this->toolbar['publish'] = array(
            'label' => 'Hiển thị',
            'url' => '#',
            'linkOptions' => array(
                'class' => 'publish',
                'ajax' => array(
                    'type' => 'POST',
                    'url' => array('publish'),
                    'data' => 'js:getChecked()',
                    'beforeSend' => 'function(){
						if(!getChecked()){
							alert(\'Hãy chọn ít nhất 1 mục.\');
							return false;
						}else if(!confirm(\'Hiển thị các mục đã chọn! Bấm OK để thực hiện hành động này?\')){
							return false;
						}
						
					}',
                    'success' => 'function(data){
						if($.trim(data) != ""){
							alert(data);
						}else{
							$.fn.yiiGridView.update($(".' . Yii::app()->params['gridViewClass'] . '").attr("id"));
						}
					}',
                    'error' => 'function(XHR){
						alert(XHR.responseText);
					}'
                )
            )
        );

        $this->toolbar['unpublish'] = array(
            'label' => 'Không hiển thị',
            'url' => '#',
            'linkOptions' => array(
                'class' => 'unpublish',
                'ajax' => array(
                    'type' => 'POST',
                    'url' => array('unpublish'),
                    'data' => 'js:getChecked()',
                    'beforeSend' => 'function(){
						if(!getChecked()){
							alert(\'Hãy chọn ít nhất 1 mục.\');
							return false;
						}else if(!confirm(\'Không hiển thị các mục đã chọn! Bấm OK để thực hiện hành động này?\')){
							return false;
						}
						//return getChecked(true);
						
					}',
                    'success' => 'function(data){
						if($.trim(data) != ""){
							alert(data);
						}else{
							$.fn.yiiGridView.update($(".' . Yii::app()->params['gridViewClass'] . '").attr("id"));
						}
					}',
                    'error' => 'function(XHR){
						alert(XHR.responseText);
					}'
                )
            )
        );

        $this->toolbar['delete_selected'] = array(
            'label' => 'Xóa chọn',
            'url' => '#',
            'linkOptions' => array(
                'class' => 'delete',
                'ajax' => array(
                    'type' => 'POST',
                    'url' => array('deleteSelected'),
                    'data' => 'js:getChecked()',
                    'beforeSend' => 'function(){
						if(!getChecked()){
							alert(\'Hãy chọn ít nhất 1 mục.\');
							return false;
						}else if(!confirm(\'Bạn có chắc chắn muốn xóa những mục đã chọn?\')){
							return false;
						}
						
					}',
                    'success' => 'function(data){
						if($.trim(data) != ""){
							alert(data);
						}else{
							$.fn.yiiGridView.update($(".' . Yii::app()->params['gridViewClass'] . '").attr("id"));
						}
					}',
                    'error' => 'function(XHR){
						alert(XHR.responseText);
					}'
                ),
            )
        );

        $this->toolbar['restore'] = array(
            'label' => 'Khôi phục',
            'url' => '#',
            'linkOptions' => array(
                'class' => 'restore',
                'ajax' => array(
                    'type' => 'POST',
                    'url' => array('restore'),
                    'data' => 'js:getChecked()',
                    'beforeSend' => 'function(){
						if(!getChecked()){
							alert(\'Hãy chọn ít nhất 1 mục.\');
							return false;
						}else if(!confirm(\'Bạn có chắc chắn muốn khôi phục những mục đã chọn?\')){
							return false;
						}
						
					}',
                    'success' => 'function(data){
						if($.trim(data) != ""){
							alert(data);
						}else{
							$.fn.yiiGridView.update($(".' . Yii::app()->params['gridViewClass'] . '").attr("id"));
						}
					}',
                    'error' => 'function(XHR){
						alert(XHR.responseText);
					}'
                ),
            )
        );

        $this->toolbar['empty'] = array(
            'label' => 'Xóa chọn',
            'url' => '#',
            'linkOptions' => array(
                'class' => 'empty',
                'ajax' => array(
                    'type' => 'POST',
                    'url' => array('empty'),
                    'data' => 'js:getChecked()',
                    'beforeSend' => 'function(){
						if(!getChecked()){
							alert(\'Hãy chọn ít nhất 1 mục.\');
							return false;
						}else if(!confirm(\'Bạn có chắc chắn muốn xóa những mục đã chọn?\')){
							return false;
						}
						
					}',
                    'success' => 'function(data){
						if($.trim(data) != ""){
							alert(data);
						}else{
							$.fn.yiiGridView.update($(".' . Yii::app()->params['gridViewClass'] . '").attr("id"));
						}
					}',
                    'error' => 'function(XHR){
						alert(XHR.responseText);
					}'
                ),
            )
        );

        $this->toolbar['back'] = array('label' => 'Quay lại Danh sách', 'url' => array('admin'), 'linkOptions' => array('class' => 'back'));

        switch ($action) {
            //create			
            case 'create': {
                    $this->menu = array(
                        //$this->toolbar['back'],				
                        $this->toolbar['cancel'],
                        $this->toolbar['create'],
                    );
                    break;
                }

            //update
            case 'update': {
                    $this->menu = array(
                        $this->toolbar['back'],
                        //$this->toolbar['cancel'],
                        $this->toolbar['delete'],
                        $this->toolbar['update'],
                    );
                    break;
                }

            //admin
            case 'admin': {
                    $this->navbarDisabled = false;
                    $this->menu = array(
                        $this->toolbar['delete_selected'],
                        $this->toolbar['publish'],
                        $this->toolbar['unpublish'],
                        array('label' => 'Thêm mới', 'url' => array('create'), 'linkOptions' => array('class' => 'add')),
                    );
                    break;
                }

            case 'view': {
                    $this->menu = array(
                        $this->toolbar['back'],
                        $this->toolbar['delete'],
                    );
                    break;
                }


            case 'index':
            default: {
                    $this->navbarDisabled = false;
                    break;
                }
        }
    }

    //not use
    /* public function accessRules()
      {
      return array(
      array('allow',
      'actions' => array('loadRegions'),
      'roles' => array('*'),
      ),
      );

      $access = array( array('deny', 'users'=>array('*'), ) );
      $userAcl= $this->getAllowActions();

      array_unshift($access, array('allow',
      'actions'=> $userAcl,
      'users'=> array(Yii::app()->admin->name )
      )  ) ;
      return $access;
      }
      public function filters()
      {
      return array(
      'accessControl', // perform access control for CRUD operations
      );
      } */

    public function attachLogBehavior($model) {
        if ($model instanceof CActiveRecord) {
            $model->attachBehavior('ARB', 'application.modules.admincp.behaviors.AdmincpARB');
        }
    }

    public function getUploadUrl($folder = NULL, $id = 0) {
        if ($folder === NULL)
            return false;
        $id = (int) $id;
        $url = Yii::app()->request->getBaseUrl(true) . Yii::app()->params['uploadPath']['view'][$folder];
        if ($id > 0)
            $url.= $id . '/';
        return $url;
    }

}
