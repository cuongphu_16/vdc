<?php

class AdmincpSession extends CHttpSession
{ 
	const REMEMBER_DAYS = 30;
	public $area = '';
	public $allowAutoLogin = TRUE;
	
	public function init()
	{
		parent::init();
		$this->area = Yii::app()->params['area']['admincp'];
	}
	
	public function setVar($key, $value)
	{
		Yii::app()->admin->$key = $value;
	}
	
	public function getVar($key)
	{
		Yii::app()->admin->$key;
	}
}