<?php

//Yii::import('zii.widgets.grid.CDataColumn');

class vH_StatusColumn extends CDataColumn {

    public $headerHtmlOptions = array('style' => 'width: 110px');
    public $filter = null;
    public $url = null;
    public $elmClass = null;

    public function init() {
        if (!isset($this->filter) || !is_array($this->filter)) {
            $this->filter = array(
                Yii::t('app', 'Not publish'),
                Yii::t('app', 'Publish'),
            );
        }
        $this->elmClass = $this->name . '_droplist';

        $this->filter = $this->filter;

        parent::init();

        $this->registerScript();
    }

    protected function registerScript() {
        $csrf = '';
        if (Yii::app()->request->enableCsrfValidation) {
            $csrfName = Yii::app()->request->csrfTokenName;
            $csrfToken = Yii::app()->request->csrfToken;
            $csrf = '+"&' . $csrfName . '=' . $csrfToken . '"';
        }

        if (!isset($this->url)) {
            $url = createAUrl('/acp/' . $this->grid->owner->id . '/updateStatus');
        } else {
            $url = $this->url;
        }
        $grid_id = $this->grid->id;
        $status_column_js = <<<VH
		$(document).on('change','select.{$this->elmClass}',function(){
                    var status = this.value;
                    var id = $(this).data('id');
                    $.fn.yiiGridView.update('{$grid_id}', {
                            type:'POST',
                            data: 'status='+status+'&id='+id{$csrf},
                            url: '{$url}',
                            cache: false,
                            success:function(data){
                                    $.fn.yiiGridView.update('{$grid_id}');
                            }
                    });
                    return false;
		});
VH;
        Yii::app()->getClientScript()->registerScript($this->elmClass . '_column_js', Utils::trimTotal($status_column_js));

        $status_column_css = <<<VH
                .{$this->elmClass}{
                width:100%;
                -moz-box-sizing:border-box;
                -webkit-box-sizing:border-box;
                box-sizing:border-box;
                }
VH;
        Yii::app()->getClientScript()->registerCss($this->elmClass . '_column_css', Utils::trimTotal($status_column_css));
    }

    protected function renderFilterCellContent() {
        if (is_string($this->filter))
            echo $this->filter;
        elseif ($this->filter !== false && $this->grid->filter !== null && $this->name !== null && strpos($this->name, '.') === false) {
            if (is_array($this->filter)) {
                echo '<span class="selectElm">';
                echo CHtml::activeDropDownList($this->grid->filter, $this->name, $this->filter, array('id' => false, 'prompt' => ''));
                echo '<i></i></span>';
                
            } elseif ($this->filter === null) {
                echo CHtml::activeTextField($this->grid->filter, $this->name, array('id' => false));
            }
        } else
            parent::renderFilterCellContent();
    }

    protected function renderDataCellContent($row, $data) {
        $status = (int) CHtml::value($data, $this->name);

        echo '<span class="selectElm">';
        echo CHtml::activeDropDownList($this->grid->filter, $this->name . "[{$data->primaryKey}]", $this->filter, array('options' => array($status => array('selected' => true)), 'class' => $this->elmClass, 'data-id' => $data->primaryKey));
        echo '<i></i></span>';
    }

}
