<?php

class AdminIdentity extends CUserIdentity
{
	const ERROR_STATE_INVALID = 11; //tai khoan bi khoa hoac chua duoc kich hoat
	const NOT_BE_ALLOWED_ACCESS = 21; //khong duoc phep truy cap vao admincp
	
	public function authenticate()
	{
            $authModel = $this->getAuthModel()->loadByAccount($this->username);
		if( !$authModel || $authModel->count() == 0) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
			
		} else if( $authModel && $authModel->getAttribute('password') !== vH_Utils::userPass($this->password,$authModel->getAttribute('salt')) ) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
			
		} else if( $authModel && $authModel->getAttribute('status')!=1) {
			$this->errorCode = self::ERROR_STATE_INVALID;
			
		}else{
			//print_r($authModel->attributes);
			$role = AdmincpRole::model()->findByPk($authModel->getAttribute('role_id'));
			$admin_use = (int)$role->getAttribute('admin_use');
			$role_status = (int)$role->getAttribute('status');
			$acl_desc = $role->getAttribute('acl_desc');
			
			if(($acl_desc != Yii::app()->params['fullAccess']) && ($admin_use != 1 || $role_status != 1)){
				//khong duoc phep truy cap vao admincp
				$this->errorCode = self::NOT_BE_ALLOWED_ACCESS;
			}else{// if($acl_desc == Yii::app()->params['fullAccess'])
				$this->errorCode = self::ERROR_NONE;				
			}
		}
		return !$this->errorCode;
	}
	
	protected function getAuthModel()
	{
		$model = User::model();
		if($model===null)
			throw new CHttpException(404,'Error occur when init auth model.');
		return $model;
	}
}