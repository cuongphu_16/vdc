<?php

class vH_CLinkPager extends CLinkPager {

    public
            $header = false,
            $cssFile = '';

    public function init() {
        parent::init();

        if (empty($this->cssFile)) {
            $this->cssFile = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admincp.assets')) . '/css/pages.css';
        }

        if ($this->nextPageLabel === null) {
            $this->nextPageLabel = Yii::t('yii', 'Next &gt;');
        }

        if ($this->prevPageLabel === null) {
            $this->prevPageLabel = Yii::t('yii', '&lt; Previous');
        }

        if ($this->firstPageLabel === null) {
            $this->firstPageLabel = Yii::t('yii', '&lt;&lt; First');
        }

        if ($this->lastPageLabel === null) {
            $this->lastPageLabel = Yii::t('yii', 'Last &gt;&gt;');
        }

        if ($this->header === null) {
            $this->header = Admincp::t('admincp', 'Go to page: ');
        }

        if (!isset($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = $this->getId();
        }

        if (!isset($this->htmlOptions['class'])) {
            $this->htmlOptions['class'] = 'yiiPager';
        }
    }

}

?>