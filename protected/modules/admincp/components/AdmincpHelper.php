<?php

class AdmincpHelper {

    public static $_assetsUrl, $_regionAndCountries, $_districtAndRegions, $_discussionTopics, $_wardAndDistrict;
    public static $performing = 2, $completed = 1;

    public static function getMenuArray() {
        //url,label,icon,disabled,visible,
        //url: router,link,params,htmlOptions
        $array = array(
                /* array(
                  'url'=>array(
                  'route'=>'/admincp/main/index',
                  ),
                  'label'=>'Home',
                  array(
                  'url'=>array(
                  'route'=>'/admincp/main/profile',
                  ),
                  'label'=>'Hồ sơ',
                  ),
                  ) */
        );

        $nodeModel = AdmincpNode::model();
        $tree = $nodeModel->getNodeTree(/* $level=1 */);
        //print_r($tree);die;
        if (!empty($tree)) {
            foreach ($tree as $v) {
                //print_r($v);
                array_push($array, $v);
            }
        }

        $menuArray['menu'] = $array;

        return $menuArray;
    }

    /* ============================ */

    public static function getYNOptions() {
        return array('1' => 'Có', '0' => 'Không');
    }

    public static function getFocusOptions() {
        return array('1' => 'Nổi bật', '0' => 'Không');
    }

    public static function getFocusLabel($value = null) {
        $array = self::getFocusOptions();
        if ($value === null || !array_key_exists($value, $array))
            return ' - ';
        return $array[$value];
    }

    public static function getYNLabel($value = null) {
        $array = self::getYNOptions();
        if ($value === null || !array_key_exists($value, $array))
            return ' - ';
        return $array[$value];
    }

    /* ============================ */

    public static function getDoneOrNotLabel($value = null) {
        $array = self::getDoneOrNotOptions();
        if ($value === null || !array_key_exists($value, $array))
            return ' - ';
        return $array[$value];
    }

    public static function getDoneOrNotOptions($img = false) {
        if ($img)
            return array('2' => 'unpublish14.png', '1' => 'publish14.png');
        else
            return array('1' => 'Chưa xử lý', '2' => 'Đã xử lý');
    }

    public static function getRequestStatusOptions($img = false) {
        if ($img)
            return array('0' => '', '1' => 'new_icon32.png');
        else
            return array('0' => 'Không có yêu cầu', '1' => 'Có yêu cầu mới');
    }

    public static function getStatusOptions($img = false) {
        if ($img)
            return array('0' => 'unpublish14.png', '1' => 'publish14.png');
        else
            return array('1' => 'Hiển thị', '0' => 'Không hiển thị');
    }

    public static function getPriorityOptions() {
            return array('0' => 'Tin thường', '1' => 'Ưu tiên tìm kiếm');
    }

    public static function getViewStatusOptions($img = false) {
        if ($img)
            return array('0' => 'unpublish14.png', '1' => 'publish14.png');
        else
            return array('1' => 'Đã xem', '0' => 'Chưa xem');
    }

    public static function getStatusLabel($value = null, $id = null, $link = true, $type = null) {

        $array = self::getStatusOptions(true);
        $stt = self::getStatusOptions(false);

        if ($type == 1) { //neu la kieu ung vien cong viec
            $array = self::getDoneOrNotOptions(true);
            $stt = self::getDoneOrNotOptions(false);
        } else if ($type == 2) {
            $array = self::getCensorOptions(true);
            $stt = self::getCensorOptions(false);
        }

        if ($value === null) {
            return '-';
        } else {
            $assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admincp.assets')) . '/images/toolbar/';
            $img = '<img src="' . $assetsUrl . $array[$value] . '" width="14" height="14" alt="' . $stt[$value] . '" />';

            if ($link) {
                Yii::app()->getClientScript()->registerScript('publish_btn', "$(document).on('click','a.publish_btn',function(){
					var id = $('." . Yii::app()->params['gridViewClass'] . "').attr('id');
					$.fn.yiiGridView.update(id, {
						type:'POST',
						data: 'ids[]='+$(this).attr('data-id'),
						url:$(this).attr('href'),
						success:function(data){
							$.fn.yiiGridView.update(id);
						},
						error:function(XHR){
							alert(XHR.responseText);
						}
					});
					return false;
				});");

                $lnk = '<a href="' . Yii::app()->controller->createUrl($value == 1 ? 'unpublish' : 'publish') . '" data-id="' . $id . '" class="publish_btn" title="' . $stt[$value] . '">' . $img . '</a>';
            } else {
                $lnk = $img;
            }
            return $lnk;
        }
    }

    public static function getNodeOptions() {
        $nodeModel = AdmincpNode::model();
        $options = $nodeModel->getNodeOptionsHash();
        //print_r($options);die;
        $options = $options;
        return $options;
    }

    public static function getUserStatusOptions($img = false) {
        if ($img)
            return array('0' => 'unpublish14.png', '1' => 'publish14.png');
        else
            return array('0' => 'Vô hiệu hóa', '1' => 'Kích hoạt');
    }

    public static function getControllerOptions($except = true, $exceptControllers = NULL) {
        //echo Yii::app()->basePath; => ../protected
        $dir = Yii::app()->basePath . '/modules/admincp/controllers/';
        $dir_handle = opendir($dir);
        $array = array();
        if ($dir_handle) {
            while (($file = readdir($dir_handle)) !== false) {
                if ($file === '.' || $file === '..') {
                    continue;
                }
                $tmp = realpath($dir . '/' . $file);
                if (!is_dir($tmp)) {
                    $tmp = substr(strstr($tmp, 'controllers'), 12, -4);
                    $array[$tmp] = $tmp;
                }
            }
            closedir($dir_handle);
        }
        if ($except === true) {
            $exceptControllers = array_merge((array) $exceptControllers, AdmincpNode::getExceptionControllers());
            foreach ($exceptControllers as $v) {
                unset($array[$v]);
            }
        }
        return $array;
    }

    public static function getRoleOptions($exceptId = null) {
        $model = AdmincpRole::model();
        $options = $model->toOptionHash();
        //print_r($options);die;
        if ($exceptId) {
            unset($options[$exceptId]);
        }
        return $options;
    }

    public static function getCountRoleTreeHtml($selected = false) {
        if ($selected !== false && $selected !== Yii::app()->params['fullAccess']) {
            $selected = @unserialize($selected);
        }
        $arr_action_allow = array('loadregions');
        $nodeModel = new AdmincpRole();
        $array = $nodeModel->getRoleTreeArray();

        //echo $html;die;
        return count($array);
    }

    public static function getRoleTreeHtml($selected = false) {
        if ($selected !== false && $selected !== Yii::app()->params['fullAccess']) {
            $selected = @unserialize($selected);
        }
        $arr_action_allow = array('loadregions');
        $nodeModel = new AdmincpRole();
        $array = $nodeModel->getRoleTreeArray();
        //print_r($array);die;
        $html = '';
        $checkHtml = ' checked=true ';
        $i = 0;
        foreach ($array as $k => $v) {
            $i++;
            $html.= "\n<li class='parent'><input id='{$k}' value='{$k}' class='fl check_pr' type='checkbox'> <label for=\"{$k}\"><b>" . $v['label'] . "</b> [{$k}]</label>\n";
            $html.= "<div class=\"clr\"></div>\n";
            $html.= "<ul class='ul sub'>";
            foreach ($v['actions'] as $sk => $sv) {
                if (in_array($sv, $arr_action_allow))
                    continue;
                $html.= "\n<li>";
                if (isset($selected[$k]) && is_array($selected[$k]) && in_array(strtolower($sv), $selected[$k])) {
                    $html.= '<input id="' . $k . '[' . $sv . ']" name="' . $k . '__' . $sv . '" value="' . $sv . '"' . $checkHtml . 'type="checkbox" class="fl"> <label>	' . AdmincpHelper::getActionLabel($sv) . '</label>';
                } else {
                    $html.= '<input id="' . $k . '[' . $sv . ']" name="' . $k . '__' . $sv . '" value="' . $sv . '" type="checkbox" class="fl"> <label>' . AdmincpHelper::getActionLabel($sv) . '</label>';
                }
                $html.= "\n<div class=\"clr\"></div>\n</li>";
            }

            $html.= "</ul>\n";
            $html.= "</li>\n";
            if ($i % 2 == 0)
                $html.= "<li class='clr'></li>\n";
        }
        //echo $html;die;
        return $html;
    }

    public static function getActionLabel($value = NULL) {
        $array = array(
            'admin' => 'Danh sách',
            'update' => 'Chỉnh sửa',
            'create' => 'Thêm mới',
            'delete' => 'Xóa',
            'view' => 'Xem',
            'deleteselected' => 'Xóa chọn',
            'publish' => 'Hiển thị',
            'unpublish' => 'Không hiển thị',
            'edittable' => 'Sửa nhanh',
            'excel'=>'Xuất dữ liệu ra file excel',
            'exceluser'=>'Xuất dữ liệu thành viên'
        );
        if ($value === NULL) {
            return $value;
        }
        return $array[$value] . ' <span>[' . $value . ']</span>';
    }

    public static function getLogTypeOptions() {
        $model = AdmincpLog::model();
        $types = $model->getActionType();
        return $types;
    }

    public static function getLogTypeLabel($value = null) {
        $array = self::getLogTypeOptions();
        if ($value === null) {
            return '-';
        } else {
            return $array[$value];
        }
    }

    public static function unsetMenu($action = array()) {
        $menu = Yii::app()->controller->menu;
        if (!is_array($action) || empty($action) || empty($menu)) {
            return false;
        }

        foreach ($menu as $k => $v) {
            $class = $v['linkOptions']['class'];
            if (in_array($class, $action)) {
                unset($menu[$k]);
            }
        }
        Yii::app()->controller->menu = $menu;
        return $menu;
    }

    public static function getCatOptions($option_null = '') {
        $catModel = Category::model();
        $options = $catModel->getCatOptionsTree('', 0);
        $options = array();
        if (!empty($option_null))
            return array('0' => $option_null) + $options;
        return $options;
    }

    public static function getTypeHightlightOptions() {
        return array('0' => 'Không', '1' => 'Có');
    }

    public static function getCatLabel($cat_id = 0) {
        $html = array();
        $catModel = Category::model()->findByPk($cat_id);
        if (!empty($catModel))
            return $catModel->title;
    }

    public static function dateConvert($str) {
        if (empty($str))
            return false;
        $str = date_format(date_create($str), 'l, H:i - d/m/Y');
        $arr_en_date = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $arr_vn_date = array('Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'Chủ nhật');
        return str_replace($arr_en_date, $arr_vn_date, $str);
    }

    public static function getTypeCatOptions($type = 0, $pid = '', $html = false) {
        $criteria = new CDbCriteria();
        $criteria->addCondition("status=1");
        if ($type > 0)
            $criteria->addCondition("type=" . $type);
        if (trim($pid) != '')
            $criteria->addCondition("p_id=" . $pid);
        if (isset($this->lang) && !empty($this->lang))
            $criteria->addCondition("language=" . $this->lang);
        $categories = Category::model()->findAll($criteria);
        if ($categories != NULL) {

            if ($html == true) {
                $return = "";
                foreach ($categories as $category) {
                    $return.='<option value="' . $category->cate_id . '">>' . $category->title . '</option>';
                }
            } else {
                $return = array();
                foreach ($categories as $category) {
                    $return[$category->cate_id] = $category->title;
                }
                return $return;
            }
        } else {
            if ($html == true)
                return "";
            else
                return array();
        }
    }

    public static function getPositionOptions() {
        return array(
            Advs::POSITION_BANNER => 'Trang chủ - Banner',            
        );
    }

    public static function getPositionLabel($value = null) {
        $array = self::getPositionOptions();
        if ($value === null) {
            return '-';
        } else {
            return $array[$value];
        }
    }

    public static function getPostsCatOptions() {
        return array(
            1 => 'Giới thiệu',
            2 => 'Tin tức',
            3 => 'Chính sách bảo mật',
        );
    }

    public static function getPostsCatLabel($value = null) {
        $array = self::getPostsCatOptions();
        if ($value === null) {
            return '-';
        } else {
            return $array[$value];
        }
    }

    public static function getServicesOptions() {
        $catModel = Services::model()->findAllByAttributes(array('status' => 1), array('select' => 'services_id, services_title', 'order' => 'sort_order ASC'));
        $options = array();
        if (!empty($catModel)) {
            foreach ($catModel as $r) {
                $options[$r->services_id] = $r->services_title;
            }
        }
        return $options;
    }

    public static function getUsersOptaions() {
        $catModel = User::model()->findAllByAttributes(array('status' => 1, 'type' => 0), array('select' => 'user_id, fullname, email', 'order' => 'user_id DESC'));
        $options = array();
        if (!empty($catModel)) {
            foreach ($catModel as $r) {
                $options[$r->user_id] = !empty($r->fullname) ? $r->fullname : $r->email;
            }
        }
        return $options;
    }

    public static function getRegionAndCountries() {

        return Regions::model()->toOptionHash();
    }

    public static function getRegionLabel($regionId) {
        $region = Regions::model()->findByPk($regionId);
        return $region->title;
    }

    public static function getDistrictAndRegions() {
        if (empty(AdmincpHelper::$_districtAndRegions)) {
            $regions = Regions::model()->findAll();
            if (empty($regions))
                return array();
            $arr_option = array();
            foreach ($regions as $region) {
                $district = $region->district;
                if (!empty($district)) {
                    $arr_option[$region->title] = CHtml::listData($district, 'district_id', 'title');
                }
            }
            AdmincpHelper::$_districtAndRegions = $arr_option;
        }
        return AdmincpHelper::$_districtAndRegions;
    }

    public static function getDistrictLabel($regionId) {
        $region = District::model()->findByPk($regionId);
        return $region->title;
    }
    public static function rrmdir__($dir, $first = 0) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir")
                        self::rrmdir__($dir . "/" . $object, 1);
                    else
                        @unlink($dir . "/" . $object);
                }
            }
            @reset($objects);
            if ($first > 0)
                @rmdir($dir);
        }
    }
}

?>