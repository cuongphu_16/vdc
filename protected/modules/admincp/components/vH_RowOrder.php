<?php
class vH_RowOrder extends CDataColumn {
    public $name = 'stt';
    public $header = null;
    public $htmlOptions = array(
        'style' => 'text-align: center; font-weight: bold'
    );
    public $headerHtmlOptions = array(
        'style' => 'text-align: center; width: 30px'
    );
    
    public function init() {
        parent::init();
        if(!isset($this->header)){
            $this->header = Yii::t('app','STT');
        }
    }

    protected function renderFilterCellContent() {
        return false;
    }

    protected function renderDataCellContent($row, $data) {
        echo $this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row + 1);
    }

}

?>