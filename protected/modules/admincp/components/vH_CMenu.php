<?php
Yii::import('zii.widgets.CMenu');

class vH_CMenu extends CMenu
{
	public function run()
	{
		//print_r($this->items);
		$this->renderMenu($this->items);
	}
	protected function renderMenuItem($item)
	{
		if(isset($item['publish'])){
			
		}
		
		if(isset($item['url']))
		{
			$label=$this->linkLabelWrapper===null ? $item['label'] : '<'.$this->linkLabelWrapper.'>'.$item['label'].'</'.$this->linkLabelWrapper.'>';
			return CHtml::link($label,$item['url'],isset($item['linkOptions']) ? $item['linkOptions'] : array());
		}
		else
			return CHtml::tag('span',isset($item['linkOptions']) ? $item['linkOptions'] : array(), $item['label']);
	}
}