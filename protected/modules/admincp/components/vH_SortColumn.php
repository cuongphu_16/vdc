<?php
//Yii::import('zii.widgets.grid.CDataColumn');

class vH_SortColumn extends CDataColumn
{
	public
		$headerHtmlOptions = array('style'=>'text-align:center;'),
		$filterHtmlOptions=array('style'=>'text-align:center;'),
		$htmlOptions = array('style'=>'text-align:center'),
		$url = NULL,
		$saveOnBlur = true,
		$csrf = NULL
	;
        
        private $_owner;
	
	public function init()
	{
		parent::init();	
                
                $this->_owner = $this->grid->owner;
		
		if(Yii::app()->request->enableCsrfValidation)
		{
			$csrfName = Yii::app()->request->csrfTokenName;
			$csrfToken = Yii::app()->request->csrfToken;
			$this->csrf = '+"&'.$csrfName.'='.$csrfToken.'"';
		}
		
		$this->registerScript();
	}
	
	protected function registerScript()
	{
		$cs = Yii::app()->getClientScript();
		$css = <<<VH
		input.sort_input{
			text-align:center
		}
VH;
		$cs->registerCss('sort_input_css', vH_Utils::trimTotal($css) );
		if( $this->saveOnBlur ){
			$grid_id = $this->grid->id;
			$ajax_url = isset($this->url) ? $this->url : Yii::app()->createAbsoluteUrl('/admincp/'.$this->_owner->id.'/sortOrder');
			$csrf = $this->csrf;
			
			$script = <<<VH
			$(document).on('change','#{$grid_id} input[name^="sort_input"]',function(){
				$.ajax({
					cache: false,
					type: "POST",
					url: "{$ajax_url}",
					data:  $("#{$grid_id} input").serialize(){$csrf},
					success: function(data){
						jQuery("#{$grid_id}").yiiGridView("update");
					},
					error : function(err){
						Utils.alert(err.responseText);
					},
				});
			});
VH;
			$cs->registerScript('sort_input_script', vH_Utils::trimTotal($script) );
		}
	}
	
	protected function renderFilterCellContent()
	{
		echo CHtml::ajaxLink(
			CHtml::image(Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admincp.assets')).'/images/toolbar/save.png'), 
			isset($this->url) ? $this->url : array('sortOrder'),
			array(
				'type' => 'POST',
				'data' => 'js:$("#'.$this->grid->id.' input").serialize()'.$this->csrf,
				'success' => vH_Utils::trimTotal('function(data){
					jQuery("#'.$this->grid->id.'").yiiGridView("update");
				}'),
				'error' => 'function(err){Utils.alert(err.responseText);}',
			),
			array(
				'title' => Yii::t('app','Save order'),
			)
		);
	}

	protected function renderDataCellContent($row,$data)
	{
		
		if($this->value!==null)
			$value=$this->evaluateExpression($this->value,array('data'=>$data,'row'=>$row));
		elseif($this->name!==null)
			$value = CHtml::value($data,$this->name);
		
		$value_ = ($value===null) ? '' : $this->grid->getFormatter()->format($value,$this->type);
		
		echo CHtml::textField('sort_input['.$data->{$data->tableSchema->primaryKey}.']', $value_, array('size'=>2, 'class'=>'sort_input','id'=>'sort_input_'.$row));
		//echo CHtml::hiddenField($data->tableSchema->primaryKey.'[]', $data->{$data->tableSchema->primaryKey});
	}	
}
?>