<?php

class CoreHelper 
{
	public static function factory()
	{
		$className = __CLASS__;
        return new $className;
	}

	protected function _getCrypt($key = null)
	{
	    $key = '_getCrypt Key';
        $mcrypt= new Mcrypt();
        $mcrypt->init($key);
        return $mcrypt;
	}
    public function encrypt($data)
    {
        return base64_encode($this->_getCrypt()->encrypt((string)$data));
    }
    public function decrypt($data)
    {
        return str_replace("\x0", '', trim($this->_getCrypt()->decrypt(base64_decode((string)$data))));
    }
    
}