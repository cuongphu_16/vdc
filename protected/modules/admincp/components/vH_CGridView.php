<?php

Yii::import('zii.widgets.grid.CGridView');

class vH_CGridView extends CGridView {

    //public $template="{summary}\n{items}\n{pager}";

    public
            $template = "{items}",
            $addFilter = array(),
            $itemsCssClass = 'adminlist',
            $itemAttributes = array('cellspacing' => '0', 'cellpadding' => '6', 'border' => '0'),
            $cssFile = '',
            $ajaxUpdate = true,
            $showFooter = true,
            $pager = array(
                'cssFile' => false,
                'header' => false,
                'pageSize' => 15,
                'class' => 'vH_CLinkPager',
                    ),
            $render_drop_pages = true,
            $showStatisticRow = NULL;

    public function init() {
        $this->cssFile = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admincp.assets')) . '/css/CGridView.css';

        parent::init();

        if (!$this->pager['cssFile'])
            $this->pager['cssFile'] = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admincp.assets')) . '/css/pages.css';

        $this->initAddFilterColumns();
    }

    protected function initAddFilterColumns() {
        if (empty($this->addFilter))
            return false;

        if ($this->addFilter === array()) {
            if ($this->dataProvider instanceof CActiveDataProvider)
                $this->addFilter = $this->dataProvider->model->attributeNames();
            elseif ($this->dataProvider instanceof IDataProvider) {
                // use the keys of the first row of data as the default addFilter
                $data = $this->dataProvider->getData();
                if (isset($data[0]) && is_array($data[0]))
                    $this->addFilter = array_keys($data[0]);
            }
        }
        $id = $this->getId();
        foreach ($this->addFilter as $i => $column) {
            if (is_string($column))
                $column = $this->createDataColumn($column);
            else {
                if (!isset($column['class']))
                    $column['class'] = 'CDataColumn';
                $column = Yii::createComponent($column, $this);
            }
            if (!$column->visible) {
                unset($this->addFilter[$i]);
                continue;
            }
            if ($column->id === null)
                $column->id = $id . '_c' . $i;
            $this->addFilter[$i] = $column;
        }

        foreach ($this->addFilter as $column)
            $column->init();
    }

    public function renderFilterAdd() {
        if (empty($this->addFilter))
            return false;

        echo "<table class=\"{$this->itemsCssClass} filter_add\"" . $this->tableAttributes . " style=\"width:auto; margin:0 0 8px 0\" align=\"left\">\n";
        echo "<thead>\n";
        echo "<tr>\n";
        foreach ($this->addFilter as $column) {
            $column->renderHeaderCell();
        }
        echo "</tr>\n";
        echo "</thead>\n";
        echo "<tr class=\"{$this->filterCssClass}\">\n";
        foreach ($this->addFilter as $column)
            $column->renderFilterCell();
        echo "</tr>\n";
        echo "</table>";
    }

    public function getTableAttributes() {
        $tableAttributes = '';
        if (is_array($this->itemAttributes) && !empty($this->itemAttributes)) {
            foreach ($this->itemAttributes as $k => $v) {
                $tableAttributes.= " {$k}=\"{$v}\"";
            }
        }
        return $tableAttributes;
    }

    public function renderItems() {

        if ($this->dataProvider->getItemCount() > 0 || $this->showTableOnEmpty) {
            $this->renderFilterAdd();
            echo "<div class=\"clr\"></div><table class=\"{$this->itemsCssClass}\" " . $this->tableAttributes . ">\n";
            $this->renderTableHeader();
            ob_start();
            $this->renderTableBody();
            $body = ob_get_clean();
            if ($this->showFooter)
                $this->renderTableFooter();

            echo $body; // TFOOT must appear before TBODY according to the standard.

            if ($this->showStatisticRow && is_array($this->showStatisticRow))
                $this->renderStatisticRow();

            echo "</table>";
        } else
            $this->renderEmptyText();
    }

    public function renderStatisticRow() {
        echo "<tbody>\n";
        echo "<tr style=\"background:#f4f5f7\">\n";
        $data = $this->dataProvider->getData();
        $count_data = count($data);

        foreach ($this->showStatisticRow as $col) {
            if (isset($col)) {
                if (is_string($col)) {
                    $col = $this->createDataColumn($col);
                } else {
                    if (!isset($col['class']))
                        $col['class'] = 'CDataColumn';
                    $col = Yii::createComponent($col, $this);
                }
                $col->init();
                $col->renderDataCell($count_data + 1);
            }else {
                echo '<td style="background:#f4f5f7">&nbsp;</td>';
            }
        }
        echo "</tr>\n";
        echo "</tbody>\n";
    }

    public function renderTableFooter() {
        $hasFilter = $this->filter !== null && $this->filterPosition === self::FILTER_POS_FOOTER;
        $hasFooter = $this->getHasFooter();
        if ($hasFilter || $hasFooter) {
            echo "<tfoot>\n";
            if ($hasFooter) {
                echo "<tr>\n";
                foreach ($this->columns as $column)
                    $column->renderFooterCell();
                echo "</tr>\n";
            }
            if ($hasFilter)
                $this->renderFilter();
            echo "</tfoot>\n";
        }else {
            echo "<tfoot>\n";
            echo '<tr>';
            echo '<th valign="middle" colspan="' . count($this->columns) . '">';
            if ($this->render_drop_pages != false)
                echo '<div class="fr">' . $this->renderDroplistLimit() . '</div>';

            echo '<div class="fl">' . $this->renderSummary() . '</div>';
            echo $this->renderPager();
            echo '</th>';
            echo '</tr>';
            echo "</tfoot>\n";
        }
    }

    public function renderDroplistLimit() {
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }
        $defaultPageSize = Yii::app()->params['defaultPageSize'];
        $pageSize = Yii::app()->user->getState('pageSize', $defaultPageSize);

//        $count = (int) $this->dataProvider->getItemCount();
        $total = (int) $this->dataProvider->getTotalItemCount();
        
        if ($total <= 0)
            return false;

        if ($total <= $defaultPageSize)
            return false;

        if (($total / $defaultPageSize) >= 2) {
            $pageOptions_ = range($defaultPageSize, $total, $defaultPageSize);
        } else {
            $pageOptions_ = array($defaultPageSize, $total);
        }
        $pageOptions = array_combine($pageOptions_, $pageOptions_);
        if ($total % $defaultPageSize != 0) {
            $pageOptions[$total] = 'Tất cả';
        } else {
            $last_key = array_search($total, $pageOptions);
            $pageOptions[$last_key] = 'Tất cả';
        }

        $droplist_limit = array(
            "{$total}" => Yii::t('admincp', 'All')
        );

        return Yii::t('admincp', 'Display') . ' ' . CHtml::dropdownList('pageSize', $pageSize, $pageOptions, array(
                    'onchange' => "$.fn.yiiGridView.update('" . $this->id . "',{ data:{ pageSize: $(this).val() }}) ",
        'style' => 'width: 80px'));
    }

    public function renderSummary() {
        if (($count = $this->dataProvider->getItemCount()) <= 0)
            return '';

        $summaryHtml = '<div class="' . $this->summaryCssClass . '">';
        if ($this->enablePagination) {
            
            $pagination = $this->dataProvider->getPagination();
            $total = $this->dataProvider->getTotalItemCount();
            $start = $pagination->currentPage * $pagination->pageSize + 1;
            $end = $start + $count - 1;
            if ($end > $total) {
                $end = $total;
                $start = $end - $count + 1;
            }
            if (($summaryText = $this->summaryText) === null)
                $summaryText = Yii::t('admincp', 'Result {start}-{end} of {count}');
            $summaryHtml.= strtr($summaryText, array(
                '{start}' => $start,
                '{end}' => $end,
                '{count}' => $total,
                '{page}' => $pagination->currentPage + 1,
                '{pages}' => $pagination->pageCount,
            ));
        }
        else {
            if (($summaryText = $this->summaryText) === null)
                $summaryText = Yii::t('zii', 'Total 1 result.|Total {count} results.', $count);
            $summaryHtml.= strtr($summaryText, array(
                '{count}' => $count,
                '{start}' => 1,
                '{end}' => $count,
                '{page}' => 1,
                '{pages}' => 1,
            ));
        }
        $summaryHtml.= '</div>';
        return $summaryHtml;
    }

    public function renderFilter() {
        if ($this->filter !== null) {
            echo "<tr class=\"{$this->filterCssClass}\">\n";
            foreach ($this->columns as $column) {
                $column->renderFilterCell();
            }
            echo "</tr>\n";
        }
    }

    public function renderPager() {
        if (!$this->enablePagination)
            return;

        $pager = array();
        $class = 'CLinkPager';
        if (is_string($this->pager))
            $class = $this->pager;
        else if (is_array($this->pager)) {
            $pager = $this->pager;
            if (isset($pager['class'])) {
                $class = $pager['class'];
                unset($pager['class']);
            }
        }
        $pager['pages'] = $this->dataProvider->getPagination();

        if ($pager['pages']->getPageCount() > 1) {
            echo '<div class="' . $this->pagerCssClass . '">';
            $a = $this->widget($class, $pager);
            echo '</div>';
        } else
            $this->widget($class, $pager);
    }

}
