<?php

class vH_CButtonColumn extends EButtonColumnWithClearFilters {

    public $onClick_BeforeClear = 'return true;';
    public $url = 'Yii::app()->controller->createUrl(Yii::app()->controller->action->ID,array("clearFilters"=>1))';
    public $template = '{update} {delete}';
    public $headerHtmlOptions = array('style' => 'width:40px', 'class' => 'align_center');
    public $htmlOptions = array('class' => 'align_center');

    public function init() {
        $this->imageUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admincp.assets')) . '/images/toolbar/delete.png';
        $this->updateButtonImageUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admincp.assets')) . '/images/toolbar/edit.png';
        $this->deleteButtonImageUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admincp.assets')) . '/images/toolbar/delete_.png';
        $this->viewButtonImageUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admincp.assets')) . '/images/toolbar/view.png';
//        print_r($this);
//        die;
        parent::init();
    }

}
