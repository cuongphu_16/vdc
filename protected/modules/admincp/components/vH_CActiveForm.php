<?php
class vH_CActiveForm extends CActiveForm
{
	public function init()
	{
		if(empty($this->htmlOptions['enctype']))
			$this->htmlOptions['enctype'] = 'multipart/form-data';
		
		if(!isset($this->htmlOptions['name']))
			$this->htmlOptions['name'] = Yii::app()->params['adminFormName'];
			
		if(!isset($this->htmlOptions['id']))
			$this->htmlOptions['id']=$this->id;
		else
			$this->id = $this->htmlOptions['id'];

		if($this->stateful)
			echo CHtml::statefulForm($this->action, $this->method, $this->htmlOptions);
		else
			echo CHtml::beginForm($this->action, $this->method, $this->htmlOptions);
	}
}