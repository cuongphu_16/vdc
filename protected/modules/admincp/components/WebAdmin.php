<?php
class WebAdmin extends CWebUser
{
	public $loginUrl = array('/admincp/main/login');
	public $allowAutoLogin = true;

	public function pushMessage($string=null, $type='successMsg')
	{
		if ($string!=null ) {
			if($this->hasFlash($type))
				$old = $this->getFlash($type);
			else 
				$old = array();
			array_push($old, $string);
			$this->setFlash($type, $old);
		}
	}
	public function showPutMsg()
	{
		$show ='';
		$successName= 'successMsg';
		$noticeName= 'noticeMsg';
		$errorName= 'errorMsg';
		
		if($this->hasFlash($successName)) {
			$show.= '<div class="flash-success">';
			$msg= (array) $this->getFlash($successName);
			if (count($msg)>1) {
				$show.= mb_substr(implode(', ', $msg), 0,-1, 'utf-8');
			} else {
				$show.= current($msg);
			}
			$show.= '</div>';
		}
		if($this->hasFlash($noticeName)) {
			$show.= '<div class="flash-notice">';
			$msg= (array) $this->getFlash($noticeName);
			if (count($msg)>1) {
				$show.= mb_substr(implode(', ', $msg), 0,-1, 'utf-8');
			} else {
				$show.= current($msg);
			}
			$show.= '</div>';
		}
		if($this->hasFlash($errorName)) {
			$show.= '<div class="flash-error">';
			$msg= (array) $this->getFlash($errorName);
			if (count($msg)>1) {
				$show.= mb_substr(implode(', ', $msg), 0,-1, 'utf-8');
			} else {
				$show.= current($msg);
			}
			$show.= '</div>';
		}
		return $show;
	}
	
	public function putSuccessMsg($string=null)
	{
		$this->pushMessage($string, 'successMsg');
	}
	public function putNoticeMsg($string=null)
	{
		$this->pushMessage($string, 'noticeMsg');
	}
	public function putErrorMsg($string=null)
	{
		$this->pushMessage($string, 'errorMsg');
	}
	
}