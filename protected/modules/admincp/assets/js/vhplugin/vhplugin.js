(function($){
    $.fn.extend({
	
		vH_Checked: function(o){
			var defaults = {
				prefix: 'vH_Check_',
				useFxStyle: false,
				onChange: false // function(isChecked, elm){}
			};
           	var o = $.extend(defaults, o);
			return this.each(function(i){
				var obj = $(this);
				var $new_elm = $('<a>').addClass(o.prefix+'Span').attr('href','javascript:;');
				obj.addClass(o.prefix+'Input').after($new_elm).appendTo($new_elm);
				
				
				if(o.useFxStyle){
					$new_elm.css({
						'display':'inline-block',
						'position':'relative',
						'width':20,
						'height':20,
						'background':'#c00'
					});
					obj.css({
						'position':'absolute',
						'left':'-9999px'	
					});
				}
				
				//check if input checked
				if(obj.attr('checked') == "checked"){
					$new_elm.addClass('checked').css(o.useFxStyle?{'background':'#008000'}:{});
					if(o.onChange)
						o.onChange.call(this, "checked", $new_elm);
				}
				
				$new_elm.unbind('click').bind('click',function(e){
					var ip = $('input',$new_elm);
					e.preventDefault();
					if(ip.attr('checked') == "checked"){
						$(this).removeClass('checked').css(o.useFxStyle?{'background':'#c00'}:{});
						ip.attr('checked',false);
					}else{
						$(this).addClass('checked').css(o.useFxStyle?{'background':'#008000'}:{});
						ip.attr('checked',true);
					}
					if(o.onChange)
						o.onChange.call(this, ip.attr('checked'), $new_elm);
				});
			});
		}
		
		,vH_SelectStyle: function(o){
			var defaults = {
				prefix: 'selectStyle_',
				useStyle: true
			};
            var o = $.extend(defaults, o);
			
			return this.each(function(i){
				var obj = $(this);
				
				var $div = $('<div id="'+o.prefix+i+'">').attr({
					'class': o.prefix	+'Elm'					
				});
				if(o.useStyle){
					$div.attr('style', obj.attr('style'));
				}
				var $txt = $('<span>').attr('class',o.prefix+'Txt').html($(':selected',obj).text());
				var $arrow = $('<span>').attr('class',o.prefix+'Arrow');
				$txt.attr('id',o.prefix+'Txt'+i);
				$div.append($arrow,$txt);
				obj.removeAttr('style').fadeTo(1,1).after($div).appendTo($div);
				
				obj.live('change',function(e){
					var t = $(':selected',this).text();
					$txt.html(t);
					$div.attr('title',t);
				});
            });
		}
		
		,vH_SelectMiniStyle: function(o){
			var defaults = {
				prefix: 'selectStyle_',
				useStyle: true
			};
            var o = $.extend(defaults, o);
			
			return this.each(function(i){
				var obj = $(this);
				
				var $div = $('<div id="'+o.prefix+i+'">').attr({
					'class': o.prefix	+'Elm_mini'					
				});
				if(o.useStyle){
					$div.attr('style', obj.attr('style'));
				}
				//var $txt = $('<span>').attr('class',o.prefix+'Txt').html($(':selected',obj).text());
				var $arrow = $('<span>').attr('class',o.prefix+'Arrow');
				
				$div.append($arrow);
				obj.removeAttr('style').fadeTo(1,1).after($div).appendTo($div);
				
				obj.live('change',function(e){
					var t = $(':selected',this).text();
					//$txt.html(t);
					$div.attr('title',t);
				});
            });
		}
		
       ,vH_Tabs: function(o){
			var defaults = {
				change: null,
				beforeChange: null, /*beforeChange:function(curr_tab, curr_tab_ct){...}*/
				afterChange: null, /*afterChange:function(curr_tab, curr_tab_ct){...}*/
				selectedClass: 'ActiveTab',
				parentTabs: '.vH_Tabs', /*class or id of ul tabs*/
				tabContent: 'vH_Tabs_Content', /*class of tab content*/
				hover: false, /* mouseover event*/
				start: 1,
				tabCtLink: 'rel' /* <a href="..." rel="..." title="title">tab 1</a>*/
			};
             
            var o = $.extend(defaults, o);
         
            return this.each(function()
			{      
				var obj = $(this),
				ulTabs = $(o.parentTabs, obj),
				tabLength = $('li',ulTabs).size(),				
				event_ = o.hover ? 'mouseover' : 'click',
				tab_ct,
				curr_tab_ct;
				
				obj.unbind('keyup.vhtabs').bind('keyup.vhtabs',function(e)
				{
					var key = e.keyCode;
					if(key === 37)
						prevTab();
					else if(key === 39)
						nextTab();
					e.preventDefault();
				});
				
				$('li',ulTabs).each(function(i)
				{
					tab_ct = $('a',this).attr(o.tabCtLink);
					$(tab_ct,obj).addClass(o.tabContent);
					if(o.start > 0 && o.start <= tabLength)
					{
						if((i+1) != o.start){
							$(tab_ct, obj).hide();
						}else{
							curr_tab_ct = $(this, ulTabs);
							$(tab_ct, obj).show();
						}
					}else{
						 if(i > 0){
							$(tab_ct, obj).hide();
						 }else{
							 curr_tab_ct = $(this, ulTabs);
						 }
					}
					if(curr_tab_ct){
						curr_tab_ct.addClass(o.selectedClass);
					}
						
				});
				
				/*tab event*/
				$('a',ulTabs).on(event_,function(e)
				{
					/*e.stopPropagation();*/
					
					if(o.beforeChange){
						o.beforeChange.call(this, $('li.'+o.selectedClass,ulTabs), $('.'+o.tabContent+':visible',obj));
					}
					/*active this tab*/
					var li_tab = $(this).closest('li');
					li_tab.addClass(o.selectedClass);
					$('li',ulTabs).not(li_tab).removeClass(o.selectedClass);
					
					/* show tab content */
					tab_ct = $($(this).attr(o.tabCtLink),obj);
					tab_ct.show();
					
					$('.'+o.tabContent, obj).not(tab_ct).hide();
					
					if(o.afterChange){
						o.afterChange.call(this, li_tab, tab_ct);
					}
					e.preventDefault();
					//return false;
				});					
				
				if(o.start > 0){
					$('.ActiveTab a',ulTabs).trigger('click');
				}
				var currTab = function(){
					var currLi = $('li.'+o.selectedClass,obj);
					var tabIndex = $('li',obj).index(currLi);
					return tabIndex;
				},
				
				nextTab = function()
				{
					var index = currTab();
					if( (index+1) === tabLength){
						return false;
					}else{
						$('li a:eq('+(index+1)+')',obj).trigger('click');
						//console.log('next');
					}
					return;
				},
				prevTab = function(){
					var index = currTab();
					if( index === 0){
						return false;
					}else{
						$('li a:eq('+(index-1)+')',obj).trigger('click');
						//console.log('prev');
					}
					return;
				};
            });
        }
		
		
		,vH_Cycle: function(o){
			var defaults = {
				start: 0,
				speed: 300,
				timeout: 1000,
				width: false,
				height: false,
				pages: false,
				btnNext: false,
				btnPrev: false,
				auto: false,
				mouseWheel: true,
				beforeStart: null,
		        afterEnd: null,
				mouseHover: null,
				mouseOut: null,
				pageClass: 'activeSlide',
				prefix: 'vhslide'
			};
             
            var o = $.extend(defaults, o);
         
            return this.each(function()
			{      
				var obj = $(this);
				
				var children = obj.children(), sl_list;
				var item_len = children.size(), btn_next = $(o.btnNext), btn_prev = $(o.btnPrev), running = false, curr = o.start, sl_w, sl_h, timeout = o.timeout;
				
				obj.append(
					sl_list = $('<div>').addClass('vH_SlideList')
				);
				children.appendTo(sl_list);
				
				var setup = function()
				{
					sl_w = o.width ? o.width : children.eq(0).width(),
					sl_h = o.height ? o.height : children.eq(0).height();
					
					obj.css({
						'width': sl_w,
						'height': sl_h,
						'overflow': 'hidden',
						'position': 'relative'
					});
					
					
					sl_list.css({
						'width': sl_w*item_len,
						'position': 'absolute'
					});
					
					children.each(function(i)
					{
						var page = i+1;
						if(o.pages){
							$(o.pages).append(
								$('<a>').attr({'href': '#'+page}).html(page).addClass(page==1 ? o.pageClass:'')
							)
						}
						var position_left = sl_w*i;
						
						$(this).css({
							'width': sl_w,
							'height': sl_h,
							'overflow': 'hidden',
							'position': 'absolute',
							'left': position_left
						}).attr('item',page);
					});
					
					//pages
					if(o.pages){
						$('a',o.pages).unbind('click.'+o.prefix).bind('click.'+o.prefix,function(e)
						{
							if(!$(this).hasClass(o.pageClass)){
								var index = $('a',o.pages).index(this);
								slide_scroll(index);							
							}
							e.preventDefault();
						});
					}
					
					//prev btn
					if(o.btnPrev){
						btn_prev.unbind('click.'+o.prefix).bind('click.'+o.prefix,function(e){
							fc_prev(this);
							e.preventDefault();
						});
					}
					
					//next btn
					if(o.btnNext){
						btn_next.unbind('click.'+o.prefix).bind('click.'+o.prefix,function(e){
							fc_next(this);	
							e.preventDefault();
						});
					}
					
					//mouse wheel
					if(o.mouseWheel && sl_list.mousewheel){
						sl_list.mousewheel(function(e, d)
						{
							return d>0 ? slide_scroll(curr-1) : slide_scroll(curr+1);
							
						});
					}
					
					autoScroll(o.timeout);
				}
				
				,slide_scroll = function(index)
				{
					if(!running)
					{
						if(index >= 0 && (index+1) <= item_len)
						{
							if(o.beforeStart)
			                    o.beforeStart.call(obj, vis());
								
							curr = index;
							clearTimeout(timeout);
								
							running = true;
							$('a',o.pages).not($('a:eq('+index+')',o.pages)).removeClass(o.pageClass);
							$('a:eq('+index+')',o.pages).addClass(o.pageClass);
							updateBtn();
							
							sl_list.animate({
								left: -(sl_w*index)
							}, o.speed, function(){
								if(o.afterEnd)
									o.afterEnd.call(obj, vis());
								running = false;
							});
						}
						
						autoScroll(o.timeout);
					}
				}
				
				,autoScroll = function(timeout_)
				{
					//auto slide
					if(o.auto){
						timeout = setTimeout(function(){
							slide_scroll( (curr+1) === item_len ? 0 : curr+1 );
						},timeout_);
					}
				}
				
				,fc_prev = function(_this)
				{
					if(!$(_this).hasClass('disabled'))
					{
						if(curr > 0){
							slide_scroll(curr-1);
						}
					}
				}
				
				,fc_next = function(_this)
				{
					if(!$(_this).hasClass('disabled'))
					{
						if((curr+1) <= item_len){ 
							slide_scroll(curr+1);
						}
					}
					return;
				}
				
				,updateBtn = function()
				{
					if(curr === 0){
						btn_prev.attr('disabled',true).addClass('disabled');
						btn_next.removeAttr('disabled').removeClass('disabled');
						//console.log('first sl');
					}else if((curr+1) === item_len){
						btn_next.attr('disabled',true).addClass('disabled');
						btn_prev.removeAttr('disabled').removeClass('disabled');
						//console.log('last sl');
					}else if(curr> 0 && (curr+1) < item_len){
						btn_prev.removeAttr('disabled').removeClass('disabled');
						btn_next.removeAttr('disabled').removeClass('disabled');
						//console.log('no first or last sl');
					}
				}
				
				,curr_slide = function(){
					return $('a',o.pages).index($('a.'+o.pageClass));
				}
				
				,vis = function(){
					return children[curr];
				};
				//end functions
				
				if(!o.width || !o.width)
				{
					var imageTotal = $('img',obj).length, imageCount=0;
					
					$('img',obj).load(function(){
						if(++imageCount == imageTotal)
						setup();
					});
				}else{
					setup();
				}
				
				//slide mouseover
				if(o.mouseHover){
					obj.unbind('mouseover').bind('mouseover mouseleave',function(e){
						
						//console.log(e.type);
						if(e.type === "mouseover"){
							clearTimeout(timeout);
							running = true;
							o.mouseHover.call(obj, vis());
							e.preventDefault();
						}else{
							running = false;
							autoScroll(o.timeout);
							
							if(o.mouseOut)
								o.mouseOut.call(obj, vis());
							e.preventDefault();
						}
					});					
				}
				
				/*if(typeof o.start === "number" && o.start > 0)
					slide_scroll(o.start);
				else					
					updateBtn();*/
			});
		}
		
		,rightMouse: function(o){
			var defaults = {
				escKey: true,
				beforeStart:null,
				disabled:true
			};
             
            var o = $.extend(defaults, o);
         
            return this.each(function()
			{      
				var obj = $(this);
				
				function Add_Context_Menu_Frm()
				{
					var Context_Menu = $('div.Context_Menu');
					if(Context_Menu.size() == 0){
						$('body').append($('<div>').attr('class','Context_Menu'));
					}
					Context_Menu.show();
					return $('.Context_Menu');
				};
				
				function Context_Menu_Hide(){
					$('.Context_Menu').empty().hide();
				}
				
				obj[0].oncontextmenu = function(e)
				{
						
					var Context_Menu = Add_Context_Menu_Frm();					
					Context_Menu.empty().disableTextSelect();
					
					Context_Menu.rightMouse({
						disabled:true
					});
					
					if(o.beforeStart){
						o.beforeStart.call(this,Context_Menu,$(e.target));
					}
					if(o.disabled){
						Context_Menu.hide();
						return false;
					}
					
					var elm = $(e.target),
					$window = $(window),
					menu_pos_left = e.pageX,
					menu_pos_top = e.pageY;

					if((menu_pos_left+Context_Menu.width()) >= $window.width()){
						menu_pos_left = ($window.width()-Context_Menu.width());
						menu_pos_left = menu_pos_left-10;
					}
					
					/*if((menu_pos_top+Context_Menu.height()) >= $window.height()){
						menu_pos_top = ($window.height()-Context_Menu.height());
						menu_pos_top = menu_pos_top-10;
					}*/
					
					Context_Menu.css({
						top: menu_pos_top,
						left: menu_pos_left
					});
					e.preventDefault();
					//return false;
				};
				
				if(o.escKey){
					$(document).unbind('keydown.rm').bind('keydown.rm',function(e){
						var charCode = e.which || e.keyCode;
						if(charCode == 27){
							Context_Menu_Hide();
							return false;
						}
					});
					
				};
				
				$(document).bind('click',function(e)
				{
					var charCode = e.which || e.keyCode;
					var elm = $(e.target);
					
					if(charCode == 1){						
						if(!elm.hasClass('Context_Menu')){
							if(elm.closest('.Context_Menu').size() == 0){
								Context_Menu_Hide();
							}else{
								if(!elm.hasClass('disabled')){
									Context_Menu_Hide();
								}
							}
						}
					}else{
						if(elm.closest('.Context_Menu').size() > 0){
							e.preventDefault();
							return false;
						}else{
							Context_Menu_Hide();
						}
					}
				});
			});
		}
		
		
		,enableTextSelect: function(){
			if ($.browser.mozilla) {
				return this.each(function() {
					$(this).css({
						'MozUserSelect' : ''
					});
				});
			}else if ($.browser.msie) {
				return this.each(function() {
					$(this).unbind('selectstart.disableTextSelect');
				});
			} else {
				return this.each(function() {
					$(this).unbind('mousedown.disableTextSelect');
				});
			}
		}
		
		,disableTextSelect: function(){
			if ($.browser.mozilla) {
				return this.each(function() {
					$(this).css({
						'MozUserSelect' : 'none'
					});
				});
			}else if ($.browser.msie) {
				 return this.each(function() {
					$(this).bind('selectstart.disableTextSelect', function() {
						return false;
					});
				});
			} else {
				 return this.each(function() {
					$(this).bind('mousedown.disableTextSelect', function() {
						return false;
					});
				});
			}
		}
		
		,vH_Like: function(_options){
			var options = {
				label: '.label',
				afterCall: false, //function(obj, resuleDataFromAjax, options){}
				data: {},
				url: rootUrl+'/main/like',
				type: 'post',
				dataType: 'json',
				cache: false
			};
			var o = $.extend(options, _options);
			if(!logged)
				return false;
				
			return this.each(function(i){
				var obj = $(this);
				var label = $(o.label, obj);
				var liked = Number(obj.data('liked'));
				
				if(liked == 1){
					label.addClass('dislike');
				}else{
					label.addClass('like');
				}
				
				obj.on('click',function(){
					o.data.liked = liked;
					
					$.ajax({
						cache: o.cache,
						data: o.data,
						type: o.type,
						url: o.url,
						dataType: o.dataType,
						success: function(data){
							if(data.success){
								if(liked == 0){
									liked = 1;
									label.html(''+lang_js['lang_unlike']+'').addClass('dislike').removeClass('like');
								}else{
									liked = 0;
									label.html(''+lang_js['lang_like']+'').addClass('like').removeClass('dislike');
								}
								if(o.afterCall)
									o.afterCall.call(this, obj, data, o);
							}
						},
						error: function(data){
							alert(data.responseText);
						}
					});
					return false;
				});		
				//end click
			});
		}
		
		,outerHTML: function() {
			return $(this).clone().wrap('<div></div>').parent().html();
		}
		
		,resetForm: function(){
			return $(this).get(0).reset();
		}  

    });
})(jQuery);