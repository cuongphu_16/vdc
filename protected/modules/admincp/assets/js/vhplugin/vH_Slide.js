(function($){
    $.fn.extend({
		vH_Cycle: function(o)
		{
			var defaults = {
				speed: 300,
				width: false,
				height: false,
				pages: false,
				btnNext: false,
				btnPrev: false,
				beforeStart: null,
		        afterEnd: null,
				mouseHover: null,
				mouseOut: null,
				pageClass: 'activeSlide',
				prefix: 'vhslide'
			};
             
            var o = $.extend(defaults, o);
         
            return this.each(function()
			{      
				var obj = $(this);
				
				var children = obj.children(), sl_list;
				var item_len = children.size(), btn_next = $(o.btnNext), btn_prev = $(o.btnPrev), running = false, curr = 0, sl_w, sl_h;
				
				obj.append(
					sl_list = $('<div>').addClass('vH_SlideList')
				).bind('contextmenu',function(){return false;});
				
				children.appendTo(sl_list);
				
				var setup = function()
				{
					sl_w = o.width ? o.width : children.eq(0).width(),
					sl_h = o.height ? o.height : children.eq(0).height();
					
					obj.css({
						'width': sl_w,
						'height': sl_h,
						'overflow': 'hidden',
						'position': 'relative'
					});
					
					
					sl_list.css({
						'width': sl_w*item_len,
						'position': 'absolute'
					});
					
					children.each(function(i)
					{
						var page = i+1;
						if(o.pages){
							$(o.pages).append(
								$('<a>').attr({'href': '#'+page}).addClass(page==1 ? o.pageClass:'').addClass('buttonImg').append(
									$('<img />').attr({
										'src': assetsUrl + '/images/spacer.gif',
										'width': 20,
										'height': 25
									})
								)
							)
						}
						var position_left = sl_w*i;
						
						$(this).css({
							'width': sl_w,
							'height': sl_h,
							'overflow': 'hidden',
							'position': 'absolute',
							'left': position_left
						}).attr('item',page).addClass('slideItem');
					});
					
					//pages
					if(o.pages){
						$('a',o.pages).unbind('click.'+o.prefix).bind('click.'+o.prefix,function(e)
						{
							if(!$(this).hasClass(o.pageClass)){
								var index = $('a',o.pages).index(this);
								slide_scroll(index);							
							}
							e.preventDefault();
						});
					}
					
					//prev btn
					if(o.btnPrev){
						btn_prev.unbind('click.'+o.prefix).bind('click.'+o.prefix,function(e){
							fc_prev(this);
							e.preventDefault();
						}).bind('contextmenu',function(){
							return false;
						});
					}
					
					//next btn
					if(o.btnNext){
						btn_next.unbind('click.'+o.prefix).bind('click.'+o.prefix,function(e){
							fc_next(this);	
							e.preventDefault();
						}).bind('contextmenu',function(){
							return false;
						});
					}
				}
				
				,slide_scroll = function(index)
				{
					if(!running)
					{
						if(index >= 0 && (index+1) <= item_len)
						{
							if(o.beforeStart)
			                    o.beforeStart.call(obj, vis());
								
							curr = index;
								
							running = true;
							$('a',o.pages).not($('a:eq('+index+')',o.pages)).removeClass(o.pageClass);
							$('a:eq('+index+')',o.pages).addClass(o.pageClass);
							
							updateBtn();
							
							sl_list.animate({
								left: -(sl_w*index)
							}, o.speed, function(){
								if(o.afterEnd)
									o.afterEnd.call(obj, vis());
								running = false;
							});
						}
					}
				}
				
				,fc_prev = function(_this)
				{
					if(!$(_this).hasClass('disabled'))
					{
						if(curr > 0){
							slide_scroll(curr-1);
						}
					}
				}
				
				,fc_next = function(_this)
				{
					if(!$(_this).hasClass('disabled'))
					{
						if((curr+1) <= item_len){ 
							slide_scroll(curr+1);
						}
					}
					return;
				}
				
				,updateBtn = function()
				{
					if(curr === 0){
						btn_prev.attr('disabled',true).addClass('disabled');
						btn_next.removeAttr('disabled').removeClass('disabled');
						
					}else if((curr+1) === item_len){
						btn_next.attr('disabled',true).addClass('disabled');
						btn_prev.removeAttr('disabled').removeClass('disabled');
						
					}else if(curr> 0 && (curr+1) < item_len){
						btn_prev.removeAttr('disabled').removeClass('disabled');
						btn_next.removeAttr('disabled').removeClass('disabled');
					}
				}
				
				,curr_slide = function(){
					return $('a',o.pages).index($('a.'+o.pageClass));
				}
				
				,vis = function(){
					return children[curr];
				};
				//end functions
				
				if(!o.width || !o.width)
				{
					var imageTotal = $('img',obj).length, imageCount=0;
					
					$('img',obj).load(function(){
						if(++imageCount == imageTotal)
						setup();
					});
				}else{
					setup();
				}
				
				//slide mouseover
				if(o.mouseHover){
					obj.hover(function(e){
						o.mouseHover.call(obj, vis());
						e.preventDefault();
					},function(e){
						if(o.mouseOut) o.mouseOut.call(obj, vis());
						e.preventDefault();
					});					
				}
				updateBtn();
			});
		}
    });
})(jQuery);