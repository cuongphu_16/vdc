$.fn.selectText = function () {
    return $(this).each(function (index, el) {
        if (document.selection) {
            var range = document.body.createTextRange();
            range.moveToElementText(el);
            range.select();
        } else if (window.getSelection) {
            var range = document.createRange();
            range.selectNode(el);
            window.getSelection().addRange(range);
        }
    });
}

$(window).bind('load', function () {
    $('#loading').stop().hide();
    /*if( $.browser.msie )
     $('#hotKey').css('background','#333');*/

    //setHdrScrollWidth();
});//.bind('resize',setHdrScrollWidth);

function setHdrScrollWidth() {
    $('.hdrScroll').width($(document).width());
}

function checkAll(check)
{
    if (check == true) {
        $('tbody#TableContent :checkbox').attr('checked', true);
        $('#TableContent tr').addClass('selected');
        $('.adminlist').addClass('selected').removeClass('NoCheckAll');
        document.adminForm.boxchecked.value = $('tbody#TableContent :checkbox').size();
    } else {
        $('tbody#TableContent :checkbox').attr('checked', false);
        $('#TableContent tr').removeClass('selected');
        $('.adminlist').removeClass('selected');
        document.adminForm.boxchecked.value = 0;
    }
}

function btnCheck(action) {
    if (action == 'order') {
        checkAll(true);
    }
    if (action == 'cancel') {
        if (confirm('Bạn có chắc chắn muốn Hủy bỏ?')) {
            submitbutton('cancel');
        }
        return false;
    }

    if (document.adminForm.boxchecked.value == 0) {
        alert('Xin hãy chọn 1 mục trong danh sách bên dưới');
    } else {
        if (action == 'remove') {
            if (confirm('Bạn có chắc chắn muốn xóa những mục đã chọn?'))
                submitbutton('remove');


        } else {
            submitbutton(action);
        }
    }
}

function numberOnly(str) {
    if ($.trim(str) == '')
        return 0;
    var num = parseFloat(str.toString().replace(/[^0-9]+/g, ''));
    if (isNaN(num))
        num = 0;
    return num;
}

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if ((charCode > 47 && charCode < 58) || (charCode > 95 && charCode < 106) || (charCode == 46 || charCode == 8))
        return true;
    return false;
}

function CurrencyFormat(num)
{
    num = Math.round((num.toString()).replace(/\$|\,/g, ''));
    if (isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));

    return (((sign) ? '' : '-') + num);
}


jQuery(function () {


    $(document).off('mouseover').on('mouseover mouseleave', 'td,  th', function (e) {
        var table = $(this).closest('table');

        if (e.type == 'mouseover') {
            var tr = $(this).closest('tr');
            var index = $(this.nodeName, tr).index(this);
            $('td, th', table).removeClass('colHover');
            $('tr', table).each(function () {
                $('td:eq(' + index + '), th:eq(' + index + ')', this).addClass('colHover');
            });
        } else {
            $('td, th', table).removeClass('colHover');
        }
    });


    jQuery('#loading').ajaxSend(function () {
        var loading = jQuery(this);
        if (loading.is(':hidden'))
            loading.show();
        loading.css({
            left: ($(window).width() - loading.width()) / 2,
            top: 20
        });


    }).ajaxComplete(function () {
        jQuery(this).hide();
    });

    var fname = $.trim($('input[name="orderBy"]').val());
    var order = $.trim($('input[name="orderType"]').val());
    if (fname != '' && order != '') {
        $('a[fname="' + fname + '"]').after('<img id="order_by_' + fname + '" src="./Images/' + order + '.gif" width="7" height="4" />');
    }

    $('.adminlist th a[rel="orderby"]').bind('click', function () {
        $('input[name="task1"]').val('orderby');
        fname = $.trim($(this).attr('fname'));
        order = $.trim($(this).attr('order'));
        if (fname == '')
            return false;

        $('input[name="orderBy"]').val(fname);

        if (order == '' || order == 'asc')
            $('input[name="orderType"]').val('desc');
        else
            $('input[name="orderType"]').val('asc');

        submitform('');
        return false;
    });

    $('.navbar li.parent').each(function () {
        if ($('li', $('ul', this)).size() == 0) {
            $('ul', this).remove();
        }
    });



    $('.navbar a').bind('contextmenu', function (e)
    {
        e.stopPropagation();
        $(this).unbind('contextmenu');
        return false;
    });


    if ($('.navbar').size() > 0)
    {
        var navbar = $('.navbar');
//			navbar.disableTextSelect(),
        navHasDis = false,
                _false_ = function () {
                    return false;
                };

        if (navbar.hasClass('disabled')) {
            navHasDis = true;
            $('a', navbar).unbind('click').bind('click', _false_);
        }

        $('li', navbar).mouseover(function () {
            if (navHasDis)
                return false;

            $(this).find('ul:first').stop().show();
            if (!$(this).hasClass('no')) {
                $(this).addClass('h');
                $(this).next().addClass('n');
                $(this).prev().addClass('n');
            }
            var sub_cat = $('ul:first', this);
            if (sub_cat.hasClass('sub')) {
                sub_cat.css('left', $(this).outerWidth());
            }

        }).mouseleave(function () {
            if (navHasDis)
                return false;

            $(this).find('ul:first').stop().hide();
            $(this).removeClass('h');
            $(this).next().removeClass('n');
            $(this).prev().removeClass('n');
        });
    }

    $(window).bind('scroll', function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 10) {
            $('body').addClass('scroll');
        } else {
            $('body').removeClass('scroll');
        }
    });


});
