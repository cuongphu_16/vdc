/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



$(function() {
    $( ".sortable" ).sortable({
        placeholder: "ui-state-highlight",
        cusor: "crosshair",
        stop: function(event, ui) {
            $('#result').html('<h4 class="alert_info">Đang sắp xếp ...</h4>');
            var rank = $(this).sortable('toArray').toString();
            var url = SITEURL + $(this).attr('func');
            $.post( url, {
                rank:rank
            }, function(data){
                if(data != "ERROR")                    
                    $('#result').html('<h4 class="alert_success">Sắp xếp danh sách HOT thành công!</h4>');
                else
                    $('#result').html('<h4 class="alert_error">Lỗi! Đã có lỗi xảy ra, không sắp xếp được danh sách HOT!</h4>');
            });                
        }
    });
    $( "#sortable" ).disableSelection();
});
