var jcore = {};
jcore.prototype = function() {
};

jcore.alert = function(message) {
    $('<div>').addClass('alertBox').html(message).dialog({
        'title': 'Thông báo',
        'resizable': false,
        'modal': true,
        'minHeight': 0,
        'minWidth': 0,
        'width': 'auto',
        'buttons': [
            {
                'text': 'OK',
                'click': function() {
                    $(this).dialog('close');
                }
            }
        ]
    });
};

jcore.confirm = function(message, callback) {
    $('<div>').addClass('alertBox').html(message).dialog({
        'title': 'Xác nhận',
        'resizable': false,
        'modal': true,
        'minHeight': 0,
        'minWidth': 0,
        'width': 'auto',
        'buttons': [
            {
                'text': 'Đồng ý',
                'click': function() {
                    $(this).dialog('close');
                    if (callback) {
                        callback.call(this);
                    }
                }
            }
            , {
                'text': 'Hủy bỏ',
                'click': function() {
                    $(this).dialog('close');
                }
            }
        ]
    });
};

jcore.dialogAlignCenter = function() {
    $('.ui-dialog').position({
        my: "center",
        at: "center",
        of: window
    });
};

jcore.parserError = function(formId, json) {
    var form = $(formId);
    $('div[id$="_em_"]', form).hide();
    $.each(json, function(k, v) {
        $('#' + k + '_em_', form).html(v[0]).addClass('errorMessage').show();
    });
};

jcore.setSelectStyle = function(){
    var span = $('<span>').addClass('selectElm');
    $('select').each(function() {
        if (!$(this).parent().hasClass('selectElm') && !$(this).attr('size') ) {
            $(this).wrap(span).after($('<i>')).data();
        }
    });
};

$(window).bind('resize', function() {
    jcore.dialogAlignCenter();
});

$(function() {
    $.extend($.ui.dialog.prototype.options, {
        'resizable': false,
        'modal': true,
        'closeOnEscape': false
    });
    
    
    $(document).on('keydown','.numberOnly', function(e){
        var key = e.keyCode||e.charCode;
//        console.log(e);
        var arr_keys = [8, 46, 37, 39, 110, 190];
        if( (key >= 48 && key <= 57)  || (key >= 96 && key <= 105 ) || $.inArray(key, arr_keys) != -1){
            
        }else{
            return false;
        }
    });
});