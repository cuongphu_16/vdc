$(window).bind('load',function(){
	//loginForm();
});
function showMsg( msg ){
	$('#msg').html( msg ).slideDown(); 
	//loginForm();
}
function loginForm(){
	var window_height = $(window).height();
	var hdr = $('.hdr');
	var footer = $('.footer');
	var loginFrm = $('.loginFrm');
	var frmHeight = hdr.outerHeight()+footer.outerHeight();
	var loginFrmHeight = window_height-frmHeight;
	loginFrm.css({
		height: loginFrmHeight
	});
	$('.loginBox').animate({
		//top: (loginFrmHeight-$('.loginBox').outerHeight())/2
		top: '20%'
	});
}

function checkLogin( frm ){
	var username = frm.username;
	var password = frm.password;
	
	if( $.trim(username.value) == '' ){
		showMsg("Hãy nhập Tên đăng nhập");
		username.focus();
		return false;	
		
	}else if( $.trim(password.value) == '' ){
		showMsg("Hãy nhập Mật khẩu.");
		password.focus();
		return false;	
	}
}