<?php

class AdmincpModule extends CWebModule {

    public $moduleLabel = '';
    private $_assetsUrl;
    private $_assetsUrlRoot;

    public function init() {
//		Yii::app()->cache->flush();
        $this->setImport(array(
            'admincp.models.*',
            'admincp.components.*',
        ));
        //correct the layout path. 
        //$this->setLayoutPath( dirname(__FILE__).'/views/layouts/' );
        header("X-Robots-Tag: noindex, nofollow", true);

        Yii::app()->name = 'Fairway Admin Control Panel';
        Yii::app()->language = 'vi';

        $this->defaultController = 'Main';
        $this->moduleLabel = Yii::app()->name;

        //change component with customer class.
        /*$adminSession = new AdmincpSession();
        $adminSession->setSessionName(Yii::app()->params['area']['admincp'] . '_SESSION');
        Yii::app()->setComponent('session', $adminSession);*/

        Yii::app()->setComponent('admin', new WebAdmin());
        Yii::app()->admin->setReturnUrl(Yii::app()->createAbsoluteUrl('admincp/main/index'));
        //Yii::app()->setHomeUrl( Yii::app()->createAbsoluteUrl('admincp/main/index') );

        Yii::app()->getErrorHandler()->errorAction = 'admincp/main/error';
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        }
        else
            return false;
    }

    public function getAssetsUrl() {
        if ($this->_assetsUrl === null)
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admincp.assets'));
        return $this->_assetsUrl;
    }

    public function setAssetsUrl($value) {
        $this->_assetsUrl = $value;
    }

    public function registerCss($file, $media = 'all') {
        $href = $this->getAssetsUrl() . '/css/' . $file;
        return '<link rel="stylesheet" type="text/css" href="' . $href . '" media="' . $media . '" />';
    }

    public function registerJs($file) {
        $href = $this->getAssetsUrl() . '/js/' . $file;
        return '<script type="text/javascript" src="' . $href . '"></script>';
    }

    public function registerImage($file) {
        return $this->getAssetsUrl() . '/images/' . $file;
    }

    //root media

    public function getAssetsUrlRoot() {
        if ($this->_assetsUrlRoot === null)
            $this->_assetsUrlRoot = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('webroot.themes.' . Yii::app()->theme->name . '.assets'));
        return $this->_assetsUrlRoot;
    }

    public function setAssetsUrlRoot($value) {
        $this->_assetsUrlRoot = $value;
    }

    public function registerCssRoot($file, $media = 'all') {
        $href = $this->getAssetsUrlRoot() . '/css/' . $file;
        return '<link rel="stylesheet" type="text/css" href="' . $href . '" media="' . $media . '" />';
    }

    public function registerJsRoot($file) {
        $href = $this->getAssetsUrlRoot() . '/js/' . $file;
        return '<script type="text/javascript" src="' . $href . '"></script>';
    }

    public function registerImageRoot($file) {
        return $this->getAssetsUrlRoot() . '/images/' . $file;
    }

    public function registerUpload($file) {
        return Yii::app()->request->baseUrl . '/upload/' . $file;
    }

}
