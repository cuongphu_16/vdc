<?php
/**
 * This is the template for generating a controller class file for CRUD feature.
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>

class <?php echo $this->controllerClass; ?> extends AdmincpController
{
	public $layout='index';
	public $defaultAction = 'admin';
	
	protected function getActionsKey()
	{
		return __CLASS__;
	}
	
	public function init()
	{
		parent::init();
		$this->controllerLable = '<?php echo $this->controllerClass?>';
	}


	/*public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}*/
	
	public function actionIndex()
	{
		$this->redirect(array('admin'));
		/*$dataProvider=new CActiveDataProvider('<?php echo $this->modelClass; ?>');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
	}
	
	public function actionAdmin()
	{
		$model = new <?php echo $this->modelClass; ?>('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['<?php echo $this->modelClass; ?>']))
			$model->attributes=$_GET['<?php echo $this->modelClass; ?>'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	
	/*public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}*/
	
	
	public function actionCreate()
	{
		$model = new <?php echo $this->modelClass; ?>('create');
		$this->attachLogBehavior($model);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['<?php echo $this->modelClass; ?>']))
		{
			$model->attributes=$_POST['<?php echo $this->modelClass; ?>'];
				
			if($model->save()){
				Yii::app()->admin->putSuccessMsg('Thêm thành công!');
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$this->attachLogBehavior($model);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['<?php echo $this->modelClass; ?>']))
		{
			$model->attributes=$_POST['<?php echo $this->modelClass; ?>'];
			if($model->save()){
				Yii::app()->admin->putSuccessMsg('Thêm thành công!');
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$this->attachLogBehavior($model);
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	public function actionPublish()
	{
		$ids = $_POST['ids'];
		if(empty($ids)){
			echo 'Bạn phải chọn ít nhất 1 mục.';
		}else{
			$criteria = new CDbCriteria;
			$criteria->condition = '<?php echo $this->tableSchema->primaryKey; ?> in('.implode(',',$ids).')';
			$model = <?php echo $this->modelClass; ?>::model()->updateAll(array('status'=>'1'), $criteria);
			//$this->attachLogBehavior($model);
		}
		return;
	}
	
	public function actionUnpublish()
	{
		$ids = $_POST['ids'];
		if(empty($ids)){
			echo 'Bạn phải chọn ít nhất 1 mục.';
			
		}else{
			$criteria = new CDbCriteria;
			$criteria->condition = '<?php echo $this->tableSchema->primaryKey; ?> in('.implode(',',$ids).')';
			$model = <?php echo $this->modelClass; ?>::model()->updateAll(array('status'=>'0'), $criteria);
			//$this->attachLogBehavior($model);
		}
		return;
	}
	
	public function actionDeleteSelected()
	{
		$ids = $_POST['ids'];		
		if(empty($ids)){
			echo 'Bạn phải chọn ít nhất 1 mục!';
			
		}else if(!is_array($ids)){
			echo 'Yêu cầu không hợp lệ!';
			
		}else{
			foreach($ids as $id){
				$model = $this->loadModel($id);
				$this->attachLogBehavior($model);
				$model->delete();
			}
		}
		return;
	}
	
	public function loadModel($id)
	{
		$model=<?php echo $this->modelClass; ?>::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='<?php echo $this->class2id($this->modelClass); ?>-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
