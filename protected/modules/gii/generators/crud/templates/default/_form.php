<div class="form">

<?php echo "<?php \$form=\$this->beginWidget('vH_CActiveForm', array(
	'id'=>'".$this->class2id($this->modelClass)."-form',
)); ?>\n"; ?>
	
	<?php echo "<?php if(\$msg = Yii::app()->admin->showPutMsg()) echo \$msg.'<br>'; ?>\n"; ?>
	<?php echo "<?php if(\$error = \$form->errorSummary(\$model)) echo \$error.'<br>'; ?>\n"; ?>
	
	<table width="100%" border="0" cellspacing="1" cellpadding="5" class="adminlist">
		<tr>
			<th colspan="2" align="left">Thông tin</th>
		</tr>
		<tr>
			<td colspan="2"><p class="note">Điền đầy đủ các mục có dấu <span class="required">*</span>.</p></td>
		</tr>
	

<?php
$i=0;
foreach($this->tableSchema->columns as $column)
{
	$i++;
	if($column->autoIncrement)
		continue;
?>
	<tr>
		<td valign="top"<?php if($i==2):?> width="200"<?php endif;?>><strong><?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>"; ?></strong></td>		
		<td>
			<?php echo "<?php echo ".$this->generateActiveField($this->modelClass,$column)."; ?>\n"; ?>
			<?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
		</td>
	</tr>

<?php
}
?>
</table>
	<!--<div class="row buttons">
		<?php echo "<?php //echo CHtml::submitButton(\$model->isNewRecord ? 'Create' : 'Save'); ?>\n"; ?>
	</div>-->
	

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>

</div><!-- form -->