<?php echo "<?php\n"; ?>
<?php
$label=$this->pluralize($this->class2name($this->modelClass));
echo "/*\$this->breadcrumbs=array(
	'$label'=>array('index'),
	'Manage',
);\n";
?>

$this->menu=array(
	array('label'=>'List <?php echo $this->modelClass; ?>', 'url'=>array('index')),
	array('label'=>'Create <?php echo $this->modelClass; ?>', 'url'=>array('create')),
);
*/
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('<?php echo $this->class2id($this->modelClass); ?>-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="fl" style="padding-bottom:5px">Có  thể sử dụng các ký tự này ở trước từ khóa cần tìm (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> hoặc <b>=</b>)</div>
<div class="clr"></div>

<?php echo "<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>"; ?>

<!--<div class="search-form" style="display:none">
<?php echo "<?php //\$this->renderPartial('_search',array('model'=>\$model,)); ?>\n"; ?>
</div>-->

<?php echo "<?php"; ?> $this->widget('vH_CGridView', array(
	'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
	'dataProvider'=>$model->search(),
	
	'cssFile' => $this->module->getAssetsUrl().'/css/CGridView.css',
	'itemsCssClass' => 'adminlist',
	'itemAttributes' => array('cellspacing'=> '1', 'cellpadding'=>'3', 'border'=> '0'),
	//'afterAjaxUpdate' => "function(){jQuery('#".CHtml::activeId($model, 'create_time')."').datepicker({'dateFormat':'yy-mm-dd'})}",
	'pager' => array(
		'class' => 'vH_CLinkPager',
	),
	
	'filter'=>$model,
	'columns'=>array(
		array(
			'header'=>'STT',
			'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			'htmlOptions'=>array('style'=>'text-align:center; width:30px; font-weight:bold'),
		),
		array(
			'class'=>'CCheckBoxColumn',
		    'id'=>'ids', 
            'selectableRows'=>2, 
			'htmlOptions'=>array('style'=>'text-align:center; width:30px'),
		),
<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if(++$count==7)
		echo "\t\t/*\n";
	echo "\t\t'".$column->name."',\n";
}
if($count>=7)
	echo "\t\t*/\n";
?>
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
			'htmlOptions'=>array('style'=>'width:40px; text-align:center'),
		),
	),
)); ?>
