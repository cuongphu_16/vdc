<?php

/*
 * Cach su dung
 * public function behaviors() {
  return array(
  'vH_Behaviors' => array(
  'class' => 'application.behaviors.vH_Behaviors',
  'randomPrimaryKey' => true,
  'titleAttribute' => 'title',
  'attachAttribute' => 'picture',
  'createAttribute' => 'create_time',
  'updateAttribute' => 'update_time',
  'userAttribute' => 'user_id',
  ),
  );
  }
 */

class vH_Behaviors extends CActiveRecordBehavior {
    /*
     * Ten truong khoa chinh
     */

    public $randomPrimaryKey = false;
    
    
    /*
     * Ten truong tieu de hoac mot truong bat ky dung de lam ten hinh anh
     * Neu de trong thi ten hinh anh se duoc lay ngau nhien: md5(rand(10000,99999).  uniqid() )
     */
    public $titleAttribute = null;
    
    
    /*
     * Ten truong hinh anh hoac file
     */
    public $attachAttribute = 'picture';
    

    /*
     * Ten truong Thoi gian them moi
     */
    public $createAttribute = 'create_time';
    
    
    /*
     * Ten truong Thoi gian cap nhat
     */
    public $updateAttribute = 'update_time';
    
    
    /*
     * Ten truong cua use duoc lien ket
     * Gan gia tri null neu ko su dung truong nay
     */
    public $userAttribute = 'user_id';
    

    public function beforeSave($event) {
        $model = $event->sender;
        if ($model->isNewRecord) {
            if ($this->randomPrimaryKey) {
                $model->{$model->tableSchema->primaryKey} = SiteHelper::getDataTableID($model);
            }
            if (isset($this->createAttribute)) {
                $model->{$this->createAttribute} = new CDbExpression('NOW()');
            }
            if (isset($this->userAttribute)) {
                $model->{$this->userAttribute} = Yii::app()->user->id;
            }
        } else {
            if (isset($this->updateAttribute)) {
                $model->{$this->updateAttribute} = new CDbExpression('NOW()');
            }
        }
        return $event->sender;
    }

    public function afterSave($event) {
        $model = $event->sender;
        if (isset($this->attachAttribute)) {
            $folder = $model->{$model->tableSchema->primaryKey} . DIRECTORY_SEPARATOR;
            $arr_update = array();

            if (!empty($_FILES[$model->resourceName]['name'][$this->attachAttribute])) {
                if (!$model->isNewRecord) {
                    $model->deleteUpload();
                }
                $new_name = md5(rand(10000, 99999) . uniqid());
                if (isset($this->titleAttribute))
                    $new_name = vH_Utils::alias($model->{$this->titleAttribute});

                $picture = Yii::app()->vH_Upload->uploadFile($model->resourceName, $this->attachAttribute, $model->uploadDir . $folder, $new_name);
                $arr_update[$this->attachAttribute] = $picture;
            }

            if (!empty($arr_update)) {
                if ($model->isNewRecord)
                    $model->isNewRecord = false;

                $model->saveAttributes($arr_update);
            }
        }
    }

    public function afterValidate($event) {
        $model = $event->sender;
        if( isset($this->attachAttribute) ){
            $model->{$this->attachAttribute} = $model->{$this->attachAttribute.'_hidden'};
        }
    }
}

?>
