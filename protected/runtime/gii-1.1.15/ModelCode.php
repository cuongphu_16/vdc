<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => 'j4s_',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
);
