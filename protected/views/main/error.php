<div class="logout_page" align="center" style="padding:50px 0;">
	<h1 style="font-weight:normal; padding-bottom:10px">Đã xảy ra lỗi <?php echo $code; ?></h1>
		
	<div class="error">
		<h3 style="font-weight:normal; padding-bottom:10px"><?php echo CHtml::encode($message); ?></h3>
		<h3 style="font-weight:normal;">
			<?php
				if($code == 2104):
			?>
			<a href="javascript:;" class="checklogin">[Đăng nhập ngay]</a>
			<?php endif;?>
			
			<a href="<?php echo Yii::app()->createAbsoluteUrl('/'); ?>">[Quay lại]</a>
		</h3>
	</div>
</div>
<div class="clr"></div>