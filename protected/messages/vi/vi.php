<?php
return array(
   'Add to ideaboxs' 	=> 'Lưu vào Ideaboxs',
   'Cancel' 			=> 'Hủy bỏ',
   'Select Category' 	=> 'Chọn chuyên mục',
   'Ask a question'		=> 'Đặt câu hỏi',
   'Confirm'			=> 'Xác nhận',
   'Embed'				=> 'Mã nhúng',
   'Private comment'	=> 'Tạo bình luận riêng',
   'Private'			=> 'Chế độ riêng tư',
);
?>