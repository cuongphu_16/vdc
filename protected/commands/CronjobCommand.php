<?php

/* * *
 * @package PHPDocCrontab
 * @subpackage example
 */

class CronjobCommand extends CConsoleCommand {

    /**
     * @cron * * * * *
     * @cron-stdout /tmp/MessageCommand_Staging.log
     * @cron-tags staging
     */
    public function run($args) {
        if (!empty($args)) {
            $action = (string) $args[0];
            $this->$action();
        }
        return true;
    }

    public function sendMailBirthday() {
        $criteria = new CDbCriteria();
        $criteria->condition = "DATE_FORMAT(birthday, '%d-%m') = DATE_FORMAT(NOW(),'%d-%m')";
        $list_user = User::model()->findAllByAttributes(array('type' => 0, 'user_type' => User::TYPE_USER_CANDIDATE), $criteria);

        if (!empty($list_user)) {
            foreach ($list_user as $user):
                MailHelper::sendCronMailBirthday($user);
            endforeach;
        }
    }

    public function sendMailNewsletter() {
        $list_newsletter = Newsletter::model()->findAll();
        $list_job = Job::model()->findAllByAttributes(array('status' => 1, 'deleted' => 0), array('condition' => 'created_at BETWEEN DATE_SUB(NOW(),INTERVAL 7 DAY) AND NOW()', 'limit' => 10, 'order' => 'created_at DESC'));
        $list_career = Post::model()->findAllByAttributes(array('status' => 1, 'type' => Post::TYPE_CAREER_ADVICE), array('condition' => 'created_at BETWEEN DATE_SUB(NOW(),INTERVAL 7 DAY) AND NOW()', 'limit' => 10, 'order' => 'created_at DESC'));
        if (!empty($list_newsletter)):
            foreach ($list_newsletter as $newsletter):
                if (!empty($newsletter)):
                    MailHelper::sendCronMailNewsletter($list_career, $list_job, $newsletter);
                endif;
            endforeach;
        endif;
    }

    public function sendMailAlertJob() {
        $list_alert_job = AlertJob::model()->findAllByAttributes(array('deleted' => 0));
        if (!empty($list_alert_job)):
            foreach ($list_alert_job as $alert_job):
                $date = strtotime($alert_job->updated_at);
                $date2 = strtotime("+" . $alert_job->time . " day", $date);
                $today = time();
                if (date('M d, Y', $date2) == date('M d, Y', $today)):
                    $criteria = new CDbCriteria();
                    $criteria->condition = 'status = 1 and deleted = 0 and language =' . $alert_job->lang;
                    $criteria->order = 'service_id DESC, created_at DESC';
                    if ($alert_job->title != "")
                        $criteria->addCondition("(MATCH (`title`) AGAINST ('" . $alert_job->title . "' IN BOOLEAN MODE))");
                    if ((int) $alert_job->job_category != 0)
                        $criteria->addCondition("job_cate_id LIKE '%," . $alert_job->job_category . ",%'");
                    if ((int) $alert_job->job_level != 0)
                        $criteria->addCondition('level = ' . $alert_job->job_level);
                    if ((int) $alert_job->work_place != 0)
                        $criteria->addCondition("work_place LIKE '%," . $alert_job->work_place . ",%'");
                    if ((int) $alert_job->expected_salary != 0)
                        $criteria->addCondition('expected_salary_from <' . $alert_job->expected_salary . ' and expected_salary_to>=' . $alert_job->expected_salary);
                    if ((int) $alert_job->time == 1) {
                        $criteria->addCondition('created_at BETWEEN DATE_SUB(NOW(),INTERVAL 1 DAY) AND NOW()');
                    } else {
                        $criteria->addCondition('created_at BETWEEN DATE_SUB(NOW(),INTERVAL 7 DAY) AND NOW()');
                    }

                    $list_job = Job::model()->findAll($criteria);
                    if (!empty($list_job)):
                        MailHelper::sendCronMailAlertJob($list_job, $alert_job);
                    endif;
                endif;
            endforeach;
        endif;
    }

    public function sendMailAlertResumes() {
        $list_alert_resume = AlertResume::model()->findAllByAttributes(array('deleted' => 0));
        if (!empty($list_alert_resume)):
            foreach ($list_alert_resume as $alert_resume):
                $criteria = new CDbCriteria();
                $criteria->condition = 'status = 1 and deleted = 0 and lang=' . $alert_resume->lang;
                $criteria->order = 'review_value DESC, created_date DESC';
                if ($alert_resume->title != "")
                    $criteria->addCondition("(MATCH (`title`) AGAINST ('" . $alert_resume->title . "' IN BOOLEAN MODE) OR title LIKE '%" . $alert_resume->title . "%' OR position LIKE '%" . $alert_resume->title . "%' OR career_highlights LIKE '%" . $alert_resume->title . "%')");
                if ((int) $alert_resume->job_category != 0)
                    $criteria->addCondition("job_category LIKE '%," . $alert_resume->job_category . ",%'");
                if ((int) $alert_resume->job_level != 0)
                    $criteria->addCondition('job_level = ' . $alert_resume->job_level);
                if ((int) $alert_resume->work_place != 0)
                    $criteria->addCondition("work_place LIKE '%," . $alert_resume->work_place . ",%'");
                if ((int) $alert_resume->expected_salary != 0)
                    $criteria->addCondition('expected_salary <=' . $alert_resume->expected_salary);
                if ((int) $alert_resume->time == 1) {
                    $criteria->addCondition('created_date BETWEEN DATE_SUB(NOW(),INTERVAL 1 DAY) AND NOW()');
                } else {
                    $criteria->addCondition('created_date BETWEEN DATE_SUB(NOW(),INTERVAL 7 DAY) AND NOW()');
                }
                $criteria->limit = 10;
                //echo $criteria->condition . '=====';
                $list_resume = Resumes::model()->findAll($criteria);
                if (!empty($list_resume)):
                    MailHelper::sendCronMailAlertResume($list_resume, $alert_resume);
                endif;
            endforeach;
        endif;
    }

    public function sendMailInvitedRated() {
        $day = Controller::settingGet('day_send_invited_rate') != FALSE && Controller::settingGet('day_send_invited_rate') != "" ? Controller::settingGet('day_send_invited_rate') : 10;
        $list_applyed = Apply::model()->findAllByAttributes(array('status' => 1, 'deleted' => 0), array('condition' => 'DATE(recruited_date) = DATE(DATE_SUB(NOW(),INTERVAL ' . $day . ' DAY))'));
        if (!empty($list_applyed)) {
            foreach ($list_applyed as $applyed):
                MailHelper::sendCronMailApplyedEmployer($applyed);
                MailHelper::sendCronMailApplyedCandidate($applyed);
            endforeach;
        }
    }

}
