<?php

class CrondCommand extends CConsoleCommand {

    public function actionSendMailBirthday() {
        $criteria = new CDbCriteria();
        $criteria->condition = "DATE_FORMAT(birthday, '%d-%m') = DATE_FORMAT(NOW(),'%d-%m')";
        $list_user = User::model()->findAllByAttributes(array('type' => 0, 'user_type' => User::TYPE_USER_CANDIDATE), $criteria);

        if (!empty($list_user)) {
            foreach ($list_user as $user):
                $mailer = SiteHelper::loadMailer();
                $mailer->clearAddresses();
                if (Controller::settingGet('sendmail_name') != FALSE)
                    $mailer->FromName = html_entity_decode(Controller::settingGet('sendmail_name'));
                if (Controller::settingGet('sendmail_email') != FALSE)
                    $mailer->From = Controller::settingGet('sendmail_email');
                if (Controller::settingGet('sendmail_email') != FALSE)
                    $mailer->ReplyTo = Controller::settingGet('sendmail_email');
                $mail_body = $this->renderPartial("/mail/mail_birthday", array('model' => $user), true);
                $mailer->AddAddress($user->email);
                $mailer->Subject = Yii::t('main', 'Happy birthday!');
                $mailer->Body = $mail_body;
                $mailer->send();
            endforeach;
        }
    }

    public function actionSendMailNewsletter() {
        $list_newsletter = Newsletter::model()->findAll();
        $list_job = Job::model()->findAllByAttributes(array('status' => 1, 'deleted' => 0), array('condition' => 'created_at BETWEEN DATE_SUB(NOW(),INTERVAL 7 DAY) AND NOW()', 'limit' => 10, 'order' => 'created_at DESC'));
        $list_career = Post::model()->findAllByAttributes(array('status' => 1, 'type' => Post::TYPE_CAREER_ADVICE), array('condition' => 'created_at BETWEEN DATE_SUB(NOW(),INTERVAL 7 DAY) AND NOW()', 'limit' => 10, 'order' => 'created_at DESC'));
        if (!empty($list_newsletter)):
            foreach ($list_newsletter as $newsletter):
                $mailer = SiteHelper::loadMailer();
                $mailer->clearAddresses();
                $mail_body = $this->renderPartial("/mail/mail_newsletter_career", array('list_career' => $list_career, 'list_job' => $list_job, 'email' => $newsletter->email), true);
                $mailer->AddAddress($newsletter->email);
                $mailer->Subject = Yii::t('main', 'J4S.vn - Newsletter');
                $mailer->Body = $mail_body;
                $mailer->send();
            endforeach;
        endif;
    }

    public function actionSendMailAlertJob() {
        $list_alert_job = AlertJob::model()->findAllByAttributes(array('deleted' => 0));
        if (!empty($list_alert_job)):
            foreach ($list_alert_job as $alert_job):
                $criteria = new CDbCriteria();
                $criteria->condition = 'status = 1 and deleted = 0';
                $criteria->order = 'service_id DESC, created_at DESC';
                if ($alert_job->title != "")
                    $criteria->addCondition("(MATCH (`title`) AGAINST ('" . $alert_job->title . "' IN BOOLEAN MODE))");
                if ((int) $alert_job->job_category != 0)
                    $criteria->addCondition("job_cate_id LIKE '%," . $alert_job->job_category . ",%'");
                if ((int) $alert_job->job_level != 0)
                    $criteria->addCondition('level = ' . $alert_job->job_level);
                if ((int) $alert_job->work_place != 0)
                    $criteria->addCondition("work_place LIKE '%," . $alert_job->work_place . ",%'");
                if ((int) $alert_job->expected_salary != 0)
                    $criteria->addCondition('expected_salary_from <' . $alert_job->expected_salary . ' and expected_salary_to>=' . $alert_job->expected_salary);
                if ((int) $alert_job->time == 1) {
                    $criteria->addCondition('created_at BETWEEN DATE_SUB(NOW(),INTERVAL 1 DAY) AND NOW()');
                } else {
                    $criteria->addCondition('created_at BETWEEN DATE_SUB(NOW(),INTERVAL 7 DAY) AND NOW()');
                }
                $list_job = Job::model()->findAll($criteria);
                if (!empty($list_job)):
                    $mailer = SiteHelper::loadMailer();
                    $mailer->clearAddresses();
                    $mail_body = $this->renderPartial("/mail/mail_alert_job", array('list_job' => $list_job, 'user' => $alert_job->user), true);
                    $mailer->AddAddress($alert_job->user->email);
                    $mailer->Subject = Yii::t('main', 'Alert Job');
                    $mailer->Body = $mail_body;
                    $mailer->send();
                endif;
            endforeach;
        endif;
    }

    
    /**
     * @cron * * * * *
     * @cron-stdout /tmp/MessageCommand_Staging.log
     * @cron-stderr /tmp/MessageCommand_StagingError.log
     * @cron-tags sendmail
     */
    public function actionSendmail() {
        Job::model()->updateAll(array('deleted'=>1),'job_id=1');
        return true;
    }
}

?>