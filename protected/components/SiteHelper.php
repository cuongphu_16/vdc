<?php

class SiteHelper {

    public $_assetsUrl;
    public static
	    $STATUS_ENABLE = 1,
	    $STATUS_DISABLE = 0;
    public static
	    $POST_FEATURE = 1,
	    $POST_NORMAL = 0;

    public static function getGenderRequired() {
	return array(
	    User::GENDER_MALE => Yii::t('main', 'Male'),
	    User::GENDER_FEMALE => Yii::t('main', 'Female'),
	    User::GENDER_CHILREND => Yii::t('main', 'Child'),
	);
    }

    public static function getGenderLabel($value = null) {
	$array = self::getGenderRequired();
	if ($value === null || !array_key_exists($value, $array))
	    return ' - ';
	return $array[$value];
    }

    public static function getTypeSponsorUser() {
	return array(
	    Sponsor::TYPE_TAITRO => 'Đơn vị tài trợ',
	    Sponsor::TYPE_TOCHUC => 'Đơn vị tổ chức',
	);
    }

    public static function getFocus() {
	return array(
	    self::$POST_NORMAL => Yii::t('main', 'Tin thường'),
	    self::$POST_FEATURE => Yii::t('main', 'Tin nổi bật'),
	);
    }

    public static function getAllowApply() {
	return array(
	    self::$POST_NORMAL => Yii::t('main', 'Không'),
	    self::$POST_FEATURE => Yii::t('main', 'Có'),
	);
    }

    public static
	    $area_northern = 1,
	    $area_central = 2,
	    $area_southern = 3;

    public static function getAllEmployer($filter = array()) {
	return User::model()->getAllEmployer($filter);
    }

    public static function getAllMember() {
	$users = User::model()->findAll();
	$return = array();
	$return[''] = '-- ' . Yii::t('main', 'Select user') . ' --';
	if ($users != NULL) {
	    foreach ($users as $user) {
		$return[$user->user_id] = $user->name;
	    }
	}
	return $return;
    }

    public static function countMange($type_id) {
	if ($type_id > 0) {
	    $count = Manga::model()->countByAttributes(array('status' => 1), array('condition' => 'category LIKE "%,' . $type_id . ',%"'));
	    return $count;
	} else {
	    return 0;
	}
    }

    public static function countFollowManga($manga_id) {
	if ($manga_id > 0) {
	    $count = Follow::model()->countByAttributes(array('manga_id' => $manga_id));
	    return $count;
	} else {
	    return 0;
	}
    }

    public static function checkUserFollow($manga_id) {
	$find = Follow::model()->countByAttributes(array('manga_id' => $manga_id, 'user_id' => Yii::app()->user->id));
	if ($find > 0) {
	    return 'true';
	} else {
	    return 'false';
	}
    }

    public static function getDataTableID($table = NULL, $primary_field = NULL) {
	$size = (int) Yii::app()->params['id_random_length'];
	if ($size <= 0)
	    $size = 8;

	if ($table == NULL || $primary_field == NULL)
	    return false;
	$sql = "SELECT SUBSTRING(CRC32(RAND()), 1, {$size}) as code FROM " . $table . " HAVING code NOT IN (SELECT " . $primary_field . " FROM " . $table . ") limit 1";
	$con = Yii::app()->db;
	$cmd = $con->createCommand($sql);
	//echo $rowCount = $cmd->execute();
	$dataReader = $cmd->query();
	foreach ($dataReader as $row)
	    $id = $row['code'];
	if (empty($id))
	    $id = str_repeat('1', $size);

	return $id;
    }

    public static function postsStatusOptions() {
	return array(
	    self::$STATUS_ENABLE => 'Hiển thị',
	    self::$STATUS_DISABLE => 'Ẩn'
	);
    }

    public static function getTypeCateOptions() {
	return array(
	    Category::TYPE_ABOUT => "Danh mục giới thiệu",
	    Category::TYPE_SERVICES => "Danh mục dịch vụ",
	    Category::TYPE_PROJECT => "Danh mục dự án",
	);
    }

    public static function getTypeCateLabel($value = null) {
	$array = self::getTypeCateOptions();
	if ($value === null || !array_key_exists($value, $array))
	    return ' - ';
	return $array[$value];
    }

    public static function getRateOptions() {
	return array(
	    0 => 'Rất đẹp, hài hòa, dễ đọc',
	    1 => 'Đẹp, dễ dàng tìm truyện',
	    2 => 'Cũng bình thường',
	    3 => 'Xấu',
	    4 => 'Ý kiến khác',
	);
    }

    public static function getRateLabel($value = null) {
	$array = self::getRateOptions();
	if ($value === null || !array_key_exists($value, $array))
	    return ' - ';
	return $array[$value];
    }

    public static function getTypeSponsorLabel($value = null) {
	$array = self::getTypeSponsorUser();
	if ($value === null || !array_key_exists($value, $array))
	    return ' - ';
	return $array[$value];
    }

    public static function getAdv($position) {
	$ads = array();
	if ($position > 0):
	    $ads = Advs::model()->findByAttributes(array('status' => 1, 'position' => $position), array('order' => 'sort_order ASC'));
	endif;
	return $ads;
    }

    public static function getFunctionPackage() {
	return array(
	    ServicePackage::OPTION_RED_BOLD => Yii::t('main', 'Set title to bold with red color 15 days'),
	    ServicePackage::OPTION_HOME_PAGE => Yii::t('main', 'Display on home page 15 days'),
	    ServicePackage::OPTION_SEARCH => Yii::t('main', 'Prioritized in search results'),
	    ServicePackage::OPTION_VIEW_DETAIL => Yii::t('main', 'View contact detail of candidate'),
	);
    }

    public static function getFunctionSubPackage() {
	return array(
	    ServicePackage::OPTION_SUB_HIGHLIGHT_JOB => Yii::t('main', 'Highlight Job'),
	    ServicePackage::OPTION_SUB_RENEW_JOB => Yii::t('main', 'Renew Job'),
	    ServicePackage::OPTION_SUB_VIEW_RESUME => Yii::t('main', 'View contact detail of candidate'),
	);
    }

    public static function getFunctionDisplayType() {
	return array(
	    ServicePackage::TYPE_DISPLAY_BY_DATE => Yii::t('main', 'Display by date'),
	    ServicePackage::TYPE_DISPLAY_BY_VIEW => Yii::t('main', 'Display by view'),
	);
    }

    public static function getFunctionPackageLabel($value = null) {
	$array = self::getFunctionPackage();
	if ($value === null || !array_key_exists($value, $array))
	    return ' - ';
	return $array[$value];
    }

    public static function getCatOptions($option_null = '') {
	$catModel = Category::model();
	$options = $catModel->getCatOptionsTree(0, 0);

	if (!empty($option_null))
	    return array('0' => $option_null) + $options;
	return $options;
    }

    public static function getUserOptions() {
	$catModel = User::model();
	$options = $catModel->getUserTree(0);

	return $options;
    }

    public static function getJobCateOptions($p_id = 0, $language = 0) {
	$catModel = JobCategory::model();
	$options = $catModel->getCatOptionsTree($p_id, $language, 0);

	if (!empty($option_null))
	    return array('0' => $option_null) + $options;
	return $options;
    }

    public static function getCatPostOptions($option_null = '') {
	$catModel = Category::model();
	$options = $catModel->getCatPostTree(0, 0);

	if (!empty($option_null))
	    return array('0' => $option_null) + $options;
	return $options;
    }

    public static function getCatParentOptions($option_null = '') {
	$criteria = new CDbCriteria();
	$criteria->select = 'cat_id, title';
	$criteria->order = 'sort_order ASC';
	$result = Category::model()->findAllByAttributes(array(
	    'p_id' => 0, 'type' => 1
		), $criteria);
	$options = array();
	if (count($result) > 0) {

	    foreach ($result as $v) {
		$cat_id = (int) $v->getAttribute('cat_id');
		$options[$cat_id] = $v->getAttribute('title');
	    }
	}
	if (!empty($option_null))
	    return array('0' => $option_null) + $options;
	return $options;
    }

    public static function getCatLabel($cat_id = 0) {
	$catModel = Category::model()->findByPk($cat_id);
	return $catModel->title;
    }

    public static function getScaleLabel($cat_id = 0) {
	$catModel = Scale::model()->findByPk($cat_id);
	return $catModel->title;
    }

    public static function loadCatOptions($cat_id = 0) {
	if ((int) $cat_id <= 0)
	    return false;

	$cat_parent = SiteHelper::getCatOptions_($cat_id);

	if (!empty($cat_parent)) {
	    $html = '<option value="">-- Phân mục</option>';
	    foreach ($cat_parent as $k => $v) {
		$html.= '<option value="' . $k . '">' . $v . '</option>';
	    }
	}
	return $html;
    }

    public static function getCatOptions_($cat_id = 0) {
	if ((int) $cat_id > 0) {
	    return Category::model()->toOptionHash(array('p_id' => $cat_id));
	} else {
	    return Category::model()->toOptionHash();
	}
	return false;
    }

    public static function getGroupOptions($cat_id = '') {
	if ((int) $cat_id > 0) {
	    return Group::model()->toOptionHash(array('p_id' => $cat_id));
	} else {
	    return Group::model()->toOptionHash();
	}
	return false;
    }

    public static function getMangaOptions($cat_id = '') {
	if ((int) $cat_id > 0) {
	    return Manga::model()->toOptionHash(array('p_id' => $cat_id));
	} else {
	    return Manga::model()->toOptionHash();
	}
	return false;
    }

    public static function getCatTypeOptions_($type = 0, $language = '') {

	return Category::model()->toOptionHash(array('type' => $type, 'language' => $language));
	return false;
    }

    public static function getMemberOptions() {

	return Member::model()->toOptionHash();
	return false;
    }

    public static function getPaymentStatusOptions($img = false) {
	if ($img)
	    return array('0' => 'unpublish14.png', '1' => 'publish14.png');
	else
	    return array('1' => 'Đã thanh toán', '0' => 'Chưa thanh toán');
    }

    public static function getTimeOptions() {

	return array('1' => Yii::t("main", "everyday"), '7' => Yii::t("main", "Every week"));
    }

    public static function getDate($date) {
	if (!empty($date)) {
	    $date_count = time() - strtotime(date('Y-m-d', strtotime($date)));
	    return floor($date_count / (60 * 60 * 24));
	}
    }

    public static function getCategoryPath($category_id, $url = '', $params = '', $categories = NULL) {
	$data = array();
	if (!empty($this))
	    $categories = $this->categories;
	if (!empty($categories)) {
	    foreach ($categories as $cat_) {
		if ($category_id == $cat_->cat_id) {
		    $cat = $cat_;
		    break;
		}
	    }
	    $params_ = array();
	    if (!empty($params)) {
		$params_ = $params;
		$params_[array_search(':space', $params_)] = $cat->alias;
	    }
	    $data[$cat->title] = array($url) + $params_;
	    if ((int) $cat->p_id > 0) {
		$parent = SiteHelper::getCategoryPath($cat->p_id, $url, $params, $categories);
		$data = array_merge($parent, $data);
	    }
	}
	//$data = array_reverse($data,true);
	return $data;
    }

    public static function loadMailer() {
	$mailer = Yii::app()->Mailer->load();
	return $mailer;
    }

    public static function stringToSlug($str) {
	$str = strtolower(trim($str));
	$str = preg_replace('/[^a-z0-9-]/', '-', $str);
	$str = preg_replace('/-+/', "-", $str);
	return $str;
    }

    public static function getTypeCatOptions($type = 0, $option_null = '', $language = 1) {
	$catModel = Category::model();
	$options = $catModel->getTypeCatOptionsTree($type, 0, $language);

	if (!empty($option_null))
	    return array('0' => $option_null) + $options;
	return $options;
    }

    public static function getMultiCategoryLabel($data) {
	if (!empty($data)) {
	    $list = explode(',', $data);
	    $list = array_filter($list);
	    $arr = '';
	    foreach ($list as $value) {
		if ($arr == '')
		    $arr .= SiteHelper::getCateLabel($value);
		else
		    $arr .= ',' . SiteHelper::getCateLabel($value);
	    }
	    return $arr;
	} else {
	    return '';
	}
    }

    public static function getTypeView($type = 0, $option_null = '') {
	$catModel = Category::model();
	$options = $catModel->getTypeViewTree($type, 0);

	if (!empty($option_null))
	    return array('0' => $option_null) + $options;
	return $options;
    }

    public static function getTypeCatOptions_($type = 0, $option_null = '', $language = 1) {
	$catModel = Category::model();
	$options = $catModel->getTypeCatOptionsTrees($type, 0, 0, $language);

	if (!empty($option_null))
	    return array('0' => $option_null) + $options;
	return $options;
    }

    public static function cut_string($str, $len, $more = '...', $charset = 'UTF-8') {
	$str = strip_tags($str);
	$str = str_replace(array("\r\n", "\r", "\n", '"', "'", "&nbsp;"), array("", "", "", '', "", " "), $str);
	if (mb_strlen($str, $charset) > $len) {
	    $arr = explode(' ', $str);
	    $str = mb_substr($str, 0, $len, $charset);
	    $arrRes = explode(' ', $str);
	    $last = $arr[count($arrRes) - 1];
	    unset($arr);
	    if (strcasecmp($arrRes[count($arrRes) - 1], $last))
		unset($arrRes[count($arrRes) - 1]);
	    return implode(' ', $arrRes) . "...";
	}
	return $str;
    }

    public static function Replace($str) {
	$str = strip_tags($str);
	$str = str_replace(array("\r\n", "\r", "\n", '"', "'", "\t", "&", "nbsp;"), array("", "", "", '', "", "", "", ""), $str);
	return $str;
    }

    public static function getRand() {
	$rand = (float) rand() / (float) getrandmax();
	if ($rand < 0.1) {
	    $rand = self::getRand();
	}
	return $rand;
    }

    public static function dateConvert($str) {
	if (empty($str))
	    return false;
	$str = date_format(date_create($str), 'l, H:i - d/m/Y');
	$arr_en_date = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
	$arr_vn_date = array('Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'Chủ nhật');
	return str_replace($arr_en_date, $arr_vn_date, $str);
    }

    public static function getFileType($file) {
	if (function_exists("mime_content_type"))
	    return mime_content_type($file);
	else if (function_exists("finfo_open")) {
	    $finfo = finfo_open(FILEINFO_MIME_TYPE);
	    $type = finfo_file($finfo, $file);
	    finfo_close($finfo);
	    return $type;
	} else {
	    $types = array(
		'mp4' => 'video/mp4', 'mpeg' => 'video/mpeg', 'mpg' => 'video/mpeg',
		'mpe' => 'video/mpeg', 'mov' => 'video/quicktime', 'avi' => 'video/x-msvideo'
	    );
	    $ext = substr($file, strrpos($file, '.') + 1);
	    if (key_exists($ext, $types))
		return $types[$ext];
	    return "unknown";
	}
    }

    public static function acceptableType($type) {
	$array = array("video/mp4", "video/mpeg", "video/quicktime", "video/x-msvideo");
	if (in_array($type, $array))
	    return true;
	return false;
    }

    public static function goodTiming() {
	$n = time();
	if (!isset(Yii::app()->session['lastcheck']) || (isset(Yii::app()->session['lastcheck']) && $n - Yii::app()->session['lastcheck'] > 5))
	    return false;
	return true;
    }

    public static function CameraModelOptions() {
	$model = CameraModel::model()->findAllByAttributes(array('status' => 1), array('order' => 'sort_order ASC'));
	$html = array();
	if (!empty($model)):
	    foreach ($model as $v) {
		$html[$v->model_id] = $v->title;
	    }
	endif;
	$html[0] = Yii::t('main', 'Other');
	return $html;
    }

    public static function CameraModelLabel($value = null) {
	$array = self::CameraModelOptions();
	if ($value === null || !array_key_exists($value, $array))
	    return ' - ';
	return $array[$value];
    }

    public static function DvrRecordOptions() {
	$model = DvrRecord::model()->findAllByAttributes(array('status' => 1), array('order' => 'sort_order ASC'));
	$html = array();
	if (!empty($model)):
	    foreach ($model as $v) {
		$html[$v->dvr_id] = $v->title;
	    }
	endif;
	$html[0] = Yii::t('main', 'Other');
	return $html;
    }

    public static function DvrRecordLabel($value = null) {
	$array = self::DvrRecordOptions();
	if ($value === null || !array_key_exists($value, $array))
	    return ' - ';
	return $array[$value];
    }

    public static function getOrderStatusOptions() {
	return array('0' => 'Chờ duyệt', '1' => 'Đang xử lý', '2' => 'Hoàn thành');
    }

    public static function getOrderStatusLabel($value = null) {
	$array = self::getOrderStatusOptions();
	if ($value === null) {
	    return '-';
	} else {
	    return $array[$value];
	}
    }

    public static function formatSizeUnits($file, $cal = 1) {
	if ($cal == 1) {
	    $bytes = filesize($file);
	} else {
	    $bytes = $file;
	}
	if ($bytes >= 1073741824) {
	    $bytes = number_format($bytes / 1073741824, 2) . ' GB';
	} elseif ($bytes >= 1048576) {
	    $bytes = number_format($bytes / 1048576, 2) . ' MB';
	} elseif ($bytes >= 1024) {
	    $bytes = number_format($bytes / 1024, 2) . ' KB';
	} elseif ($bytes > 1) {
	    $bytes = $bytes . ' bytes';
	} elseif ($bytes == 1) {
	    $bytes = $bytes . ' byte';
	} else {
	    $bytes = '0 bytes';
	}

	return str_replace('.00', '', $bytes);
    }

    public static function calSizeUnits($bytes) {
	list($storage, $title) = explode(' ', $bytes);
	if ($title == 'GB') {
	    $bytes = $storage * 1073741824;
	} elseif ($title == 'MB') {
	    $bytes = $storage * 1048576;
	} elseif ($title == 'KB') {
	    $bytes = $storage * 1024;
	} elseif ($title == 'bytes') {
	    $bytes = $storage;
	} elseif ($title == 'byte') {
	    $bytes = $storage;
	} else {
	    $bytes = 0;
	}

	return $bytes;
    }

    public static function StoragesOptions() {
	$model = Storages::model()->findAllByAttributes(array('status' => 1), array('order' => 'sort_order ASC'));
	$html = array();
	if (!empty($model)):
	    foreach ($model as $v):
		$html[$v->storage_id] = $v->title . (!empty($v->price) ? ' (' . $v->storage . ') - <strong style="color: red">' . number_format($v->price) . '</strong> VNĐ' : '');
	    endforeach;
	endif;
	return $html;
    }

    public static function getResponseDescription($responseCode) {

	switch ($responseCode) {
	    case "0" :
		$result = "Giao dịch thành công - Approved";
		break;
	    case "1" :
		$result = "Ngân hàng từ chối giao dịch - Bank Declined";
		break;
	    case "3" :
		$result = "Mã đơn vị không tồn tại - Merchant not exist";
		break;
	    case "4" :
		$result = "Không đúng access code - Invalid access code";
		break;
	    case "5" :
		$result = "Số tiền không hợp lệ - Invalid amount";
		break;
	    case "6" :
		$result = "Mã tiền tệ không tồn tại - Invalid currency code";
		break;
	    case "7" :
		$result = "Lỗi không xác định - Unspecified Failure ";
		break;
	    case "8" :
		$result = "Số thẻ không đúng - Invalid card Number";
		break;
	    case "9" :
		$result = "Tên chủ thẻ không đúng - Invalid card name";
		break;
	    case "10" :
		$result = "Thẻ hết hạn/Thẻ bị khóa - Expired Card";
		break;
	    case "11" :
		$result = "Thẻ chưa đăng ký sử dụng dịch vụ - Card Not Registed Service(internet banking)";
		break;
	    case "12" :
		$result = "Ngày phát hành/Hết hạn không đúng - Invalid card date";
		break;
	    case "13" :
		$result = "Vượt quá hạn mức thanh toán - Exist Amount";
		break;
	    case "21" :
		$result = "Số tiền không đủ để thanh toán - Insufficient fund";
		break;
	    case "99" :
		$result = "Người sủ dụng hủy giao dịch - User cancel";
		break;
	    default :
		$result = "Giao dịch thất bại - Failured";
	}
	return $result;
    }

    public static function null2unknown($data) {
	if ($data == "") {
	    return "No Value Returned";
	} else {
	    return $data;
	}
    }

    public static function getAreaOptions() {
	return array(
	    self::$area_northern => 'Miền Bắc',
	    self::$area_central => 'Miền Trung',
	    self::$area_southern => 'Miền Nam'
	);
    }

    public static function getCountriesOption($regionId = 0) {
	if ((int) $regionId > 0) {
	    return Countries::model()->toOptionHash(array('country_id' => $regionId));
	} else {
	    return Countries::model()->toOptionHash();
	}
	return false;
    }

    public static function getRegionOption($regionId = 0, $lang = 1) {
	if ((int) $regionId > 0) {
	    return Regions::model()->toOptionHash(array('country_id' => $regionId), $lang);
	} else {
	    return Regions::model()->toOptionHash(array(), $lang);
	}
	return false;
    }

    public static function getRegionOptionExtra($regionId = 0, $lang = 1) {
	if ((int) $regionId > 0) {
	    return Regions::model()->toOptionHashExtra(array('country_id' => $regionId), $lang);
	} else {
	    return Regions::model()->toOptionHashExtra(array(), $lang);
	}
	return false;
    }

    public static function getRegionAndCountries() {
	return Regions::model()->toOptionHash();
    }

    public static function getDistrictOptions_($regionId = 0) {
	if ((int) $regionId > 0) {
	    return District::model()->toOptionHash(array('region_id' => $regionId));
	} else {
	    return array();
	}
	return false;
    }

    public static function loadDistrictOptions($regionId = 0, $lang = 1, $default = true) {
	if ((int) $regionId <= 0)
	    return false;

	$district = SiteHelper::getDistrictOptions_($regionId);

	if (!empty($district)) {
	    if ($default == true)
		$html = '<option value="">' . Yii::t('main', 'Choose District') . '</option>';
	    foreach ($district as $k => $v) {
		if ($lang == 1)
		    $html.= '<option value="' . $k . '">' . $v . '</option>';
		else
		    $html.= '<option value="' . $k . '">' . vH_Utils::stripUnicode($v) . '</option>';
	    }
	}
	return $html;
    }

    public static function loadRegionsOptions($regionId = 0, $default = true) {
	if ((int) $regionId <= 0)
	    return false;

	$district = SiteHelper::getRegionOption($regionId);

	if (!empty($district)) {
	    if ($default == true)
		$html = '<option value="">' . Yii::t('main', 'Choose Region') . '</option>';
	    foreach ($district as $k => $v) {
		$html.= '<option value="' . $k . '">' . $v . '</option>';
	    }
	}
	return $html;
    }

    public static function getRegionLabel($regionId) {
	$region = Regions::model()->findByPk($regionId);
	return $region->title;
    }

    public static function getDistrictLabel($regionId) {
	$region = District::model()->findByPk($regionId);
	return $region->title;
    }

    public static function getCountriesLabel($regionId) {
	$region = Countries::model()->findByPk($regionId);
	return $region->title;
    }

    public static function getLanguageOption() {
	return Language::model()->toOptionHash();
    }

    public static function getCateLabel($cat_id = 0) {
	$catModel = Category::model()->findByPk($cat_id);
	return $catModel->title;
    }

    public static function getServicePackageOption($filter = array()) {
	return ServicePackage::model()->toOptionHash($filter);
    }

    public static function getServicePackageLabel($value = null) {
	$array = self::getServicePackageOption();
	if ($value === null || !array_key_exists($value, $array))
	    return ' - ';
	return $array[$value];
    }

    public static function getLanguageLabel($value = null) {
	$array = self::getLanguageOption();
	if ($value === null || !array_key_exists($value, $array))
	    return ' - ';
	return $array[$value];
    }

    public static function getDateTime() {
	$arr = array(Yii::t('main', 'Sunday'), Yii::t('main', 'Monday'), Yii::t('main', 'Tuesday'), Yii::t('main', 'Wednesday'), Yii::t('main', 'Thursday'), Yii::t('main', 'Friday'), Yii::t('main', 'Saturday'));
	$today = getdate();
	echo Yii::t('main', 'Today') . ': ' . $arr[$today['wday']] . ', ' . $today['mday'] . '/' . $today['mon'] . '/' . $today['year'];
    }

    public static function getLanguage() {
	$language = Language::model()->findAllByAttributes(array('is_web' => 1, 'status' => 1));
	if (isset($language) && !empty($language)) {
	    foreach ($language as $value) {
		$arr[$value->lang_id] = $value->title;
	    }
	}
	return $arr;
    }

    public static function getCategory($type, $lang) {
	$arr_category = Category::model()->findAllByAttributes(array('type' => $type, 'status' => 1, 'language' => $lang), array('order' => 'sort_order ASC', 'select' => 'cate_id,title'));
	return $arr_category;
    }

    public static function getRegionDistrict($id_district) {
	if (!empty($id_district)) {
	    $district = District::model()->findByPk($id_district);
	    return $district->region->title;
	}
    }

    public static function getAutoCode() {
	$member = Member::model()->findBySql('select MAX(member_id) as member_id from {{member}}');
	$new_code = $member->member_id + 1;
	return $new_code;
    }

}
