<?php

class XPCLinkPager extends CLinkPager {

    public
//            $cssFile = '/css/pages.css',
            $hiddenPageCssClass = 'page_hidden',
            $header = false,
            $url='',
            $page_title='page';
            

    protected function createPageButton($label, $page, $class, $hidden, $selected) {
        
        if (empty($this->url))
        {
            if ($hidden) {
                $class.=' ' . $this->hiddenPageCssClass;
                return '<li class="' . $class . '">' . CHtml::link($label, 'javascript:;') . '</li>';
            } else if ($selected) {
                $class.=' ' . $this->selectedPageCssClass;
                return '<li class="' . $class . '">' . CHtml::link($label, 'javascript:;') . '</li>';
            } elseif ($page == 0) {
                return '<li class="' . $class . '">' . CHtml::link($label, $this->createPageUrl($page)) . '</li>';
            } else {
                return '<li class="' . $class . '">' . CHtml::link($label, $this->createPageUrl($page)) . '</li>';
            }
            /* if($hidden || $selected)
              $class.=' '.($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);
              return '<li class="'.$class.'">'.CHtml::link($label,$this->createPageUrl($page)).'</li>'; */
        }else{
            $pos = strpos($this->url,'?');
            if ($pos!=false)
            {
                $url1 = substr($this->url,0,$pos);
                $url2 = substr($this->url, $pos+1);
                $url = explode('&',$url2);
                if (!empty($url))
                {
                    foreach ($url as $key=>$value){
                        if (strpos($value,$this->page_title.'=')!==false){
                            unset($url[$key]);
                        }
                    }
                }
                if (!empty($url))
                    $this->url =$url1.'?'.implode('&',$url);
                else
                    $this->url =$url1;
            }
            if ($hidden) {
                $class.=' ' . $this->hiddenPageCssClass;
                return '<li class="' . $class . '">' . CHtml::link($label, 'javascript:;') . '</li>';
            } else if ($selected) {
                $class.=' ' . $this->selectedPageCssClass;
                return '<li class="' . $class . '">' . CHtml::link($label, 'javascript:;') . '</li>';
            } elseif ($page == 0) {
                return '<li class="' . $class . '">' . CHtml::link($label, $this->url) . '</li>';
            } else {
                if (strpos($this->url,'?')==false)
                    return '<li class="' . $class . '">' . CHtml::link($label, $this->url.'?'.$this->page_title.'='.($page+1)) . '</li>';
                else
                    return '<li class="' . $class . '">' . CHtml::link($label, $this->url.'&'.$this->page_title.'='.($page+1)) . '</li>';
            }
        }
    }

    public function registerClientScript() {
        if ($this->cssFile !== false)
            self::registerCssFile($this->cssFile);
    }

//    protected function createPageUrl($page) {
//        if ($page > 0) {
//            $url = Yii::app()->createUrl(Yii::app()->controller->id . '/' . Yii::app()->controller->action->id . '/lang/' . Yii::app()->language, array('trang' => $page));
//        } else {
//            $url = Yii::app()->createUrl(Yii::app()->controller->id . '/' . Yii::app()->controller->action->id . '/lang/' . Yii::app()->language);
//        }
//
//        return $url;
//    }

    public static function registerCssFile($url = null) {
        if ($url === null)
            return false;
        Yii::app()->getClientScript()->registerCssFile(Yii::app()->controller->assetsUrl . $url);
    }

}

?>