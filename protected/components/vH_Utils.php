<?php

class vH_Utils {
    public static function showPhotoCount($count) {
        if ($count <= 0)
            return 0;

        if ($count < 1000) {
            return 1000;
        }
        $c = $count % 1000;
        if ($c > 0) {
            return ($count - $c) + 1000;
        }
        return $count;
    }

    public static function mediaTypeOptions() {
        return array('1' => 'Picture', '2' => 'Flash', '3' => 'Video');
    }

    public static function getMediaTypeLabel($value = 0) {
        if (($value = (int) $value) <= 0)
            return false;
        $options = vH_Utils::mediaTypeOptions();
        return $options[$value];
    }

    public static function ImgThumbSize($max_width, $max_height, $img_width, $img_height) {
        $new_width = $img_width;
        $new_height = $img_height;

        $x_ratio = $max_width / $img_width;
        $y_ratio = $max_height / $img_height;
        if (($x_ratio >= 1) && ($y_ratio >= 1)) {
            $new_width = $img_width;
            $new_height = $img_height;
        } else {
            $min_ratio = min($x_ratio, $y_ratio);
            $new_width = $min_ratio * $img_width;
            $new_height = $min_ratio * $img_height;
        }
        $x_left = round(($max_width - $new_width) / 2);
        $y_top = round(($max_height - $new_height) / 2);
        $return = array('w' => $new_width, 'h' => $new_height, 'x' => $x_left, 'y' => $y_top);
        return (object) $return;
    }
	
	public static function scaleImage($imgWidth, $imgHeight, $targetWidth, $targetHeight, $fLetterBox) {

            $result = array(
				'width'=>0, 
				'height' => 0, 
				'fScaleToTargetWidth' => true,
			);

            if (($imgWidth <= 0) || ($imgHeight <= 0) || ($targetWidth <= 0) || ($targetHeight <= 0)) {
                return $result;
            }

            // scale to the target width
            $scaleX1 = $targetWidth;
            $scaleY1 = ($imgHeight * $targetWidth) / $imgWidth;

            // scale to the target height
            $scaleX2 = ($imgWidth * $targetHeight) / $imgHeight;
            $scaleY2 = $targetHeight;

            // now figure out which one we should use
            $fScaleOnWidth = ($scaleX2 > $targetWidth);
            if ($fScaleOnWidth) {
                $fScaleOnWidth = $fLetterBox;
            }
            else {
                $fScaleOnWidth = !$fLetterBox;
            }

            if ($fScaleOnWidth) {
                $result['width'] = floor($scaleX1);
                $result['height'] = floor($scaleY1);
                $result['fScaleToTargetWidth'] = true;
            }
            else {
                $result['width'] = floor($scaleX2);
                $result['height'] = floor($scaleY2);
                $result['fScaleToTargetWidth'] = false;
            }
            $result['targetLeft'] = floor(($targetWidth - $result['width']) / 2);
            $result['targetTop'] = floor(($targetHeight - $result['height']) / 2);

            return $result;
        }

    //================

    static function timeago($timestamp, $precision = 2) {
        /* $precision = kieu hien thi 
          1: 1 năm trước
          2: 1 năm 3 tuần trước
          3: 1 năm 3 tuần 5 ngày trước
         */
        $time = time() - $timestamp;
        $a = array(' thập kỷ' => 315576000, ' năm' => 31557600, ' tháng' => 2629800, ' tuần' => 604800, ' ngày' => 86400, ' giờ' => 3600, " phút" => 60, " giây" => 1);
        $i = 0;
        foreach ($a as $k => $v) {
            $$k = floor($time / $v);
            if ($$k)
                $i++;
            $time = $i >= $precision ? 0 : $time - $$k * $v;
            //$s = $$k > 1 ? 's' : '';
            $$k = $$k ? $$k . $k . $s . ' ' : '';
            @$result .= $$k;
        }
        return $result ? $result . 'trước' : '1 giây trước';
    }

    public static function isEmail($email) {
        return filter_var(trim($email), FILTER_VALIDATE_EMAIL);
    }

    public static function isUrl($url) {
        return filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_SCHEME_REQUIRED);
    }

    public static function strto_boolean($str) {
        return filter_var($str, FILTER_VALIDATE_BOOLEAN);
    }

    public static function isFile($file = NULL, $type = 'image') {
        if ($file === NULL || $type === NULL)
            return false;
        $arr_img_type = array('jpg', 'jpeg', 'gif', 'png');
        $fileEx = vH_Utils::fileEx($file);

        switch ($type) {
            case 'image': {
                    if (in_array($fileEx, $arr_img_type))
                        return true;
                    break;
                }
            case 'flash': {
                    if ($fileEx == 'swf')
                        return true;
                    break;
                }
            default:
                return false;
                break;
        }

        return false;
    }

    public static function showEditor($width = 820, $height = 300, $toolbar = 'Basic') {
        include_once $_SERVER['DOCUMENT_ROOT'] . '/FCKEditor/ckeditor.php';
        include_once $_SERVER['DOCUMENT_ROOT'] . '/ckfinder/ckfinder.php';
        $ckeditor = new CKEditor();
        $ckeditor->basePath = '/FCKEditor/';
        if ($width > 0)
            $ckeditor->config['width'] = $width;
        if ($height > 0)
            $ckeditor->config['height'] = $height;
        $ckeditor->config['toolbar'] = $toolbar;
        $ckeditor->config['enterMode'] = 2; //1: <p>, 2: <br>, 3:<div>
        CKFinder::SetupCKEditor($ckeditor, '/ckfinder/');
        return $ckeditor;
    }

    public static function html_decode($str) {
        return vH_Utils::trimTotal(stripcslashes($str));
    }

    public static function userPass($pass = NULL, $salt = NULL) {
        if (empty($salt))
            $salt = Yii::app()->params['salt'];
        $password = md5(md5(md5("$pass|$salt")));
        return $password;
    }

    public static function alias($str = '') {
        $str = trim($str);

        if (empty($str))
            return false;

        $str = vH_Utils::stripUnicode($str);
        $str = preg_replace("/[^a-zA-Z- \d]/i", "", $str);
        $str = vH_Utils::trimTotal($str);
        $str = str_replace(' ', '-', $str);
        return $str;
    }

    public static function stripUnicode($str) {
        if (empty($str))
            return false;
        $unicode = array(
            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
            'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'd' => 'đ',
            'D' => 'Đ',
            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
            'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'i' => 'í|ì|ỉ|ĩ|ị',
            'I' => 'Í|Ì|Ỉ|Ĩ|Ị',
            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
            'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
            'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'y' => 'ý|ỳ|ỷ|ỹ|ỵ',
            'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
        );
        foreach ($unicode as $nonUnicode => $uni)
            $str = preg_replace("/($uni)/i", $nonUnicode, $str);
        return $str;
    }

    /* public static function stripUnicode($str){
      $string	=	array('à','á','ạ','ả','ã','â','ầ','ấ','ậ','ẩ','ẫ','ă','ằ','ắ',
      'ặ','ẳ','ẵ','è','é','ẹ','ẻ','ẽ','ê','ề','ế','ệ','ể','ễ',
      'ì','í','ị','ỉ','ĩ','ò','ó','ọ','ỏ','õ','ô','ồ','ố','ộ',
      'ổ','ỗ','ơ','ờ','ớ','ợ','ở','ỡ','ù','ú','ụ','ủ','ũ','ư',
      'ừ','ứ','ự','ử','ữ','ỳ','ý','ỵ','ỷ','ỹ','đ','À','Á','Ạ',
      'Ả','Ã','Â','Ầ','Ấ','Ậ','Ẩ','Ẫ','Ă','Ằ','Ắ','Ặ','Ẳ','Ẵ',
      'È','É','Ẹ','Ẻ','Ẽ','Ê','Ề','Ế','Ệ','Ể','Ễ','Ì','Í','Ị',
      'Ỉ','Ĩ','Ò','Ó','Ọ','Ỏ','Õ','Ô','Ồ','Ố','Ộ','Ổ','Ỗ','Ơ',
      'Ờ','Ớ','Ợ','Ở','Ỡ','Ù','Ú','Ụ','Ủ','Ũ','Ư','Ừ','Ứ','Ự',
      'Ử','Ữ','Ỳ','Ý','Ỵ','Ỷ','Ỹ','Đ','ê','ù','à');
      $replace=	array('a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
      'e','e','e','e','e','e','e','e','e','e','e','i','i','i','i','i','o',
      'o','o','o','o','o','o','o','o','o','o','o','o','o','o','o','o','u',
      'u','u','u','u','u','u','u','u','u','u','y','y','y','y','y','d','A',
      'A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','E',
      'E','E','E','E','E','E','E','E','E','E','I','I','I','I','I','O','O',
      'O','O','O','O','O','O','O','O','O','O','O','O','O','O','O','U','U',
      'U','U','U','U','U','U','U','U','U','Y','Y','Y','Y','Y','D','e','u','a');
      return str_replace($string,$replace,$str);
      } */

    public static function trimTotal($str_in) {
        $str_in = trim($str_in);
        if (empty($str_in))
            return false;

        $str_in = str_replace(array("\n", "\t"), ' ', $str_in);
        $str_in = explode(' ', $str_in);
        $str_out = '';
        foreach ($str_in as $v) {
            if (($c = trim($v)) != '') {
                $str_out.= ' ' . $c;
            }
        }
        return trim($str_out);
    }

    public static function fileName($filename = NULL) {
        if (empty($filename)) {
            return false;
        }
        $file = pathinfo($filename);
        if (empty($file['extension']))
            return false;

        if (empty($file['filename']))
            $file['filename'] = time();
        else
            $file['filename'] = vH_Utils::alias($file['filename']);
        return $file['filename'] . '.' . $file['extension'];
    }

    //================
    public static function searchNumBack($str, $key) {
        $j = 0;
        for ($i = strlen($str) - 1; $i >= 0; $i--) {
            if (substr($str, $i, strlen($key)) == $key) {
                $j = $i;
                break;
            }
        }
        return $j;
    }

    //cắt chuỗi
    public static function substr_($str, $start, $len, $charset = 'UTF-8') {
        $str = html_entity_decode($str, ENT_QUOTES, $charset);
        if (mb_strlen($str, $charset) > $len) {
            $arr = explode(' ', $str);
            $str = mb_substr($str, $start, $len, $charset);
            $arrRes = explode(' ', $str);
            $last = $arr[count($arrRes) - 1];
            unset($arr);
            if (strcasecmp($arrRes[count($arrRes) - 1], $last))
                unset($arrRes[count($arrRes) - 1]);
            return implode(' ', $arrRes) . "...";
        }
        return $str;
    }

    //chuyen toan bo ky tu thanh chu thuong
    public static function strtolower_($str, $charset = 'UTF-8') {
        return mb_strtolower($str, $charset);
    }

    /* public static function substr_($str, $start = 0, $width, $trimmarker = ' ...', $encoding = 'UTF-8')
      {

      $str_b = mb_strimwidth($str, $start, $width, '', $encoding);
      if(strlen($str_b) == strlen($str))
      return $str;
      $str_a = substr($str_b,0,vH_Utils::searchNumBack($str_b, ' '));
      if(strlen($str_b) > strlen($str_a)){
      return $str_a.$trimmarker;
      }
      return $str_b;
      } */

    //================
    //Chữ đầu tiên thành chữ hoa
    public static function ucfirst_($str, $encoding = 'UTF-8') {
        if (mb_check_encoding($str, $encoding)) {
            $first = mb_substr(
                    mb_strtoupper($str, $encoding), 0, 1, $encoding
            );
            return $first . mb_substr(
                            mb_strtolower($str, $encoding), 1, mb_strlen($str), $encoding
            );
        } else {
            return $str;
        }
    }

    //================
    public static function strlen_($str, $encoding = 'UTF-8') {
        return mb_strlen($str, $encoding);
    }

    //================
    public static function rmdir_($directory, $delete_directory = true) {
        if (!file_exists($directory) || !is_dir($directory))
            return false;

        if (is_readable($directory)) {
            $handle = opendir($directory);
            while (($item = readdir($handle)) !== false) {
                if ($item != '.' && $item != '..' && $item != '.svn') {
                    $path = $directory . DIRECTORY_SEPARATOR . $item;

                    if (is_dir($path))
                        self::rmdir_($path, true);
                    else
                        @unlink($path);
                }
            }
            closedir($handle);
        }
        if ($delete_directory)
            @rmdir($directory);
    }

    public static function rmdirf_($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != ".." && $object != '.svn') {
                    if (filetype($dir . "/" . $object) == "dir")
                        vH_Utils::rmdirf_($dir . "/" . $object);
                    else
                        @unlink($dir . "/" . $object);
                }
            }
            reset($objects);
            @rmdir($dir);
        }
    }

    //================
    public static function fileEx($str = NULL, $dot = false) {
        if (empty($str))
            return false;
        $ex = explode('.', $str);
        $count = count($ex);
        if ($count == 1)
            return false;

        if ($dot == TRUE)
            return '.' . end($ex);
        return end($ex);
    }

    //================
    public static function thumbImg($width, $height, $maxwidth, $maxheight) {
        if (!is_numeric($width) || $width == 0 || !is_numeric($height) || $height == 0)
            return false;

        if ($maxwidth >= $maxheight) {
            if ($width > $height) {
                $newDimensions = self::calcHeight($width, $height, $maxwidth, $maxheight);

                if ($newDimensions['newWidth'] < $maxwidth) {
                    $newDimensions = self::calcWidth($width, $height, $maxwidth, $maxheight);
                }
            } elseif ($height >= $width) {
                $newDimensions = self::calcWidth($width, $height, $maxwidth, $maxheight);

                if ($newDimensions['newHeight'] < $maxheight) {
                    $newDimensions = self::calcHeight($width, $height, $maxwidth, $maxheight);
                }
            }
        } else if ($maxheight > $maxwidth) {
            if ($width >= $height) {
                $newDimensions = self::calcWidth($width, $height, $maxwidth, $maxheight);

                if ($newDimensions['newHeight'] < $maxheight) {
                    $newDimensions = self::calcHeight($width, $height, $maxwidth, $maxheight);
                }
            } elseif ($height > $width) {
                $newDimensions = self::calcHeight($width, $height, $maxwidth, $maxheight);

                if ($newDimensions['newWidth'] < $maxwidth) {
                    $newDimensions = self::calcWidth($width, $height, $maxwidth, $maxheight);
                }
            }
        }
        $newDimensions['left'] = ($newDimensions['newWidth'] - $maxwidth) / 2;
        $newDimensions['top'] = ($newDimensions['newHeight'] - $maxheight) / 2;
        return json_encode($newDimensions);
    }

    private static function calcWidth($width, $height, $maxwidth, $maxheight) {
        $newWidthPercentage = (100 * $maxwidth) / $width;
        $newHeight = ($height * $newWidthPercentage) / 100;

        return array
            (
            'newWidth' => intval($maxwidth),
            'newHeight' => intval($newHeight)
        );
    }

    private static function calcHeight($width, $height, $maxwidth, $maxheight) {
        $newHeightPercentage = (100 * $maxheight) / $height;
        $newWidth = ($width * $newHeightPercentage) / 100;

        return array
            (
            'newWidth' => ceil($newWidth),
            'newHeight' => ceil($maxheight)
        );
    }
    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

}
