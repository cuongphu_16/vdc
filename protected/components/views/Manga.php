<div class="widgets box-story">
    <div class="boxtitle">
	Xếp hạng truyện
	<ul class="nav nav-tabs">
	    <?php if (!empty($this->MangaDays)): ?>
    	    <li class="active"><a href="#day" aria-controls="day" role="tab" data-toggle="tab">Ngày</a></li>
	    <?php endif; ?>
	    <?php if (!empty($this->MangaWeek)): ?>
    	    <li class=""><a href="#week" aria-controls="week" role="tab" data-toggle="tab">Tuần</a></li>
	    <?php endif; ?>
	    <?php if (!empty($this->MangaMonth)): ?>
    	    <li class=""><a href="#month" aria-controls="month" role="tab" data-toggle="tab">Tháng</a></li>
	    <?php endif; ?>
	</ul>
    </div>
    <div class="widgets-content">
	<div class="tab-content">
	    <?php if (!empty($this->MangaDays)): ?>
    	    <div class="tab-pane active" id="day">
    		<ul>
			<?php foreach ($this->MangaDays as $days): ?>
			    <li><a href="<?= $days->url ?>"><?= $days->title ?></a> <a href="#" class="chap">Xem thêm</a></li>
			<?php endforeach; ?>    		    
    		</ul>
    	    </div>
	    <?php endif; ?>
	    <?php if (!empty($this->MangaWeek)): ?>
    	    <div class="tab-pane" id="week">
    		<ul>
			<?php foreach ($this->MangaWeek as $week): ?>
			    <li><a href="<?= $week->url ?>"><?= $week->title ?></a> <a href="#" class="chap">Xem thêm</a></li>
			<?php endforeach; ?>    		    
    		</ul>
    	    </div>
	    <?php endif; ?>
	    <?php if (!empty($this->MangaMonth)): ?>
    	    <div class="tab-pane" id="month">
    		<ul>
			<?php foreach ($this->MangaMonth as $month): ?>
			    <li><a href="<?= $month->url ?>"><?= $month->title ?></a> <a href="#" class="chap">Xem thêm</a></li>
			<?php endforeach; ?>    		    
    		</ul>
    	    </div>
	    <?php endif; ?>
	</div>  
    </div>
</div>
<style>
    .tab-content ul li a{color:#337ab7 !important}
</style>