<h2 class="bordered light"><?= Yii::t('main','The specialist') ?></h2>
<?php if (!empty($this->CacChuyenKhoa)): ?>
    <div class="panel-group panel-list-right" id="accordion">
        <?php $ick=0; foreach ($this->CacChuyenKhoa as $ck): $ick++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title <?= $ick == 1 ? "active" : "" ?>">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $ick ?>">
                            <span><i class="fa fa-plus <?= $ick == 1 ? "fa-minus" : "" ?>"></i></span><?= $ck->title ?>
                        </a>
                    </h4>
                </div>

                <div id="collapse<?= $ick ?>" class="panel-collapse collapse <?= $ick == 1 ? "in" : "" ?>">
                    <div class="panel-body">
                        <?= $ck->description != "" ? SiteHelper::cut_string($ck->description, 150) : SiteHelper::cut_string($ck->content, 150) ?>
                        <br /><br />
                        <a href="<?= Yii::app()->createUrl('main/viewCategory/lang/' . Yii::app()->language, array(
                        'id' => $ck->p_id != "" || $ck->p_id != 0 ? $ck->p_id : $ck->cate_id,
                        'url_cate' => $ck->alias,
            )); ?>"><em>- <?= Yii::t('main', 'View detail') ?></em></a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

<?php else: ?>
    <h4 align="center" style="margin-bottom: 15px"><?= Yii::t('main', 'Database is updating') . "!" ?></h4>
<?php endif; ?>
