<div class="home_video">
    <h1>Video <a href="<?= Yii::app()->createUrl('main/galleryVideo') ?>">Xem tất cả</a></h1>
    <?php
    if (!empty($this->list)):$i = 0;
        foreach ($this->list as $lvideo):$i++;
            if ($i == 1):
                if (!empty($lvideo->link_yt)):
                    $file_link = $lvideo->link_yt;
                else:
                    $file_link = $lvideo->getUrlVideo();
                endif;
                ?>
                <div class="video_hot">
                    <div id='player'></div>
                    <script type='text/javascript'>
                        jwplayer('player').setup({
                        file: '<?= $file_link ?>',
            <?php if ($lvideo->getAvatar(585, 330)->src != null): ?>
                            image: "<?= $lvideo->getAvatar(585, 330)->src ?>",
            <?php endif; ?>
                        width: '100%',
                                aspectratio: '16:9',
                                skin: 'five',
                                logo: {
                                file: "<?= $this->assetsUrl ?>/images/logo_video.png",
                                        link: '<?= $file_link ?>',
                                }
                        });
                    </script>
                    <h2><a href="<?= !empty($lvideo->member) ? $lvideo->member->url : $lvideo->url ?>"><?= $lvideo->title ?></a></h2>
                    <p class="datetime">Ngày <?= date('d.m.Y', strtotime($lvideo->created_at)) ?></p>
                </div>
                <?php
            elseif ($i == 2):
                ?>
                <div class="video_top">
                    <div class="backgorund_overlay fl">
                        <img src="<?=$lvideo->getAvatar(196, 110)->src?>" alt="<?= $lvideo->title ?>" class="fl" />
                        <a href="<?= !empty($lvideo->member) ? $lvideo->member->url : $lvideo->url ?>"><span><i></i></span></a>
                    </div>
                    <h3><a href="<?= !empty($lvideo->member) ? $lvideo->member->url : $lvideo->url ?>"><?= $lvideo->title ?></a></h3>
                    <!--<p class="upload_by">Bởi: <strong>Mr.phu</strong></p>-->
                    <p class="datetime">Ngày <?= date('d.m.Y', strtotime($lvideo->created_at)) ?></p>
                    <!--<p class="views">6.352.398 lượt xem</p>-->
                </div>
                <?php
            else:
                if ($i == 3)
                    echo '<ul class="video_list">';
                ?>
                <li>
                    <h3 class="fl"><a href="<?= !empty($lvideo->member) ? $lvideo->member->url : $lvideo->url ?>"><?= $lvideo->title ?></a></h3>
                    <p class="datetime fr">Ngày <?= date('d.m.Y', strtotime($lvideo->created_at)) ?></p>
                    <div class="clearfix"></div>
                </li>
                <?php
                if ($i == count($this->list) && $i > 2)
                    echo '</ul>';
            endif;
        endforeach;
    endif;
    ?>
    <div align="right" class="view_all_video"><a href="<?= Yii::app()->createUrl('main/galleryVideo') ?>">Xem tất cả</a></div>
</div>
<iframe src="<?= Controller::settingGet('link_file_pptx')?>/embed?start=true&loop=true&delayms=7000" frameborder="0" width="412" height="260" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true" style="margin-top: 20px"></iframe>