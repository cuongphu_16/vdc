<?php if (!empty($this->ThanhVien)): ?>
    <div class="widgets box-member">
        <div class="boxtitle">Xếp hạng thành viên</div>
        <div class="widgets-content">
    	<ul>
		<?php
		$i = 0;
		foreach ($this->ThanhVien as $member): $i++;
		    ?>
		    <li>
			<span class="numb"><?= $i ?></span>
			<a href="#" class="avatar"><img src="<?= $member->getImage(32, 32)->src ?>" width="32" height="32" /></a>
			<aside>
			    <h5><a href="#"><?= !empty($member->firstname) || !empty($member->lastname) ? $member->lastname . ' ' . $member->firstname : $member->username ?></a></h5>
			    <a href="#" class="level"><?= $member->email ?></a>
			</aside>
		    </li>
		<?php endforeach; ?>
    	</ul>
        </div>
    </div>
<?php endif; ?>