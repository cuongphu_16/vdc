<?php
class vH_Upload
{

    public function init(){
    }
	
    static function uploadFile2($model, $root_file = '', $to_dir = '', $new_name = NULL) {

        $filename = $_FILES[$model]['name'][$root_file];
        $file_tmp_name = $_FILES[$model]['tmp_name'][$root_file];
		
		$pathinfo = pathinfo($filename);
        $extension = '.' . $pathinfo['extension'];
        $file_name = $pathinfo['filename'];

        if ($file_name == '') {
            return false;
        }
        if (empty($new_name))
            $file_tmp = self::getFileName($filename, $to_dir);
        else {
            $file_tmp = $new_name;
            //$file_ex = end(explode('.', $new_name));
            //$file_tmp1 = substr($new_name, 0, -(strlen($file_ex) + 1));
            $i = 1;
            while (file_exists($to_dir . $file_tmp . $extension)) {
                $file_tmp = $file_tmp . '-' . $i . $extension;
                $i++;
            }
        }
        //after uploading, file is stored in tmp folder. We copy it to destination $dest
        if (is_uploaded_file($file_tmp_name)) {
            if (!file_exists($to_dir)) {
                mkdir($to_dir, 0777, TRUE);
                exec('chmod 0777 ' . $to_dir);
            }
            if (copy($file_tmp_name, $to_dir . $file_tmp)) {
                return $file_tmp;
            }
        }
        return false;
    }
	
    static function uploadFile($model, $root_file = '', $to_dir = '', $new_name = NULL)
	{
        $filename = $_FILES[$model]['name'][$root_file];
        $file_tmp_name = $_FILES[$model]['tmp_name'][$root_file];
		
		$pathinfo = pathinfo($filename);
        $extension = '.' . $pathinfo['extension'];
        $file_name = $pathinfo['filename'];

        if ($file_name == '') {
            return '';
        }
        if (empty($new_name))
            $file_tmp = self::getFileName($filename, $to_dir);
        else {
            $file_tmp = $new_name;
            //$file_ex = end(explode('.', $new_name));
            //$file_tmp1 = substr($new_name, 0, -(strlen($file_ex) + 1));
            $i = 1;
            while (file_exists($to_dir . $file_tmp.$extension)) {
                $file_tmp = $file_tmp. '-' . $i . $extension;
                $i++;
            }
        }
        //after uploading, file is stored in tmp folder. We copy it to destination $dest
        if (is_uploaded_file($file_tmp_name)) {
            if (!file_exists($to_dir)) {
                mkdir($to_dir,0777,true);
//                exec('chmod 0777' . $to_dir);
            }
            if (copy($file_tmp_name, $to_dir . $file_tmp)) {
                return $file_tmp;
            }
        }
        return '';
    }

    static function uploadFile1($root_file = '', $tmp_file = '', $to_dir = '', $new_name = NULL) {
        $i = 0;
		$pathinfo = pathinfo($root_file);
        $extension = $pathinfo['extension'];
        $file_name = $pathinfo['filename'];

        if ($file_name == '') {
            return '';
        }
        if (!$new_name)
            $file_tmp = self::getFileName($root_file, $to_dir);
        else {
            $file_tmp = $new_name.'.'.$extension;
            //$file_ex = end(explode('.', $new_name));
            //$file_tmp1 = substr($new_name, 0, -(strlen($file_ex) + 1));
            $i = 1;
            while (file_exists($to_dir . $file_tmp)) {
                $file_tmp = $new_name. '-' . $i . '.' . $extension;
                $i++;
            }
        }
        //after uploading, file is stored in tmp folder. We copy it to destination $dest
        if (is_uploaded_file($tmp_file)) {
            if (!file_exists($to_dir)) {
                mkdir($to_dir, 0777, TRUE);
                exec('chmod 0777 ' . $to_dir);
            }
            if (copy($tmp_file, $to_dir . $file_tmp)) {
                return $file_tmp;
            }
        }
        return '';
    }

    static function moveFile($root_file = '', $dest_from = NULL, $dest_to = NULL) {
        if ($root_file == '') {
            return false;
        }
        //if the file is exists, change the name of this file
        $i = 0;
        $file_name = substr($root_file, 0, -4);
        $extension = substr($root_file, -4);
        $file = $file_name;
        if (file_exists($dest_from . $root_file)) {
            if (!file_exists($dest_to)) {
                mkdir($dest_to, 0777, TRUE);
                exec('chmod 777 ' . $dest_to);
            }

            $file_tmp = self::getFileName($root_file, $dest_to);

            //after uploading, file is stored in tmp folder. We copy it to destination $dest
            copy($dest_from . $root_file, $dest_to . $file_tmp);
            self::deleteFile($root_file, $dest_from);
            return $file_tmp;
        }
        return false;
    }

    static function deleteFile($root_file = '', $dest_from = NULL) {
        if ($root_file == '') {
            return;
        }

        if (file_exists($dest_from . $root_file)) {
            unlink($dest_from . $root_file);
            // Delete thumb			
            $thumb = str_replace('thumb_', '', $root_file);
            if (file_exists($thumb))
                unlink($thumb);
        }
        return;
    }

    static function getFileName($root_file = '', $fromdir = NULL, $thumb_ = '') {
        if ($thumb_)
            return $thumb_ . $root_file;
        $root_file = strtolower($root_file);
		
		$pathinfo = pathinfo($root_file);
        $extension = $pathinfo['extension'];
        $file_name = $pathinfo['filename'];
        $name = time();
        $i = 0;
        $name_tmp = $root_file;// . '_' . $i;
        while (file_exists($fromdir . $name_tmp)) {
            $name_tmp = $file_name . '-' . $i.'.'.$extension;
            $i++;
        }
        return $name_tmp;
    }

}

?>