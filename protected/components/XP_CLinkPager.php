<?php

class XP_CLinkPager extends CLinkPager {

    public
            $cssFile = '/css/pages.css',
            $hiddenPageCssClass = 'page_hidden',
            $header = false;

    protected function createPageButton($label, $page, $class, $hidden, $selected) {
        if ($hidden || $selected)
            $class.=' ' . ($hidden ? self::CSS_HIDDEN_PAGE : self::CSS_SELECTED_PAGE);
        return '<li class="' . $class . '">' . CHtml::Link($label, $this->createPageUrl($page)) . '</li>';
    }

    public function registerClientScript() {
        if ($this->cssFile !== false)
            self::registerCssFile($this->cssFile);
    }

    public static function registerCssFile($url = null) {
        if ($url === null)
            return false;
        Yii::app()->getClientScript()->registerCssFile(Yii::app()->controller->assetsUrl . $url);
    }

}
