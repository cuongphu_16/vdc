<?php

class WD_Manga extends Portlet {

    public $lang;

    public function getMangaDays() {
	return Manga::model()->findAllByAttributes(array('status' => 1), array('limit' => 10, 'order' => 'created_at DESC'));
    }

    public function getMangaWeek() {
	return Manga::model()->findAllByAttributes(array('status' => 1), array('limit' => 10, 'order' => 'created_at DESC'));
    }
    
    public function getMangaMonth() {
	return Manga::model()->findAllByAttributes(array('status' => 1), array('limit' => 10, 'order' => 'created_at DESC'));
    }
    
    protected function renderContent() {
	$this->render('Manga', array());
    }

}

?>