<?php
class vH_CLinkPager extends CLinkPager
{
	public
		$cssFile = '/css/yii/pages.css',
		$hiddenPageCssClass = 'page_hidden',
		$header = false;
	
	protected function createPageButton($label,$page,$class,$hidden,$selected)
	{
		if($hidden){
			$class.=' '.$this->hiddenPageCssClass;
			return '<li class="'.$class.'">'.CHtml::link($label,'javascript:;').'</li>';
		}else if($selected){
			$class.=' '.$this->selectedPageCssClass;
			return '<li class="'.$class.'">'.CHtml::link($label,'javascript:;').'</li>';
		}else{
			return '<li class="'.$class.'">'.CHtml::link($label,$this->createPageUrl($page)).'</li>';
		}
		/*if($hidden || $selected)
			$class.=' '.($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);
		return '<li class="'.$class.'">'.CHtml::link($label,$this->createPageUrl($page)).'</li>';*/
	}
	
	public function registerClientScript()
	{
		if($this->cssFile!==false)
			self::registerCssFile($this->cssFile);
	}
	
	public static function registerCssFile($url=null)
	{
		if($url===null)
			return false;		
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->controller->assetsUrl.$url);
	}
	
}
?>