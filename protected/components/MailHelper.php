<?php

class MailHelper {

    public static function sendEmployerChangeProfile($model) {
        if (!empty($model)) {
            $mailer = SiteHelper::loadMailer();
            $mailer->clearAddresses();
            $content = "<p>";
            $content.=Yii::t('main', 'You or another person has just updated your account informations. If its not you please check');
            $content.="</p>";
            $mail_body = Yii::app()->controller->renderPartial("/mail/employer_alert", array('user' => $model, 'content' => $content), true);
            $mailer->AddAddress($model->email);
            $mailer->Subject = Yii::t('main', 'Activity alert');
            $mailer->Body = $mail_body;
            return $mailer->send();
        } else
            return false;
    }

    public static function sendCronMailAlertResume($list_resume, $alert_resume) {
        $mailer = SiteHelper::loadMailer();
        $mailer->clearAddresses();

        if (isset(Yii::app()->controller))
            $controller = Yii::app()->controller;
        else
            $controller = new CController('YiiMail');

        $content = "";
        if (!empty($list_resume)):
            $content.="<ul>";
            foreach ($list_resume as $resume):
                $content.= "<li style='margin-bottom:15px'><a href='" . $resume->serverUrl . "' style='margin-bottom:8px'>" . $resume->title . "</a><br>" . SiteHelper::cut_string($resume->career_highlights, 150) . "</li>";
            endforeach;
            $content.= "</ul>";
        endif;
        $mail_body = Controller::settingGet('mail_alert_resumes');
        if ($mail_body != false && !empty($mail_body)) {
            $mail_body = str_replace('{mail_content}', $content, $mail_body);
            $mail_body = str_replace('{user}', $alert_resume->user->name, $mail_body);
            $mailer->AddAddress($alert_resume->user->email);
            $mailer->Subject = Yii::t('main', 'Alert Resume');
            $mailer->Body = $mail_body;
            if ($mailer->send()) {
                $alert_resume->updated_at = date('Y-m-d H:m:s', time());
                $alert_resume->save();
                return true;
            }
        }
    }

    public static function sendMailReviewAlertToCandidate($model, $employer, $review_title = '', $url = "") {
        if (!empty($review_title) && !empty($url)) {
            $mailer = SiteHelper::loadMailer();
            $mailer->clearAddresses();
            $mail_body = Yii::app()->controller->renderPartial("/mail/mail_review_alert_to_candidate", array('model' => $model, 'employer_name' => $employer->name, 'url' => $url, 'review_title' => $review_title), true);
            $mailer->AddAddress($model->email);
            $mailer->Subject = Yii::t('main', 'Activity alert');
            $mailer->Body = $mail_body;
            return $mailer->send();
        } else
            return false;
    }

    public static function sendMailRenewjob($model, $job) {
        $mailer = SiteHelper::loadMailer();
        $mailer->clearAddresses();
        $mail_body = Yii::app()->controller->renderPartial("/mail/mail_renew", array('model' => $model, 'job_name' => $job->title, 'url' => $job->urlCandidate), true);
        $mailer->AddAddress($model->email);
        $mailer->Subject = Yii::t('main', 'Activity alert');
        $mailer->Body = $mail_body;
        $mailer->send();
    }

    public static function sendMailRegister($model, $text_pass) {
        $mailer = SiteHelper::loadMailer();
        $mailer->clearAddresses();
        $mail_body = Yii::app()->controller->renderPartial("/mail/mail_register", array('model' => $model, 'text_pass' => $text_pass), true);
        $mailer->AddAddress($model->email);
        $mailer->Subject = Yii::t('main', 'Register Successful');
        $mailer->Body = $mail_body;
        $mailer->send();
    }

    public static function sendMailFriend($job, $user, $message, $email) {
        $mailer = SiteHelper::loadMailer();
        $mailer->clearAddresses();
        $mail_body = Yii::app()->controller->renderPartial("/mail/mail_friend", array('job' => $job, 'user' => $user, 'message' => $message, 'email' => $email), true);
        $mailer->AddAddress($_POST['MailFriend']['send_email']);
        $mailer->Subject = $_POST['MailFriend']['title'];
        $mailer->Body = $mail_body;
        $mailer->send();
    }

    public static function sendMailFeedback($email, $message, $user) {
        $mailer = SiteHelper::loadMailer();
        $mailer->clearAddresses();
        if (Controller::settingGet('sendmail_name') != FALSE)
            $mailer->FromName = html_entity_decode(Controller::settingGet('sendmail_name'));
        if (Controller::settingGet('sendmail_email') != FALSE)
            $mailer->From = Controller::settingGet('sendmail_email');
        $mail_body = Yii::app()->controller->renderPartial("/mail/mail_feedback", array('email' => $email, 'message' => $message, 'user' => $user), true);
        $mailer->AddAddress($email);
        $mailer->Subject = Yii::t('main', 'Candidate feedback');
        $mailer->Body = $mail_body;
        $mailer->send();
    }

    public static function sendMailApplyCandidate($user, $job) {
        $mailer = SiteHelper::loadMailer();
        $mailer->clearAddresses();
        
        $mail_body = Yii::app()->controller->renderPartial("/mail/mail_apply_user", array('model' => $user, 'job' => $job), true);
        $mailer->AddAddress($user->email);
        $mailer->Subject = Yii::t('main', 'Apply Job');
        $mailer->Body = $mail_body;
        return $mailer->send();
    }

    public static function sendMailApplyEmployer($user, $job) {
        $mailer_employer = SiteHelper::loadMailer();
        $mailer_employer->clearAddresses();
       
        $mail_body_employer = Yii::app()->controller->renderPartial("/mail/mail_apply_employer", array('model' => $user, 'job' => $job), true);
        $mailer_employer->AddAddress($job->user->email);
        $mailer_employer->Subject = Yii::t('main', 'Candidate Apply Job');
        $mailer_employer->Body = $mail_body_employer;
        return $mailer_employer->send();
    }

    public static function sendMailChangeMail($user) {
        $mailer = SiteHelper::loadMailer();
        $mailer->clearAddresses();
        $mail_body = Yii::app()->controller->renderPartial("/mail/mail_change_email", array('user' => $user), true);
        $mailer->AddAddress($user->email);
        $mailer->Subject = Yii::t('main', 'Edit Email');
        $mailer->Body = $mail_body;
        $mailer->send();
    }

    public static function sendMailForgotPass($email, $members) {
        $mailer = SiteHelper::loadMailer();
        $mailer->clearAddresses();
        $mail_body = Yii::app()->controller->renderPartial("/mail/mail_new_pass", array('model' => $members), true);
        $mailer->AddAddress($email);
        $mailer->Subject = Yii::t('main', 'Get new password');
        $mailer->Body = $mail_body;
        $mailer->send();
    }

    public static function sendCronMailAlertJob($list_job, $alert_job) {
        $mailer = SiteHelper::loadMailer();
        $mailer->clearAddresses();

        if (isset(Yii::app()->controller))
            $controller = Yii::app()->controller;
        else
            $controller = new CController('YiiMail');

        $content = "";
        if (!empty($list_job)):
            $content.="<ul>";
            foreach ($list_job as $job):
                $content.= "<li style='margin-bottom:15px'><a href='" . $job->serverUrl . "' style='margin-bottom:8px'>" . $job->title . "</a><br>" . SiteHelper::cut_string($job->description, 150) . "</li>";
            endforeach;
            $content.= "</ul>";
        endif;
        $mail_body = Controller::settingGet('mail_alert_job');
        if ($mail_body != false && !empty($mail_body)) {
            $mail_body = str_replace('{mail_content}', $content, $mail_body);
            $mail_body = str_replace('{user}', $alert_job->user->firstname . " " . $alert_job->user->lastname, $mail_body);
            $mailer->AddAddress($alert_job->user->email);
            $mailer->Subject = Yii::t('main', 'Alert Job');
            $mailer->Body = $mail_body;
            if ($mailer->send()) {
                $alert_job->updated_at = date('Y-m-d H:m:s', time());
                $alert_job->save();
                return true;
            }
        }
    }

    public static function sendCronMailNewsletter($list_career, $list_job, $newsletter) {
        $mailer = SiteHelper::loadMailer();
        $mailer->clearAddresses();

        if (isset(Yii::app()->controller))
            $controller = Yii::app()->controller;
        else
            $controller = new CController('YiiMail');

        $content = "";
        if (!empty($list_job)):
            $content.= "<h3>" . Yii::t('main', 'Latest Work') . "</h3>";
            $content.= "<ul>";
            foreach ($list_job as $job):
                $content.= "<li style='margin-bottom:15px'><a href='" . $job->urlCandidate . "' style='margin-bottom:8px'>" . $job->title . "</a><br>" . SiteHelper::cut_string($job->description, 150) . "</li>";
            endforeach;
            $content.= "</ul>";
        endif;

        if (!empty($list_career)):
            $content.= "<h3>" . Yii::t('main', 'Latest Career Advice') . "</h3>";
            $content.="<ul>";
            foreach ($list_career as $career):
                $content.= "<li style='margin-bottom:15px'><a href='" . $career->url . "' style='margin-bottom:8px'>" . $career->title . "</a></li>";
            endforeach;
            $content.= "</ul>";
        endif;
        $mail_body = Controller::settingGet('mail_newsletter_career');
        if ($mail_body != false && !empty($mail_body)) {
            $mail_body = str_replace('{mail_content}', $content, $mail_body);
            $mail_body = str_replace('{user}', $newsletter->user->name, $mail_body);
            $mailer->AddAddress($newsletter->email);
            $mailer->Subject = Yii::t('main', 'J4S.vn - Newsletter');
            $mailer->Body = $mail_body;
            $mailer->send();
        }
    }

    public static function sendCronMailBirthday($user) {
        $mailer = SiteHelper::loadMailer();
        $mailer->clearAddresses();

        if (isset(Yii::app()->controller))
            $controller = Yii::app()->controller;
        else
            $controller = new CController('YiiMail');

        $content = "";

        $content.= "<p>" . $this->lang == 1 ? Controller::settingGet('happy_birthday_message') : Controller::settingGet('happy_birthday_message_en') . "</p>";

        $mail_body = Controller::settingGet('mail_birthday');
        if ($mail_body != false && !empty($mail_body)) {
            $mail_body = str_replace('{mail_content}', $content, $mail_body);
            $mail_body = str_replace('{user}', $user->firstname . " " . $user->lastname, $mail_body);
            $mailer->AddAddress($user->email);
            $mailer->Subject = Yii::t('main', 'Happy birthday!');
            $mailer->Body = $mail_body;
            $mailer->send();
        }
    }

    public static function sendCronMailApplyedEmployer($applyed) {
        $mailer = SiteHelper::loadMailer();
        $mailer->clearAddresses();

        if (isset(Yii::app()->controller))
            $controller = Yii::app()->controller;
        else
            $controller = new CController('YiiMail');

        $content = "";

        $content.= "<p>" . Yii::t('main', 'You got resumes {resumes} apply to work {job}', array('{resumes}' => '<strong>' . $applyed->resumes->title . '</strong>', '{job}' => '<strong>' . $applyed->job->title . '</strong>')) . "</p>";
        $content.= "<p>" . Yii::t('main', 'Please go to job management and send comment - rated candidate') . "</p>";

        $mail_body = Controller::settingGet('mail_invited_rate');
        if ($mail_body != false && !empty($mail_body)) {
            $mail_body = str_replace('{mail_content}', $content, $mail_body);
            $mail_body = str_replace('{user}', $applyed->job->user->name, $mail_body);
            $mailer->AddAddress($applyed->job->user->email);
            $mailer->Subject = Yii::t('main', 'Invited rated');
            $mailer->Body = $mail_body;
            $mailer->send();
        }
    }
    public static function sendCronMailApplyedCandidate($applyed) {
        $mailer = SiteHelper::loadMailer();
        $mailer->clearAddresses();

        if (isset(Yii::app()->controller))
            $controller = Yii::app()->controller;
        else
            $controller = new CController('YiiMail');

        $content = "";

        $content.= "<p>" . Yii::t('main', 'You have been admitted to the {job} when applying for jobs with resumes {resumes}', array('{resumes}' => '<strong>' . $applyed->resumes->title . '</strong>', '{job}' => '<strong>' . $applyed->job->title . '</strong>')) . "</p>";
        $content.= "<p>" . Yii::t('main', 'Please go to job management and send comment - rated employer') . "</p>";

        $mail_body = Controller::settingGet('mail_invited_rate');
        if ($mail_body != false && !empty($mail_body)) {
            $mail_body = str_replace('{mail_content}', $content, $mail_body);
            $mail_body = str_replace('{user}', $applyed->resumes->user->name, $mail_body);
            $mailer->AddAddress($applyed->resumes->user->email);
            $mailer->Subject = Yii::t('main', 'Invited rated');
            $mailer->Body = $mail_body;
            $mailer->send();
        }
    }

}

?>