<?php

class WD_ThanhVien extends Portlet {

    public $lang;

    public function getThanhVien() {
	return User::model()->findAllByAttributes(array('status' => 1, 'type' => 0), array('limit' => 10, 'order' => 'created_at DESC'));
    }

    protected function renderContent() {
	$this->render('ThanhVien', array());
    }

}

?>