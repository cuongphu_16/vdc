<?php

class Controller extends CController {

    public
            $layout = 'column1',
            $menu = array(),
            $breadcrumbs = array(),
            $pageDesc = '', //meta description
            $lang,
            $langCode,
            $langSupport,
            $pageKey = '', //meta keywords
            $shareTitle = '',
            $shareLink = '',
            $shareContent = '',
            $shareImage = '',
            $pageImg = '',
            $siteName = '',
            $_categories = array(),
            $_pageNoIndex = false,
            $photoView,
            $_countPhotos = 0,
            $site_suggets = null,
            $register_url = "",
            $login_url = "",
            $urlNeed = array(),
            $employerUrl = "",
            $mypage = true,
            $mobile = true,
            $mobile_type = '',
            $extra_service = array(),
            $show_package = false,
            $search_by_lang = true,
            $arr_cat = array();
    public static $_rootUrl;
    private $_assetsUrl, $_pageTitle;

    public function __construct($id, $module = null) {
        parent::__construct($id, $module);
    }

    public function init() {
        if (substr(Yii::app()->request->requestUri, -1, 1) === '/' && !empty(Yii::app()->request->pathInfo)) {
            $new_url = substr(Yii::app()->request->requestUri, 0, -1);
            $this->redirect($new_url);
            Yii::app()->end();
        }
        if (!Yii::app()->request->isAjaxRequest) {
            Yii::app()->session['token'] = Yii::app()->params['salt'];
        }
        $lang_code = isset($_GET['lang']) ? $_GET['lang'] : '';

        $lang = Language::model()->findByAttributes(
                array("status" => 1, "code" => $lang_code, 'is_web' => 1));

        if ($lang == null) {
            $lang_default = Language::model()->findByAttributes(
                    array("status" => 1, "default" => 1, 'is_web' => 1));

            if ($lang_default !== null)
                $this->redirect($this->createUrl("/$lang_default->code"));
            else
                $this->redirect($this->createUrl('/vi'));
        } else {
            $this->lang = $lang->lang_id;
            $this->langCode = $lang->code;
        }
        $this->langSupport = Language::model()->findAllByAttributes(
                array("status" => 1, 'is_web' => 1));
        Yii::app()->language = $this->langCode;
    }

    public function getClientScript() {
        return Yii::app()->clientScript;
    }

    public function getCategories() {
        if (empty($this->_categories)) {
            $sql = array(
                'condition' => 'status=1',
                'order' => 'sort_order asc'
            );

            $this->_categories = Category::model()->findAll($sql);

            if (empty($this->_categories)) {
                $this->_categories = array();
            }
        }
        return $this->_categories;
    }

    protected function beforeAction($action) {
//        if ($action->id!=='voteSMS' && $action->id!=='index' && $action->id!=='listEntrant' && $action->id!=='memberDetail'){
//                header('Content-Type: text/html; charset=utf-8');
//                die('Hệ thống đang bị quá tải, vui lòng thử lại sau ít phút');
//        }
        if (!in_array($this->id, array('photos', 'products')) and ! Yii::app()->request->isAjaxRequest)
            unset(Yii::app()->session['condition']);

        if (!Yii::app()->request->isAjaxRequest && Yii::app()->controller->id !== 'user' && (Yii::app()->controller->id !== 'employer' || ($action->id != 'login' && $action->id != 'register'))) {
            unset(Yii::app()->session['redirect_url']);
        }

        //echo $action->id;
        if (!Yii::app()->request->isAjaxRequest && Yii::app()->controller->id !== 'employer' || (Yii::app()->controller->id === 'employer' && ($action->id !== 'paymentcomplete'))) {
            unset(Yii::app()->session['complete_payment']);
        }

        //if (Yii::app()->controller->id!='employer' && !Yii::app()->request->isAjaxRequest) unset(Yii::app()->session['package_register']);
        //echo Yii::app()->controller->id;

        if (Yii::app()->controller->id != 'candidate' && !Yii::app()->request->isAjaxRequest)
            unset(Yii::app()->session['page_size']);
        return true;
    }

    public function beforeRender($view) {
        parent::beforeRender($view);
        if (!$this->_pageNoIndex) {
            if (!empty($this->pageDesc)) {
                Yii::app()->clientScript->registerMetaTag(html_entity_decode(SiteHelper::cut_string($this->pageDesc, 300)), 'description');
            }

            if (!empty($this->pageKey)) {
                Yii::app()->clientScript->registerMetaTag(html_entity_decode(SiteHelper::cut_string($this->pageKey, 300)), 'keywords');
            }

            if (!empty($this->pageImg)) {
                Yii::app()->clientScript->registerLinkTag('image_src', NULL, $this->pageImg);
            }
            if (!empty($this->shareTitle)) {
                Yii::app()->clientScript->registerMetaTag(html_entity_decode(SiteHelper::cut_string($this->shareTitle, 300)), null, null, array('property' => 'og:title'));
            } else {
                Yii::app()->clientScript->registerMetaTag(html_entity_decode(SiteHelper::cut_string(Controller::settingGet("share_title"), 300)), null, null, array('property' => 'og:title'));
            }
            if (!empty($this->shareLink)) {
                Yii::app()->clientScript->registerMetaTag(html_entity_decode(SiteHelper::cut_string($this->shareLink, 300)), null, null, array('property' => 'og:url'));
            } else {
                Yii::app()->clientScript->registerMetaTag(html_entity_decode(SiteHelper::cut_string(Controller::settingGet("share_link"), 300)), null, null, array('property' => 'og:url'));
            }
            if (!empty($this->shareContent)) {
                Yii::app()->clientScript->registerMetaTag(html_entity_decode(SiteHelper::cut_string($this->shareContent, 300)), null, null, array('property' => 'og:description'));
                Yii::app()->clientScript->registerMetaTag(html_entity_decode(SiteHelper::cut_string($this->shareContent, 300)), null, null, array('itemprop' => 'description'));
            } else {
                Yii::app()->clientScript->registerMetaTag(html_entity_decode(SiteHelper::cut_string(Controller::settingGet("share_content"), 300)), null, null, array('property' => 'og:description'));
                Yii::app()->clientScript->registerMetaTag(html_entity_decode(SiteHelper::cut_string(Controller::settingGet("google_content"), 300)), null, null, array('itemprop' => 'description'));
            }
            if (!empty($this->shareImage)) {
                Yii::app()->clientScript->registerMetaTag($this->shareImage, null, null, array('property' => 'og:image'));
                Yii::app()->clientScript->registerMetaTag($this->shareImage, null, null, array('itemprop' => 'image'));
            } elseif ((Controller::settingGet("shareImage") != "" && Controller::settingGet("shareImage") != FALSE)) {
                preg_match('/\< *[img][^\>]*[src] *= *[\"\']{0,1}([^\"\']*)/i', Controller::settingGet("shareImage"), $matches);
                if ($matches[1] != "") {
                    if (strpos($matches[1], "http://") === false) {
                        Yii::app()->clientScript->registerMetaTag('http://' . $_SERVER['HTTP_HOST'] . $matches[1], null, null, array('property' => 'og:image'));
                    } else {
                        Yii::app()->clientScript->registerMetaTag($matches[1], null, null, array('property' => 'og:image'));
                    }
                }
            } else {
                Yii::app()->clientScript->registerMetaTag('http://' . $_SERVER['HTTP_HOST'] . Yii::app()->theme->baseUrl . '/assets/images/logo-face.jpg', null, null, array('property' => 'og:image'));
                Yii::app()->clientScript->registerMetaTag('http://' . $_SERVER['HTTP_HOST'] . Yii::app()->theme->baseUrl . '/assets/images/logo-face.jpg', null, null, array('itemprop' => 'image'));
            }
            if (empty($this->siteName)) {
                Yii::app()->clientScript->registerMetaTag(html_entity_decode(SiteHelper::cut_string(Controller::settingGet("website_name"), 300)), null, null, array('property' => 'og:site_name'));
                Yii::app()->clientScript->registerMetaTag(html_entity_decode(SiteHelper::cut_string(Controller::settingGet("website_name"), 300)), null, null, array('itemprop' => 'name'));
            }
        }
        return true;
    }

    protected function afterRender($view, &$output) {
        parent::afterRender($view, $output);
        return true;
    }

    public function getAssetsUrl() {
        if ($this->_assetsUrl === NULL) {
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('webroot.themes.' . Yii::app()->theme->name . '.assets'), false, -1, YII_DEBUG);
        }
        return $this->_assetsUrl;
    }

    public static function getRootUrl() {
        if (empty(self::$_rootUrl)) {
            self::$_rootUrl = $_rootUrl = Yii::app()->request->baseUrl;
            if (empty($_rootUrl))
                self::$_rootUrl = Yii::app()->request->hostInfo;
        }

        return self::$_rootUrl;
    }

    public function setPageTitle($title = NULL) {
        $this->_pageTitle = $title;
        return;
    }

    //<link rel="canonical" href="link" />
    public function setCanonLink($link) {
        Yii::app()->clientScript->registerLinkTag('canonical', NULL, $link);
    }

    public function getPageTitle() {
        $page_tile = Controller::settingGet('website_name');
        if (!empty($this->_pageTitle))
            $page_tile = $page_tile . ' - ' . $this->_pageTitle;
        else
            $page_tile = $page_tile . ' - ' . ucfirst($this->id) . ' - ' . ucfirst($this->action->id);
        return $this->_pageTitle = $page_tile;
    }

    public function setPageNoIndex() {
        $this->_pageNoIndex = true;
        $value = 'noindex, nofollow, noarchive, nosnippet';
        header("X-Robots-Tag: {$value}");
        header("HTTP/1.0 404 Not Found");
        //header('Status: 404 Not Found');
        Yii::app()->clientScript->registerMetaTag($value, 'robots');
    }

    public static function getUploadDir($folder = NULL) {
        if ($folder === NULL)
            return false;
        return Yii::app()->params['uploadPath'][$folder];
    }

    public static function getUploadUrl($folder = NULL, $id = 0, $not_int = '') {
        if ($folder === NULL)
            return false;
        if ($id != 'default')
            if ($not_int == 'true')
                $id = trim($id);
            else
                $id = (int) $id;

        $url = Yii::app()->request->getBaseUrl(true) . Yii::app()->params['uploadPath']['view'][$folder];
        if ($id > 0 || $id == 'default')
            $url.= $id . '/';
        return $url;
    }

    //======================	
    public function getUserinfo() {
        if (Yii::app()->user->isGuest)
            return false;
        $user = User::model()->findByPk(Yii::app()->user->id);
        if (empty($user) || $user->status != 1) {
            Yii::app()->user->logout();
            $this->refresh();
            return false;
        }
        return $user;
    }

    //======================
    public function mailtempGet($key = NULL, $category = 'system') {
        if ($key == NULL)
            return false;
        return Yii::app()->settings->get($category, $key);
    }

    //======================
    public static function settingGet($key = NULL, $category = 'system') {
        if ($key == NULL)
            return false;
        return Yii::app()->settings->get($category, $key);
    }

    public static function settingSet($key = NULL, $value = NULL, $category = 'system') {
        if ($key == NULL)
            return false;
        return Yii::app()->settings->set($category, $key, $value);
    }

    public static function settingDelete($cat = 'system') {
        return Yii::app()->settings->deleteCache($cat);
    }

    public static function shinra_fb_count($url_or_id) {
        /* url or id (for optional validation) */
        if (is_int($url_or_id))
            $url = 'http://graph.facebook.com/' . $url_or_id;
        else
            $url = 'http://graph.facebook.com/' . $url_or_id;

        /* get json */
        $json = json_decode(file_get_contents($url), false);

        /* has likes or shares? */
        if (isset($json->likes))
            return (int) $json->likes;
        elseif (isset($json->shares))
            return (int) $json->shares;

        return 0; // otherwise zed
    }

    public static function shinra_twitter_count($url) {
        /* build the pull URL */
        $url = 'http://cdn.api.twitter.com/1/urls/count.json?url=' . urlencode($url);

        /* get json */
        $json = json_decode(file_get_contents($url), false);
        if (isset($json->count))
            return (int) $json->count;

        return 0; // else zed
    }

    public static function shinra_gplus_count($url) {
        /* get source for custom +1 button */
        $contents = file_get_contents('https://plusone.google.com/_/+1/fastbutton?url=' . $url);

        /* pull out count variable with regex */
        preg_match('/window\.__SSR = {c: ([\d]+)/', $contents, $matches);

        /* if matched, return count, else zed */
        if (isset($matches[0]))
            return (int) str_replace('window.__SSR = {c: ', '', $matches[0]);
        return 0;
    }

    //======================	
    public static function getCounter($key = NULL, $category = 'system') {
        if ($key == NULL)
            return false;
        return Yii::app()->counter->get($category, $key);
    }

    public static function setCounter($key = NULL, $value = NULL, $category = 'system') {
        if ($key == NULL)
            return false;
        return Yii::app()->counter->set($category, $key, $value);
    }

    //=====================================
    public function alert($msg, $callback) {
        Yii::app()->clientScript->registerScript('alert', "
			jCore.alert('" . $msg . "', " . $callback . ");
		");
        return;
    }

    //=====================================
    public static function getImageUpload($object = NULL, $subdir = '') {
        if (empty($object) || !is_object($object))
            return false;

        set_time_limit(0);

        /* $object = (object)array(
          'folder' => trim($_GET['folder']),
          'id' => (int)$_GET['id'],
          'width' => (int)$_GET['width'],
          'height' => (int)$_GET['height'],
          'filename' => trim($_GET['filename'])
          ); */

        if (empty($object->filename))
            return false;

        $upload_dir = self::getUploadDir($object->folder);
        if ($object->id > 0 || $object->id == "default")
            $upload_dir.= $object->id . '/';
        $upload_dir.=$subdir;
        $file = $upload_dir . $object->filename;
        if (!file_exists($file)) {
            return false;
        }
        list($root_img_w, $root_img_h) = getimagesize($file);

        if ($object->width > 0 and $object->height > 0 && !isset($object->resize)) {

            if ($object->width < $root_img_w || $object->height < $root_img_h) {
                $resize_name = "w{$object->width}_h{$object->height}_" . $object->filename;
                $file_ = $upload_dir . $resize_name;
                if (!file_exists($file_)) {
                    $thumb = Yii::app()->phpThumb->create($file);
                    $thumb->resize_crop($object->width, $object->height)
                            ->save($file_);
                }
                $file = $file_;
            }
        } else if ($object->width > 0 and $object->height == 0 && !isset($object->resize)) {
            if ($object->width < $root_img_w) {
                $resize_name = "w{$object->width}_hauto_" . $object->filename;
                $file_ = $upload_dir . $resize_name;
                if (!file_exists($file_)) {
                    Yii::app()->phpThumb
                            ->create($file)->resize($object->width)
                            ->save($file_);
                }
                $file = $file_;
            }
        } else if ($object->width > 0 and $object->height > 0 && $object->resize == 'true') {
            $new_image_w = $root_img_w * $object->height / $root_img_h;
            $new_image_h = $root_img_h * $object->width / $root_img_w;
            if ($new_image_h >= $object->height) {
                $resize_name = "resize_wauto_h{$object->height}_" . $object->filename;
                $file_ = $upload_dir . $resize_name;
                if (!file_exists($file_)) {
                    Yii::app()->phpThumb
                            ->create($file)->resize(0, $object->height)
                            ->save($file_);
                }
                $file = $file_;
            } else {
                $resize_name = "resize_w{$object->width}_hauto_" . $object->filename;
                $file_ = $upload_dir . $resize_name;
                if (!file_exists($file_)) {
                    Yii::app()->phpThumb
                            ->create($file)->resize($object->width, 0)
                            ->save($file_);
                }
                $file = $file_;
            }
        }
        list($w, $h) = getimagesize($file);
        $pathinfo = pathinfo($file);

        return (object) array(
                    'src' => self::getUploadUrl($object->folder, $object->id, isset($object->not_int) ? $object->not_int : "") . $subdir . $pathinfo['basename'],
                    'w' => $w,
                    'h' => $h,
                    'full_src' => self::getUploadUrl($object->folder, $object->id) . $subdir . $object->filename,
                    'full_w' => $root_img_w,
                    'full_h' => $root_img_h,
        );
    }

    public function deleteUploadCache() {
        $dir = Yii::app()->params['uploadPath']['cache'];
        $interval = 1 * 24 * 60 * 60; //1 ngay
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if (!in_array($file, array('.', '..', '.svn', '_notes'))) {
                        if ((time() - filemtime($dir . $file)) > $interval)
                            @unlink($dir . $file);
                    }
                }
                closedir($dh);
            }
        }
    }

    public function getRealestateType() {
        $realestateType = RealestateType::model()
                ->findAllByAttributes(array('status' => 1, 'deleted' => 0, 'type' => 0), array('order' => 'sort_order ASC'));
        if ($realestateType !== NULL)
            return $realestateType;
        else
            return false;
    }

    public function getProjectType() {
        $realestateType = RealestateType::model()
                ->findAllByAttributes(array('status' => 1, 'deleted' => 0, 'type' => 1), array('order' => 'sort_order ASC'));
        if ($realestateType !== NULL)
            return $realestateType;
        else
            return false;
    }

    public function getArrMonth() {
        if (isset($this->lang) && $this->lang == "en")
            $arr_month = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
        else
            $arr_month = array(1 => "01 -", 2 => "02 -", 3 => "03 -", 4 => "04 -", 5 => "05 -", 6 => "06 -", 7 => "07 -", 8 => '08 -', 9 => '09 -', 10 => '10 -', 11 => '11 -', 12 => '12 -');
        return $arr_month;
    }

    public function getArrDay() {
        $arr_day = array();
        for ($i = 1; $i <= 31; $i++) {
            if ($i < 10)
                $j = '0' . $i;
            else
                $j = $i;
            $arr_day[$i] = $i;
        }
        return $arr_day;
    }

    public function getMaxday($month, $year) {
        if ($year >= 4 && $year % 4 == 0) {
            if ($month == 2)
                return 29;
            elseif (in_array($month, array(1, 3, 5, 7, 8, 10, 12)))
                return 31;
            else
                return 30;
        }else {
            if ($month == 2)
                return 28;
            elseif (in_array($month, array(1, 3, 5, 7, 8, 10, 12)))
                return 31;
            else
                return 30;
        }
    }

    //=====================================	
}
