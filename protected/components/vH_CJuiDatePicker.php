<?php
Yii::import('zii.widgets.jui.CJuiDatePicker');

class vH_CJuiDatePicker extends CJuiDatePicker{
    public $theme = 'vstart';
    public $cssFile = 'jquery-ui-custom.css';
    public $scriptFile = false;
    
    private $_options = array(                 
        'dateFormat' => 'yy-mm-dd',
        'changeMonth' => true,
        'changeYear' => true,
    );
    
    public function init() {
        if( !empty($this->options) ){
            foreach($this->options as $key=>$value){
                $this->_options[$key] = $value;
            }
        }
        $this->options = $this->_options;
        $this->themeUrl = Yii::app()->getBaseUrl(false).'/themes/default/assets/jquery-ui-1.9.2/themes';
        parent::init();
    }
}