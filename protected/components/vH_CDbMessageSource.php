<?php
class vH_CDbMessageSource extends CDbMessageSource
{	
	public $sourceMessageTable='{{sourcemessage}}';
	
	public $translatedMessageTable='{{message}}';
	
	public $cachingDuration=0;
}