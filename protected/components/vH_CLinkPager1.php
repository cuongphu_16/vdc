<?php
class vH_CLinkPager1 extends CLinkPager
{
	public
		$cssFile = false,
		$hiddenPageCssClass = 'page_hidden',
		$header = false,
		$maxButtonCount=1000,
		$htmlOptions = array('class'=>'ul');
	
	/*protected function createPageButton($label,$page,$class,$hidden,$selected)
	{
		if($hidden){
			$class.=' '.$this->hiddenPageCssClass;
			return '<li class="'.$class.'">'.CHtml::link($label,'javascript:;').'</li>';
		}else if($selected){
			$class.=' '.$this->selectedPageCssClass;
			return '<li class="'.$class.'">'.CHtml::link($label,'javascript:;').'</li>';
		}else{
			return '<li class="'.$class.'">'.CHtml::link($label,$this->createPageUrl($page)).'</li>';
		}
	}*/
	
	public function run()
	{
		$this->registerClientScript();
		$buttons=$this->createPageButtons();
		if(empty($buttons))
			return;
			
		//print_r($buttons);
		if(count($buttons)<=5)
			return;
		$page_first = array_slice($buttons,0,5);		
		$buttons = array_chunk(array_slice($buttons,5),10);
		$buttons_ = array();
		$buttons_[] = end($page_first);
		foreach($buttons as $btns)
			$buttons_[] = end($btns);
		//print_r($buttons_);
		//die;
		//echo $this->header;
		echo CHtml::tag('ul',$this->htmlOptions,implode("\n",$buttons_));
		//echo $this->footer;
	}
	
	protected function createPageButtons()
	{
		if(($pageCount=$this->getPageCount())<=1)
		return array();
		
		list($beginPage,$endPage)=$this->getPageRange();
		$currentPage=$this->getCurrentPage(false); // currentPage is calculated in getPageRange()
		$buttons=array();
		
		// first page
		//$buttons[]=$this->createPageButton($this->firstPageLabel,0,$this->firstPageCssClass,$currentPage<=0,false);
		
		// prev page
		/*if(($page=$currentPage-1)<0)
		$page=0;
		$buttons[]=$this->createPageButton($this->prevPageLabel,$page,$this->previousPageCssClass,$currentPage<=0,false);*/
		
		// internal pages
		for($i=$beginPage;$i<=$endPage;++$i)
		$buttons[]=$this->createPageButton($i+1,$i,$this->internalPageCssClass,false,$i==$currentPage);
		
		// next page
		/*if(($page=$currentPage+1)>=$pageCount-1)
		$page=$pageCount-1;
		$buttons[]=$this->createPageButton($this->nextPageLabel,$page,$this->nextPageCssClass,$currentPage>=$pageCount-1,false);*/
		
		// last page
		//$buttons[]=$this->createPageButton($this->lastPageLabel,$pageCount-1,$this->lastPageCssClass,$currentPage>=$pageCount-1,false);
		
		return $buttons;
	}
	
}
?>