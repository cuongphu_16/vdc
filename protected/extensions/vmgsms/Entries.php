<?php

function messageReceiver($userID, $serviceID, $commandCode, $message, $requestID) {
        $charging = new VmgCharging();
        $charging->logToFile(date('d-m-Y H:m:s') . ": " . $userID . '==' . $serviceID . '==' . $commandCode . "==" . $message . '==' . $requestID);
        return new soapval('return', 'xsd:string', $charging->getMO($userID, $serviceID, $commandCode, $message, $requestID));
}

function sendMT($wsdl = '') {
        require_once('lib/nusoap.php');

        $client = new nusoap_client("http://tainangnudoanhnhanviet.vn/charging/sendMT.xml", 'wsdl',						'', '', '', '');
        $err = $client->getError();
        $result = $client->call('sendMT', array(
                'userID' => '84969258412',
                'message' => 'Ban da binh chon thanh cong',
                'serviceID' => '8579',
                'commandCode' => 'NDN',
                'messageType' => '0',
                'requestID' => '111',
                'totalMessage' => '1',
                'messageIndex' => '1',
                'isMore' => '0',
                'contentType' => '0'
                ), '', '', '');
}
