<?php
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "nusoap.php";
include dirname(__FILE__) . DIRECTORY_SEPARATOR . "Entries.php";
class VmgCharging {
        const MESSAGE_TYPE_ARISING = 0;
        const MESSAGE_TYPE_CHARGE=1;
        const MESSAGE_TYPE_INVALID =2;
        public $ns,$server,$soapClient,$apiurl,$return_message;
        private $config = array();
        function __construct() {
                //require_once("config.php");
                $this->config = require(dirname(__FILE__) . '/config.php');
                $this->apiurl = $this->config['url'];
                $this->ns = "http://tainangnudoanhnhanviet.vn/charging/MyThanhReceiver";
//                $this->soapClient = new SoapClient(null, array(
//                        'location' => $this->config['webservice'],
//                        'uri' => "http://113.161.78.134/VNPTEPAY/"
//                    ));
                $this->return_message=array(
                        'invalid_code'=>'SBD khong dung',
                        'success_vote'=>'Cam on quy khach da binh chon cho thi sinh {member} SBD {code}. Chi tiet xin vui long truy cap website: Tainangnudoanhnhanviet.vn',
                        'endofservice'=>'Tong dai binh chon da dong, cam on quy khach.',
                );
                $this->server = new soap_server();
                $this->server->configureWSDL('MOReceiver', $this->ns);
                $this->server->wsdl->schemaTargetNamespace = $this->ns;
                $this->server->soap_defencoding = 'UTF-8';
                $this->server->register('messageReceiver', array('userID' => 'xsd:string',
                        'serviceID' => 'xsd:string',
                        'commandCode' => 'xsd:string',
                        'message' => 'xsd:string',
                        'requestID' => 'xsd:string'), array('return' => 'xsd:string'), 'urn:MOReceiver', 'urn:MOReceiver#messageReceiver', 'rpc', 'encoded', 'Get MO'
                );
        }
        public function showConfig(){
                print_r($this->config);
        }

        public function receiveData($HTTP_RAW_POST_DATA) {
                if (!isset($HTTP_RAW_POST_DATA))
                        $HTTP_RAW_POST_DATA = file_get_contents('php://input');
                $this->server->service($HTTP_RAW_POST_DATA);
        }
        function getMO($userID, $serviceID, $commandCode, $message, $requestID) {
                $wsdl = "http://tainangnudoanhnhanviet.vn/charging/sendMT.xml"; //duong dan file xml save duoc ve sau khi chay http://123.29.69.168/api/services/sendMT?wsdl. Chu y nho sua cac link local trong file do thanh 123.29.69.168
                $returnTest = ' '.$message;
                if (strtolower($commandCode)==strtolower($this->config['commandCode']))
                {
                        $this->logToFile($userID.'|##|'.$serviceID.'|##|'.$commandCode.'|##|'.$message.'|##|'.$requestID.PHP_EOL);
                        
                        $messageReturn="";
                        $memberCode="";
                        $arrMessage = explode(" ", trim($message));
                        $messageType = self::MESSAGE_TYPE_ARISING;
                        $isMore='0';
                        $returnTest.='  '. count($arrMessage).'    '.$arrMessage[0];
                        @date_default_timezone_set('Asia/Ho_Chi_Minh');
                        $time = strtotime('2015-11-08 21:09:00');
                        $send=true;
                        if ($time<time())
                        {
                                $messageReturn = $this->return_message['endofservice'];
                                $messageType = self::MESSAGE_TYPE_INVALID;
                                $isMore='0';
                                $send=false;
                        }else{
                                if (count($arrMessage)==2 && strtolower($arrMessage[0])==strtolower($commandCode))
                                {
                                        $memberCode = $arrMessage[1];
                                        $returnTest.='  '. $memberCode;
                                        if ($memberCode!="" && $memberCode<10)
                                        {
                                                $memberCode = '0'.(int)$memberCode;
                                        }
                                }
                                if ($memberCode!="" && $this->checkMember($memberCode))
                                {
                                        $returnTest.='##'.$memberCode.'-'.$userID;
                                        $updateVoted = $this->updateVote($memberCode, $userID,$requestID);
                                        $messageReturn = $this->return_message['success_vote'];
                                        if ($updateVoted!=false)
                                        {
                                                $messageReturn = str_replace(array('{member}','{code}'), array($updateVoted,$memberCode), $messageReturn);
                                        }else{
                                                $messageReturn = str_replace(array('{member}','{code}'), array('',$memberCode), $messageReturn);
                                        }
                                        $messageType= self::MESSAGE_TYPE_CHARGE;
                                        $isMore='0';
                                }else{
                                        $messageReturn = $this->return_message['invalid_code'];
                                        $messageType = self::MESSAGE_TYPE_INVALID;
                                        $isMore='0';
                                }
                        }
                        
                        $client = new nusoap_client($wsdl, 'wsdl',						'', '', '', '');
                        if (!empty($requestID)){
                                $param = array(
                                        'userID' => $userID,
                                        'message' => $messageReturn,
                                        'serviceID' => $this->config['serviceID'],
                                        'commandCode' => $this->config['commandCode'],
                                        'messageType' => $messageType,
                                        'requestID' => $requestID,
                                        'totalMessage' => '1',
                                        'messageIndex' => '1',
                                        'isMore' => $isMore,
                                        'contentType' => '0');
                                $guid = uniqid();
                                $filename = "senmt".$guid.time();
                                $tab = "\r\n";
                                $data = "";
                                $data.="userID=".$userID.$tab;
                                $data.="message=".$messageReturn.$tab;
                                $data.="serviceID=".$this->config['serviceID'].$tab;
                                $data.="messageType=".$messageType.$tab;
                                $data.="requestID=".$requestID.$tab;
                                $data.="isMore=".$isMore.$tab;

                                if ($this->writeMT($filename.'.txt',$data))
                                        file_get_contents('http://tainangnudoanhnhanviet.vn/sendmt/sendmt.php?token='.$filename);
//                                $result = $client->call('sendMT', $param,'','','');
//                                sendMT($wsdl);
                        }
                }
//                return $wsdl."-1-".$this->config['commandCode'].' test '.$messageType.'  '.$requestID.' '.$isMore.'  '.$userID.'  '.$messageReturn.'  '.$this->config['serviceID'].'  '. $this->config['commandCode'];
                return '1';
        }
        function checkMember($memberCode=''){
                $criteria = new CDbCriteria();
                $memberCode = str_replace(array("'",'"','~',"\\","/"), "", $memberCode);
                if ((int)$memberCode<10){
                        $memberCode = (int)$memberCode;
                        $memberCode = '0'.$memberCode;
                }
                $criteria->addCondition("code='$memberCode' and cate_id=".Member::THISINH);
                $member= Member::model()->find($criteria);
                if ($member!=NULL) return true;
                else return false;
        }
        function updateVote($memberCode,$clientID,$request_id=''){
                $criteria = new CDbCriteria();
                $memberCode = str_replace(array("'",'"','~',"\\","/"), "", $memberCode);
                $criteria->addCondition("(code='$memberCode' OR LCASE(code) like 'ndn $memberCode%') and cate_id=".Member::THISINH);
                $member= Member::model()->find($criteria);
                if ($member!=NULL)
                {
                        $member_id = $member->member_id;
                        if ($member->video!=NULL)
                        {
                                $gallery_id = $member->video->gallery_id;
                        }else{
                                $gallery_id=0;
                        }
                        $voted = new Voted;
                        $voted->gallery_id = $gallery_id;
                        $voted->member_id = $member_id;
                        $voted->phone = $clientID;
                        $voted->request_id = $request_id;
                        $voted->user_id =0;
                        $voted->created_at = date('Y-m-d H:i:s');
                        if ($voted->save())
                                return vH_Utils::stripUnicode($member->fullname);
                        else
                                return false;
                }else return false;
        }
        
        function writeMT($filename,$data) {
                $root_directory =Yii::app()->params['uploadPath']['sendmt'];
                $file = $root_directory.$filename;
                try {
                        $logFile = fopen($file, 'w+');
                        fwrite($logFile, $data);
                        fclose($logFile);
                        return true;
                } catch(Exception $ex) {
                        return false;
                }
        }
        
        function logToFile($textString) {
                $root_directory =Yii::app()->params['uploadPath']['mainupload'];
                $file = $root_directory."charginLog.txt";
                if (file_exists($file))
                {
                        $handle = fopen($file, "r");
                        $logo_src = "";
                        while (($line = fgets($handle)) !== false) {
                                if (trim($line)!="")
                                {
                                        $logo_src = $line;
                                        break;
                                }
                        }
                }
                try {
                        $logFile = fopen($root_directory."charginLog.txt", 'a');
                        fwrite($logFile, $textString);
                        fwrite($logFile, "\n");
                        fclose($logFile);
                } catch(Exception $ex) {
                        echo "Error on LogToFile";
                }
        }
}
