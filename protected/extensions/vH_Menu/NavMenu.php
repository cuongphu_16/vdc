<?php
class NavMenu extends CWidget
{
	private $_menu;
	
	public $stylesheet = "";
	
	public $menuID = "NavMenu";
	
	private $_delay = 6;
	
	private $_html;
	
	private $_baseUrl;
	
	public function getHtml() {
		return $this->_html;
	}
	
	public function setMenu($menu) {
		if(is_array($menu)) {
			$this->_menu = $menu;
		} else {
			throw new CException("Menu must be an array");
		}
	}
	
	public function setDelay($delay)
	{
		$delay = $delay ==1 ? 2 : $delay;
		if(is_integer($delay)) {
			if($delay > 0) {
				$this->_delay = $delay;
			}
		}
	}
	
	public function run() {
		if(!isset ($this->_menu) || $this->_menu == array()) {
			throw new CException("Menu is not set or it's empty");
		}
		$this->registerClientScripts();
		$this->createMarkup();
	}
	
	public function registerClientScripts(){
	}
	
	public function createMarkUp()
	{
		$this->_html = "<ul id='".$this->menuID."' class='ul'>\n";
		$this->_createMenu($this->_menu);
		$this->_html .= "</ul>\n";
		
		echo $this->_html;
	}
	
	private function _createMenu($menu,$sub = false, $first=true)
	{
		if(is_array($menu))
		{
			//CVarDumper::dump($data, 5, true);
			//exit();
			$i=0;
			foreach ($menu as $data)
			{
				$i++;
				isset($data['disabled']) ? $disabled = true : $disabled = false;
				isset($data['url']['params']) ? $params = $data['url']['params'] : $params = array();
				isset($data['url']['htmlOptions']) ? $htmlOptions = $data['url']['htmlOptions'] : $htmlOptions = array();
				isset($data['icon']) ? $icon = $data['icon'] : $icon ="";
				isset($data['label']) ? $label = $data['label'] : $label = "";
				$style = "";
				$liClass="";
				$subImage = "";
				// If in top menu set class topline
				if(!$sub) {
					$liClass= 'class=parent';
				}
				// If there's a menu item to display
				if($this->_isMenuItem($data))
				{
					$label = $label;
					$url = $this->_createUrl($data);
					if($first)
						$linkClass = 'c';
					else
						$linkClass = '';
						
					if (!$this->hasChild($data))
					{
						$this->_html.= '<li '.$liClass.'>';
						$htmlOptions['class'] = $linkClass;
						$this->_html .= CHtml::link($label, (!empty($url) ? $url : '#1'), $htmlOptions);
						$this->_html.= '</li>';
						if($i<count($menu) && $first)
							$this->_html.= '<li class="s"></li>';
					} else {
						$this->_html.= '<li '.$liClass.'>';
							$htmlOptions['class'] = $linkClass;
							$this->_html .= CHtml::link($label, (!empty($url) ? $url : '#'), $htmlOptions);
							$this->_html.= '<ul class="ul dsd">';
							$this->_html.= $this->_createMenu($data,true, false);
							$this->_html.= '</ul>';
						$this->_html.= '</li>';
						if($i<count($menu) && $first)
							$this->_html.= '<li class="s"></li>';
					}
				}
			}
		}
	}
	
	private function _isMenuItem($data)
	{
		isset($data['label']) ? $label = $data['label'] : $label = "";
		if(is_array($data) && $label && (!isset($data["visible"])|| !$data["visible"]==false)){		
			return true;
		}
		return false;
		
	}
	
	private function _createUrl($data)
	{
		// If url is an array or a non blank string and it's not disabled
		isset($data['url']['route']) ? $route = $data['url']['route'] : $route = "";
		isset($data["disabled"]) ? $disabled = $data["disabled"] : $disabled = "" ;
		isset($data['url']['params']) ? $params = $data['url']['params'] : $params = array();
		isset($data['url']['link']) ? $link = $data['url']['link'] : $link = "";
		if(($route!= "" || $data['url'] != array()) && !$disabled)
		{
			//if url is array create the url
			if($route) {
				// If there are parameters get them
				//if(is_array($params)) {
				//  $params = $data['url']['params'];
				//} else {
				//  $params = array();
				//}
				// Create the url
				$url = Yii::app()->getUrlManager()->createUrl($route,$params);
			} else {
				$url = $link;
			}
		} else {
			$url="";
		}
		
		return $url;
	}
	
	private function hasChild($arr)
	{
		if(!is_array($arr))
			return false;
			
		foreach ($arr as $title=>$value)
		{
			if(!$title == "url") {
				if(is_array($value))
					return true;
			}
		}
		return false;
	}
	
	private function set($param){
		isset($param) ? $param = $param : $param = "";
	}
}