<?php

require_once(dirname(__FILE__) . '/class.phpmailer.php');
require_once(dirname(__FILE__) . '/class.smtp.php');

class Mailer extends CComponent {

    public $options = array();
    public $default = array();

    public function init() {
        $this->default = Yii::app()->params['mail'];
    }

    public function load() {
        if (Controller::settingGet('sendmail_manual') == 1 && Controller::settingGet('sendmail_password') != "" && Controller::settingGet('sendmail_account') != "" && Controller::settingGet('sendmail_server') != "") {
            $mail = new PHPMailer();
//            $mail->IsSMTP();
            $mail->IsHTML($this->default['IsHTML']);
            $mail->SMTPAuth = $this->default['SMTPAuth'];
            $mail->Timeout = $this->default['Timeout'];
            $mail->CharSet = "UTF-8";
            if (Controller::settingGet('sendmail_ssl') == 1)
                $mail->SMTPSecure = 'tls';
             if (strip_tags(Controller::settingGet('sendmail_port')) != "" && strip_tags(Controller::settingGet('sendmail_port')) != FALSE)
                $mail->Port = strip_tags(Controller::settingGet('sendmail_port'));
            $mail->Host = strip_tags(Controller::settingGet('sendmail_server'));
            $mail->Username = strip_tags(Controller::settingGet('sendmail_email')) != "" ? strip_tags(Controller::settingGet('sendmail_email')) : $this->default['Username'];
            $mail->Password = strip_tags(Controller::settingGet('sendmail_password')) != "" ? strip_tags(Controller::settingGet('sendmail_password')) : $this->default['Password'];
            $mail->From = strip_tags(Controller::settingGet('sendmail_account')) != "" ? strip_tags(Controller::settingGet('sendmail_account')) :  $this->default['From'];
            $mail->FromName = strip_tags(Controller::settingGet('sendmail_name')) != "" ? strip_tags(Controller::settingGet('sendmail_name')) : $this->default['FromName'];
            $mail->Subject = '=?UTF-8?B?'.base64_encode($this->default['Subject']).'?=';
            $mail->Body = '=?UTF-8?B?'.base64_encode($this->default['Body']).'?=';
        } else {
            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->IsHTML($this->default['IsHTML']);
            $mail->SMTPAuth = $this->default['SMTPAuth'];
            $mail->Timeout = $this->default['Timeout'];

            $mail->Host = $this->default['Host'];
            $mail->Username = $this->default['Username'];
            $mail->Password = $this->default['Password'];
            $mail->From =  $this->default['From'];
            $mail->FromName = '=?UTF-8?B?'.base64_encode($this->default['FromName']).'?=';
            $mail->Subject = '=?UTF-8?B?'.base64_encode($this->default['Subject']).'?=';
            $mail->Body = $this->default['Body'];
        }
        if (!empty($this->default['AdminMail']))
            $mail->AddAddress($this->default['AdminMail'][0], $this->default['AdminMail'][1]);

        if (!empty($this->options) && is_array($this->options)) {
            foreach ($this->options as $k => $v) {
                if ($k == 'AddAddress' && is_array($v) && !empty($v)) {
                    foreach ($v as $m => $n)
                        eval('$mail->AddAddress("' . $m . '","' . $n . '");');
                } else {
                    eval('$mail->' . $k . ' = "' . $v . '";');
                }
            }
        }
        return $mail;
    }

}
