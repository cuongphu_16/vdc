<?php

class MailChimp extends CApplicationComponent {

  public $apiKey = 'd270e3fd12b14edc8376c0fa8dc7448b-us3';

  public $list;

  protected $api;

  /**
   * @return MCAPI
   */
  public function getMCAPI() {
    if (!$this->api) {
      Yii::import('ext.MailChimp.vendors.MCAPI', true);
      if (isset($_ENV['COMPANY'])) {
        $this->apiKey = $this->list[$_ENV['COMPANY']];
      }
      $this->api = new MCAPI($this->apiKey);
    }
    return $this->api;
  }
}
