
<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie ie9" lang="en-US">
<![endif]-->
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <title>VDC - Demo website</title>


        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>



        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/flexslider.css">
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/simple-line-icons.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/icon-font.css">
        <link rel="stylesheet" href="css/flat-icon.css">
        <link rel="stylesheet" href="css/innovation.css">

        <link rel="stylesheet" href="css/settings.css">
    </head>
    <body>


        <div class="sidebar-menu-container" id="sidebar-menu-container">

            <div class="sidebar-menu-push">

                <div class="sidebar-menu-overlay"></div>

                <div class="sidebar-menu-inner">

                    <div id="sub-header">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="left-info">
                                        <ul>
                                            <li><i class="fa fa-phone"></i>04 36330559</li>
                                            <li><i class="fa fa-envelope-o"></i>info@fairway.vn</li>
                                            <li><i class="fa fa-clock-o"></i>Thứ 2 - Thứ 7 : 08:00 - 18:00</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="social-icons">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-behance"></i></a></li>
                                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <header class="site-header">
                        <div id="main-header" class="main-header header-sticky">
                            <div class="inner-header container clearfix">

                                <div class="logo">
                                    <a href="index.php"><img src="images/logo.png" alt=""></a>
                                </div>
                                <div class="header-right-toggle pull-right hidden-md hidden-lg">
                                    <a href="javascript:void(0)" class="side-menu-button"><i class="fa fa-bars"></i></a>
                                </div>
                                <nav class="main-navigation text-right hidden-xs hidden-sm">
                                    <ul>
                                        <li><a href="index.php">Trang chủ</a>
                                        </li>
                                        <li><a href="news-list.php" class="has-submenu">Giới thiệu</a>
                                            <ul class="sub-menu">
                                                <li><a href="news-list.php">Giới thiệu chung</a></li>
                                                <li><a href="news-list.php">Tầm nhìn, sứ mệnh</a></li>
                                                <li><a href="news-list.php">Sơ đồ tổ chức</a></li>
                                                <li><a href="news-list.php">Thư viện ảnh</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#" class="has-submenu">Dịch vụ</a>
                                            <ul class="sub-menu">
                                                <li><a href="news-list.php">Valuation & Advisory</a></li>
                                                <li><a href="news-list.php">Development Services</a></li>
                                                <li><a href="news-list.php">Project Sales & Marketing</a></li>
                                                <li><a href="news-list.php">Investment & Investment Services</a></li>

                                            </ul>
                                        </li>

                                        <li><a href="project-list.php" class="has-submenu">Dự án</a>
                                            <ul class="sub-menu">
                                                <li><a href="project-list.php">Tham gia đầu tư</a></li>
                                                <li><a href="project-list.php">Tư vấn phát triển dự án</a></li>

                                            </ul>
                                        </li>
                                        <li><a href="#">Sàn giao dịch</a></li>
                                        <li><a href="news-list.php">Tin tức – Sự kiện</a>
                                        </li>
                                        <li><a href="news-list.php">Tuyển dụng</a></li>
                                        <li><a href="news-list.php">Liên hệ</a></li>
                                        <span style="padding-right:2px"><a href=""><img src="images/vn.jpg" alt="" height="18px"></a></span>
                                        <span style="padding-right:2px"><a href=""><img src="images/en.jpg" alt="" height="18px"></a></span>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </header>
                    <div id="page-heading">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <h1>tin tức - sự kiện</h1>
                                </div>

                                <div class=" col-md-8 sitepath-r">

                                    <a class="first" href="#">Trang chủ</a><i class="fa fa-angle-double-right"></i><a href="#" class="sub">Tin tức - Sự kiện</a>
                                    <i class="fa fa-angle-double-right"></i>
                                    <a href="">Tin tức hoạt động</a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <section class="blog-classic" style="padding:10px;">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="side-bar">

                                        <div class="sidebar-widget">
                                            <h4>Danh mục</h4>
                                            <div class="line-dec"></div>
                                            <ul>
                                                <li>::   Giới thiệu</li>
                                                <li>::   Tin tức - Sự kiện</li>
                                                <li>::   Tuyển dụng</li>

                                            </ul>
                                        </div>


                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="blog-posts">

                                        <div class="blog-post">
                                            <img src="images/01-news.jpg" alt="">
                                            <div class="down-content">

                                                <div class="right-cotent">
                                                    <a href="#"><h4>Đồ án thể hiện tầm nhìn khoa học về đặc trưng độc đáo của Khu di tích</h4></a>

                                                </div>
                                                <p>Đó là khẳng định của Thứ trưởng Bộ Xây dựng Phạm Hồng Hà tại lễ công bố và bàn giao hồ sơ quy hoạch tổng thể bảo tồn, tôn tạo và phát huy giá trị Khu di tích thành Cổ Loa (tỷ lệ 1/2.000) do UBND TP Hà Nội phối hợp với Bộ Xây dựng tổ chức ngày 26/10.</p>

                                            </div>
                                        </div>
                                        <div class="blog-post">
                                            <img src="images/02-news.jpg" alt="">
                                            <div class="down-content">

                                                <div class="right-cotent">
                                                    <a href="#"><h4>Đồ án thể hiện tầm nhìn khoa học về đặc trưng độc đáo của Khu di tích</h4></a>

                                                </div>
                                                <p>Đó là khẳng định của Thứ trưởng Bộ Xây dựng Phạm Hồng Hà tại lễ công bố và bàn giao hồ sơ quy hoạch tổng thể bảo tồn, tôn tạo và phát huy giá trị Khu di tích thành Cổ Loa (tỷ lệ 1/2.000) do UBND TP Hà Nội phối hợp với Bộ Xây dựng tổ chức ngày 26/10.</p>

                                            </div>
                                        </div>
                                        <div class="blog-post">
                                            <img src="images/03-news.jpg" alt="">
                                            <div class="down-content">

                                                <div class="right-cotent">
                                                    <a href="#"><h4>Đồ án thể hiện tầm nhìn khoa học về đặc trưng độc đáo của Khu di tích</h4></a>

                                                </div>
                                                <p>Đó là khẳng định của Thứ trưởng Bộ Xây dựng Phạm Hồng Hà tại lễ công bố và bàn giao hồ sơ quy hoạch tổng thể bảo tồn, tôn tạo và phát huy giá trị Khu di tích thành Cổ Loa (tỷ lệ 1/2.000) do UBND TP Hà Nội phối hợp với Bộ Xây dựng tổ chức ngày 26/10.</p>

                                            </div>
                                        </div>
                                        <div class="blog-post">
                                            <img src="images/04-news.jpg" alt="">
                                            <div class="down-content">

                                                <div class="right-cotent">
                                                    <a href="#"><h4>Đồ án thể hiện tầm nhìn khoa học về đặc trưng độc đáo của Khu di tích</h4></a>

                                                </div>
                                                <p>Đó là khẳng định của Thứ trưởng Bộ Xây dựng Phạm Hồng Hà tại lễ công bố và bàn giao hồ sơ quy hoạch tổng thể bảo tồn, tôn tạo và phát huy giá trị Khu di tích thành Cổ Loa (tỷ lệ 1/2.000) do UBND TP Hà Nội phối hợp với Bộ Xây dựng tổ chức ngày 26/10.</p>

                                            </div>
                                        </div>
                                        <div class="blog-post">
                                            <img src="images/05-news.jpg" alt="">
                                            <div class="down-content">

                                                <div class="right-cotent">
                                                    <a href="#"><h4>Đồ án thể hiện tầm nhìn khoa học về đặc trưng độc đáo của Khu di tích</h4></a>

                                                </div>
                                                <p>Đó là khẳng định của Thứ trưởng Bộ Xây dựng Phạm Hồng Hà tại lễ công bố và bàn giao hồ sơ quy hoạch tổng thể bảo tồn, tôn tạo và phát huy giá trị Khu di tích thành Cổ Loa (tỷ lệ 1/2.000) do UBND TP Hà Nội phối hợp với Bộ Xây dựng tổ chức ngày 26/10.</p>

                                            </div>
                                        </div>
                                        <div class="blog-post">
                                            <img src="images/06-news.jpg" alt="">
                                            <div class="down-content">

                                                <div class="right-cotent">
                                                    <a href="#"><h4>Đồ án thể hiện tầm nhìn khoa học về đặc trưng độc đáo của Khu di tích</h4></a>

                                                </div>
                                                <p>Đó là khẳng định của Thứ trưởng Bộ Xây dựng Phạm Hồng Hà tại lễ công bố và bàn giao hồ sơ quy hoạch tổng thể bảo tồn, tôn tạo và phát huy giá trị Khu di tích thành Cổ Loa (tỷ lệ 1/2.000) do UBND TP Hà Nội phối hợp với Bộ Xây dựng tổ chức ngày 26/10.</p>

                                            </div>
                                        </div>

                                        <!-- cuongnx-->

                                        <div class="col-md-12">
                                            <div class="pagination">
                                                <ul>
                                                    <li class="active"><a href="#">1</a></li>
                                                    <li><a href="#">2</a></li>
                                                    <li><a href="#">3</a></li>
                                                    <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                </div>
                </section>				
                <footer>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="about-us">
                                    <img src="images/logo.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="company-pages">
                                    <h2>FAIRWAY PROPERTY JSC</h2>
                                    <ul class="first-list">
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Đ/c: 9 floor, HD building, 57 Tran Quoc Toan, Hoan Kiem, Ha Noi</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Tel: 04 8888 9999 - Fax: 04 8888 9998</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Email: info@fairway.vn</a></li>

                                    </ul>

                                </div>
                            </div>


                        </div>
                    </div>
                </footer>



                <a href="#" class="go-top"><i class="fa fa-angle-up"></i></a>

            </div>			</div>
        <nav class="sidebar-menu slide-from-left">
            <div class="nano">
                <div class="content">
                    <nav class="responsive-menu">
                        <ul>
                            <li class="menu-item-has-children"><a href="#">Trang chủ</a>

                            </li>
                            <li class="menu-item-has-children"><a href="#">Giới thiệu</a>
                                <ul class="sub-menu">
                                    <li><a href="about.html">Giới thiệu chung</a></li>
                                    <li><a href="contact.html">Tầm nhìn, sứ mệnh</a></li>
                                    <li><a href="404.html">Sơ đồ tổ chức</a></li>
                                    <li><a href="coming.html">Thư viện ảnh</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children"><a href="#">Lĩnh vực kinh doanh</a>
                                <ul class="sub-menu">
                                    <li><a href="services-listing.html">Đầu tư xây dựng</a></li>
                                    <li><a href="construction.html">Đầu tư Bất động sản</a></li>
                                    <li><a href="isolation.html">Tư vấn đầu tư dự án</a></li>
                                    <li><a href="renovation.html">Kinh doanh vật tư, thiết bị</a></li>
                                    <li><a href="electric-welding.html">Cho thuê kho bãi, nhà xưởng</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children"><a href="#">Dự án</a>
                                <ul class="sub-menu">
                                    <li><a href="projects.html">Đầu tư xây dựng</a></li>
                                    <li><a href="projects-3-grids.html">Đầu tư và kinh doanh BDS</a></li>
                                    <li><a href="projects-3-full.html">Tư vấn đầu tư dự án</a></li>		
                                </ul>
                            </li>
                            <li class="menu-item-has-children"><a href="#">Tin tức - Sự kiện</a>
                                <ul class="sub-menu">
                                    <li><a href="blog-right.html">Tin nội bộ</a></li>
                                    <li><a href="blog-grid.html">Tin tức chung</a></li>
                                </ul>
                            </li>
                            <li><a href="contact.html">Tuyển dụng</a></li>
                            <li><a href="contact.html">Liên hệ</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </nav>	</div>
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script src="js/jquery.themepunch.tools.min.js"></script>
    <script src="js/jquery.themepunch.revolution.min.js"></script>

    <script type="text/javascript" src="js/plugins.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
</body>
</html>