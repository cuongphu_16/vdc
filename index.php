<?php

session_set_cookie_params(7 * 24 * 3600, '/');
session_start();
ini_set('gd.jpeg_ignore_warning', 1);
error_reporting(E_ALL ^ E_NOTICE);
$yii = dirname(__FILE__) . '/../framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require_once($yii);
Yii::createWebApplication($config)->run();

//echo Yii::app()->settings->get('mailchimp_api');