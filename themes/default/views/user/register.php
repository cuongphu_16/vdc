<div class="main-container candidate-page">
    <div class="container page-register-candidate">

        <ul class="ul mcr-lg">
            <li class="fl mcl">
                <div class="box box-radius register-box _padd20">
                    <div class="box-container">
                        <?php
                        if (Yii::app()->user->isGuest):
                            $this->widget('ext.eauth.EAuthWidget', array('action' => 'user/login/lang/'.$this->langCode, 'view' => 'candidate', 'assetsUrl' => $this->assetsUrl));
                        endif;
                        ?>
                        <div class="page-title"><?= Yii::t('main', 'Student Register') ?></div>
                        <div class="clr"></div>
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'signup-form-dialog',
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'action' => $this->createUrl($this->langCode . '/user/signupcandidate'),
                            'clientOptions' => array(
                                'afterValidate' => 'js:function(form, data, hasError){
											}',
                                'afterValidateAttribute' => 'js:function(form, data, hasError){
											}',
                            ),
                            'htmlOptions' => array(
                                'class' => 'form_2col',
                            ),
                        ));
                        ?>
                        <div class="form-head"><?= Yii::t('main', 'Contact Info') ?></div>
                        <div class="row">
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'firstname'); ?>
                                <?php echo $form->textField($model, 'firstname', array('class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'firstname'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'lastname'); ?>
                                <?php echo $form->textField($model, 'lastname', array('class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'lastname'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <?= $form->labelEx($model, 'gender'); ?>
                                <?php
                                echo $form->dropDownList($model, 'gender', array(0 => Yii::t('main', 'Male'), 1 => Yii::t('main', 'Female')), array('class' => 'form-control vSelect'));
                                ?>
                            </div>
                            <div class="form-group">
                                <?= $form->labelEx($model, 'mobile'); ?>
                                <?php echo $form->textField($model, 'mobile', array('class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'mobile'); ?>
                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group">
                                <?= $form->labelEx($model, 'birthday'); ?>
                                <?php echo $form->textField($model, 'birthday', array('class' => 'form-control', 'readonly' => 'true')); ?>
                                <?php echo $form->error($model, 'birthday'); ?>
                            </div>
                            <div class="form-group">
                                <?= $form->labelEx($model, 'email'); ?>
                                <?php echo $form->textField($model, 'email', array('class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'email'); ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'password'); ?>
                                <div class="position_relative">
                                    <?php echo $form->passwordField($model, 'password', array('class' => 'form-control check_password', 'value' => '')); ?>
                                    <a class="eye_pass input_password"></a>
                                    <div class="meter"><span id="result"></span></div>
                                    <span id="result_"></span>
                                    <?php echo $form->error($model, 'password'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'password_cf'); ?>
                                <div class="position_relative">
                                    <?php echo $form->passwordField($model, 'password_cf', array('class' => 'form-control', 'value' => '')); ?>
                                    <a class="eye_pass input_password_cf"></a>
                                    <?php echo $form->error($model, 'password_cf'); ?>
                                </div>
                            </div>
                        </div>


                        <div class="form-head"><?= Yii::t('main', 'Terms and regulations') ?></div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox"  id="user_agree" checked="checked">
                                <?= Yii::t('main', 'I agree with the') ?> <a href="<?= Yii::app()->createUrl('candidate/term/lang/' . $this->langCode) ?>" target="blank"><?= Yii::t('main', 'Terms and regulations') ?></a>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"  id="user_nt" checked="checked">
                                <?= Yii::t('main', 'I agree with the') ?> <a href="<?= Yii::app()->createUrl('candidate/policy/lang/' . $this->langCode) ?>" target="blank"><?= Yii::t('main', 'Privacy Policy') ?></a>
                            </label>
                        </div>
                        <div class="help-block"><?= Yii::t('main', 'When you click Create Account below means that you agree to the above items') ?></div>

                        <br>
                        <div class="checkbox">
                            <label>
                                <?php echo $form->checkBox($model, 'receive_notification', array()); ?>
                                <?= Yii::t('main', 'I want to receive information automatically from j4s.vn') ?>
                            </label>
                        </div>
                        <div style="margin-top: 20px">
                            <input type="hidden" name="ajax" value="signup-form-dialog" />
                            <?php
                            echo CHtml::ajaxSubmitButton(Yii::t('main', 'Create account'), $this->createUrl($this->langCode . '/user/signupcandidate'), array(
                                'beforeSend' => 'function(){
											$("#signup-form-dialog .errorMessage").hide();	
											$("#register_submit").attr("disabled",true);
										}',
                                'dataType' => 'json',
                                'success' => 'function(data){
											$("#signup-form-dialog .errorMessage").hide();	
											
											if(data.hasOwnProperty("show_dialog_login")){
												showNotify(data.msg,function(){
													window.location.href="' . $this->createUrl("candidate/login/lang/" . $this->langCode) . '";
												});
											}else{
												$("#register_submit").attr("disabled",false);
												$.each(data,function(k,v){
													$("#"+k+"_em_").html(v[0]).show();
                                                                                                        $("#"+k+"_em_").parent().addClass("error");
                                                                                                        
												});
												$("#captcha_img1").trigger("click");
																						
											}
											
										}'
                                    ), array('class' => 'btn btn-warning', 'id' => 'register_submit'));
                            ?>
                        </div>

                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </li>
            <li class="fr mcr">
                <div class="box box-radius if-login" style="background: #d1d3d4">
                    <div class="box-container">
                        <div class="row">
                            <div class="t"><?= Yii::t('main', 'You are already a member') ?>?</div>
                            <div class="n"><?= Yii::t('main', 'Please') ?> <a href="<?= Yii::app()->createUrl('candidate/login/lang/' . $this->langCode) ?>" class="underline"><?= Yii::t('main', 'login') ?></a> <?= Yii::t('main', 'now') ?>!</div>
                        </div>
                        <div class="row">
                            <div class="t"><?= Yii::t('main', 'Forgot your password') ?>?</div>
                            <div class="n"><a href="<?= Yii::app()->createUrl('user/forgotPass/lang/' . $this->langCode) ?>" class="underline fancybox fancybox.ajax"><?= Yii::t('main', 'Recover password') ?></a> <?= Yii::t('main', 'now') ?>!</div>
                        </div>
                    </div>
                </div>
              
            </li>
        </ul>
    </div>
</div>
<script>
    var checker_qd = document.getElementById('user_agree');
    var checker_nt = document.getElementById('user_nt');
    var sendbtn = document.getElementById('register_submit');
    checker_qd.onchange = function() {
        if (this.checked) {
            if (checker_nt.checked) {
                sendbtn.disabled = false;
            } else {
                sendbtn.disabled = true;
            }
        } else {
            sendbtn.disabled = true;
        }

    };
    checker_nt.onchange = function() {
        if (this.checked) {
            if (checker_qd.checked) {
                sendbtn.disabled = false;
            } else {
                sendbtn.disabled = true;
            }
        } else {
            sendbtn.disabled = true;
        }

    };
    $(document).ready(function() {
        $('.fancybox').fancybox({
            width: 400,
            height: 70,
            scrolling: 'no',
            autoScale: false,
            fitToView: false,
            autoHeight: true,
            aspectRatio: true,
            autoSize: false,
            title: false,
            helpers: {
                overlay: {closeClick: false}
            },
            closeClick: false,
            autoDimension: false
        });
    });
    $("#User_password").on("keyup", function() {
        if ($(this).val() == '') {
            $('.input_password').hide();
        }
    });
    $("#User_password").keypress(function() {
        $('.input_password').show();

    });
    $("#User_password_cf").on("keyup", function() {
        if ($(this).val() == '') {
            $('.input_password_cf').hide();
        }
    });
    $("#User_password_cf").keypress(function() {
        $('.input_password_cf').show();

    });


    $('.input_password').mousedown(function() {
        $("#User_password").attr('type', 'text');
    }).bind('mouseup mouseleave', function() {
        $("#User_password").attr('type', 'password');
    });
    $('.input_password_cf').mousedown(function() {
        $("#User_password_cf").attr('type', 'text');
    }).bind('mouseup mouseleave', function() {
        $("#User_password_cf").attr('type', 'password');
    });
    $("#User_birthday").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1970:2015'
    });
</script>
<style>
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        background-color: #fff;
        cursor: pointer;
        opacity: 1;
    }    
</style>
<script>
    /*
     jQuery document ready.
     */
    $(document).ready(function()
    {

        $('.check_password').keyup(function()
        {
            $('#result_').html(checkStrength($('.check_password').val()))
        });

        /*
         checkStrength is function which will do the 
         main password strength checking for us
         */

        function checkStrength(password)
        {
            //initial strength
            var strength = 0

            //if the password length is less than 6, return message.
            if (password.length == 0) {
                $('.meter').hide();
                return '';
            }
            if (password.length < 6) {
                $('.meter').show();
                $("#result").removeAttr('class');
                $("#result").attr('class', '');
                $('#result').addClass('short');
                return '<strong><?= Yii::t('main', 'Password strength') ?>:</strong> <?= Yii::t('main', 'Too short') ?>';
            }

            //length is ok, lets continue.

            //if length is 8 characters or more, increase strength value
            if (password.length > 7)
                strength += 1;

            //if password contains both lower and uppercase characters, increase strength value
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
                strength += 1;

            //if it has numbers and characters, increase strength value
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
                strength += 1;

            //if it has one special character, increase strength value
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
                strength += 1;

            //if it has two special characters, increase strength value
            if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
                strength += 1;

            //now we have calculated strength value, we can return messages

            //if value is less than 2
            if (strength < 2)
            {
               $("#result").removeAttr('class');
                $("#result").attr('class', '');
                $('#result').addClass('weak')
                return '<strong><?= Yii::t('main', 'Password strength') ?>:</strong> <?= Yii::t('main', 'Weak') ?>';
            }
            else if (strength == 2)
            {
                $("#result").removeAttr('class');
                $("#result").attr('class', '');
                $('#result').addClass('good')
                return '<strong><?= Yii::t('main', 'Password strength') ?>:</strong> <?= Yii::t('main', 'Good') ?>';
            }
            else
            {
                $("#result").removeAttr('class');
                $("#result").attr('class', '');
                $('#result').addClass('strong');
                return '<strong><?= Yii::t('main', 'Password strength') ?>:</strong> <?= Yii::t('main', 'Strong') ?>';
            }
        }
    });
</script>