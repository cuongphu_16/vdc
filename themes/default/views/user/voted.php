<article>
    <div class="container">
        <div class="col_left" style="margin-top: 40px">
            <h1 class="titleDetail"><?=Yii::t('main', 'Manage Voted')?></h1>
            <?php
            if(!empty($voted)):
            ?>
            <table cellpadding="0" border="0" class="table_voted">
                <tr>
                    <th style="text-align: center">STT</th>
                    <th>Thí sinh</th>
                    <th>Bài hát/Sáng tác</th>
                    <th nowrap>Ngày bình chọn</th>
                </tr>
                <?php
                $i=0;
                    foreach($voted as $v):$i++;
                ?>
                <tr>
                    <td align="center"><?=$i?></td>
                    <td nowrap><a href="<?=$v->member->url?>"><strong><?=$v->member->fullname?></strong></a></td>
                    <td>
                        Bài hát: <?=$v->video->title?><br>
                        Sáng tác: <?=$v->video->composer?>
                    </td>
                    <td nowrap><?=date('H:i d/m/Y', strtotime($v->created_at))?></td>
                </tr>
                <?php endforeach;?>
            </table>
            <?php else:?>
            Bạn không có bình chọn nào!
            <?php endif;?>
        </div>
        <div class="col_right" style="margin-top: 40px;">
            <!-- news -->
            <?php $this->widget('WD_ListVideo') ?>
            <!-- end news -->
        </div>
        <div class="clearfix" style="margin-bottom: 30px;"></div>
    </div>
</article>