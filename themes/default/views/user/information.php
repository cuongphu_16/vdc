<div class="wrapper-main">
    <div class="user">
	<div class="user-header">
	    <div class="avatar"><img src="<?= $model->getImage(100, 100)->src ?>" /></div>
	    <div class="user-name">
		<h2><?= !empty($model->firstname) || !empty($model->lastname) ? $model->firstname . $model->lastname : $model->username ?></h2>
		<!--<h5 class="color-red">Member</h5>-->
		<span class="date">Tham gia từ: <?= date("H:i A d-m-Y", strtotime($model->created_at)) ?></span>
	    </div>
	    <a href="<?= Yii::app()->createUrl('user/logout') ?>" class="btn btn-lg btn-default"><i class="fa fa-external-link"></i> Thoát</a>
	</div>
	<div class="user-content">
	    <!-- Nav tabs -->
	    <ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#comm" aria-controls="comm" role="tab" data-toggle="tab"><i class="fa fa-comment"></i> Bình luận</a></li>
		<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-user"></i> Chỉnh sửa thông tin</a></li>
		<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-check-square-o"></i> Theo dõi Manga</a></li>
		<li role="presentation"><a href="#changepass" aria-controls="changepass" role="tab" data-toggle="tab"><i class="fa fa-unlock-alt"></i> Đổi mật khẩu</a></li>
		<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-folder-open"></i> Quản lý Manga</a></li>
	    </ul>
	    <!-- Tab panes -->
	    <div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="comm">
		    <div class="fb-comments" data-href="http://developers.facebook.com/docs/plugins/comments/" data-width="1102" data-numposts="10"></div>
		</div>
		<div role="tabpanel" class="tab-pane" id="profile">
		    <?php
		    $form_edit = $this->beginWidget('CActiveForm', array(
			'id' => 'member_form',
			'enableAjaxValidation' => true,
			'enableClientValidation' => true,
			'clientOptions' => array(
			    'validateOnSubmit' => true,
			),
			'htmlOptions' => array(
			    'enctype' => 'multipart/form-data',
			    'class' => 'form-horizontal',
			),
		    ));
		    ?>
		    <div class="form-group">
			<label for="name" class="col-sm-2 control-label">Tên tài khoản</label>
			<div class="col-sm-5">
			    <?php echo $form_edit->textField($model, 'username', array('value' => $model->username, 'class' => 'form-control', 'disabled' => 'disabled')); ?>
			</div>
		    </div>
		    <div class="form-group">
			<label for="email" class="col-sm-2 control-label">Email</label>
			<div class="col-sm-5">			    
			    <?php echo $form_edit->textField($model, 'firstname', array('value' => $model->email, 'class' => 'form-control', 'disabled' => 'disabled')); ?>
			</div>
		    </div>
		    <div class="form-group">
			<label class="col-sm-2 control-label">Tên</label>
			<div class="col-sm-5">
			    <?php echo $form_edit->textField($model, 'firstname', array('value' => $model->firstname, 'class' => 'form-control')); ?>
			    <?php echo $form_edit->error($model, 'firstname'); ?>
			    <a href="javascript:;" class="edit"><i class="fa fa-edit"></i></a>
			</div>
		    </div>
		    <div class="form-group">
			<label for="gt" class="col-sm-2 control-label">Họ</label>
			<div class="col-sm-5">
			    <?php echo $form_edit->textField($model, 'lastname', array('value' => $model->lastname, 'class' => 'form-control')); ?>
			    <?php echo $form_edit->error($model, 'lastname'); ?>
			    <a href="javascript:;" class="edit"><i class="fa fa-edit"></i></a>
			</div>
		    </div>
		    <div class="form-group">
			<label for="sdt" class="col-sm-2 control-label">Số điện thoại</label>
			<div class="col-sm-5">
			    <?php echo $form_edit->textField($model, 'mobile', array('value' => $model->mobile, 'class' => 'form-control')); ?>
			    <?php echo $form_edit->error($model, 'mobile'); ?>
			    <a href="javascript:;" class="edit"><i class="fa fa-edit"></i></a>
			</div>
		    </div>
		    <div class="form-group">
			<label for="fb" class="col-sm-2 control-label">Link Facebook</label>
			<div class="col-sm-5">
			    <?php echo $form_edit->textField($model, 'link_facebook', array('value' => $model->link_facebook, 'class' => 'form-control')); ?>
			    <?php echo $form_edit->error($model, 'link_facebook'); ?>
			    <a href="javascript:;" class="edit"><i class="fa fa-edit"></i></a>
			</div>
		    </div>
		    <div class="form-group">
			<label for="fb" class="col-sm-2 control-label">Hình đại diện</label>
			<div class="col-sm-5">
			    <div class="avatar"><img src="<?= $model->getImage(100, 100)->src ?>" /></div>
			    <div class="avatar-img">
				<?php echo $form_edit->fileField($model, 'avatar'); ?>
				<?php echo $form_edit->error($model, 'avatar'); ?>
				<?php
				if (!empty($model->avatar)) :
				    echo $form_edit->hiddenField($model, 'avatar_hidden', array('value' => $model->avatar));
				endif;
				?>
				<script type="text/javascript">
				    $("#ytUser_avatar").val($("#User_avatar_hidden").val());
				</script> 
				<div class="avatar-txt">Kích thước ảnh không vượt quá 2M hoặc 2000 px</div>
			    </div>
			</div>
		    </div>
		    <div class="form-group form-btn">
			<label for="fb" class="col-sm-2 control-label"></label>
			<div class="col-sm-5">
			    <button type='submit' class="btn btn-lg btn-primary">Lưu thay đổi</button>
			</div>
		    </div>
		    <?php $this->endWidget(); ?>
		</div>
		<div role="tabpanel" class="tab-pane" id="messages">


		</div>
		<script language="javascript">
		    function ajaxlog(page)
		    {
			$.post('<?= $this->createUrl("user/ajaxLog") ?>', {
			    page: page
			}, function(data) {

			    $(".pagging_page").removeClass('active');
			    $("#page_" + page).addClass('active');
			    $('#c_page').val(page);
			    $('#messages').html(data);
			});
		    }
		    function nextPage()
		    {
			c_page = parseInt($('#c_page').val());
			if (c_page <<?= (isset($total_pages)) ? $total_pages : 1 ?>)
			{
			    c_page++;
			    ajaxlog(c_page);
			}
		    }

		    function previousPage()
		    {
			c_page = parseInt($('#c_page').val());
			if (c_page > 1)
			{
			    c_page--;
			    ajaxlog(c_page);
			}
		    }
		    ajaxlog(1);
                </script> 
                <div class="clr"></div>
                <input type="hidden" name="cur_page" id="c_page" value="<?= (isset($page)) ? $page : 1 ?>" />


		<div role="tabpanel" class="tab-pane changepass" id="changepass">
		    <?php
		    $form = $this->beginWidget('CActiveForm', array(
			'id' => 'edit_pass',
			'enableAjaxValidation' => true,
			'enableClientValidation' => true,
			'clientOptions' => array(
			    'validateOnSubmit' => true,
			),
			'htmlOptions' => array(
			    'class' => 'form-horizontal',
			),
		    ));
		    ?>
		    <div class="form-group">
			<label class="col-sm-2 control-label">Nhập mật khẩu cũ</label>
			<div class="col-sm-5">
			    <?php echo $form->passwordField($model2, 'password_old', array('class' => 'form-control', 'value' => '')); ?>
			    <?php echo $form->error($model2, 'password_old'); ?>
			</div>
		    </div>
		    <div class="form-group">
			<label class="col-sm-2 control-label">Nhập mật khẩu mới</label>
			<div class="col-sm-5">
			    <?php echo $form->passwordField($model2, 'password_new', array('class' => 'form-control check_password', 'value' => '')); ?>
			    <?php echo $form->error($model2, 'password_new'); ?>

			</div>
		    </div>
		    <div class="form-group">
			<label class="col-sm-2 control-label">Nhập lại mật khẩu mới</label>
			<div class="col-sm-5">
			    <?php echo $form->passwordField($model2, 'password_cf', array('class' => 'form-control', 'value' => '')); ?>
			    <?php echo $form->error($model2, 'password_cf'); ?>
			</div>
		    </div>
		    <div class="form-group form-btn">
			<label for="fb" class="col-sm-2 control-label"></label>
			<div class="col-sm-5">
			    <button type="submit" class="btn btn-lg btn-primary">Lưu thay đổi</button>
			</div>
		    </div>
		    <?php $this->endWidget(); ?>
		</div>
		<div role="tabpanel" class="tab-pane" id="settings">
		    <div class="note">
			<p><strong>Lưu ý thêm Manga mới :</strong></p>
			<p>- Chỉ thêm Manga chưa có - Dùng chức năng search để xem Manga đã có trên website chưa.</p>
			<p>- Bạn chỉ có thể cập nhật thông tin manga nào do bạn tạo.</p>
			<p>- Khi thêm manga xong thì bạn có thể upload chapter cho manga đó.</p>
			<p>- Manga phải được Editor duyệt thì mới hiện ra trang chủ.</p>
			<p>- Nếu muốn xóa manga nào, bạn click vào nút yêu cầu xóa manga nhé, Editor sẽ xóa sau khi kiểm tra.</p>
		    </div>
		    <div class="upload-btn">
			<button type="button" class="btn btn-lg btn-warning"><i class="fa fa-edit"></i> Upload - Cập nhật - Chỉnh sửa Chapter</button>
			<button type="button" class="btn btn-lg btn-success"><i class="fa fa-plus-circle"></i> Thêm Manga mới</button>
		    </div>
		    <div class="userStory">
			<h3 class="color-blue">Danh sách Manga đã thêm </h3>
			<table class="table table-bordered">
			    <tr>
				<th class="text-center stt">#</th>
				<th class="name">Tên Mange</th>    
				<th class="img">Hình ảnh</th>
				<th class="status">Tình trạng</th>
				<th class="uptime">Upload time</th>
				<th class="action">Pick</th>
				<th class="action">Duyệt</th>
				<th class="action">Xóa</th>
			    </tr>
			    <tr>
				<td class="stt">1</td>
				<td>Special Martial Arts Extreme Hell Private High  School</td>
				<td><img src="images/img_2.jpg" width="46" height="46"></td>
				<td>Đang tiếp tục</td>
				<td>15/09/2015</td>
				<td><i class="fa fa-close color-red"></i></td>
				<td><i class="fa fa-check color-green"></i></td>
				<td><i class="fa fa-check color-green"></i></td>
			    </tr>
			</table>
		    </div>
		    <div class="pagingList">
			<div class="btn-group" role="group">
			    <button type="button" class="btn btn-default active">10</button>
			    <button type="button" class="btn btn-default">25</button>
			    <button type="button" class="btn btn-default">30</button>
			    <button type="button" class="btn btn-default">100</button>
			    <button type="button" class="btn btn-default">All</button>
			</div>
			<div class="btn-group" role="group">
			    <button type="button" class="btn btn-default">Search</button>
			</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>    
</div>
<div class="clear"></div>
<?php if (!empty(Yii::app()->session['status_change_pass'])): ?>
    <div class="modal fade" id="changePass" tabindex="-1" role="dialog" aria-labelledby="changePassLbl">
        <div class="modal-dialog" role="document">
    	<div class="modal-content">
    	    <div class="modal-header">
    		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    		<h4 class="modal-title" id="changePassLbl">Thông báo</h4>
    	    </div>
    	    <div class="modal-body">
    		<h4><?= Yii::app()->session['status_change_pass'] ?></h4>
    	    </div>
    	</div>
        </div>
    </div>
    <a href="javascript:;" data-toggle="modal" data-target="#changePass" id="when_changepass"></a>

    <script>
        $(document).ready(function() {
    	$("#when_changepass").trigger('click');
        });
    </script>
    <?php
    unset(Yii::app()->session['status_change_pass']);
endif;
?>
<?php if (!empty(Yii::app()->session['status_update_info'])): ?>
    <div class="modal fade" id="updateInfo" tabindex="-1" role="dialog" aria-labelledby="updateInfoLbl">
        <div class="modal-dialog" role="document">
    	<div class="modal-content">
    	    <div class="modal-header">
    		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    		<h4 class="modal-title" id="updateInfoLbl">Thông báo</h4>
    	    </div>
    	    <div class="modal-body">
    		<h4><?= Yii::app()->session['status_update_info'] ?></h4>
    	    </div>
    	</div>
        </div>
    </div>
    <a href="javascript:;" data-toggle="modal" data-target="#updateInfo" id="when_update_info"></a>

    <script>
        $(document).ready(function() {
    	$("#when_update_info").trigger('click');
        });
    </script>
    <?php
    unset(Yii::app()->session['status_update_info']);
endif;
?>