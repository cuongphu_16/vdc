
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
	'validateOnSubmit' => true,
    ),
    'htmlOptions' => array()
	));
?>
<?php if (isset($_GET['return']) && !empty($_GET['return'])): ?>
    <input type="hidden" id="return_url" name='return_url' value="<?= $_GET['return'] ?>" />
<?php endif; ?>
<div class="form-group">
    <?php echo $form->labelEx($model, 'username'); ?>
    <?php echo $form->textField($model, 'username', array('class' => 'form-login input-account', 'autocomplete' => 'off')); ?>
    <?php echo $form->error($model, 'username'); ?>
</div>
<div class="form-group">
    <?php echo $form->labelEx($model, 'password'); ?>
    <div class="position_relative">
	<?php echo $form->passwordField($model, 'password', array('class' => 'form-login input-password', 'value' => '')); ?>
	<a class="eye_pass input_password"></a>
	<?php echo $form->error($model, 'password'); ?>
    </div>                                
</div>
<div class="checkbox">
    <label class="fr">
	<a href="<?= Yii::app()->createUrl('user/forgotPass/lang/' . $this->langCode) ?>" class="<?= Yii::app()->MobileDetect->isMobile() || Yii::app()->MobileDetect->isTablet() ? "" : "fancybox fancybox.ajax" ?>"><?= Yii::t('main', 'I forgot my password') ?>!</a>
    </label>
    <label>
	<?php echo $form->checkBox($model, 'rememberMe', array()); ?>   
	<?= Yii::t('main', 'Remember me') ?>
    </label>
    <div class="clr"></div>
</div>
<input type="submit" class="btn btn-warning btn-block" value="<?= Yii::t('main', 'Signin') ?>">

<div class="help-block" style="padding-top: 10px;">
    <?= Yii::t('main', 'if you do not have an account, please') ?> <a href="<?= Yii::app()->createUrl('user/register') ?>"><?= Yii::t('main', 'Register now') ?>!</a>
</div>

<?php $this->endWidget(); ?>
