<?php if (!empty($log)): ?>
    <table class="table table-bordered">
        <tr>
    	<th class="text-center stt">#</th>
    	<th class="name">Manga</th>    
    	<th class="chap">Chapter mới đọc</th>
    	<th class="thdate">Ngày</th>
    	<th class="tdtime">Thời gian cập nhật</th>
    	<th class="action"></th>
        </tr>
	<?php
	$i = 0;
	foreach ($log as $l): $i++;
	    ?>
	    <tr>
		<td class="stt"><?= $i ?></td>
		<td><?= $l->chapter->manga->title ?></td>
		<td><a href="<?= $l->chapter->url ?>" target="blank"><?= $l->chapter->title ?></a></td>
		<td><?= date('H:i:s d/m/Y', strtotime($l->created_at)) ?></td>
		<td><?= date('H:i:s d/m/Y', strtotime($l->update_at)) ?></td>
		<td class="text-center"><button type="button" class="btn btn-danger remove_chapter" data-id="<?= $l->log_chapter_id ?>"><i class="fa fa-close"></i></button></td>
	    </tr>   
	<?php endforeach; ?>
    </table>

    <?php if ($total_page > 1): ?>
	<div class="paging">
	    <?php
	    $p = new pagination();
	    $p->items($total);
	    $p->limit($limit);
	    $p->itemClass('pagging_page');
	    $p->itemId('pages_');
	    $p->itemClick("ajaxlog");
	    $p->currentPage($pages);

	    $p->show();
	    ?>
	</div>
    <?php endif; ?>
    <script>

        $(".remove_chapter").click(function() {
    	var id = $(this).data('id');
    	$.ajax({
    	    'type': 'POST',
    	    'url': '<?= $this->createUrl('user/removeChapter') ?>',
    	    'cache': false,
    	    'data': {id: id},
    	    'success': function(data) {
    		if (data != '') {
    		    $(this).parent().parent().remove();
    		    ajaxlog(<?= $pages ?>);
    		} else {
    		    alert('Đã xảy ra lỗi');
    		}
    	    }
    	});
        });

    </script>
<?php else: ?>
    <center><h1>Bạn chưa theo dõi truyện nào!</h1></center>
<?php endif; ?>
