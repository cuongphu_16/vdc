<article>
    <div class="container">
        <div class="col_left register_box">
            <h1 class="titleDetail"><?=Yii::t('main', 'Information Member')?></h1>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'member_form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data',
                    'name' => 'member_form'
                ),
            ));
            ?>
            <?php if (Yii::app()->user->hasFlash('notices')): ?>
                <div class="line notices" style="color: red; padding: 5px 0 10px 170px"><?= Yii::app()->user->getFlash('notices') ?></div>
                <script>
                    $(function () {
                        $('html, body').animate({scrollTop: $(".line.notices").offset().top - 50}, 1000);
                    })
                </script>
            <?php endif; ?>
            <div class="line">
                <?php echo $form->labelEx($model, 'firstname', array('class' => 'lb')); ?>
                <?php echo $form->textField($model, 'firstname', array('class' => 'input')); ?>
                <div class="clearfix"></div>
                <?php echo $form->error($model, 'firstname'); ?>
            </div>
            <div class="line">
                <?php echo $form->labelEx($model, 'email', array('class' => 'lb')); ?>
                <?php echo $form->textField($model, 'email', array('class' => 'input', 'readonly' => true)); ?>
                <div class="clearfix"></div>
                <?php echo $form->error($model, 'email'); ?>
            </div>
            <div class="line">
                <?php echo $form->labelEx($model, 'address', array('class' => 'lb')); ?>
                <?php echo $form->textField($model, 'address', array('class' => 'input')); ?>
                <div class="clearfix"></div>
                <?php echo $form->error($model, 'address'); ?>
            </div>
            <div class="line">
                <?php echo $form->labelEx($model, 'phone', array('class' => 'lb')); ?>
                <?php echo $form->textField($model, 'phone', array('class' => 'input')); ?>
                <div class="clearfix"></div>
                <?php echo $form->error($model, 'phone'); ?>
            </div>
            <div class="line" align="right">
                <input type="submit" class="sb" value="<?= Yii::t('main', 'Update') ?>" />
            </div>
            <?php $this->endWidget(); ?>
        </div>
        <div class="col_right" style="margin-top: 50px;">
            <!-- news -->
            <?php $this->widget('WD_ListVideo') ?>
            <!-- end news -->
        </div>
        <div class="clearfix"></div>
    </div>
</article>