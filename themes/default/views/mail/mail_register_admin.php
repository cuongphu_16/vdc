<?php
$this->beginContent('/mail/index', array(
    'title' => Yii::t('main', 'New register from website'), 'name' => 'Admin Tainangnudoanhnhan.vn'
        )
);
?>
<?= Yii::t('main', 'Dear {user}', array('{user}' => $model->fullname)) ?>,<br/><br/>
<table spacepadding="0" cellpadding="5" border="1" style="border-collapse: collapse; border: #ccc 1px solid" width="100%">
    <tr>
        <td style="border: #ccc 1px solid"><?= Yii::t('main', 'Fullname') ?> :</td>
        <td style="border: #ccc 1px solid"><?= $model->fullname ?></td>
    </tr>
    <tr>
        <td style="border: #ccc 1px solid"><?= Yii::t('main','Email') ?> :</td>
        <td style="border: #ccc 1px solid"><?= $model->email ?></td>
    </tr>
    <tr>
        <td nowrap style="border: #ccc 1px solid"><?= Yii::t('main', 'Countries') ?> :</td>
        <td style="border: #ccc 1px solid"><?= SiteHelper::getCountriesLabel($model->countryId)?></td>
    </tr>
    <tr>
        <td style="border: #ccc 1px solid"><?= Yii::t('main', 'Region') ?> :</td>
        <td style="border: #ccc 1px solid"><?= SiteHelper::getRegionLabel($model->regionId) ?></td>
    </tr>
    <tr>
        <td style="border: #ccc 1px solid"><?= Yii::t('main','District') ?> :</td>
        <td style="border: #ccc 1px solid"><?= SiteHelper::getDistrictLabel($model->district_id) ?></td>
    </tr>
    <tr>
        <td nowrap style="border: #ccc 1px solid"><?= Yii::t('main', 'Address') ?> :</td>
        <td style="border: #ccc 1px solid"><?= $model->address ?></td>
    </tr>
    <tr>
        <td style="border: #ccc 1px solid"><?= Yii::t('main', 'Business') ?> :</td>
        <td style="border: #ccc 1px solid"><?= $model->business ?></td>
    </tr>
    <tr>
        <td style="border: #ccc 1px solid"><?= Yii::t('main','Field') ?> :</td>
        <td style="border: #ccc 1px solid"><?= $model->field ?></td>
    </tr>
    <tr>
        <td style="border: #ccc 1px solid"><?= Yii::t('main','Position') ?> :</td>
        <td style="border: #ccc 1px solid"><?= $model->position ?></td>
    </tr>
    <tr>
        <td nowrap style="border: #ccc 1px solid"><?= Yii::t('main', 'Website') ?> :</td>
        <td style="border: #ccc 1px solid"><?= $model->link ?></td>
    </tr>
    <tr>
        <td style="border: #ccc 1px solid"><?= Yii::t('main', 'About me') ?> :</td>
        <td style="border: #ccc 1px solid"><?= $model->about ?></td>
    </tr>
    <tr>
        <td style="border: #ccc 1px solid"><?= Yii::t('main', 'Created At') ?> :</td>
        <td style="border: #ccc 1px solid"><?= date('H:i d-m-Y', strtotime($model->created_at)) ?></td>
    </tr>
</table><br/><br/>
<?php echo Yii::t('main', 'Best Regards') ?>, <br/>
<?php echo Yii::app()->name ?>
<?php $this->endContent('/mail/index'); ?>