<?php
$this->beginContent('/mail/index', array(
    'title' => Yii::t('main', 'New contact from website'), 'name' => 'Admin Fairway.vn'
        )
);
?>
<?= Yii::t('main', 'Dear {user}', array('{user}' => $model->fullname)) ?>,<br/><br/>
<?= Yii::t('main', 'You just sent a mail contact to Fairway.vn with content') . ": " ?><br/><br/>
<table spacepadding="0" cellpadding="5" border="1" style="border-collapse: collapse; border: #ccc 1px solid" width="100%">
    <tr>
        <td><?= Yii::t('main', 'Fullname') ?> :</td>
        <td><?= $model->fullname ?></td>
    </tr>
     <tr>
        <td><?= Yii::t('main','Title') ?> :</td>
        <td><?= $model->title ?></td>
    </tr>
    <tr>
        <td>Email :</td>
        <td><?= $model->email ?></td>
    </tr>
    <tr>
        <td nowrap><?= Yii::t('main', 'Content') ?> :</td>
        <td><?= str_replace("\n", "<br>", $model->content) ?></td>
    </tr>
    <tr>
        <td><?= Yii::t('main', 'Created At') ?> :</td>
        <td><?= date('H:i d-m-Y', strtotime($model->created_at)) ?></td>
    </tr>
</table><br/><br/>
<?php echo Yii::t('main', 'Best Regards') ?>, <br/>
<?php echo Yii::app()->name ?>
<?php $this->endContent('/mail/index'); ?>