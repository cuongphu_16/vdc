<?php
$this->beginContent('/mail/index', array(
    'title' => Yii::t('main', 'New register from website'), 'name' => 'Admin mangalegend.com'
	)
);
?>
<?= Yii::t('main', 'Dear {user}', array('{user}' => $model->username)) ?>,<br/><br/>
<?= "Cám ơn bạn đã đăng ký tài khoản trên mangalegend: " ?><br/><br/>
<table spacepadding="0" cellpadding="5" border="1" style="border-collapse: collapse; border: #ccc 1px solid" width="100%">
    <tr>
        <td style="border: #ccc 1px solid"><?= Yii::t('main', 'Username') ?> :</td>
        <td style="border: #ccc 1px solid"><?= $model->username ?></td>
    </tr>
    <tr>
        <td style="border: #ccc 1px solid"><?= Yii::t('main', 'Email') ?> :</td>
        <td style="border: #ccc 1px solid"><?= $model->email ?></td>
    </tr>  

    <tr>
        <td style="border: #ccc 1px solid"><?= Yii::t('main', 'Created At') ?> :</td>
        <td style="border: #ccc 1px solid"><?= date('H:i d-m-Y', strtotime($model->created_at)) ?></td>
    </tr>
</table><br/><br/>
<?php echo Yii::t('main', 'Best Regards') ?>, <br/>
<?php echo Yii::app()->name ?>
<?php $this->endContent('/mail/index'); ?>