<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script  type="text/javascript" src="<?php echo $this->assetsUrl; ?>/js/jquery-1.7.2.js"></script>
        <title><?= $this->pageTitle ?></title>
    </head>
    <body>
        <?php echo $content; ?>
    </body>
</html>
