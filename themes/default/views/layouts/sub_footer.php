<div id="sub-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p>Copyright 2015 fairway.vn, All rights reserved</p>
            </div>
            <div class="col-md-6">
                <ul>
                    <?php if (Controller::settingGet('link_facebook') != FALSE): ?>
                        <li><a href="<?= Controller::settingGet('link_facebook') ?>"><i class="fa fa-facebook"></i></a></li>
                    <?php endif; ?>
                    <?php if (Controller::settingGet('link_twitter') != FALSE): ?>
                        <li><a href="<?= Controller::settingGet('link_twitter') ?>"><i class="fa fa-twitter"></i></a></li>    
                    <?php endif; ?>
                    <?php if (Controller::settingGet('link_googleplus') != FALSE): ?>
                        <li><a href="<?= Controller::settingGet('link_googleplus') ?>"><i class="fa fa-google"></i></a></li>
                            <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>