<!doctype html>
<html>    
    <?php $this->renderPartial('/layouts/header_inc'); ?>
    <body>      
        <div class="sidebar-menu-container" id="sidebar-menu-container">
            <div class="sidebar-menu-push">
                <div class="sidebar-menu-overlay"></div>
                <div class="sidebar-menu-inner">
                    <?php $this->renderPartial('/layouts/header'); ?>

                    <?php echo $content ?>
                </div>
                <?php $this->renderPartial('/layouts/footer'); ?>
            </div>
            <?php $this->renderPartial('/layouts/sub_footer'); ?>
        </div>
        <nav class="sidebar-menu slide-from-left">
            <div class="nano">
                <div class="content">
                    <nav class="responsive-menu">
                        <ul>
                            <li class="menu-item-has-children"><a href="#">Home</a>
                                <ul class="sub-menu">
                                    <li><a href="index.html">Homepage 1</a></li>
                                    <li><a href="homepage-2.html">Homepage 2</a></li>
                                    <li><a href="homepage-3.html">Homepage 3</a></li>
                                    <li><a href="homepage-4.html">Homepage 4</a></li>
                                    <li><a href="homepage-5.html">Homepage 5</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children"><a href="#">Pages</a>
                                <ul class="sub-menu">
                                    <li><a href="about.html">About Us</a></li>
                                    <li><a href="contact.html">Contact Us</a></li>
                                    <li><a href="404.html">404 Error Page</a></li>
                                    <li><a href="coming.html">Coming Soon</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children"><a href="#">Our Services</a>
                                <ul class="sub-menu">
                                    <li><a href="services-listing.html">Services Listing</a></li>
                                    <li><a href="construction.html">Construction</a></li>
                                    <li><a href="isolation.html">Isolation</a></li>
                                    <li><a href="renovation.html">Renovation</a></li>
                                    <li><a href="electric-welding.html">Electric Welding</a></li>
                                    <li><a href="sawn-stone.html">Sawn Stone</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children"><a href="#">Our Projects</a>
                                <ul class="sub-menu">
                                    <li><a href="projects.html">Projects Listing</a></li>
                                    <li><a href="projects-3-grids.html">3 Columns Grids</a></li>
                                    <li><a href="projects-3-full.html">3 Columns Full</a></li>
                                    <li><a href="projects-4-grids.html">4 Columns Grids</a></li>
                                    <li><a href="projects-4-full.html">4 Columns Full</a></li>
                                    <li><a href="single-project.html">Single Project</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children"><a href="#">Blog</a>
                                <ul class="sub-menu">
                                    <li><a href="blog-right.html">Blog Classic</a></li>
                                    <li><a href="blog-grid.html">Blog Grids</a></li>
                                    <li><a href="grid-right.html">Grids Sidebar</a></li>
                                    <li><a href="single-blog.html">Single Post</a></li>
                                </ul>
                            </li>
                            <li><a href="contact.html">Contact</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </nav>
    </body>
</html>