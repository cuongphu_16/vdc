<div id="sub-header">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="left-info">
                    <ul>
                        <li><i class="fa fa-phone"></i><?= Controller::settingGet('hotline') ?></li>
                        <li><i class="fa fa-envelope-o"></i><?= Controller::settingGet('sendmail_admin') ?></li>
                        <li><i class="fa fa-clock-o"></i><?= Controller::settingGet('working_time') ?>  </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="social-icons">
                    <ul>
                        <?php if (Controller::settingGet('link_facebook') != FALSE): ?>
                            <li><a href="<?= Controller::settingGet('link_facebook') ?>"><i class="fa fa-facebook"></i></a></li>
                        <?php endif; ?>
                        <?php if (Controller::settingGet('link_twitter') != FALSE): ?>
                            <li><a href="<?= Controller::settingGet('link_twitter') ?>"><i class="fa fa-twitter"></i></a></li>    
                        <?php endif; ?>
                        <?php if (Controller::settingGet('link_googleplus') != FALSE): ?>
                            <li><a href="<?= Controller::settingGet('link_googleplus') ?>"><i class="fa fa-google"></i></a></li>
                        <?php endif; ?>
                        <?php if ($this->langCode == 'en'): ?>
                            <li><span style="padding-right:2px"><a href="<?= Yii::app()->createUrl('main/index/lang/vi') ?>"><img src="<?= $this->assetsUrl ?>/images/vn.jpg" alt="" height="18px"></a></span></li>
                        <?php elseif ($this->langCode == 'vi'): ?>
                            <li><span style="padding-right:2px"><a href="<?= Yii::app()->createUrl('main/index/lang/en') ?>"><img src="<?= $this->assetsUrl ?>/images/en.jpg" alt="" height="18px"></a></span></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<header class="site-header">
    <div id="main-header" class="main-header header-sticky">
        <div class="inner-header container clearfix">

            <div class="logo">
                <a href="<?= Yii::app()->createUrl('main/index/lang/' . $this->langCode) ?>"><img src="<?= $this->assetsUrl ?>/images/logo.png" alt=""></a>
            </div>
            <div class="header-right-toggle pull-right hidden-md hidden-lg">
                <a href="javascript:void(0)" class="side-menu-button"><i class="fa fa-bars"></i></a>
            </div>
            <nav class="main-navigation text-right hidden-xs hidden-sm">
                <ul>
                    <li><a href="<?= Yii::app()->createUrl('main/index/lang/' . $this->langCode) ?>" class="<?= Yii::app()->controller->action->id == 'index' ? "active" : "" ?>"><?= Yii::t('main', 'Homepage') ?></a>
                    </li>
                    <?php
                    $cate_about = About::model()->findAllByAttributes(array('status' => 1, 'language' => $this->lang), array('select' => 'title,alias', 'order' => 'sort_order ASC, created_at DESC'));
                    ?>
                    <li><a href="<?= Yii::app()->createUrl('main/about/lang/' . $this->langCode) ?>" class="<?= !empty($cate_about) ? "has-submenu" : "" ?> <?= Yii::app()->controller->action->id == 'listAbout' || Yii::app()->controller->action->id == 'detailAbout' || Yii::app()->controller->action->id == 'about' ? "active" : "" ?>"><?= Yii::t('main', 'About us') ?></a>            
                        <?php
                        if (!empty($cate_about)):
                            ?>
                            <ul class="sub-menu">

                                <?php foreach ($cate_about as $ct_about): ?>
                                    <li><a href="<?= $ct_about->url ?>"><?= $ct_about->title ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                    <?php
//                    $cate_services = Category::model()->findAllByAttributes(array('status' => 1, 'type' => Category::TYPE_SERVICES, 'language' => $this->lang), array('select' => 'title,alias,type', 'order' => 'sort_order ASC, created_at DESC'));
                    ?>
                    <li><a href="<?= Yii::app()->createUrl('main/services/lang/' . $this->langCode) ?>" class="<?= !empty($cate_services) ? "has-submenu" : "" ?> <?= Yii::app()->controller->action->id == 'listServices' || Yii::app()->controller->action->id == 'detailServices' || Yii::app()->controller->action->id == 'services' ? "active" : "" ?>"><?= Yii::t('main', 'Services') ?></a>
                        <?php
                        if (!empty($cate_services)):
                            ?>
                            <ul class="sub-menu">

                                <?php foreach ($cate_services as $ct_services): ?>
                                    <li><a href="<?= $ct_services->url ?>"><?= $ct_services->title ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>

                    <?php
                    $cate_project = Category::model()->findAllByAttributes(array('status' => 1, 'type' => Category::TYPE_PROJECT, 'language' => $this->lang), array('select' => 'title,alias,type', 'order' => 'sort_order ASC, created_at DESC'));
                    ?>

                    <li><a href="<?= Yii::app()->createUrl('main/project/lang/' . $this->langCode) ?>" class="<?= !empty($cate_project) ? "has-submenu" : "" ?>" <?= Yii::app()->controller->action->id == 'project' || Yii::app()->controller->action->id == 'detailProject' ? "active" : "" ?>><?= Yii::t('main', 'Project') ?></a>
                        <?php
                        if (!empty($cate_project)):
                            ?>
                            <ul class="sub-menu">
                                <?php foreach ($cate_project as $ct_project): ?>
                                    <li><a href="<?= $ct_project->url ?>"><?= $ct_project->title ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                    <li><a href="<?= Yii::app()->createUrl('main/trading/lang/' . $this->langCode) ?>" class="<?= Yii::app()->controller->action->id == 'trading' || Yii::app()->controller->action->id == 'detailTrading' ? "active" : "" ?>"><?= Yii::t('main', 'Trading Floor') ?></a></li>
                    <li><a href="<?= Yii::app()->createUrl('main/news/lang/' . $this->langCode) ?>" class="<?= Yii::app()->controller->action->id == 'news' || Yii::app()->controller->action->id == 'detailNews' ? "active" : "" ?>"><?= Yii::t('main', 'News - Events') ?></a>
                    </li>
                    <li><a href="<?= Yii::app()->createUrl('main/recruitment/lang/' . $this->langCode) ?>" class="<?= Yii::app()->controller->action->id == 'recruitment' || Yii::app()->controller->action->id == 'detailRecruitment' ? "active" : "" ?>"><?= Yii::t('main', 'Recruitment') ?></a></li>
                    <li><a href="<?= Yii::app()->createUrl('main/contact/lang/' . $this->langCode) ?>" class="<?= Yii::app()->controller->action->id == 'contact' ? "active" : "" ?>"><?= Yii::t('main', 'Contact us') ?></a></li>    
                </ul>
            </nav>
        </div>
    </div>
</header>	