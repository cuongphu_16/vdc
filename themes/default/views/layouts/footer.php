<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-6 col-xs-12" style="margin-top: 20px">
                <div class="about-us">
                    <img src="<?= $this->assetsUrl ?>/images/logo.png" alt="">
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="company-pages">
                    <h2><?= Controller::settingGet('company_name') ?></h2>
                    <ul class="first-list">
                        <li><a href="#"><i class="fa fa-angle-double-right"></i><?= Controller::settingGet('info_contact_address') ?></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i><?= Controller::settingGet('hotline_footer') ?></a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Email: <?= Controller::settingGet('sendmail_admin') ?></a></li>

                    </ul>

                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12" style="color:#fff;margin-top: 20px">
                <?= Controller::settingGet('footer_right') ?>                
            </div>


        </div>
    </div>
</footer>



<a href="#" class="go-top"><i class="fa fa-angle-up"></i></a>


<script type="text/javascript" src="<?= $this->assetsUrl ?>/js/bootstrap.min.js"></script>
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script src="<?= $this->assetsUrl ?>/js/jquery.themepunch.tools.min.js"></script>
<script src="<?= $this->assetsUrl ?>/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="<?= $this->assetsUrl ?>/js/plugins.js"></script>
<script type="text/javascript" src="<?= $this->assetsUrl ?>/js/custom.js"></script>