<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <script type="text/javascript" src="<?= $this->assetsUrl ?>/js/jquery-1.11.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <title><?= $this->pageTitle ?></title>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/animate.css">
    <link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/flexslider.css">
    <link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/jquery-ui.css">
    <link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/simple-line-icons.css">
    <link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/icon-font.css">
    <link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/flat-icon.css">
    <link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/innovation.css">

    <link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/settings.css">



    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>