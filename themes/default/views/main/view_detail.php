<div id="page-heading">
    <div class="container">
        <div class="row">

        </div>
    </div>

</div>
<section class="blog-classic" style="padding:10px;">
    <div class="container">
        <div class="row">
            <div class="col-md-3">

                <div class="side-bar">
                    <div class="sidebar-widget">
                        <h4><?= Yii::t('main', 'Category') ?></h4>
                        <div class="line-dec"></div>
                        <ul>
                            <?php if (!empty($list_cate)): ?>
                                <?php foreach ($list_cate as $cate): ?>
                                    <li class="<?= !empty($_GET['url_cate']) && $_GET['url_cate'] == $cate->alias ? "active" : "" ?> "><a href="<?= $cate->url ?>">:: &nbsp;&nbsp; <?= $cate->title ?></a></li>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <li class="<?= Yii::app()->controller->action->id == 'about' || Yii::app()->controller->action->id == 'detailAbout' ? "active" : "" ?>"><a href="<?= Yii::app()->createUrl('main/about/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'About') ?></a></li>
                                <li class="<?= Yii::app()->controller->action->id == 'services' ? "active" : "" ?>"><a href="<?= Yii::app()->createUrl('main/services/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'Services') ?></a></li>
                                <li class="<?= Yii::app()->controller->action->id == 'trading' || Yii::app()->controller->action->id == 'detailTrading' ? "active" : "" ?>"><a href="<?= Yii::app()->createUrl('main/trading/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'Trading Floor') ?></a></li>
                                <li class="<?= Yii::app()->controller->action->id == 'news' || Yii::app()->controller->action->id == 'detailNews' ? "active" : "" ?>"><a href="<?= Yii::app()->createUrl('main/news/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'News') ?></a></li>
                                <li><a href="<?= Yii::app()->createUrl('main/recruitment/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'Recruitment') ?></a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-md-9">
                <div class="blog-posts">
                    <h3><strong><?= $detail->title ?></strong></h3>
                    <div class="blog-posts">
                        <?= $detail->content; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php if (!empty($other_cate)): ?>
                    <div class="col-md-12" style="margin-top: 20px">
                        <h5 style="    border-top: 1px solid;padding-top: 5px;"><strong><?= Yii::t('main', 'Other Post') ?></strong></h5>
                        <ul>
                            <?php foreach ($other_cate as $other): ?>
                                <li><a href="<?= $other->url ?>" class="hover_a"><?= $other->title ?></a></li>
                                <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>

            </div>
        </div>

    </div>
</section>
