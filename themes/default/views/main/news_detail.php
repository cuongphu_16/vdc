<div class="wrapper-main">
    <div class="wrapper-left">
	<div class="news-detail">
	    <h2><?= $detail->title ?></h2>
	    <p class="date"><?= date('H:i:s d/m/Y', strtotime($detail->created_at)) ?></p>
	    <div class="share" style="margin-bottom: 20px">
		<div class="fb-like" data-href="<?= $detail->url ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true" style="float: left"></div>
		<div id="fb-root"></div>
		<script>(function (d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id))
			    return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0";
			fjs.parentNode.insertBefore(js, fjs);
		    }(document, 'script', 'facebook-jssdk'));</script>

		<script src="https://apis.google.com/js/platform.js" async defer>
		    {
			lang: 'vi'
		    }
		</script>

		<div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?= $detail->url ?>"></div>	   
	    </div>
	    <?= $detail->content ?>
	    <div class="clear"></div>	    
	</div>
    </div>
    <div class="wrapper-right">
	<div class="widgets box-comfb">
	    <div class="widgets-content">
		<div class="fb-comments" data-href="<?= Yii::app()->request->hostInfo . Yii::app()->request->url ?>" data-width="335" data-numposts="5"></div>  
	    </div>
	</div>
	<?php if (!empty($other_news)): ?>
    	<div class="widgets box-comfb">
    	    <div class="boxtitle"><?= Yii::t('main','Other Post') ?></div>
    	    <div class="widgets-content news_ohter uln">
    		<ul>
			<?php foreach ($other_news as $other): ?>
			    <li>
                                <h4><a href="<?= $other->url ?>" class="hover_a"><?= $other->title ?></a></h4>
				<span class="date"><?= date('H:i:s d/m/Y', strtotime($other->created_at)) ?></span>
			    </li>
			<?php endforeach; ?>
    		</ul>
    	    </div>
    	</div>
	<?php endif; ?>
    </div>
</div>
<div class="clear"></div>