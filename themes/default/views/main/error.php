
<div class="container">
    <div class="box box-radius register-box _padd20">
        <div class="box-container" align="center">
            <h1><?= Yii::t('main', 'No links found') . "!" ?></h1>
            <h5><?= Yii::t('main', 'You can visit the homepage or use the search box on the right') ?></h5>
        </div>
    </div>
</div>