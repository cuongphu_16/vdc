<div class="wrapper-main">
    <div class="search-adv">
	<form method="post" id="form-adv-search">
	    <h3>Tìm kiếm nâng cao truyện MangaLegend</h3>
	    <div class="row form-horizontal">
		<div class="col-md-1 text-right control-label">
		    Tên truyện:
		</div>
		<div class="col-md-4">
		    <input type="text" class="form-control" placeholder="" name="title" value="<?= $title ?>">
		</div>
		<div class="col-md-1 text-right control-label">
		    Tác giả:
		</div>
		<div class="col-md-4">
		    <input type="text" class="form-control" placeholder="" name="author" value="<?= $author ?>">
		</div>
	    </div>
	    <?php if (!empty($list_cate)): ?>
    	    <div class="row search-story">
    		<div class="col-md-1 text-right">
    		    Thể loại:
    		</div>
    		<div class="col-md-11">
    		    <ul>
			    <?php foreach ($list_cate as $cate_manga): ?>
				<li><label class="checkbox"><input type="checkbox" name="check_list_manga_category[]" value="<?= $cate_manga->cate_id ?>" <?= !empty($arr_manga_category) && in_array($cate_manga->cate_id, $arr_manga_category) ? "checked" : "" ?> /><span><?= $cate_manga->title ?></span></label></li>
			    <?php endforeach; ?>   
    		    </ul>
    		</div>
    	    </div>
	    <?php endif; ?>
	    <div class="row search-chap">
		<div class="col-md-1 text-right">
		    Chappers:
		</div>
		<div class="col-md-11">
		    <ul>
			<li><label class="checkbox"><input name="chapter_quantity" type="checkbox" value="1" class="chapter_quantity"/><span>1+</span></label></li>
			<li><label class="checkbox"><input name="chapter_quantity" type="checkbox" value="5" class="chapter_quantity"/><span>5+</span></label></li>
			<li><label class="checkbox"><input name="chapter_quantity" type="checkbox" value="10" class="chapter_quantity"/><span>10+</span></label></li>
			<li><label class="checkbox"><input name="chapter_quantity" type="checkbox" value="20" class="chapter_quantity"/><span>20+</span></label></li>
			<li><label class="checkbox"><input name="chapter_quantity" type="checkbox" value="30" class="chapter_quantity"/><span>30+</span></label></li>
			<li><label class="checkbox"><input name="chapter_quantity" type="checkbox" value="50" class="chapter_quantity"/><span>50+</span></label></li>
			<li><label class="checkbox"><input name="chapter_quantity" type="checkbox" value="100" class="chapter_quantity"/><span>100+</span></label></li>
			<li><label class="checkbox"><input name="chapter_quantity" type="checkbox" value=150 class="chapter_quantity"/><span>150+</span></label></li>
			<li><label class="checkbox"><input name="chapter_quantity" type="checkbox" value="200" class="chapter_quantity"/><span>200+</span></label></li>
		    </ul>
		</div>
	    </div>
	    <?php if (!empty($list_manga_status)): ?>
    	    <div class="row search-status">
    		<div class="col-md-1 text-right">
    		    Tình trạng:
    		</div>
    		<div class="col-md-11">
    		    <ul>
			    <?php foreach ($list_manga_status as $manga_status): ?>
				<li><label class="checkbox"><input name="check_list_manga_status[]" type="checkbox" value="<?= $manga_status->cate_id ?>" <?= !empty($arr_manga_status) && in_array($manga_status->cate_id, $arr_manga_status) ? "checked" : "" ?> /><span><?= $manga_status->title ?></span></label></li>
			    <?php endforeach; ?>    			
    		    </ul>
    		</div>
    	    </div>
	    <?php endif; ?>
	    <div class="row search-rating">
		<div class="col-md-1 text-right">
		    Đánh giá:
		</div>
		<div class="col-md-11">
		    <ul>
			<li><label class="checkbox"><input type="checkbox" /><span>5</span><span class="rating"><span class="star active"></span></span></label></li>
			<li><label class="checkbox"><input type="checkbox" /><span>4</span><span class="rating"><span class="star active"></span></span></label></li>
			<li><label class="checkbox"><input type="checkbox" /><span>3</span><span class="rating"><span class="star active"></span></span></label></li>
			<li><label class="checkbox"><input type="checkbox" /><span>2</span><span class="rating"><span class="star active"></span></span></label></li>
			<li><label class="checkbox"><input type="checkbox" /><span>1</span><span class="rating"><span class="star active"></span></span></label></li>
			<li><label class="checkbox"><input type="checkbox" /><span>0</span><span class="rating"><span class="star"></span></span></label></li>
		    </ul>
		</div>
	    </div>
	    <?php if (!empty($list_type)): ?>
    	    <div class="row search-type">
    		<div class="col-md-1 text-right">
    		    Kiểu truyện:
    		</div>
    		<div class="col-md-11">
    		    <ul>    		
			    <?php foreach ($list_type as $type): ?>
				<li><label class="checkbox"><input name="check_list_type[]" type="checkbox" value="<?= $type->cate_id ?>" <?= !empty($arr_type) && in_array($type->cate_id, $arr_type) ? "checked" : "" ?>/><span><?= $type->title ?></span></label></li>
			    <?php endforeach; ?>			     			
    		    </ul>
    		</div>
    	    </div>
	    <?php endif; ?>
	    <div class="search-choose">
		<div class="row">
		    <div class="col-md-1 text-right">
			Đã chọn:
		    </div>
		    <div class="col-md-11">
			<input type="text" id="search-choose" name="choose" placeholder="Chon" />
		    </div>
		</div>
	    </div>
	    <div class="row search-btn">
		<div class="col-md-12">
		    <button type="submit" class="btn btn-lg btn-danger">Tìm kiếm</button><span class="split"></span><button type="reset" class="btn btn-lg btn-default reset-form">Xóa lựa chọn</button>
		</div>
	    </div>
	</form>
    </div>

    <div class="search-list">
	<?php if (!empty($list_manga)): ?>
    	<table class="table">
    	    <thead>
    		<tr>
    		    <th>Tên truyện</th>
    		    <th>Tên tác giả/ nhóm dịch</th>
    		    <th width="120">Đánh giá</th>
    		    <!--<th width="100" class="text-right">Chappers</th>-->
    		    <th width="100" class="text-right">Lượt xem</th>
    		    <th width="165" class="text-right">Cập nhật</th>
    		</tr>
    	    </thead>
    	    <tbody>
		    <?php foreach ($list_manga as $manga): ?>
			<tr>
			    <td><a href="<?= $manga->url ?>" class="name"><?= $manga->title ?></a></td>
			    <td><?= $manga->author ?> <?= !empty($manga->group_id) ? "/ " . $manga->group->title : "" ?></td>
			    <td><span class="rating"><span class="star active"></span><span class="star"></span><span class="star"></span><span class="star"></span><span class="star"></span></span></td>
			    <!--<td class="text-right"><a href="#" class="chap">Chap 12</a></td>-->
			    <td class="text-right"><?= $manga->view_count ?></td>
			    <td class="text-right">
				<?php
				$date = SiteHelper::getDate($manga->created_at);
				if ($date > 0)
				    echo 'Cách đây ' . $date . ' ngày';
				else
				    echo 'Mới cập nhật';
				?>
			    </td>
			</tr>
		    <?php endforeach; ?>
    	    </tbody>
    	</table>
	<?php else: ?>
    	<div style="position: relative">
    	    <center><h1><?= Yii::t('main', 'No result for your criteria') ?></h1></center>
    	</div>
	<?php endif; ?>
    </div>

</div>
<div class="clear"></div>
<script defer type="text/javascript" language="javascript" src="<?= $this->assetsUrl ?>/js/jquery.tokeninput.js"></script>

<script>
    $(document).ready(function () {
	$(".reset-form").click(function () {
	    $(location).attr('href', '<?= Yii::app()->createUrl('main/search') ?>');
	});
	$("#search-choose").tokenInput([
	    {id: 7, name: "Ruby"},
	    {id: 11, name: "Python"},
	    {id: 13, name: "JavaScript"},
	    {id: 17, name: "ActionScript"},
	    {id: 19, name: "Scheme"},
	    {id: 23, name: "Lisp"},
	    {id: 29, name: "C#"},
	    {id: 31, name: "Fortran"},
	    {id: 37, name: "Visual Basic"},
	    {id: 41, name: "C"},
	    {id: 43, name: "C++"},
	    {id: 47, name: "Java"}
	], {
	    prePopulate: [
		{id: 7, name: "Ruby"},
		{id: 11, name: "Python"},
		{id: 13, name: "JavaScript"}
	    ]
	});
	$('.chapter_quantity').change(function () {
	    $('.chapter_quantity').removeAttr('checked');
	    $(this).prop('checked', 'checked');
	});
    });

</script>