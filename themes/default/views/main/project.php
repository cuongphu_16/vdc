<div id="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?= Yii::t('main', 'Projects') ?></h1>
            </div>
        </div>
    </div>
</div>
<section class="projects-page">
    <div class="container">
        <?php if (!empty($list_project)): ?>
            <div class="row">
                <div class="group text-center">
                    <span class="btn filter active" data-filter="all"><?= Yii::t('main', 'All') ?></span>
                    <?php
                    if (!empty($list_cate_project)):
                        foreach ($list_cate_project as $cate_project):
                            ?>
                            <span class="btn filter" data-filter=".category-<?= $cate_project->cate_id ?>"><?= $cate_project->title ?></span>
                            <?php
                        endforeach;
                    endif;
                    ?>                   

                </div>
            </div>
            <div class="row">
                <div class="projects">
                    <?php foreach ($list_project as $project): ?>
                        <div class="mix second-row col-md-3 category-<?= $project->cate_id ?> ">
                            <div class="thumb-holder">
                                <a href="<?= $project->url ?>"><img src="<?= $project->getThumb(270, 270)->src ?>" alt="<?= $project->title ?>"></a>
                                <div class="thumb-content">
                                    <div class="thumb-text">
                                        <a href="<?= $project->url ?>"><h4><?= SiteHelper::cut_string($project->title,35) ?></h4></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>                      
                </div>                                        
            </div>
        <?php else: ?>
            <center><h1><?= Yii::t('main', 'The content is being updated') ?></h1></center>
        <?php endif; ?>
    </div>
</section>	