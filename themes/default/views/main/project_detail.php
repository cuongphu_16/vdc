<link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/jquery.ad-gallery.css">
<script type="text/javascript" src="<?= $this->assetsUrl ?>/js/jquery.ad-gallery.min.js"></script>


<script type="text/javascript">
    $(function() {
        $('img.fig').data('');
        $('img.mix').data('');
        $('img.orang').data('');
        $('img.image3').data('');
        var galleries = $('.ad-gallery').adGallery({slideshow: {
                enable: true,
                autostart: true,
                speed: 5000,
                start_label: 'Start',
                stop_label: 'Stop',
                // Should the slideshow stop if the user scrolls the thumb list?
                stop_on_scroll: true,
                // Wrap around the countdown
                countdown_prefix: '(',
                countdown_sufix: ')',
                onStart: function() {
                    // Do something wild when the slideshow starts
                },
                onStop: function() {
                    // Do something wild when the slideshow stops
                }
            }});
        $('#switch-effect').change(
                function() {
                    galleries[0].settings.effect = $(this).val();
                    return false;
                }
        );
        $('#toggle-slideshow').click(
                function() {
                    galleries[0].slideshow.toggle();
                    return false;
                }
        );
        $('#toggle-description').click(
                function() {
                    if (!galleries[0].settings.description_wrapper) {
                        galleries[0].settings.description_wrapper = $('#descriptions');
                    } else {
                        galleries[0].settings.description_wrapper = false;
                    }
                    return false;
                }
        );
    });
</script>

<div id="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
    </div>
</div>

<section class="single-project">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div id="gallery" class="ad-gallery">
                    <div class="ad-nav">
                        <div class="ad-thumbs">
                            <ul class="ad-thumb-list">
                                <?php
                                $list_thumbnai = $detail->getSlideImage();
                                if (isset($list_thumbnai) && !empty($list_thumbnai)):
                                    $stt = 0;
                                    foreach ($list_thumbnai as $thumbnai):
                                        ?>  
                                        <li>
                                            <a href="<?= $detail->getThumbSlide(780, 500, $thumbnai)->src ?>"><img src="<?= $detail->getThumbSlide(780, 500, $thumbnai)->src ?>" alt="<?= $detail->title ?>" class="image<?= $stt ?>" /></a>
                                        </li>
                                        <?php
                                        $stt++;
                                    endforeach;
                                endif;
                                ?>     
                            </ul>
                        </div>
                    </div>
                    <div class="ad-image-wrapper">
                    </div>
                </div>
            </div>

            <div class="col-md-4" style="background-color: #2e2d32;color: #fff; padding-bottom: 10px;margin-top: 5px">
                <div class="left-info" style="min-height: 465px;">
                    <h4><?= $detail->title ?></h4>
                    <div><?= $detail->desc ?></div>

                </div>
            </div>

            <div class="col-md-12" style="margin-top: 20px">
                <?= $detail->content ?>
            </div>
        </div>
    </div>
</section>