<div id="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?= Yii::t('main', 'Contact us') ?></h1>
            </div>
        </div>
    </div>
</div>
<section class="blog-classic" style="padding:10px;">
    <div class="container">
        <div class="row">
            <div class="col-md-3">

                <div class="side-bar">
                    <div class="sidebar-widget">
                        <h4><?= Yii::t('main', 'Category') ?></h4>
                        <div class="line-dec"></div>
                        <ul>                            
                            <li><a href="<?= Yii::app()->createUrl('main/about/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'About') ?></a></li>
                            <li><a href="<?= Yii::app()->createUrl('main/services/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'Services') ?></a></li>
                            <li class="<?= Yii::app()->controller->action->id == 'trading' || Yii::app()->controller->action->id == 'detailTrading' ? "active" : "" ?>"><a href="<?= Yii::app()->createUrl('main/trading/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'Trading Floor') ?></a></li>
                            <li class="<?= Yii::app()->controller->action->id == 'news' || Yii::app()->controller->action->id == 'detailNews' ? "active" : "" ?>"><a href="<?= Yii::app()->createUrl('main/news/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'News') ?></a></li>
                            <li><a href="<?= Yii::app()->createUrl('main/recruitment/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'Recruitment') ?></a></li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-md-9">
                <div class="map" style="margin-top: 20px">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.3034218725406!2d105.84438531433852!3d21.02054199343605!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab91f5e683d9%3A0xba184aa6f6287f30!2zNTcgVHLhuqduIFF14buRYyBUb-G6o24sIFRy4bqnbiBIxrBuZyDEkOG6oW8sIEhvw6BuIEtp4bq_bSwgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1452826339683" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>

                <div id="form-contact" class="col-md-12">
                    <h3><?= Yii::t('main', 'Send Contact from Fairway') ?></h3>                    
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'contact_form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                        ),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data',
                            'name' => 'contact_form'
                        ),
                    ));
                    ?>
                    <div class="form-contact">
                        <?php echo $form->textField($model, 'fullname', array('placeholder' => Yii::t('main', 'Fullname'), 'class' => 'form-contact')); ?>
                        <?php echo $form->error($model, 'fullname'); ?>
                    </div>
                    <div class="form-contact">
                        <?php echo $form->textField($model, 'email', array('placeholder' => Yii::t('main', 'Email'), 'class' => 'form-contact')); ?>
                        <?php echo $form->error($model, 'email'); ?>
                    </div>
                    <div class="clearfix"></div>                    
                    <div class="form-contact">
                        <?php echo $form->textField($model, 'title', array('placeholder' => Yii::t('main', 'Title'), 'class' => 'form-contact')); ?>
                        <?php echo $form->error($model, 'title'); ?>
                    </div>
                    <div class="form-contact">
                        <?php echo $form->textField($model, 'phone', array('placeholder' => Yii::t('main', 'Phone'), 'class' => 'form-contact')); ?>
                        <?php echo $form->error($model, 'phone'); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div>

                        <?php echo $form->textArea($model, 'content', array('placeholder' => Yii::t('main', 'Content'), 'maxlength' => 500)); ?>
                        <?php echo $form->error($model, 'content'); ?>              
                    </div>

                    <div>
                        <input type="submit" class="btn btn-default" value="Gửi đi">
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>

    </div>
</section>

<style>
    #contact_form input[type='submit']{
        margin-left: 5px;
        background: #e8aa00;
        border: 1px solid #C99506;
        padding: 8px 25px;
        color: #fff;
        font-size: 17px;
        border-radius: 5px;
        margin-top: 5px;
    }
    #contact_form input[type='submit']:hover{
        -webkit-transition: all 0.40s ease-in-out;
        -moz-transition: all 0.40s ease-in-out;
        -ms-transition: all 0.40s ease-in-out;
        -o-transition: all 0.40s ease-in-out;
        background: #C99506
    }
    #contact_form div.form-contact{
        width: 45%;
        float: left;
        margin: 5px;        
        display: inline-block
    }
    #contact_form input[type='text']{
        width: 100%;
        padding: 5px;
    }
    #contact_form textarea{
        width: 91.3%;
        margin-left: 5px;
        margin-top: 5px;
        height: 75px;
        padding: 5px;
    }
    .errorMessage{color: red}
</style>
<?php if (isset(Yii::app()->session['contact_success']) && !empty(Yii::app()->session['contact_success'])): ?>
    <script>
        alert('<?= Yii::t('main', 'Thank you! We will get back to you as soon as possible') ?>');
    </script>
    <?php
    unset(Yii::app()->session['contact_success']);
endif;
?>