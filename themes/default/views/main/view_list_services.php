<link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/cuong.css">
<div id="page-heading">
    <div class="container">
        <div class="row">

        </div>
    </div>

</div>
<section class="blog-classic" style="padding:10px;">
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 list_services">
                <h3 class="h3_title_services"><?= Yii::t('main','Our Services') ?></h3>
                <?php if(Controller::settingGet('about_services') != FALSE): ?>
                <div style="margin-bottom: 20px"><?= Controller::settingGet('about_services') ?></div>
                <?php endif; ?>
                <?php if (!empty($list)): ?>
                    <?php
                    $i = 0;
                    foreach ($list as $p): $i++;
                        ?>
                        <div class="div <?= $i % 2 == 0 ? "chan" : "le" ?>">
                            <p><a href="<?= $p->url ?>" class="title" title="<?= $p->title ?>"><?= SiteHelper::cut_string($p->title, 25) ?></a><a href="<?= $p->url ?>"><span class="icon-right"></span></a></p>
                            <img src="<?= $p->getImage(400, 200)->src ?>" />
                        </div>                        
                        <?= $i % 2 == 0 ? '<div class="clearfix"></div>' : '' ?>
                    <?php endforeach; ?>

                    <!-- cuongnx-->

                    <div class="col-md-12">
                        <div class="pagination">
                            <?php
                            $this->widget('XPCLinkPager', array(
                                'pages' => $pages,
                                'nextPageLabel' => '&raquo',
                                'prevPageLabel' => '&laquo',
                                'firstPageLabel' => '',
                                'lastPageLabel' => '',
                                'url' => Yii::app()->request->hostInfo . Yii::app()->request->url,
                            ));
                            ?>                                
                        </div>
                    </div>
                <?php else: ?>
                    <center><h1><?= Yii::t('main', 'The content is being updated') ?></h1></center>
                <?php endif; ?>
            </div>
            <div class="col-md-3">

                <div class="side-bar">
                    <div class="sidebar-widget">
<!--                        <h4><?= Yii::t('main', 'Category') ?></h4>
                        <div class="line-dec"></div>
                        <ul>
                        <?php if (!empty($list_cate)): ?>
                            <?php foreach ($list_cate as $cate): ?>
                                                                                            <li class="<?= !empty($_GET['url_about']) && $_GET['url_about'] == $cate->alias ? "active" : "" ?> <?= !empty($_GET['url_services']) && $_GET['url_services'] == $cate->alias ? "active" : "" ?>"><a href="<?= $cate->url ?>">:: &nbsp;&nbsp; <?= $cate->title ?></a></li>
                            <?php endforeach; ?>
                        <?php else: ?>
                                                            <li><a href="<?= Yii::app()->createUrl('main/about/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'About') ?></a></li>
                                                            <li><a href="<?= Yii::app()->createUrl('main/services/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'Services') ?></a></li>
                                                            <li class="<?= Yii::app()->controller->action->id == 'trading' || Yii::app()->controller->action->id == 'detailTrading' ? "active" : "" ?>"><a href="<?= Yii::app()->createUrl('main/trading/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'Trading Floor') ?></a></li>
                                                            <li class="<?= Yii::app()->controller->action->id == 'news' || Yii::app()->controller->action->id == 'detailNews' ? "active" : "" ?>"><a href="<?= Yii::app()->createUrl('main/news/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'News') ?></a></li>
                                                            <li><a href="<?= Yii::app()->createUrl('main/recruitment/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'Recruitment') ?></a></li>
                        <?php endif; ?>
                        </ul>-->
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>
