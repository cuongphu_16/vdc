<div class="wrapper-main">
    <div class="news-page uln">
	<?php if (!empty($list_news)): ?>
    	<ul class="new-list">
		<?php foreach ($list_news as $news): ?>
		    <li>
			<a href="<?= $news->url ?>"><img src="<?= $news->getImage(148, 148)->src ?>" /></a>
			<h4><a href="<?= $news->url ?>"><?= $news->title ?></a></h4>
			<span class="date"><?= date('H:i:s d/m/Y', strtotime($news->created_at)) ?></span>
			<p><?= !empty($news->description) ? SiteHelper::cut_string(strip_tags($news->description), 150) : SiteHelper::cut_string(strip_tags($news->content), 150) ?></p>
		    </li>
		<?php endforeach; ?>
    	</ul>
	<?php else: ?>
    	<center><h1><?= Yii::t('main', 'The content is being updated') ?></h1></center>
	<?php endif; ?>
	<div class="clear"></div>
	<div class="paging">
	    <?php
	    $this->widget('XPCLinkPager', array(
		'pages' => $pages,
		'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
		'prevPageLabel' => '<i class="fa fa-angle-left"></i>',
		'firstPageLabel' => '<i class="fa fa-angle-double-left"></i>',
		'lastPageLabel' => '<i class="fa fa-angle-double-right"></i>',
		'url' => Yii::app()->request->hostInfo . Yii::app()->request->url,
	    ));
	    ?>      
	</div>
    </div>
</div>
<div class="clear"></div>