<link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/jquery-ui-google.css">
<?php if (!empty($list_slide)): ?>
    <div class="slider">
        <div class="fullwidthbanner-container">
            <div class="fullwidthbanner">
                <ul>
                    <?php foreach ($list_slide as $slide): ?>
                        <li class="first-slide" data-transition="fade" data-slotamount="10" data-masterspeed="300">
                            <img src="<?= $slide->getImage(1349, 680)->src ?>" data-fullwidthcentering="on" alt="<?= $slide->title ?>">
                            <div class="tp-caption first-line lft tp-resizeme start" data-x="left" data-hoffset="0" data-y="180" data-speed="1000" data-start="200" data-easing="Power4.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0" data-endelementdelay="0"><?= $slide->title ?></div>
                            <div class="tp-caption first-line lft tp-resizeme start" data-x="left" data-hoffset="0" data-y="340" data-speed="1000" data-start="200" data-easing="Power4.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0" data-endelementdelay="0"><div class="line-dec"></div></div>
                            <div class="tp-caption second-line lfb tp-resizeme start" data-x="left" data-hoffset="0" data-y="380" data-speed="1000" data-start="800" data-easing="Power4.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0" data-endelementdelay="0"><?= $slide->description ?></div>

                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>		
<?php endif; ?>

<!--<section class="our-services">    
    <div class="container">
        <div class="row">
            <div class="section-heading col-md-12">
                <h2><?= Yii::t('main', 'Our Services') ?></h2>
                <div class="line-dec"></div>
            </div>
        </div>
<?php if (!empty($list_services)): ?>
                <div class="row">
    <?php foreach ($list_services as $services): ?>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="service-item text-center">
                                    <div class="flipper">
                                        <div class="front">
                                            <i class="fa fa-building"></i>
                                            <h2><?= $services->title ?></h2>
                                        </div>
                                        <a href="<?= $services->url ?>">
                                            <div class="back">
                                                <h2><?= $services->title ?></h2>
                                                <p><?= SiteHelper::cut_string(strip_tags($services->description), 200) ?>
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
    <?php endforeach; ?>               
                </div>
<?php else: ?>
                <center><h1><?= Yii::t('main', 'The content is being updated') ?></h1></center>
<?php endif; ?>
    </div>

</section>-->

<section class="recent-projects">
    <div class="container">
        <div class="row">
            <div class="section-heading col-md-6">
                <h2>Dự án đã và đang triển khai</h2>
                <div class="line-dec"></div>
            </div>
        </div>
        <?php if (!empty($list_project)): ?>
            <div class="group text-right">
                <span class="btn filter active" data-filter="all"><?= Yii::t('main', 'All') ?></span>
                <?php
                if (!empty($list_cate_project)):
                    foreach ($list_cate_project as $cate_project):
                        ?>
                        <span class="btn filter" data-filter=".category-<?= $cate_project->cate_id ?>"><?= $cate_project->title ?></span>
                        <?php
                    endforeach;
                endif;
                ?>

            </div>
            <div class="row">
                <div class="projects">
                    <div id="owl-recent">
                        <?php foreach ($list_project as $project): ?>
                            <div class="mix category-<?= $project->cate_id ?> ">
                                <div class="thumb-holder">
                                    <a href="<?= $project->url ?>"><img src="<?= $project->getThumb(270, 270)->src ?>" alt="<?= $project->title ?>"></a>
                                    <div class="thumb-content">
                                        <div class="thumb-text">
                                            <a href="<?= $project->url ?>"><h4><?= SiteHelper::cut_string($project->title,35) ?></h4></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>                      
                    </div>
                </div>
            </div>
        <?php else: ?>
            <center><h1><?= Yii::t('main', 'The content is being updated') ?></h1></center>
        <?php endif; ?>
    </div>
</section>
<?php if (!empty($list_news) || !empty($list_trading) || !empty($list_rec)) : ?>
    <section class="latest-news" style="padding-top:0px !important;">
        <div class="container">
            <div class="row">
                <div class="section-heading col-md-12">
                    <h2 style=""><?= Yii::t('main', 'Featured Articles') ?></h2>
                    <div class="line-dec"></div>
                </div>
            </div>
            <div class="row">

                <div id="tabs">
                    <ul>
                        <?php if (!empty($list_rec)): ?>
                            <li><a href="#tabs-3"><?= Yii::t('main', 'Recruitment') ?></a></li>
                        <?php endif; ?>
                        <?php if (!empty($list_trading)): ?>
                            <li><a href="#tabs-2"><?= Yii::t('main', 'Trading Floor') ?></a></li>
                        <?php endif; ?>
                        <?php if (!empty($list_news)): ?>
                            <li><a href="#tabs-1"><?= Yii::t('main', 'News') ?></a></li>
                        <?php endif; ?>
                    </ul>          
                    <?php if (!empty($list_news)): ?>
                        <div id="tabs-1">                   
                            <div class="col-md-12">
                                <?php
                                $i = 0;
                                foreach ($list_news as $news): $i++;
                                    ?>
                                    <div class="news_home <?= $i % 2 == 0 ? 'right' : 'left' ?>">
                                        <a href="<?= $news->url ?>"><img src="<?= $news->getImage(100, 60)->src ?>" alt="<?= $news->title ?>"></a>
                                        <div class="right-content1">
                                            <a href="<?= $news->url ?>"><h6><?= $news->title ?></h6></a>    
                                            <p><?= !empty($news->description) ? SiteHelper::cut_string(strip_tags($news->description), 80) : SiteHelper::cut_string(strip_tags($news->content), 80) ?></p>   
                                        </div>
                                    </div>                               
                                    <?= $i % 2 == 0 || $i == count($list_news) ? "</div> <div class='clear'></div><div class='col-md-12'>" : "" ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (!empty($list_trading)): ?>
                        <div id="tabs-2">
                            <div class="col-md-12">
                                <?php
                                $i_tds = 0;
                                foreach ($list_trading as $trading): $i_tds++;
                                    ?>
                                    <div class="news_home <?= $i_tds % 2 == 0 ? 'right' : 'left' ?>">
                                        <a href="<?= $trading->url ?>"><img src="<?= $trading->getImage(100, 60)->src ?>" alt="<?= $trading->title ?>"></a>
                                        <div class="right-content1">
                                            <a href="<?= $trading->url ?>"><h6><?= $trading->title ?></h6></a>    
                                            <p><?= !empty($trading->description) ? SiteHelper::cut_string(strip_tags($trading->description), 80) : SiteHelper::cut_string(strip_tags($trading->content), 80) ?></p>   
                                        </div>
                                    </div>                               
                                    <?= $i_tds % 2 == 0 || $i_tds == count($list_trading) ? "</div> <div class='clear'></div><div class='col-md-12'>" : "" ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (!empty($list_rec)): ?>
                        <div id="tabs-3">
                            <div class="col-md-12">
                                <?php
                                $i_rec = 0;
                                foreach ($list_rec as $rec): $i_rec++;
                                    ?>
                                    <div class="news_home <?= $i_rec % 2 == 0 ? 'right' : 'left' ?>">
                                        <a href="<?= $rec->url ?>"><img src="<?= $rec->getImage(100, 60)->src ?>" alt="<?= $rec->title ?>"></a>
                                        <div class="right-content1">
                                            <a href="<?= $rec->url ?>"><h6><?= $rec->title ?></h6></a>    
                                            <p><?= !empty($rec->description) ? SiteHelper::cut_string(strip_tags($rec->description), 80) : SiteHelper::cut_string(strip_tags($rec->content), 80) ?></p>   
                                        </div>
                                    </div>                               
                                    <?= $i_rec % 2 == 0 || $i_rec == count($list_rec) ? "</div> <div class='clear'></div><div class='col-md-12'>" : "" ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <script>
                    $(function() {
                        $("#tabs").tabs();
                        $("#first").trigger('click');
                    });
                </script>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php if (!empty($list_partner)): ?>
    <section class="clients">
        <div class="container">
            <div class="row">
                <div class="section-heading">
                    <h2>Đối tác </h2>
                    <div class="line-dec"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="owl-clients">
                        <?php foreach ($list_partner as $partner): ?>
                            <div class="item" title="<?= $partner->title ?>">
                                <img src="<?= $partner->getImage(167, 133)->src ?>" alt="<?= $partner->title ?>">
                            </div>
                        <?php endforeach; ?>                        
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
