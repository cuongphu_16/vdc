<div id="page-heading">
    <div class="container">
        <div class="row">

        </div>
    </div>

</div>
<section class="blog-classic" style="padding:10px;">
    <div class="container">
        <div class="row">
            <div class="col-md-3">

                <div class="side-bar">
                    <div class="sidebar-widget">
                        <h4><?= Yii::t('main', 'Category') ?></h4>
                        <div class="line-dec"></div>
                        <ul>
                            <?php if (!empty($list_cate)): ?>
                                <?php foreach ($list_cate as $cate): ?>
                                    <li class="<?= !empty($_GET['url_about']) && $_GET['url_about'] == $cate->alias ? "active" : "" ?> <?= !empty($_GET['url_services']) && $_GET['url_services'] == $cate->alias ? "active" : "" ?>"><a href="<?= $cate->url ?>">:: &nbsp;&nbsp; <?= $cate->title ?></a></li>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <li><a href="<?= Yii::app()->createUrl('main/about/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'About') ?></a></li>
                                <li><a href="<?= Yii::app()->createUrl('main/services/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'Services') ?></a></li>
                                <li class="<?= Yii::app()->controller->action->id == 'trading' || Yii::app()->controller->action->id == 'detailTrading' ? "active" : "" ?>"><a href="<?= Yii::app()->createUrl('main/trading/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'Trading Floor') ?></a></li>
                                <li class="<?= Yii::app()->controller->action->id == 'news' || Yii::app()->controller->action->id == 'detailNews' ? "active" : "" ?>"><a href="<?= Yii::app()->createUrl('main/news/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'News') ?></a></li>
                                <li><a href="<?= Yii::app()->createUrl('main/recruitment/lang/' . $this->langCode) ?>"><?= Yii::t('main', 'Recruitment') ?></a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-md-9">
                <?php if (!empty($list)): ?>
                    <div class="blog-posts">
                        <?php foreach ($list as $p): ?>
                            <div class="blog-post">
                                <a href="<?= $p->url ?>"><img src="<?= $p->getImage(200, 131)->src ?>" alt="<?= $p->title ?>"></a>
                                <div class="down-content">

                                    <div class="right-cotent">
                                        <a href="<?= $p->url ?>"><h4><?= $p->title ?></h4></a>

                                    </div>
                                    <p> <?= !empty($p->description) ? SiteHelper::cut_string(strip_tags($p->description), 200) : SiteHelper::cut_string(strip_tags($p->content), 200) ?></p>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        <?php endforeach; ?>

                        <!-- cuongnx-->

                        <div class="col-md-12">
                            <div class="pagination">
                                <?php
                                $this->widget('XPCLinkPager', array(
                                    'pages' => $pages,
                                    'nextPageLabel' => '&raquo',
                                    'prevPageLabel' => '&laquo',
                                    'firstPageLabel' => '',
                                    'lastPageLabel' => '',
                                    'url' => Yii::app()->request->hostInfo . Yii::app()->request->url,
                                ));
                                ?>                                
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <center><h1><?= Yii::t('main', 'The content is being updated') ?></h1></center>
                <?php endif; ?>
            </div>
        </div>

    </div>
</section>
