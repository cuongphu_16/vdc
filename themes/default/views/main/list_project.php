<div id="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?= Yii::t('main', 'Projects') . ': ' . $cate->title ?></h1>
            </div>
        </div>
    </div>
</div>
<section class="projects-page">
    <div class="container">
        <?php if (!empty($list_project)): ?>

            <div class="row">
                <div class="projects">
                    <?php foreach ($list_project as $project): ?>
                        <div class="mix second-row col-md-3 category-<?= $project->cate_id ?> ">
                            <div class="thumb-holder">
                                <a href="<?= $project->url ?>"><img src="<?= $project->getThumb(270, 270)->src ?>" alt="<?= $project->title ?>"></a>
                                <div class="thumb-content">
                                    <div class="thumb-link">
                                        <a href="<?= $project->url ?>"><i class="fa fa-plus"></i></a>
                                    </div>
                                    <div class="thumb-text">
                                        <a href="<?= $project->url ?>"><h4><?= $project->title ?></h4></a>
                                        <span><i class="fa fa-folder-o"></i><?= $project->category->title ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>                      
                </div>                                        
            </div>
        <?php else: ?>
            <center><h1><?= Yii::t('main', 'The content is being updated') ?></h1></center>
        <?php endif; ?>
    </div>
</section>	