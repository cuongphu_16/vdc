<link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/cuong.css">
<div id="page-heading">
    <div class="container">
        <div class="row">

        </div>
    </div>

</div>
<section class="blog-classic" style="padding:10px;">
    <div class="container">
        <div class="row">            
            <div class="col-md-8">
                <h3 class="h3_title"><?= Yii::t('main', 'Our Recruitment') ?></h3>
                <?php if (!empty($list)): ?>
                    <?php foreach ($list as $p): ?>
                        <div class="box_rec">
                            <div class="img">
                                <img src="<?= $p->getImage(210, 147)->src ?>" alt="<?= $p->title ?>" />
                            </div>
                            <div class="content">
                                <h2><a href="<?= $p->url ?>"><?= $p->title ?></a></h2>
                                <div class="form-2">
                                    <div class="left">
                                        <strong><?= Yii::t('main', 'Quantity') ?>: </strong><?= $p->quantity ?>
                                    </div>
                                    <div class="right">
                                        <strong><?= Yii::t('main', 'Gender') ?>: </strong><?= $p->gender ?>
                                    </div>
                                </div>
                                <div class="form-2">
                                    <div class="left">
                                        <strong><?= Yii::t('main', 'Education') ?>: </strong><?= $p->education ?>
                                    </div>
                                    <div class="right">
                                        <strong><?= Yii::t('main', 'Experience') ?>: </strong><?= $p->experience ?>
                                    </div>
                                </div>
                                <div class="form-2">
                                    <div class="left">
                                        <strong><?= Yii::t('main', 'Dateline') ?>: </strong><?= $p->dateline ?>
                                    </div>
                                    <div class="right">
                                        <strong><?= Yii::t('main', 'Salary') ?>: </strong><?= $p->salary ?>
                                    </div>
                                </div>
                                <div class="form-2">
                                    <div class="left">
                                        &nbsp;
                                    </div>
                                    <div class="right">
                                        <a href="<?= $p->url ?>" class="view_more"><?= Yii::t('main', 'View more') ?> →</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    <?php endforeach; ?>

                    <!-- cuongnx-->

                    <div class="col-md-12">
                        <div class="pagination">
                            <?php
                            $this->widget('XPCLinkPager', array(
                                'pages' => $pages,
                                'nextPageLabel' => '&raquo',
                                'prevPageLabel' => '&laquo',
                                'firstPageLabel' => '',
                                'lastPageLabel' => '',
                                'url' => Yii::app()->request->hostInfo . Yii::app()->request->url,
                            ));
                            ?>                                
                        </div>
                    </div>
                <?php else: ?>
                    <center><h1><?= Yii::t('main', 'The content is being updated') ?></h1></center>
                <?php endif; ?>
            </div>

            <div class="col-md-4">
                <h3 class="h3_title" style="border: 0; width: 100%"><?= Yii::t('main','Latest News') ?></h3>

                    <div class="list_news_rec">
                        <?php
                        if (!empty($list_new)):
                            foreach ($list_new as $news):
                                ?>
                                <div class="item_news" style="margin-bottom: 15px;">
                                    <div style="float: left; width: 30%">
                                        <img style="width: 100%" src="<?= $news->getImage(80, 80)->src ?>" />
                                    </div>
                                    <div style="float: right; width: 65%">
                                        <h4 style="margin-top: 0;"><a href="<?= $news->url ?>"><?= $news->title ?></a></h4>
                                        <p><?= !empty($news->description) ? SiteHelper::cut_string($news->description, 100) : SiteHelper::cut_string($news->content, 100) ?></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </div>
            </div>
        </div>

    </div>
</section>
