<link rel="stylesheet" href="<?= $this->assetsUrl ?>/css/cuong.css">
<div id="page-heading">
    <div class="container">
        <div class="row">

        </div>
    </div>

</div>
<section class="blog-classic" style="padding:10px;">
    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <div class="detail_rec">
                    <h4><strong><?= $detail->title ?></strong></h4>
                    <hr>
                    <div class="box_rec" style="border: 0;height: 90px">

                        <div class="form-2">
                            <div class="left">
                                <strong><?= Yii::t('main', 'Quantity') ?>: </strong><?= $detail->quantity ?>
                            </div>
                            <div class="right">
                                <strong><?= Yii::t('main', 'Gender') ?>: </strong><?= $detail->gender ?>
                            </div>
                        </div>
                        <div class="form-2">
                            <div class="left">
                                <strong><?= Yii::t('main', 'Education') ?>: </strong><?= $detail->education ?>
                            </div>
                            <div class="right">
                                <strong><?= Yii::t('main', 'Experience') ?>: </strong><?= $detail->experience ?>
                            </div>
                        </div>
                        <div class="form-2">
                            <div class="left">
                                <strong><?= Yii::t('main', 'Dateline') ?>: </strong><?= $detail->dateline ?>
                            </div>
                            <div class="right">
                                <strong><?= Yii::t('main', 'Salary') ?>: </strong><?= $detail->salary ?>
                            </div>
                        </div>                                               
                    </div>
                    <div class="clearfix"></div>
                    <hr>

                    <img src="<?= $detail->getImage(800, 0)->src ?>" width="100%" />
                    <div class="content">
                        <div class="left">
                            <?= Yii::t('main', 'Description') ?>
                        </div>
                        <div class="right">
                            <?= $detail->description ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br />
                    <div class="content">
                        <div class="left">
                            <?= Yii::t('main', 'Request') ?>
                        </div>
                        <div class="right">
                            <?= $detail->request ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br />
                    <div class="content">
                        <div class="left">
                            <?= Yii::t('main', 'Benefit') ?>
                        </div>
                        <div class="right">
                            <?= $detail->benefit ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br />
                    <div class="content">
                        <div class="left">
                            <?= Yii::t('main', 'Resume') ?>
                        </div>
                        <div class="right">
                            <?= $detail->cv ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br />
                </div>
                <div class="clearfix"></div>
                <?php if (!empty($other_cate)): ?>
                    <div class="col-md-12" style="margin-top: 20px;border: 1px solid rgba(27, 10, 10, 0.24); padding-bottom: 15px;">      
                        <div class="other_rec">
                            <h5><strong><?= Yii::t('main', 'Other Post') ?></strong></h5>
                            <?php
                            $i_other = 0;
                            foreach ($other_cate as $other): $i++;
                                ?>
                                <div class="<?= $i % 2 == 0 ? 'chan' : 'le' ?>">
                                    <div class="img"><img src='<?= $other->getImage(150, 0)->src ?>' /></div>
                                    <div class="title"><a href="<?= $other->url ?>" class="hover_a"><?= $other->title ?></a></div>
                                </div>
                                <?= $i % 2 == 0 ? ' <div class="clearfix end_"></div>' : '' ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>

            </div>

            <div class="col-md-4">               

                <div class="list_news_rec">
                    <h3 class="h3_title" style="border: 0; width: 100%;margin-top: 0"><?= Yii::t('main', 'Latest News') ?></h3>
                    <?php
                    if (!empty($list_new)):
                        foreach ($list_new as $news):
                            ?>
                            <div class="item_news" style="margin-bottom: 15px;">
                                <div style="float: left; width: 30%">
                                    <img style="width: 100%" src="<?= $news->getImage(80, 80)->src ?>" />
                                </div>
                                <div style="float: right; width: 65%">
                                    <h4 style="margin-top: 0;"><a href="<?= $news->url ?>"><?= $news->title ?></a></h4>
                                    <p><?= !empty($news->description) ? SiteHelper::cut_string($news->description, 100) : SiteHelper::cut_string($news->content, 100) ?></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
        </div>

    </div>
</section>
